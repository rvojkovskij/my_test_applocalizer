package com.avaloq.build;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.velocity.app.Velocity;
import org.eclipse.jgit.api.CheckoutCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avaloq.build.config.Build;
import com.avaloq.build.config.Configuration;

/**
 * Hello world!
 * 
 */
public class App {
	
	/**
	 * Initializes Velocity
	 */
	static {
    	Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM, new Slf4jLogChute());
    	Velocity.init();
	}

	/**
	 * The Logger for this Class
	 */
	private static Logger log = LoggerFactory.getLogger(App.class);

	/**
	 * Entry point for the program.
	 * Uses the App class to do all the work and reports errors.
	 * @param args Takes one argument, the build number.
	 */
	public static void main(String[] args) {
		if(args.length == 1) {
			String buildNumber = args[0];
			long millis = System.currentTimeMillis();
			try {
				final App app = new App();
				try {
					// clean up here too, just in case directories still exist.
					app.cleanUp();
					// checkout the large repository
					app.initEnvironment();
					try {
						app.runBuilds();
						app.copyGeneratedFiles(buildNumber);
					} catch (StackOverflowError e) {
						handleError(new ConfigurationException("Dependency cycle detected. Check your dependencies for cycles.", e));
					}
				} catch (Exception e) {
					handleError(e);
				} finally {
					// clean up
					// app.cleanUp();
					long elapsedTime = System.currentTimeMillis() - millis;
					log.debug("Build took " + elapsedTime + "ms");
				}
			} catch (Exception configException) {
				handleError(configException);
			}
		} else {
			handleError(new IllegalArgumentException("The build number argument is missing."));
		}
	}

	/**
	 * The repository URL
	 */
	final String repository;
	
	/**
	 * The repository branch
	 */
	final String branch;
	
	/**
	 * The directory where the source is checked out in
	 */
	private File sourceDir;
	
	/**
	 * The target directory (where the build happens)
	 */
	private File temporaryDirectory;

	/**
	 * Creates a new instance of the app class.
	 * @throws ConfigurationException Errors are passed back to the main method
	 */
	private App() throws ConfigurationException {
		repository = Configuration.getInstance().getRepository();
		branch = Configuration.getInstance().getRepositoryBranch();
		sourceDir = new File(Configuration.getInstance().getSourceDir());
		// TODO The naming is not ideal, we should rename Target Dir to temp dir or smth
		temporaryDirectory = new File(Configuration.getInstance().getTemporaryDir());
	}

	/**
	 * Initiates the Enviroment (clones the repository)
	 * @throws Exception Errors are passed back to the main method
	 */
	private void initEnvironment() throws Exception {
		temporaryDirectory.mkdir();
		CloneCommand clone = Git.cloneRepository();
		clone.setBare(false);
		clone.setCloneAllBranches(true);
		clone.setDirectory(sourceDir).setURI(repository);
		// This can be used for local testing:
		log.debug("Cloning " + repository + " into " + sourceDir.getAbsolutePath());
		Git git = clone.call();
		PullCommand pc = git.pull();
		pc.call();
		CheckoutCommand co = git.checkout();
		co.setName("remotes/origin/"+branch);
		co.setCreateBranch(false);
		co.call();
	}

	/**
	 * Runs all the builds
	 * @throws Exception Errors are passed back to the main method
	 */
	private void runBuilds() throws Exception {
		log.debug("builds: "+Configuration.getInstance().getBuilds().size());
		for (Build build : Configuration.getInstance().getBuilds().values()) {
			if (!build.wasExecuted()) {
				build.build();
			}
		}
		for (Build build : Configuration.getInstance().getBuilds().values()) {
			if (!build.werePostActionsExecuted()) {
				build.runPostBuildActions();
			}
		}
	}
	
	/**
	 * Copies all the generated apk files to the build's directory
	 * @param buildNumber The number of this build (eg. 23)
	 * @throws Exception If an error occurs (copying)
	 */
	private void copyGeneratedFiles(String buildNumber) throws Exception {
		File buildDirectory = new File(MessageFormat.format(Configuration.getInstance().getOutputDir(), buildNumber));
		// copy apks to the build directory
		File apkDestDir = new File(buildDirectory.getAbsolutePath() + File.separator + "apk");
		log.debug("Creating directory " + apkDestDir.getAbsolutePath());
		apkDestDir.mkdirs();
		for(Build build : Configuration.getInstance().getBuilds().values()) {
			File srcApkFile = new File(Configuration.getInstance().getTemporaryDir() + File.separator + "apk" + File.separator + build.getProjectName() + "-debug.apk");
			if(srcApkFile.exists()) {
				log.debug("Copying file " + srcApkFile.getAbsolutePath() + " to directory " + apkDestDir.getAbsolutePath());
				FileUtils.copyFileToDirectory(srcApkFile, apkDestDir);
			}
		}
		// copy docs to the build directory
		File docDestDir = new File(buildDirectory.getAbsolutePath() + File.separator + "doc");
		docDestDir.mkdirs();
		log.debug("Creating directory " + docDestDir.getAbsolutePath());
		File docSrcDir = new File(Configuration.getInstance().getTemporaryDir() + File.separator + "doc");
		if(docSrcDir.exists()) {
			log.debug("Copying directory " + docSrcDir.getAbsolutePath() + " to " + docDestDir.getAbsolutePath());
			FileUtils.copyDirectory(docSrcDir, docDestDir);
		}
	}

	/**
	 * Cleans up after the program was run
	 * @throws IOException If a directory can't be deleted
	 */
	private void cleanUp() throws IOException {
		if(sourceDir.exists()) FileUtils.deleteDirectory(sourceDir);
		if(temporaryDirectory.exists()) FileUtils.deleteDirectory(temporaryDirectory);
	}
	
	/**
	 * Common error handling
	 * @param e The caught exception
	 */
	private static void handleError(Exception e) {
		log.error(e.getMessage(), e);
		System.exit(-1);
	}

}