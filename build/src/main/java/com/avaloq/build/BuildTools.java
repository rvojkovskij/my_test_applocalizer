package com.avaloq.build;

import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Method-toolkit for the build process
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class BuildTools {

	private static final Logger log = LoggerFactory.getLogger(BuildTools.class);

	private static DocumentBuilder documentBuilder = null;

	/**
	 * Returns a configured instance of the DocumentBuilder (to parse xml)
	 * 
	 * @return A configured instance of the DocumentBuilder (to parse xml)
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public static DocumentBuilder getDocumentBuilder() throws Exception {
		if (documentBuilder == null) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setNamespaceAware(true);
			documentBuilder = dbf.newDocumentBuilder();
		}
		return documentBuilder;
	}

	/**
	 * Generates XML markup for a Node
	 * 
	 * @param node
	 *            The node to generate xml for
	 * @return The node's xml code as a string
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public static String nodeToString(Node node) throws Exception {
		StringBuffer sb = new StringBuffer();
		if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
			sb.append("<");
			sb.append(node.getNodeName());
			NamedNodeMap attributes = node.getAttributes();
			for (int i = 0; i < attributes.getLength(); i++) {
				sb.append(" ");
				if (attributes.item(i).getPrefix() != null) {
					sb.append(attributes.item(i).getPrefix());
					sb.append(":");
				}
				sb.append(attributes.item(i).getLocalName());
				sb.append("=");
				sb.append('"');
				sb.append(attributes.item(i).getNodeValue());
				sb.append('"');
			}
			NodeList childNodes = node.getChildNodes();
			if (childNodes.getLength() == 0) {
				sb.append("/>");
			} else {
				sb.append(">");
				for (int i = 0; i < childNodes.getLength(); i++) {
					sb.append(nodeToString(childNodes.item(i)));
				}
				sb.append("</");
				sb.append(node.getNodeName());
				sb.append(">");
			}
		}
		return sb.toString();
	}

	/**
	 * Indicates whether an activity is in the packages of a build
	 * 
	 * @param activityName
	 *            The activity name
	 * @param buildPackages
	 *            provide the packages to check
	 * @return true if the activity is in one of the packages
	 */
	public static boolean isActivitiyInBuildPackages(String activityName, List<String> buildPackages) {
		for (String buildPkg : buildPackages) {
			if (activityName.startsWith(buildPkg)) {
				return true;
			} else {
				log.debug("Activity " + activityName + " does not match the package name: " + buildPkg);
			}
		}
		return false;
	}

}
