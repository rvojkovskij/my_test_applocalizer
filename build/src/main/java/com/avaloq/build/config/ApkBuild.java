package com.avaloq.build.config;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.avaloq.build.BuildTools;

/**
 * This class does builds of apk files. <br />
 * The action happens in {@link #execute()}, where all the private methods are
 * called.
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ApkBuild extends Build {

	/**
	 * The Logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(ApkBuild.class);

	/**
	 * The Java-packages that the apk project will use
	 */
	private final List<String> buildPackages;

	/**
	 * Creates a new ApkBuild
	 * 
	 * @param projectName
	 *            The name of the resulting apk
	 * @param buildPackages
	 *            The Java-Packages that the apk project will use
	 * @param dependencies
	 *            The dependencies of this build
	 */
	public ApkBuild(String projectName, List<String> buildPackages, List<String> dependencies) {
		super(projectName, dependencies);
		this.buildPackages = buildPackages;
	}

	@Override
	public void execute() throws Exception {
		log.info("Building " + getProjectName());
		copyGitignore();
		copySource();
		createResources();
		copyLibraries();
		createProjectProperties();
		
		createAndroidManifest(null);
		buildSecondaryLanguages();
		// androidUpdateProject();
		// antClean();
		// antDebugBuild();
		// copyApk();
	}

	/**
	 * Creates the file .gitignore in this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	protected void copyGitignore() throws Exception {
		// File srcGitignore = new
		// File(ClassLoader.getSystemResource("templates/apk/gitignore").toURI());
		File srcGitignore = new File("target/classes/apk-gitignore");
		File destGitignore = new File(getBuildDirectory().getAbsolutePath() + File.separator + ".gitignore");
		FileUtils.copyFile(srcGitignore, destGitignore);
	}

	/**
	 * Copies the source code to the src directory inside this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void copySource() throws Exception {
		File srcDir = new File(getBuildDirectory().getAbsolutePath() + File.separator + "src");
		log.debug("Creating directory: " + srcDir.getAbsolutePath());
		srcDir.mkdirs();
		for (String buildPackage : buildPackages) {
			log.debug("buildPackage = " + buildPackage);
			String packageDirectory = buildPackage.replaceAll("\\.", File.separator);
			log.debug("packageDirectory = " + packageDirectory);
			File sourcePackageDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "src" + File.separator + packageDirectory);
			File projectPackageDir = new File(srcDir.getAbsolutePath() + File.separator + packageDirectory);
			log.debug("Creating directory " + projectPackageDir.getAbsolutePath());
			projectPackageDir.mkdirs();
			log.debug("Copying directory " + sourcePackageDir.getAbsolutePath() + " to " + projectPackageDir.getAbsolutePath());
			log.debug("exists = " + sourcePackageDir.exists());
			FileUtils.copyDirectory(sourcePackageDir, projectPackageDir);
		}
	}

	/**
	 * Creates the project.properties in this build's directory from a template
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createProjectProperties() throws Exception {
		final Template template = Velocity.getTemplate("target/classes/apk-project.properties.vm");
		File destFile = new File(getBuildDirectory() + File.separator + "project.properties");
		destFile.createNewFile();
		final VelocityContext context = new VelocityContext();
		context.put("dollar", "$");
		context.put("androidTargetSdk", Configuration.getInstance().getAndroidTargetSdk());
		context.put("androidMinSdk", Configuration.getInstance().getAndroidMinSdk());
		context.put("dependencies", getDependencies());
		FileWriter fw = new FileWriter(destFile, false);
		template.merge(context, fw);
		fw.close();

	}

	/**
	 * Creates the AndroidManifest.xml in this build's directory from a template
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createAndroidManifest(String specialPackageName) throws Exception {
		// final Template template =
		// Velocity.getTemplate("templates/apklib/AndroidManifest.xml.vm");
		final Template template = Velocity.getTemplate("target/classes/apk-AndroidManifest.xml.vm");
		File destFile = new File(getBuildDirectory() + File.separator + "AndroidManifest.xml");
		destFile.createNewFile();
		final VelocityContext context = new VelocityContext();
		context.put("dollar", "$");
		if(specialPackageName == null) {
			context.put("apkPackage", "com.avaloq.framework");
		} else {
			context.put("apkPackage", specialPackageName);
		}
		context.put("androidTargetSdk", Configuration.getInstance().getAndroidTargetSdk());
		context.put("androidMinSdk", Configuration.getInstance().getAndroidMinSdk());
		context.put("apkIcon", getApkIcon());
		context.put("apkTheme", getApkTheme());
		context.put("apkLabel", Configuration.getInstance().getString("build.apkLabel." + getProjectName()));
		context.put("permissions", getPermissions());
		context.put("activities", getActivities());
		FileWriter fw = new FileWriter(destFile, false);
		template.merge(context, fw);
		fw.close();
		log.debug("Wrote android manifest to: " + destFile.getAbsolutePath());
	}

	/**
	 * Copies all the resources matching the prefix from the avaloqkit project
	 * to this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createResources() throws Exception {
		File destResourceDirectory = new File(getBuildDirectory() + File.separator + "res");
		log.debug("Creating directory " + destResourceDirectory.getAbsolutePath());
		destResourceDirectory.mkdirs();
		File sourceResourceDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "res");
		for (File sourceResourceSubdirectory : sourceResourceDir.listFiles()) {
			if (sourceResourceSubdirectory.isDirectory()) {
				for (File sourceResourceFile : sourceResourceSubdirectory.listFiles()) {
					if (sourceResourceFile.getName().startsWith(getResourcePrefix())) {
						File destResourceFile = new File(destResourceDirectory.getAbsolutePath() + File.separator + sourceResourceSubdirectory.getName() + File.separator + sourceResourceFile.getName());
						File destResourceSubdirectory = destResourceFile.getParentFile();
						if (!destResourceSubdirectory.exists()) {
							log.debug("Creating directory " + destResourceSubdirectory.getAbsolutePath());
							destResourceSubdirectory.mkdirs();
						}
						log.debug("Copying file " + sourceResourceFile.getAbsolutePath() + " to " + destResourceFile.getAbsolutePath());
						FileUtils.copyFile(sourceResourceFile, destResourceFile);
					}
				}
			}
		}
	}

	/**
	 * Invokes "ant clean" in this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
//	private void antClean() throws Exception {
//		String antCmd = Configuration.getInstance().getAntBinary() + " clean";
//		systemCall(antCmd, getBuildDirectory());
//	}

	/**
	 * Invokes "ant debug" in this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
//	private void antDebugBuild() throws Exception {
//		String antCmd = Configuration.getInstance().getAntBinary() + " debug";
//		systemCall(antCmd, getBuildDirectory());
//	}

	/**
	 * Copies the generated apk file to the output directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
//	private void copyApk() throws Exception {
//		File srcApk = new File(getBuildDirectory().getAbsolutePath() + File.separator + "bin" + File.separator + getProjectName() + "-debug.apk");
//		File destApk = new File(Configuration.getInstance().getTemporaryDir() + File.separator + "apk" + File.separator + getProjectName() + "-debug.apk");
//		if (!destApk.getParentFile().exists()) {
//			log.debug("Creating directory " + destApk.getParentFile().getAbsolutePath());
//			destApk.getParentFile().mkdirs();
//		}
//		log.debug("Copying file " + srcApk.getAbsolutePath() + " to " + destApk.getAbsolutePath());
//		FileUtils.copyFile(srcApk, destApk);
//	}

	/**
	 * Returns the uses-permission tags of this build (currently all of the
	 * source's manifest)
	 * 
	 * @return All the uses-permission tags (xml format)
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private List<String> getPermissions() throws Exception {
		Document document = BuildTools.getDocumentBuilder().parse(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "AndroidManifest.xml");
		Element node = document.getDocumentElement();
		NodeList childNodes = node.getChildNodes();
		List<String> permissions = new ArrayList<String>();
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeName().equals("uses-permission")) {
				permissions.add(BuildTools.nodeToString(childNodes.item(i)));
			}
		}
		return permissions;
	}

	/**
	 * Returns the activities for this build (all that match this build's
	 * packages)
	 * 
	 * @return All the activities for this build
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private List<String> getActivities() throws Exception {
		Document document = BuildTools.getDocumentBuilder().parse(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "AndroidManifest.xml");
		Element node = document.getDocumentElement();
		NodeList childNodes = node.getChildNodes();
		List<String> activities = new ArrayList<String>();
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeName().equalsIgnoreCase("application")) {
				NodeList applicationChildNodes = childNodes.item(i).getChildNodes();
				for (int ii = 0; ii < applicationChildNodes.getLength(); ii++) {
					if (applicationChildNodes.item(ii).getNodeName().equalsIgnoreCase("activity")) {
						Node activityNode = applicationChildNodes.item(ii);
						Node nameAttribute = activityNode.getAttributes().getNamedItemNS("http://schemas.android.com/apk/res/android", "name");
						String activityName = nameAttribute.getNodeValue();
						if (BuildTools.isActivitiyInBuildPackages(activityName, buildPackages)) {
							String activity = BuildTools.nodeToString(activityNode);
							log.debug("Found activity: " + activityName);
							log.debug(activity);
							activities.add(activity);
						} else {
							log.debug("Activity does not match the packages: "+activityName);
						}
					}
				}
			}
		}
		return activities;
	}

	/**
	 * Returns the apk icon definition
	 * @return The apk icon definition
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public String getApkIcon() throws Exception {
		return Configuration.getInstance().getString("build.apkIcon." + getProjectName());
	}

	/**
	 * Returns the apk's theme definition
	 * @return The apk's theme definition
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public String getApkTheme() throws Exception {
		return Configuration.getInstance().getString("build.apkTheme." + getProjectName());
	}
	
	@Override
	protected void doPostBuildActions() throws Exception {
		createAndroidManifest(Configuration.getInstance().getString("build.apkPackage." + getProjectName()));
	}

}