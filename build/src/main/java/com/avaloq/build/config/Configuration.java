package com.avaloq.build.config;

import java.io.File;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The configuration class
 * <br />
 * Reads the build.properties configuration file and returns it's values
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class Configuration extends PropertiesConfiguration {
	
	/**
	 * The logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(Configuration.class);
	
	/**
	 * The singleton instance
	 */
	private static Configuration instance = null;
	
	/**
	 * A map of build names and their objects
	 */
	private Map<String,Build> builds = new HashMap<String,Build>();
	
	/**
	 * The version identifier of this build (format: yyyymmddhhmm)
	 */
	private final String buildVersion;
	
	/**
	 * Returns the singleton instance
	 * @return The singleton instance
	 * @throws ConfigurationException Errors are passed back to the main method
	 */
	public static Configuration getInstance() throws ConfigurationException {
		if(instance == null) {
			log.debug(new File(".").getAbsolutePath());
			File configFile = new File("target/classes/build.properties");
			if(configFile.exists()) {
				instance = new Configuration(configFile);
			} else {
				throw new ConfigurationException("Config file (" + configFile.getAbsolutePath() + ") does not exist.");
			}
		}
		return instance;
	}

	/**
	 * The constructor using a URL
	 * @param url The configuration file URL
	 * @throws ConfigurationException Errors are passed back to the main method
	 */
	private Configuration(URL url) throws ConfigurationException {
		super(url);
		init();
		buildVersion = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
	}
	
	/**
	 * The constructor using a File
	 * @param file The configuration File
	 * @throws ConfigurationException Errors are passed back to the main method
	 */
	private Configuration(File file) throws ConfigurationException {
		super(file);
		init();
		buildVersion = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
	}
	
	/**
	 * Constructor using no values
	 */
	private Configuration() {
		super();
		init();
		buildVersion = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
	}

	/**
	 * Shared method to initialize the configuration object: Fills the map of builds.
	 */
	private void init() {
		List<String> provided = getStringList("build.provided");
		for(String buildName : provided) {
			log.debug("Adding "+buildName+" to the build pipeline");
			builds.put(buildName, new ProvidedBuild(buildName, getStringList("build.dependency."+buildName)));
		}
		List<String> apklibs = getStringList("build.apklib");
		for(String buildName : apklibs) {
			log.debug("Adding "+buildName+" to the build pipeline");
    		builds.put(buildName, new ApklibBuild(buildName, getStringList("build.package."+buildName), getStringList("build.dependency."+buildName)));
		}
		List<String> apps = getStringList("build.apk");
		for(String buildName : apps) {
			log.debug("Adding "+buildName+" to the build pipeline");
    		builds.put(buildName, new ApkBuild(buildName, getStringList("build.package."+buildName), getStringList("build.dependency."+buildName)));
		}
		List<String> antBuilds = getStringList("build.javadoc");
		for(String buildName : antBuilds) {
			log.debug("Adding "+buildName+" to the build pipeline");
    		builds.put(buildName, new JavadocBuild(buildName, getStringList("build.dependency."+buildName)));
		}
	}
	
	/**
	 * Returns the Builds
	 * @return The Builds
	 */
	public Map<String,Build> getBuilds() {
		return builds;
	}
	
	/**
	 * Returns a specific build
	 * @param buildName The name of the build
	 * @return The specified build or null
	 */
	public Build getBuild(String buildName) throws ConfigurationException {
		if(builds.containsKey(buildName)) {
			return builds.get(buildName);
		} else {
			throw new ConfigurationException("The build with the name \"" + buildName +"\" does not exist.");
		}
	}
	
	/**
	 * Returns the repository for this build or null
	 * @return The repository for this build or null
	 */
	public String getRepository() {
		return getString("build.repository");
	}
	
	/**
	 * Returns the repository branch for this build or null
	 * @return The repository branch for this build or null
	 */
	public String getRepositoryBranch() {
		return getString("build.repository.branch");
	}
	
	/**
	 * Returns the source directory
	 * @return The source directory
	 */
	public String getSourceDir() {
		return getString("build.sourceDir");
	}
	
	/**
	 * Returns the target directory
	 * @return The target directory
	 */
	public String getTemporaryDir() {
		return getString("build.temporaryDir");
	}
	
	/**
	 * Returns the target android sdk
	 * @return The target android sdk
	 */
	public String getAndroidTargetSdk() {
		return getString("build.androidTargetSdk");
	}
	
	/**
	 * Returns the minimal android sdk
	 * @return The minimal android sdk
	 */
	public String getAndroidMinSdk() {
		return getString("build.androidMinSdk");
	}
	
	/**
	 * Returns the android sdk directory
	 * @return The android sdk directory
	 */
	public String getAndroidSdkDir() {
		return getString("build.androidSdkDir");
	}

	/**
	 * Returns the ant binary
	 * @return The ant binary
	 */
	public String getAntBinary() {
		return getString("build.antBinary");
	}
	
	/**
	 * Returns the build version
	 * @return The build version
	 */
	public String getBuildVersion() {
		return buildVersion;
	}
	
	/**
	 * Returns the output directory
	 * @return The output directory
	 */
	public String getOutputDir() {
		return getString("build.outputDir");
	}
	
	/**
	 * Returns a list of Strings
	 * @return A list of strings
	 */
	@SuppressWarnings("unchecked")
	public List<String> getStringList(String key) {
		return (List<String>)getList(key); 
	}
	
}
