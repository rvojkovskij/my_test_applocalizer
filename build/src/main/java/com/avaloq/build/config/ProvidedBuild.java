package com.avaloq.build.config;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A build that is taken "as-is" from the main repository.
 * <br />
 * Usually library projects from external parties are provided builds.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ProvidedBuild extends Build {
	
	/**
	 * The logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(ProvidedBuild.class);

	/**
	 * Creates a new provided build
	 * @param projectName The build's name
	 * @param dependencies The build's dependencies
	 */
	public ProvidedBuild(String projectName, List<String> dependencies) {
		super(projectName, dependencies);
	}

	@Override
	protected void execute() throws Exception {
		copyAndroidProject();
//		androidUpdateProject();
	}
	
	/**
	 * Copies the complete android library project
	 * @throws Exception Errors are passed back to the main method
	 */
	private void copyAndroidProject() throws Exception {
		File sourceDirectory = new File(getSourceDirectory().getAbsolutePath() + File.separator + getProjectName());
		File destDirectory = getBuildDirectory();
		log.debug("Copying directory " + sourceDirectory + " to " + destDirectory);
		FileUtils.copyDirectory(sourceDirectory, destDirectory);
	}

}
