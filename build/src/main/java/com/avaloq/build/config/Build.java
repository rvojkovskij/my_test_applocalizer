package com.avaloq.build.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.AddCommand;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.CommitCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.RemoteRefUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.avaloq.build.BuildTools;

/**
 * Abstract parent class for Builds. Has some general stuff like dependencies,
 * project name, source directory, build directory and if it was already
 * executed. There's also a few commonly used helper methods. <br />
 * Overriding classes must implement {@link #execute()}.
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public abstract class Build {

	/**
	 * The logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(Build.class);

	/**
	 * The name of this build (usually the project name)
	 */
	private final String projectName;

	/**
	 * The dependencies for this build
	 */
	private final List<String> dependencies;

	/**
	 * If the build was already executed
	 */
	protected boolean executed = false;

	/**
	 * If the build post-actions were already executed
	 */
	protected boolean werePostActionsExecuted = false;

	/**
	 * The directory for this build
	 */
	private File buildDirectory;

	/**
	 * The source directory from the configuration
	 */
	private File sourceDirectory;

	/**
	 * New instances must have a project name and dependencies (0 - n)
	 * 
	 * @param projectName
	 *            This build's name
	 * @param dependencies
	 *            This build's dependencies
	 */
	public Build(String projectName, List<String> dependencies) {
		this.projectName = projectName;
		this.dependencies = dependencies;
	}

	/**
	 * Returns the project name
	 * 
	 * @return The project name
	 */
	public String getProjectName() {
		return this.projectName;
	}

	/**
	 * Returns the dependencies
	 * 
	 * @return The dependencies
	 */
	public List<String> getDependencies() {
		return dependencies;
	}

	/**
	 * Returns if the build was executed
	 * 
	 * @return If the build was executed
	 */
	public boolean wasExecuted() {
		return executed;
	}

	public void buildDependencies() throws Exception {
		for (String dependency : dependencies) {
			Build dependencyBuild = Configuration.getInstance().getBuild(dependency);
			if (dependencyBuild == null) {
				throw new ConfigurationException("Missing dependency: " + dependency);
			}
			if (!dependencyBuild.wasExecuted()) {
				dependencyBuild.build();
			}
		}
	}

	public void buildSecondaryLanguages() throws Exception {
		try {
			File defaultValuesFile = new File(getBuildDirectory() + File.separator + "res" + File.separator + "values" + File.separator + getResourcePrefix() + "_strings.xml");
			log.debug("Searching for default file " + defaultValuesFile.getAbsolutePath());
			if (defaultValuesFile.exists()) {
				// reading the default language and saving all string names it
				// finds
				List<String> defaultStringNames = new ArrayList<String>();
				Document document = BuildTools.getDocumentBuilder().parse(defaultValuesFile.getAbsolutePath());
				Element node = document.getDocumentElement();
				NodeList childNodes = node.getChildNodes();
				log.debug("Child nodes: " + childNodes.getLength());
				for (int i = 0; i < childNodes.getLength(); i++) {
					if (childNodes.item(i).hasAttributes()) {
						defaultStringNames.add(childNodes.item(i).getAttributes().getNamedItem("name").getTextContent());
					}
				}
				log.debug("Default language file " + defaultValuesFile.getAbsolutePath() + " exists and contains " + defaultStringNames.size() + " strings.");

				// reading all secondary languages and comparing to the default
				// language
				for (String lang : Configuration.getInstance().getStringList("build.secondaryLanguages"+getProjectName())) {
					// if the directory doesn't exist, create it
					String langDirectoryPath = getBuildDirectory() + File.separator + "res" + File.separator + "values-" + lang;
					File secondaryValuesDirectory = new File(langDirectoryPath);
					if (!secondaryValuesDirectory.exists()) {
						secondaryValuesDirectory.mkdirs();
						log.debug("The directory " + langDirectoryPath + " was created because it didn't exist.");
					}

					String langFilePath = langDirectoryPath + File.separator + getResourcePrefix() + "_strings.xml";
					File secondaryValuesFile = new File(langFilePath);
					log.debug("Searching for secondary language file " + secondaryValuesFile.getAbsolutePath());

					Document langDoc = null;
					Element langNode = null;
					List<String> langStringNames = new ArrayList<String>();

					// if the string file exists, read all its contents and make
					// a list of all string names
					if (secondaryValuesFile.exists()) {
						langDoc = BuildTools.getDocumentBuilder().parse(secondaryValuesFile.getAbsolutePath());
						langNode = langDoc.getDocumentElement();
						NodeList langChildNodes = langNode.getChildNodes();
						for (int i = 0; i < langChildNodes.getLength(); i++) {
							if (langChildNodes.item(i).hasAttributes()) {
								langStringNames.add(langChildNodes.item(i).getAttributes().getNamedItem("name").getNodeValue());
							}
						}
						log.debug("Secondary language file " + secondaryValuesFile.getAbsolutePath() + " exists and contains " + langStringNames.size() + " strings.");
					}
					// if the string file doesn't exist, create an empty file
					else {
						secondaryValuesFile.createNewFile();
						FileWriter fw = new FileWriter(secondaryValuesFile, false);
						fw.write("<?xml version=\"1.0\" encoding=\"utf-8\"?><resources></resources>");
						fw.close();
						log.debug("The file " + langFilePath + " was created because it didn't exist.");
						langDoc = BuildTools.getDocumentBuilder().parse(secondaryValuesFile.getAbsolutePath());
						langNode = langDoc.getDocumentElement();
					}

					// go through all default language strings and check if they
					// occur in the current language
					boolean isChanged = false;
					for (String defaultString : defaultStringNames) {
						if (!langStringNames.contains(defaultString)) {
							Element newString = (Element) langDoc.createElement("string");
							langNode.appendChild(newString);
							newString.setAttribute("name", defaultString);
							newString.appendChild(langDoc.createTextNode(Configuration.getInstance().getString("build.missingTranslationText")));
							isChanged = true;
							log.warn("String " + defaultString + " cannot be found.");
						} else {
							log.debug("String " + defaultString + " was found.");
						}

						// if there were any missing strings, add them and save
						// the new xml-file
					}
					if (isChanged) {
						Transformer transformer = TransformerFactory.newInstance().newTransformer();
						Result output = new StreamResult(secondaryValuesFile);
						Source input = new DOMSource(langDoc);
						transformer.transform(input, output);
						log.debug("File successfully written.");
					}
				}
			}
		} catch (Exception e) {
			log.error("Error while creating string file: " + e.getMessage());
		}
	}

	/**
	 * The instance of Git, in case this build should be pushed in a repository
	 */
	private Git git = null;

	/**
	 * Builds this build: <br />
	 * - Builds all dependencies <br />
	 * - Checks out the repository (if thats set) <br />
	 * - Prunes the repository <br />
	 * - Invokes {@link #execute()} <br />
	 * - Commits and pushes to the repository <br />
	 * - Sets executed to true
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public void build() throws Exception {
		try {
			buildDependencies();
			File buildDirectory = getBuildDirectory();
			log.debug("Creating directory: " + buildDirectory.getAbsolutePath());
			buildDirectory.mkdirs();
			String buildRepository = Configuration.getInstance().getString("build.repository." + projectName);
			if (buildRepository != null) {
				// we have a build that will be deployed to a repository, lets
				// check it out
				CloneCommand clone = Git.cloneRepository();
				clone.setBare(false);
				clone.setCloneAllBranches(true);
				clone.setDirectory(buildDirectory).setURI(buildRepository);
				log.debug("Cloning " + buildRepository + " into " + buildDirectory.getAbsolutePath());
				git = clone.call();
				// delete the legacy files
				for (File file : buildDirectory.listFiles()) {
					if (!file.getName().equals(".git") && !file.getName().equals("pom.xml")) {
						if (file.isDirectory()) {
							FileUtils.deleteDirectory(file);
						} else {
							file.delete();
						}
					}
				}
				getClass().getClassLoader();
			}
			execute();
			executed = true;
		} catch (Exception exc) {
			throw exc;
		} finally {
			executed = true;
		}
	}

	/**
	 * Returns the resource prefix for this build, or null if none
	 * 
	 * @return The resource prefix for this build, or null if none
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public String getResourcePrefix() throws Exception {
		return Configuration.getInstance().getString("build.resourcePrefix." + projectName);
	}

	/**
	 * Returns this build's directory
	 * 
	 * @return This build's directory
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public File getBuildDirectory() throws Exception {
		if (buildDirectory == null) {
			buildDirectory = new File(Configuration.getInstance().getTemporaryDir() + File.separator + projectName);
		}
		return buildDirectory;
	}

	/**
	 * Returns this build's source directory
	 * 
	 * @return This build's source directory
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public File getSourceDirectory() throws Exception {
		if (sourceDirectory == null) {
			sourceDirectory = new File(Configuration.getInstance().getSourceDir());
		}
		return sourceDirectory;
	}

	/**
	 * Copies the libraries to this build's libs directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	protected void copyLibraries() throws Exception {
		File srcLibDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "libs");
		File destLibDir = new File(getBuildDirectory().getAbsolutePath() + File.separator + "libs");
		log.debug("Creating directory: " + destLibDir.getAbsolutePath());
		destLibDir.mkdirs();
		log.debug("Copying directory " + srcLibDir + " to " + destLibDir);
		FileUtils.copyDirectory(srcLibDir, destLibDir);
	}

	/**
	 * Invokes "android update project"
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
//	protected void androidUpdateProject() throws Exception {
//		File projectDirectory = getBuildDirectory();
//		String androidBinary = Configuration.getInstance().getAndroidSdkDir() + File.separator + "tools" + File.separator + "android";
//		// String updateProjectCommand = androidBinary + " update project -t " +
//		// Configuration.getInstance().getAndroidTargetSdk() + " -p . -n " +
//		// getProjectName();
//		String updateProjectCommand = androidBinary + " update project -p . -n " + getProjectName();
//		systemCall(updateProjectCommand, projectDirectory);
//	}

	/**
	 * Wrapper to {@link #systemCall(String, File, String[])} with no env-vars
	 * 
	 * @param command
	 *            The command
	 * @param directory
	 *            The directory to execute the command in
	 * @throws IOException
	 *             Errors are passed back to the main method
	 */
	protected void systemCall(String command, File directory) throws IOException {
		systemCall(command, directory, new String[] {});
	}

	/**
	 * Executes a system command.
	 * 
	 * @param command
	 *            The command
	 * @param directory
	 *            The directory to execute the command in
	 * @param envVars
	 *            The environment variables to pass
	 * @throws IOException
	 *             Errors are passed back to the main method
	 */
	protected void systemCall(String command, File directory, String[] envVars) throws IOException {
		log.debug("Executing " + command + " in " + directory.getAbsolutePath());
		Process p = Runtime.getRuntime().exec(command, envVars, directory);
		try {
			BufferedReader outputReader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String outputLine;
			log.debug("Command Output:");
			while ((outputLine = outputReader.readLine()) != null) {
				log.debug(outputLine);
			}
			if (p.waitFor() != 0) {
				BufferedReader errorReader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				String errorLine;
				log.error("Error in command:");
				while ((errorLine = errorReader.readLine()) != null) {
					log.error(errorLine);
				}
				throw new IOException("Ant build failed!");
			}
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

	/**
	 * Executes this build's logic: <br />
	 * This class must
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	protected abstract void execute() throws Exception;

	/**
	 * Will be run after everything was built (but before the builds are
	 * commited).
	 * By default, this method does nothing (you don't have to call super).
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	protected void doPostBuildActions() throws Exception {
	}

	/**
	 * Returns if the post-actions were executed
	 * 
	 * @return If the post-actions were executed
	 */
	public boolean werePostActionsExecuted() {
		return werePostActionsExecuted;
	}

	/**
	 * Runs the post-actions
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	public void runPostBuildActions() throws Exception {
		doPostBuildActions();
		if (git != null) {
			AddCommand addCommand = git.add();
			addCommand.addFilepattern(".gitignore");
			addCommand.addFilepattern("AndroidManifest.xml");
			addCommand.addFilepattern("src");
			addCommand.addFilepattern("res");
			addCommand.addFilepattern("libs");
			addCommand.call();
			CommitCommand commitCommand = git.commit();
			commitCommand.setCommitter("Insign Build Server", "core@insign.ch");
			commitCommand.setAuthor("Insign Build Server", "core@insign.ch");
			commitCommand.setMessage("Automatic build version " + Configuration.getInstance().getBuildVersion());
			commitCommand.setAll(true);
			RevCommit revCommit = commitCommand.call();
			if (revCommit == null) {
				log.debug("Nothing was committed");
			} else {
				log.debug("Committed revision: " + revCommit.getName());
				PushCommand pushCommand = git.push();
				for (PushResult pushResult : pushCommand.call()) {
					for (RemoteRefUpdate remoteUpdate : pushResult.getRemoteUpdates()) {
						log.debug("Pushed revision to remote: " + remoteUpdate.getNewObjectId().getName());
					}
				}
			}
		}

	}

}
