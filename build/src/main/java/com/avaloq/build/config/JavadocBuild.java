package com.avaloq.build.config;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A build that creates the Javadoc for this project.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class JavadocBuild extends Build {
	
	/**
	 * The logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(JavadocBuild.class);

	/**
	 * Creates a new Javadoc build task.
	 * @param projectName The build's name
	 * @param dependencies The build's dependencies 
	 */
	public JavadocBuild(String projectName, List<String> dependencies) {
		super(projectName, dependencies);
	}

	@Override
	protected void execute() throws Exception {
		File antDirectory = new File(getAntDirectory());
		String antFile = getAntFile();
		String antBinary = Configuration.getInstance().getAntBinary();
		String[] envp = new String[] {
			"ANDROID_HOME=" + Configuration.getInstance().getAndroidSdkDir(),
			"DEST_DIR=" + getBuildDirectory().getAbsolutePath()
		};
		for(String env : envp) {
			log.debug("env: " + env);
		}
		systemCall(antBinary + " -f " + antFile, antDirectory, envp);
	}
	
	/**
	 * Returns the ant build file
	 * @return The ant build file
	 * @throws Exception Errors are passed back to the main method
	 */
	private String getAntFile() throws Exception {
		return Configuration.getInstance().getString("build.javadoc.antFile." + getProjectName());
	}
	
	/**
	 * Returns the directory in which ant will be run
	 * @return The directory in which ant will be run
	 * @throws Exception Errors are passed back to the main method
	 */
	private String getAntDirectory() throws Exception {
		return getSourceDirectory() + File.separator + Configuration.getInstance().getString("build.javadoc.antDirectory." + getProjectName());
	}

}