package com.avaloq.build.config;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.avaloq.build.BuildTools;

/**
 * This class does builds of apk libraries. <br />
 * The action happens in {@link #execute()}, where all the private methods are
 * called. <br />
 * <b>Note:</b> The library projects are not actually being built, but the build
 * program puts them in the right place, copies the required files and generates
 * the build script (build.xml) for ant. They will be built by
 * {@link ApkBuild#antDebugBuild()} later when the apk is built.
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ApklibBuild extends Build {

	/**
	 * The logger for this class
	 */
	private static final Logger log = LoggerFactory.getLogger(ApklibBuild.class);

	/**
	 * The Java-packages for this build
	 */
	private final List<String> buildPackages;

	/**
	 * Creates a new ApklibBuild
	 * 
	 * @param projectName
	 *            The name of this android library project
	 * @param buildPackages
	 *            The Java-packages for this build
	 * @param dependencies
	 *            The dependencies of this build
	 */
	public ApklibBuild(String projectName, List<String> buildPackages, List<String> dependencies) {
		super(projectName, dependencies);
		this.buildPackages = buildPackages;
	}

	@Override
	public void execute() throws Exception {
		log.info("Building " + getProjectName());
		copyGitignore();
		copySource();
		// copyLibraries();
		copyExtras();
		createProjectProperties();
		createResources();
		createAndroidManifest(null);
		buildSecondaryLanguages();
		// androidUpdateProject();
	}

	/**
	 * Copies the .gitignore file to this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	protected void copyGitignore() throws Exception {
		// URL url =
		// ClassLoader.getSystemResource("src/main/resources/templates/apklib/gitignore");
		// File srcGitignore = new File(url.toURI());
		File srcGitignore = new File("target/classes/apklib-gitignore");
		File destGitignore = new File(getBuildDirectory().getAbsolutePath() + File.separator + ".gitignore");
		FileUtils.copyFile(srcGitignore, destGitignore);
	}

	/**
	 * Copies the source code for this library project to the build's src
	 * directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void copySource() throws Exception {
		File srcDir = new File(getBuildDirectory().getAbsolutePath() + File.separator + "src");
		log.debug("Creating directory: " + srcDir.getAbsolutePath());
		srcDir.mkdirs();
		for (String buildPackage : buildPackages) {
			log.debug("buildPackage = " + buildPackage);
			String packageDirectory = buildPackage.replaceAll("\\.", File.separator);
			log.debug("packageDirectory = " + packageDirectory);
			File sourcePackageDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "src" + File.separator + packageDirectory);
			File projectPackageDir = new File(srcDir.getAbsolutePath() + File.separator + packageDirectory);
			log.debug("Creating directory " + projectPackageDir.getAbsolutePath());
			projectPackageDir.mkdirs();
			log.debug("Copying directory " + sourcePackageDir.getAbsolutePath() + " to " + projectPackageDir.getAbsolutePath());
			FileUtils.copyDirectory(sourcePackageDir, projectPackageDir);
		}
	}

	/**
	 * Copies extras to the target directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void copyExtras() throws Exception {
		String[] extras = Configuration.getInstance().getStringArray("build.copyExtra." + getProjectName());
		for(String extra : extras) {
			File srcDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + extra);
			File destDir = new File(getBuildDirectory().getAbsolutePath() + File.separator + extra);
			if(!destDir.exists()) {
				log.debug("Creating directory " + destDir.getAbsolutePath());
				destDir.mkdirs();
			}
			log.debug("Copying directory " + srcDir.getAbsolutePath() + " to " + destDir.getAbsolutePath());
			FileUtils.copyDirectory(srcDir, destDir);
		}
	}

	/**
	 * Creates the project.properties in this build's directory from a template
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createProjectProperties() throws Exception {
		// final Template template =
		// Velocity.getTemplate("templates/apklib/project.properties.vm");
		final Template template = Velocity.getTemplate("target/classes/apklib-project.properties.vm");
		File destFile = new File(getBuildDirectory() + File.separator + "project.properties");
		destFile.createNewFile();
		final VelocityContext context = new VelocityContext();
		context.put("dollar", "$");
		context.put("androidTargetSdk", Configuration.getInstance().getAndroidTargetSdk());
		context.put("androidMinSdk", Configuration.getInstance().getAndroidMinSdk());
		context.put("dependencies", getDependencies());
		FileWriter fw = new FileWriter(destFile, false);
		template.merge(context, fw);
		fw.close();
	}

	/**
	 * Creates the AndroidManifest.xml in this build's directory from a template
	 * 
	 * @param specialPackageName If this name is specified, it will be used instead of the default package name ("com.avaloq.framework")
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createAndroidManifest(String specialPackageName) throws Exception {
		// final Template template =
		// Velocity.getTemplate("templates/apklib/AndroidManifest.xml.vm");
		final Template template = Velocity.getTemplate("target/classes/apklib-AndroidManifest.xml.vm");
		File destFile = new File(getBuildDirectory() + File.separator + "AndroidManifest.xml");
		destFile.createNewFile();
		final VelocityContext context = new VelocityContext();
		context.put("dollar", "$");
		if(specialPackageName == null) {
			context.put("apklibPackage", "com.avaloq.framework");
		} else {
			context.put("apklibPackage", specialPackageName);
		}
		context.put("androidTargetSdk", Configuration.getInstance().getAndroidTargetSdk());
		context.put("androidMinSdk", Configuration.getInstance().getAndroidMinSdk());
		context.put("permissions", getPermissions());
		context.put("activities", getActivities());
		FileWriter fw = new FileWriter(destFile, false);
		template.merge(context, fw);
		fw.close();
	}

	/**
	 * Copies all the resources matching the prefix from the avaloqkit project
	 * to this build's directory
	 * 
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private void createResources() throws Exception {
		File destResourceDirectory = new File(getBuildDirectory() + File.separator + "res");
		log.debug("Creating directory " + destResourceDirectory.getAbsolutePath());
		destResourceDirectory.mkdirs();
		File sourceResourceDir = new File(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "res");
		for (File sourceResourceSubdirectory : sourceResourceDir.listFiles()) {
			if (sourceResourceSubdirectory.isDirectory()) {
				for (File sourceResourceFile : sourceResourceSubdirectory.listFiles()) {
					if (sourceResourceFile.getName().startsWith(getResourcePrefix())) {
						File destResourceFile = new File(destResourceDirectory.getAbsolutePath() + File.separator + sourceResourceSubdirectory.getName() + File.separator + sourceResourceFile.getName());
						File destResourceSubdirectory = destResourceFile.getParentFile();
						if (!destResourceSubdirectory.exists()) {
							log.debug("Creating directory " + destResourceSubdirectory.getAbsolutePath());
							destResourceSubdirectory.mkdirs();
						}
						log.debug("Copying file " + sourceResourceFile.getAbsolutePath() + " to " + destResourceFile.getAbsolutePath());
						FileUtils.copyFile(sourceResourceFile, destResourceFile);
					}
				}
			}
		}
	}

	/**
	 * Returns the uses-permission tags of this build (currently all of the
	 * source's manifest)
	 * 
	 * @return All the uses-permission tags (xml format)
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private List<String> getPermissions() throws Exception {
		Document document = BuildTools.getDocumentBuilder().parse(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "AndroidManifest.xml");
		Element node = document.getDocumentElement();
		NodeList childNodes = node.getChildNodes();
		List<String> permissions = new ArrayList<String>();
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeName().equals("uses-permission")) {
				permissions.add(BuildTools.nodeToString(childNodes.item(i)));
			}
		}
		return permissions;
	}

	/**
	 * Returns the activities for this build (all that match this build's
	 * packages)
	 * 
	 * @return All the activities for this build
	 * @throws Exception
	 *             Errors are passed back to the main method
	 */
	private List<String> getActivities() throws Exception {
		Document document = BuildTools.getDocumentBuilder().parse(getSourceDirectory() + File.separator + "avaloqkit" + File.separator + "AndroidManifest.xml");
		Element node = document.getDocumentElement();
		NodeList childNodes = node.getChildNodes();
		List<String> activities = new ArrayList<String>();
		for (int i = 0; i < childNodes.getLength(); i++) {
			if (childNodes.item(i).getNodeName().equalsIgnoreCase("application")) {
				NodeList applicationChildNodes = childNodes.item(i).getChildNodes();
				for (int ii = 0; ii < applicationChildNodes.getLength(); ii++) {
					if (applicationChildNodes.item(ii).getNodeName().equalsIgnoreCase("activity")) {
						Node activityNode = applicationChildNodes.item(ii);
						Node nameAttribute = activityNode.getAttributes().getNamedItemNS("http://schemas.android.com/apk/res/android", "name");
						if (BuildTools.isActivitiyInBuildPackages(nameAttribute.getNodeValue(), buildPackages)) {
							String activity = BuildTools.nodeToString(activityNode);
							log.debug("Found activity: " + activity);
							activities.add(activity);
						}
					}
				}
			}
		}
		return activities;
	}
	
	@Override
	protected void doPostBuildActions() throws Exception{
		createAndroidManifest(Configuration.getInstance().getString("build.apklibPackage." + getProjectName()));
	}

}