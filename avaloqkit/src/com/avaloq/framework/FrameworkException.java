package com.avaloq.framework;

/**
 * This class is a subclass of Runtime Exception, indicating that a Error has happened in the Framework.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class FrameworkException extends RuntimeException {

	/**
	 * The generated UID of this class
	 */
	private static final long serialVersionUID = 6162524805111786685L;

	/**
	 * Creates a new FrameworkException
	 * @param string The Exception message
	 */
	public FrameworkException(String detailMessage) {
		super(detailMessage);
	}

}
