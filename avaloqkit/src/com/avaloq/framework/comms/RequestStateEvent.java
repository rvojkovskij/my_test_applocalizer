package com.avaloq.framework.comms;

import android.util.Log;

import com.avaloq.framework.AvaloqApplication;

public abstract class RequestStateEvent<RequestType extends AbstractServerRequest<?>> {

	private static final String TAG = "RequestStateEvent";
	
	/**
	 * Set a new request state event. 
	 * Note: The code is executed on the UI thread!
	 * 	
	 * @param aRequest
	 * @param newRequestState
	 */
	public void setRequestState(final AbstractServerRequest<?> aRequest, final RequestState newRequestState) {

		Runnable r = new Runnable() {
			
			public void run() {
			
				@SuppressWarnings("unchecked")
				RequestType request = (RequestType) aRequest;
				
				Log.v(TAG, "Request " + request.getRequestIdentifier() + ": State set to " + newRequestState);
				Log.d(TAG, QueueManager.getInstance().getThreadState());
				
				switch (newRequestState) {
				
				case INITIALIZING:			
					onRequestInitializing(request);
					onRequestStateChanged(RequestState.INITIALIZING, request);			
					break;

				case QUEUED:			
					onRequestQueued(request);
					onRequestStateChanged(RequestState.QUEUED, request);			
					break;

				case EXECUTING:			
					onRequestExecuting(request);
					onRequestStateChanged(RequestState.EXECUTING, request);			
					break;

				case PENDING_AUTHENTICATION:			
					onRequestPendingAuthentication(request);
					onRequestStateChanged(RequestState.INITIALIZING, request);			
					break;

				case COMPLETED:			
					onRequestCompleted(request);
					onRequestStateChanged(RequestState.COMPLETED, request);			
					break;

				case FAILED:			
					onRequestFailed(request);
					onRequestStateChanged(RequestState.FAILED, request);			
					break;

				default:
					Log.e(TAG, "No events for state defined: " + newRequestState);
				}
				
			};
		};
				
		AvaloqApplication.getInstance().runOnUiThread(r);
		
	}


	public void onRequestInitializing(RequestType aRequest) {}

	public void onRequestQueued(RequestType aRequest) {}

	public void onRequestExecuting(RequestType aRequest) {}

	public void onRequestPendingAuthentication(RequestType aRequest) {}

	public void onRequestCompleted(RequestType aRequest) {}

	public void onRequestFailed(RequestType aRequest) {}
					

	/**
	 * Called on every state change (after the specialized handler).
	 * Note: The code is executed on the UI thread!
	 * 
	 * @param requestState
	 * @param aRequest
	 */
	public void onRequestStateChanged(RequestState requestState, RequestType aRequest) {}
}
