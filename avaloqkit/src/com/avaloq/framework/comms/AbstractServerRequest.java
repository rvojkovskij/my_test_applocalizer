package com.avaloq.framework.comms;

import java.util.Observable;
import java.util.Observer;

import android.util.Log;

/**
 * An AbstractServerRequest encapsulates all data necessary to perform a single server
 * service request. It additionally manages state for the request execution.
 */
public abstract class AbstractServerRequest<ResponseType> {

	private static final String TAG = "ServerRequest";

	private Observable mObservers = new Observable() {
		public boolean hasChanged() {
			return true;
		};
	};	
	private AbstractServerResponse<ResponseType> mResponse = null;

	private static int nextRequestCounter = 1;
	
	public enum CachePolicy {
		/** Complete skips the cache and always gets fresh data. Data is not added to cache **/
		NO_CACHE,
		
		/** Use normal caching **/
		USE_CACHE,
		
		/** Execute an explicit server request and refresh the cache **/
		REFRESH_CACHE
	}
	
//	/** Complete skips the cache and always gets fresh data. Data is not added to cache **/
//	protected static final int CACHE_POLICY_NO_CACHE = 0;	
//	
//	/** Use normal caching **/
//	protected static final int CACHE_POLICY_USE_CACHE = 1;	
//	
//	/** Execute an explicit server request and refresh the cache **/
//	protected static final int CACHE_POLICY_REFRESH_CACHE = 2;
		
	private RequestState mRequestState = RequestState.INITIALIZING;
	private CachePolicy mCachePolicy = CachePolicy.USE_CACHE;	
	/** Is a cached response being used? **/
	private boolean mIsCachedResponse = false;

	
	private int mRequestCounter = -1;
	private int mRequestProtocolVersion = -1;
	private int mRequestingProxyId = -1;

	private boolean mPartOfAuthExchange = false;

	private RequestStateEvent<?> mRequestStateEvent;
	
	private String mRequestIdentifier;


	// *****************************************************
	// *** Constructors
	// *****************************************************

	public AbstractServerRequest(RequestStateEvent<?> rse) {
		super();
		this.mRequestStateEvent = rse;
		
		mRequestCounter = nextRequestCounter++;
		mRequestIdentifier = mRequestCounter + ": " + this.getClass().getSimpleName();
	}


	// *****************************************************
	// *** Observer handling
	// *****************************************************

	public void addObserver(Observer o) {
		mObservers.addObserver(o);		
	}

	public void deleteObserver(Observer o) {		
		mObservers.deleteObserver(o);
	} 

	/**
	 * Notify observers that the request state has changed
	 */
	protected void notifyObservers() {
		mObservers.notifyObservers();
	}


	// *****************************************************
	// *** Request state check methods
	// *****************************************************

	/**
	 * Return a boolean indicating whether the receiver is currently in the
	 * initial state. This is only true while the receiver is being initialized.
	 */
	public boolean isRequestStateInitializing() {
		return mRequestState == RequestState.INITIALIZING;
	}

	/**
	 * Return a boolean indicating whether the receiver is currently queued for
	 * execution. This is true after the receiver has been initialize, but
	 * before it has started executing.
	 */
	public boolean isRequestStateQueued() {
		return mRequestState == RequestState.QUEUED;
	}

	/**
	 * Return a boolean indicating whether the receiver is currently executing.
	 * This is true while server communications are in progress.
	 */
	public boolean isRequestStateExecuting() {
		return mRequestState == RequestState.EXECUTING;
	}

	/**
	 * Return a boolean indicating whether the receiver is currently pending
	 * authorization. This is true while a server request has been parked to
	 * allow additional steps to be taken to validate the user authorization.
	 */
	public boolean isRequestStatePendingAuthentication() {
		return mRequestState == RequestState.PENDING_AUTHENTICATION;
	}

	/**
	 * Return a boolean indicating whether the receiver execution has completed.
	 * This is true once a response has been received that does not require an
	 * authentication challenge.
	 */
	public boolean isRequestStateCompleted() {
		return mRequestState == RequestState.COMPLETED;
	}

	/**
	 * Return a boolean indicating whether the receiver was cancelled before it
	 * finished executing.
	 */
	public boolean isRequestStateFailed() {
		return mRequestState == RequestState.FAILED;
	}


	// *****************************************************
	// *** Request state update methods
	// *****************************************************

	/**
	 * Change the receiver state to QUEUED. This transition can only be made
	 * from the INITIALIZING or PENDING_AUTHENTICATION state.
	 * 
	 * @return A boolean indicating whether the state change was successful
	 */
	protected boolean setRequestStateQueued() {
		// Use synchronization to prevent concurrency issues.
		boolean success = false;

		synchronized (this) {
			if (mRequestState == RequestState.INITIALIZING || mRequestState == RequestState.PENDING_AUTHENTICATION) {				
				mRequestState = RequestState.QUEUED;
				success = true;
			} else {
				Log.e(TAG, "setRequestStateQueued: cannot move request " + mRequestCounter + " from " + mRequestState + " to QUEUED");
			}
		}

		if (success) {			
			if(mRequestStateEvent != null) {
				this.mRequestStateEvent.setRequestState(this, RequestState.QUEUED);
			}
		}

		return success;
	}

	/**
	 * Change the receiver state to EXECUTING. This transition can only be made
	 * from either the INITIALIZING (for direct executions), QUEUED or PENDING AUTHENTICATION states.
	 * 
	 * @return A boolean indicating whether the state change was successful
	 */
	protected boolean setRequestStateExecuting() {
		// Use synchronization to prevent concurrency issues.
		boolean success = false;

		synchronized (this) {
			if (mRequestState == RequestState.INITIALIZING || mRequestState == RequestState.QUEUED || mRequestState == RequestState.PENDING_AUTHENTICATION) {
				mRequestState = RequestState.EXECUTING;
				success = true;
			} else {
				Log.e(TAG, "setRequestStateExecuting: cannot move request " + mRequestCounter + " from " + mRequestState + " to EXECUTING");
			}
		}

		if (success) {
			if(mRequestStateEvent != null) {
				this.mRequestStateEvent.setRequestState(this, RequestState.EXECUTING);
			}
		}

		return success;
	}

	/**
	 * Change the receiver state to PENDING AUTHENTICATION. This transition can
	 * only be made from the EXECUTING state.
	 * 
	 * @return A boolean indicating whether the state change was successful
	 */
	public boolean setRequestStatePendingAuthentication() {
		// Use synchronization to prevent concurrency issues.
		boolean success = false;

		synchronized (this) {
			if (mRequestState == RequestState.EXECUTING) {
				mRequestState = RequestState.PENDING_AUTHENTICATION;
				success = true;
			} else {
				Log.e(TAG, "setRequestStatePendingAuthentication: cannot move request " + mRequestCounter + " from " + mRequestState + " to PENDING AUTHENTICATION");
			}
		}

		if (success) {
			if(mRequestStateEvent != null) {
				this.mRequestStateEvent.setRequestState(this, RequestState.PENDING_AUTHENTICATION);
			}
		}
		return success;
	}

	/**
	 * Change the receiver state to COMPLETED. This transition can only be made
	 * from either the EXECUTING or PENDING AUTHENTICATION states.
	 * 
	 * @return A boolean indicating whether the state change was successful
	 */
	public boolean setRequestStateCompleted() {
		// Use synchronization to prevent concurrency issues.
		boolean success = false;

		synchronized (this) {
			if (mRequestState == RequestState.EXECUTING || mRequestState == RequestState.PENDING_AUTHENTICATION) {				
				mRequestState = RequestState.COMPLETED;
				success = true;
			} else {
				Log.e(TAG, "setRequestStateCompleted: cannot move request " + mRequestCounter + " from " + mRequestState + " to COMPLETED");
			}
		}

		if (success) {
			if(mRequestStateEvent != null) {
				this.mRequestStateEvent.setRequestState(this, RequestState.COMPLETED);
			}
		}
		return success;
	}

	/**
	 * Change the receiver state to FAILED. This transition can only be made
	 * from either the EXECUTING or PENDING AUTHENTICATION states.
	 * 
	 * @return A boolean indicating whether the state change was successful
	 */
	public boolean setRequestStateFailed() {
		// Use synchronization to prevent concurrency issues.
		boolean success = false;

		synchronized (this) {
			if (mRequestState == RequestState.QUEUED || mRequestState == RequestState.EXECUTING || mRequestState == RequestState.PENDING_AUTHENTICATION) {				
				mRequestState = RequestState.FAILED;
				success = true;
			} else {
				Log.e(TAG, "setRequestStateFailed: cannot move request " + mRequestCounter + " from " + mRequestState + " to FAILED");
			}
		}

		if (success) {
			if(mRequestStateEvent != null) {
				this.mRequestStateEvent.setRequestState(this, RequestState.FAILED);
			}
		}
		return success;
	}


	// *****************************************************
	// *** Public methods
	// *****************************************************

	/**
	 * Get the server response object this request has created
	 * @return the response
	 */
	public AbstractServerResponse<ResponseType> getResponse() {
		return mResponse;
	}

	/**
	 * Sets the Response for this request (should be called by the implemented {@link AbstractServerRequest#executeRequest()} method)
	 * @param response The Response object.
	 */
	protected void setResponse(AbstractServerResponse<ResponseType> aResponse) {
		this.mResponse = aResponse;
	}

	/**
	 * Return an identifier of the request of the form 'requestno: classname'
	 */
	public String getRequestIdentifier() {		
		return mRequestIdentifier;
	}
	
	/**
	 * Manually set a request identifier, in case you need more debug info.
	 * It is recommended to append to getRequestIdentifier().
	 * 
	 * @param requestIdentifier
	 */
	public void setRequestIdentifier(String requestIdentifier) {
		mRequestIdentifier = requestIdentifier;
	}

	/**
	 * Return the id of the initiating proxy
	 */
	public int getRequestingProxyId() {
		return mRequestingProxyId;
	}


	/**
	 * Return the protocol version of the request
	 */
	public int getProtocolVersion() {
		return mRequestProtocolVersion;
	}


	/**
	 * Return true if the request has been explicitly marked as being part of an
	 * authentication exchange
	 */
	public boolean isPartOfAuthenticationExchange() {		
		return mPartOfAuthExchange;
	}

	/**
	 * Explicitly mark the request as being part of an authentication exchange
	 */
	public void setPartOfAuthenticationExchange() {
		mPartOfAuthExchange = true;
	}

	/**
	 * Adds the request to the request queue.
	 * 	
	 */
	public void initiateServerRequest() {
		QueueManager.getInstance().enqueueServerRequest(this);	
	}
	
	/**
	 * Get this request's current caching policy.
	 * Default is CACHE_POLICY_USE_CACHE
	 * 
	 * @return
	 */
	public CachePolicy getCachePolicy() {
		return mCachePolicy;
	}
	
	/**
	 * Set this request's currnt caching policy.
	 * 
	 * @param cachePolicy
	 */
	public void setCachePolicy(CachePolicy cachePolicy) {
		mCachePolicy =  cachePolicy;
	}
	
	/**
	 * Load a previously cached response if it is available and the cache policy allows it.
	 * @return true if a cache entry as successfully loaded, false otherwise
	 */
	public boolean useCachedResponse() {
		
		if (getCachePolicy() != CachePolicy.USE_CACHE) {
			return false;
		}
		
		String cacheKey = getCacheKey();
		
		@SuppressWarnings("unchecked")
		AbstractServerResponse<ResponseType> cachedResponse = (AbstractServerResponse<ResponseType>) RequestCache.getInstance().get(cacheKey);		
		
		if (cachedResponse != null) {
			mResponse = cachedResponse;
			mIsCachedResponse = true;
			Log.v(TAG, "Using a cached response for request " + getRequestIdentifier());				
			return true;
		} 
		
		Log.v(TAG, "No cached response found for request " + getRequestIdentifier());
		mIsCachedResponse = false;
		return false;
	}
	
	/**
	 * Add the response obj to the cache if the cache policy allows it.
	 */
	public void addResponseToCache() {
		if (getCachePolicy() != CachePolicy.NO_CACHE) {
			
			synchronized(RequestCache.getInstance()) {
				RequestCache.getInstance().put(getCacheKey(), getResponse());
			}
			
			Log.v(TAG, "Caching response to request " + getRequestIdentifier());
		}		
	}
	
	public boolean isCachedResponse() {
		return mIsCachedResponse;
	}
	
	public String toString() {
		return getClass().getSimpleName() + ": " + getRequestIdentifier() + " ( " + mRequestState + " )";
	}

	// *****************************************************
	// *** Methods to be reimplemented
	// *****************************************************

	/**
	 * Create this request's response object of the proper type 
	 */
	public abstract AbstractServerResponse<ResponseType> createEmptyResponseObject();

	/**
	 * Immediately execute the request and loads the data over the network.
	 * Create a response (createEmptyResponseObject()) and set it to the request (setResponse(aResponse)) 
	 * 
	 * Use this if you need synchronous execution, bypassing the request queues.
	 * For queued async calls, use initiateServerRequest()
	 * 
	 * Set the request state to executing once execution starts.
	 */
	public abstract void executeRequest();

//	/**
//	 * Returns the Type of this WebService
//	 * @return The Type of this WebService
//	 * 
//	 * FIXME: Remove (also from code generator), as this is not needed,
//	 * since the superclass already defines the type (e.g. in AbstractJsonHTTPRequest)
//	 * 
//	 */
//	@Deprecated
//	public abstract String getWebserviceType();

	/**
	 * Get a hash key used for caching this request's response.
	 * Return 'null' to indicate no caching.
	 * 
	 * @return per-cache-entry unique hash key 
	 */
	public abstract String getCacheKey();



}
