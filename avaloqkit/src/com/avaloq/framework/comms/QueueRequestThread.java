package com.avaloq.framework.comms;

import android.os.SystemClock;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.framework.FrameworkService;
import com.avaloq.framework.comms.webservice.framework.Request;
import com.avaloq.framework.ui.DialogActivity;


class QueueRequestThread extends Thread {

	private final static String TAG = QueueRequestThread.class.getSimpleName();

	private long mLastHeartbeatSent;
	private static final int HEARTBEAT_MIN_PERIOD = 10;

	public QueueRequestThread() {
		setName(TAG);
	}

	@Override
	public void run() {

		QueueManager queueManager = QueueManager.getInstance();

		while (!queueManager.shutdown()) {

			AbstractServerRequest<?> request = queueManager.getNextServerRequest();
			if (request != null) {
				Log.d(TAG, "sleeping");

				// Execute the request only if no cached response is available.
				if (!request.useCachedResponse()) {
					
					// Do live request
					
					// Ensure network data connectivity. If not present, 
					// BLOCK the queue thread and show the no connection dialog.
					waitForDataConnectivity();
					
					// Execute the request now
					queueManager.removeServerRequest();
					request.executeRequest();						



				} else {								
					// Use cached response
					
					queueManager.removeServerRequest();
					
					// For a proper state transition we set the request to executing even if it's technically not.
					request.setRequestStateExecuting();

					// Send server session heartbeat when using cached responses 
					sendServerSessionHeartbeat();

				}

				// Add the request with response on the response queue (if we have a uncancelled response)	
				if (request.getResponse() != null && !request.isRequestStateFailed()) {					
					queueManager.enqueueServerResponse(request);

					// Mark the timestamp as last request (for session handling)
					queueManager.markTimeOfLastExecutedRequest();
				}
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {			
				e.printStackTrace();
			}
		}			
	}
	
	/**
	 * Checks if network and data connectivity is available.
	 * Shows the network dialog if not. Removes the dialog immediately if connection becomes available.
	 * 
	 * This is a BLOCKING method.
	 */
	private void waitForDataConnectivity() {
		boolean first = true;
		int i=0;
		while (!AvaloqApplication.getInstance().isNetworkConnected()) {
			
			// Bail out if an app exit is signalled from outside
			if (QueueManager.getInstance().shutdown()) {
				DialogActivity.hide();
				return;
			}
			
			if (!DialogActivity.isShowing()) {
				DialogActivity.showNetworkError(true);
			}
			
			if (first) {
				Log.i(TAG, "Internet connection not available, blocking request queue");
				first = false;
			}
			if (++i==10) {
				Log.i(TAG, "Internet connection still not available, request queue still blocked.");
				i=0;
			}			
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {			
				e.printStackTrace();
			}
		}
		
		if (!first) {
			DialogActivity.hide();
		}

	}

	/**
	 * Ping the server to ensure it keeps the session alive. This is used when we're dealing with cached, local ws responses.
	 * 
	 * Note: This doesn't perfectly fit here. Ideally, it would be a feature of an auth handler, and we would here have an
	 * observable / pubsub pattern which lets interested parties like auth handlers monitor ongoing requests.
	 * Refactor if needed.
	 * 
	 */
	private void sendServerSessionHeartbeat() {		

		// Prevent server from becoming a heart attack
		if (mLastHeartbeatSent + (HEARTBEAT_MIN_PERIOD * 1000) > SystemClock.elapsedRealtime()) {
			return;
		}
		
		// Don't try to send heartbeats if no network conn. is available
		// (so the failed conn. dialog is not triggered)
		
		if (AvaloqApplication.getInstance().isNetworkConnected()) {
			return;
		}

		Request heart = FrameworkService.clientSessionHeartbeat(new RequestStateEvent<Request>() {
			@Override
			public void onRequestCompleted(Request aRequest) {
				Log.i(TAG, "Session heartbeat sent to server");
				super.onRequestCompleted(aRequest);
			}

			@Override
			public void onRequestFailed(Request aRequest) {
				Log.w(TAG, "Could not send session heartbeat to server: Request failed");
				super.onRequestFailed(aRequest);
			}
		});		

		heart.setCachePolicy(CachePolicy.NO_CACHE);		
		heart.initiateServerRequest();

		mLastHeartbeatSent = SystemClock.elapsedRealtime();
	}

}
