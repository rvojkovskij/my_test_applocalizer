package com.avaloq.framework.comms.authentication.medusa;

import android.os.AsyncTask;
import android.os.Handler;

import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.ui.AbstractPasswordChangeActivity;

public class MedusaAuthenticationPasswordChangeActivity extends AbstractPasswordChangeActivity {

	protected static MedusaAuthenticationHandler mAuthHandler;
	protected static Handler mAuthThreadHandler;
	
	/**
	 * Launch the user input activity and attach the auth handler and a thread handler to post to
	 * @param authHandler
	 */
	protected static void requestInput(MedusaAuthenticationHandler authHandler, String text, String errorMsg) {
		mAuthHandler = authHandler;		
		requestInput(MedusaAuthenticationPasswordChangeActivity.class, text, errorMsg);		
	}
	

	/**
	 * Send the l/p to the server. 
	 * 
	 * Note: This is called from the UI thread and the request is sent in a an async task
	 * The queue thread meanwhile is still running and waiting for queue release.
	 */
	@Override	
	protected void onCompleteInput(String oldPassword, String newPassword, String newPasswordConfirm) {
		
		final MedusaAuthenticationAbstractRequest securityRequest = new MedusaAuthenticationPasswordChangeRequest(
				new RequestStateEvent<MedusaAuthenticationPasswordChangeRequest>() {},
				oldPassword, newPassword, newPasswordConfirm);
				
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				mAuthHandler.executeAuthRequest(securityRequest);
				return null;
			}
		}.execute();

				
	}

	@Override
	protected void onAbortInput() {		
		// mAuthHandler.authenticationCancelledByUser();
	}

}
