package com.avaloq.framework.comms.authentication.medusa;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * Executes an Medusa Matrix authentication request
 * 
 *  @author bachi
 */
public class MedusaAuthenticationMatrixRequest extends MedusaAuthenticationAbstractRequest{

	public MedusaAuthenticationMatrixRequest(RequestStateEvent<MedusaAuthenticationMatrixRequest> rse, 
			String code) {

		super(rse);		
		setRequestBody("response=" + code);							
	}			
}
