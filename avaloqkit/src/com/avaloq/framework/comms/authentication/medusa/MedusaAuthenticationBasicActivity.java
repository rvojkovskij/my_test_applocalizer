package com.avaloq.framework.comms.authentication.medusa;

import android.os.AsyncTask;
import android.os.Handler;

import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.ui.AbstractUsernamePasswordInputActivity;

public class MedusaAuthenticationBasicActivity extends AbstractUsernamePasswordInputActivity {

	protected static MedusaAuthenticationHandler mAuthHandler;
	protected static Handler mAuthThreadHandler;
	
	/**
	 * Launch the user input activity and attach the auth handler and a thread handler to post to
	 * @param authHandler
	 */
	protected static void requestInput(MedusaAuthenticationHandler authHandler, String text, String errorMsg) {
		mAuthHandler = authHandler;		
		requestInput(MedusaAuthenticationBasicActivity.class, text, errorMsg);		
	}
	

	/**
	 * Send the l/p to the server. 
	 * 
	 * Note: This is called from the UI thread and the request is sent in an async task.
	 * The queue thread meanwhile is still running and waiting for queue release.
	 */
	@Override	
	protected void onCompleteInput(String username, String password) {
		
		final MedusaAuthenticationAbstractRequest securityRequest = new MedusaAuthenticationBasicRequest(
				new RequestStateEvent<MedusaAuthenticationBasicRequest>() {},
				username, password);
				
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				mAuthHandler.executeAuthRequest(securityRequest);
				return null;
			}
		}.execute();

				
	}

	@Override
	protected void onAbortInput() {		
		//mAuthHandler.authenticationCancelledByUser();
	}

}
