package com.avaloq.framework.comms.authentication.medusa;

import java.util.HashMap;
import java.util.Map;

import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.HttpConstants;

/**
 * Base class for all Medusa authentication request types
 * 
 * TODO: Consider using char[] instead of String for sensitive data. However, this is nowadays
 * not regarded as improving memory security in any way, especially since the user input comes in 
 * as Strings from the UI side. 
 * 
 *  @author bachi
 */
public abstract class MedusaAuthenticationAbstractRequest extends AbstractHTTPServerRequest<MedusaAuthenticationResponse>{
	
	private String mRequestBody;
	
	public MedusaAuthenticationAbstractRequest(RequestStateEvent<? extends MedusaAuthenticationAbstractRequest> rse) {
		super(rse);
			
		setCachePolicy(CachePolicy.NO_CACHE);
		setPartOfAuthenticationExchange();
		setFollowRedirects(false);
	}
	
	/**
	 * Set the http body of the POST request to Medusa
	 * @param requestBody
	 */
	public void setRequestBody(String requestBody) {
		mRequestBody = requestBody;
	}
	
	/**
	 * The request body contains sensitive data, thus we try to make sure implementations
	 * will store it in one controllable member only.
	 */
	@Override
	final public String getRequestBody() {		
		if (mRequestBody == null) {
			throw new IllegalStateException("Medusa request body read before it was set.");
		}
		return mRequestBody;		
	}
	

	@Override
	public MedusaAuthenticationResponse createEmptyResponseObject() {		
		return new MedusaAuthenticationResponse();
	}


	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_POST;
	}

	@Override
	public String getRequestURL() {
		return MedusaAuthenticationHandler.mMedusaURL.toString();
	}

	@Override
	public Map<String, String> getHttpHeaders() {		
		Map<String, String> headers = new HashMap<String, String>();

		headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("User-Agent", "Android");
        
        return headers;
	}
	
}
