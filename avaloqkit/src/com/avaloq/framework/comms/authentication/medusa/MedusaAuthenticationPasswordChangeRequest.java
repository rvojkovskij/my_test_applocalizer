package com.avaloq.framework.comms.authentication.medusa;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * Executes an Medusa password change request
 * 
 *  @author bachi
 */
public class MedusaAuthenticationPasswordChangeRequest extends MedusaAuthenticationAbstractRequest{

	public MedusaAuthenticationPasswordChangeRequest(RequestStateEvent<MedusaAuthenticationPasswordChangeRequest> rse, 
			String oldPassword, String newPassword, String newPasswordConfirm) {
		
		super(rse);		
		setRequestBody("LOCATION=nowhere&" + 
				"&OLDPASSWORD=" + oldPassword +
				"&NEWPASSWORD=" + newPassword +
				"&NEWPASSWORDCONFIRMATION=" + newPasswordConfirm);						
	}

}
