package com.avaloq.framework.comms.authentication.medusa;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;

import android.content.Intent;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.authentication.AbstractAuthenticationHandler;
import com.avaloq.framework.comms.authentication.medusa.MedusaAuthenticationResponse.MedusaAuthStatus;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;
import com.avaloq.framework.ui.LogoutActivity;

/**
 * The Medusa authentication handler, implementing Basic/Mtan/Matrix auth and pw change.
 * @author bachi
 *
 */
public class MedusaAuthenticationHandler extends AbstractAuthenticationHandler {

	private static final String TAG = MedusaAuthenticationHandler.class.getSimpleName();	
	private static final String REALM_MEDUSA = "Medusa";
	
	/* Used with the Fake Medusa server - set a real host to use */
	static final String FAKE_URL = "http://not-used-now:80/auth-medusa-fake/";
	
	/** The medusa url is dynamically retrieved from the initial 303 response **/
	static URL mMedusaURL;


	/**
	 * The type of authentication required:
	 * Basic, Mtan, Matrix, PwChange
	 */
	private enum AuthType {
		Basic,
		Mtan,
		Matrix,
	}

	@Override
	public boolean isAuthenticationResponse(AbstractServerRequest<?> aRequest) {

		// Only care about http 
		if (!(aRequest instanceof AbstractHTTPServerRequest)) return false;

		// Get the http response
		AbstractHTTPServerResponse<?>response = (AbstractHTTPServerResponse<?>) aRequest.getResponse();

		// 1. Check if this is an initial Airlock Medusa redirect.
		String location = response.getHTTPResponseHeader("Location");
		if (response.getHTTPResponseCode() == 303) {			
			return true;
		}

		// 2. Check if this is a Medusa auth request during an already initiated exchange
		String realm = response.getAuthenticationRealm();
		if (response.getHTTPResponseCode() == 401 && realm.equals(REALM_MEDUSA)) {
			return true;
		} else {
			return false;
		}

	}

	@Override
	public void processAuthenticationResponse(AbstractServerRequest<?> aRequest) {		

		// Only care about http 
		if (!(aRequest instanceof AbstractHTTPServerRequest)) return;

		// Is this an initial redirect request? Then process it and finish. 
		if (processInitialRedirect((AbstractHTTPServerRequest<?>) aRequest)) return;

		// From here, we can assume we're in the Medusa security exchange flow
		if (!(aRequest instanceof MedusaAuthenticationAbstractRequest)) {
			throw new IllegalStateException("Cannot process Medusa autentication response: Request is not a MedusaAuthentication request");
		}

		// Get the http response		
		MedusaAuthenticationResponse response = (MedusaAuthenticationResponse) aRequest.getResponse();

		// Get the requested authentication method (WWW-Authenticate)
		String method = response.getAuthenticationType();

		// Determine the auth type (Basic, Mtan, Pwchange etc)
		AuthType authType;
		if (method != null) {

			// Assuming a known auth type.
			try {
				authType = AuthType.valueOf(method);
			} catch (Exception e) {
				throw new IllegalArgumentException("No valid Medusa auth type: " + method);
			}

		} else {

			// Check if we're inside a password change flow (sends no WWW-Authenticate)
			if (response.isPasswordChangeResponse()) {
				authType = AuthType.Basic; // pw change is actually a status of type Basic

			} else {
				
				// If a login fails, the current Medusa server seems to be in an undefined state
				// and it doesn't ask for a new auth type anymore (and won't respond to the previous properly)
				// Thus, we at the moment just trigger the logout to restart login
				
				// TODO: Switch to proper behaviour once this is solved in the Medusa interface.
				
				// Hint: Since currently the 401 response from Medusa is malformed (contains now WWW-Authenticate field), 
				// Java complains and throws an IOException in AbstractHTTPServerRequest.java.
				// So this particular case was handled there as it currently never gets here.
				
				authenticationCancelledByUser();		
				
				Intent intent = new Intent(AvaloqApplication.getContext(), LogoutActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
				AvaloqApplication.getContext().startActivity(intent);
				return;
				
				// throw new IllegalArgumentException("No Medusa auth type found in response header");
			}									
		}

		// Determine the status of the current auth type (Proceed, Completed etc)
		MedusaAuthStatus authStatus = response.getMedusaAuthStatus();


		Log.i(TAG,"Medusa requests " + authType + " authentication with status: " + authStatus);

		// Start the auth UI for the corresponding auth type.
		// The request/response processing is done in the respective activities.
		String errorMsg = null;
		
		switch (authType) {		
		case Basic:

			// Password change required?
			if (authStatus == MedusaAuthStatus.PasswordChangeRequired) {
				MedusaAuthenticationPasswordChangeActivity.requestInput(this, null, null);				
				
			} else if (authStatus == MedusaAuthStatus.PasswordChangeFailed) {
				// Previous pw change failed - try again
				errorMsg = response.getMedusaStatusText();
				MedusaAuthenticationPasswordChangeActivity.requestInput(this, null, errorMsg);
				
			} else {				
				// Show normal login screen				
				if (authStatus == MedusaAuthStatus.Failed) {
					errorMsg = AvaloqApplication.getInstance().getString(R.string.avq_login_invalid_text); 
				}
				MedusaAuthenticationBasicActivity.requestInput(this, null, errorMsg);
			}

			break;

		case Mtan:
			if (authStatus == MedusaAuthStatus.Failed) {
				errorMsg = AvaloqApplication.getInstance().getString(R.string.avq_login_invalid_text); 
			}
			MedusaAuthenticationMtanActivity.requestInput(this, null, errorMsg);			
			break;

		case Matrix:
			String challenge = response.getMedusaMatrixChallenge();
			if (challenge == null || challenge.equals("")) {
				throw new IllegalStateException("Medusa matrix auth requested but no X-Auth-Challenge header sent.");
			}
						
			if (authStatus == MedusaAuthStatus.Failed) {
				errorMsg = AvaloqApplication.getInstance().getString(R.string.avq_login_invalid_text); 
			}
			MedusaAuthenticationMatrixActivity.requestInput(this, challenge, null, errorMsg);
			break;			

		default:
			throw new IllegalStateException("Unimplemented Medusa method: " + authType);			
		}

	}

	/**
	 * Process the initial redirect: Create a new redirect request
	 * @param response
	 */
	private boolean processInitialRedirect(AbstractHTTPServerRequest<?> request) {
		AbstractHTTPServerResponse<?> response = (AbstractHTTPServerResponse<?>) request.getResponse();	
		if (response.getHTTPResponseCode() == 303) {			
			final MedusaAuthenticationAbstractRequest securityRequest = new MedusaAuthenticationInitialRequest(
					new RequestStateEvent<MedusaAuthenticationInitialRequest>() {},
					request);
			
			executeAuthRequest(securityRequest);			
			return true;

		} else {
			return false;
		}		

	}

	protected void executeAuthRequest(MedusaAuthenticationAbstractRequest securityRequest) {

		securityRequest.executeRequest();
		MedusaAuthenticationResponse securityResponse = (MedusaAuthenticationResponse) securityRequest.getResponse();
		if (securityResponse != null) {

			// Since we bypass the queue, we need to manually timestamp this request
			QueueManager.getInstance().markTimeOfLastExecutedRequest();

			// Handle the returned data
			securityResponse.onParseServerResponseData();

			if (!securityResponse.wasRequestSuccessfullyExecuted()) {}

			switch (securityResponse.getMedusaAuthStatus()) {
			case Completed:
				testIsLoggedin = true; // TODO: Remove after testing
				setAuthenticationCompleted(true);
				break;

				// For all states != Success, the response requires another auth step (or the same if it failed)				
			case Proceed:				
				/*case CredentialsMissing:*/
			case Locked:
			case Failed:
			case PasswordChangeRequired:
			case PasswordChangeFailed:
				processAuthenticationResponse(securityRequest);
				break;

			default:
				throw new IllegalStateException("Status '" + securityResponse.getMedusaAuthStatus() + "' not implemented!");
			}

			securityRequest.setRequestStateCompleted();

		} else {
			// Technical problem with the request
			securityRequest.setRequestStateFailed();
		}

	}


	@Override
	public void onLogout() {
		testIsLoggedin = false; // TODO Replace after testing
	}



	/** TESTING CODE **/
	static boolean testIsLoggedin = false;
	static int testWrongLoginCounter = 0;
	private static Boolean fakeServerIsAvailable = null;

	/**
	 * TEST REDIRECT URL
	 * @param urlString
	 * @return
	 */
	public static String testRedirectModifyURL(String urlString, AbstractHTTPServerRequest<?> request) {
		synchronized (request) {
			
			// Check if the FakeMedusa Server is available
			if (fakeServerIsAvailable ==  null) {
				fakeServerIsAvailable = false;
				int slashslash = FAKE_URL.indexOf("//") + 2;										
				String host = FAKE_URL.substring(slashslash, FAKE_URL.indexOf(':', slashslash));
				try {
					fakeServerIsAvailable = InetAddress.getByName(host).isReachable(300);
					Log.i(TAG, "FakeMedusaServer available: " + (fakeServerIsAvailable ? "yes" : "no"));
				} catch (UnknownHostException e) {
					
				} catch (IOException e) {
					
				}
			}
			
			if (!fakeServerIsAvailable) return "";
			
			boolean isAuthPart = request.isPartOfAuthenticationExchange();
			String outUrl;
			if (isAuthPart || testIsLoggedin) {
				outUrl = "";									
			} else {
				outUrl =  FAKE_URL + "?Location=" + urlString;
			}

			Log.w(TAG, "testRedirectModifyURL out:" + outUrl);
			return outUrl;
		}

	}



}
