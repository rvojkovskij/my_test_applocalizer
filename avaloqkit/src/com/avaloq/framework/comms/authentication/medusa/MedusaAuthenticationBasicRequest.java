package com.avaloq.framework.comms.authentication.medusa;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * Executes an Medusa Basic authentication request
 * 
 *  @author bachi
 */
public class MedusaAuthenticationBasicRequest extends MedusaAuthenticationAbstractRequest{

	public MedusaAuthenticationBasicRequest(RequestStateEvent<MedusaAuthenticationBasicRequest> rse, 
			String username, String password) {
		
		super(rse);		
		setRequestBody("user=" + username + "&password=" + password);						
	}

}
