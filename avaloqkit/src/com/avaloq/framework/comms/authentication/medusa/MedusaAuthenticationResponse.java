package com.avaloq.framework.comms.authentication.medusa;

import java.net.HttpCookie;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;

import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;

/**
 * Handles a Medusa authentication response.
 * 
 * @author bachi
 *
 */
public class MedusaAuthenticationResponse extends AbstractHTTPServerResponse<MedusaAuthenticationResponse> {										  
	
	private static final String TAG = MedusaAuthenticationResponse.class.getSimpleName();
	
	/** The response's X-Auth-Status header **/
	private MedusaAuthStatus mMedusaAuthStatus;	

	/** The response's status code - currently only used in pwd change responses **/
	private int mMedusaStatusCode;
	
	/** The response's (localized) status text message - currently only used in pwd change responses **/
	private String mMedusaStatusText;

	/** Is this a password change response? */
	private boolean mPasswordChangeResponse = false;
	
	/** The Matrix challenge sent with the request **/
	private String mMatrixChallenge;
	

	/**
	 * The status of the current auth method's flow. 
	 * Proceed, Completed, Failed etc.
	 */
	public enum MedusaAuthStatus {
		/*CredentialsMissing,*/
		Locked,
		Failed,
		PasswordChangeRequired,
		PasswordChangeFailed, /* added by us - not directly sent by the server */
		Proceed,
		Completed
	}

	/**
	 * Provides access to the password change cookie's data.
	 * Defaults code=9 / other error if the cooke could not be parsed.
	 * 
	 * Examples raw cookies:
	 *  
	 * PWD_CHANGE=<code>10</code><text>The password is to easy to guess.</text>
	 * PWD_CHANGE=<code>0</code><text></text>
	 * 
	 * @author bachi
	 *
	 */
	public class PasswordChangeCookie {
		int mCode = 9; // defaults to 'Other error'
		String mText = "Other error"; 

		public PasswordChangeCookie(HttpCookie cookie) {

			if (cookie == null) {
				Log.e(TAG, "Password change cookie not set, setting to 'other error'");
				return;
			}

			Pattern p = Pattern.compile("<code>(.*)</code><text>(.*)</text>", Pattern.DOTALL);
			Matcher matcher = p.matcher(cookie.getValue());

			if(matcher.matches()){
				mCode = Integer.valueOf(matcher.group(1));
				mText = matcher.group(2);
			} else {
				Log.e(TAG, "Could not parse password change cookie: " + cookie.getName());
			}
		}

		public int getCode() {
			return mCode;
		}

		public String getText() {
			return mText;
		}				
	}

	@Override
	public MedusaAuthenticationResponse onParseServerResponseData() throws IllegalArgumentException {

		// Note: This flag only indicates the technical request was ok.
		// Use getMedusaAuthStatus() for evaluating the result.
		setRequestSuccessfullyExecuted(true);

		// Our signal of success is X-Auth-Status = Completed. The original request will be re-queued, so 
		// there is no need to further handle the redirect or redirect location.

		// Extract the X-Auth-Status
		String status="";
		List<String> xAuthStatusHeader = getHTTPResponseHeaders().get("X-Auth-Status");
		
		
		// X-Auth-Status is present
		if (xAuthStatusHeader != null && xAuthStatusHeader.size() > 0) {

			try {					
				status = xAuthStatusHeader.get(0);
				mMedusaAuthStatus = MedusaAuthStatus.valueOf(status);
				
				mMatrixChallenge = getHTTPResponseHeader("X-Auth-Challenge");

			} catch (Exception e) {
				Log.e(TAG, "Medusa returned an undefined X-Auth-Status header: " + status);
				e.printStackTrace();
				mMedusaAuthStatus = MedusaAuthStatus.Failed;
				setRequestSuccessfullyExecuted(false);
			}						
		} 
		
		// No X-Auth-Status found - could be a pwd change
		else if (getHTTPResponseCode() == 302) {

			// This happens only during the password change flow - which uses a 302 
			HttpCookie pwdChangeCookie = getCookie("PWD_CHANGE");
			if (pwdChangeCookie != null) {
				PasswordChangeCookie pwChangeResponse = new PasswordChangeCookie(pwdChangeCookie);
				if (pwChangeResponse.getCode() == 0) {
					mMedusaAuthStatus = MedusaAuthStatus.Proceed;
				} else {
					mMedusaAuthStatus = MedusaAuthStatus.PasswordChangeFailed;
					mMedusaStatusCode = pwChangeResponse.getCode();
					mMedusaStatusText = pwChangeResponse.getText();
				}
				mPasswordChangeResponse  = true;
			} else {
				Log.w(TAG, "No PWD_CHANGE cookie found!");
			}
		} 
		
		// Undefined state - bail out
		else {
			
			setRequestSuccessfullyExecuted(false);
			throw new IllegalArgumentException("Medusa returned an undefined http response code: " + getHTTPResponseCode());
		}


		Log.v(TAG, "Medusa auth status: " + mMedusaAuthStatus + " / http response code : " + getHTTPResponseCode());
						
		if (mMedusaAuthStatus == MedusaAuthStatus.Completed) {			
			Log.i(TAG, "Medusa login successful!");											
		} 		
		
		return null;
		 		

	}

	/**
	 * Get the Medusa authentication response status, either
	 * Completed, Proceeed or any of the defined X-Auth-Status.
	 * 
	 * @return medusa auth status
	 */
	public MedusaAuthStatus getMedusaAuthStatus() {
		return mMedusaAuthStatus;
	}
	
	/**
	 * Get the response's status code - currently only used in pwd change responses
	 * @return
	 */
	public int getMedusaStatusCode() {
		return mMedusaStatusCode;
	}

	/**
	 * Get the response's (localized) status text message - currently only used in pwd change responses
	 * @return
	 */
	public String getMedusaStatusText() {
		return mMedusaStatusText;
	}

	/**
	 * Return whether this is a response to a password change request.
	 * 
	 * Example: 
	 * 302 
	 * Location: /auth/app-authenticate
	 * Set-Cookie: PWD_CHANGE=<code>10</code><text>The password is to easy to guess.</text>

	 * @return
	 */
	public boolean isPasswordChangeResponse() {
		return mPasswordChangeResponse;
	}

	/**
	 * Get the challenge for a matrix auth request.
	 * @return challenge string or null if not present.
	 */
	public String getMedusaMatrixChallenge() {
		return mMatrixChallenge;
	}

}
