package com.avaloq.framework.comms.authentication.medusa;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * Executes an Medusa Basic authentication request
 * 
 *  @author bachi
 */
public class MedusaAuthenticationMtanRequest extends MedusaAuthenticationAbstractRequest{
	
	public MedusaAuthenticationMtanRequest(RequestStateEvent<MedusaAuthenticationMtanRequest> rse, 
			String mtan) {
		
		super(rse);		
		setRequestBody("response=" + mtan);		
	}

}
