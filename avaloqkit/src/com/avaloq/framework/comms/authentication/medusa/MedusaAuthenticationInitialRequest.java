package com.avaloq.framework.comms.authentication.medusa;

import java.net.URL;

import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;
import com.avaloq.framework.comms.http.HttpConstants;

/**
 * Handle the initial redirect to Medusa.
 * Example: GET /auth/app-authenticate?Location=http://test.com/afs/payment/list 
 *
 * @author bachi
 *
 */
public class MedusaAuthenticationInitialRequest extends MedusaAuthenticationAbstractRequest {

	private String mURL;
	
	public MedusaAuthenticationInitialRequest(RequestStateEvent<? extends MedusaAuthenticationAbstractRequest> rse,
			AbstractHTTPServerRequest<?> request) {
		super(rse);				

		AbstractHTTPServerResponse<?> response = (AbstractHTTPServerResponse<?>) request.getResponse();	
		
		// Extract the redirect location from the previous response.
		try {
			URL requestURL = new URL(request.getRequestURL());
			String relativeMedusaURL = response.getHTTPResponseHeader("Location");
			URL medusa303URL = new URL(requestURL, relativeMedusaURL);
			mURL =  medusa303URL.toString();
			String medusaBaseURL = mURL.substring(0, mURL.indexOf("?"));
			MedusaAuthenticationHandler.mMedusaURL = new URL(medusaBaseURL);
			
			
		} catch (Exception e) {
			throw new IllegalStateException("The Medusa initial redirect is invalid.");			
		} 
	}
	
	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_GET;
	}

	@Override
	public String getRequestURL() {
		return mURL;				
	}
	
	

}
