package com.avaloq.framework.comms.authentication;

import java.util.List;

import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.QueueManager;

public class AuthenticationManager {

	private static AuthenticationManager mInstance;
	
	private boolean mAuthenticationInProgress;

	private final static String TAG = AuthenticationManager.class.getSimpleName();
	
	/**
	 * Singleton constructor
	 */
	private AuthenticationManager() {}
	
	public static AuthenticationManager getInstance() {
		if (mInstance == null) {
			mInstance = new AuthenticationManager(); 
		}		
		return mInstance;
	}
	
	
	/**
	 * Pass the requestWithResponse to all configured authentication providers for inspection.
	 * Return true if any of them identifies the response as authentication response, false otherwise.
	 * 
	 * @param aRequest
	 * @return whether this is a security response
	 */
	public boolean isAuthenticationResponse(AbstractServerRequest<?> requestWithResponse) {
		
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for (AbstractAuthenticationHandler handler : handlers) {
			if (handler.isAuthenticationResponse(requestWithResponse)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the instance specific authentication handler.
	 * @param clazz auth handler class
	 * @return the corresponding auth handler instance, or null if not available / configured / used
	 */
	public AbstractAuthenticationHandler getAuthenticationHandler(Class<? extends AbstractAuthenticationHandler> clazz) {
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for (AbstractAuthenticationHandler handler : handlers) {
			if (handler.getClass() == clazz) {
				return handler;
			}
		}
		return null;
	}
	

	
	/**
	 * Pass the requestWithResponse to all configured authentication providers for inspection, 
	 * and if any of them identifies the response as authentication response, for processing.
	 * 
	 * Does not change the request status - should be done by the caller.
	 * 
	 * @param aRequest
	 * @return 
	 * @return whether this is a security response
	 */
	public void processAuthenticationResponse(AbstractServerRequest<?> requestWithResponse) {
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for (AbstractAuthenticationHandler handler : handlers) {
			
			if (handler.isAuthenticationResponse(requestWithResponse)) {
				
				// Handle the auth response, can either be async or sync
				
				// The auth in progress flag is set here, blocking the server request queue.
				// It needs to be unset by the handler once the (possibly async) sequence is finished.
				setAuthenticationInProgress(true);
				handler.processAuthenticationResponse(requestWithResponse);
			}
		}		
	}
	
	/**
	 * Send a logout to all authentication providers
	 * (regardless of current login state)
	 */
	public void logoutAll() {
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for (AbstractAuthenticationHandler handler : handlers) {
			handler.logout();
		}		
	}

	/**
	 * Set whether currently an authentication sequence is in progress.
	 * All other server requests will be held back until the authentication sequence has finished.
	 * 
	 * @param inProgress
	 */
	public void setAuthenticationInProgress(boolean inProgress) {
		if (inProgress) {
			Log.d(TAG, "Authentication is in progress (halting request queue)");
		} else {
			Log.d(TAG, "Authentication finished (resuming request queue)");
		}
		mAuthenticationInProgress = inProgress;
	}
	
	/**
	 * Is currently an authentication sequence in progress?
	 * @return
	 */
	public boolean isAuthenticationInProgress() {
		return mAuthenticationInProgress;
	}
	
	/**
	 * Cancel the authentication process for all auth handlers, and re-start the queue
	 */
	public void authenticationCancelledByUser() {
		QueueManager.getInstance().failAllPendingRequests();
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for(AbstractAuthenticationHandler handler : handlers) {
			handler.setAuthenticationCompleted(false);
		}
		setAuthenticationInProgress(false);
	}
	
	/**
	 * Returns true if at least one 
	 * @return
	 */
	public boolean isOneAuthenticated() {
		List<AbstractAuthenticationHandler> handlers = AvaloqApplication.getInstance().getAuthenticationHandlers();
		for (AbstractAuthenticationHandler handler : handlers) {
			if (handler.isAuthenticated()) {
				return true;
			}
		}
		return false;
	}
	
}
