package com.avaloq.framework.comms.authentication;

import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.LoginTimeoutHandlerThread;

public abstract class AbstractAuthenticationHandler {

	private static final String LOG_TAG = "AbstractAuthenticationHandler";

	// The base address of the authentication server
	protected String mAuthServerBaseUrl = null;
	// The address of the currently used authentication service
	protected String mAuthLocation = null;

	protected boolean mIsAuthenticated;


	public void setAuthServerBaseUrl(String baseUrl) {
		mAuthServerBaseUrl = baseUrl;
	}

	/**
	 * Inform the AuthenticationManager that the auth process was completed
	 * so the server request queue can be unblocked, and set the authentication status.
	 */
	public void setAuthenticationCompleted(boolean success) {
		AuthenticationManager.getInstance().setAuthenticationInProgress(false);	
		setAuthenticationStatus(success);
	}

	/**
	 * This method is called when a challenge was cancelled by the user
	 */
	public void authenticationCancelledByUser() {
		Log.v(LOG_TAG,"auth cancelled: failing all pending authentication requests");
		//TODO: failAllPendingRequests();
		setAuthenticationCompleted(false);
	}
	
	/**
	 * Set the status to either authenticated (logged in) or unauthenticated (not logged in)
	 * 
	 * @param isAuthenticated
	 */
	public void setAuthenticationStatus(boolean isAuthenticated) {
		mIsAuthenticated = isAuthenticated;
		
		// Start the timeout thread on the first login (and ensure it's still running on subsequent ones)
		// if a session timeout is configured		
		if (mIsAuthenticated && AvaloqApplication.getInstance().getConfiguration().getServerSessionDefaultTimeout() > 0) {
			LoginTimeoutHandlerThread.startTimeoutHandler();
		}
	}

	/**
	 * Return true if an authentication is currently in progress.
	 * 
	 * @return
	 */
	public boolean isAuthenticationInProgress() {
		return AuthenticationManager.getInstance().isAuthenticationInProgress();
	}
	
	/**
	 * Is the app currently authenticated / logged in with this auth provider?
	 * @return true if logged in
	 */
	public boolean isAuthenticated() {
		return mIsAuthenticated;
	}


	public abstract boolean isAuthenticationResponse(AbstractServerRequest<?> aRequest);
	
	/**
	 * Set this handler to logged out state. 
	 * It will call then onLogout() event after setting the login flag to false.
	 */
	public void logout() {
		mIsAuthenticated = false;
		onLogout();
	}
	
	/**
	 * Process the response from a server request. Before invoking this method,
	 * the caller must first ensure that an authentication process in in
	 * progress This method must fill mAuthLocation and potentially
	 * mAuthChallenge if the response triggers a new process.
	 * 
	 * @param aResponse
	 *            The server response to process
	 */
	public abstract void processAuthenticationResponse(AbstractServerRequest<?> aRequest);
	
	/**
	 * Is called when the user is logged out of the Application
	 */
	public abstract void onLogout();


}
