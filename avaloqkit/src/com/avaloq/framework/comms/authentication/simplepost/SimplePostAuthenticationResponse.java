package com.avaloq.framework.comms.authentication.simplepost;

import android.util.Log;

import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;

/**
 * Executes a test/demo user login by sending a fixed demo username/password.
 * The service will set a SAML cookie that can be re-used in subsequent requests.
 * 
 * TestSecurityRequest and TestSecurityResponse also serve as an working example of layer separation between HTTP and JSON
 * by implenting their own non-json, form-style encoding over AbstractHTTPServerRequest
 * 
 *  @author bachi
 */
public class SimplePostAuthenticationResponse extends AbstractHTTPServerResponse<SimplePostAuthenticationResponse> {										  

	private static final String TAG = "TestSecurityResponse";

	@Override
	public SimplePostAuthenticationResponse onParseServerResponseData() throws IllegalArgumentException {
		
		if (getHTTPResponseBody().equals("<html><body>SAML Cookie set!</body></html>")) {
			setRequestSuccessfullyExecuted(true);
			Log.v(TAG, "Demo login successful, SAML Cookies set!");
			return null;
			
		} else {			
			setRequestSuccessfullyExecuted(false);
			Log.w(TAG, "Demo login failed, reponse was: " + getHTTPResponseBody());
			return null;
		}
	}

}
