package com.avaloq.framework.comms.authentication.simplepost;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.ui.DialogActivity;

/**
 * This activity handles the UI aspect of a specific authentication handler (SimplePostAuthenticationHandler).
 * It is not meant as an independent login screen usable for different authentication handlers.
 * 
 * TODO: Base this on Abstract
 *
 */
public class SimplePostLoginActivity extends SherlockFragmentActivity implements OnEditorActionListener {

	private static final String TAG = SimplePostLoginActivity.class.getSimpleName();

	private LinearLayout mLoginBox;
	private ProgressBar mProgress;

	private EditText mUsername;
	private EditText mPassword;
	private EditText mContract;

	private SimplePostAuthenticationHandler authHandler; 

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		// Get our authentication handler
		authHandler = (SimplePostAuthenticationHandler) AuthenticationManager.getInstance().getAuthenticationHandler(SimplePostAuthenticationHandler.class);
		if (authHandler == null) {
			// This is not recoverable.
			throw new IllegalStateException("LoginActivity could not retrieve its authentication provider.");
		}

		setContentView(R.layout.avq_login_dialog);

		mLoginBox = (LinearLayout)findViewById(R.id.avq_login_fieldbox);
		mProgress = (ProgressBar)findViewById(R.id.avq_login_progress);

		mLoginBox.setVisibility(View.VISIBLE);
		mProgress.setVisibility(View.GONE);

		mUsername = (EditText)findViewById(R.id.avq_login_username);
		mPassword = (EditText)findViewById(R.id.avq_login_password);
		mContract = (EditText)findViewById(R.id.avq_login_contract);

		mUsername.setOnEditorActionListener(this);
		mPassword.setOnEditorActionListener(this);
		mContract.setOnEditorActionListener(this);

		mPassword.setTypeface(Typeface.DEFAULT);
		mPassword.setTransformationMethod(new PasswordTransformationMethod());		


		// Convenience hack
		((ImageView) findViewById(R.id.avq_login_headerimg)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*mUsername.setText("demo");
				mPassword.setText("AFSdemo1");
				mContract.setText("190101");*/
				mUsername.setText("demo");
				mPassword.setText("AFSdemo3");
				mContract.setText("190101");
			}
		});

		// Flag the window as secure so the taskmanager will not generate thumbnails
		// and no screenshots can be made. Works only as of Honeycomb
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) { 
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
		}
		
		TextView version = (TextView)findViewById(R.id.version_number);
		version.setText(getString(R.string.version_number).replace("%Version%", AvaloqApplication.getInstance().getVersion()));
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
		mLoginBox.setVisibility(View.VISIBLE);
		mProgress.setVisibility(View.GONE);
		mUsername.setText("");
		mPassword.setText("");
		mContract.setText("");
	}

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {
			new AsyncTask<Void, Void, SimplePostAuthenticationRequest>() {
				protected void onPreExecute() {
					mLoginBox.setVisibility(View.GONE);
					mProgress.setVisibility(View.VISIBLE);
				};
				// Step 2 of 3: Execute POST req. immediately (no queueing)
				@Override
				protected SimplePostAuthenticationRequest doInBackground(Void... params) {
					SimplePostAuthenticationRequest securityRequest = new SimplePostAuthenticationRequest(
							new RequestStateEvent<SimplePostAuthenticationRequest>() {},
							mUsername.getText().toString(),
							mPassword.getText().toString(),
							mContract.getText().toString()
							);

					// Execute and handle the response
					securityRequest.executeRequest();    				
					if (securityRequest.getResponse() != null) {

						// Since we bypass the queue, we need to manually timestamp this request 
						QueueManager.getInstance().markTimeOfLastExecutedRequest();

						// Handle the returned data
						securityRequest.getResponse().onParseServerResponseData();
					}
					return securityRequest;
				}						

				@Override
				protected void onPostExecute(SimplePostAuthenticationRequest securityRequest) {

					// Step 3 of 3: Evaluate response
					// AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SimplePostLoginActivity.this);	// FIXME: This is not a good context-giver as it can be null

					// Network error
					if (securityRequest.getResponse() == null) {
						Log.w(TAG, "No answer from SimplePostAuth request");

						securityRequest.setRequestStateFailed();
						
						DialogActivity.show(
								getText(R.string.avq_login_not_possible_title).toString(),
								getText(R.string.avq_login_not_possible_text).toString(),
								getText(R.string.avq_ok).toString(),
								false
						);

						mProgress.setVisibility(View.GONE);
						mLoginBox.setVisibility(View.VISIBLE);

					} else if (securityRequest.getResponse().wasRequestSuccessfullyExecuted()) {

						// Successful login
						Log.i(TAG, "Login successful.");

						securityRequest.setRequestStateCompleted();    					    					
						authHandler.setAuthenticationCompleted(true);

						// TODO Find out if we have to finish now...
						//    					if(AvaloqApplication.getInstance().isInitialLoginDone()) {
						//        					Log.d(TAG, "finishing");
						//        					finish();
						//    					} else {
						//        					AvaloqApplication.getInstance().setInitialLoginDone(true);
						//        					Log.d(TAG, "initial login done");
						//    					}

					} else {

						// Invalid login
						Log.i(TAG, "Login invalid.");
						securityRequest.setRequestStateFailed(); 
						DialogActivity.show(
								getText(R.string.avq_login_invalid_title).toString(),
								getText(R.string.avq_login_invalid_text).toString(),
								getText(R.string.avq_login_invalid_button).toString(),
								false
						);

						mUsername.setText("");
						mPassword.setText("");
						mContract.setText("");

						mProgress.setVisibility(View.GONE);
						mLoginBox.setVisibility(View.VISIBLE);

					}
				}

			}.execute();
			return true;
		}

		return false;
	}
	
	@Override
	public void onBackPressed() {
		Log.d(TAG, "cancelling requests");
		// Shutdown
		AvaloqApplication.shutdown();
		AuthenticationManager.getInstance().authenticationCancelledByUser();
		// BankletActivity.exitApplication();
		Intent homeIntent = new Intent(Intent.ACTION_MAIN);
		homeIntent.addCategory(Intent.CATEGORY_HOME);
		homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(homeIntent);
	}

}
