package com.avaloq.framework.comms.authentication.simplepost;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.LoginTimeoutHandlerThread;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.authentication.AbstractAuthenticationHandler;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.ui.BankletActivity;

/**
 * Provides a simple authentication handler for the login/pw post authentication used during development.
 * The user interaction part is handled in LoginActivity.
 */
public class SimplePostAuthenticationHandler extends AbstractAuthenticationHandler {

	@Override
	public boolean isAuthenticationResponse(AbstractServerRequest<?> aRequest) {
		if (aRequest instanceof AbstractHTTPServerRequest<?>) {						
			int responseCode = ((AbstractHTTPServerRequest<?>) aRequest).getHttpResponse().getHTTPResponseCode();
			String body = ((AbstractHTTPServerRequest<?>) aRequest).getHttpResponse().getHTTPResponseBody();
			
			// Trigger on that text
			return (body != null && body.equals("Could not extract SAML assertion.") && responseCode == 200);
		}

		return false;
	}

	@Override
	public void processAuthenticationResponse(AbstractServerRequest<?> aRequest) {

		// Re-check if it's an auth response
		if (!isAuthenticationResponse(aRequest)) return;

		// Step 1 of 3: Show login user dialog (further steps handled in the corresponding activity)
		showLoginDialog();
	}

	/**
	 * Show the login activity. Input handling etc. is controlled from there.
	 */
	public void showLoginDialog() {
		
		// FIXME: Check if BankletActivity.getActiveActivity() is a stable context-source.
		Context ctx = BankletActivity.getActiveActivity();		
		if (ctx == null) {
			throw new IllegalStateException("BankletActivity.getActiveActivity() unexpectedly returned null :(.");
		}
		Intent intent = new Intent(ctx, SimplePostLoginActivity.class);		
		//intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
		ctx.startActivity(intent);
	}

	
	/**
	 * Turn the session timeout monitoring on/off depending on the set auth status
	 */
	@Override
	public void setAuthenticationStatus(boolean isAuthenticated) {		
		super.setAuthenticationStatus(isAuthenticated);
	}
	
	
	@Override
	public void onLogout() {
		AbstractHTTPServerRequest.emptyCookieStore();
	}

}
