package com.avaloq.framework.comms.authentication.simplepost;

import java.util.HashMap;
import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.HttpConstants;

/**
 * Executes a test/demo user login by sending a fixed demo username/password.
 * The service will set a SAML cookie that can be re-used in subsequent requests.
 * 
 * TestSecurityRequest and TestSecurityResponse also serve as an working example of layer separation between HTTP and JSON
 * by implenting their own non-json, form-style encoding over AbstractHTTPServerRequest
 * 
 *  @author bachi
 */
public class SimplePostAuthenticationRequest extends AbstractHTTPServerRequest<SimplePostAuthenticationResponse>{

	String mUserName;
	String mPassword;
	String mContract;
	
	public SimplePostAuthenticationRequest(RequestStateEvent<SimplePostAuthenticationRequest> rse, String userName, String password, String contract) {
		super(rse);
		
		mUserName = userName;
		mPassword = password;
		mContract = contract;
				
		setCachePolicy(CachePolicy.NO_CACHE);
		setPartOfAuthenticationExchange();
		disableChunkedMode();
	}

	@Override
	public SimplePostAuthenticationResponse createEmptyResponseObject() {		
		return new SimplePostAuthenticationResponse();
	}
	
//	@Override
//	public JsonHTTPResponse<ResponseType> createEmptyResponseObject() {
//		return new JsonHTTPResponse<ResponseType>(mClass);
//	}

	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_POST;
	}

	@Override
	public String getRequestURL() {
		
		return AvaloqApplication.getInstance().getConfiguration().getBaseUrl() + "/avaloq.front.login/";		
	}

	@Override
	public Map<String, String> getHttpHeaders() {		
		Map<String, String> headers = new HashMap<String, String>();

		headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("User-Agent", "Android");
        
        return headers;
	}

	@Override
	public String getRequestBody() {		
		return "noRedirect=true&clientId=" + mContract + "&username=" + mUserName + "&password=" + mPassword;
	}

//	@Override
//	public String getWebserviceType() {
//		return "";
//	}
//
//	@Override
//	public String getWebserviceVersion() {
//		return "";
//	}

 

	 

}
