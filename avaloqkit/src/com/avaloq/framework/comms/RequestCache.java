package com.avaloq.framework.comms;

import android.support.v4.util.LruCache;

/**
 * Caches the parsed server response (and its already re-created TO payload) 
 * to any given server request (identified by its cache key).
 * It uses Android's LRU, limited-size cache.
 * 
 * @author bachi
 *
 */
public class RequestCache extends LruCache<String, AbstractServerResponse<?>>{

	private static final int MAX_CACHE_ENTRIES = 20;
	
	private static RequestCache mInstance;
	
	private RequestCache(int maxSize) {
		super(maxSize);		
	}
	
	/**
	 * Get the singleton instance.
	 * @return
	 */
	public static RequestCache getInstance() {
		if (mInstance == null) {
			mInstance = new RequestCache(MAX_CACHE_ENTRIES);
		}		
		return mInstance;
	}
}
