package com.avaloq.framework.comms;

import android.util.Log;

import com.avaloq.framework.comms.authentication.AuthenticationManager;


class QueueResponseThread extends Thread {

	private final static String TAG = QueueResponseThread.class.getSimpleName();

	public QueueResponseThread() {
		setName(TAG);
	}

	@Override
	public void run() {

		QueueManager queueManager = QueueManager.getInstance();

		while (!queueManager.shutdown()) {
			AbstractServerRequest<?> requestWithResponse = queueManager.getNextServerResponse();

			// Process the response
			if (requestWithResponse != null && requestWithResponse.getResponse() != null) {
								
				// Process security/authentication
				// If any auth handler takes interest in this response, pass it for authentication processing
				AuthenticationManager am = AuthenticationManager.getInstance();
				if (am.isAuthenticationResponse(requestWithResponse)) {
					Log.d(TAG, "Request " + requestWithResponse.getRequestIdentifier() + " requires authentication.");
					requestWithResponse.setPartOfAuthenticationExchange();
					requestWithResponse.setRequestStatePendingAuthentication();

					am.processAuthenticationResponse(requestWithResponse);
					
					// Re-queue the original request.
					// Remove the response, and enqueue the same request again
					Log.d(TAG, "Re-queueing request that triggered an auth response: "+ requestWithResponse.getRequestIdentifier());
					requestWithResponse.setResponse(null);
					
					// Request is already in PENDING_AUTHENTICATION
					QueueManager.getInstance().enqueueServerRequest(requestWithResponse);
					
					// Re-queue all other pending responses, too.										
					QueueManager.getInstance().requeueAllQueuedResponses();					
										
				} 
				
				// Process a non-auth response
				// Note: Technical errors (i.e., io errors) are processed on the receiving side,
				// possibly incomplete responses are not passed to the response queue
				else {
					
					// TODO: If this was a http request, and we received a 400/500 status code,
					// and it was not picked up by security (e.g. 500er on the demo system means "need login"),
					// the body will contain the error string, which is not parseable by the specialized
					// response class. Do something about it (but in there we're protocol-agnostic)
					// -> AbstractServerResponse.isValidResponse() -> implement it in HTTP

					// Cached responses are already deserialized
					if (requestWithResponse.isCachedResponse()) {
						// Let the subclasses act also when a cached response is used
						requestWithResponse.getResponse().onUseCachedResponse();
						Log.d(TAG, "Using cached response for request " + requestWithResponse.getRequestIdentifier());
					}
					// Uncached responses need processing
					else {
						Log.d(TAG, "Processing server response to request " + requestWithResponse.getRequestIdentifier());
						requestWithResponse.getResponse().parseServerResponseData();
						
						// Give the request a signal to cache the response
						requestWithResponse.addResponseToCache();
					}
					
					

					// Mark the request as completed. This does not determine if it was successful.
					// Successful or not must be set in parseServerResponseData()
					requestWithResponse.setRequestStateCompleted();					
				}								
			} 
			
			// Got an avq_activity_empty response
			else{
				if (requestWithResponse != null) {
					Log.w(TAG, "ResponseQueueHandler - no response for request " + requestWithResponse.getRequestIdentifier());
				}
				// TODO: Handling of technical/network errors
				//An avq_activity_empty response means there was a technical, i.e. IO error.
			}
			

			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {			
				e.printStackTrace();
			}
		}
	}

}
