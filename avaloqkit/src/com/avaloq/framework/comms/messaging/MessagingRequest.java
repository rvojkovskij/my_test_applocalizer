package com.avaloq.framework.comms.messaging;

import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.HttpConstants;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;

public class MessagingRequest extends AbstractJsonHTTPRequest<MessagingResult>{

	public MessagingRequest(RequestStateEvent<?> rse) {
		super("", rse, null, MessagingResult.class);
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}
	
	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_GET;
	}
	
	@Override
	public MessagingJsonHTTPResponse createEmptyResponseObject() {
		return new MessagingJsonHTTPResponse();
	}

	@Override
	public String getRequestURL() {
		/*return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + 
				"" +
				"app-maintenance-message" +
				"?lang="+Locale.getDefault().getLanguage();*/
		return "http://192.168.1.125/medusa/app-maintenance-message.php";
	}

}
