package com.avaloq.framework.comms.messaging;

public class MessagingResult {
	boolean online;
	String message;
	
	public void setOnline(boolean aOnline){
		online = aOnline;
	}
	
	public boolean getOnline(){
		return online;
	}
	
	public void setMessage(String aMessage){
		message = aMessage;
	}
	
	public String getMessage(){
		return message;
	}
}
