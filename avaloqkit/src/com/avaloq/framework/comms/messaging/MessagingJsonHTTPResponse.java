package com.avaloq.framework.comms.messaging;

import com.avaloq.framework.comms.http.json.JsonHTTPResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class MessagingJsonHTTPResponse extends JsonHTTPResponse<MessagingResult> {

	public MessagingJsonHTTPResponse() {
		super(MessagingResult.class);
	}
	
	@Override
	public MessagingResult onParseServerResponseData() throws IllegalArgumentException {

		Gson gson = new GsonBuilder()
						.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
						.create();
		MessagingResult responseObject = gson.fromJson(getHTTPResponseBody(), MessagingResult.class);
		return responseObject;
	}

}
