package com.avaloq.framework.comms.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.comms.authentication.medusa.MedusaAuthenticationAbstractRequest;
import com.avaloq.framework.comms.authentication.medusa.MedusaAuthenticationHandler;
import com.avaloq.framework.comms.http.pinning.util.PinningHelper;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.LogoutActivity;
import com.avaloq.framework.util.Tools;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

/**
 * The abstract class for HTTP Requests
 * @author bachi
 * @param <ResponseType> The type of response
 */
public abstract class AbstractHTTPServerRequest<ResponseType> extends AbstractServerRequest<ResponseType> {

	private static final int TIMEOUT_CONNECT = 600 * 1000; // TODO: Could be increased later	
	private static final int TIMEOUT_READ = 600 * 1000;
	private static boolean mCookieHandlerInitialized = false;

	/**
	 * Do not follow 30x redirects automatically by default. 
	 * Reason: auth redirects require manual auth handler intervention (or the response type would be wrong)
	 */
	protected boolean mFollowRedirects = false;
	private boolean mNoChunkedMode = false;;


	/**
	 * A Map of all HTTP headers for this request
	 */
	protected static final Map<String,String> HTTP_HEADERS = new HashMap<String,String>();

	static {

		// Connection: Close is required in Jelly Bean, otherwise random EOF errors can appear.
		// see http://stackoverflow.com/a/13981774/454667
		HTTP_HEADERS.put("Connection", "Close");
		System.setProperty("http.keepAlive", "false");

		// The device locale is by default enabled and sent 
		HTTP_HEADERS.put("Accept-Language", AvaloqApplication.getInstance().getLocale());

	}

	// *****************************************************
	// *** Constructors
	// *****************************************************

	/**
	 * The Log-Tag for this class
	 */
	private static final String TAG = AbstractHTTPServerRequest.class.getSimpleName();

	public AbstractHTTPServerRequest(RequestStateEvent<?> rse) {
		super(rse);
	}

	/**
	 * Return a hash over this request used by the cache. 
	 * It is based on the headers, body and url of this request.
	 * 
	 * @return hash key
	 */
	public String getCacheKey() {
		// Ensure header order is ignored for hash generation
		String headerString = "";
		if(getHttpHeaders() != null) {
			List<String> sortedKeys=new ArrayList<String>(getHttpHeaders().keySet());
			for(String key : sortedKeys) {										
				headerString += (key + getHttpHeaders().get(key));
			}
		}		
		return Tools.md5(getRequestURL() + getRequestMethod() + headerString + getRequestBody());
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	private URLConnection getConnection() throws IOException, NoSuchAlgorithmException, KeyManagementException{
		URLConnection serviceConnection = null;

		// Create a connection
		String urlString = getRequestURL();
		URL webServiceURL = new URL(urlString);
		if (!webServiceURL.getProtocol().equals("https")){
			serviceConnection = (HttpURLConnection) webServiceURL.openConnection();
		}else{			
			serviceConnection = PinningHelper.getPinnedHttpsURLConnection(AvaloqApplication.getContext(), AvaloqApplication.getInstance().getConfiguration().getSSLTrustedPins(), webServiceURL);
		}

		// Configure the request
		if (serviceConnection instanceof HttpsURLConnection){
			((HttpsURLConnection) serviceConnection).setRequestMethod(getRequestMethod());
			// TODO: check whether we can follow redirects "blindly"
			((HttpURLConnection) serviceConnection).setInstanceFollowRedirects(mFollowRedirects);
			if (!mNoChunkedMode ) ((HttpURLConnection) serviceConnection).setChunkedStreamingMode(0);		
		}else{
			((HttpURLConnection) serviceConnection).setRequestMethod(getRequestMethod());
			// TODO: check whether we can follow redirects "blindly"
			((HttpURLConnection) serviceConnection).setInstanceFollowRedirects(mFollowRedirects);
			if (!mNoChunkedMode ) ((HttpURLConnection) serviceConnection).setChunkedStreamingMode(0);		
		}
		serviceConnection.setConnectTimeout(TIMEOUT_CONNECT);
		serviceConnection.setReadTimeout(TIMEOUT_READ);

		serviceConnection.setDoInput(true);

		serviceConnection.setUseCaches(false);
		serviceConnection.setAllowUserInteraction(false);

		return serviceConnection;
	}

	/**
	 * Execute the request immediately
	 * Reads in the response entity and initiates handling in subclasses
	 * Creates and sets the avq_activity_empty response.
	 * 
	 * Responsibility of this method is to prepare everything so the request can be put 
	 * on the response handling queue by the calling party.
	 * 
	 * Not called if a cached response can be used.
	 * 
	 * Note: If you bypass the queue using this method directly, you might want to mark
	 * this request's timestamp for proper session handling: QueueManager.getInstance().markTimeOfLastExecutedRequest(); 
	 * 
	 */
	@Override
	public void executeRequest() {

		// Set the state to executing - abort if it cannot be set.
		if (!setRequestStateExecuting()) {
			Log.e(TAG, "Execution of request aborted - cannot set to state EXECUTING");
			return;
		}

		URLConnection serviceConnection = null;

		// Initialize the cookie handler for all subsequent request 
		// Note: We receive cookies but don't automatically resent them.		
		if (!mCookieHandlerInitialized) {
			CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
			mCookieHandlerInitialized = true;
		}

		// Create an empty response to wrap the result of the request
		AbstractHTTPServerResponse<ResponseType> aResponse = createEmptyResponseObject();

		try {
			String urlString = getRequestURL();

			serviceConnection = getConnection();

			// set the headers
			Map<String,String> httpHeaders = getHttpHeaders();
			if(httpHeaders != null) {
				for(Entry<String,String> header : httpHeaders.entrySet()) {										
					serviceConnection.addRequestProperty(header.getKey(), header.getValue());
					Log.d(TAG, "header: "+header.getKey()+" / "+header.getValue());
				}
			}

			// POST request: Add the body 
			if (getRequestMethod().equals(HttpConstants.METHOD_POST)) {

				serviceConnection.setDoOutput(true);

				OutputStream out = serviceConnection.getOutputStream();
				Writer writer = new OutputStreamWriter(out, "UTF-8");

				String postData = getRequestBody();
				Log.v(TAG, "executeRequest: POST request " + getRequestIdentifier() + " to URL " + getRequestURL() + " with data: " + postData);

				if (postData != null) {
					// writer.write(URLEncoder.encode(postData, "UTF-8"));
					writer.write(postData);
				}
				writer.close();
				out.close();

				// GET request
			} else if (getRequestMethod().equals(HttpConstants.METHOD_GET)) {
				Log.v(TAG, "executeRequest: GET request " + getRequestIdentifier() + " to URL " + getRequestURL());
			}

			// Cookies: We don't automatically share and send them across ws requests.
			// This code is here as example should the requirements change.			
			/*for(HttpCookie cookie : getCookieManager().getCookieStore().getCookies()){
				serviceConnection.setRequestProperty("Cookie", cookie.toString());
			}*/


			// * Execute the request *
			long requestStartTime = System.currentTimeMillis();
			serviceConnection.connect();


			// Get and store the HTTP response code
			int responseCode = (serviceConnection instanceof HttpsURLConnection) ? ((HttpsURLConnection)serviceConnection).getResponseCode() : ((HttpURLConnection)serviceConnection).getResponseCode(); 			
			aResponse.setHTTPResponseCode(responseCode);
			long duration = System.currentTimeMillis() - requestStartTime;
			Log.d(TAG, "Server returned a " + responseCode + " in " + duration + "ms for url " + getRequestURL());

			aResponse.setRequestSuccessfullyExecuted(true);

			// Store the headers
			aResponse.setHTTPResponseHeaders(serviceConnection.getHeaderFields());				

			// HttpUrlConnection puts 4xx/5xx response bodies to the error stream
			// We put the body from both to the response, as some 400/500 codes may 
			// mean a authentication response. 
			// The response handler needs to check the response code.
			InputStream in = null;			

			if (responseCode / 100 != 4 && responseCode / 100 != 5) {
				in = serviceConnection.getInputStream();
			} else {
				in = (serviceConnection instanceof HttpsURLConnection) ? ((HttpsURLConnection) serviceConnection).getErrorStream() : ((HttpURLConnection) serviceConnection).getErrorStream();				
			}

			if (in != null) {
				readHTTPBody(aResponse, in);
				if (serviceConnection instanceof HttpsURLConnection) {
					((HttpsURLConnection) serviceConnection).disconnect();
				}else{
					((HttpURLConnection) serviceConnection).disconnect();
				}
			}


			// FIXME: Let the AirlockMedusa auth handler fake a 303 redirect to somewhere. Remove once we have a real server to test against			
			// FIXME: Some NPE sometimes happens in here, so sliced up the code to spot the cause
			try {
				AvaloqApplication app = AvaloqApplication.getInstance();
				AppConfigurationInterface conf = app.getConfiguration();
				Class<?>[] authhandlers = conf.getAuthenticationHandlers();
				if (authhandlers == null) {
					Log.e(TAG, "Auth handers list == null? This is weird, can't do online banking without auth...");
				}
				//if (Arrays.asList(AvaloqApplication.getInstance().getConfiguration().getAuthenticationHandlers()).contains(MedusaAuthenticationHandler.class)) {
				if (Arrays.asList(authhandlers).contains(MedusaAuthenticationHandler.class)) {

					// Inject if we're not already in a Medusa exchange
					if (!(this instanceof MedusaAuthenticationAbstractRequest)) {

						urlString = MedusaAuthenticationHandler.testRedirectModifyURL(urlString, this);

						if (!urlString.equals("")) {

							Log.w(TAG, "Injecting Medusa 303 redirect location (=auth start signal) while we don't have a real server yet");

							aResponse.setHTTPResponseCode(303);
							Map<String, List<String>> injectHeaders = new HashMap<String, List<String>>();
							List<String>locAr = new ArrayList<String>();
							locAr.add(urlString);
							injectHeaders.put("Location", locAr);
							aResponse.setHTTPResponseHeaders(injectHeaders);
						}												

					}

				}
			} catch (Exception e) {
				Log.d(TAG, "Fake Medusa Server not present.");
			}
			// FIXME end -------------


			// Log the response details
			/////Log.v(TAG, "Server request " + getRequestIdentifier() + " response " + serviceConnection.getResponseCode() + ", " + serviceConnection.getResponseMessage());
			/////Log.v(TAG, "Server request " + getRequestIdentifier() + " response content: " + aResponse.getHTTPResponseBody());

			// Set the response to this request 
			setResponse(aResponse);


			// Do nothing more. The calling party will put the object on the response queue.	
		}

		catch (Exception exception) {

			// ** Workaround for a current bug in the Medusa interface **
			// It returns 401 but without the mandatory WWW-Authenticate header (as defined in the http protocol)
			// Thus Java complains and throws an IOException
			if (exception.getMessage().equals("No authentication challenges found")) {

				// If a login fails, the current Medusa server seems to be in an undefined state
				// and it doesn't askWorkaround for a new auth type anymore (and won't respond to the previous properly)
				// Thus, we at the moment just trigger the logout to restart login

				// TODO: Remove workaround once this is solved in the Medusa interface.
				Log.w(TAG, "Wrong login, Medusa sent malformed 401 response - workaround: reinitiating login");
				AuthenticationManager.getInstance().authenticationCancelledByUser();	
				Context ctx = BankletActivity.getActiveActivity();
				Intent intent = new Intent(AvaloqApplication.getContext(), LogoutActivity.class);
				ctx.startActivity(intent);

			}

			// Normal exception handling (incl. other IOExceptions)
			Log.e(TAG, "executeRequest: Server request " + this.getRequestIdentifier() + " failed: " + exception.getMessage(), exception);
			Log.e(TAG, exception.getClass().getSimpleName());
			aResponse.setRequestSuccessfullyExecuted(false);

			// Set the response to this failed request to null and CANCEL the request (can't use partial results)
			// It is not transferred to the response queue as there was no valid response.
			setResponse(null);
			if (!isRequestStateFailed()) setRequestStateFailed();		
		}

		finally {
			if (serviceConnection != null) {
				// Break the connection
				if (serviceConnection instanceof HttpsURLConnection) {
					((HttpsURLConnection) serviceConnection).disconnect();
				}else{
					((HttpURLConnection) serviceConnection).disconnect();
				}
			}		
		}
	}

	/**
	 * This method is responsible to read the InputStream and call setHTTPResponseBody, if the returned data was a string.
	 * This method can be overridden if necessary, see {@link com.avaloq.banklet.documentsafe.DownloadRequest#readHTTPBody(AbstractHTTPServerResponse, InputStream)} for an example.
	 * @param in The InputStream from the HTTP Request
	 * @throws IOException If an error occurred reading the HTTP request
	 */
	protected void readHTTPBody(AbstractHTTPServerResponse<ResponseType> aResponse, InputStream in) throws IOException {
		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(new InputStreamReader(in));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		// Store the HTTP response body
		aResponse.setHTTPResponseBody(sb.toString());
	}

	/**
	 * Empty stored cookies.
	 */
	public static void emptyCookieStore() {
		CookieHandler.setDefault(null);
		mCookieHandlerInitialized = false;
		Log.i(TAG, "Cookie store emptied.");
	}

	/**
	 * Define whether the http client should follow 30x redirects
	 * Default is true.
	 * 
	 * @param follow
	 */
	public void setFollowRedirects(boolean follow) {
		mFollowRedirects = follow;
	}

	/**
	 * Get the (global) CookieManager.
	 * @return
	 */
	public CookieManager getCookieManager() {
		return ((CookieManager) CookieManager.getDefault());
	}

	public void disableChunkedMode() {
		mNoChunkedMode = true;
	}


	// *****************************************************
	// *** Methods to be reimplemented
	// *****************************************************

	/**
	 * Create this request's response object of the proper type 
	 */
	public abstract AbstractHTTPServerResponse<ResponseType> createEmptyResponseObject();

	/**
	 * Return the used method (either HttpConstants.METHOD_GET or HttpConstants.METHOD_POST)
	 * @return The Request Method {@link HttpConstants#METHOD_GET}|{@link HttpConstants#METHOD_POST}
	 */
	public abstract String getRequestMethod();

	/**
	 * Return a string representation of the URL to be called
	 */
	public abstract String getRequestURL();

	/**
	 * Can be used to pass specific HTTP Headers to this HTTP Request
	 * @return A Map of HTTP Headers
	 */
	public abstract Map<String,String> getHttpHeaders();

	/**
	 * Can be used to pass data that will be sent with the HTTP Request via POST.
	 * @return The request body
	 */
	public abstract String getRequestBody();


	/**
	 * Get the sever response, casted to the AbstractHTTPServerResponse type
	 * @return AbstractHTTPServerResponse
	 */
	public AbstractHTTPServerResponse<?> getHttpResponse() {
		return (AbstractHTTPServerResponse<ResponseType>) getResponse();
	}

}