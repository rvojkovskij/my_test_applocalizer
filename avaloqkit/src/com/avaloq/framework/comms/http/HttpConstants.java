package com.avaloq.framework.comms.http;

public interface HttpConstants {

	// HTTP methods
	public static final String METHOD_POST = "POST";
	public static final String METHOD_GET = "GET";
	// HTTP headers
	public static final String HEADER_WWW_AUTHENTICATE = "WWW-Authenticate";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_AUTH_STATUS = "X-Auth-Status";
	// Content types
	public static final String CONTENT_TYPE_JSON = "application/json";

}
