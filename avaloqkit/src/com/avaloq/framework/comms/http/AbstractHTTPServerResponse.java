package com.avaloq.framework.comms.http;

import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;
import java.util.Map;

import com.avaloq.framework.comms.AbstractServerResponse;

public abstract class AbstractHTTPServerResponse<ResponseType> extends AbstractServerResponse<ResponseType> {

	private int mHTTPResponseCode = -1;
	private String mHTTPResponseBody = null;
	private Map<String, List<String>> mHTTPResponseHeaders = null;

	// *****************************************************
	// *** Constructors
	// *****************************************************

	public AbstractHTTPServerResponse() {
		
	}

	// *****************************************************
	// *** HTTP methods
	// *****************************************************

	/**
	 * Get the HTTP response code
	 */
	public int getHTTPResponseCode() {
		return mHTTPResponseCode;
	}

	/**
	 * Set the HTTP response code
	 */
	public void setHTTPResponseCode(int responseCode) {
		mHTTPResponseCode = responseCode;
	}

	/**
	 * Get the HTTP response body
	 */
	public String getHTTPResponseBody() {
		return mHTTPResponseBody;
	}

	/**
	 * Set the HTTP response body
	 */
	public void setHTTPResponseBody(String responseBody) {
		mHTTPResponseBody = responseBody;
	}

	/**
	 * Get the HTTP response body
	 */
	public Map<String, List<String>> getHTTPResponseHeaders() {
		return mHTTPResponseHeaders;
	}
	
	/**
	 * Get a single HTTP header 
	 * (the first if multiple were sent) or null if not present
	 */
	public String getHTTPResponseHeader(String name) {
		List<String> headers = mHTTPResponseHeaders.get(name);
		if (headers != null && headers.size()>0) {
			return headers.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Set the HTTP response body
	 */
	public void setHTTPResponseHeaders(Map<String, List<String>> headers) {
		mHTTPResponseHeaders = headers;		
	}
	
	/**
	 * Extract the response's 'WWw-Authenticate' header type. 
	 * @return header type or null if not set
	 */
	public String getAuthenticationType() {
		List<String> authHeaderList = mHTTPResponseHeaders.get(HttpConstants.HEADER_WWW_AUTHENTICATE);
		if (authHeaderList == null) return null;
		String authHeader = authHeaderList.get(0);
		return authHeader.split(" ")[0];
	}
	
	/**
	 * Extract the response's 'WWw-Authenticate' header realm
	 * @return header realm or null if not set
	 */
	public String getAuthenticationRealm() {
		List<String> authHeaderList = mHTTPResponseHeaders.get(HttpConstants.HEADER_WWW_AUTHENTICATE);
		if (authHeaderList == null) return null;
		String authHeader = authHeaderList.get(0);
		String realm = authHeader.substring(authHeader.indexOf("realm=\"")+7, authHeader.length()-1);
		return realm;
	}
	
	/**
	 * Get the (global) CookieManager.
	 * @return
	 */
	public CookieManager getCookieManager() {
		return ((CookieManager) CookieManager.getDefault());
	}
	
	/**
	 * Get the cookie with the given name.
	 * 
	 * @param name
	 * @return the sought cookie or null if not found (or expired)
	 */
	public HttpCookie getCookie(String name) {
		CookieStore cookieStore = getCookieManager().getCookieStore();
		for (HttpCookie cookie : cookieStore.getCookies()) {
			if (cookie.getName().equals(name)) {
				return cookie;
			}
		}
		return null;
	}

}
