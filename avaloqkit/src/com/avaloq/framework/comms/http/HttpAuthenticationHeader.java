package com.avaloq.framework.comms.http;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.http.HeaderElement;
import org.apache.http.message.BasicHeaderValueParser;

@Deprecated
public class HttpAuthenticationHeader {
	private String mScheme = null;
	private Map<String, String> mParameters = new HashMap<String, String>();

	//private static final String LOG_TAG = "HttpAuthenticationHeader";

	public HttpAuthenticationHeader(String header) throws IllegalArgumentException {
		if (header == null) {
			throw new IllegalArgumentException(
					"WWW-Authentication header is null");
		}
		StringTokenizer headerTokenizer = new StringTokenizer(header);
		// get the scheme
		if (headerTokenizer.hasMoreTokens()) {
			mScheme = headerTokenizer.nextToken();
		} else {
			throw new IllegalArgumentException(
					"Malformed WWW-Authentication header: no scheme indicated");
		}
		// get some attributes
		if(headerTokenizer.hasMoreTokens()) {
			String rest = headerTokenizer.nextToken();
			HeaderElement[] elements = BasicHeaderValueParser.parseElements(rest, null);
			for (HeaderElement e: elements) {
				//Log.v(LOG_TAG, "Header element: " + e.getName() + "=" + e.getValue());
				mParameters.put(e.getName(), e.getValue());
			}
		}
	}

	public String getScheme() {
		return mScheme;
	}

	public Map<String, String> getParameters() {
		return mParameters;
	}
}
