package com.avaloq.framework.comms.http.json;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.avaloq.afs.server.bsp.client.ws.ChartPeriod;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.HttpConstants;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The abstract JSON type of a http request. 
 * @param <ResponseType> The matching response type
 */
public abstract class AbstractJsonHTTPRequest<ResponseType> extends AbstractHTTPServerRequest<ResponseType> {

	// Set the headers for JSON WS requests 
	static {
		HTTP_HEADERS.put("Content-Type", "application/json;charset=UTF-8");
		HTTP_HEADERS.put("Accept", "application/json");
		HTTP_HEADERS.put("Accept-Charset", "UTF-8");			
	}
	
	private final String mMethodName;
	private final Map<String,Object> mParams;
	private final Class<ResponseType> mClass;

	/**
	 * Constructor
	 * 
	 * @param aMethodName
	 * @param rse
	 * @param aParams
	 * @param aClass
	 */
	public AbstractJsonHTTPRequest(final String aMethodName, final RequestStateEvent<?> rse, final Map<String,Object> aParams, final Class<ResponseType> aClass) {
		super(rse);
		mMethodName = aMethodName;
		mParams = (aParams == null) ? new HashMap<String,Object>() : aParams;
		mClass = aClass;
	}

	/**
	 * Get the http headers map
	 * @return HTTP_HEADERS 
	 */
	@Override
	public Map<String, String> getHttpHeaders() {
		return HTTP_HEADERS;
	}
	
	class BooleanSerializer implements JsonSerializer<Boolean>, JsonDeserializer<Boolean> {

		@Override
		public JsonElement serialize(Boolean cond, Type t, JsonSerializationContext c) {
			return new JsonPrimitive(cond ? 1 : 0);
		}

		@Override
		public Boolean deserialize(JsonElement el, Type t, JsonDeserializationContext c) throws JsonParseException {
			return el.getAsInt() == 1 ? true : false;
		}
		
	};
	
	class DateSerializer implements JsonSerializer<Date>{

		@Override
		public JsonElement serialize(Date arg0, Type arg1,JsonSerializationContext arg2) {
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ");
			StringBuffer sb = new StringBuffer(df.format(arg0));
			
			if (sb.charAt(sb.length()-3) == ':'){
				return new JsonPrimitive(sb.toString());
			}else{
				return new JsonPrimitive(sb.insert(sb.length()-2, ':').toString());
			}			
		}
		
	}
	
	@Override
	public String getRequestBody() {
		
		

		Map<String,Object> requestMap = new HashMap<String,Object>();

		requestMap.put("type", "jsonwsp/request");
		requestMap.put("version", this.getWebserviceVersion());
		requestMap.put("methodname", this.mMethodName);
		requestMap.put("args", this.mParams);

		// FIXME The TypeAdapter is just a hack for the proof-of-concept.
		// .registerTypeAdapter(Boolean.class, new BooleanSerializer())
		return new GsonBuilder().serializeNulls().registerTypeAdapter(Date.class, new DateSerializer()).registerTypeAdapter(ChartPeriod.class, new JsonSerializer<ChartPeriod>() {
			@Override
			public JsonElement serialize(ChartPeriod src, Type typeOfSrc, JsonSerializationContext context) {
				String[] words = src.toString().split("_");
				StringBuffer buf = new StringBuffer();
				for(int i = 0; i < words.length; i++) {
					buf.append(cap(words[i]));
				}
				return new JsonPrimitive(buf.toString());
			}
			private String cap(String str) {
				return str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase();
			}
		}).create().toJson(requestMap);
	}

	@Override
	public JsonHTTPResponse<ResponseType> createEmptyResponseObject() {
		return new JsonHTTPResponse<ResponseType>(mClass);
	}

	@Override
	public String getRequestMethod() {		
		return HttpConstants.METHOD_POST;
	}

	/**
	 * Returns the Version of this WebService
	 * @return The Version of this WebService
	 * 
	 * FIXME: Remove or move down (also from code generator), as this is not needed here (who says it is a Webservice?)
	 */
	@Deprecated
	public abstract String getWebserviceVersion();

}
