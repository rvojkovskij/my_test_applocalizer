package com.avaloq.framework.comms.http.json;

import java.util.Map;

import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

/**
 * JSON implementation of a http server response handler.
 *
 * @param <ResponseType> The server response type
 */
public class JsonHTTPResponse<ResponseType> extends AbstractHTTPServerResponse<ResponseType> {

	private Class<ResponseType> mClass;

	public JsonHTTPResponse(Class<ResponseType> aClass) {		
		mClass = aClass;
	}

	/**
	 * Creaet the response object using json deserialisation.
	 * (requires Gson)
	 * 
	 * @return the built response object
	 */
	@Override
	public ResponseType onParseServerResponseData() throws IllegalArgumentException {

		Gson gson = new GsonBuilder()
						.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
						.create();
		Map<?, ?> proxyMap;
		try {
			proxyMap = gson.fromJson(getHTTPResponseBody(), Map.class);
		} catch (JsonSyntaxException e) {
			throw new IllegalArgumentException("Failed to deserialize http response into json: " + e.getMessage());			
		}
		if (proxyMap == null) {
			throw new IllegalArgumentException("Failed to deserialize http response into json");
		}
		Object result = proxyMap.get("result");
		if (result == null){
			throw new IllegalArgumentException("Result is empty");
		}
		String resultJson = gson.toJson(result);
		ResponseType responseObject = gson.fromJson(resultJson, mClass);
		return responseObject;
	}

}
