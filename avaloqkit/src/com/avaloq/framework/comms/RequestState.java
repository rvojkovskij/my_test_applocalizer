package com.avaloq.framework.comms;

/**
 * The available server request states.
 */
public enum RequestState {
	INITIALIZING,
	QUEUED,
	EXECUTING,
	PENDING_AUTHENTICATION,
	COMPLETED,
	FAILED	
}
