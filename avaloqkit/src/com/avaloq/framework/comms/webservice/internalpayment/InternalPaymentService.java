package com.avaloq.framework.comms.webservice.internalpayment;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class InternalPaymentService {

	private InternalPaymentService() {
	}

	
	public static InternalPaymentRequest getPayment( Long paymentOrderId,  RequestStateEvent<InternalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new InternalPaymentRequest("getPayment", rse, params);
	}

	
	public static InternalPaymentRequest savePayment( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentOrderTO paymentOrder,  RequestStateEvent<InternalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternalPaymentRequest("savePayment", rse, params);
	}

	
	public static PaymentCurrenciesRequest getPaymentCurrencies( RequestStateEvent<PaymentCurrenciesRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentCurrenciesRequest("getPaymentCurrencies", rse, params);
	}

	
	public static InternalStandingPaymentRequest saveStandingPayment( com.avaloq.afs.aggregation.to.payment.internal.InternalStandingOrderTO standingOrder,  RequestStateEvent<InternalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternalStandingPaymentRequest("saveStandingPayment", rse, params);
	}

	
	public static InternalStandingPaymentRequest getStandingPayment( Long paymentOrderId,  RequestStateEvent<InternalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new InternalStandingPaymentRequest("getStandingPayment", rse, params);
	}

	
	public static InternalStandingPaymentRequest verifyStandingPayment( com.avaloq.afs.aggregation.to.payment.internal.InternalStandingOrderTO standingOrder,  RequestStateEvent<InternalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternalStandingPaymentRequest("verifyStandingPayment", rse, params);
	}

	
	public static InternalPaymentTemplateRequest verifyPaymentTemplate( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternalPaymentTemplateRequest("verifyPaymentTemplate", rse, params);
	}

	
	public static InternalPaymentRequest verifyPayment( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentOrderTO paymentOrder,  RequestStateEvent<InternalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternalPaymentRequest("verifyPayment", rse, params);
	}

	
	public static InternalPaymentRequest submitPayment( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentOrderTO paymentOrder,  RequestStateEvent<InternalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternalPaymentRequest("submitPayment", rse, params);
	}

	
	public static PaymentDefaultsRequest getDefaults( RequestStateEvent<PaymentDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentDefaultsRequest("getDefaults", rse, params);
	}

	
	public static InternalPaymentTemplateRequest submitPaymentTemplate( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternalPaymentTemplateRequest("submitPaymentTemplate", rse, params);
	}

	
	public static InternalPaymentTemplateRequest getPaymentTemplate( Long paymentTemplateId,  RequestStateEvent<InternalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new InternalPaymentTemplateRequest("getPaymentTemplate", rse, params);
	}

	
	public static InternalPaymentTemplateRequest savePaymentTemplate( com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternalPaymentTemplateRequest("savePaymentTemplate", rse, params);
	}

	
	public static InternalStandingPaymentRequest submitStandingPayment( com.avaloq.afs.aggregation.to.payment.internal.InternalStandingOrderTO standingOrder,  RequestStateEvent<InternalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternalStandingPaymentRequest("submitStandingPayment", rse, params);
	}

	
	public static EndOfDaySwitchRequest getEndOfDaySwitch( RequestStateEvent<EndOfDaySwitchRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new EndOfDaySwitchRequest("getEndOfDaySwitch", rse, params);
	}

	
	public static PaymentHolidaysRequest getPaymentHolidays( Long countries,  RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("countries", countries);
		return new PaymentHolidaysRequest("getPaymentHolidays", rse, params);
	}

}