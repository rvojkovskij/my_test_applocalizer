package com.avaloq.framework.comms.webservice.internalpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.PaymentDefaultsResult;

/**
 * @author jsonwsp2java
 */
public final class PaymentDefaultsRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.PaymentDefaultsResult> {

	PaymentDefaultsRequest(final String aMethodName, final RequestStateEvent<PaymentDefaultsRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.PaymentDefaultsResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "InternalPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}