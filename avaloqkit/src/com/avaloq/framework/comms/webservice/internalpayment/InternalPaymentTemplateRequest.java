package com.avaloq.framework.comms.webservice.internalpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateResult;

/**
 * @author jsonwsp2java
 */
public final class InternalPaymentTemplateRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateResult> {

	InternalPaymentTemplateRequest(final String aMethodName, final RequestStateEvent<InternalPaymentTemplateRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "InternalPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}