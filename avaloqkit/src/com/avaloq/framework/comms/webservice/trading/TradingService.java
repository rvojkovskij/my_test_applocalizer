package com.avaloq.framework.comms.webservice.trading;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class TradingService {

	private TradingService() {
	}

	
	public static TradingDefaultsRequest getTradingDefaults( RequestStateEvent<TradingDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new TradingDefaultsRequest("getTradingDefaults", rse, params);
	}

	
	public static MarketDataRequest getMarketData( com.avaloq.afs.server.bsp.client.ws.MarketDataQueryTO marketDataQuery,  RequestStateEvent<MarketDataRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("marketDataQuery", marketDataQuery);
		return new MarketDataRequest("getMarketData", rse, params);
	}

	
	public static StexOrdersRequest getOrderList( com.avaloq.afs.server.bsp.client.ws.StexOrderQueryTO stexOrderQuery,  Long startIndex,  Long size,  RequestStateEvent<StexOrdersRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("stexOrderQuery", stexOrderQuery);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new StexOrdersRequest("getOrderList", rse, params);
	}

	
	public static TransactionsRequest findTransactionList( com.avaloq.afs.server.bsp.client.ws.TransactionListQueryTO transactionListQuery,  RequestStateEvent<TransactionsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionListQuery", transactionListQuery);
		return new TransactionsRequest("findTransactionList", rse, params);
	}

	
	public static ListingListRequest getInstrumentListingList( Long instrumentId,  RequestStateEvent<ListingListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentId", instrumentId);
		return new ListingListRequest("getInstrumentListingList", rse, params);
	}

	
	public static StexOrderRequest requestOrderCancelation( Long orderId,  Long orderVersionId,  RequestStateEvent<StexOrderRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("orderId", orderId);
		params.put("orderVersionId", orderVersionId);
		return new StexOrderRequest("requestOrderCancelation", rse, params);
	}

	
	public static IdListRequest getTradablePortfolioIdList( RequestStateEvent<IdListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new IdListRequest("getTradablePortfolioIdList", rse, params);
	}

	
	public static MoneyAccountsRequest getTradingMoneyAccountList( RequestStateEvent<MoneyAccountsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new MoneyAccountsRequest("getTradingMoneyAccountList", rse, params);
	}

	
	public static StexOrderRequest submitOrder( com.avaloq.afs.server.bsp.client.ws.StexOrderTO order,  RequestStateEvent<StexOrderRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("order", order);
		return new StexOrderRequest("submitOrder", rse, params);
	}

	
	public static PositionsRequest findPositionList( com.avaloq.afs.server.bsp.client.ws.BankingPositionListQueryTO bankingPositionListQuery,  Long startIndex,  Long size,  RequestStateEvent<PositionsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("bankingPositionListQuery", bankingPositionListQuery);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PositionsRequest("findPositionList", rse, params);
	}

	
	public static BusinessPartnersRequest getBusinessPartners( RequestStateEvent<BusinessPartnersRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new BusinessPartnersRequest("getBusinessPartners", rse, params);
	}

	
	public static Request verifyOrder( com.avaloq.afs.server.bsp.client.ws.StexOrderTO order,  RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("order", order);
		return new Request("verifyOrder", rse, params);
	}

	
	public static IdListRequest getTradablePortfolioForInstrumentSellIdList( Long instrumentId,  RequestStateEvent<IdListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentId", instrumentId);
		return new IdListRequest("getTradablePortfolioForInstrumentSellIdList", rse, params);
	}

}