package com.avaloq.framework.comms.webservice.trading;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.trading.StexOrderResult;

/**
 * @author jsonwsp2java
 */
public final class StexOrderRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.trading.StexOrderResult> {

	StexOrderRequest(final String aMethodName, final RequestStateEvent<StexOrderRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.trading.StexOrderResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "TradingService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}