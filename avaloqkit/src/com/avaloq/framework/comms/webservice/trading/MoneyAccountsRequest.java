package com.avaloq.framework.comms.webservice.trading;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.wealth.MoneyAccountsResult;

/**
 * @author jsonwsp2java
 */
public final class MoneyAccountsRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.wealth.MoneyAccountsResult> {

	MoneyAccountsRequest(final String aMethodName, final RequestStateEvent<MoneyAccountsRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.wealth.MoneyAccountsResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "TradingService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}