package com.avaloq.framework.comms.webservice.directdebitmandateoverview;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.DirectDebitMandateListResult;

/**
 * @author jsonwsp2java
 */
public final class DirectDebitMandateListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.DirectDebitMandateListResult> {

	DirectDebitMandateListRequest(final String aMethodName, final RequestStateEvent<DirectDebitMandateListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.DirectDebitMandateListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DirectDebitMandateOverviewService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}