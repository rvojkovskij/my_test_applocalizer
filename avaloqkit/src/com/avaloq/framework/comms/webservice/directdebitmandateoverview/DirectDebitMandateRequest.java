package com.avaloq.framework.comms.webservice.directdebitmandateoverview;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.DirectDebitMandateResult;

/**
 * @author jsonwsp2java
 */
public final class DirectDebitMandateRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.DirectDebitMandateResult> {

	DirectDebitMandateRequest(final String aMethodName, final RequestStateEvent<DirectDebitMandateRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.DirectDebitMandateResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DirectDebitMandateOverviewService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}