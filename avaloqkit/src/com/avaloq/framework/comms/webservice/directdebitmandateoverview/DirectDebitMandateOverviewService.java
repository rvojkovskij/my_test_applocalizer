package com.avaloq.framework.comms.webservice.directdebitmandateoverview;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class DirectDebitMandateOverviewService {

	private DirectDebitMandateOverviewService() {
	}

	
	public static DirectDebitMandateListRequest getDirectDebitMandates( com.avaloq.afs.server.bsp.client.ws.DirectDebitMandateQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<DirectDebitMandateListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new DirectDebitMandateListRequest("getDirectDebitMandates", rse, params);
	}

	
	public static DirectDebitMandateRequest cancelDirectDebitMandates( Long mandateId,  RequestStateEvent<DirectDebitMandateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("mandateId", mandateId);
		return new DirectDebitMandateRequest("cancelDirectDebitMandates", rse, params);
	}

}