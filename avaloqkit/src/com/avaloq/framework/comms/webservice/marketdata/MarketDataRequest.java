package com.avaloq.framework.comms.webservice.marketdata;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.marketdata.MarketDataResult;

/**
 * @author jsonwsp2java
 */
public final class MarketDataRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.marketdata.MarketDataResult> {

	MarketDataRequest(final String aMethodName, final RequestStateEvent<MarketDataRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.marketdata.MarketDataResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "MarketDataService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}