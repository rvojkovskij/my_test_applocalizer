package com.avaloq.framework.comms.webservice.marketdata.model;

/**
 * @author jsonwsp2java
 */
public class MarketDataUrlResult {

	/**
	 * Generated Members
	 */
	private java.util.Date dateTime;
	
	private com.avaloq.afs.server.bsp.client.ws.AlertsTO alerts;
	
	private String marketDataUrl;
	
	private java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> notificationList;
	
	private java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> errorNotificationList;
	

	/**
	 * Generated Getters and Setters 
	 */
	public java.util.Date getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(final java.util.Date dateTime) {
		this.dateTime = dateTime;
	}
	
	public com.avaloq.afs.server.bsp.client.ws.AlertsTO getAlerts() {
		return this.alerts;
	}

	public void setAlerts(final com.avaloq.afs.server.bsp.client.ws.AlertsTO alerts) {
		this.alerts = alerts;
	}
	
	public String getMarketDataUrl() {
		return this.marketDataUrl;
	}

	public void setMarketDataUrl(final String marketDataUrl) {
		this.marketDataUrl = marketDataUrl;
	}
	
	public java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> getNotificationList() {
		return this.notificationList;
	}

	public void setNotificationList(final java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> notificationList) {
		this.notificationList = notificationList;
	}
	
	public java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> getErrorNotificationList() {
		return this.errorNotificationList;
	}

	public void setErrorNotificationList(final java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> errorNotificationList) {
		this.errorNotificationList = errorNotificationList;
	}
	

}