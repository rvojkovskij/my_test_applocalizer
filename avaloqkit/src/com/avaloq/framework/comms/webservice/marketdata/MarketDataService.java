package com.avaloq.framework.comms.webservice.marketdata;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class MarketDataService {

	private MarketDataService() {
	}

	
	public static MarketDataRequest getMarketData( com.avaloq.afs.server.bsp.client.ws.MarketDataQueryTO marketDataQuery,  RequestStateEvent<MarketDataRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("marketDataQuery", marketDataQuery);
		return new MarketDataRequest("getMarketData", rse, params);
	}

	
	public static MarketDataChartRequest getMarketDataChart( com.avaloq.afs.server.bsp.client.ws.MarketDataChartQueryTO marketDataChartQuery,  RequestStateEvent<MarketDataChartRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("marketDataChartQuery", marketDataChartQuery);
		return new MarketDataChartRequest("getMarketDataChart", rse, params);
	}

	
	public static MarketDataUrlRequest getMarketDataFallbackUrl( Long arg0,  Long arg1,  Long arg2,  RequestStateEvent<MarketDataUrlRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("arg0", arg0);
		params.put("arg1", arg1);
		params.put("arg2", arg2);
		return new MarketDataUrlRequest("getMarketDataFallbackUrl", rse, params);
	}

	
	public static InstrumentRequest getInstrument( Long instrumentId,  RequestStateEvent<InstrumentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentId", instrumentId);
		return new InstrumentRequest("getInstrument", rse, params);
	}

	
	public static InstrumentsRequest getInstruments( com.avaloq.afs.server.bsp.client.ws.InstrumentQueryTO instrumentQuery,  Long startIndex,  Long size,  RequestStateEvent<InstrumentsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentQuery", instrumentQuery);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new InstrumentsRequest("getInstruments", rse, params);
	}

}