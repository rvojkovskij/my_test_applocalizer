package com.avaloq.framework.comms.webservice.marketdata;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.marketdata.MarketDataUrlResult;

/**
 * @author jsonwsp2java
 */
public final class MarketDataUrlRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.marketdata.MarketDataUrlResult> {

	MarketDataUrlRequest(final String aMethodName, final RequestStateEvent<MarketDataUrlRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.marketdata.MarketDataUrlResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "MarketDataService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}