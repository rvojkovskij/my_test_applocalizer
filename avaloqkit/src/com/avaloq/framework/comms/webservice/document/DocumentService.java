package com.avaloq.framework.comms.webservice.document;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class DocumentService {

	private DocumentService() {
	}

	
	public static DocumentDescriptionListRequest getDocumentDescriptions( Long documentCategoryId,  RequestStateEvent<DocumentDescriptionListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("documentCategoryId", documentCategoryId);
		return new DocumentDescriptionListRequest("getDocumentDescriptions", rse, params);
	}

	
	public static DocumentCategoryListRequest getDocumentCategories( RequestStateEvent<DocumentCategoryListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new DocumentCategoryListRequest("getDocumentCategories", rse, params);
	}

}