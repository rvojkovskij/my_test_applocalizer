package com.avaloq.framework.comms.webservice.document;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.documents.DocumentDescriptionListResult;

/**
 * @author jsonwsp2java
 */
public final class DocumentDescriptionListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.documents.DocumentDescriptionListResult> {

	DocumentDescriptionListRequest(final String aMethodName, final RequestStateEvent<DocumentDescriptionListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.documents.DocumentDescriptionListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DocumentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}