package com.avaloq.framework.comms.webservice.document;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.documents.DocumentCategoryListResult;

/**
 * @author jsonwsp2java
 */
public final class DocumentCategoryListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.documents.DocumentCategoryListResult> {

	DocumentCategoryListRequest(final String aMethodName, final RequestStateEvent<DocumentCategoryListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.documents.DocumentCategoryListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DocumentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}