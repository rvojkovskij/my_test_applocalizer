package com.avaloq.framework.comms.webservice.swissredpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentResult;

/**
 * @author jsonwsp2java
 */
public final class SwissRedPaymentRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentResult> {

	SwissRedPaymentRequest(final String aMethodName, final RequestStateEvent<SwissRedPaymentRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "SwissRedPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}