package com.avaloq.framework.comms.webservice.swissredpayment;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class SwissRedPaymentService {

	private SwissRedPaymentService() {
	}

	
	public static SwissRedPaymentRequest getPayment( Long paymentOrderId,  RequestStateEvent<SwissRedPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new SwissRedPaymentRequest("getPayment", rse, params);
	}

	
	public static SwissRedPaymentRequest savePayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentOrderTO paymentOrder,  RequestStateEvent<SwissRedPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissRedPaymentRequest("savePayment", rse, params);
	}

	
	public static PaymentCurrenciesRequest getPaymentCurrencies( RequestStateEvent<PaymentCurrenciesRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentCurrenciesRequest("getPaymentCurrencies", rse, params);
	}

	
	public static SwissRedStandingPaymentRequest saveStandingPayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedStandingOrderTO standingOrder,  RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissRedStandingPaymentRequest("saveStandingPayment", rse, params);
	}

	
	public static SwissRedStandingPaymentRequest getStandingPayment( Long paymentOrderId,  RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new SwissRedStandingPaymentRequest("getStandingPayment", rse, params);
	}

	
	public static SwissRedStandingPaymentRequest verifyStandingPayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedStandingOrderTO standingOrder,  RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissRedStandingPaymentRequest("verifyStandingPayment", rse, params);
	}

	
	public static SwissPostCheckInfoRequest getPostCheckInfo( String postCheckAccount,  RequestStateEvent<SwissPostCheckInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("postCheckAccount", postCheckAccount);
		return new SwissPostCheckInfoRequest("getPostCheckInfo", rse, params);
	}

	
	public static SwissRedPaymentTemplateRequest verifyPaymentTemplate( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissRedPaymentTemplateRequest("verifyPaymentTemplate", rse, params);
	}

	
	public static SwissRedPaymentRequest verifyPayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentOrderTO paymentOrder,  RequestStateEvent<SwissRedPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissRedPaymentRequest("verifyPayment", rse, params);
	}

	
	public static SwissRedPaymentRequest submitPayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentOrderTO paymentOrder,  RequestStateEvent<SwissRedPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissRedPaymentRequest("submitPayment", rse, params);
	}

	
	public static SwissPaymentDefaultsRequest getDefaults( RequestStateEvent<SwissPaymentDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new SwissPaymentDefaultsRequest("getDefaults", rse, params);
	}

	
	public static SwissRedPaymentTemplateRequest submitPaymentTemplate( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissRedPaymentTemplateRequest("submitPaymentTemplate", rse, params);
	}

	
	public static SwissRedPaymentTemplateRequest getPaymentTemplate( Long paymentTemplateId,  RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new SwissRedPaymentTemplateRequest("getPaymentTemplate", rse, params);
	}

	
	public static SwissRedPaymentTemplateRequest savePaymentTemplate( com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissRedPaymentTemplateRequest("savePaymentTemplate", rse, params);
	}

	
	public static SwissRedStandingPaymentRequest submitStandingPayment( com.avaloq.afs.aggregation.to.payment.red.SwissRedStandingOrderTO standingOrder,  RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissRedStandingPaymentRequest("submitStandingPayment", rse, params);
	}

	
	public static EndOfDaySwitchRequest getEndOfDaySwitch( RequestStateEvent<EndOfDaySwitchRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new EndOfDaySwitchRequest("getEndOfDaySwitch", rse, params);
	}

	
	public static PaymentHolidaysRequest getPaymentHolidays( Long countries,  RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("countries", countries);
		return new PaymentHolidaysRequest("getPaymentHolidays", rse, params);
	}

}