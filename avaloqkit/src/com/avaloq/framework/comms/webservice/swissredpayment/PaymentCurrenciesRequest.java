package com.avaloq.framework.comms.webservice.swissredpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.PaymentCurrenciesResult;

/**
 * @author jsonwsp2java
 */
public final class PaymentCurrenciesRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.PaymentCurrenciesResult> {

	PaymentCurrenciesRequest(final String aMethodName, final RequestStateEvent<PaymentCurrenciesRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.PaymentCurrenciesResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "SwissRedPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}