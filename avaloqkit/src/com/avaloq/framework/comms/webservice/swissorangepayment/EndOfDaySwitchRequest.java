package com.avaloq.framework.comms.webservice.swissorangepayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.EndOfDaySwitchResult;

/**
 * @author jsonwsp2java
 */
public final class EndOfDaySwitchRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.EndOfDaySwitchResult> {

	EndOfDaySwitchRequest(final String aMethodName, final RequestStateEvent<EndOfDaySwitchRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.EndOfDaySwitchResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "SwissOrangePaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}