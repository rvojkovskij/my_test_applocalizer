package com.avaloq.framework.comms.webservice.swissorangepayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentResult;

/**
 * @author jsonwsp2java
 */
public final class SwissOrangePaymentRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentResult> {

	SwissOrangePaymentRequest(final String aMethodName, final RequestStateEvent<SwissOrangePaymentRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "SwissOrangePaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}