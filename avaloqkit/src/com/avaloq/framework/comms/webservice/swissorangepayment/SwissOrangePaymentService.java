package com.avaloq.framework.comms.webservice.swissorangepayment;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class SwissOrangePaymentService {

	private SwissOrangePaymentService() {
	}

	
	public static SwissOrangePaymentRequest getPayment( Long paymentOrderId,  RequestStateEvent<SwissOrangePaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new SwissOrangePaymentRequest("getPayment", rse, params);
	}

	
	public static PaymentCurrenciesRequest getPaymentCurrencies( RequestStateEvent<PaymentCurrenciesRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentCurrenciesRequest("getPaymentCurrencies", rse, params);
	}

	
	public static SwissOrangePaymentRequest savePayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentOrderTO paymentOrder,  RequestStateEvent<SwissOrangePaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissOrangePaymentRequest("savePayment", rse, params);
	}

	
	public static SwissOrangeStandingPaymentRequest saveStandingPayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangeStandingOrderTO standingOrder,  RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissOrangeStandingPaymentRequest("saveStandingPayment", rse, params);
	}

	
	public static SwissOrangeStandingPaymentRequest getStandingPayment( Long paymentOrderId,  RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new SwissOrangeStandingPaymentRequest("getStandingPayment", rse, params);
	}

	
	public static SwissOrangeStandingPaymentRequest verifyStandingPayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangeStandingOrderTO standingOrder,  RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissOrangeStandingPaymentRequest("verifyStandingPayment", rse, params);
	}

	
	public static SwissPostCheckInfoRequest getPostCheckInfo( String postCheckAccount,  RequestStateEvent<SwissPostCheckInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("postCheckAccount", postCheckAccount);
		return new SwissPostCheckInfoRequest("getPostCheckInfo", rse, params);
	}

	
	public static SwissOrangePaymentTemplateRequest verifyPaymentTemplate( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissOrangePaymentTemplateRequest("verifyPaymentTemplate", rse, params);
	}

	
	public static SwissOrangePaymentRequest verifyPayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentOrderTO paymentOrder,  RequestStateEvent<SwissOrangePaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissOrangePaymentRequest("verifyPayment", rse, params);
	}

	
	public static SwissOrangePaymentRequest submitPayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentOrderTO paymentOrder,  RequestStateEvent<SwissOrangePaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new SwissOrangePaymentRequest("submitPayment", rse, params);
	}

	
	public static SwissPaymentDefaultsRequest getDefaults( RequestStateEvent<SwissPaymentDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new SwissPaymentDefaultsRequest("getDefaults", rse, params);
	}

	
	public static SwissOrangePaymentTemplateRequest submitPaymentTemplate( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissOrangePaymentTemplateRequest("submitPaymentTemplate", rse, params);
	}

	
	public static SwissOrangePaymentTemplateRequest getPaymentTemplate( Long paymentTemplateId,  RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new SwissOrangePaymentTemplateRequest("getPaymentTemplate", rse, params);
	}

	
	public static SwissOrangePaymentTemplateRequest savePaymentTemplate( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentTemplateTO paymentTemplate,  RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new SwissOrangePaymentTemplateRequest("savePaymentTemplate", rse, params);
	}

	
	public static SwissOrangeStandingPaymentRequest submitStandingPayment( com.avaloq.afs.aggregation.to.payment.orange.SwissOrangeStandingOrderTO standingOrder,  RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new SwissOrangeStandingPaymentRequest("submitStandingPayment", rse, params);
	}

	
	public static EndOfDaySwitchRequest getEndOfDaySwitch( RequestStateEvent<EndOfDaySwitchRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new EndOfDaySwitchRequest("getEndOfDaySwitch", rse, params);
	}

	
	public static PaymentHolidaysRequest getPaymentHolidays( Long countries,  RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("countries", countries);
		return new PaymentHolidaysRequest("getPaymentHolidays", rse, params);
	}

}