package com.avaloq.framework.comms.webservice.domesticpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.BankInfoListResult;

/**
 * @author jsonwsp2java
 */
public final class BankInfoListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.BankInfoListResult> {

	BankInfoListRequest(final String aMethodName, final RequestStateEvent<BankInfoListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.BankInfoListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DomesticPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}