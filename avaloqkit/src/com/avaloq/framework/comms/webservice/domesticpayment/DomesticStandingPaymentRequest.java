package com.avaloq.framework.comms.webservice.domesticpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingPaymentResult;

/**
 * @author jsonwsp2java
 */
public final class DomesticStandingPaymentRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingPaymentResult> {

	DomesticStandingPaymentRequest(final String aMethodName, final RequestStateEvent<DomesticStandingPaymentRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingPaymentResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "DomesticPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}