package com.avaloq.framework.comms.webservice.domesticpayment;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class DomesticPaymentService {

	private DomesticPaymentService() {
	}

	
	public static DomesticPaymentRequest getPayment( Long paymentOrderId,  RequestStateEvent<DomesticPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new DomesticPaymentRequest("getPayment", rse, params);
	}

	
	public static PaymentCurrenciesRequest getPaymentCurrencies( RequestStateEvent<PaymentCurrenciesRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentCurrenciesRequest("getPaymentCurrencies", rse, params);
	}

	
	public static DomesticPaymentRequest savePayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentOrderTO paymentOrder,  RequestStateEvent<DomesticPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new DomesticPaymentRequest("savePayment", rse, params);
	}

	
	public static DomesticStandingPaymentRequest saveStandingPayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingOrderTO standingOrder,  RequestStateEvent<DomesticStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new DomesticStandingPaymentRequest("saveStandingPayment", rse, params);
	}

	
	public static DomesticStandingPaymentRequest getStandingPayment( Long paymentOrderId,  RequestStateEvent<DomesticStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new DomesticStandingPaymentRequest("getStandingPayment", rse, params);
	}

	
	public static BankInfoListRequest findDomesticBankInfos( com.avaloq.afs.server.bsp.client.ws.BankInfoQueryTO query,  RequestStateEvent<BankInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		return new BankInfoListRequest("findDomesticBankInfos", rse, params);
	}

	
	public static BankInfoListRequest findDomesticBankInfoByNtnl( String ntnlNumber,  RequestStateEvent<BankInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("ntnlNumber", ntnlNumber);
		return new BankInfoListRequest("findDomesticBankInfoByNtnl", rse, params);
	}

	
	public static BankInfoRequest findDomesticBankInfoByIban( String iban,  RequestStateEvent<BankInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("iban", iban);
		return new BankInfoRequest("findDomesticBankInfoByIban", rse, params);
	}

	
	public static DomesticStandingPaymentRequest verifyStandingPayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingOrderTO standingOrder,  RequestStateEvent<DomesticStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new DomesticStandingPaymentRequest("verifyStandingPayment", rse, params);
	}

	
	public static DomesticPaymentTemplateRequest verifyPaymentTemplate( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentTemplateTO paymentTemplate,  RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new DomesticPaymentTemplateRequest("verifyPaymentTemplate", rse, params);
	}

	
	public static CountryListRequest getDomesticPaymentCountries( RequestStateEvent<CountryListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new CountryListRequest("getDomesticPaymentCountries", rse, params);
	}

	
	public static PaymentHolidaysRequest getDomesticPaymentHolidays( RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentHolidaysRequest("getDomesticPaymentHolidays", rse, params);
	}

	
	public static DomesticPaymentRequest verifyPayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentOrderTO paymentOrder,  RequestStateEvent<DomesticPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new DomesticPaymentRequest("verifyPayment", rse, params);
	}

	
	public static DomesticPaymentRequest submitPayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentOrderTO paymentOrder,  RequestStateEvent<DomesticPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new DomesticPaymentRequest("submitPayment", rse, params);
	}

	
	public static DomesticPaymentDefaultsRequest getDefaults( RequestStateEvent<DomesticPaymentDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new DomesticPaymentDefaultsRequest("getDefaults", rse, params);
	}

	
	public static DomesticPaymentTemplateRequest getPaymentTemplate( Long paymentTemplateId,  RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new DomesticPaymentTemplateRequest("getPaymentTemplate", rse, params);
	}

	
	public static DomesticPaymentTemplateRequest submitPaymentTemplate( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentTemplateTO paymentTemplate,  RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new DomesticPaymentTemplateRequest("submitPaymentTemplate", rse, params);
	}

	
	public static DomesticPaymentTemplateRequest savePaymentTemplate( com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentTemplateTO paymentTemplate,  RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new DomesticPaymentTemplateRequest("savePaymentTemplate", rse, params);
	}

	
	public static DomesticStandingPaymentRequest submitStandingPayment( com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingOrderTO standingOrder,  RequestStateEvent<DomesticStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new DomesticStandingPaymentRequest("submitStandingPayment", rse, params);
	}

	
	public static EndOfDaySwitchRequest getEndOfDaySwitch( RequestStateEvent<EndOfDaySwitchRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new EndOfDaySwitchRequest("getEndOfDaySwitch", rse, params);
	}

	
	public static PaymentHolidaysRequest getPaymentHolidays( Long countries,  RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("countries", countries);
		return new PaymentHolidaysRequest("getPaymentHolidays", rse, params);
	}

}