package com.avaloq.framework.comms.webservice.usersettings;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.user.settings.UserSettingsGeneralResult;

/**
 * @author jsonwsp2java
 */
public final class UserSettingsGeneralRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.user.settings.UserSettingsGeneralResult> {

	UserSettingsGeneralRequest(final String aMethodName, final RequestStateEvent<UserSettingsGeneralRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.user.settings.UserSettingsGeneralResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "UserSettingsService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}