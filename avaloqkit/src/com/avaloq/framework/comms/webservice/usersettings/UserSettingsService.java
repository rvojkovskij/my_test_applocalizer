package com.avaloq.framework.comms.webservice.usersettings;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class UserSettingsService {

	private UserSettingsService() {
	}

	
	public static UserSettingsRequest updateUserSettings( com.avaloq.afs.server.bsp.client.ws.UserSettingsTO userSettings,  RequestStateEvent<UserSettingsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("userSettings", userSettings);
		return new UserSettingsRequest("updateUserSettings", rse, params);
	}

	
	public static UserSettingsPaymentRequest updatePaymentSettings( com.avaloq.afs.server.bsp.client.ws.PaymentSettingsTO paymentSettings,  RequestStateEvent<UserSettingsPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentSettings", paymentSettings);
		return new UserSettingsPaymentRequest("updatePaymentSettings", rse, params);
	}

	
	public static UserSettingsPaymentRequest updateDefaultDebitMoneyAccount( Long defaultDebitMoneyAccountId,  RequestStateEvent<UserSettingsPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("defaultDebitMoneyAccountId", defaultDebitMoneyAccountId);
		return new UserSettingsPaymentRequest("updateDefaultDebitMoneyAccount", rse, params);
	}

	
	public static UserSettingsTradingRequest updateTradingSettings( com.avaloq.afs.server.bsp.client.ws.TradingSettingsTO tradingSettings,  RequestStateEvent<UserSettingsTradingRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("tradingSettings", tradingSettings);
		return new UserSettingsTradingRequest("updateTradingSettings", rse, params);
	}

	
	public static UserSettingsRequest getUserSettings( RequestStateEvent<UserSettingsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new UserSettingsRequest("getUserSettings", rse, params);
	}

	
	public static UserSettingsGeneralRequest getGeneralSettings( RequestStateEvent<UserSettingsGeneralRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new UserSettingsGeneralRequest("getGeneralSettings", rse, params);
	}

	
	public static UserSettingsTradingRequest getTradingSettings( RequestStateEvent<UserSettingsTradingRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new UserSettingsTradingRequest("getTradingSettings", rse, params);
	}

	
	public static UserSettingsGeneralRequest updateGeneralSettings( com.avaloq.afs.server.bsp.client.ws.GeneralSettingsTO generalSettings,  RequestStateEvent<UserSettingsGeneralRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("generalSettings", generalSettings);
		return new UserSettingsGeneralRequest("updateGeneralSettings", rse, params);
	}

	
	public static UserSettingsPaymentRequest getPaymentSettings( RequestStateEvent<UserSettingsPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new UserSettingsPaymentRequest("getPaymentSettings", rse, params);
	}

}