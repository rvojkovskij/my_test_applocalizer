package com.avaloq.framework.comms.webservice.usersettings;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.user.settings.UserSettingsPaymentResult;

/**
 * @author jsonwsp2java
 */
public final class UserSettingsPaymentRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.user.settings.UserSettingsPaymentResult> {

	UserSettingsPaymentRequest(final String aMethodName, final RequestStateEvent<UserSettingsPaymentRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.user.settings.UserSettingsPaymentResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "UserSettingsService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}