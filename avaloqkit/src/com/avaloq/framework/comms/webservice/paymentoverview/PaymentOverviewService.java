package com.avaloq.framework.comms.webservice.paymentoverview;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class PaymentOverviewService {

	private PaymentOverviewService() {
	}

	
	public static PaymentInfoRequest deleteOpenPayment( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentId,  RequestStateEvent<PaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentId", paymentId);
		return new PaymentInfoRequest("deleteOpenPayment", rse, params);
	}

	
	public static TransactionSigningRequestStandingPaymentInfoRequest approveStandingOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentStandingOrderId,  RequestStateEvent<TransactionSigningRequestStandingPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentStandingOrderId", paymentStandingOrderId);
		return new TransactionSigningRequestStandingPaymentInfoRequest("approveStandingOrder", rse, params);
	}

	
	public static BeneficiaryListRequest getBeneficiaries( com.avaloq.afs.server.bsp.client.ws.BeneficiaryQueryTO query,  RequestStateEvent<BeneficiaryListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		return new BeneficiaryListRequest("getBeneficiaries", rse, params);
	}

	
	public static IbanRequest validateIban( String iban,  RequestStateEvent<IbanRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("iban", iban);
		return new IbanRequest("validateIban", rse, params);
	}

	
	public static StandingOrderPeriodListRequest getStandingOrderPeriods( RequestStateEvent<StandingOrderPeriodListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new StandingOrderPeriodListRequest("getStandingOrderPeriods", rse, params);
	}

	
	public static StandingPaymentInfoRequest deactivateStandingOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentId,  RequestStateEvent<StandingPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentId", paymentId);
		return new StandingPaymentInfoRequest("deactivateStandingOrder", rse, params);
	}

	
	public static TransactionSigningRequestPaymentInfoRequest approvePaymentOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentOrderId,  RequestStateEvent<TransactionSigningRequestPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new TransactionSigningRequestPaymentInfoRequest("approvePaymentOrder", rse, params);
	}

	
	public static TransactionSigningRequestPaymentInfoRequest approvePaymentCordOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentCordOrderId,  RequestStateEvent<TransactionSigningRequestPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentCordOrderId", paymentCordOrderId);
		return new TransactionSigningRequestPaymentInfoRequest("approvePaymentCordOrder", rse, params);
	}

	
	public static StandingPaymentInfoRequest activateStandingOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentId,  RequestStateEvent<StandingPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentId", paymentId);
		return new StandingPaymentInfoRequest("activateStandingOrder", rse, params);
	}

	
	public static PaymentInfoRequest deletePaymentCordOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentCordOrderId,  RequestStateEvent<PaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentCordOrderId", paymentCordOrderId);
		return new PaymentInfoRequest("deletePaymentCordOrder", rse, params);
	}

	
	public static TransactionSigningVerificationRequest verifyPaymentTransactionSigning( com.avaloq.afs.server.bsp.client.ws.TransactionSigningVerifyTO transactionSigningVerify,  RequestStateEvent<TransactionSigningVerificationRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionSigningVerify", transactionSigningVerify);
		return new TransactionSigningVerificationRequest("verifyPaymentTransactionSigning", rse, params);
	}

	
	public static StandingPaymentInfoRequest deleteStandingOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentId,  RequestStateEvent<StandingPaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentId", paymentId);
		return new StandingPaymentInfoRequest("deleteStandingOrder", rse, params);
	}

	
	public static PaymentMaskListRequest getPaymentMaskList( com.avaloq.afs.server.bsp.client.ws.PaymentMaskQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<PaymentMaskListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PaymentMaskListRequest("getPaymentMaskList", rse, params);
	}

	
	public static Request deleteBeneficiary( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO beneficiaryBase,  RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("beneficiaryBase", beneficiaryBase);
		return new Request("deleteBeneficiary", rse, params);
	}

	
	public static PaymentInfoRequest getPaymentOverview( Long paymentOrderId,  RequestStateEvent<PaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new PaymentInfoRequest("getPaymentOverview", rse, params);
	}

	
	public static PaymentInfoRequest rejectPaymentCordOrder( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentCordOrderId,  RequestStateEvent<PaymentInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentCordOrderId", paymentCordOrderId);
		return new PaymentInfoRequest("rejectPaymentCordOrder", rse, params);
	}

	
	public static PaymentTemplateInfoRequest savePaymentTemplateFavorite( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentTemplateId,  Boolean favorite,  RequestStateEvent<PaymentTemplateInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		params.put("favorite", favorite);
		return new PaymentTemplateInfoRequest("savePaymentTemplateFavorite", rse, params);
	}

	
	public static PaymentCordOrderRequest getCordPayment( Long paymentCordOrderId,  com.avaloq.afs.server.bsp.client.ws.PaymentCordQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<PaymentCordOrderRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentCordOrderId", paymentCordOrderId);
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PaymentCordOrderRequest("getCordPayment", rse, params);
	}

	
	public static PaymentMoneyAccountListRequest getPaymentMoneyAccounts( RequestStateEvent<PaymentMoneyAccountListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentMoneyAccountListRequest("getPaymentMoneyAccounts", rse, params);
	}

	
	public static PaymentApprovalInfoListRequest getPaymentApprovals( Long startIndex,  Long size,  RequestStateEvent<PaymentApprovalInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PaymentApprovalInfoListRequest("getPaymentApprovals", rse, params);
	}

	
	public static PaymentInfoListRequest getPayments( com.avaloq.afs.server.bsp.client.ws.PaymentQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<PaymentInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PaymentInfoListRequest("getPayments", rse, params);
	}

	
	public static PaymentTemplateInfoListRequest getPaymentTemplates( com.avaloq.afs.server.bsp.client.ws.PaymentTemplateQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<PaymentTemplateInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new PaymentTemplateInfoListRequest("getPaymentTemplates", rse, params);
	}

	
	public static StandingPaymentInfoListRequest getStandingOrders( com.avaloq.afs.server.bsp.client.ws.StandingOrderQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<StandingPaymentInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new StandingPaymentInfoListRequest("getStandingOrders", rse, params);
	}

	
	public static BeneficiaryRequest getBeneficiary( Long beneficiaryId,  RequestStateEvent<BeneficiaryRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("beneficiaryId", beneficiaryId);
		return new BeneficiaryRequest("getBeneficiary", rse, params);
	}

	
	public static PaymentTemplateInfoRequest deletePaymentTemplate( com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO paymentTemplateId,  RequestStateEvent<PaymentTemplateInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new PaymentTemplateInfoRequest("deletePaymentTemplate", rse, params);
	}

}