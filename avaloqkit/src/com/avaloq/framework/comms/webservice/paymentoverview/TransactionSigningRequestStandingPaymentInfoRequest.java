package com.avaloq.framework.comms.webservice.paymentoverview;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.TransactionSigningRequestStandingPaymentInfoResult;

/**
 * @author jsonwsp2java
 */
public final class TransactionSigningRequestStandingPaymentInfoRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.TransactionSigningRequestStandingPaymentInfoResult> {

	TransactionSigningRequestStandingPaymentInfoRequest(final String aMethodName, final RequestStateEvent<TransactionSigningRequestStandingPaymentInfoRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.TransactionSigningRequestStandingPaymentInfoResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "PaymentOverviewService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}