package com.avaloq.framework.comms.webservice.paymentoverview;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.PaymentMaskListResult;

/**
 * @author jsonwsp2java
 */
public final class PaymentMaskListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.PaymentMaskListResult> {

	PaymentMaskListRequest(final String aMethodName, final RequestStateEvent<PaymentMaskListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.PaymentMaskListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "PaymentOverviewService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}