package com.avaloq.framework.comms.webservice.investmentproposition;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class InvestmentPropositionService {

	private InvestmentPropositionService() {
	}

	
	public static InvestmentPropositionDetailRequest compareInvestmentPropositionDetail( Long investmentPropositionId1,  Long investmentPropositionId2,  RequestStateEvent<InvestmentPropositionDetailRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("investmentPropositionId1", investmentPropositionId1);
		params.put("investmentPropositionId2", investmentPropositionId2);
		return new InvestmentPropositionDetailRequest("compareInvestmentPropositionDetail", rse, params);
	}

	
	public static TransactionsRequest findInvestmentPropositionTransactionList( com.avaloq.afs.server.bsp.client.ws.TransactionListQueryTO transactionListQuery,  RequestStateEvent<TransactionsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionListQuery", transactionListQuery);
		return new TransactionsRequest("findInvestmentPropositionTransactionList", rse, params);
	}

	
	public static InvestmentPropositionDefaultsRequest getInvestmentPropositionDefaults( RequestStateEvent<InvestmentPropositionDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new InvestmentPropositionDefaultsRequest("getInvestmentPropositionDefaults", rse, params);
	}

	
	public static InvestmentPropositionListRequest getInvestmentPropositionList( RequestStateEvent<InvestmentPropositionListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new InvestmentPropositionListRequest("getInvestmentPropositionList", rse, params);
	}

	
	public static BookingsRequest findInvestmentPropositionBookingList( com.avaloq.afs.server.bsp.client.ws.BookingListQueryTO bookingListQuery,  RequestStateEvent<BookingsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("bookingListQuery", bookingListQuery);
		return new BookingsRequest("findInvestmentPropositionBookingList", rse, params);
	}

	
	public static InvestmentPropositionDetailRequest getInvestmentPropositionDetail( Long investmentPropositionId,  RequestStateEvent<InvestmentPropositionDetailRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("investmentPropositionId", investmentPropositionId);
		return new InvestmentPropositionDetailRequest("getInvestmentPropositionDetail", rse, params);
	}

}