package com.avaloq.framework.comms.webservice.investmentproposition;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionDetailResult;

/**
 * @author jsonwsp2java
 */
public final class InvestmentPropositionDetailRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionDetailResult> {

	InvestmentPropositionDetailRequest(final String aMethodName, final RequestStateEvent<InvestmentPropositionDetailRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionDetailResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "InvestmentPropositionService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}