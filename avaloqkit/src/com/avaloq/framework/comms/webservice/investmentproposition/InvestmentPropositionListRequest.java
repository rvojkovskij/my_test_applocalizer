package com.avaloq.framework.comms.webservice.investmentproposition;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionListResult;

/**
 * @author jsonwsp2java
 */
public final class InvestmentPropositionListRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionListResult> {

	InvestmentPropositionListRequest(final String aMethodName, final RequestStateEvent<InvestmentPropositionListRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.investmentproposition.InvestmentPropositionListResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "InvestmentPropositionService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}