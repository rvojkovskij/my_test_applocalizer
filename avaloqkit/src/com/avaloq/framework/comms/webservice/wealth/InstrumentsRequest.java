package com.avaloq.framework.comms.webservice.wealth;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.marketdata.InstrumentsResult;

/**
 * @author jsonwsp2java
 */
public final class InstrumentsRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.marketdata.InstrumentsResult> {

	InstrumentsRequest(final String aMethodName, final RequestStateEvent<InstrumentsRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.marketdata.InstrumentsResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "WealthService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}