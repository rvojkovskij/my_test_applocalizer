package com.avaloq.framework.comms.webservice.wealth;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.wealth.PortfolioPositionsResult;

/**
 * @author jsonwsp2java
 */
public final class PortfolioPositionsRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.wealth.PortfolioPositionsResult> {

	PortfolioPositionsRequest(final String aMethodName, final RequestStateEvent<PortfolioPositionsRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.wealth.PortfolioPositionsResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "WealthService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}