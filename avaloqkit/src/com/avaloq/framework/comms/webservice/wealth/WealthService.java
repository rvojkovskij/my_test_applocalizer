package com.avaloq.framework.comms.webservice.wealth;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class WealthService {

	private WealthService() {
	}

	
	public static BusinessPartnersRequest getWealthBusinessPartners( RequestStateEvent<BusinessPartnersRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new BusinessPartnersRequest("getWealthBusinessPartners", rse, params);
	}

	
	public static BookingsRequest findBookings( com.avaloq.afs.server.bsp.client.ws.BookingListQueryTO bookingListQuery,  RequestStateEvent<BookingsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("bookingListQuery", bookingListQuery);
		return new BookingsRequest("findBookings", rse, params);
	}

	
	public static Request removeExternalWealthItems( String externalWealthItemIds,  RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("externalWealthItemIds", externalWealthItemIds);
		return new Request("removeExternalWealthItems", rse, params);
	}

	
	public static TemplateRequest getProductTemplate( Long productId,  RequestStateEvent<TemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("productId", productId);
		return new TemplateRequest("getProductTemplate", rse, params);
	}

	
	public static Request setObjectAccessFilter( com.avaloq.afs.server.bsp.client.ws.ObjectAccessFilterTO objectAccessFilter,  RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("objectAccessFilter", objectAccessFilter);
		return new Request("setObjectAccessFilter", rse, params);
	}

	
	public static PortfolioPositionsRequest getProductDetails( Long productId,  Long valuationCurrencyId,  RequestStateEvent<PortfolioPositionsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("productId", productId);
		params.put("valuationCurrencyId", valuationCurrencyId);
		return new PortfolioPositionsRequest("getProductDetails", rse, params);
	}

	
	public static ListingListRequest getInstrumentListingList( Long instrumentId,  RequestStateEvent<ListingListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentId", instrumentId);
		return new ListingListRequest("getInstrumentListingList", rse, params);
	}

	
	public static TransactionSigningRequestRequest resendTransactionSigningToken( com.avaloq.afs.server.bsp.client.ws.TransactionSigningObjectTO transactionSigningVerify,  RequestStateEvent<TransactionSigningRequestRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionSigningVerify", transactionSigningVerify);
		return new TransactionSigningRequestRequest("resendTransactionSigningToken", rse, params);
	}

	
	public static IdListRequest getIpsListFromBusinessPartnerListWithCommonContainer( Long businessPartnerIdList,  RequestStateEvent<IdListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("businessPartnerIdList", businessPartnerIdList);
		return new IdListRequest("getIpsListFromBusinessPartnerListWithCommonContainer", rse, params);
	}

	
	public static WealthItemRequest getWealthItem( String wealthItemId,  RequestStateEvent<WealthItemRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("wealthItemId", wealthItemId);
		return new WealthItemRequest("getWealthItem", rse, params);
	}

	
	public static ProductListRequest getExternalWealthItemProductList( RequestStateEvent<ProductListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new ProductListRequest("getExternalWealthItemProductList", rse, params);
	}

	
	public static PortfolioDetailRequest getProductPortfolioDetails( com.avaloq.afs.server.bsp.client.ws.PortfolioIdTO portfolioId,  Long productId,  RequestStateEvent<PortfolioDetailRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("portfolioId", portfolioId);
		params.put("productId", productId);
		return new PortfolioDetailRequest("getProductPortfolioDetails", rse, params);
	}

	
	public static StructureRequest getProductStructure( String productStructureId,  RequestStateEvent<StructureRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("productStructureId", productStructureId);
		return new StructureRequest("getProductStructure", rse, params);
	}

	
	public static WealthDefaultsRequest getWealthDefaults( RequestStateEvent<WealthDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new WealthDefaultsRequest("getWealthDefaults", rse, params);
	}

	
	public static InstrumentRequest getInstrument( Long instrumentId,  RequestStateEvent<InstrumentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("instrumentId", instrumentId);
		return new InstrumentRequest("getInstrument", rse, params);
	}

	
	public static TransactionSigningVerificationRequest verifyTransactionSigning( com.avaloq.afs.server.bsp.client.ws.TransactionSigningVerifyTO transactionSigningVerify,  RequestStateEvent<TransactionSigningVerificationRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionSigningVerify", transactionSigningVerify);
		return new TransactionSigningVerificationRequest("verifyTransactionSigning", rse, params);
	}

	
	public static BankingPositionRequest getBankingPosition( Long positionId,  RequestStateEvent<BankingPositionRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("positionId", positionId);
		return new BankingPositionRequest("getBankingPosition", rse, params);
	}

	
	public static TransactionsRequest findTransactions( com.avaloq.afs.server.bsp.client.ws.TransactionListQueryTO transactionListQuery,  RequestStateEvent<TransactionsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("transactionListQuery", transactionListQuery);
		return new TransactionsRequest("findTransactions", rse, params);
	}

	
	public static BusinessPartnersRequest findBusinessPartners( com.avaloq.afs.server.bsp.client.ws.BusinessPartnerListQueryTO businessPartnerListQuery,  RequestStateEvent<BusinessPartnersRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("businessPartnerListQuery", businessPartnerListQuery);
		return new BusinessPartnersRequest("findBusinessPartners", rse, params);
	}

	
	public static WealthItemRequest addExternalWealthItem( Long businessPartnerId,  com.avaloq.afs.server.bsp.client.ws.WealthItemTO externalWealthItem,  RequestStateEvent<WealthItemRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("businessPartnerId", businessPartnerId);
		params.put("externalWealthItem", externalWealthItem);
		return new WealthItemRequest("addExternalWealthItem", rse, params);
	}

	
	public static WealthItemRequest updateExternalWealthItem( com.avaloq.afs.server.bsp.client.ws.WealthItemTO externalWealthItem,  RequestStateEvent<WealthItemRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("externalWealthItem", externalWealthItem);
		return new WealthItemRequest("updateExternalWealthItem", rse, params);
	}

	
	public static WealthOverviewRequest getWealthOverview( RequestStateEvent<WealthOverviewRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new WealthOverviewRequest("getWealthOverview", rse, params);
	}

}