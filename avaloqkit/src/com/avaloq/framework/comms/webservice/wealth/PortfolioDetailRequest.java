package com.avaloq.framework.comms.webservice.wealth;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.wealth.PortfolioDetailResult;

/**
 * @author jsonwsp2java
 */
public final class PortfolioDetailRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.wealth.PortfolioDetailResult> {

	PortfolioDetailRequest(final String aMethodName, final RequestStateEvent<PortfolioDetailRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.wealth.PortfolioDetailResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "WealthService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}