package com.avaloq.framework.comms.webservice.internationalpayment;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateResult;

/**
 * @author jsonwsp2java
 */
public final class InternationalPaymentTemplateRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateResult> {

	InternationalPaymentTemplateRequest(final String aMethodName, final RequestStateEvent<InternationalPaymentTemplateRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "InternationalPaymentService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}