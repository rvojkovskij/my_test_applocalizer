package com.avaloq.framework.comms.webservice.internationalpayment;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class InternationalPaymentService {

	private InternationalPaymentService() {
	}

	
	public static InternationalPaymentRequest getPayment( Long paymentOrderId,  RequestStateEvent<InternationalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new InternationalPaymentRequest("getPayment", rse, params);
	}

	
	public static PaymentCurrenciesRequest getPaymentCurrencies( RequestStateEvent<PaymentCurrenciesRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PaymentCurrenciesRequest("getPaymentCurrencies", rse, params);
	}

	
	public static InternationalPaymentRequest savePayment( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentOrderTO paymentOrder,  RequestStateEvent<InternationalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternationalPaymentRequest("savePayment", rse, params);
	}

	
	public static InternationalStandingPaymentRequest saveStandingPayment( com.avaloq.afs.aggregation.to.payment.international.InternationalStandingOrderTO standingOrder,  RequestStateEvent<InternationalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternationalStandingPaymentRequest("saveStandingPayment", rse, params);
	}

	
	public static SwiftBankInfoRequest findSwiftBankInfoByIban( String iban,  RequestStateEvent<SwiftBankInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("iban", iban);
		return new SwiftBankInfoRequest("findSwiftBankInfoByIban", rse, params);
	}

	
	public static InternationalStandingPaymentRequest getStandingPayment( Long paymentOrderId,  RequestStateEvent<InternationalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrderId", paymentOrderId);
		return new InternationalStandingPaymentRequest("getStandingPayment", rse, params);
	}

	
	public static SwiftBankInfoRequest getBankDetailByBic( String bic,  RequestStateEvent<SwiftBankInfoRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("bic", bic);
		return new SwiftBankInfoRequest("getBankDetailByBic", rse, params);
	}

	
	public static InternationalStandingPaymentRequest verifyStandingPayment( com.avaloq.afs.aggregation.to.payment.international.InternationalStandingOrderTO standingOrder,  RequestStateEvent<InternationalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternationalStandingPaymentRequest("verifyStandingPayment", rse, params);
	}

	
	public static SwiftBankInfoListRequest findSwiftBankInfos( com.avaloq.afs.server.bsp.client.ws.SwiftBankInfoQueryTO query,  RequestStateEvent<SwiftBankInfoListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		return new SwiftBankInfoListRequest("findSwiftBankInfos", rse, params);
	}

	
	public static CountryListRequest getCountries( RequestStateEvent<CountryListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new CountryListRequest("getCountries", rse, params);
	}

	
	public static InternationalPaymentTemplateRequest verifyPaymentTemplate( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternationalPaymentTemplateRequest("verifyPaymentTemplate", rse, params);
	}

	
	public static InternationalPaymentRequest verifyPayment( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentOrderTO paymentOrder,  RequestStateEvent<InternationalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternationalPaymentRequest("verifyPayment", rse, params);
	}

	
	public static InternationalPaymentRequest submitPayment( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentOrderTO paymentOrder,  RequestStateEvent<InternationalPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentOrder", paymentOrder);
		return new InternationalPaymentRequest("submitPayment", rse, params);
	}

	
	public static InternationalPaymentDefaultsRequest getDefaults( RequestStateEvent<InternationalPaymentDefaultsRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new InternationalPaymentDefaultsRequest("getDefaults", rse, params);
	}

	
	public static InternationalPaymentTemplateRequest getPaymentTemplate( Long paymentTemplateId,  RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplateId", paymentTemplateId);
		return new InternationalPaymentTemplateRequest("getPaymentTemplate", rse, params);
	}

	
	public static InternationalPaymentTemplateRequest submitPaymentTemplate( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternationalPaymentTemplateRequest("submitPaymentTemplate", rse, params);
	}

	
	public static InternationalPaymentTemplateRequest savePaymentTemplate( com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateTO paymentTemplate,  RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("paymentTemplate", paymentTemplate);
		return new InternationalPaymentTemplateRequest("savePaymentTemplate", rse, params);
	}

	
	public static InternationalStandingPaymentRequest submitStandingPayment( com.avaloq.afs.aggregation.to.payment.international.InternationalStandingOrderTO standingOrder,  RequestStateEvent<InternationalStandingPaymentRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("standingOrder", standingOrder);
		return new InternationalStandingPaymentRequest("submitStandingPayment", rse, params);
	}

	
	public static EndOfDaySwitchRequest getEndOfDaySwitch( RequestStateEvent<EndOfDaySwitchRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new EndOfDaySwitchRequest("getEndOfDaySwitch", rse, params);
	}

	
	public static PaymentHolidaysRequest getPaymentHolidays( Long countries,  RequestStateEvent<PaymentHolidaysRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("countries", countries);
		return new PaymentHolidaysRequest("getPaymentHolidays", rse, params);
	}

}