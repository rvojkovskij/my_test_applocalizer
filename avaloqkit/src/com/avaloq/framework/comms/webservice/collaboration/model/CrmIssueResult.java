package com.avaloq.framework.comms.webservice.collaboration.model;

/**
 * @author jsonwsp2java
 */
public class CrmIssueResult {

	/**
	 * Generated Members
	 */
	private java.util.Date dateTime;
	
	private com.avaloq.afs.server.bsp.client.ws.AlertsTO alerts;
	
	private java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> notificationList;
	
	private java.util.List<com.avaloq.afs.server.bsp.client.ws.CrmIssueTO> issueList;
	
	private java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> errorNotificationList;
	

	/**
	 * Generated Getters and Setters 
	 */
	public java.util.Date getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(final java.util.Date dateTime) {
		this.dateTime = dateTime;
	}
	
	public com.avaloq.afs.server.bsp.client.ws.AlertsTO getAlerts() {
		return this.alerts;
	}

	public void setAlerts(final com.avaloq.afs.server.bsp.client.ws.AlertsTO alerts) {
		this.alerts = alerts;
	}
	
	public java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> getNotificationList() {
		return this.notificationList;
	}

	public void setNotificationList(final java.util.List<com.avaloq.afs.aggregation.to.LocalizedNotification> notificationList) {
		this.notificationList = notificationList;
	}
	
	public java.util.List<com.avaloq.afs.server.bsp.client.ws.CrmIssueTO> getIssueList() {
		return this.issueList;
	}

	public void setIssueList(final java.util.List<com.avaloq.afs.server.bsp.client.ws.CrmIssueTO> issueList) {
		this.issueList = issueList;
	}
	
	public java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> getErrorNotificationList() {
		return this.errorNotificationList;
	}

	public void setErrorNotificationList(final java.util.List<com.avaloq.afs.aggregation.to.LocalizedErrorNotification> errorNotificationList) {
		this.errorNotificationList = errorNotificationList;
	}
	

}