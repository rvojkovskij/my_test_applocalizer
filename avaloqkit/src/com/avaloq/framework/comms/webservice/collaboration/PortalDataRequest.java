package com.avaloq.framework.comms.webservice.collaboration;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.framework.comms.webservice.collaboration.model.PortalDataResult;

/**
 * @author jsonwsp2java
 */
public final class PortalDataRequest extends AbstractJsonHTTPRequest<com.avaloq.framework.comms.webservice.collaboration.model.PortalDataResult> {

	PortalDataRequest(final String aMethodName, final RequestStateEvent<PortalDataRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.framework.comms.webservice.collaboration.model.PortalDataResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "CollaborationService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}