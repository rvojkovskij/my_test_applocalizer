package com.avaloq.framework.comms.webservice.collaboration;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class CollaborationService {

	private CollaborationService() {
	}

	
	public static CrmIssueRequest getCrmIssuesWithQuery( com.avaloq.afs.server.bsp.client.ws.CrmIssueQueryTO query,  Long startIndex,  Long size,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("query", query);
		params.put("startIndex", startIndex);
		params.put("size", size);
		return new CrmIssueRequest("getCrmIssuesWithQuery", rse, params);
	}

	
	public static CrmIssueRequest markCrmIssueRead( Long id,  Boolean read,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("id", id);
		params.put("read", read);
		return new CrmIssueRequest("markCrmIssueRead", rse, params);
	}

	
	public static CrmIssueRequest updateCrmIssue( com.avaloq.afs.server.bsp.client.ws.CrmIssueTO issue,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("issue", issue);
		return new CrmIssueRequest("updateCrmIssue", rse, params);
	}

	
	public static PortalDataRequest getPortalData( RequestStateEvent<PortalDataRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new PortalDataRequest("getPortalData", rse, params);
	}

	
	public static BusinessPartnersRequest getBusinessPartners( RequestStateEvent<BusinessPartnersRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new BusinessPartnersRequest("getBusinessPartners", rse, params);
	}

	
	public static CrmIssueRequest createCrmIssue( com.avaloq.afs.server.bsp.client.ws.CrmIssueTO issue,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("issue", issue);
		return new CrmIssueRequest("createCrmIssue", rse, params);
	}

	
	public static CrmIssueRequest getCrmIssue( Long id,  Boolean markAsRead,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("id", id);
		params.put("markAsRead", markAsRead);
		return new CrmIssueRequest("getCrmIssue", rse, params);
	}

	
	public static Request closeCrmIssues( Long idList,  RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("idList", idList);
		return new Request("closeCrmIssues", rse, params);
	}

	
	public static CrmIssueRequest verifyCrmIssue( com.avaloq.afs.server.bsp.client.ws.CrmIssueTO issue,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("issue", issue);
		return new CrmIssueRequest("verifyCrmIssue", rse, params);
	}

	
	public static CrmIssueRequest addCommentToCrmIssue( Long id,  String comment,  RequestStateEvent<CrmIssueRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("id", id);
		params.put("comment", comment);
		return new CrmIssueRequest("addCommentToCrmIssue", rse, params);
	}

}