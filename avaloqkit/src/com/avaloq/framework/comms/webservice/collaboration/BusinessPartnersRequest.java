package com.avaloq.framework.comms.webservice.collaboration;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.wealth.BusinessPartnersResult;

/**
 * @author jsonwsp2java
 */
public final class BusinessPartnersRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.wealth.BusinessPartnersResult> {

	BusinessPartnersRequest(final String aMethodName, final RequestStateEvent<BusinessPartnersRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.wealth.BusinessPartnersResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "CollaborationService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}