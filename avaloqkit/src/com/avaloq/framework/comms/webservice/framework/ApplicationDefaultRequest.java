package com.avaloq.framework.comms.webservice.framework;

import java.util.Map;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.json.AbstractJsonHTTPRequest;
// import com.avaloq.afs.aggregation.to.framework.ApplicationDefaultResult;

/**
 * @author jsonwsp2java
 */
public final class ApplicationDefaultRequest extends AbstractJsonHTTPRequest<com.avaloq.afs.aggregation.to.framework.ApplicationDefaultResult> {

	ApplicationDefaultRequest(final String aMethodName, final RequestStateEvent<ApplicationDefaultRequest> aRequestStateEvent, final Map<String,Object> aParams) {
		super(aMethodName, aRequestStateEvent, aParams, com.avaloq.afs.aggregation.to.framework.ApplicationDefaultResult.class);
	}
	
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getWebserviceBaseUrl() + "FrameworkService";
	}

	@Override
	public String getWebserviceVersion() {
		return "avaloq/1.0";
	}

	/*
	@Override
	public String getWebserviceType() {
		return "jsonwsp";
	}
	*/

}