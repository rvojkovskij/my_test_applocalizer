package com.avaloq.framework.comms.webservice.framework;

import com.avaloq.framework.comms.RequestStateEvent;

/**
 * @author jsonwsp2java
 */
public final class FrameworkService {

	private FrameworkService() {
	}

	
	public static ApplicationDefaultRequest getApplicationDefaults( com.avaloq.afs.aggregation.to.framework.ClientAppInfoTO clientAppInfo,  RequestStateEvent<ApplicationDefaultRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		params.put("clientAppInfo", clientAppInfo);
		return new ApplicationDefaultRequest("getApplicationDefaults", rse, params);
	}

	
	public static Request initializeClientSession( RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new Request("initializeClientSession", rse, params);
	}

	
	public static FrameworkCurrencyListRequest getFullCurrencyList( RequestStateEvent<FrameworkCurrencyListRequest> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new FrameworkCurrencyListRequest("getFullCurrencyList", rse, params);
	}

	
	public static Request clientSessionHeartbeat( RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new Request("clientSessionHeartbeat", rse, params);
	}

	
	public static Request logoutClientSession( RequestStateEvent<Request> rse) {
		final java.util.Map<String,Object> params = new java.util.HashMap<String,Object>();
		return new Request("logoutClientSession", rse, params);
	}

}