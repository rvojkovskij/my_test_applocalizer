package com.avaloq.framework.comms;

public abstract class AbstractServerResponse<ResponseType> {

//	private static final String TAG = "AbstractServerResponse";
	
	private boolean mRequestSuccessful = false;
	private boolean mProtocolInvalid = false;

	 /** The data we got from this request **/
	private ResponseType mData = null;
	


	// *****************************************************
	// *** Constructors
	// *****************************************************

	/**
	 * Prevent the public use of the default initialiser
	 */
	public AbstractServerResponse() {
	}


	// *****************************************************
	// *** Public methods
	// *****************************************************

	/**
	 * Return true if the request was successfully executed
	 */
	public boolean wasRequestSuccessfullyExecuted() {
		return mRequestSuccessful;
	}

	/**
	 * Set the request execution result
	 */
	public void setRequestSuccessfullyExecuted(boolean success) {
		mRequestSuccessful = success;
	}

	/**
	 * Return true if the protocol used to communicate with the server is no
	 * longer valid. This implies that the user should install an updated
	 * version of the App.
	 */
	public boolean isProtocolInvalid() {
		return mProtocolInvalid;
	}

	/**
	 * Set the invalid protocol flag
	 */
	public void setProtocolInvalid() {
		mProtocolInvalid = true;
	}
	
	
	/**
	 * Parse the returned data - called from the queue manager.
	 * Set setRequestSuccessfullyExecuted to false if it fails   
	 */
	public void parseServerResponseData() {
		try {
			mData = onParseServerResponseData();						

		} catch (Exception e) {
			e.printStackTrace();
			setRequestSuccessfullyExecuted(false);			
		}
	}


	/**
	 * Handle the returned response body
	 * @throws IllegalArgumentException if the data cannot be parsed.
	 */
	public abstract ResponseType onParseServerResponseData() throws IllegalArgumentException;

	/**
	 * Called instead of onParseServerResponseData() when a cache entry is being used.
	 */
	public void onUseCachedResponse () {}
	
	/**
	 * Returns the server's data transfer object from this request, or null if there is no data.
	 */
	public ResponseType getData() {
		return this.mData;
	}

}
