package com.avaloq.framework.comms;

import java.util.LinkedList;
import java.util.Queue;

import android.os.SystemClock;
import android.util.Log;

import com.avaloq.framework.comms.authentication.AuthenticationManager;

/**
 * Provides access to the server request and response queues.
 * 
 */
public class QueueManager {

	private final static String TAG = "QueueManager";

	private static QueueManager mInstance;

	private Queue<AbstractServerRequest<?>> mRequestQueue = new LinkedList<AbstractServerRequest<?>>();
	private Queue<AbstractServerRequest<?>> mResponseQueue = new LinkedList<AbstractServerRequest<?>>();

	private QueueRequestThread mRequestQueueThread = new QueueRequestThread();
	private QueueResponseThread mResponseQueueThread = new QueueResponseThread();

	private long mTimeOfLastExecutedRequest;
	private boolean mShutdown = false;


	private QueueManager() {
		// Do not start the worker threads here, before the singleton instance is set.
	}

	public static QueueManager getInstance() {
		if (mInstance == null) {
			mInstance = new QueueManager();
			mInstance.startWorkerThreads();
		}
		return mInstance;
	}
	
	public static void resetInstance() {
		mInstance = null;
	}

	private void startWorkerThreads() {

		// Start the handler threads
		mRequestQueueThread.setDaemon(true);
		mRequestQueueThread.start();

		mResponseQueueThread.setDaemon(true);
		mResponseQueueThread.start();

		Log.d(TAG, "Queue threads started");
	}

	/**
	 * Add a request to the request queue.
	 * @param request
	 */
	public void enqueueServerRequest(AbstractServerRequest<?> request) {
		if (request.setRequestStateQueued()) {
			mRequestQueue.add(request);
		} else {
			Log.w(TAG, "Cannot add request to queue: not in state QUEUED: " + request.getRequestIdentifier());
		}
	}

	/**
	 * Add a server response (wrapped in its request) to the response queue.
	 * @param request
	 */
	public void enqueueServerResponse(AbstractServerRequest<?> request) {
		mResponseQueue.add(request);
	}

	/**
	 * Get the next to-be-executed request from the request queue (or null of the queue is avq_activity_empty).
	 * Does return null while an authentication sequence is in progress.
	 * 
	 * Does NOT remove the request from the queue - use
	 * 
	 * @return the server request
	 */
	AbstractServerRequest<?> getNextServerRequest() {

		if (AuthenticationManager.getInstance().isAuthenticationInProgress()) {
			return null;
		}

		AbstractServerRequest<?> request = mRequestQueue.peek();
		if (request != null) Log.v(TAG, "Take request from queue: request " + request.getRequestIdentifier());
		return request;		
	}

	/**
	 * Removes the HEAD element from the queue. 
	 * @return true if successful, false if not (=empty queue)
	 */
	boolean removeServerRequest () {
		if (mRequestQueue.size() == 0) return false;

		mRequestQueue.remove();
		return true;
	}


	/**
	 * Get the next to-be-parsed response (wrapped in its request) from the response queue
	 * 
	 * DOES remove the response from the queue.
	 * 
	 * @return the server response
	 */
	AbstractServerRequest<?> getNextServerResponse() {
		AbstractServerRequest<?> requestWithResponse = mResponseQueue.poll();
		if (requestWithResponse != null) Log.v(TAG, "removing response from queue for: " + requestWithResponse.getRequestIdentifier());
		return requestWithResponse;
	}

	/**
	 * Move all queued responses back to the request queue, removing the initial response.
	 * Used when a response triggers an authentication.
	 */
	void requeueAllQueuedResponses() {
		AbstractServerRequest<?> requestWithResponse = null;
		while (mResponseQueue.size() > 0) {						 
			requestWithResponse = mResponseQueue.poll();
			if (requestWithResponse == null) continue;
			requestWithResponse.setRequestStatePendingAuthentication();
			Log.d(TAG, "Re-queueing (due to an auth process) request "+ requestWithResponse.getRequestIdentifier());
			requestWithResponse.setResponse(null);
			enqueueServerRequest(requestWithResponse);
		}

	}

	/**
	 * Empty both request and response queues.
	 */
	public void emptyQueues() {
		// FIXME: Make this thread-safe and ensure any in-process requests/responses are cleared, too. 
		mRequestQueue.clear();
		mResponseQueue.clear();
		Log.i(TAG, "Request/response queues emptied.");
	}

	/**
	 * Get the time in milliseconds since the last server request was executed
	 */
	public long getElapsedTimeSinceLastExecutedRequest() {
		return (SystemClock.elapsedRealtime() - mTimeOfLastExecutedRequest);
	}

	/**
	 * Mark current time (SystemClock.elapsedRealtime()) as time of the last executed request 
	 * (done from the request queue thread)
	 */
	public void markTimeOfLastExecutedRequest() {
		mTimeOfLastExecutedRequest = SystemClock.elapsedRealtime();
		//Log.d(TAG, "Marking time of last request at " + mTimeOfLastExecutedRequest);
	}

	/**
	 * Set all pending requests to failed.
	 */
	public void failAllPendingRequests() {
		try {
			for(AbstractServerRequest<?> request : mRequestQueue) {				
				mRequestQueue.remove(request);
				request.setRequestStateFailed();
			}
		} catch (Exception e) {
			// Queue has (concurrently) already taken that one from the list - no problem.
		}
	}

	/**
	 * Dumps the Stack Trace to the log
	 */
	public void dump(String tag) {
		Log.d(tag, "REQUEST QUEUE:");
		for(AbstractServerRequest<?> request : mRequestQueue) {
			Log.d(tag, request.toString());
		}
		Log.d(tag, "RESPONSE QUEUE:");
		for(AbstractServerRequest<?> request : mResponseQueue) {
			Log.d(tag, request.toString());
		}
	}

	/**
	 * Signal the worker threads to end.
	 * Will block until the threads have stopped.
	 *  
	 * Use this only when exiting the application.
	 */
	public void exit() {
		mShutdown = true;

		// Block until both threads are dead.
		while (mRequestQueueThread.isAlive() || mResponseQueueThread.isAlive()) {
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {				
				e.printStackTrace();
			}
		}		
	}

	/**
	 * Is a app shutdown in progress?
	 * @return
	 */
	boolean shutdown() {
		return mShutdown;
	}

	@Deprecated
	public String getThreadState() {
		return "mRequestQueueThread: " + mRequestQueueThread.isAlive() + " / mRequestQueueThread: " + mRequestQueueThread.isAlive();
	}

}
