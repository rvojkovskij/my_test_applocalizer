package com.avaloq.framework.comms;

import android.content.Intent;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.LogoutActivity;

/**
 * Monitor possible timeouts and invalidate the authentication if reached
 */
public class LoginTimeoutHandlerThread extends Thread {

	private String TAG = LoginTimeoutHandlerThread.class.getSimpleName();
	private boolean mBreak = false;
	private static LoginTimeoutHandlerThread mTimeoutHandlerThread;
	
	/**
	 * Start the timeout handler thread if it's not yet running.
	 */
	public static void startTimeoutHandler() {
		if (mTimeoutHandlerThread == null || !mTimeoutHandlerThread.isAlive()) {
			mTimeoutHandlerThread = new LoginTimeoutHandlerThread();
			mTimeoutHandlerThread.setDaemon(true);
			mTimeoutHandlerThread.start();
		}
	}
	
	private LoginTimeoutHandlerThread() {
		setName(TAG);			
	}

	@Override
	public void run() {
		
		int sessionTimeout = AvaloqApplication.getInstance().getConfiguration().getServerSessionDefaultTimeout();
		Log.i(TAG, "Starting session timeout monitoring. Session timeout period is " + sessionTimeout + "s.");

		while (!mBreak) {

			// If logged in by at least one auth handler, continually check for timeout conditions
			// and auto-logout + show login screen if met.
			
			if (AuthenticationManager.getInstance().isOneAuthenticated()) {

				long timeSinceLastRequest = QueueManager.getInstance().getElapsedTimeSinceLastExecutedRequest();
				if(timeSinceLastRequest > (sessionTimeout * 1000)) {	
					Log.i(TAG, "Session timeout occured. Last request is " + (timeSinceLastRequest/1000) + "s old.");
					mBreak = true;					
					Intent intent = new Intent(BankletActivity.getActiveActivity(), LogoutActivity.class);
					BankletActivity.getActiveActivity().startActivity(intent);
				}										
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {					
				e.printStackTrace();
			}				
		}
		Log.i(TAG, "Login timeout thread stopped");
	}
}
