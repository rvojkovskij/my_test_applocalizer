package com.avaloq.framework;

import java.util.HashMap;

import android.util.Log;

import com.avaloq.framework.ui.ActivityNavigationItem;

/**
 * Defines the overall banklet configuration, such as top level navigation elements.
 * The framework will use this values for proper integration of the banklet into the application.
 *
 * Singleton with public constructor for setup
 * 
 * @author bachi
 *
 */
public abstract class Banklet {
	
	private static final String TAG = Banklet.class.getSimpleName();
	
	private static HashMap<Class<? extends Banklet>, Banklet> instances = new HashMap<Class<? extends Banklet>, Banklet>();
	
	public Banklet() {
		instances.put(this.getClass(), this);
		onCreate();
		Log.d(TAG, "Banklet " + this.getClass().getSimpleName() + " initialized");
	}
	
	
	/**
	 * Get the singleton instance of the passed banklet subclass.
	 * Use this in your banklet implementation's getInstance() method.
	 *  
	 * @param bankletClass
	 * @return the singleton instance or null if not no instance is available
	 */
	public static Banklet getInstanceOf(Class<? extends Banklet> bankletClass) {
		return instances.get(bankletClass);
	}
	
	
	/**
	 * Called when the Banklet singleton instance is created. Use to setup configuration.
	 */
	public abstract void onCreate();

	
	/**
	 * Returns the initial activity of this banklet.
	 * @return
	 */
	public abstract ActivityNavigationItem getInitialActivity();
	
	/**
	 * This method is supposed to return all the Navigation Items that will be used in either the Sliding Menu or in the Action Bar.
	 * @return The Array of Navigation Items
	 */
	public abstract ActivityNavigationItem[] getMainNavigationItems();
	
	
//	TODO: Check if we should rather have a 'authenticationChanged(smth fromLevel, smth toLevel)'
//	/**
//	 * The current session has ended, either due to an explicit logout, a timeout, or other event
//	 * The application might still be running.
//	 */
//	public abstract onLogout();
//	
//	/**
//	 * The user has logged in
//	 */
//	public abstract onLogin();
	
	/**
	 * All cached data in every banklet should be flushed immediately.
	 * Expect the caller to avq_activity_empty the activity task (so you don't need to worry about activity instances)
	 */
	public abstract void onEmptyCache();
	
}
