package com.avaloq.framework.slidingmenu;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.ActivityNavigationItem;
import com.avaloq.framework.ui.NavigationItem;
import com.avaloq.framework.util.ApiSpecificCalls;

public class SlidingMenuAdapter extends ArrayAdapter<ActivityNavigationItem> {
	
	private static final String TAG = SlidingMenuAdapter.class.getSimpleName();
	
	private Activity context;
	
	public SlidingMenuAdapter(Activity context, List<ActivityNavigationItem> items){
		super(context, R.layout.avq_slidingmenu_item, R.id.slidingmenu_label, items);
		this.context = context;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);
		final ActivityNavigationItem item = getItem(position);
		NavigationItem mainItem = AvaloqApplication.getInstance().getCurrentMainNavigationItem();
		if(mainItem != null) {
			Log.d(TAG, "main item: " + context.getString(mainItem.getNameResourceId()));
		}
		TextView text = (TextView)row.findViewById(R.id.slidingmenu_label);
		ImageView icon = (ImageView)row.findViewById(R.id.slidingmenu_icon);
		// set the selected state
		boolean selected = item == mainItem;
		// It seems we have to use setActivated inside a ListView.
		// setSelected won't work here
		// TODO Check with Jan, if we can let this go on Android Version 10.
		ApiSpecificCalls.setViewActivated(row, selected);
		// set the text
		text.setText(item.getNameResourceId());
		// set the icon
		if(item.getIconResourceId() == 0) {
			// TODO Fallback is launcher icon right now
			icon.setImageResource(R.drawable.avq_ic_launcher);
		} else {
			icon.setImageResource(item.getIconResourceId());
		}
		return row;
	}
	
}