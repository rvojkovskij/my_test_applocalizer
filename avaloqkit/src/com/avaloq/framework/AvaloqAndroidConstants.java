package com.avaloq.framework;

public interface AvaloqAndroidConstants {

	// Supported security levels
	public static final int SERVER_REQUEST_AUTHENTICATION_LEVEL_NONE = 0;
	public static final int SERVER_REQUEST_AUTHENTICATION_LEVEL_DEVICE = 1;
	public static final int SERVER_REQUEST_AUTHENTICATION_LEVEL_PASSWORD = 2;
	public static final int SERVER_REQUEST_AUTHENTICATION_LEVEL_TOKEN = 3;

	// Security level transitions
	// These MUST be unique, and cannot clash with any other security level
	// transition constants
	public static final String AUTH_OP_NONE = "NoAuthenticationRequired";
	public static final String AUTH_OP_UNDEFINED = "AvqAuthOpUndefined";
	public static final String AUTH_OP_HTTP_BASIC = "Basic";
	public static final String AUTH_OP_HTTP_DIGEST = "Digest";
	public static final String AUTH_OP_DEVICE_SECRET_INIT = "AvqDoubleSecInit";
	public static final String AUTH_OP_DOUBLE_SEC_ONLY = "AvqDoubleSecOnly";
	public static final String AUTH_OP_DEVICE_SECRET_PASSWORD = "AvqDoubleSecSrp";
	public static final String AUTH_OP_DEVICE_SECRET_CRONTO_TOKEN = "AvqCrontoSignOnly";
	public static final String AUTH_OP_PASSWORD_CRONTO_TOKEN = "AvqCrontoSignPw";

	// Supported dialogs
	public static final int DIALOG_PROTOCOL_WARNING = 100;
}
