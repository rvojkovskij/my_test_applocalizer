package com.avaloq.framework;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import android.app.Activity;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ListView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.internal.app.ActionBarImpl;
import com.actionbarsherlock.internal.app.ActionBarWrapper;
import com.actionbarsherlock.view.MenuItem;
import com.avaloq.framework.slidingmenu.SlidingMenuAdapter;
import com.avaloq.framework.ui.ActivityNavigationItem;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.NavigationItem;

/**
 * Delegate method for the different Banklet superclasses to be used in banklets, such
 * as FragmentBankletActivity, ListBankletActivity, MapBankletActivity
 * @author bachi
 *
 */
public class BankletActivityDelegate {

	protected static final String TAG = BankletActivityDelegate.class.getSimpleName();
	
	public static int LAYOUT_TEMPLATE = R.layout.avq_activity_template;

	/**
	 * Use this method in a banklet activity's onCreate() instead of setContentLayout().
	 * It will load the banklet activity layout into the app's layout container.
	 * 
	 * @param activity
	 * @param layout
	 */
	public static DrawerSet setContentView(Activity activity, int layout) {
		// TODO check if we need the upEnabled parameter here.
		configureNavigation(activity, false);
		ViewStub stub = (ViewStub) activity.findViewById(R.id.banklet_stub);
		stub.setLayoutResource(layout);
		stub.inflate();
		return initializeDrawer(activity);
	}
	
	/**
	 * Sets the ContentView for an activity using a View as layout element (for example PdfViewerActivity did that, when it still existed).
	 * @param activity The activity to set the View for.
	 * @param view The view to set as the "main" view.
	 * @param upEnabled If the up-button should be enabled.
	 */
	public static DrawerSet setContentView(Activity activity, View view, boolean upEnabled) {
		configureNavigation(activity, upEnabled);
		View stub = activity.findViewById(R.id.banklet_stub);
		ViewGroup parent = (ViewGroup)stub.getParent();
		int childIndex = parent.indexOfChild(stub);
		parent.removeView(parent);
		parent.addView(view, childIndex);
		return initializeDrawer(activity);
	}
	
	/**
	 * Configures the slidingmenu.
	 * @param activiy The activity to be configured
	 * @return Returns a DrawerSet containing the Drawer and a Toggle, connected to it
	 */
	private static DrawerSet initializeDrawer(final Activity activity){
		if (isLargeDevice(activity))
			return null;
		final ActionBar actionbar = getActionBar(activity);
        if (actionbar == null)
        	return null;
		
		if (activity.getIntent().hasExtra(BankletActivity.EXTRA_HIDE_MENU) && activity.getIntent().getExtras().getBoolean(BankletActivity.EXTRA_HIDE_MENU)) {
			return null;
		}
        
        final DrawerLayout layout = (DrawerLayout)activity.findViewById(R.id.drawer_layout);
        final ListView drawer = (ListView) activity.findViewById(R.id.left_drawer);
        final List<ActivityNavigationItem> navigationItems = AvaloqApplication.getInstance().getMainNavigationItems();
        drawer.setAdapter(new SlidingMenuAdapter(activity, navigationItems));
        drawer.setOnItemClickListener(new ListView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ActivityNavigationItem item = navigationItems.get(position);
                layout.closeDrawer(drawer);
                AvaloqApplication.getInstance().navigateToActivity(activity, item);
            }
        });

        layout.setDrawerShadow(R.drawable.avq_drawer_shadow, GravityCompat.START);

		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setHomeButtonEnabled(true);
		actionbar.setDisplayShowTitleEnabled(true);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, layout, R.drawable.avq_ic_drawer, R.string.avq_open_drawer, R.string.avq_close_drawer) {};
        layout.setDrawerListener(toggle);

        return new DrawerSet(toggle, layout);
    }
	
	/**
	 * Configures the Navigation for the BankletActivity
	 * @param activity The activity to configure the Navigation for
	 */
	private static void configureNavigation(Activity activity, boolean upEnabled) {
		if (isLargeDevice(activity)){
			configureActionBar(activity, upEnabled);
		}
	}
	
	public static boolean isSmallDevice(Activity activity){
		return (activity.findViewById(R.id.device_small) != null);
	}
	
	/**
	 * isLargeDevice
	 * DRY-Inversion for isSmallDevice
	 * @param activity The activity to be checked against.
	 * @return
	 */
	public static boolean isLargeDevice(Activity activity) {
		return !isSmallDevice(activity);
	}
	
	/**
	 * This method is needed because the different types of SherlockActivities
	 * don't inherit from SherlockActivity but from the corresponding Android
	 * activities. That's why the Activity has to be casted to the correct
	 * SherlockActivity first.
	 */
	
	public static ActionBar getActionBar(Activity activity){
		if (activity instanceof SherlockListActivity){
			return ((SherlockListActivity)activity).getSupportActionBar();
		} else if (activity instanceof SherlockFragmentActivity) {
			return ((SherlockFragmentActivity)activity).getSupportActionBar();
		} else if (activity instanceof SherlockActivity){
			return ((SherlockActivity)activity).getSupportActionBar();
		}
		throw new FrameworkException("The current activity is not inheriting from configured Sherlock activities!");
	}
	
	/**
	 * Wrapper to {@link #configureActionBar(Activity)}, with the up button disabled.
	 * @param activity
	 */
	public static void configureActionBar(final Activity activity) {
		configureActionBar(activity, false);
	}

	/**
	 * Place the tabs inside the title bar. In portrait mode, by default, these are placed
	 * under the title bar.
	 * 
	 * More info: 
	 * 		http://sparetimedev.blogspot.co.uk/2012/11/forcing-embedded-tabs-in-actionbar.html
	 * 		http://stackoverflow.com/questions/11484426/force-collapse-of-tabs-in-actionbar
	 * 		http://stackoverflow.com/questions/12392541/replicate-actionbar-tabs-with-custom-view
	 * 		https://groups.google.com/forum/#!topic/actionbarsherlock/hmmB1JqDeCk

	 * @param actionbar
	 */
	private static void enableEmbeddedTabs(Object actionbar) {
		
		Object t = null;
		
		//pre-ICS 
		if (actionbar instanceof ActionBarImpl) {
		    t = actionbar;
		 
		//ICS and forward
		} else if (actionbar instanceof ActionBarWrapper) {
		    try {
		        Field actionBarField = actionbar.getClass().getDeclaredField("mActionBar");
		        actionBarField.setAccessible(true);
		        t = actionBarField.get(actionbar);
		    } catch (Exception e) {
		        Log.e(TAG, "Error enabling embedded tabs", e);
		    }
		}
		
	    try {
	        Method setHasEmbeddedTabsMethod = t.getClass().getDeclaredMethod("setHasEmbeddedTabs", boolean.class);
	        setHasEmbeddedTabsMethod.setAccessible(true);
	        setHasEmbeddedTabsMethod.invoke(t, true);
	    } catch (Exception e) {
	        Log.e(TAG, "Error marking actionbar embedded", e);
	    }
	}
	
	/**
	 * Has to be called by the banklet activity onResume. It sets the currently selected navigation item
	 * to the correct value after pressing back
	 * @param activity
	 */
	public static void selectCurrentTabInActionbar(Activity activity){
		if (isLargeDevice(activity)){
			final ActionBar actionbar = getActionBar(activity);
			if (actionbar == null) return;
			
			NavigationItem currentNavItem = AvaloqApplication.getInstance().getCurrentMainNavigationItem();

			int i = 0;
			for (final ActivityNavigationItem item : AvaloqApplication.getInstance().getMainNavigationItems()) {				
				if (item.getTarget().equals(activity.getClass()) ){
					AvaloqApplication.getInstance().setCurrentMainNavigationItem(item);
					actionbar.setSelectedNavigationItem(i);
					break;
				}
				i++;
			}
		}
	}
	
	/**
	 * Build the action bar
	 * TODO This method might not be public later.
	 * @param activity
	 * @param upEnabled If the "Up" Button should be enabled
	 * @return Returns true when the action bar was configured
	 */
	public static boolean configureActionBar(final Activity activity, boolean upEnabled) {
		if (isLargeDevice(activity)){
			final ActionBar actionbar = getActionBar(activity);
			if (actionbar == null)
				return true;
			actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			actionbar.setDisplayHomeAsUpEnabled(upEnabled);
			actionbar.setDisplayShowTitleEnabled(false);
			NavigationItem currentNavItem = AvaloqApplication.getInstance().getCurrentMainNavigationItem();
			
			enableEmbeddedTabs(actionbar);
			
			for (final ActivityNavigationItem item : AvaloqApplication.getInstance().getMainNavigationItems()) {
				// Configure the Tab
				ActionBar.Tab tab = actionbar.newTab()
						.setText(item.getNameResourceId())
						.setIcon( (item.getIconResourceId() > 0) ? item.getIconResourceId() : R.drawable.avq_ic_action_appicon)
						.setTag(item) // add the navigation item as tag
						.setTabListener(new ActionBar.TabListener() {
							
							@Override
							public void onTabSelected(Tab tab, FragmentTransaction ft) {							
								NavigationItem currentNavItem = AvaloqApplication.getInstance().getCurrentMainNavigationItem();
															
								// Launch the target activity if the current is != the clicked tab
								// (comparing our own navigation  item, which is stored in the tab's tag)
								if (!tab.getTag().equals(currentNavItem)) {
									AvaloqApplication.getInstance().navigateToActivity(activity, (ActivityNavigationItem) tab.getTag());
									// activity.startActivity(new Intent(activity, item.getTarget()));
								}
							}
							
							@Override
							public void onTabUnselected(Tab tab, FragmentTransaction ft) {														
							}												
							
							@Override
							public void onTabReselected(Tab tab, FragmentTransaction ft) {
								//AvaloqApplication.getInstance().navigateToActivity(activity, (ActivityNavigationItem) tab.getTag());
							}
							
						});
				
				// Add the tab, select it if it's the active one
				boolean selectedTab = (item.equals(currentNavItem));
				actionbar.addTab(tab, selectedTab);
			}
			
			// Select the first tab if we have no currently selected one
			if (currentNavItem == null && actionbar.getNavigationItemCount() > 0) {
				actionbar.setSelectedNavigationItem(0);
			}
			return true;
		}
		return false;
	}
	
	public static boolean onOptionsItemSelected(MenuItem item, DrawerLayout drawer) {
        if (item.getItemId() == android.R.id.home && drawer != null){
            if (drawer.isDrawerOpen(Gravity.LEFT))
                drawer.closeDrawer(Gravity.LEFT);
            else
                drawer.openDrawer(Gravity.LEFT);
            return true;
        }
        return false;
    }
	
	public static class DrawerSet{
        public ActionBarDrawerToggle toggle;
        public DrawerLayout layout;

        public DrawerSet(ActionBarDrawerToggle aToggle, DrawerLayout aLayout){
            toggle = aToggle;
            layout = aLayout;
        }
    }

}
