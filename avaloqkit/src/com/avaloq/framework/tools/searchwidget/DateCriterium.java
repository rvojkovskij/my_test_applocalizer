package com.avaloq.framework.tools.searchwidget;

import java.util.Date;

import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public abstract class DateCriterium extends Criterium {
	
	protected Date mFrom, mTo;

	public DateCriterium(Activity activity) {
		super(activity);
	}
	
	@Override
	public View formatSummary() {
		TextView view = new TextView(mActivity);
		DateUtil dateUtil = new DateUtil(mActivity);
		view.setText(dateUtil.format(mFrom) + " - " + dateUtil.format(mTo));
		return view;
	}

	@Override
	public String getName() {
		return mActivity.getString(R.string.criterium_date);
	}

	@Override
	public Intent getSelectionActivity() {
		Intent intent = new Intent(mActivity, DateCriteriumActivity.class);
		if (mFrom != null)
			intent.putExtra(DateCriteriumActivity.EXTRA_FROM, mFrom);
		if (mTo != null)
			intent.putExtra(DateCriteriumActivity.EXTRA_TO, mTo);
		
		return intent;
	}
	
	@Override
	public void onResult(Intent intent){
		if (intent.getExtras() != null){
			if (intent.getExtras().containsKey(DateCriteriumActivity.EXTRA_FROM))
				mFrom = (Date)intent.getExtras().getSerializable(DateCriteriumActivity.EXTRA_FROM);
			if (intent.getExtras().containsKey(DateCriteriumActivity.EXTRA_TO))
				mTo = (Date)intent.getExtras().getSerializable(DateCriteriumActivity.EXTRA_TO);
		}
	}
	
	@Override
	public void clear() {
		mFrom = null;
		mTo = null;
	}
	
	@Override
	public Bundle saveToBundle() {
		Bundle bundle = new Bundle();
		bundle.putSerializable("mFrom", mFrom);
		bundle.putSerializable("mTo", mTo);
		return bundle;
	}
	
	@Override
	public void restoreFromBundle(Bundle in) {
		if (in == null)
			return;
		if (in.containsKey("mFrom"))
			mFrom = (Date)in.getSerializable("mFrom");
		if (in.containsKey("mTo"))
			mTo = (Date)in.getSerializable("mTo");
	}

}
