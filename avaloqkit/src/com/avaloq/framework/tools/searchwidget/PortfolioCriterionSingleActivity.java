package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.framework.R;

public class PortfolioCriterionSingleActivity extends SingleChoiceCriteriumActivity{
	
	public static final String EXTRA_PORTFOLIO_NAMES = "EXTRA_PORTFOLIO_NAMES";
	public static final String EXTRA_PORTFOLIO_HOLDERS = "EXTRA_PORTFOLIO_HOLDERS";
	public static final String EXTRA_PORTFOLIO_AMOUNTS = "EXTRA_PORTFOLIO_AMOUNTS";
	
	ArrayList<String> portfolioNames = new ArrayList<String>();
	ArrayList<String> portfolioHolders = new ArrayList<String>();
	ArrayList<String> portfolioAmounts = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	protected void initializeKeyValueLists(Bundle extras) {
		super.initializeKeyValueLists(extras);
		
		if (extras.containsKey(EXTRA_PORTFOLIO_NAMES))
			portfolioNames = extras.getStringArrayList(EXTRA_PORTFOLIO_NAMES);
		if (extras.containsKey(EXTRA_PORTFOLIO_HOLDERS))
			portfolioHolders = extras.getStringArrayList(EXTRA_PORTFOLIO_HOLDERS);
		if (extras.containsKey(EXTRA_PORTFOLIO_AMOUNTS))
			portfolioAmounts = extras.getStringArrayList(EXTRA_PORTFOLIO_AMOUNTS);
	}
	
	@Override
	protected void fillValue(TextView view, int index, String value) {
		ViewGroup parent = (ViewGroup) view.getParent();
	    int ind = parent.indexOfChild(view);
	    parent.removeView(view);
		
		View layout = getLayoutInflater().inflate(R.layout.avq_portfolio_criterion_item, parent, false);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)layout.getLayoutParams();
		params.width = 0;
		params.weight = 1;
		((TextView)layout.findViewById(R.id.portfolio_name)).setText(portfolioNames.get(index));
		((TextView)layout.findViewById(R.id.portfolio_holder)).setText(portfolioHolders.get(index));
		((TextView)layout.findViewById(R.id.portfolio_amount)).setText(portfolioAmounts.get(index));
		
	    parent.addView(layout, ind);
	}
}
