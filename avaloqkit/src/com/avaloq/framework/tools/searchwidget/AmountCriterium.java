package com.avaloq.framework.tools.searchwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.avaloq.framework.R;

public abstract class AmountCriterium extends Criterium {
	
	protected int mFrom, mTo;

	public AmountCriterium(Activity activity) {
		super(activity);
	}

	@Override
	public View formatSummary() {
		TextView view = new TextView(mActivity);
		view.setText(mFrom + " - " + mTo);
		return view;
	}

	@Override
	public String getName() {
		return mActivity.getString(R.string.criterium_amount);
	}

	@Override
	public Intent getSelectionActivity() {
		Intent intent = new Intent(mActivity, AmountCriteriumActivity.class);
		intent.putExtra(AmountCriteriumActivity.EXTRA_FROM, mFrom);
		intent.putExtra(AmountCriteriumActivity.EXTRA_TO, mTo);
		
		return intent;
	}
	
	@Override
	public void onResult(Intent intent){
		if (intent.getExtras() != null){
			mFrom = intent.getExtras().getInt(AmountCriteriumActivity.EXTRA_FROM, 0);
			mTo = intent.getExtras().getInt(AmountCriteriumActivity.EXTRA_TO, 0);
		}
	}
	
	@Override
	public void clear() {
		mFrom = 0;
		mTo = 0;
	}
	
	@Override
	public Bundle saveToBundle() {
		Bundle bundle = new Bundle();
		bundle.putInt("mFrom", mFrom);
		bundle.putInt("mTo", mTo);
		return bundle;
	}
	
	@Override
	public void restoreFromBundle(Bundle in) {
		if (in == null)
			return;
		if (in.containsKey("mFrom"))
			mFrom = in.getInt("mFrom");
		if (in.containsKey("mTo"))
			mTo = in.getInt("mTo");
	}

}
