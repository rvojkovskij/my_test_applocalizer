package com.avaloq.framework.tools.searchwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import java.util.Observable;

public abstract class Criterium extends Observable{
	
	protected Activity mActivity;
	protected boolean mReady = false;
	protected Bundle mRestoreBundle;
	
	public Criterium(Activity activity){
		mActivity = activity;
		loadData();
	}
	
	public void loadData(){
		setReady(true);
	}
	
	protected void setReady(boolean ready){
		mReady = ready;
		if (mRestoreBundle != null){
			restoreFromBundle(mRestoreBundle);
			mRestoreBundle = null;
		}
		setChanged();
		notifyObservers(this);
		
	}
	
	public boolean isReady(){
		return mReady;
	}
	
	public abstract void addToQuery();
	public abstract View formatSummary();
	public abstract String getName();
	public abstract Intent getSelectionActivity();
	public abstract void onResult(Intent intent);
	public abstract void clear();
	
	public abstract Bundle saveToBundle();
	public abstract void restoreFromBundle(Bundle in);

	public void setRestoreBundle(Bundle bundle) {
		mRestoreBundle = bundle;
	}
}
