package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.framework.R;

public class CurrencyCriterionActivity extends SingleChoiceCriteriumActivity{
	
	public static final String EXTRA_CURRECY_ISO = "EXTRA_CURRECY_ISO";
	public static final String EXTRA_CURRENCY_ID = "EXTRA_CURRENCY_ID";	
	
	ArrayList<String> currencyISO = new ArrayList<String>();
	long[] currencyId;	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	protected void initializeKeyValueLists(Bundle extras) {
		super.initializeKeyValueLists(extras);
		
		if (extras.containsKey(EXTRA_CURRECY_ISO))
			currencyISO = extras.getStringArrayList(EXTRA_CURRECY_ISO);
		if (extras.containsKey(EXTRA_CURRENCY_ID))
			currencyId = extras.getLongArray(EXTRA_CURRENCY_ID);		
	}
	
	@Override
	protected void fillValue(TextView view, int index, String value) {
		ViewGroup parent = (ViewGroup) view.getParent();
	    int ind = parent.indexOfChild(view);
	    parent.removeView(view);
		
		View layout = getLayoutInflater().inflate(R.layout.avq_currency_criterion_item, parent, false);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)layout.getLayoutParams();
		params.width = 0;
		params.weight = 1;
		((TextView)layout.findViewById(R.id.currency_iso)).setText(currencyISO.get(index));		
		
	    parent.addView(layout, ind);
	}
}
