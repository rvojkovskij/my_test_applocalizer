package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public abstract class MultipleChoiceCriterium<Type> extends SingleChoiceCriterium<Type> {
	
	protected ArrayList<Type> mElements = getDefaultList();

	public MultipleChoiceCriterium(Activity activity) {
		super(activity);
	}
	
	@Override
	public View formatSummary() {
		TextView view = new TextView(mActivity);
		String output = "";
		int counter = 0;
		for (Type element: mElements){			
			if (counter != 0)
				output+= ", ";
			output+= formatElement(element);
			counter++;
		}
		
		if (counter == 0) return null;
		
		view.setText(output);
		return view;
	}
	
	public String formatElement(Type element){
		return getTypeLabelMapping().get(element);
	}
	
	public Intent createEmptySelectionIntent(){
		Intent intent = new Intent(mActivity, MultipleChoiceCriteriumActivity.class);
		if (mElements != null){
			ArrayList<String> tmp = new ArrayList<String>();
			for (Type element: mElements){
				tmp.add(element.toString());
			}
			intent.putExtra(MultipleChoiceCriteriumActivity.EXTRA_ELEMENT, tmp);
		}
		return intent;
	}
	
	@Override
	public void onResult(Intent intent){
		if (intent.getExtras() != null){
			if (intent.getExtras().containsKey(MultipleChoiceCriteriumActivity.EXTRA_ELEMENT)){
				ArrayList<String> tmp = intent.getStringArrayListExtra(MultipleChoiceCriteriumActivity.EXTRA_ELEMENT);
				mElements = new ArrayList<Type>();
				for (String element: tmp){
					mElements.add(getTypeByString(element));
				}
			}
		}
	}
	
	@Override
	public void clear() {
		mElements = getDefaultList();
	}
	
	public ArrayList<Type> getDefaultList() {
		return new ArrayList<Type>();
	}
	
	@Override
	public Bundle saveToBundle() {
		Bundle bundle = new Bundle();
		ArrayList<String> tmp = new ArrayList<String>();
		for (Type element: mElements){
			tmp.add(element.toString());
		}
		bundle.putStringArrayList("mElements", tmp);
		return bundle;
	}
	
	@Override
	public void restoreFromBundle(Bundle in) {
		if (in == null)
			return;
		if (in.containsKey("mElements")){
			ArrayList<String> tmp = in.getStringArrayList("mElements");
			mElements = new ArrayList<Type>();
			for (String element: tmp){
				mElements.add(getTypeByString(element));
			}
		}
	}
	
	
}
