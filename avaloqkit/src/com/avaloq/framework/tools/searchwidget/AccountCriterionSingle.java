package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.PaymentMoneyAccountListResult;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMoneyAccountListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.util.CurrencyUtil;

public abstract class AccountCriterionSingle extends SingleChoiceCriterium<MoneyAccountTO> {
	
	List<MoneyAccountTO> mList = new ArrayList<MoneyAccountTO>();
	
	public AccountCriterionSingle(Activity activity) {
		super(activity);
	}

	@Override
	public LinkedHashMap<MoneyAccountTO, String> getTypeLabelMapping() {
		LinkedHashMap<MoneyAccountTO, String> map = new LinkedHashMap<MoneyAccountTO, String>();
		if (mList != null){
			for (MoneyAccountTO account: mList)
				map.put(account, account.getId().toString());
		}
		return map;
	}

	@Override
	public View formatSummary() {
		TextView view = new TextView(mActivity);
		
		if (mElement == null) return null;
		
		view.setText(mElement.getAccountType());
			
		return view;
	}
	
	@Override
	public String getName() {
		return mActivity.getString(R.string.criterium_account);
	}
	
	@Override
	public Intent createEmptySelectionIntent(){
		Intent intent = new Intent(mActivity, AccountCriterionSingleActivity.class);
		if (mElement != null){
			intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_ELEMENT, mElement.toString());
		}
		
		ArrayList<String> accountNames = new ArrayList<String>();
		ArrayList<String> accountIbans = new ArrayList<String>();
		ArrayList<String> accountAmounts = new ArrayList<String>();
		
		if (mList != null){
			for (MoneyAccountTO account: mList){
				accountNames.add(account.getAccountType());
				accountIbans.add(account.getAccountIban());
				accountAmounts.add(CurrencyUtil.formatMoney(account.getAmount(), account.getCurrencyId()));
			}
		}
		
		intent.putExtra(AccountCriteriumActivity.EXTRA_ACCOUNT_NAMES, accountNames);
		intent.putExtra(AccountCriteriumActivity.EXTRA_ACCOUNT_IBANS, accountIbans);
		intent.putExtra(AccountCriteriumActivity.EXTRA_ACCOUNT_AMOUNTS, accountAmounts);
		
		return intent;
	}
	
	@Override
	public void loadData(){
		PaymentOverviewService.getPaymentMoneyAccounts(new RequestStateEvent<PaymentMoneyAccountListRequest>(){
			@Override
			public void onRequestCompleted(PaymentMoneyAccountListRequest aRequest) {
				PaymentMoneyAccountListResult result = aRequest.getResponse().getData();
				if (result == null){
					onRequestFailed(aRequest);
					return;
				}
				mList = result.getMoneyAccounts();
				setReady(true);
			}
		}).initiateServerRequest();
	}
}
