package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;

public class SearchView extends FrameLayout implements Observer{
	
	public final static int RESULT_CODE = 20;
	
	/**
	 * The Activity used for activity related functionality,
	 * like for example creation of dialogs.
	 */
	protected BankletActivity mActivity;
	
	/**
	 * The runnable which gets executed when the criteria change.
	 */
	Runnable onCriteriaChangedCallback;
	
	/**
	 * The recepient of the onActivityResult event. Could be
	 * either an activity or a fragment
	 */
	Activity mActivityResultRecepient;
	SherlockFragment mFragmentResultRecepient;
	
	/**
	 * A list of all criteria associated with this search view
	 */
	List<Criterium> criteria = new ArrayList<Criterium>();
	
	/**
	 * Contains the indexes of the selected criteria
	 */
	ArrayList<Integer> indexes = new ArrayList<Integer>();
	
	/**
	 * When a criteria dialog is opened, this variable holds the position of the
	 * criterium in the SearchView list of criteria
	 */
	Integer currentDialogPosition = null;
	boolean stopFirstTextChanged = false;
	
	EditText search;
	LinearLayout criteriaContainer;
	Button filterButton;
	Dialog mDialog;
	CriteriumAdapter mCriteriumAdapter;
	
	private View mButtonClear;	
	private View mProgress;
	private Timer searchTimer;
	
	public SearchView(Context context) {
		super(context);
		mActivity = (BankletActivity)context;
	}

	public SearchView(Context context, AttributeSet attrs) {		
		super(context, attrs);
		mActivity = (BankletActivity)context;
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SearchView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		mActivity = (BankletActivity)context;
	}
	
	
	public String getText(){
		return search.getText().toString();
	}
	
	public void addCriterium(Criterium crit){
		filterButton.setVisibility(View.VISIBLE);
		criteria.add(crit);
		crit.deleteObservers();
		crit.addObserver(this);
	}
	
	public List<Criterium> getCriteria(){
		return criteria;
	}
	
	public void setOnCriteriaChangedCallback(Runnable callback){
		onCriteriaChangedCallback = callback;
	}
	
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if (areAllCriteriaReady() && !stopFirstTextChanged){
			initiateSearch();
		}
		stopFirstTextChanged = false;
	}
	
	public void init(Activity recepient){
		mActivityResultRecepient = recepient;
		init();
	}
	
	public void init(SherlockFragment recepient){
		mFragmentResultRecepient = recepient;
		init();
	}
	
	protected void init(){
		View view = LayoutInflater.from(getContext()).inflate(R.layout.avq_search_widget, this, true);
		mCriteriumAdapter = new CriteriumAdapter(mActivity, criteria);
		
		search = (EditText)view.findViewById(R.id.search_field);
		criteriaContainer = (LinearLayout)view.findViewById(R.id.criteria_container);
		filterButton = (Button)view.findViewById(R.id.filter_button);
		
		mButtonClear = findViewById(R.id.search_btnClear);
		mProgress = findViewById(R.id.search_progress);
		
		mButtonClear.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				search.setText("");
				search.clearFocus();
				search.setSelected(false);
				AvaloqApplication.closeSoftKeyboard(mActivity);
			}
		});
		
		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
				
				RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mButtonClear.getLayoutParams();
				
				if (s.toString().length()>0){
					mButtonClear.setVisibility(View.VISIBLE);
					params.width = RelativeLayout.LayoutParams.WRAP_CONTENT;
				}
				else{
					mButtonClear.setVisibility(View.INVISIBLE);
					params.width = 0;
				}
				
				mButtonClear.setLayoutParams(params);
								
				if (searchTimer != null) searchTimer.cancel();
				searchTimer = new Timer();
				searchTimer.schedule(new TimerTask() {
					@Override
					public void run() {		
						SearchView.this.post(new Runnable() {
							
							@Override
							public void run() {
								SearchView.this.onTextChanged(s, start, before, count);
							}
						});
					}
				}, 500);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});
		
		filterButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
				builder.setAdapter(mCriteriumAdapter, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (criteria.get(which).isReady())
							startCriteriumDialog(which);
					}
				});
				
				builder.setNegativeButton(R.string.avq_button_cancel, null);
				builder.setTitle(R.string.avq_start_criterium_dialog);
				mDialog = builder.create();
				mDialog.show();
			}
		});
	}
	
	public void initiateSearch(){
		if (onCriteriaChangedCallback != null)
			onCriteriaChangedCallback.run();
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK){
			if (mDialog != null)
				mDialog.dismiss();
			
			if (currentDialogPosition != null){
				Criterium crit = criteria.get(currentDialogPosition);
				crit.onResult(data);
				if (!indexes.contains(currentDialogPosition))
					indexes.add(currentDialogPosition);
				renderSummaryForCriterium(currentDialogPosition);
				initiateSearch();
				
				currentDialogPosition = null;
			}
		}
	}
	
	protected void renderSummaryForCriterium(int position){
		Criterium crit = criteria.get(position);
		View view = crit.formatSummary();
		
		// if nothing was selected
		if (view == null) return;
		
		View exists = findViewWithTag("criterium"+position);
		if (exists == null){
			View layout = LayoutInflater.from(mActivity).inflate(R.layout.avq_criterium_summary, null, false);
			LinearLayout placeholder = (LinearLayout)layout.findViewById(R.id.summary_placeholder);
			layout.setTag("criterium"+position);
			
			View close = layout.findViewById(R.id.closeButton);
			close.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					View parent = (View)v.getParent();
					String tag = (String)parent.getTag();
					removeSummaryFromContainer(parent);
					tag = tag.substring("criterium".length());
					criteria.get(Integer.valueOf(tag)).clear();
					initiateSearch();
				}
			});
			
			placeholder.addView(view);
			addSummaryToContainer(layout);
		}
		else {
			LinearLayout placeholder = (LinearLayout)exists.findViewById(R.id.summary_placeholder);
			placeholder.removeAllViews();
			placeholder.addView(view);
		}
	}
	
	protected List<String> getCriteriaNames(){
		List<String> names = new ArrayList<String>();
		for (Criterium crit: criteria)
			names.add(crit.getName());
		
		return names;
	}
	
	protected void startCriteriumDialog(int position){
		Criterium crit = criteria.get(position);
		currentDialogPosition = position;
		if (mActivityResultRecepient != null)
			mActivityResultRecepient.startActivityForResult(crit.getSelectionActivity(), RESULT_CODE);
		else if (mFragmentResultRecepient != null)
			mFragmentResultRecepient.startActivityForResult(crit.getSelectionActivity(), RESULT_CODE);
	}
	
	protected void addSummaryToContainer(View layout){
		criteriaContainer.addView(layout);
		criteriaContainer.setVisibility(View.VISIBLE);
	}
	
	protected void removeSummaryFromContainer(View layout){
		int tag = Integer.valueOf(layout.getTag().toString().substring("criterium".length()));
		if (indexes.contains(tag))
			indexes.remove((Integer)tag);
		
		criteriaContainer.removeView(layout);
		if (criteriaContainer.getChildCount() == 0)
			criteriaContainer.setVisibility(View.GONE);
	}
	
	@Override
	protected Parcelable onSaveInstanceState() {
		Parcelable state = super.onSaveInstanceState();
		SavedState newState = new SavedState(state, this);
		return newState;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable savedState) {
		stopFirstTextChanged = true;
		if (!(savedState instanceof SavedState)){
			super.onRestoreInstanceState(savedState);
			return;
		}
			
		SavedState state = (SavedState) savedState;
		super.onRestoreInstanceState(state.getSuperState());
	        
	    for (int i=0; i<state.bundles.size(); i++){
	    	Criterium crit = criteria.get(i);
	    	if (crit.isReady())
	    		crit.restoreFromBundle(state.bundles.get(i));
	    	else
	    		crit.setRestoreBundle(state.bundles.get(i));
	    }
	        
	    indexes = state.indexes;
	    
	}
	
	protected abstract class RightDrawableOnTouchListener implements OnTouchListener {
	    Drawable drawable;
	    private int fuzz = 10;

	    @Override
	    public boolean onTouch(final View v, final MotionEvent event) {
	    	TextView view = (TextView)v;
	    	final Drawable[] drawables = view.getCompoundDrawables();
	        if (drawables != null && drawables.length == 4)
	            this.drawable = drawables[2];
	        if (event.getAction() == MotionEvent.ACTION_DOWN && drawable != null) {
	            final int x = (int) event.getX();
	            final int y = (int) event.getY();
	            final Rect bounds = drawable.getBounds();
	            if (x >= (v.getRight() - bounds.width() - fuzz) && x <= (v.getRight() - v.getPaddingRight() + fuzz)
	                    && y >= (v.getPaddingTop() - fuzz) && y <= (v.getHeight() - v.getPaddingBottom()) + fuzz) {
	                return onDrawableTouch(event);
	            }
	        }
	        return false;
	    }

	    public abstract boolean onDrawableTouch(final MotionEvent event);
	}
	
	protected static class SavedState extends BaseSavedState {
		protected static final String EXTRA_BUNDLES = "extra_bundles";
		protected static final String EXTRA_INDEXES = "extra_indexes";
		
		ArrayList<Bundle> bundles = new ArrayList<Bundle>();
		ArrayList<Integer> indexes;

		public SavedState(Parcelable arg, SearchView searchView) {
			super(arg);
			for (Criterium crit: searchView.criteria)
				bundles.add(crit.saveToBundle());
			indexes = searchView.indexes;
		}
		
		public SavedState(Parcel in) {
			super(in);
			Bundle b = in.readBundle();
			bundles = b.getParcelableArrayList(EXTRA_BUNDLES);
			indexes = b.getIntegerArrayList(EXTRA_INDEXES);
			
		}
		
		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			Bundle b = new Bundle();
			b.putParcelableArrayList(EXTRA_BUNDLES, bundles);
			b.putIntegerArrayList(EXTRA_INDEXES, indexes);
			dest.writeBundle(b);
		}

		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			@Override
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}
			
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}
	
	public void setProgressVisibility(boolean visible){
		if (visible){
			mProgress.setVisibility(View.VISIBLE);
		}else{
			mProgress.setVisibility(View.GONE);
		}
	}
	
	public class CriteriumAdapter extends ArrayAdapter<Criterium>{
		List<Criterium> mCriteria;
		
		public CriteriumAdapter(Context context, List<Criterium> objects){
			super(context, android.R.layout.simple_list_item_1, criteria);
			mCriteria = objects;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = LayoutInflater.from(getContext()).inflate(R.layout.avq_criterium_dialog_item, null, false);
			Criterium crit = getItem(position);
			((TextView)view.findViewById(R.id.criterium_name)).setText(crit.getName());
			view.findViewById(R.id.progress_bar).setVisibility(crit.isReady() ? View.INVISIBLE : View.VISIBLE);
			
			return view;
		}

		public void update(Observable observable, Object data) {
			notifyDataSetChanged();
		}
		
	}
	
	public boolean areAllCriteriaReady(){
		for (Criterium crit: criteria)
			if (!crit.isReady())
				return false;
		
		return true;
	}

	@Override
	public void update(Observable observable, Object data) {
		if (mDialog != null && mDialog.isShowing() && mCriteriumAdapter != null){
			mCriteriumAdapter.update(observable, data);
		}
		
		if (areAllCriteriaReady()){
			for (Integer index: indexes)
		    	renderSummaryForCriterium(index);
			
			initiateSearch();
		}
	}
	
}
