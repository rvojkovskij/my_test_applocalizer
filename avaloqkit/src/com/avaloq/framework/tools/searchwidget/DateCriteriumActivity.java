package com.avaloq.framework.tools.searchwidget;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.DateUtil;

public class DateCriteriumActivity extends BankletActivity{
	public static final String EXTRA_FROM = "extra_from";
	public static final String EXTRA_TO = "extra_to";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avq_criterium_date);
		
		Bundle extras = getIntent().getExtras();
		final DateUtil dateUtil = new DateUtil(this);
		if (extras != null){
			if (extras.containsKey(EXTRA_FROM)){
				Date tmp = (Date)extras.getSerializable(EXTRA_FROM);
				if (tmp != null)
					((TextView)findViewById(R.id.amount_from_value)).setText(dateUtil.format(tmp));
			}
			if (extras.containsKey(EXTRA_TO)){
				Date tmp = (Date)extras.getSerializable(EXTRA_TO);
				if (tmp != null)
					((TextView)findViewById(R.id.amount_to_value)).setText(dateUtil.format(tmp));
			}
		}
		
		TextView from = (TextView)findViewById(R.id.amount_from_value);
		from.setOnClickListener(new EventDateListenerFactory(this, from));
		
		TextView to = (TextView)findViewById(R.id.amount_to_value);
		to.setOnClickListener(new EventDateListenerFactory(this, to));
		
		Button button = (Button)findViewById(R.id.button_criterium_ok);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Date from, to;
				try {
					from = dateUtil.parse(((TextView)findViewById(R.id.amount_from_value)).getText().toString());
				}
				catch (ParseException e){
					from = null;
				}
				try {
					to = dateUtil.parse(((TextView)findViewById(R.id.amount_to_value)).getText().toString());
				}
				catch (ParseException e){
					to = null;
				}
				
				if (from != null && to != null && from.before(to)){
					Intent result = new Intent();
					result.putExtra(EXTRA_FROM, (from == null) ? 0 : from);
					result.putExtra(EXTRA_TO, (to == null) ? 0 : to);
					setResult(Activity.RESULT_OK, result);
					finish();
				}
				else {
					DialogFragment.createAlert(getString(R.string.avq_criterium_validation_error_title), getString(R.string.avq_criterium_date_validation_error)).show();
				}
			}
		});
	}
	
	public class EventDateListenerFactory implements OnDateSetListener, OnClickListener{
		TextView mTarget;
		Activity mActivity;
		
		public EventDateListenerFactory(Activity activity, TextView aTarget){
			mTarget = aTarget;
			mActivity = activity;
			Calendar cal = Calendar.getInstance();
			onDateSet(null, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		}
		
		@Override
		public void onClick(View v) {
			Calendar cal = Calendar.getInstance();
			DateUtil dateUtil = new DateUtil(mActivity);
			try{
				Date date = dateUtil.parse((String)mTarget.getText());
				cal.setTime(date);
			}
			catch (Exception e){
				// Nothing to do here. If the date can't be parsed, today's date is
				// taken by default from the Calendar class.
			}
			Dialog dialog = new DatePickerDialog(mActivity,
					this, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			DialogFragment df = new DialogFragment();
			df.setDialog(dialog);
			df.show();
		}
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
			DateUtil dateUtil = new DateUtil(mActivity);
			mTarget.setText(dateUtil.format(cal.getTime()));
		}
	}
}
