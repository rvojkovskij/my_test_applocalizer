package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;

import com.avaloq.afs.aggregation.to.payment.PaymentMoneyAccountListResult;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMoneyAccountListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.util.CurrencyUtil;

public abstract class PortfolioCriterion extends MultipleChoiceCriterium<ContainerPortfolioTO> {
	
	List<ContainerPortfolioTO> mList = new ArrayList<ContainerPortfolioTO>();
	
	public PortfolioCriterion(Activity activity) {
		super(activity);		
	}

	@Override
	public LinkedHashMap<ContainerPortfolioTO, String> getTypeLabelMapping() {
		LinkedHashMap<ContainerPortfolioTO, String> map = new LinkedHashMap<ContainerPortfolioTO, String>();

		if (mList != null){
			for (ContainerPortfolioTO portfolio: mList)
				map.put(portfolio, portfolio.getName());
		}

		return map;
	}

	@Override
	public String getName() {
		return mActivity.getString(R.string.criterium_portfolio);
	}
	
	@Override
	public Intent createEmptySelectionIntent(){
		Intent intent = new Intent(mActivity, PortfolioCriterionActivity.class);
		if (mElements != null){
			ArrayList<String> tmp = new ArrayList<String>();
			for (ContainerPortfolioTO element: mElements){
				tmp.add(element.toString());
			}
			intent.putExtra(MultipleChoiceCriteriumActivity.EXTRA_ELEMENT, tmp);
		}
		
		ArrayList<String> portfolioNames = new ArrayList<String>();
		ArrayList<String> portfolioHolders = new ArrayList<String>();
		ArrayList<String> portfolioAmounts = new ArrayList<String>();
		
		if (mList != null){
			for (ContainerPortfolioTO portfolio: mList){
				portfolioNames.add(portfolio.getName());
				portfolioHolders.add(portfolio.getBusinessPartnerName());
				portfolioAmounts.add(CurrencyUtil.formatMoney(portfolio.getTotalValue(), portfolio.getCurrencyId()));
			}
		}
		
		intent.putExtra(PortfolioCriterionActivity.EXTRA_PORTFOLIO_NAMES, portfolioNames);
		intent.putExtra(PortfolioCriterionActivity.EXTRA_PORTFOLIO_HOLDERS, portfolioHolders);
		intent.putExtra(PortfolioCriterionActivity.EXTRA_PORTFOLIO_AMOUNTS, portfolioAmounts);
		
		return intent;
	}
	
	@Override
	public void loadData(){				
		TradingService.getTradingDefaults(new RequestStateEvent<TradingDefaultsRequest>(){
			@Override
			public void onRequestCompleted(TradingDefaultsRequest aRequest) {
				TradingDefaultsResult result = aRequest.getResponse().getData();
				if (result == null){
					onRequestFailed(aRequest);
					return;
				}
				mList = result.getPortfolios();
				setReady(true);
			}
		}).initiateServerRequest();		
	}
}
