package com.avaloq.framework.tools.searchwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.DialogFragment;

public class AmountCriteriumActivity extends BankletActivity {
	
	public static final String EXTRA_FROM = "extra_from";
	public static final String EXTRA_TO = "extra_to";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avq_criterium_amount);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null){
			if (extras.containsKey(EXTRA_FROM)){
				int tmp = extras.getInt(EXTRA_FROM, 0);
				if (tmp != 0)
					((EditText)findViewById(R.id.amount_from_value)).setText(String.valueOf(tmp));
			}
			if (extras.containsKey(EXTRA_TO)){
				int tmp = extras.getInt(EXTRA_TO, 0);
				if (tmp != 0)
					((EditText)findViewById(R.id.amount_to_value)).setText(String.valueOf(tmp));
			}
		}
		
		Button button = (Button)findViewById(R.id.button_criterium_ok);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Integer from, to;
				try {
					from = Integer.valueOf(((EditText)findViewById(R.id.amount_from_value)).getText().toString());
				}
				catch (NumberFormatException e){
					from = 0;
				}
				try {
					to = Integer.valueOf(((EditText)findViewById(R.id.amount_to_value)).getText().toString());
				}
				catch (NumberFormatException e){
					to = 0;
				}
				if (from <= to){
					Intent result = new Intent();
					result.putExtra(EXTRA_FROM, (from == null) ? 0 : from);
					result.putExtra(EXTRA_TO, (to == null) ? 0 : to);
					setResult(Activity.RESULT_OK, result);
					finish();
				}
				else {
					DialogFragment.createAlert(getString(R.string.avq_criterium_validation_error_title), getString(R.string.avq_criterium_amount_validation_error)).show();
				}
			}
		});
	}
}
