package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;

import com.avaloq.afs.aggregation.to.payment.PaymentMoneyAccountListResult;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMoneyAccountListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.util.CurrencyUtil;

public abstract class CurrencyCriterion extends SingleChoiceCriterium<CurrencyTO> {
	
	List<CurrencyTO> mList = new ArrayList<CurrencyTO>();
	
	public CurrencyCriterion(Activity activity) {
		super(activity);
		requestCurrencies();
	}

	@Override
	public LinkedHashMap<CurrencyTO, String> getTypeLabelMapping() {
		LinkedHashMap<CurrencyTO, String> map = new LinkedHashMap<CurrencyTO, String>();
		synchronized(mList){
			if (mList != null){
				for (CurrencyTO currency: mList)
					map.put(currency, currency.getName());
			}
		}
		return map;
	}

	@Override
	public String getName() {
		return mActivity.getString(R.string.criterium_currency);
	}
	
	@Override
	public Intent createEmptySelectionIntent(){
		Intent intent = new Intent(mActivity, CurrencyCriterionActivity.class);
		if (mElement != null){
			intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_ELEMENT, mElement.toString());
		}
		
		ArrayList<String> currencyISOs = new ArrayList<String>();
		ArrayList<Long> currencyIds = new ArrayList<Long>();		
		
		if (mList != null){
			for (CurrencyTO currency: mList){
				currencyISOs.add(currency.getIsoCode());
				currencyIds.add(currency.getId());				
			}
		}
		
		intent.putExtra(CurrencyCriterionActivity.EXTRA_CURRECY_ISO, currencyISOs);
		Long[] tmp = new Long[currencyIds.size()];
		currencyIds.toArray(tmp);
		intent.putExtra(CurrencyCriterionActivity.EXTRA_CURRENCY_ID, tmp);		
		
		return intent;
	}
	
	private void requestCurrencies(){
		
		mList = AvaloqApplication.getInstance().getCurrencyList();
	}
}
