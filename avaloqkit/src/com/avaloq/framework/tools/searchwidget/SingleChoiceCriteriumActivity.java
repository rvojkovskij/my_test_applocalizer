package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;

public class SingleChoiceCriteriumActivity extends BankletActivity {
	
	protected String mElement;
	protected ArrayList<String> keys, values;
	
	protected ArrayList<Integer> icons;
	
	public static final String EXTRA_ELEMENT = "extra_element";
	public static final String EXTRA_KEY_LIST = "extra_key_list";
	public static final String EXTRA_VALUE_LIST = "extra_value_list";
	public static final String EXTRA_ICONS_LIST = "EXTRA_ICONS_LIST";
	
	public static final String TAG_PREFIX = "criterium_";
	
	LinearLayout container;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avq_criterium_single_choice);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null)
			throw new IllegalArgumentException("The extras are empty");
		
		initializeElement(extras);
		
		if ( !extras.containsKey(EXTRA_KEY_LIST) || !extras.containsKey(EXTRA_VALUE_LIST) )
			throw new IllegalArgumentException("The keys or values are empty");
		
		initializeKeyValueLists(extras);
		
		if (keys.size() != values.size())
			throw new IllegalArgumentException("The key and value list arrays contain a different number of elements");
		
		container = (LinearLayout)findViewById(R.id.inputFields);
		
		for (int i=0; i<keys.size(); i++){
			View item = getLayoutInflater().inflate(R.layout.avq_single_choice_criterium_item, null, false);
			final String key = keys.get(i);
			final String value = values.get(i);
			item.setTag(TAG_PREFIX + key);
			fillValue((TextView)item.findViewById(R.id.booking_type_value), i, value);
			if (isSelected(key))
				item.findViewById(R.id.booking_type_selected).setVisibility(View.VISIBLE);
			
			// check if needed to set the icon
			if ( icons != null && i < icons.size() && icons.get(i) != null ){
				((ImageView)item.findViewById(R.id.single_choice_icon)).setImageDrawable(getResources().getDrawable(icons.get(i)));
				item.findViewById(R.id.single_choice_icon).setVisibility(View.VISIBLE);
			}
			
			item.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					itemSelected(v, key);
				}
			});
			
			container.addView(item);
			((LinearLayout.LayoutParams)item.getLayoutParams()).bottomMargin = (int)getResources().getDimension(R.dimen.list_padding);
		}
		
		Button button = (Button)findViewById(R.id.button_criterium_ok);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent result = createResultIntent();
				setResult(Activity.RESULT_OK, result);
				finish();
			}
		});
	}
	
	protected void fillValue(TextView view, int index, String value){
		view.setText(value);
	}
	
	protected void initializeElement(Bundle extras){
		if (extras.containsKey(EXTRA_ELEMENT)){
			mElement = extras.getString(EXTRA_ELEMENT);
		}
	}
	
	protected void initializeKeyValueLists(Bundle extras){
		keys = extras.getStringArrayList(EXTRA_KEY_LIST);
		values = extras.getStringArrayList(EXTRA_VALUE_LIST);
		if (extras.containsKey(EXTRA_ICONS_LIST)){
			icons = extras.getIntegerArrayList(EXTRA_ICONS_LIST);
		}
	}
	
	protected Intent createResultIntent(){
		Intent result = new Intent();
		if (mElement != null)
			result.putExtra(EXTRA_ELEMENT, mElement);
		return result;
	}
	
	protected boolean isSelected(View view){
		if (view.getTag().equals(TAG_PREFIX + mElement))
			return true;
		else
			return false;
	}
	
	protected boolean isSelected(String key){
		if (mElement != null && key.equals(mElement))
			return true;
		else
			return false;
	}
	
	protected void itemSelected(View view, String key){
		onItemSelected(view, key);
		for (int i=0; i<container.getChildCount(); i++){
			View child = container.getChildAt(i);
			if (!isSelected(child))
				child.findViewById(R.id.booking_type_selected).setVisibility(View.INVISIBLE);
		}
	}
	
	protected void onItemSelected(View view, String key){
		view.findViewById(R.id.booking_type_selected).setVisibility(View.VISIBLE);
		mElement = key;
	}
}
