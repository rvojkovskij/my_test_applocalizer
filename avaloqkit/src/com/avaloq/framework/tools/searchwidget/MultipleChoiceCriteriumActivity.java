package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import com.avaloq.framework.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MultipleChoiceCriteriumActivity extends SingleChoiceCriteriumActivity{
	
	ArrayList<String> mElements = new ArrayList<String>();
	
	@Override
	protected void initializeElement(Bundle extras){
		if (extras.containsKey(EXTRA_ELEMENT)){
			mElements = extras.getStringArrayList(EXTRA_ELEMENT);
		}
	}
	
	@Override
	protected Intent createResultIntent(){
		Intent result = new Intent();
		if (mElements != null)
			result.putExtra(EXTRA_ELEMENT, mElements);
		return result;
	}
	
	@Override
	protected boolean isSelected(View view){
		for (String element: mElements){
			if (view.getTag().equals(TAG_PREFIX + element))
				return true;
		}
		
		return false;
	}
	
	@Override
	protected boolean isSelected(String key){
		for (String element: mElements){
			if (element != null && key.equals(element))
				return true;
		}
		
		return false;
	}
	
	protected void onItemSelected(View view, String key){
		if (mElements.contains(key)){
			mElements.remove(key);
			view.findViewById(R.id.booking_type_selected).setVisibility(View.INVISIBLE);
		}
		else {
			mElements.add(key);
			view.findViewById(R.id.booking_type_selected).setVisibility(View.VISIBLE);
		}
	}
}
