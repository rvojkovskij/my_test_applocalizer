package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public abstract class SingleChoiceCriterium<Type> extends Criterium {
	protected Type mElement = getDefault();
	
	public SingleChoiceCriterium(Activity activity) {
		super(activity);
	}
	
	@Override
	public View formatSummary() {
		TextView view = new TextView(mActivity);
		
		if (mElement == null) return null;
		
		view.setText(getTypeLabelMapping().get(mElement));
			
		return view;
	}
	
	@Override
	public Intent getSelectionActivity() {
		Intent intent = createEmptySelectionIntent();
		
		ArrayList<String> keys = new ArrayList<String>();
		ArrayList<String> values = new ArrayList<String>();
		
		Iterator<Entry<Type, String>> iterator = getTypeLabelMapping().entrySet().iterator();
		while (iterator.hasNext()){
			Entry<Type, String> element = iterator.next();
			keys.add(element.getKey().toString());
			values.add(element.getValue());
		}
		
		intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_KEY_LIST, keys);
		intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_VALUE_LIST, values);
		
		// pass the icons
		intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_ICONS_LIST, getIcons());
		
		return intent;
	}
	
	public Intent createEmptySelectionIntent(){
		Intent intent = new Intent(mActivity, SingleChoiceCriteriumActivity.class);
		if (mElement != null)
			intent.putExtra(SingleChoiceCriteriumActivity.EXTRA_ELEMENT, mElement.toString());
		return intent;
	}
	
	@Override
	public void onResult(Intent intent){
		if (intent.getExtras() != null){
			if (intent.getExtras().containsKey(SingleChoiceCriteriumActivity.EXTRA_ELEMENT))
				mElement = getTypeByString(intent.getExtras().getString(SingleChoiceCriteriumActivity.EXTRA_ELEMENT));
		}
	}
	
	@Override
	public void clear() {
		mElement = getDefault();
	}
	
	protected Type getTypeByString(String value){
		Iterator<Entry<Type, String>> iterator = getTypeLabelMapping().entrySet().iterator();
		while (iterator.hasNext()){
			Entry<Type, String> element = iterator.next();
			if (element.getKey().toString().equals(value))
				return element.getKey();
		}
		
		throw new IllegalArgumentException("String " + value + " not found while traversing type map");
	}
	
	public Type getDefault() {
		return null;
	}
	
	public abstract LinkedHashMap<Type, String> getTypeLabelMapping();
	
	public ArrayList<Integer> getIcons(){
		return null;
	}
	
	@Override
	public Bundle saveToBundle() {
		Bundle bundle = new Bundle();
		if (mElement != null)
			bundle.putString("mElement", mElement.toString());
		return bundle;
	}
	
	@Override
	public void restoreFromBundle(Bundle in) {
		if (in == null)
			return;
		if (in.containsKey("mElement")){
			mElement = getTypeByString(in.getString("mElement"));
		}
	}

}
