package com.avaloq.framework.tools.searchwidget;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.framework.R;

public class AccountCriteriumActivity extends MultipleChoiceCriteriumActivity{
	
	public static final String EXTRA_ACCOUNT_NAMES = "EXTRA_ACCOUNT_NAMES";
	public static final String EXTRA_ACCOUNT_IBANS = "EXTRA_ACCOUNT_IBANS";
	public static final String EXTRA_ACCOUNT_AMOUNTS = "EXTRA_ACCOUNT_AMOUNTS";
	
	ArrayList<String> accountNames = new ArrayList<String>();
	ArrayList<String> accountIbans = new ArrayList<String>();
	ArrayList<String> accountAmounts = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	protected void initializeKeyValueLists(Bundle extras) {
		super.initializeKeyValueLists(extras);
		
		if (extras.containsKey(EXTRA_ACCOUNT_NAMES))
			accountNames = extras.getStringArrayList(EXTRA_ACCOUNT_NAMES);
		if (extras.containsKey(EXTRA_ACCOUNT_IBANS))
			accountIbans = extras.getStringArrayList(EXTRA_ACCOUNT_IBANS);
		if (extras.containsKey(EXTRA_ACCOUNT_AMOUNTS))
			accountAmounts = extras.getStringArrayList(EXTRA_ACCOUNT_AMOUNTS);
	}
	
	@Override
	protected void fillValue(TextView view, int index, String value) {
		ViewGroup parent = (ViewGroup) view.getParent();
	    int ind = parent.indexOfChild(view);
	    parent.removeView(view);
		
		View layout = getLayoutInflater().inflate(R.layout.avq_account_criterium_item, parent, false);
		LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)layout.getLayoutParams();
		params.width = 0;
		params.weight = 1;
		((TextView)layout.findViewById(R.id.account_name)).setText(accountNames.get(index));
		((TextView)layout.findViewById(R.id.account_iban)).setText(accountIbans.get(index));
		((TextView)layout.findViewById(R.id.account_amount)).setText(accountAmounts.get(index));
		
	    parent.addView(layout, ind);
	}
}
