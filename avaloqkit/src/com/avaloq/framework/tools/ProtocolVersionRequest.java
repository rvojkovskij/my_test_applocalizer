package com.avaloq.framework.tools;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;
import com.avaloq.framework.comms.http.HttpConstants;

public class ProtocolVersionRequest extends AbstractHTTPServerRequest<Integer> {
	
	private static final String TAG = ProtocolVersionRequest.class.getSimpleName();
	
	private static final String PROTOCOL_VERSION_INFO_URL_SEGMENT = "afs-mobile/version.properties";
	
	private static final String KEY_PROTOCOL_VERSION = "ProtocolVersion";
	
	public ProtocolVersionRequest(RequestStateEvent<ProtocolVersionRequest> rse) {
		super(rse);
	}
	
	@Override
	public AbstractHTTPServerResponse<Integer> createEmptyResponseObject() {
		return new ProtocolVersionResponse();
	}

	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_GET;
	}

	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getBaseUrl() + PROTOCOL_VERSION_INFO_URL_SEGMENT;
	}
	
	@Override
	public Map<String, String> getHttpHeaders() {
		return new HashMap<String,String>();
	}

	@Override
	public String getRequestBody() {
		return null;
	}

	/**
	 * FIXME
	 * In the main implementation the newlines were missing in the returned string, so i override the method here with the correct behaviour.
	 * The reason we don't do it in the main implementation is, we don't want to break anything so short before the release.
	 * In the long run, this code should replace the one in {@link AbstractHTTPServerRequest}.
	 */
	@Override
	protected void readHTTPBody(AbstractHTTPServerResponse<Integer> aResponse, InputStream in) throws IOException {
		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(new InputStreamReader(in));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
			sb.append("\n");
		}
		rd.close();

		// Store the HTTP response body
		aResponse.setHTTPResponseBody(sb.toString());
	}
	
	class ProtocolVersionResponse extends AbstractHTTPServerResponse<Integer> {

		@Override
		public Integer onParseServerResponseData() throws IllegalArgumentException {
			String responseBody = getHTTPResponseBody();
			String[] lines = responseBody.split("\r?\n");
			String completeKey = KEY_PROTOCOL_VERSION.concat("=");
			for(String line : lines) {
				if(line.startsWith(completeKey)) {
					// return Integer.valueOf(line.substring(completeKey.length()));
					return 1;
				}
				Log.d(TAG, "Line: " + line);
			}
			throw new IllegalArgumentException("Cannot find the key '" + KEY_PROTOCOL_VERSION + "'");
		}
		
	}

}
