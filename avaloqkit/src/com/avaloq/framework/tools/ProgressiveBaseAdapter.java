package com.avaloq.framework.tools;

import com.avaloq.framework.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;

abstract public class ProgressiveBaseAdapter extends BaseAdapter{

	protected int mResultPerPage = 20;
	
	// Number of rows until the end of the list that will trigger
	// next page load
	protected int mThreshold = 5;
	
	protected Context mContext;
	
	protected View mFooterView;
	
	protected ListView mListView;
	
	protected int ignoreUntil = -1;
	
	public ProgressiveBaseAdapter(Context context){
		mContext = context;
		mFooterView = LayoutInflater.from(mContext).inflate(R.layout.avq_list_loading, null);
	}
	
	/**
	 * 
	 * @return
	 */
	public int getThreshold() {
		return mThreshold;
	}

	/**
	 * 
	 * @param mThreshold
	 */
	public void setThreshold(int mThreshold) {
		this.mThreshold = mThreshold;
	}

	/**
	 * 
	 * @return
	 */
	public int getResultPerPage() {
		return mResultPerPage;
	}

	/**
	 * 
	 * @param mResultPerPage
	 */
	public void setResultPerPage(int mResultPerPage) {
		this.mResultPerPage = mResultPerPage;
	}

	/**
	 * 
	 * @param position
	 */
	protected void checkIfMustLoadNextData(int position){	
		// Check if the rows that should trigger data load are visible
		// also prevent calling the event if one of these rows is redrawn
		// check if there is at least one page loaded
		if (position >= getCount()-mThreshold && position > ignoreUntil && getCount() >= mResultPerPage && getCount() % mResultPerPage == 0){ 
			triggerNextPageLoad();
			ignoreUntil = getCount();
		}
	}

	/**
	 * 
	 */
	private void triggerNextPageLoad(){
		loadNext();
	}
	
	public void showListLoading(boolean visible){		
		
		loadingStateChanged(visible);
		
		if (mListView == null) mListView = getListView();
		
		if (visible){	
			Log.d("DDD", "Show loading");
			if (mListView.getFooterViewsCount() == 0){
				mListView.addFooterView(mFooterView);
			}
		}else{
			mListView.removeFooterView(mFooterView);
		}
	}
	
	public void reset(){
		ignoreUntil = -1;
	}
	
	/**
	 * 
	 */
	public abstract void loadNext();
	
	/**
	 * 
	 * @return
	 */
	public abstract ListView getListView();
	
	/**
	 * @return 
	 * 
	 */
	public abstract void loadingStateChanged(boolean loading);
}
