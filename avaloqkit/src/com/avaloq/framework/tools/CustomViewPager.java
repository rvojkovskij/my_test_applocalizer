/**
 * A class that resolves the scroll bug when a ViewPager is placed
 * within a vertical ScrollView.
 */
package com.avaloq.framework.tools;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ScrollView;

import com.viewpagerindicator.PageIndicator;

public class CustomViewPager extends ViewPager{
	
	/**
	 *  Let the parent ScrollView intercept the touch event if the initial vertical scroll
	 *  is at least so many times bigger than the horizontal scroll. Necessary so that the
	 *  vertical ScrollView can be used even from within the ViewPager 
	 */
	protected static final int X_TO_Y_RATIO = 7;
	
	ViewGroup parent;
	
	/**
	 *  The minimum scroll distance below which the touch event is considered to be
	 *  a normal click, instead of scroll.
	 */
	int minDistance;
	float initX, initY;

	public CustomViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		// get the device-specific minDistance
		ViewConfiguration vc = ViewConfiguration.get(context);
		minDistance = vc.getScaledTouchSlop();
		
		// Detect the ScrollView amongst the ascendants of the ViewPager.
		// This can only be done once the ViewPager is completely inflated and visible.
		// That is why we are using the post-function.
		post(new Runnable() {
			@Override
			public void run() {
				View candidate = (View)getParent();
				while ( (!(candidate instanceof ScrollView || candidate instanceof ListView)) && candidate != null){
					if (!(candidate.getParent() instanceof View))
						return;
					candidate = (View)candidate.getParent();
				}
				parent = (ViewGroup)candidate;
			}
		});
	}
	
	@Override
	public final boolean onInterceptTouchEvent(MotionEvent event) {
		// If it is the beginning of an event-chain, save the initial touch coordinates
		if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_CANCEL){
			initX = event.getX();
			initY = event.getY();
		}
		
		// This logic for intercepting the motion is so complicated because we have to watch for the simple click
		// event within the ViewPager (this motion should not get intercepted).
		if (event.getAction() == MotionEvent.ACTION_MOVE && ( (scrollDistance(event) > minDistance) || shouldStopParent(event) ))
			return true;
		else{
			if (shouldStopParent(event))
				return true;
			return super.onInterceptTouchEvent(event);
		}
	}
	
	protected boolean shouldStopParent(MotionEvent event){
		return Math.abs(event.getX()-initX)*X_TO_Y_RATIO > Math.abs(event.getY() - initY );
	}
	
	protected float scrollDistance(MotionEvent event){
		return (float) Math.sqrt(Math.pow((event.getX()-initX), 2) + Math.pow((event.getY()-initY), 2));
	}
	
	/**
	 * If the touch motion has been dispatched to the ViewPager's onTouchEvent,
	 * then stop the parent ScrollView from interacting. Also request that the rest
	 * of the event-chain is sent to the ViewPager directly (by returning true).
	 */
	@Override
	public final boolean onTouchEvent(MotionEvent event) {
		if (parent != null)
			parent.requestDisallowInterceptTouchEvent(true);
		super.onTouchEvent(event);
		return true;
	}
	/**
	 * The first ScrollView in the ascendant-chain gets selected as the ViewPager contender for the
	 * interception of touch events, but if a different ScrollView is necessary, you can use this function.
	 * @param the ScrollView
	 */
	public void setScrollView(ViewGroup v){
		parent = v;
	}
	
	public void setIndicator(Activity activity, int id){
		PageIndicator indicator = (PageIndicator)activity.findViewById(id);
		if (indicator != null){
			if (getAdapter() != null && getAdapter().getCount() > 1)
				indicator.setViewPager(this);
			else
				((View)indicator).setVisibility(View.GONE);
		}
	}

}