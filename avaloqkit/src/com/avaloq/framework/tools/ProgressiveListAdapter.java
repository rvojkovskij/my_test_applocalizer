package com.avaloq.framework.tools;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public abstract class ProgressiveListAdapter<Element> extends ArrayAdapter<Element>{
	
	protected int maxScrollPosition = 0;
	ProgressiveListView mListView;
	//List<Element> mObjects;
	
	boolean mStop = false;

	public ProgressiveListAdapter(Context context, ProgressiveListView listView, int resource, int textViewResourceId, List<Element> objects) {
		super(context, resource, textViewResourceId, objects);
		mListView = listView;
		/*if (objects.size() == 0)
			onEndScrollToBottom();*/
	}
	
	public boolean checkForEndResults(int count){		
		if (count == 0){
			return true;
		}
		if (count-getCount() < mListView.getCountElementsPerPage())
			return true;
		
		return false;
	}
	
	public void onEndScrollToBottom() {
		mListView.removeFooterView();
		mStop = true;
	}
	
	public void setElements(List<?> objects){
		mStop = false;
		
		if (objects == null){
			clear();
			mStop = true;
		}
		else {
			if (checkForEndResults(objects.size()))
				mStop = true;
			
			clear();
				
			for (Object obj: objects){
				Element element = (Element)obj;
				add(element);
			}
		}
		
		if (mStop)
			onEndScrollToBottom();
		notifyDataSetChanged();
	}

	@Override
	public final View getView(int position, View convertView, ViewGroup parent) {
		if (position+1 == getCount() && !mStop)
			scrolledToBottom(position, convertView, parent);
		return createView(position, convertView, parent);
	}
	
	protected void scrolledToBottom(int position, View convertView, ViewGroup parent) {
		if (maxScrollPosition < position){
			maxScrollPosition = position;
			mListView.onScrollToBottom(position, convertView, parent);
		}
	}
	
	public void setMaxScrollPosition(int aPostion){
		maxScrollPosition = aPostion;
	}
	
	public int getMaxScrollPosition() {
		return maxScrollPosition;
	}
	
	protected abstract View createView(int position, View convertView, ViewGroup parent);
}
