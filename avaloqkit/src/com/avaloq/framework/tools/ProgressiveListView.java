package com.avaloq.framework.tools;

import java.util.List;

import com.avaloq.framework.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ProgressiveListView extends ListView{
	View mFooterView;
	ProgressiveListAdapter<?> mAdapter;
	Runnable refreshDataCallback;
	
	int currentPage = 0;
	int elementsPerPage = getCountElementsPerPage();

	public ProgressiveListView(Context context) {
		super(context);
		initialize();
	}

	public ProgressiveListView(Context context, AttributeSet attrs) {		
		super(context, attrs);
		initialize();
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public ProgressiveListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initialize();
	}
	
	protected void initialize(){
		mFooterView = LayoutInflater.from(getContext()).inflate(R.layout.avq_list_loading, null);
		addFooterView(mFooterView);
	}
	
	public int getCountElementsPerPage(){
		return getContext().getResources().getInteger(R.integer.list_count_elements_per_page);
	}
	
	public void init(Runnable refreshDataRunnable){
		refreshDataCallback = refreshDataRunnable;
		/*if (refreshDataRunnable != null)
			refreshDataCallback.run();*/
	}
	
	public void refresh(){
		mAdapter.clear();
		mAdapter.notifyDataSetChanged();
		mAdapter.setMaxScrollPosition(0);
		currentPage = 0;
		if (getFooterViewsCount() == 0)
			addFooterView(mFooterView);
		if (refreshDataCallback != null)
			refreshDataCallback.run();
	}
	
	public void setElements(List<?> objects){
		mAdapter.setElements(objects);
	}
	
	public final void setAdapter(ProgressiveListAdapter<?> adapter){
		mAdapter = adapter;
		super.setAdapter(adapter);
	}
	
	/*public final void setAdapter(ProgressiveListAdapter<?> adapter){
		if (adapter != null && mAdapter != null){
			adapter.setMaxScrollPosition(mAdapter.getMaxScrollPosition());
		}
		
		if (adapter != null && mAdapter != null && mAdapter.checkForEndResults(adapter.getCount())){
			mAdapter = adapter;
			mAdapter.onEndScrollToBottom();
			super.setAdapter(adapter);
			return;
		}
		else {
			mAdapter = adapter;
			//setAdapter(mAdapter, aReqId);
			currentPage++;
		}
		
		if (getFooterViewsCount() == 0)
			addFooterView(mFooterView);
		
		int index = getFirstVisiblePosition();
		View v = getChildAt(0);
		int top = (v == null) ? 0 : v.getTop();

		super.setAdapter(null);
		super.setAdapter(adapter);
		
		setSelectionFromTop(index, top);
		// Empty views don't need infinite scroll
		if (adapter.getCount() == 0)
			removeFooterView(mFooterView);
		
		// If it is the first request, it must be explicitly checked, because
		// checkforEndResults has not been executed yet (mAdapter empty).
		if (adapter.getCount() < getCountElementsPerPage()){
			mAdapter.onEndScrollToBottom();
		}
	}*/
	
	public void removeFooterView(){
		removeFooterView(mFooterView);
	}
	
	public void onScrollToBottom(int position, View convertView, ViewGroup parent) {
		currentPage++;
		if (refreshDataCallback != null)
			refreshDataCallback.run();
		//fetchData();
	}
	
	public int getCurrentPage(){
		return currentPage;
	}
	
	public int getElementsPerPage() {
		return elementsPerPage;
	}
	
	public int getStartIndex(){
		return 0;
	}
	
	public int getEndIndex(){
		return (getCurrentPage()+1)*getElementsPerPage();
	}
}
