package com.avaloq.framework.tools;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.avaloq.framework.R;

/**
 * ImageView for a refresh spinner
 * @author zahariev
 *
 */
public class RefreshSpinner extends ImageView{
	
	boolean isRefreshing = false;
	/**
	 * The spinner animation. Created and cached the first time the spinner is started.
	 */
	Animation mAnimation;

	public RefreshSpinner(Context context) {
		super(context);
	}
	
	public RefreshSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public RefreshSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	/**
	 * Start the spinner animation if it is not currently running.
	 */
	public void start(){
		if (!isRefreshing){
			isRefreshing = true;
			if (mAnimation == null){
				mAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.avq_spinner_actionbar);
				mAnimation.setRepeatCount(Animation.INFINITE);
			}
			startAnimation(mAnimation);
		}
	}
	
	/**
	 * Stop the spinner animation
	 */
	public void stop(){
		clearAnimation();
        isRefreshing = false;
	}
	
	/**
	 * Determines if the spinner animation is currently running.
	 * @return
	 */
	public boolean isRefreshing(){
		return isRefreshing;
	}

}
