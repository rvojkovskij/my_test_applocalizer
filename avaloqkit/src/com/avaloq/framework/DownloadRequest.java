package com.avaloq.framework;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.comms.http.AbstractHTTPServerResponse;
import com.avaloq.framework.comms.http.HttpConstants;

/**
 * A request that downloads a document from the Document Safe.
 * This class is a specialized version of {@link AbstractServerRequest}. It will use the same authentication mechanisms
 * as the other webservice requests.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class DownloadRequest extends AbstractHTTPServerRequest<File> {
	
	static class DocumentDownloadEvent extends RequestStateEvent<DownloadRequest> {
	} 
	
	private static final String TAG = DownloadRequest.class.getSimpleName();

	/**
	 * The Fragment that causes the click event
	 */
	private final Activity mActivity;

	/**
	 * The ID of the Document (used in the download URL)
	 */
	private final long mDocumentId;
	
	/**
	 * The Destination file
	 */
	private File destFile = null;
	
	/**
	 * Creates a new instance of a Document Download
	 * @param aDocumentSafeFragment The calling fragment
	 * @param aId The ID of the Document
	 * @param aDocumentDownloadEvent The special RequestStateEvent implementation
	 */
	public DownloadRequest(Activity activity, long aId, DocumentDownloadEvent aDocumentDownloadEvent) {
		super(aDocumentDownloadEvent);
		mActivity = activity;
		mDocumentId = aId;
		// TODO check if we can and should make cache work with binary data
		setCachePolicy(CachePolicy.NO_CACHE);
	}
	
	/**
	 * Reads the HTTP Body.
	 * Usually, the HTTP Body is interpreted as a string, but in this case we have binary data, so we use a special logic here.
	 * In this case we write the binary contents to a cache file that can be opened by other apps.
	 */
	@Override
	protected void readHTTPBody(AbstractHTTPServerResponse<File> aResponse, InputStream inputStream) throws IOException {
		try{
			File cacheDir = mActivity.getCacheDir();
			if(cacheDir == null) {
				Log.d(TAG, "Cannot get cache dir.");
				return;
			}
			if(!cacheDir.isDirectory()) {
				cacheDir.mkdirs();
			}
	
			destFile = File.createTempFile("document", ".pdf", cacheDir);
			destFile.deleteOnExit();
			Log.d(TAG, destFile.getAbsolutePath());
			OutputStream outputStream  = new FileOutputStream(destFile);
//			long totalRead = 0;
//			StringBuffer buf = new StringBuffer();
			int read = 0;
			byte[] buffer = new byte[1024];
			while((read = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, read);
//				totalRead += read;
//				for(int i = 0; i < read;i++) {
//					buf.append((char)buffer[i]);
//				}
//				char c = (char)read;
//				buf.append(c);
			}
			outputStream.flush();
			outputStream.close();
//			Log.d(TAG, "Total read: " + totalRead + " bytes: " + buf.toString());
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * Creates the avq_activity_empty response object.
	 * In this case, we already created the file, so we just have to return it in {@link AbstractHTTPServerResponse#onParseServerResponseData()}.
	 */
	@Override
	public AbstractHTTPServerResponse<File> createEmptyResponseObject() {
		return new AbstractHTTPServerResponse<File>() {
			@Override
			public File onParseServerResponseData() throws IllegalArgumentException {
				return destFile;
			}
			
		};
	}

	/**
	 * For document downloads the request method is GET.
	 */
	@Override
	public String getRequestMethod() {
		return HttpConstants.METHOD_GET;
	}

	/**
	 * We can just return the URL here and append the Document ID.
	 * TODO We probably will have to parameterize the Request URL.
	 */
	@Override
	public String getRequestURL() {
		return AvaloqApplication.getInstance().getConfiguration().getBaseUrl() + "/frontAggregationServices/DocumentDownload?id=" + mDocumentId;
	}

	/**
	 * No special HTTP headers are required
	 */
	@Override
	public Map<String, String> getHttpHeaders() {
		return new HashMap<String,String>();
	}

	/**
	 * No HTTP request body for GET requests
	 */
	@Override
	public String getRequestBody() {
		return "";
	}

//	/**
//	 * TODO Return the correct WS type here
//	 */
//	@Override
//	public String getWebserviceType() {
//		return DownloadRequest.class.getSimpleName();
//	}
//
//	/**
//	 * The version is set to 1.0 in this case.
//	 */
//	@Override
//	public String getWebserviceVersion() {
//		return "1.0";
//	}

}
