package com.avaloq.framework;

import android.app.ListActivity;
import android.os.Bundle;

public abstract class ListBankletActivity extends ListActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	/**
	 * Banklets set their layout within the Banklet container. It will be loaded 
	 * into the banklet template
	 */
	@Override
	public void setContentView(int layoutResID) {
		BankletActivityDelegate.setContentView(this, layoutResID);
	}
	
	/**
	 * Return the Banklet instance this activity belongs to
	 * @return
	 */
	public abstract Banklet getBanklet();
}
