package com.avaloq.framework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.RequestCache;
import com.avaloq.framework.comms.authentication.AbstractAuthenticationHandler;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.tools.ProtocolVersionRequest;
import com.avaloq.framework.tools.RootCheck;
import com.avaloq.framework.ui.ActivityNavigationItem;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.DialogActivity;
import com.avaloq.framework.ui.LogoutActivity;
import com.avaloq.framework.ui.NavigationItem;

/**
 * Global (singleton) application instance. 
 * 
 * @author bachi
 *
 */
@ReportsCrashes(formKey = "")
public class AvaloqApplication extends android.app.Application {
	
	private static final Integer AVQ_CLIENT_PROTOCOL_VERSION_NUMBER = 1;

	private static final String TAG = AvaloqApplication.class.getSimpleName();

	private static AvaloqApplication mInstance;

	private AppConfigurationInterface mConfiguration;
	private List<Banklet> mBanklets = new ArrayList<Banklet>();
	private List<ActivityNavigationItem> mMainNavigationItems = new ArrayList<ActivityNavigationItem>();
	private ActivityNavigationItem mCurrentMainNavigationItem;
	private List<AbstractAuthenticationHandler> mAuthHandlers = new ArrayList<AbstractAuthenticationHandler>();
	/** FIXME: still nedded? **/
	private List<CurrencyTO> mCurrencyList = null;
	private boolean mInitialized = false;
	private Activity mRootActivity;
	
	private List<Runnable> mRunAfterInitialisation = new ArrayList<Runnable>();

	/**
	 * singleton - private constructor
	 */
	public AvaloqApplication() {
		super();
		mInstance = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(TAG, "onCreate");
		if(getResources().getBoolean(R.bool.avq_send_bugesense_reports)) {
			String formUri = getResources().getString(R.string.avq_bugsense_id);
			ACRA.getConfig().setFormUri("http://www.bugsense.com/api/acra?api_key=" + formUri); 
			ACRA.init(this);
		}
	}

	public static AvaloqApplication getInstance() {
		if (mInstance == null) {
			// This is bad practice and can lead to severe errors (context will be null etc...)
			// mInstance = new AvaloqApplication();
			// rather point out to the programmer that something has gone wrong badly
			throw new RuntimeException("getInstance was called, but the application object does not exist!");
		}
		return mInstance;
	}

	/**
	 * Get this app's application context
	 * @return
	 */
	public static Context getContext() {
		return mInstance.getApplicationContext();
	}

	/**
	 * Closes the software keyboard if necessary
	 * @param activity - the current activity
	 */
	public static void closeSoftKeyboard(Activity activity){
		try {
			((InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
		}
		catch (NullPointerException e){
			// activity.getCurrentFocus() ----> no current focus. No need to do anything
		}
	}

	/** 
	 * Initialize the app: Load the configuration, the banklets etc.
	 * @param context
	 */
	public void initialize(final Activity rootActivity, AppConfigurationInterface config) {
		
		if(mInitialized) {
			return;
		}
		
		// Store the task root activity so we can go back to it when exiting the app.
		mRootActivity = rootActivity;
		
		// Initialize and store the global app configuration
		mConfiguration = config;
		
		if(mConfiguration.getBaseUrl() == null) {
			Log.d(TAG, "Base URL is not yet set... aborting AvaloqApplication.initialize().");
			return;
		}
		
		if (RootCheck.isDeviceRooted()){
			DialogActivity.show(
					getString(R.string.avq_root_check_title),
					getString(R.string.avq_root_check_text),
					getString(R.string.avq_root_check_dismiss),
					true
			);
			mRootActivity.finish();
		}else{
			checkClientProtocol();
		}
	}
	
	private void checkClientProtocol() {
		new AsyncTask<Void, Void, ProtocolVersionRequest>() {

			@Override
			protected ProtocolVersionRequest doInBackground(Void... params) {
				ProtocolVersionRequest request = new ProtocolVersionRequest(null);
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.executeRequest();
				return request;
			};
			
			@Override
			protected void onPostExecute(ProtocolVersionRequest result) {
				if(result != null && result.getResponse() != null) {
					result.getResponse().parseServerResponseData();
					if(result.getResponse().wasRequestSuccessfullyExecuted()) {
						Integer serverProtocolVersion = result.getResponse().getData();
						Log.d(TAG, "The server protocol version is " + serverProtocolVersion);
						if(serverProtocolVersion < 1) {
							Log.d(TAG, "log 1");
							clientProtocolCheckFailedWithOldServer();
						} else if(serverProtocolVersion == AVQ_CLIENT_PROTOCOL_VERSION_NUMBER) {
							Log.d(TAG, "The Server version check succeeded.");
							clientProtocolCheckSuccessful();
						} else {
							Log.d(TAG, "log 2");
							clientProtocolCheckFailedWithOldClient();
						}
					} else {
						Log.d(TAG, "log 3");
						clientProtocolCheckFailedWithOldServer();
					}
				} else {
					DialogActivity.showNetworkError();
				}
			}
			
		}.execute();
	}
	
	private void bankingMessagesSuccessful(){
		QueueManager.resetInstance();
		
		// Initialize all configured banklets
		if(mBanklets.size() == 0) {
			for (Class<?> bankletClass : mConfiguration.getBanklets()) {
				try {
					mBanklets.add((Banklet) bankletClass.newInstance());
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}

		// Initialize the global main navigation elements
		// Usually a banklet has only 1 entry, but we offer the possibility for more.
		if(mMainNavigationItems.size()  == 0) {
			for (Banklet banklet : mBanklets) {
				NavigationItem items[]  = banklet.getMainNavigationItems();
				if (items != null) {
					Log.v(TAG, "Adding " + items.length + " navigation items for banklet " + banklet.getClass().getSimpleName());
					mMainNavigationItems.addAll(Arrays.asList(banklet.getMainNavigationItems()));
				}
			}

			// Add the logout link
			ActivityNavigationItem logout = new ActivityNavigationItem(AvaloqApplication.getContext(), LogoutActivity.class, R.string.avq_logout, R.attr.IconLogout) {
				/**
				 * Clear the task (previous activity instances). Only possible as of API 11.
				 */
				@Override
				@TargetApi(11)
				public void onClick(Intent intent) {
					if(Build.VERSION.SDK_INT >= 11) {
						intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_CLEAR_TASK);
					}
					intent.setFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
					Log.i(TAG, "Clear task (activity instances).");
					super.onClick(intent);
				}
			};
			mMainNavigationItems.add(logout);
		}

		// Initialize the available authentication handlers
		if(mAuthHandlers.size() == 0) {
			for (Class<?> authClass : mConfiguration.getAuthenticationHandlers()) {
				try {
					mAuthHandlers.add((AbstractAuthenticationHandler) authClass.newInstance());
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		synchronized (mRunAfterInitialisation) {
			Iterator<Runnable> iterator = mRunAfterInitialisation.iterator();
			while(iterator.hasNext()) {
				iterator.next().run();
			}
			mRunAfterInitialisation.clear();

			mInitialized  = true;
		}
	}

	
	private void clientProtocolCheckSuccessful() {
		bankingMessagesSuccessful();
		/*new AsyncTask<Void, Void, MessagingRequest>() {

			@Override
			protected MessagingRequest doInBackground(Void... params) {
				MessagingRequest request = new MessagingRequest(null);
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.executeRequest();
				return request;
			};
			
			@Override
			protected void onPostExecute(MessagingRequest result) {
				if(result != null && result.getResponse() != null) {
					result.getResponse().parseServerResponseData();
					if(result.getResponse().wasRequestSuccessfullyExecuted()) {
						MessagingResult message = result.getResponse().getData();
						if (message == null)
							bankingMessagesSuccessful();
						else {
							showBankingMessage(message.getMessage(), message.getOnline());
						}
					}
					else{
						// TODO: Show network error in production mode
						bankingMessagesSuccessful();
						//DialogActivity.showNetworkError();
					}
				}
				else{
					// TODO: Show network error in production mode
					bankingMessagesSuccessful();
					//DialogActivity.showNetworkError();
				}
			}
			
		}.execute();*/
	}
	
	private void showBankingMessage(String message, final boolean online){
		DialogActivity.showWithCallback(getString(R.string.avq_banking_message_title), message, getString(R.string.avq_version_check_dismiss), new Runnable(){
			@Override
			public void run() {
				if (!online)
					mRootActivity.finish();
				else
					bankingMessagesSuccessful();
			}
			
		});
	}
	
	private void clientProtocolCheckFailedWithOldClient() {
		DialogActivity.show(
				getString(R.string.avq_version_check_title),
				getString(R.string.avq_version_check_old_client_version),
				getString(R.string.avq_version_check_dismiss),
				true
		);
		mRootActivity.finish();
	}
	
	private void clientProtocolCheckFailedWithOldServer() {
		DialogActivity.show(
				getString(R.string.avq_version_check_title),
				getString(R.string.avq_version_check_old_client_version),
				getString(R.string.avq_version_check_dismiss),
				true
		);
		mRootActivity.finish();
	}


	/**
	 * Get the current network data connection state.
	 */
	public boolean isNetworkConnected() {
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		return (networkInfo != null && networkInfo.isConnected());
	}

	/**
	 * @return the configuration
	 */
	public AppConfigurationInterface getConfiguration() {
		if(mConfiguration == null) {
			// The configuration is missing. This means, the application has not been initialized yet.
			throw new RuntimeException("The configuration is missing.");
		}
		return mConfiguration;
	}

	/**
	 * @return the banklets
	 */
	public List<Banklet> getBanklets() {
		return mBanklets;
	}

	/**
	 * @return the main navigation entries
	 */
	public List<ActivityNavigationItem> getMainNavigationItems() {
		return mMainNavigationItems;
	}

	/**
	 * @return the currentMainNavigationItem 
	 */
	public NavigationItem getCurrentMainNavigationItem() {				
		return mCurrentMainNavigationItem;
	}

	/**
	 * @param currentMainNavigationItem 
	 */
	public void setCurrentMainNavigationItem(ActivityNavigationItem currentMainNavigationItem) {
		this.mCurrentMainNavigationItem = currentMainNavigationItem;
	}


	/**
	 * Get all configured authentication handler instances
	 * @return
	 */
	public List<AbstractAuthenticationHandler> getAuthenticationHandlers() {
		return mAuthHandlers;
	}

	public void navigateToActivity(Activity context, ActivityNavigationItem navigationItem) {
		navigateToActivity(context, navigationItem, null);
	}

	public void navigateToActivity(Activity context, ActivityNavigationItem navigationItem, Bundle extras) {
		setCurrentMainNavigationItem(navigationItem);
		Intent intent = new Intent(context, navigationItem.getTarget());
		if(extras != null) {
			intent.putExtras(extras);
		}
		navigationItem.onClick(intent);
		context.startActivity(intent);
		// TODO Maybe we need to be able to configure this.
		context.overridePendingTransition(0, 0);
	}

	/**
	 * Delete all application data: Pending requests/responses, cached data and
	 * also ask all banklets to do so. 
	 */
	public void clearAllData() {

		// clear currency list
		mCurrencyList = null;

		// Empty the http cookie store
		AbstractHTTPServerRequest.emptyCookieStore();

		// Remove all requests / responses from the queue
		QueueManager.getInstance().emptyQueues();

		// Empty the request cache
		RequestCache.getInstance().evictAll();
		Log.i(TAG, "Request cache emptied.");

		// Ask all banklets to delete their local data
		for (Banklet banklet : mBanklets) {
			banklet.onEmptyCache();
			Log.i(TAG, banklet.getClass().getSimpleName() + ": data emptied.");
		}

		AuthenticationManager.getInstance().logoutAll();

	}

	/**
	 * Start the initial activity of the first banklet.
	 * Called when starting and after a logout.
	 * @param context
	 */
	public void startFirstActivity(Activity context) {
		Bundle bundle = new Bundle();
		bundle.putBoolean(BankletActivity.EXTRA_LAUNCHED_FROM_LOGIN, true);
		Banklet firstBanklet = (Banklet) AvaloqApplication.getInstance().getBanklets().get(0);
		AvaloqApplication.getInstance().navigateToActivity(context, firstBanklet.getInitialActivity(), bundle);
	}

	/**
	 * Run the runnable parameter on the UI thread.
	 * @param action
	 */
	public void runOnUiThread(Runnable action) {

		// Get a handler that can be used to post to the main thread
		Handler mainHandler = new Handler(getContext().getMainLooper());
		mainHandler.post(action);		
	}

	/**
	 * Finds a currency for a specified id
	 * @param id The currency id
	 * @return The currency for the specified id
	 */
	public CurrencyTO findCurrencyById(long id) {
		for(CurrencyTO currency : mCurrencyList) {
			if(currency.getId() == id) {
				return currency;
			}
		}
		throw new RuntimeException("The currency with the id '" + id + "' was not found.");
	}

	/**
	 * Finds a currency id a specified iso code
	 * @param isoCode The currency ISO Code
	 * @return The currency for the specified id
	 */
	public long findCurrencyByISOCode(String isoCode) {
		for(CurrencyTO currency : mCurrencyList) {
			if(currency != null && currency.getIsoCode() != null && currency.getIsoCode().compareTo(isoCode) == 0) {
				return currency.getId();
			}
		}
		throw new RuntimeException("The currency with the ISO code '" + isoCode + "' was not found.");
	}
	
	public List<CurrencyTO> getCurrencyList() {
		return mCurrencyList;
	}

	/**
	 * Sets the list of available currencies
	 * @param currencyList The list of currencies
	 */
	public void setCurrencyList(List<CurrencyTO> currencyList) {
		mCurrencyList = currencyList;
	}

	//	private boolean initialLoginDone = false;
	//
	//	public boolean isInitialLoginDone() {
	//		return initialLoginDone;
	//	}
	//
	//	public void setInitialLoginDone(boolean done) {
	//		initialLoginDone = done;
	//	}
	//
	//	private boolean isLoginProcessStarted = false;
	//
	//	public boolean isStarting() {
	//		return isLoginProcessStarted;
	//	}
	//
	//	public void setStarting(boolean started) {
	//		isLoginProcessStarted = started;
	//	}

	/*
	public void logout(final Context context) {
		
		
	}
	*/

	/**
	 * Get the locale string in the form of e.g. "de-CH"
	 * @return locale
	 */
	public String getLocale() {
		return Locale.getDefault().getLanguage()+"-"+Locale.getDefault().getCountry();
	}

	public void runWithInitializedApp(Runnable runnable) {
		if(mInitialized) {
			runnable.run();
		} else {
			synchronized (mRunAfterInitialisation) {
				mRunAfterInitialisation.add(runnable);
			}
		}
	}

	public static void shutdown() {
		AuthenticationManager.getInstance().authenticationCancelledByUser();
		BankletActivity.exitApplication();
		getInstance().mInitialized = false;
	}
	
	public static boolean shouldDisplayPriorityView(String currencyIso, PaymentType type){
		String[] types_string = getContext().getResources().getStringArray(R.array.pmt_priority_payment_types);
		String[] excluded = getContext().getResources().getStringArray(R.array.pmt_priority_excluded_currencies);
		String[] included = getContext().getResources().getStringArray(R.array.pmt_priority_included_currencies);
		
		List<PaymentType> types = new ArrayList<PaymentType>();
		for (String str: types_string){
			if (str.equals("ORANGE"))
				types.add(PaymentType.SWISS_ORANGE_PAYMENT_SLIP);
			else if (str.equals("RED"))
				types.add(PaymentType.SWISS_RED_PAYMENT_SLIP);
			else if (str.equals("ACCOUNT_TRANSFER"))
				types.add(PaymentType.INTERNAL_PAYMENT);
			else if (str.equals("INTERNATIONAL"))
				types.add(PaymentType.INTERNATIONAL_PAYMENT);
			else if (str.equals("DOMESTIC"))
				types.add(PaymentType.DOMESTIC_PAYMENT);
		}
		
		if (excluded.length > 0 && included.length > 0){
			return false;
		}
		if (excluded.length == 0 && included.length == 0){
			return types.contains(type);
		}
		if (excluded.length > 0){
			for (String cur: excluded){
				if (cur.equals(currencyIso))
					return false;
			}
			
			return types.contains(type);
		}
		else {
			for (String cur: included){
				if (cur.equals(currencyIso))
					return types.contains(type);
			}
			
			return false;
		}
	}
	
	public String getVersion(){	
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			return pInfo.versionName+"."+pInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		
	}

}
