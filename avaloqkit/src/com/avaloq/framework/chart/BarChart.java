package com.avaloq.framework.chart;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;

import com.androidplot.Plot.BorderStyle;
import com.androidplot.ui.AnchorPosition;
import com.androidplot.ui.SizeLayoutType;
import com.androidplot.ui.SizeMetrics;
import com.androidplot.ui.TextOrientationType;
import com.androidplot.ui.widget.UserTextLabelWidget;
import com.androidplot.ui.widget.Widget;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XLayoutStyle;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.YLayoutStyle;
import com.avaloq.framework.R;

public class BarChart extends Chart{
	
	public static int BAR_INNER_PADDING = 60;
	public static int BAR_OUTER_PADDING = 30;
	public final static int LABEL_OUTER_PADDING = 10;
	public static double PADDING_TO_BAR_RATIO = 0.3;
	
	private double lowerBoundary = 0;
	
	public BarChart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors) {
		super(context, listValues, listLabels, listSubLabels, listColors);
	}
	
	public BarChart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors, int width, int height) {
		super(context, listValues, listLabels, listSubLabels, listColors, width, height);
	}
	
	@Override
	protected void instantiateChart() {
		mChart = new XYPlot(mContext, "");
	}
	
	@Override
	public XYPlot getView(){
    	return (XYPlot)mChart;
    }
	
	@Override
    protected void createChart() {
		int labelSizeWithPadding = (int)((double)mContext.getResources().getInteger(R.integer.chart_label_size_bar)*1.5);
		
		for (int i=0; i<mValues.size(); i++){
			List<Double> tmpList = new ArrayList<Double>();
			for (int j=0; j<mValues.size(); j++){
				if (i != j)
					tmpList.add(null);
				else{
					if (mValues.get(i) < 0.0){
						/*if (lowerBoundary > mValues.get(i))
							lowerBoundary = -530;
						tmpList.add(-530.0);*/
						if (lowerBoundary > mValues.get(i))
							lowerBoundary = mValues.get(i);
						tmpList.add(mValues.get(i));
					}
					else
						tmpList.add(mValues.get(i));
				}
			}
			
			SimpleXYSeries series = new SimpleXYSeries(tmpList, SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, mLabels.get(i));
			getView().addSeries(series, new BarFormatter(Color.parseColor(mColors.get(i)), Color.TRANSPARENT));
		}
		
		//getView().setRangeLowerBoundary(0, BoundaryMode.FIXED);
		
		
		
			// Determine the optimal bar width
		
		int graphWidth = (int)getView().getGraphWidget().getWidthPix(getWidth())-(int)getView().getGraphWidget().getMarginRight()-(int)getView().getGraphWidget().getMarginLeft() - (int)getView().getGraphWidget().getRangeLabelWidth();
		double barSpace = graphWidth / mValues.size();
		BAR_INNER_PADDING = (int)(barSpace * PADDING_TO_BAR_RATIO / 2);
		BAR_OUTER_PADDING = BAR_INNER_PADDING;
		int barWidth = (int)(barSpace - 2*BAR_INNER_PADDING);
		
			/*int graphWidth = (int)getView().getGraphWidget().getWidthPix(getWidth())-(int)getView().getGraphWidget().getMarginRight()-(int)getView().getGraphWidget().getMarginLeft();
			int barWidth = graphWidth - 2*BAR_OUTER_PADDING - (int)getView().getGraphWidget().getRangeLabelWidth(); // Padding on both sides of the bars
			barWidth-= 2 * (mValues.size()-1) * BAR_INNER_PADDING; // For each inner bar there are 2 paddings, for the outer 2 bars there is only one padding
			barWidth = barWidth / mValues.size();*/
			((BarRenderer<?>)getView().getRenderer(BarRenderer.class)).setBarWidth((int)barWidth);
			
			getView().setRangeLowerBoundary(lowerBoundary, BoundaryMode.FIXED);
			getView().setUserRangeOrigin(0);
			getView().getGraphWidget().setGridPadding(BAR_OUTER_PADDING + (int)(barWidth/2), getView().getGraphWidget().getWidthPix((int)(labelSizeWithPadding*1.3)), BAR_OUTER_PADDING + (int)(barWidth/2), getView().getGraphWidget().getWidthPix(labelSizeWithPadding));

			// Set the background to transparent
			Paint bgPaint = new Paint();
	        bgPaint.setColor(Color.TRANSPARENT);
	        bgPaint.setStyle(Paint.Style.FILL);
	        getView().setBackgroundPaint(bgPaint);
	        
	        // Remove grids, backgrounds, domains, origin-lines and so on
	        getView().getGraphWidget().getDomainLabelPaint().setColor(Color.TRANSPARENT);
	        getView().getGraphWidget().getBackgroundPaint().setColor(Color.TRANSPARENT);
	        //getView().getGraphWidget().getGridLinePaint().setColor(Color.TRANSPARENT);
	        getView().getGraphWidget().getRangeLabelPaint().setColor(Color.TRANSPARENT);
	        //getView().getGraphWidget().getDomainOriginLabelPaint().setColor(Color.TRANSPARENT);
	        //getView().getGraphWidget().getDomainOriginLinePaint().setColor(Color.TRANSPARENT);
	        getView().getGraphWidget().getRangeOriginLinePaint().setColor(Color.BLACK);
	        Paint gridLinePaint = new Paint();
	        gridLinePaint.setColor(Color.parseColor("#CCCCCC"));
	        
	        getView().getGraphWidget().setGridLinePaint(gridLinePaint);
	        getView().getGraphWidget().getGridBackgroundPaint().setColor(Color.TRANSPARENT);
	        getView().getGraphWidget().getGridDomainLinePaint().setColor(Color.TRANSPARENT);
	        getView().getLegendWidget().setVisible(false);
	        getView().setBorderStyle(BorderStyle.NONE, (float)0, (float)0);
	        
	        // getView().setRangeStepMode(XYStepMode.INCREMENT_BY_VAL); // Steps depend on the value, not a fixed amount of lines
	        
	        //getView().getRangeLabelWidget().setVisible(true);
	        //getView().getRangeLabelWidget().setSize(new SizeMetrics(0,SizeLayoutType.ABSOLUTE, 0,SizeLayoutType.ABSOLUTE));
	        getView().getDomainLabelWidget().setVisible(false);
	        //getView().getGraphWidget().getRangeOriginLabelPaint().setColor(Color.TRANSPARENT);
	        
	        // Position the legend
	        int index = 0;
	        for (String label: mLabels){
		        UserTextLabelWidget selectionWidget = new UserTextLabelWidget(label, new SizeMetrics(labelSizeWithPadding,
		                SizeLayoutType.ABSOLUTE, barWidth + 2*BAR_INNER_PADDING, SizeLayoutType.ABSOLUTE), TextOrientationType.HORIZONTAL);
		        Paint legendBgPaint = new Paint();
		        legendBgPaint.setColor(Color.TRANSPARENT);
		        legendBgPaint.setStyle(Paint.Style.FILL);
		        selectionWidget.setBackgroundPaint(legendBgPaint);
		        selectionWidget.getLabelPaint().setColor(mContext.getResources().getColor(R.color.text_color_gray));
		        selectionWidget.getLabelPaint().setAntiAlias(true);
		        selectionWidget.getLabelPaint().setTextSize(mContext.getResources().getInteger(R.integer.chart_label_size_bar));
		        
		        // Determine if the label fits in its box and shorten it if necessary
		        Paint labelPaint = selectionWidget.getLabelPaint();
		        if (labelPaint.measureText(label) > (barWidth + 2*BAR_INNER_PADDING - 2*LABEL_OUTER_PADDING)){
			        while (labelPaint.measureText(label+"...") > (barWidth + 2*BAR_INNER_PADDING - 2*LABEL_OUTER_PADDING) ){
			        	if (label.length() > 0){
			        		label = label.substring(0, label.length()-1);
			        	}
			        	else
			        		break;
			        }
			        if (label.length() > 0)
			        	label+= "...";
			        selectionWidget.setText(label);
		        }
		        
		        int itemX = (int)getView().getGraphWidget().getRangeLabelWidth() + BAR_OUTER_PADDING + index * (2*BAR_INNER_PADDING + barWidth) - BAR_INNER_PADDING;
		        
		        getView().position(selectionWidget,
		        		itemX, XLayoutStyle.ABSOLUTE_FROM_LEFT,
		                0, YLayoutStyle.ABSOLUTE_FROM_TOP,
		                AnchorPosition.LEFT_TOP);
		        
		        
		        
		        
		        
		        
		        UserTextLabelWidget percentWidget = new UserTextLabelWidget(mSubLabels.get(index), new SizeMetrics(labelSizeWithPadding,
		                SizeLayoutType.ABSOLUTE, barWidth + 2*BAR_INNER_PADDING, SizeLayoutType.ABSOLUTE), TextOrientationType.HORIZONTAL);
		        percentWidget.setBackgroundPaint(legendBgPaint);
		        percentWidget.getLabelPaint().setColor(mContext.getResources().getColor(R.color.text_color_gray));
		        percentWidget.getLabelPaint().setAntiAlias(true);
		        percentWidget.getLabelPaint().setTextSize(mContext.getResources().getInteger(R.integer.chart_label_size_bar));
		        
		        getView().position(percentWidget,
		        		itemX, XLayoutStyle.ABSOLUTE_FROM_LEFT,
		                0, YLayoutStyle.ABSOLUTE_FROM_BOTTOM,
		                AnchorPosition.LEFT_BOTTOM);
		        
		        
		        
		        
		        index++;
	        }
	        
	        // Make the Grid Widget take up the whole available space
	        Widget gw = getView().getGraphWidget();
	        SizeMetrics sm = new SizeMetrics(0,SizeLayoutType.FILL, 0,SizeLayoutType.FILL);
	        gw.setSize(sm);
	        getView().position(gw, 0, XLayoutStyle.ABSOLUTE_FROM_LEFT,
	                 0, YLayoutStyle.ABSOLUTE_FROM_TOP);
    }
}
