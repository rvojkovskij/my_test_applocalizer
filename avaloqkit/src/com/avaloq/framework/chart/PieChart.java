package com.avaloq.framework.chart;

import java.util.Collections;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;

import com.androidplot.Plot.BorderStyle;
import com.androidplot.pie.Segment;
import com.avaloq.framework.R;

public class PieChart extends Chart{
	
	public PieChart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors) {
		super(context, listValues, listLabels, listSubLabels, listColors);
	}
	
	public PieChart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors, int width, int height) {
		super(context, listValues, listLabels, listSubLabels, listColors, width, height);
	}
	
	@Override
	protected void instantiateChart() {
		mChart = new com.androidplot.pie.PieChart(mContext, "");
	}

	public com.androidplot.pie.PieChart getView(){
    	return (com.androidplot.pie.PieChart)mChart;
    }

	@Override
    protected void createChart() {
		boolean containsLongLabels = false;
		for (String label: mLabels){
			if (label.length() > 4){
				containsLongLabels = true;
				break;
			}
		}
    	if (mValues != null){
    		Double maxValue = Collections.max(mValues);
	        for (int i=0; i<mValues.size(); i++){
	        	if ((int)(mValues.get(i)*100) > 0){
	        		Segment segment;
	        		int min = mContext.getResources().getInteger(R.integer.avq_min_chart_ration_to_show_label);
	        		if ((mValues.get(i) * 100 / maxValue) > min)
	        			segment = new Segment(mLabels.get(i)+"\n"+mSubLabels.get(i), mValues.get(i));
	        		else
	        			segment = new Segment("", mValues.get(i));
		        	//Segment segment = new Segment(mLabels.get(i), mValues.get(i));
		        	SegmentFormatter formatter = new SegmentFormatter(Color.parseColor(mColors.get(i)), Color.parseColor(mColors.get(i)));
		        	formatter.getOuterEdgePaint().setStrokeWidth(4);
		        	formatter.getRadialEdgePaint().setStrokeWidth(4);
		        	formatter.getInnerEdgePaint().setStrokeWidth(4);
		        	formatter.getInnerEdgePaint().setDither(false);
		        	formatter.getOuterEdgePaint().setDither(false);
		        	formatter.getRadialEdgePaint().setDither(false);
		        	formatter.getLabelPaint().setTextSize(mContext.getResources().getInteger(R.integer.chart_label_size_pie));
		        	
		        	/*if (containsLongLabels){
		        		formatter.getLabelPaint().setColor(Color.BLACK);
		        		formatter.getLabelPaint().setShadowLayer(2, 0, 1, Color.WHITE);
		        	}
		        	else{
		        		formatter.getLabelPaint().setShadowLayer(2, 0, -1, Color.BLACK);
		        	}*/
		        	formatter.getLabelPaint().setShadowLayer(2, 0, -1, Color.BLACK);
		        	
		        	getView().addSegment(segment, formatter);
	        	}
	        }
        }
    	
    	Paint bgPaint = new Paint();
        bgPaint.setColor(Color.TRANSPARENT);
        bgPaint.setStyle(Paint.Style.FILL);

        mChart.setBackgroundPaint(bgPaint);
        mChart.setBorderStyle(BorderStyle.NONE, (float)0, (float)0);
    }
}
