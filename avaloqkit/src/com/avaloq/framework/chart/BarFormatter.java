package com.avaloq.framework.chart;

import android.content.Context;

import com.androidplot.ui.SeriesRenderer;
import com.androidplot.xy.XYPlot;

public class BarFormatter extends com.androidplot.xy.BarFormatter{

	public BarFormatter(Context ctx, int xmlCfgId) {
		super(ctx, xmlCfgId);
	}
	
	public BarFormatter(int fillColor, int borderColor) {
		super(fillColor, borderColor);
	}
	
	@Override
    public Class<? extends SeriesRenderer> getRendererClass() {
        return BarRenderer.class;
    }

    @Override
    public SeriesRenderer getRendererInstance(XYPlot plot) {
        return new BarRenderer(plot);
    }

}
