package com.avaloq.framework.chart;

import android.graphics.PointF;

import com.androidplot.pie.PieChart;

public class PieRenderer extends com.androidplot.pie.PieRenderer{
	public PieRenderer(PieChart plot) {
        super(plot);
    }
	
	@Override
	public boolean isIntersection(PointF inner1, PointF inner2, PointF outer1, PointF outer2, PointF origin, float labelLength, String label){
    	if (
    		(origin.y > inner1.y && origin.y > outer1.y && origin.y > inner2.y && origin.y > outer2.y) ||
    		(origin.y < inner1.y && origin.y < outer1.y && origin.y < inner2.y && origin.y < outer2.y) ||
    		(origin.x > inner1.x && origin.x > outer1.x && origin.x > inner2.x && origin.x > outer2.x) ||
    		(origin.x < inner1.x && origin.x < outer1.x && origin.x < inner2.x && origin.x < outer2.x)
    	)
    		return false;
    	
    	Float coef1a = (inner1.y - outer1.y)/(inner1.x - outer1.x);
    	Float coef1b = inner1.y - coef1a*inner1.x;
    	
    	Float coef2a = (inner2.y - outer2.y)/(inner2.x - outer2.x);
    	Float coef2b = inner2.y - coef2a*inner2.x;
    	
    	Float coef0a = 0f;
    	Float coef0b = origin.y;
    	
    	if (coef1a == 0){
    		//TODO: ++++++++++++++++++++
    		return false;
    	}
    	if (coef2a == 0){
    		//TODO: ++++++++++++++++++++
    		return false;
    	}

    	PointF intersection1 = new PointF((coef0b - coef1b) / coef1a, origin.y);
    	PointF intersection2 = new PointF((coef0b - coef2b) / coef2a, origin.y);
    	
    	if (
    		(intersection1.x > origin.x && intersection2.x > origin.x ) ||
    		(intersection1.x < origin.x && intersection2.x < origin.x )
    	)
    		return false;
    	
    	/*double intersectionLength = Math.sqrt(Math.pow(intersection1.x - intersection2.x, 2) + Math.pow(intersection1.y - intersection2.y, 2));
    	
    	return (labelLength > intersectionLength);*/
    	
    	double intersectionLength1 = Math.sqrt(Math.pow(intersection1.x - origin.x, 2) + Math.pow(intersection1.y - origin.y, 2));
    	double intersectionLength2 = Math.sqrt(Math.pow(intersection2.x - origin.x, 2) + Math.pow(intersection2.y - origin.y, 2));
    	
    	return (intersectionLength1 < labelLength/2 || intersectionLength2 < labelLength / 2);
    	
    }
}
