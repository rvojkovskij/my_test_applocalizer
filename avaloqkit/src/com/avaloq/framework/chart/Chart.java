package com.avaloq.framework.chart;

import java.util.List;

import android.content.Context;
import android.view.ViewGroup.LayoutParams;

import com.androidplot.Plot;

public abstract class Chart {
	List<Double> mValues;
	List<String> mLabels;
	List<String> mSubLabels;
	List<String> mColors;
	Context mContext;
	int mWidth, mHeight;
	protected Plot<?, ?, ?> mChart;
	
    public Chart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors) {
    	mValues = listValues;
    	mLabels = listLabels;
    	mSubLabels = listSubLabels;
    	mColors = listColors;
    	mContext = context;
    	
    	instantiateChart();
    	createChart();
    }
    
    public Chart(Context context, List<Double> listValues, List<String> listLabels, List<String> listSubLabels, List<String> listColors, int width, int height) {
    	mValues = listValues;
    	mLabels = listLabels;
    	mSubLabels = listSubLabels;
    	mColors = listColors;
    	mContext = context;
    	instantiateChart();
    	
    	setWidth(width);
    	setHeight(height);
    	createChart();
    }
    
    public void setWidth(int width){
    	this.mWidth = width;
    	setChartDimensions();
    }
    
    public void setHeight(int height){
    	this.mHeight = height;
    	setChartDimensions();
    }
    
    public int getWidth(){
    	return mWidth;
    }
    
    public int getHeight(){
    	return mHeight;
    }
    
    private void setChartDimensions(){
    	if (mHeight > 0 && mWidth > 0 ){
    		LayoutParams params = new LayoutParams(mWidth, mHeight);
    		mChart.setLayoutParams(params);
    	}
    }
    
    public Plot<?, ?, ?> getView(){
    	return mChart;
    }
    
    protected abstract void createChart();
    protected abstract void instantiateChart();
}
