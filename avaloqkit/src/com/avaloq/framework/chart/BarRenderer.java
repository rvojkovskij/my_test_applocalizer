package com.avaloq.framework.chart;

import android.graphics.Canvas;
import android.graphics.RectF;

import com.androidplot.util.ValPixConverter;
import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

public class BarRenderer<T extends BarFormatter> extends com.androidplot.xy.BarRenderer<T> {
	public BarRenderer(XYPlot plot) {
        super(plot);
    }
	
	@Override
	protected void drawBar(Canvas canvas, RectF plotArea, int index, XYSeries series) {
		Number xVal = series.getX(index);
        Number yVal = series.getY(index);
        BarFormatter formatter = getFormatter(index, series);
        if (yVal != null && xVal != null) {
            switch (style) {
                case FIXED_WIDTH:
                    float halfWidth = barWidth / 2;
                    
                    float pixX = ValPixConverter.valToPix(xVal.doubleValue(), getPlot().getCalculatedMinX().doubleValue(), getPlot().getCalculatedMaxX().doubleValue(), plotArea.width(), false) + (plotArea.left);
                    float pixY = ValPixConverter.valToPix(yVal.doubleValue(), getPlot().getCalculatedMinY().doubleValue(), getPlot().getCalculatedMaxY().doubleValue(), plotArea.height(), true) + plotArea.top;
                    float zeroY = ValPixConverter.valToPix(0, getPlot().getCalculatedMinY().doubleValue(), getPlot().getCalculatedMaxY().doubleValue(), plotArea.height(), true) + plotArea.top;
                    
                    if (yVal.doubleValue() >= 0){
	                    canvas.drawRect(pixX - halfWidth, pixY, pixX + halfWidth, zeroY, formatter.getFillPaint());
	                    canvas.drawRect(pixX - halfWidth, pixY, pixX + halfWidth, zeroY, formatter.getBorderPaint());
                    }
                    else {
                    	canvas.drawRect(pixX - halfWidth, zeroY, pixX + halfWidth, pixY, formatter.getFillPaint());
                    	canvas.drawRect(pixX - halfWidth, zeroY, pixX + halfWidth, pixY, formatter.getBorderPaint());
                    }
                    break;
                default:
                    throw new UnsupportedOperationException("Not yet implemented.");
            }
        }
	}
}
