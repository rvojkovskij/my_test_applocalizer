package com.avaloq.framework.chart;

import com.androidplot.pie.PieChart;
import com.androidplot.ui.SeriesRenderer;

public class SegmentFormatter extends com.androidplot.pie.SegmentFormatter{
	public SegmentFormatter(Integer fillColor) {
		super(fillColor);
	}
	
	public SegmentFormatter(Integer fillColor, Integer borderColor) {
		super(fillColor, borderColor);
	}
	
	public SegmentFormatter(Integer fillColor, Integer outerEdgeColor,
            Integer innerEdgeColor, Integer radialEdgeColor) {
		super(fillColor, outerEdgeColor, innerEdgeColor, radialEdgeColor);
	}
	
	@Override
    public Class<? extends SeriesRenderer> getRendererClass() {
        return PieRenderer.class;
    }

    @Override
    public SeriesRenderer getRendererInstance(PieChart plot) {
        return new PieRenderer(plot);
    }
}
