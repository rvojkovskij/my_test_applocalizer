package com.avaloq.framework;

import java.util.Map;

import android.app.Activity;


/**
 * Defines the method the global app configuration class must provide to the framework.
 * @author bachi
 */
public interface AppConfigurationInterface {

	/**
	 * The banklet classes enabled in this project
	 */
	public Class<?>[] getBanklets();
	
	/**
	 * The available authentication handler classes
	 */
	public Class<?>[] getAuthenticationHandlers();

	/**
	 * The base-url for the web-services
	 */
	public String getBaseUrl();

	public String getWebserviceBaseUrl();
	
	/**
	 * The activity that is called when the application is started.
	 */
	public Class<? extends Activity> getStartupActivity();
	
	/**
	 * The default server session timeout in seconds (can be used by auth handlers)
	 */
	public int getServerSessionDefaultTimeout();
	
	/**
	 * Indicates whether to show and set the expiration date when selling a fund
	 */
	public boolean showExpDateOnFundSale();
	
	/**
	 * Indicates whether to show chars in the Wealth banklet
	 */
	public boolean showWealthCharts();
	
	/**
	 * Returns a map that contains all the endpoints for this application. 
	 * @return
	 */
	public Map<String,String> getApplicationEndpoints();
	
	/**
	 * Indicates whether to show or hide the market data in the Trading banklet 
	 * @return
	 */
	public boolean hideMarketData();
	
	/**
	 * Indicates wheter it is possible to sell funds by amount
	 */
	public boolean allowedSellFundsByAmount();
	
	/**
	 * Returns the hashes of the trusted CAs
	 * 
	 * @return
	 */
	public String[] getSSLTrustedPins();
}
