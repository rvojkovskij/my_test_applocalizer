package com.avaloq.framework;

import java.io.File;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.avaloq.framework.DownloadRequest.DocumentDownloadEvent;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.DialogFragment;

/**
 * Listener for clicks on the "Download Document" button.
 * Downloads documents using the DocumentDownloadRequest
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public abstract class DownloadClickListener implements View.OnClickListener {
	
	protected static final String TAG = DownloadClickListener.class.getSimpleName();
	public static final int RESULT_PDF = 1235;

	private ProgressDialog dialog = null;

	/**
	 * The Fragment that causes the click event
	 */
	private final Activity mActivity;

	/**
	 * The ID of the document (used in the download URL)
	 */
	private final long mDocumentId;
	
	protected File pdfFile = null;
	private Runnable pdfFileCallback;

	/**
	 * Creates a new listener
	 * @param aDocumentSafeFragment The fragment reference
	 * @param l The ID of the document
	 */
	public DownloadClickListener(Activity activity, long id) {
		mActivity = activity;
		mDocumentId = id;
		pdfFileCallback = getOnSetPdfCallback();
	}
	
	public File getPdfFile(){
		return pdfFile;
	}

	/**
	 * Handles the click events
	 */
	@Override
	public void onClick(final View v) {
		// show the Progress Dialog
		dialog = ProgressDialog.show(mActivity, mActivity.getResources().getString(R.string.documentsafe_downloading_title), mActivity.getResources().getString(R.string.documentsafe_downloading_text), false);
		
		DownloadRequest request = new DownloadRequest(mActivity, mDocumentId, new DocumentDownloadEvent() {
			@Override
			public void onRequestCompleted(DownloadRequest aRequest) {				
				final File result = aRequest.getResponse().getData();
				dialog.dismiss();
				Log.d(TAG, "result: " + result);
				if(result != null && result.exists()) {
					openPdfFile(result);
				} else {
					Log.e(TAG, "Error: result is null or does not exist");
				}
			}
			
			@Override
			public void onRequestFailed(DownloadRequest aRequest){
				dialog.dismiss();				
				DialogFragment.createAlert(mActivity.getString(R.string.documentsafe_download_error), mActivity.getString(R.string.documentsafe_failed_to_download_file)).show();
			}
		});
		
		request.initiateServerRequest();		
	}
	
	/**
	 * Opens the PDF file in a PDF viewer or alerts the user if that's not possible.
	 * @param file The PDF file
	 */
	void openPdfFile(File file) {
		if(file == null) {
			Log.d(TAG, "openPdfFile: file is null");
        	BankletActivity.toast(R.string.documentsafe_filedownload_error);
			return;
		}
		pdfFile = file;
		onPdfFileSet();
		Log.d(TAG, "openPdfFile: " + pdfFile);
		Uri uri = Uri.parse(FileProvider.CONTENT_URI + Uri.fromFile(pdfFile).getLastPathSegment());
		Log.d(TAG, "opening uri: "+uri.toString());
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        
        try {
        	displayDocument(intent, RESULT_PDF);
        	//startActivityForResult(intent, Fragment.RESULT_PDF);
        } catch (ActivityNotFoundException e) {
        	Log.e(TAG, "No application available to view PDF");
        	Toast.makeText(mActivity, mActivity.getText(R.string.documentsafe_no_pdf_viewer), Toast.LENGTH_LONG).show();
        	// BankletActivity.toast(R.string.documentsafe_no_pdf_viewer, Toast.LENGTH_LONG);
        	e.printStackTrace();
        }
	}
	
	private void onPdfFileSet() {
		if(pdfFileCallback != null){
			pdfFileCallback.run();
		}
		
	}

	protected abstract void displayDocument(Intent intent, int result_code);
	protected abstract Runnable getOnSetPdfCallback();

}