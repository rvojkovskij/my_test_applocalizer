package com.avaloq.framework.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;

/**
 * A convenience class adding some functionality passing Dialogs to DialogFragment,
 * it helps creating framework dialog services.
 * 
 * @author bachi
 * @deprecated use DialogActivity instead
 */
public class DialogFragment extends android.support.v4.app.DialogFragment {

	/**
	 * The Log-Tag for this class
	 */
	private final static String TAG = DialogFragment.class.getSimpleName();

	/**
	 * The Dialog
	 */
	protected Dialog mDialog;

	/**
	 * The Fragment's Activity
	 */
	protected FragmentActivity mActivity;

	/**
	 * The Tag for this DialogFragment
	 */
	protected String mTag;

	/**
	 * The List of Pending Dialogs
	 */
	protected static List<DialogFragment> mPendingDialogs = new ArrayList<DialogFragment>();

	/**
	 * Sets the Dialog
	 * @param dialog The Dialog
	 */
	public void setDialog(Dialog dialog) {
		mDialog = dialog;
	}

	/**
	 * Gets the Dialog
	 * @param dialog The Dialog
	 */
	public Dialog getDialog() {
		if (super.getDialog() != null) return super.getDialog();
		return mDialog;
	}
	
	/**
	 * If you want to use the simple show() method, you need to set the activity first.
	 * @param activity
	 */
	public void setActivity(FragmentActivity activity) {
		mActivity = activity;
	}

	/**
	 * If you want to use the simple show() method, you can set the fragment tag here first.
	 * @param tag
	 */
	public void setTag(String tag) {
		mTag = tag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		if (mDialog ==  null) {
			return super.onCreateDialog(savedInstanceState);
		}		

		return mDialog;
	}

	/**
	 * Create a DialogFragment wrapping a Dialog.
	 * Use '.show();' to show the managed dialog. 
	 * 
	 * @param dialog The dialog to show in the Fragment
	 * @return The created DialogFragment
	 */
	public static DialogFragment create(Dialog dialog) {
		DialogFragment frag = new DialogFragment();
		frag.setTag("Dialog");
		frag.setDialog(dialog);
		return frag;
	}

	/**
	 * Show the DialogFragment. It will be shown immediately if an active BankletActivity is available,
	 * or queued for later when one becomes available.
	 */
	public void show() {
		Activity act = BankletActivity.getActiveActivity();
		if (act != null && act instanceof FragmentActivity) {
			Log.v(TAG, "Show DialogFragment now:" + toString());
			show(((FragmentActivity) act).getSupportFragmentManager(), mTag);
		} else {
			Log.v(TAG, "Queue DialogFragment:" + toString());
			mPendingDialogs.add(this);
		}
	}
	
	/**
	 * Show the DialogFragment. It will be shown immediately if an active BankletActivity is available,
	 * or queued for later when one becomes available.
	 */
	public void show(FragmentActivity context) {
		FragmentActivity act = context;
		if (act != null) {
			Log.v(TAG, "Show DialogFragment now:" + toString());
			show(act.getSupportFragmentManager(), mTag);
		} else {
			Log.v(TAG, "Queue DialogFragment:" + toString());
			mPendingDialogs.add(this);
		}
	}

	/**
	 * Warning: Use the native show(FragmentManager manager, String tag) method only if you can guarantee 
	 * that the FragmentManager relates to an active activity.
	 * For any other case use the managed show() method. 
	 */
	@Override
	public void show(FragmentManager manager, String tag) {
		super.show(manager, tag);
	}

	/**
	 * Warning: Use the native show(FragmentTransaction transaction, String tag) method only if you can guarantee 
	 * that the FragmentTransaction relates to an active activity.
	 * For any other case use the managed show() method. 
	 */
	@Override
	public int show(FragmentTransaction transaction, String tag) {
		return super.show(transaction, tag);
	}

	/**
	 * Show all dialogs that were queued (because no active activity was available)
	 * @param activity
	 */
	public static void showPending(FragmentActivity activity) {
		for (DialogFragment frag : mPendingDialogs) {
			frag.show();
		}
		mPendingDialogs.clear();	
	}

	/**
	 * Create a simple alert box with title, text and "ok" button.
	 * @param title 
	 * @param text 
	 * @return the Dialog Fragment
	 */
	public static DialogFragment createAlert(String title, String text) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BankletActivity.getActiveActivity()); 
		alertDialogBuilder
			.setTitle(title)
			.setMessage(text)
			.setPositiveButton(R.string.avq_ok, null);

		return DialogFragment.create(alertDialogBuilder.create());
	}

	/**
	 * Create a simple alert box with title, text and "ok" button.
	 * @param title 
	 * @param text 
	 * @return the Dialog Fragment
	 */
	public static DialogFragment createAlert(String title, String text, Context activity) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity); 
		alertDialogBuilder
			.setTitle(title)
			.setMessage(text)
			.setPositiveButton(R.string.avq_ok, null);

		return DialogFragment.create(alertDialogBuilder.create());
	}

	/**
	 * Create a simple alert box with title, text and "ok" button.
	 * @param title 
	 * @param text 
	 * @param listener
	 * @return the Dialog Fragment
	 */
	public static DialogFragment createAlert(String title, String text, DialogInterface.OnClickListener listener) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BankletActivity.getActiveActivity()); 
		alertDialogBuilder
			.setTitle(title)
			.setMessage(text)
			.setPositiveButton(R.string.avq_ok, listener);

		return DialogFragment.create(alertDialogBuilder.create());
	}

	/**
	 * Show the default network connection error dialog.
	 * @deprecated use DialogActivity.showNetworkError() instead
	 */
	public static void showNetworkError() {				
		//showNetworkError(false);
		DialogActivity.showNetworkError(false);
	}

	/**
	 * Show the default network connection error dialog.
	 */
	public static void showNetworkError(final boolean finishOnDispose) {				
		createAlert(AvaloqApplication.getContext().getString(R.string.avq_error_no_connection_title), 
				AvaloqApplication.getContext().getString(R.string.avq_error_no_connection),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(finishOnDispose) {
							System.exit(0);
						}
					}
				})
				.show();
	}
}
