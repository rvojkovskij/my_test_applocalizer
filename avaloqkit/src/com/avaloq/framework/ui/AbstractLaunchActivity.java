package com.avaloq.framework.ui;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.framework.ClientAppInfoTO;
import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.comms.webservice.framework.ApplicationDefaultRequest;
import com.avaloq.framework.comms.webservice.framework.FrameworkService;

/**
 * This is the main app launcher activity.
 * It will load the banklet configuration and then open the app's configured first activity.
 * 
 * There is no view for this activity.
 * 
 */
public abstract class AbstractLaunchActivity extends BankletActivity {

	private static final String TAG = AbstractLaunchActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		
		Log.d(TAG, "onCreate");
		
		setContentView(R.layout.avq_activity_empty);
		TextView tv = (TextView) findViewById(R.id.avq_tabbed_activity_loading);
		tv.setText(R.string.avq_logging_in);
		
		BankletActivity.setActiveActivity(this);
	}
	
	/**
	 * Provide the app configuration object of this app.
	 * @return
	 */
	protected abstract AppConfigurationInterface getAppConfiguration();
	
	@Override
	protected void onResume() {
		BankletActivity.setActiveActivity(this);
		
		setIsShuttingDown(false);
		
		super.onResume();
		
		AvaloqApplication.getInstance().initialize(this, getAppConfiguration());
		
		// FIXME: If we get back here (i.e. after switching tasks) while there
		// was an auth in progress previously, the queue is stopped.
		Log.d(TAG, "runWithInitializedApp");
		
		AvaloqApplication.getInstance().runWithInitializedApp(new Runnable() {

			@Override
			public void run() {
				
				Log.d(TAG, "doing framework request");
				
				try {
					// Do the initial framework request
					
					// prepare the infos to send
					ClientAppInfoTO clientAppInfo = new ClientAppInfoTO();
					clientAppInfo.setApplicationVersion(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
					clientAppInfo.setDevice(Build.DEVICE);
					clientAppInfo.setOsName("Android " + Build.VERSION.RELEASE);

					// debug output
					Log.d(TAG, "Application Version: " + clientAppInfo.getApplicationVersion());
					Log.d(TAG, "Device: " + clientAppInfo.getDevice());
					Log.d(TAG, "OS Name: " + clientAppInfo.getOsName());
					
					Log.d(TAG, "isAuthenticationInProgress: "+AuthenticationManager.getInstance().isAuthenticationInProgress());
					Log.d(TAG, "" + QueueManager.getInstance().getThreadState());
					// make the request
					ApplicationDefaultRequest appDefaultsRequest = FrameworkService.getApplicationDefaults(clientAppInfo, new RequestStateEvent<ApplicationDefaultRequest>() {
						
						@Override
						public void onRequestCompleted(final ApplicationDefaultRequest aRequest) {
							
							// in case the server returns wrong or no data
							if (aRequest.getResponse().getData() == null) {
								DialogActivity.showNetworkError(true); 
								return;
							}
							
							// set the currency list
							AvaloqApplication.getInstance().setCurrencyList(aRequest.getResponse().getData().getCurrencyList());
							
							Log.d(TAG, "startFirstActivity");
							
							// start the first banklet
							AvaloqApplication.getInstance().startFirstActivity(AbstractLaunchActivity.this);				
						}
						
						@Override
						public void onRequestFailed(ApplicationDefaultRequest aRequest) {
							DialogActivity.showNetworkError(true);				
						}
						
					});
					appDefaultsRequest.setCachePolicy(CachePolicy.NO_CACHE);
					appDefaultsRequest.initiateServerRequest();
				} catch (NameNotFoundException e) {
					// TODO this should actually never happen. if you never get an error here, everything is fine.
					DialogActivity.showNetworkError();
				}
			}
			
		});
	}

}
