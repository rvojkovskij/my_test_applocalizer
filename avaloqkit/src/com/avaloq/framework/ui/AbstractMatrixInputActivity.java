package com.avaloq.framework.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * Abstract activity for a Matrix-type login.
 *
 */
public abstract class AbstractMatrixInputActivity extends AbstractInputActivity {

	private static final String TAG = AbstractMatrixInputActivity.class.getSimpleName();

	protected EditText mMatrix;
	protected TextView mChallenge;
	
	private static final String EXTRA_CHALLENGE = "challenge";
	
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		mMatrix = (EditText)findViewById(R.id.avq_login_matrix);				
		mMatrix.setOnEditorActionListener(this);		
				
		mChallenge = (TextView)findViewById(R.id.avq_login_matrix_challenge);
		
		if (getIntent().hasExtra(EXTRA_CHALLENGE)) {
			mChallenge.setText((getIntent().getExtras().getString(EXTRA_CHALLENGE)));
			mChallenge.setVisibility(View.VISIBLE);
		} else {
			mChallenge.setVisibility(View.GONE);
		}
		
	}
			
	@Override
	protected int getLayout() {
		return R.layout.avq_login_dialog_matrix;
	}

	/**
	 * Launch the user input activity and attach the auth handler and a thread handler to post to
	 */
	protected static void requestInput(Class<? extends AbstractInputActivity> activity, String challenge, String text, String errorMsg) {
		Bundle intentExtras = new Bundle();
		intentExtras.putString(EXTRA_CHALLENGE, challenge);
		requestInput(activity, text, errorMsg, intentExtras);
	}
	
	/**
	 * Do not call this method for Matrix input.
	 * @deprecated
	 */
//	protected static void requestInput(Class<? extends AbstractInputActivity> activity, String text, String errorMsg) {
//		throw new IllegalAccessError("Please use the specific requestInput() for Matrix auth"); 
//	}
	

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {

			onCompleteInput(mMatrix.getText().toString());
		}

		return false;
	}
	

	/**
	 * Called when the user finishes the Matrix input.
	 * Validation to be done in the implementing class.
	 * 
	 * @param Matrix
	 */
	abstract protected void onCompleteInput(String Matrix);



}
