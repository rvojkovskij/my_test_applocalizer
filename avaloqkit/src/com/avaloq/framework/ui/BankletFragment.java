package com.avaloq.framework.ui;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragment;

public abstract class BankletFragment extends SherlockFragment {
	
//	private static String TAG = BankletFragment.class.getSimpleName();

	/**
	 * Refreshes the Fragment without a bundle.
	 * I believe this won't work anymore since we started using Fragments.
	 */
	@Deprecated
	public void refreshData() {
//		refresh(null);
	}

	/**
	 * Refreshes the Fragment.
	 * I believe this won't work anymore since we started using Fragments.
	 * @param bundle The bundle
	 */
	@Deprecated
	public void refresh(Bundle bundle) {
//		FragmentTransaction tx = getFragmentManager().beginTransaction();
//		Fragment fragment = Fragment.instantiate(getActivity(), getClass().getName());
//		Log.d(TAG, "===> "+getClass().getName());
//		if(bundle != null) {
//			fragment.setArguments(bundle);
//		}	
//		// tx.replace(getView().getParent(), fragment);
//		tx.replace(R.id.avq_tabbed_activity_pager, fragment);
//		tx.addToBackStack(null);
//		tx.commit();
	}

	/*
	public void refresh(LocalActivityManager mLam, String targetId, Intent intent) {
		LocalActivityManager mLam = (WealthWelcomeActivity)this.getParent()
		Activity test = mLam.getActivity(targetId);
		if (test != null){
			View view = mLam.startActivity(targetId, intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)).getDecorView();
			this.setContentView(view);
		}
		// TODO Refreshing will work differently
	}
	*/
	
}