package com.avaloq.framework.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

/**
 * A Navigation Item that will display an Activity on selection.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ActivityNavigationItem extends NavigationItem {

	/**
	 * The Target Activity
	 */
	private Class<? extends Activity> mTarget;

	/**
	 * Creates an Object using target class and the name's Resource ID.
	 * @param aTarget The Target Class
	 * @param aNameStringResId The name's Resource ID
	 */
	public ActivityNavigationItem(Class<? extends Activity> aTarget, int aNameStringResId) {
		this(aTarget, aNameStringResId, 0);
	}
	
	/**
	 * Creates an Object using target class, the name's Resource ID and the icon's Resource ID.
	 * @param aTarget The target class
	 * @param aNameStringResId The name's Resource ID
	 * @param iconResId The icon's Resource ID
	 */
	public ActivityNavigationItem(Class<? extends Activity> aTarget, int aNameStringResId, int iconResId) {
		super(aNameStringResId, iconResId);
		this.mTarget = aTarget;
	}
	
	/**
	 * Creates an Object using app context, target class, the name's Resource ID, a styleable attribute and a fallback
	 * theme, which is used if the current application theme doesn't have the specified attribute
	 * @param aContext
	 * @param aTarget
	 * @param aNameStringResId
	 * @param aStyleAttr
	 * @param fallBackTheme
	 */
	public ActivityNavigationItem(Context aContext, Class<? extends Activity> aTarget, int aNameStringResId, int aStyleAttr, int fallBackTheme){
		super(aContext, aNameStringResId, aStyleAttr, fallBackTheme);
		this.mTarget = aTarget;
	}
	
	/**
	 * Creates an Object using app context, target class, the name's Resource ID and a styleable attribute
	 * @param aContext
	 * @param aTarget
	 * @param aNameStringResId
	 * @param aStyleAttr
	 */
	public ActivityNavigationItem(Context aContext, Class<? extends Activity> aTarget, int aNameStringResId, int aStyleAttr){
		super(aContext, aNameStringResId, aStyleAttr);
		this.mTarget = aTarget;
	}
	
	/**
	 * Returns the target Class.
	 * @return The target Class
	 */
	public Class<? extends Activity> getTarget() {
		return mTarget;
	}

	/**
	 * Called when a navigation item is clicked. You can modify the created
	 * intent.
	 * 
	 * @param intent
	 */
	public void onClick(Intent intent) {}
	
}
