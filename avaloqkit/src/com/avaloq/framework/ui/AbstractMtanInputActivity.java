package com.avaloq.framework.ui;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * Abstract activity for a mTan-type login.
 *
 */
public abstract class AbstractMtanInputActivity extends AbstractInputActivity {

	private static final String TAG = AbstractMtanInputActivity.class.getSimpleName();

	protected EditText mMtan;	
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		
		mMtan = (EditText)findViewById(R.id.avq_login_mtan);				
		mMtan.setOnEditorActionListener(this);

	}
	
	@Override
	protected int getLayout() {
		return R.layout.avq_login_dialog_mtan;
	}
	

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {

			onCompleteInput(mMtan.getText().toString());
		}

		return false;
	}
	

	/**
	 * Called when the user finishes the mTAN input.
	 * Validation to be done in the implementing class.
	 * 
	 * @param mTan
	 */
	abstract protected void onCompleteInput(String mTan);

}
