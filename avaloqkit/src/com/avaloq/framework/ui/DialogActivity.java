package com.avaloq.framework.ui;

import java.lang.ref.WeakReference;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;

public class DialogActivity extends BankletActivity {

	protected TextView mTitle;
	protected TextView mMessage;
	protected Button mButton;

	private static final String EXTRA_TITLE = "title";
	private static final String EXTRA_MESSAGE = "message";
	private static final String EXTRA_BUTTONTXT = "buttontext";
	private static final String EXTRA_EXIT = "exitonclick";

	private static final String TAG = DialogActivity.class.getSimpleName();
	private static boolean mIsShowing = false;
	private static WeakReference<DialogActivity> instance;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		instance = new WeakReference<DialogActivity>(this);
		
		setContentView(R.layout.avq_dialog_activity);
		
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			this.setFinishOnTouchOutside(false);
		}
		

		mMessage = (TextView)findViewById(R.id.avq_dialog_message);
		mButton = (Button)findViewById(R.id.avq_dialog_button);

		if (getIntent().hasExtra(EXTRA_TITLE)) {
			setTitle(getIntent().getExtras().getString(EXTRA_TITLE));
		}

		if (getIntent().hasExtra(EXTRA_MESSAGE)) {
			mMessage.setText(getIntent().getExtras().getString(EXTRA_MESSAGE));
		}

		if (getIntent().hasExtra(EXTRA_BUTTONTXT)) {
			mButton.setText(getIntent().getExtras().getString(EXTRA_BUTTONTXT));
		}


		mButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				DialogActivity.this.onClick(DialogActivity.this);

				mIsShowing = false;
				finish();
				
				if (getIntent().hasExtra(EXTRA_EXIT)) {					
					BankletActivity.exitApplication();
					finish();
				}
			}

		});
	}
	

	/**
	 * Overwrite to receive the button onClick event.
	 */
	public void onClick(DialogActivity activity) {}


	/**
	 * Show a dialog-styled modal message with ok button.
	 *  
	 * @param title
	 * @param message
	 * @param buttonText
	 * @param exitOnButtonClick should the app finish after the user has clicked the button?
	 */
	public static void show(String title, String message, String buttonText, boolean exitOnButtonClick) {
		show(title, message, buttonText, exitOnButtonClick, false);
	}


	/**
	 * Show a dialog-styled modal message with ok button.
	 *  
	 * @param title
	 * @param message
	 * @param buttonText
	 * @param exitOnButtonClick should the app finish after the user has clicked the button?
	 * @param showMenu Should the menu be shown in the activity?
	 */
	public static void show(String title, String message, String buttonText, boolean exitOnButtonClick, boolean hideMenu) {

		Context ctx = null;
		boolean newTask = false;

		if (BankletActivity.getActiveActivity() != null) {
			ctx = BankletActivity.getActiveActivity();			
		} else {
			ctx = AvaloqApplication.getContext();
			newTask = true;
			Log.w(TAG, "No active activty found, starting DialogActivity in a new task.");
		}
		
		Intent intent = new Intent(ctx, DialogActivity.class);
		// if (newTask) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		if (title != null) intent.putExtra(EXTRA_TITLE, title);
		if (message != null) intent.putExtra(EXTRA_MESSAGE, message);		
		if (buttonText != null) intent.putExtra(EXTRA_BUTTONTXT, buttonText);
		if (exitOnButtonClick) intent.putExtra(EXTRA_EXIT, exitOnButtonClick);
		if (hideMenu) intent.putExtra(BankletActivity.EXTRA_HIDE_MENU, true);

		ctx.startActivity(intent);		
	}
	
	public static void showWithCallback(String title, String message, String buttonText, final Runnable callback){
		Context ctx = null;

		if (BankletActivity.getActiveActivity() != null) {
			ctx = BankletActivity.getActiveActivity();
		} else {
			ctx = AvaloqApplication.getContext();
			Log.w(TAG, "No active activty found, starting DialogActivity in a new task.");
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(ctx.getString(R.string.avq_banking_message_title))
			.setMessage(message)
			.setPositiveButton(ctx.getString(R.string.avq_version_check_dismiss), new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					callback.run();
				}
			}).create().show();
	}

	/**
	 * Show a dialog-styled modal default network error message
	 * 
	 * @param exitOnButtonClick whether the app should be closed when the user clicks the button
	 */
	public static void showNetworkError(boolean exitOnButtonClick) {
		show (AvaloqApplication.getContext().getString(R.string.avq_error_no_connection_title),
				AvaloqApplication.getContext().getString(R.string.avq_error_no_connection),
				null, 
				exitOnButtonClick);		 		
	}

	/**
	 * Show a dialog-styled modal default network error message
	 */
	public static void showNetworkError() {
		showNetworkError(false);
	}

	/**
	 * Is this modal dialog already showing?
	 * @return 
	 */
	public static boolean isShowing() {
		return mIsShowing;
	}

	/**
	 * Get the current dialog instance (not guaranteeing that it is still shown)
	 * @return
	 */
	public static DialogActivity getInstance() {
		return instance.get();
	}

	@Override
	protected void onResume() {
		mIsShowing = true;
		super.onResume();
	}
	
	@Override
	public void onBackPressed() {
		mIsShowing = false;
		finish();
		super.onBackPressed();
	}


	/**
	 * Finish() the dialog if still there. 
	 */
	public static void hide() {
		mIsShowing = false;
		if (getInstance()!=null) getInstance().finish();
	}

}
