package com.avaloq.framework.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * Abstract activity for a password change request.
 */
public abstract class AbstractPasswordChangeActivity extends AbstractInputActivity {

	private static final String TAG = AbstractPasswordChangeActivity.class.getSimpleName();
	
	protected EditText mOldPassword;
	protected EditText mNewPassword;
	protected EditText mNewPasswordConfirm;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		mOldPassword = (EditText)findViewById(R.id.avq_login_old_password);
		mNewPassword = (EditText)findViewById(R.id.avq_login_new_password);
		mNewPasswordConfirm = (EditText)findViewById(R.id.avq_login_new_password_confirm);

		mOldPassword.setOnEditorActionListener(this);
		mNewPassword.setOnEditorActionListener(this);
		mNewPasswordConfirm.setOnEditorActionListener(this);		

		// Android changes input typefaces for pw fields, override that
		
		mOldPassword.setTypeface(Typeface.DEFAULT);
		mOldPassword.setTransformationMethod(new PasswordTransformationMethod());
		
		mNewPassword.setTypeface(Typeface.DEFAULT);
		mNewPassword.setTransformationMethod(new PasswordTransformationMethod());
		
		mNewPasswordConfirm.setTypeface(Typeface.DEFAULT);
		mNewPasswordConfirm.setTransformationMethod(new PasswordTransformationMethod());

	}
	
	@Override
	protected int getLayout() {
		return R.layout.avq_login_dialog_change_password;
	}


	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {

			onCompleteInput(mOldPassword.getText().toString(), mNewPassword.getText().toString(), mNewPasswordConfirm.getText().toString());
		}

		return false;
	}
	

	/**
	 * Called when the user finishes the username/password input.
	 * Validation to be done in the implementing class.
	 * 
	 * @param username
	 * @param password
	 */
	abstract protected void onCompleteInput(String oldPassword, String newPassword, String confirmNewPassword);


}
