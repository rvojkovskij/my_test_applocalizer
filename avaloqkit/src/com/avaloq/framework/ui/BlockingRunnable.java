package com.avaloq.framework.ui;

import android.app.ProgressDialog;
import android.content.Context;

public class BlockingRunnable{
	
	ProgressDialog pd;
	Context mContext;
	String mTitle, mMessage;
	
	public BlockingRunnable(Context aContext, String aTitle, String aMessage){
		mContext = aContext;
		mTitle = aTitle;
		mMessage = aMessage;
	}

	/**
	 * Called when the execution of the Runnable is done.
	 * 
	 * This function should be called from within onRun(). It is possible that 
	 * an AsyncTask is used in onRun(), in which case it wouldn't be possible
	 * to call done() automatically.
	 */
	public void hide(){
		pd.dismiss();
	}
	
	public void show() {
		pd = ProgressDialog.show(mContext, mTitle, mMessage, true, false);
	}
	
	
	
}
