package com.avaloq.framework.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;
import com.viewpagerindicator.TabPageIndicator;

public abstract class TabbedBankletFragment extends BankletFragment{
	
	private static final String TAG = TabbedBankletActivity.class.getSimpleName();

	private ViewPager mViewPager;

	private TabPageIndicator mIndicator;

	private SectionsPagerAdapter mAdapter;
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.avq_tabbed_activity, null, false);
		
		mAdapter = new SectionsPagerAdapter(this, new FragmentNavigationItem[] {});
		mViewPager = (ViewPager) view.findViewById(R.id.avq_tabbed_activity_pager);
		mViewPager.setAdapter(mAdapter);

        mIndicator = (TabPageIndicator)view.findViewById(R.id.avq_tabbed_activity_indicator);
        mIndicator.setViewPager(mViewPager);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		showNavigationItems(getNavigationItems());
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
	}
	
	/**
	 * Displays the provided Navigation Items
	 * @param aNavigationItems The Navigation Items to display
	 */
	protected void showNavigationItems(final FragmentNavigationItem[] aNavigationItems) {
		Log.d(TAG, "showNavigationItems");
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// update the ui accordingly
				getActivity().findViewById(R.id.avq_tabbed_activity_error_no_connection).setVisibility(View.GONE);
				getActivity().findViewById(R.id.avq_tabbed_activity_loading).setVisibility(View.GONE);
				getActivity().findViewById(R.id.avq_tabbed_activity_progress_bar).setVisibility(View.GONE);
				mViewPager.setVisibility(View.VISIBLE);
				mViewPager.setOffscreenPageLimit(2);
				// set the items in the adapter
				mAdapter.setNavigationItems(aNavigationItems);
				mIndicator.setVisibility((aNavigationItems.length == 1) ? View.GONE : View.VISIBLE);
				// apparently has to be called manually when the data changes...
				mIndicator.notifyDataSetChanged();
			}
		});
	}
	
	public abstract FragmentNavigationItem[] getNavigationItems();
	
	private class SectionsPagerAdapter extends FragmentPagerAdapter {
		
		private FragmentNavigationItem[] mNavigationItems;

		public SectionsPagerAdapter(Fragment aFragment, FragmentNavigationItem[] aNavigationItems) {
			super(aFragment.getChildFragmentManager());
			this.mNavigationItems = aNavigationItems;
		}

		@Override
		public Fragment getItem(int position) {
			Log.d("Test", "dddddd "+position);
			return Fragment.instantiate(TabbedBankletFragment.this.getActivity(), this.mNavigationItems[position].getTarget().getName(), this.mNavigationItems[position].getBundle());
		}

		@Override
		public int getCount() {
			return mNavigationItems.length;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO This is a hack, maybe styles will suit this better, if possible.
			return "  " + this.mNavigationItems[position].getName() + "  ";
		}
		
		public void setNavigationItems(FragmentNavigationItem[] aNavigationItems) {
			this.mNavigationItems = aNavigationItems;
			notifyDataSetChanged();
		}
		
	}
}
