package com.avaloq.framework.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * A Navigation Item that will display a Fragment on selection.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class FragmentNavigationItem extends NavigationItem {
	
	/**
	 * The target class
	 */
	protected Class<? extends Fragment> mTarget;
	
	/**
	 * The bundle for the Fragment
	 */
	protected Bundle mBundle;
	
	/**
	 * Creates an Object using target class and the name's resource ID.
	 * @param aTarget The Target Class
	 * @param aName The name's Resource ID
	 */
	public FragmentNavigationItem(Class<? extends Fragment> aTarget, String aName) {
		this(aTarget, aName, 0, null);
	}

	/**
	 * Creates an Object using target class, the name's resource ID and the icon's resource ID.
	 * @param aTarget The target class
	 * @param aName The name's Resource ID
	 * @param aBundle The Bundle for the Fragment
	 */
	public FragmentNavigationItem(Class<? extends Fragment> aTarget, String aName, Bundle aBundle) {
		this(aTarget, aName, 0, aBundle);
	}
	
	/**
	 * Creates an Object using target class, the name's resource ID and the icon's resource ID.
	 * @param aTarget The target class
	 * @param aName The name's resource ID
	 * @param aIconResId The icon's resource ID
	 */
	public FragmentNavigationItem(Class<? extends Fragment> aTarget, String aName, int aIconResId) {
		this(aTarget, aName, aIconResId, null);
	}
	
	/**
	 * Creates an Object using target class, the name's resource ID and the icon's resource ID.
	 * @param aTarget The target class
	 * @param aName The name's resource ID
	 * @param aIconResId The icon's resource ID
	 * @param aBundle The bundle for the Fragment
	 */
	public FragmentNavigationItem(Class<? extends Fragment> aTarget, String aName, int aIconResId, Bundle aBundle) {
		super(aName, aIconResId);
		this.mTarget = aTarget;
		this.mBundle = aBundle;
	}
	
	/**
	 * Returns the target Class.
	 * @return The target Class
	 */
	public Class<? extends Fragment> getTarget() {
		return mTarget;
	}
	
	/**
	 * The Bundle for this navigation item
	 * @return The Bundle for this Navigation Item
	 */
	public Bundle getBundle() {
		return this.mBundle;
	}

}