package com.avaloq.framework.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.framework.FrameworkService;
import com.avaloq.framework.comms.webservice.framework.Request;

public class LogoutActivity extends BankletActivity {

	private static final String TAG = LogoutActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.avq_activity_empty);
		TextView tv = (TextView) findViewById(R.id.avq_tabbed_activity_loading);
		tv.setText(R.string.avq_logging_out);
		
		Log.d(TAG, "onCreate");
		
		// First fire the logout requst asynchronously
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				Request logoutRequest = FrameworkService.logoutClientSession(new RequestStateEvent<Request>() {
					public void onRequestCompleted(Request aRequest) {
					};
					public void onRequestFailed(Request aRequest) {
					};
				});
				logoutRequest.setCachePolicy(CachePolicy.NO_CACHE);
				logoutRequest.executeRequest();
				if(logoutRequest.getResponse() != null && logoutRequest.getResponse().wasRequestSuccessfullyExecuted()) {
					Log.d(TAG, "Server logout successful");
				} else {
					Log.e(TAG, "Server logout failed");
				}
				return null;
			}
			protected void onPostExecute(Void result) {
				
				// Continue with the logout procedure once the (synchronous) logout request is done.
				AvaloqApplication.getInstance().clearAllData();
				
				AppConfigurationInterface config = AvaloqApplication.getInstance().getConfiguration();
				Intent intent = new Intent(LogoutActivity.this, config.getStartupActivity());
				startActivity(intent);
			};
			
		}.execute();
		
	}

}
