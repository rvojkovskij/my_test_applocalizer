package com.avaloq.framework.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * Abstract activity for a username/password-type login.
 */
public abstract class AbstractUsernamePasswordInputActivity extends AbstractInputActivity {

	private static final String TAG = AbstractUsernamePasswordInputActivity.class.getSimpleName();

	protected EditText mUsername;
	protected EditText mPassword;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		mUsername = (EditText)findViewById(R.id.avq_login_username);
		mPassword = (EditText)findViewById(R.id.avq_login_password);		

		mUsername.setOnEditorActionListener(this);
		mPassword.setOnEditorActionListener(this);		

		mPassword.setTypeface(Typeface.DEFAULT);
		mPassword.setTransformationMethod(new PasswordTransformationMethod());		
		
	}
	
	protected int getLayout() {
		return R.layout.avq_login_dialog_basic;
	}
		

	@Override
	public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		if (EditorInfo.IME_ACTION_DONE == actionId) {

			onCompleteInput(mUsername.getText().toString(), mPassword.getText().toString());
		}

		return false;
	}
	

	/**
	 * Called when the user finishes the username/password input.
	 * Validation to be done in the implementing class.
	 * 
	 * @param username
	 * @param password
	 */
	abstract protected void onCompleteInput(String username, String password);
	

}
