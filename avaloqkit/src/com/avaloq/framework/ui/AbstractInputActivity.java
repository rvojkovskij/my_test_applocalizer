package com.avaloq.framework.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.R;

/**
 * Base activity for login-type user input dialog activities.
 * 
 * The layout needs to be provided through getLayout() by the implementing class.
 * 
 * Layouts need to provide the common R.id.avq_login_text 
 * and R.id.avq_login_errormsg textviews.
 * 
 * @author bachi
 *
 */
public abstract class AbstractInputActivity extends SherlockFragmentActivity implements OnEditorActionListener {

	protected TextView mText;
	protected TextView mErrorMsg;

	protected LinearLayout mLoginBox;
	protected ProgressBar mProgress;

	private static final String EXTRA_TEXT = "text";
	private static final String EXTRA_ERRORMSG = "errormsg";
	private static final String EXTRA_EXIT_ON_BACK = "exitonback";

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

		setContentView(getLayout());

		mProgress = (ProgressBar)findViewById(R.id.avq_login_progress);
		mText = (TextView)findViewById(R.id.avq_login_text);
		mErrorMsg = (TextView)findViewById(R.id.avq_login_errormsg);

		if (getIntent().hasExtra(EXTRA_TEXT)) {
			showText(getIntent().getExtras().getString(EXTRA_TEXT));
		}

		if (getIntent().hasExtra(EXTRA_ERRORMSG)) {
			showErrorMsg(getIntent().getExtras().getString(EXTRA_ERRORMSG));
		}

		//mBackToast = Toast.makeText(this, R.string.avq_logout_press_back_again, Toast.LENGTH_LONG);

		// Flag the window as secure so the taskmanager will not generate thumbnails
		// and no screenshots can be made. Works only as of Honeycomb
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) { 
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
		}

	}

	@Override
	protected void onResume() {
		BankletActivity.setActiveActivity(this);
		super.onResume();
	}

	/**
	 * Launch the user input activity and attach the auth handler and a thread handler to post to
	 * @param activity the activity class to be launched
	 * @param text the message to be shown to the user or null if none is to be shown
	 * @param errorMsg an error message or null if none if none is to be shown
	 * @param intentExtras additional extras to be passed to the activity
	 */
	protected static void requestInput(Class<? extends AbstractInputActivity> activity, String text, String errorMsg, Bundle intentExtras) {
		Context ctx = BankletActivity.getActiveActivity();

		Intent intent = new Intent(ctx, activity);
		if (text!=null) intent.putExtra(EXTRA_TEXT, text);
		if (errorMsg != null) intent.putExtra(EXTRA_ERRORMSG, errorMsg);
		if (intentExtras != null) intent.putExtras(intentExtras);
		//if (exitOnBack) intent.putExtra(EXTRA_EXIT_ON_BACK, true);


		//intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
		ctx.startActivity(intent);		
	}

	/**
	 * Launch the user input activity and attach the auth handler and a thread handler to post to
	 * @param activity the activity class to be launched
	 * @param text the message to be shown to the user or null if none is to be shown
	 * @param errorMsg an error message or null if none if none is to be shown
	 */
	protected static void requestInput(Class<? extends AbstractInputActivity> activity, String text, String errorMsg) {
		requestInput(activity, text, errorMsg, null); 		
	}

	/**
	 * Provide the layout to be shown.
	 * @return
	 */
	abstract protected int getLayout();

	/**
	 * Called when the user tries to navigate away using the back button.
	 */
	abstract protected void onAbortInput();

	/**
	 * Show the error message.
	 * @param text
	 */
	protected void showErrorMsg(String text) {
		mErrorMsg.setText(text);
		mErrorMsg.setVisibility(View.VISIBLE);
	}

	/**
	 * Hide the error message
	 * @param text
	 */
	protected void hideErrorMsg() {
		mErrorMsg.setVisibility(View.GONE);
	}

	/**
	 * Show the description text
	 * @param text
	 */
	protected void showText(String text) {
		mText.setText(text);
		mText.setVisibility(View.VISIBLE);
	}

	/**
	 * Hide the description text
	 * @param text
	 */
	protected void hideText() {
		mText.setVisibility(View.GONE);
	}

	/**
	 * Show an one-button non-cancellable alert dialog. 
	 * 
	 * @param title
	 * @param message
	 * @param buttonText
	 * @param clickListener
	 */
	protected void showDialog(String title, String message, String buttonText, OnClickListener clickListener) {

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(buttonText, clickListener);

		DialogFragment frag = DialogFragment.create(alertDialogBuilder.create());
		frag.setCancelable(false);
		frag.show();
	}

	@Override
	public void onBackPressed() {

		// Currently we do not allow back navigation on the login sequence.
		onAbortInput();
		return;

		// Exit on back-press (not supported/needed yet)
		//		if(getIntent() != null && getIntent().getBooleanExtra(EXTRA_EXIT_ON_BACK, false) == true) {
		//						 			
		//			if(mToastIsVisible) {
		//				mBackToast.cancel();
		//				startActivity(new Intent(this, LogoutActivity.class));
		//				//BankletActivity.exitApplication();
		//				finish();
		//			} else {
		//				mBackToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
		//				mBackToast.show();
		//				mToastIsVisible = true;
		//				int toastActualTime = (mBackToast.getDuration() == Toast.LENGTH_LONG) ? 3500 : 2000;
		//				final long mToastVisibleTime = System.currentTimeMillis() + toastActualTime;
		//				new Thread() {
		//					public void run() {
		//						while(System.currentTimeMillis() < mToastVisibleTime) {
		//							try {
		//								Thread.sleep(100);
		//							} catch (InterruptedException e) {
		//								e.printStackTrace();
		//							}
		//						}
		//						mToastIsVisible = false;
		//					};
		//				}.start();
		//			}
		//		} else {
		//			super.onBackPressed();
		//		}
	}
}

