package com.avaloq.framework.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.BankletActivityDelegate.DrawerSet;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.util.ActivityTracker;

/**
 * Abstract Activity for all Banklet activities.
 * The Banklet Activity does provides an API for Banklets to create their own activities that have either an ActionBar or a SlidingMenu using the normal Banklet Navigation.
 * Calls like {@link #setContentView(int)} and {@link #onMenuItemSelected(int, MenuItem)} are handled by {@link BankletActivityDelegate}.
 * It also provides utility methods like {@link BankletActivity#getActiveActivity()}, {@link #toast(String)}, {@link #toast(String, int)}.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public abstract class BankletActivity extends SherlockFragmentActivity {
	
	public static final String EXTRA_HIDE_MENU = "hide_menu";

	public static final String EXTRA_LAUNCHED_FROM_LOGIN = "extra_launched_from_login";
	private static final String TAG = BankletActivity.class.getSimpleName();

	private static boolean mIsShuttingDown = false;

	/**
	 * The static reference to the activity that is currently active
	 */
	private static Activity mActiveActivity;

	DrawerLayout mDrawer;

	ActionBarDrawerToggle mToggle;

	Toast mBackToast = null;

	private boolean mToastIsVisible = false;

	@Override
	@SuppressLint("ShowToast")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.w(getClass().getSimpleName(), "onCreate");
		mBackToast = Toast.makeText(this, R.string.avq_logout_press_back_again, Toast.LENGTH_LONG);
		
		// Flag the window as secure so the taskmanager will not generate thumbnails
		// and no screenshots can be made. Works only as of Honeycomb
		if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) { 
		    getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
		} 
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume: mIsShuttingDown: " + mIsShuttingDown + " (" + this + ")");
		
		BankletActivityDelegate.selectCurrentTabInActionbar(this);
		
		if (mIsShuttingDown) {
			// Check if a shutdown is in progress.			
			Log.v(TAG, "Shutdown. Finishing activity " + this + ". Remaining active activities on stack (including this one): " + ActivityTracker.getInstance().getStackSize());
			finish();
			return;
		}


		Log.d(getClass().getSimpleName(), "onResume");
		setActiveActivity(this);

		// Show any waiting dialogs now that we're back in the foreground.
		DialogFragment.showPending(this);
	}

	@Override
	protected void onPause() {
		// Unset the active activity since we're no longer in the foreground
		mActiveActivity = null;
		//Log.w(TAG, "onPause: " + this.toString());
		super.onPause();
	}

	@Override
	protected void onStop() {
		// Log.w(TAG, "onStop(): " + this.toString());
		super.onStop();
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (mToggle != null)
			mToggle.syncState();
	}

	@Override
	public void onBackPressed() {
		if(getIntent() != null && getIntent().getBooleanExtra(EXTRA_LAUNCHED_FROM_LOGIN, false) == true) {
			if(mToastIsVisible) {
				mBackToast.cancel();
				startActivity(new Intent(this, LogoutActivity.class));
				finish();
			} else {
				mBackToast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
				mBackToast.show();
				mToastIsVisible = true;
				int toastActualTime = (mBackToast.getDuration() == Toast.LENGTH_LONG) ? 3500 : 2000;
				final long mToastVisibleTime = System.currentTimeMillis() + toastActualTime;
				new Thread() {
					public void run() {
						while(System.currentTimeMillis() < mToastVisibleTime) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						mToastIsVisible = false;
					};
				}.start();
			}
		} else {
			super.onBackPressed();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (mToggle != null)
			mToggle.onConfigurationChanged(newConfig);
	}

	/**
	 * Banklets set their layout within the Banklet container. It will be loaded into the banklet template
	 */
	@Override
	public void setContentView(int layoutResID) {
		// Need to set template content view here, the delegate will inflate the banklet's content
		super.setContentView(BankletActivityDelegate.LAYOUT_TEMPLATE);
		DrawerSet set = BankletActivityDelegate.setContentView(this, layoutResID);
		if (set != null){
			mDrawer = set.layout;
			mToggle = set.toggle;
		}
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(BankletActivityDelegate.LAYOUT_TEMPLATE);
		DrawerSet set = BankletActivityDelegate.setContentView(this, view, false);
		if (set != null){
			mDrawer = set.layout;
			mToggle = set.toggle;
		}
	}

	public void setContentView(View view, boolean upEnabled) {
		super.setContentView(BankletActivityDelegate.LAYOUT_TEMPLATE);
		DrawerSet set = BankletActivityDelegate.setContentView(this, view, upEnabled);
		if (set != null){
			mDrawer = set.layout;
			mToggle = set.toggle;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// If it is the DrawerLayout, handle it and return
		if (BankletActivityDelegate.onOptionsItemSelected(item, mDrawer)) {
			return true;
		}

		// Otherwise, switch to the default action
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Get the currently active activity (can be a BankletActivity or other, 
	 * such as the LaunchActivity). The reference is set onResume().
	 * @return The currently active activity
	 */
	public static Activity getActiveActivity() {
		Activity act = ActivityTracker.getInstance().getLastActive();
		Log.v(TAG, "getActiveActivity: " + act);
		if (act == null)
			throw new IllegalStateException("getActiveActivity: active activity is null :(");
		return act;
	}

	/**
	 * Set the currently active activity. BanketActivities do this automatically,
	 * other activities should register themselves onResume() here.
	 * 
	 * @param activity
	 */
	public static void setActiveActivity(Activity activity) {
		ActivityTracker.getInstance().add(activity);
	}

	/**
	 * Shows a Toast (short length) on the currently active Banklet activity
	 * @param resId The string resource id
	 */
	public static void toast(int resId) {
		toast(resId, Toast.LENGTH_SHORT);
	}

	public static void toast(int resId, int length) {
		if(mActiveActivity != null) {
			toast(mActiveActivity.getString(resId));
		}
	}

	/**
	 * Show a toast msg (short length) on the currently active Banklet activity
	 * @param msg The Toast Message
	 */
	public static void toast(String msg) {
		toast(msg, Toast.LENGTH_SHORT);
	}

	/**
	 * Show a toast msg on the currently active Banklet activity
	 * @param msg The Toast Message
	 * @param length Toast.LENGTH_SHORT | Toast.LENGTH_LONG
	 */
	public static void toast(String msg, int length) {
		if (mActiveActivity!= null) {
			Toast.makeText(mActiveActivity, msg, length).show();			
		}
	}
	

	/**
	 * TODO Must be reworked.
	 * Exit the application: Logout and finish all activities on the stack.
	 * Hint: You need to finish() the calling activity manually.
	 */
	public static void exitApplication() {

		Log.i(TAG, "Exiting the app...");
		Log.d(TAG, "exitApplication: mIsShuttingDown: " + mIsShuttingDown);
		mIsShuttingDown = true;

		// Shutdown worker threads
		// TODO: Only do this if we can be sure the app really exits.
		QueueManager.getInstance().exit();

		// Logout
		// AvaloqApplication.getInstance().logout();

		
	}
	
	public static void setIsShuttingDown(boolean aIsShuttingDown) {
		Log.d(TAG, "setIsShuttingDown: mIsShuttingDown: " + mIsShuttingDown);
		mIsShuttingDown = aIsShuttingDown;
	}
	
	public static boolean isShuttingDown() {
		return mIsShuttingDown;
	}

}
