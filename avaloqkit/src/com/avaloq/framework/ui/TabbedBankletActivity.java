package com.avaloq.framework.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.avaloq.framework.R;
import com.viewpagerindicator.IconPagerAdapter;
import com.viewpagerindicator.TabPageIndicator;

/**
 * Base class for most of the Banklets.
 * It will render a loading screen for you and Tabs, as soon as you say they are ready!
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public abstract class TabbedBankletActivity extends BankletActivity {
	
	private static final String STATE_TAB = "state_tab";
	private int stateTab = 0;
	
	private static final String TAG = TabbedBankletActivity.class.getSimpleName();

	private ViewPager mViewPager;

	private TabPageIndicator mIndicator;

	private SectionsPagerAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avq_tabbed_activity);
		
		mAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), new FragmentNavigationItem[] {});
		mViewPager = (ViewPager) findViewById(R.id.avq_tabbed_activity_pager);
		mViewPager.setOffscreenPageLimit(10);
		mViewPager.setAdapter(mAdapter);

        mIndicator = (TabPageIndicator)findViewById(R.id.avq_tabbed_activity_indicator);
        mIndicator.setViewPager(mViewPager);
		
		prepareNavigationItems();
	}
	
	public ViewPager getViewPager() {
		return mViewPager;
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		savedInstanceState.putInt(STATE_TAB, mViewPager.getCurrentItem());
		super.onSaveInstanceState(savedInstanceState);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		stateTab = savedInstanceState.getInt(STATE_TAB);
	}
	
	/**
	 * Displays the provided Navigation Items
	 * @param aNavigationItems The Navigation Items to display
	 */
	protected void showNavigationItems(final FragmentNavigationItem[] aNavigationItems) {
		Log.d(TAG, "showNavigationItems");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// update the ui accordingly
				findViewById(R.id.avq_tabbed_activity_error_no_connection).setVisibility(View.GONE);
				findViewById(R.id.avq_tabbed_activity_loading).setVisibility(View.GONE);
				findViewById(R.id.avq_tabbed_activity_progress_bar).setVisibility(View.GONE);
				mViewPager.setVisibility(View.VISIBLE);
				mViewPager.setOffscreenPageLimit(2);
				// set the items in the adapter
				mAdapter.setNavigationItems(aNavigationItems);
				mIndicator.setVisibility((aNavigationItems.length == 1) ? View.GONE : View.VISIBLE);
				// apparently has to be called manually when the data changes...
				mIndicator.notifyDataSetChanged();
				if (stateTab != 0){
					mViewPager.setCurrentItem(stateTab);
				}
			}
		});
	}
	
	protected void showError() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				findViewById(R.id.avq_tabbed_activity_error_no_connection).setVisibility(View.VISIBLE);
				findViewById(R.id.avq_tabbed_activity_loading).setVisibility(View.GONE);
				findViewById(R.id.avq_tabbed_activity_progress_bar).setVisibility(View.GONE);
				mViewPager.setVisibility(View.GONE);
			}
		});
	}
	
	/**
	 * Use this method to fill your Tabs with the Navigation items
	 * 
	 * TODO: This may not be required, but in case of the Document Safe Banklet we had to load the Tabs, before they were displayed.
	 * So you can use this method to load the values for {@link #showNavigationItems(FragmentNavigationItem[])}, if you need to.
	 * After that, you have to call {@link #showNavigationItems(FragmentNavigationItem[])} to hide the loading screen and display the Tabs and their contents.
	 */
	public abstract void prepareNavigationItems();
	
	private class SectionsPagerAdapter extends FragmentPagerAdapter implements IconPagerAdapter{
		
		private FragmentNavigationItem[] mNavigationItems;

		public SectionsPagerAdapter(FragmentManager aFragmentManager, FragmentNavigationItem[] aNavigationItems) {
			super(aFragmentManager);
			this.mNavigationItems = aNavigationItems;
		}

		@Override
		public Fragment getItem(int position) {
			return Fragment.instantiate(TabbedBankletActivity.this, this.mNavigationItems[position].getTarget().getName(), this.mNavigationItems[position].getBundle());
		}

		@Override
		public int getCount() {
			return mNavigationItems.length;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			// TODO This is a hack, maybe styles will suit this better, if possible.
			return "  " + this.mNavigationItems[position].getName() + "  ";
		}
		
		public void setNavigationItems(FragmentNavigationItem[] aNavigationItems) {
			this.mNavigationItems = aNavigationItems;
			notifyDataSetChanged();
		}

		@Override
		public int getIconResId(int index) {
			return mNavigationItems[index].getIconResourceId();
		}
		
	}

}
