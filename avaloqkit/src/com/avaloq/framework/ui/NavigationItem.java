package com.avaloq.framework.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;

/**
 * Main navigation items that a Banklet can pass to the application framework for rendering.
 * @author bachi
 */
public abstract class NavigationItem {
	
	protected String mName;
	protected int mNameStringResId;
	protected int mIconResId;

	/**
	 * Creates a new object using a name resource id and an icon resource id.
	 * @param aNameStringResId The name string id of this navigation item
	 * @param aIconResId The id of the icon of this navigation item
	 */
	protected NavigationItem(int aNameStringResId, int aIconResId) {
		this.mNameStringResId = aNameStringResId;
		this.mIconResId = aIconResId;
		verify();
	}

	/**
	 * Creates a new object using a name and an icon resource id.
	 * @param aName The name of this navigation item
	 * @param aIconResId The id of the icon of this navigation item
	 */
	protected NavigationItem(String aName, int aIconResId) {
		this.mName = aName;
		this.mIconResId = aIconResId;
		verify();
	}
	
	/**
	 * Searches for the specified styleable attribute in the specified theme and returns the referenced resource id
	 * @param aContext
	 * @param aNameStringResId
	 * @param aStyleAttr - example: if there is a reference attribute <attr name="IconWealth" format="reference"/>
	 *                     you should pass R.attr.IconWealth as a parameter.
	 */
	private int initializeFromThemeAttr(Context aContext, int aStyleAttr, int theme){		
		TypedArray a = aContext.getTheme().obtainStyledAttributes(theme, new int[] {aStyleAttr});
		//TypedArray taa = aContext.getTheme().obtainStyledAttributes(a.getResourceId(0, 0), new int[]{android.R.attr.background});
		int resId = a.getResourceId(0, 0);
		a.recycle();		
		//taa.recycle();
		return resId;
	}
	
	/**
	 * Creates a new object using a name and a styleable reference to an icon. Uses a fallback theme
	 * if the attribute is not found in the application theme.
	 * @param aContext
	 * @param aNameStringResId
	 * @param aStyleAttr - example: if there is a reference attribute <attr name="IconWealth" format="reference"/>
	 *                     you should pass R.attr.IconWealth as a parameter.
	 */
	public NavigationItem(Context aContext, int aNameStringResId, int aStyleAttr, int fallBackTheme){
		this.mNameStringResId = aNameStringResId;
		
		// Get the ID of the current theme and search for the styleable reference in it
		int themeId = aContext.getApplicationInfo().theme;
		this.mIconResId = initializeFromThemeAttr(aContext, aStyleAttr, themeId);
		
		// If it doesn't find anything in the current theme, fallback to the default banklet theme implementation
		if (this.mIconResId == 0)
			this.mIconResId = initializeFromThemeAttr(aContext, aStyleAttr, fallBackTheme);
	}
	
	/**
	 * Creates a new object using a name and a styleable reference to an icon.
	 * @param aContext
	 * @param aNameStringResId
	 * @param aStyleAttr - example: if there is a reference attribute <attr name="IconWealth" format="reference"/>
	 *                     you should pass R.attr.IconWealth as a parameter.
	 */
	public NavigationItem(Context aContext, int aNameStringResId, int aStyleAttr){
		this.mNameStringResId = aNameStringResId;
		
		// Get the ID of the current theme and search for the styleable reference in it
		int themeId = aContext.getApplicationInfo().theme;
		this.mIconResId = initializeFromThemeAttr(aContext, aStyleAttr, themeId);
	}
	
	/**
	 * Internally used to verify that either the name or the resource id are set.
	 */
	private void verify() {
		if(TextUtils.isEmpty(this.mName) && this.mNameStringResId == 0) {
			//throw new IllegalArgumentException("One of name and name's resource id must be set.");
		}
	}

	/**
	 * Returns the name's resource id
	 * @return The name's resource id
	 */
	public int getNameResourceId() {
		if(mNameStringResId == 0) {
			throw new IllegalAccessError("The nameStringResId for this NavigationItem was not set. Try getting the name instead.");
		}
		return mNameStringResId;
	}
	
	/**
	 * Returns the name
	 * @return The name
	 */
	public String getName() {
		if(TextUtils.isEmpty(mName)) {
			//throw new IllegalAccessError("The name for this NavigationItem was not set. Try getting the nameStringResourceId instead.");
		}
		return mName;
	}

	/**
	 * Returns the icon's resource id
	 * @return The icon's resource id
	 */
	public int getIconResourceId() {
		return mIconResId;
	}

}