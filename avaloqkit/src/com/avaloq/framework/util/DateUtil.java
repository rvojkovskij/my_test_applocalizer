package com.avaloq.framework.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;

/**
 * Class to provide helper methods for dates.
 * <br />
 * Example:
 * <br />
 * <code>
 * Date date = new Date();
 * <br />
 * String dateFormatted = new DateUtil(activity).format(date);
 * </code>
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class DateUtil {
	
	/**
	 * The Application Context
	 */
	private final Context mContext;
	
	/**
	 * Creates the helper
	 * @param aContext The Context for the helper
	 */
	public DateUtil(Context aContext) {
		this.mContext = aContext;
	}
	
	/**
	 * Formats the Date according to the user's settings
	 * @param date The Java Date to be formatted
	 * @return A nicely formatted string
	 */
	public String format(java.util.Date date) {
		final String userDateFormat = Settings.System.getString(this.mContext.getContentResolver(), Settings.System.DATE_FORMAT);
		if(TextUtils.isEmpty(userDateFormat)) {
			return DateFormat.getMediumDateFormat(this.mContext).format(date);
		} else {
			return new SimpleDateFormat(userDateFormat, Locale.getDefault()).format(date);
		}
	}
	
	/**
	 * Parses a formatted string to a date according to the user's settings
	 * @param txt The formatted string
	 * @return A Date
	 * @throws ParseException
	 */
	public java.util.Date parse(String txt) throws ParseException{
		final String userDateFormat = Settings.System.getString(this.mContext.getContentResolver(), Settings.System.DATE_FORMAT);
		if(TextUtils.isEmpty(userDateFormat)) {
			return DateFormat.getMediumDateFormat(this.mContext).parse(txt);
		} else {
			return new SimpleDateFormat(userDateFormat, Locale.getDefault()).parse(txt);
		}
	}
	
	/**
	 * Formats the time according to the user's settings
	 * @param date The Java Date to be formatted
	 * @return A nicely formatted string
	 */
	public String formatTime(java.util.Date date){
		return DateFormat.getTimeFormat(mContext).format(date);
	}
	
	/**
	 * Parses a formatted string with time information to a Date according to the user's settings
	 * @param txt The formatted string
	 * @return A Date
	 * @throws ParseException
	 */
	public java.util.Date parseTime(String txt) throws ParseException{
		return DateFormat.getTimeFormat(mContext).parse(txt);
	}
	
	/**
	 * This parsing method is usefull when the date and the time are stored in two different EditViews or Strings
	 * @param aDate The String containing the date
	 * @param aTime The String containing the time
	 * @return A date consisting of the date-part of the date-string and the time-part of the time-string
	 * @throws ParseException
	 */
	public java.util.Date parseDateTime(String aDate, String aTime) throws ParseException{
		Calendar calDate = getCalendar(parse(aDate));
		Calendar calTime = getCalendar(parseTime(aTime));
		
		calDate.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
		calDate.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
		
		return calDate.getTime();
	}
	
	/**
	 * Formats the date and time according to the user's settings
	 * @param date The Java Date to be formatted
	 * @return A nicely formatted string
	 */
	public String formatDateTime(java.util.Date date){
		return format(date) + " " + formatTime(date);
	}
	
	public static String formatLong(java.util.Date date){
		SimpleDateFormat formater = new SimpleDateFormat("EEEE, dd.MMMM yyyy HH:mm", Locale.getDefault());
		return formater.format(date);
	}
	
	/**
	 * Returns a calendar for the specified date.
	 * @param date The date.
	 * @return The Calendar object
	 */
	public static Calendar getCalendar(java.util.Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}
}
