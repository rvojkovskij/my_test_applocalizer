package com.avaloq.framework.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.InputFilter;
import android.text.Spanned;

public class CurrencyFormatInputFilter implements InputFilter {

	//Pattern mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\"+DecimalFormatSymbols.getInstance().getDecimalSeparator()+"[0-9]{0,2})?");
	Pattern mPattern;
	
	private BigDecimal minValue = null;
	private BigDecimal maxValue = null;
	private BigDecimal epsilon = new BigDecimal(0.000001);
	
	public CurrencyFormatInputFilter(){
		super();
		mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\"+DecimalFormatSymbols.getInstance().getDecimalSeparator()+"[0-9]{0,2})?");
	}
	
	public CurrencyFormatInputFilter(int decimals){
		super();
		mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\"+DecimalFormatSymbols.getInstance().getDecimalSeparator()+"[0-9]{0,"+Integer.toString(decimals)+"})?");
	}
	
	public CurrencyFormatInputFilter(BigDecimal aMinValue, BigDecimal aMaxValue){
		super();
		if (aMinValue != null){
			this.minValue = aMinValue.subtract(epsilon);
		}else{
			this.minValue = null;
		}
		if (aMaxValue != null){
			this.maxValue = aMaxValue.add(epsilon);
		}else{
			this.maxValue = null;
		}
		mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\"+DecimalFormatSymbols.getInstance().getDecimalSeparator()+"[0-9]{0,2})?");
	}
	
	public CurrencyFormatInputFilter(BigDecimal aMinValue, BigDecimal aMaxValue, int decimals){
		this(aMinValue, aMaxValue);
		if (decimals > 0){
			mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?(\\"+DecimalFormatSymbols.getInstance().getDecimalSeparator()+"[0-9]{0,"+Integer.toString(decimals)+"})?");
		}else{
			mPattern = Pattern.compile("(0|[1-9]+[0-9]*)?");
		}
	}
	
	/**
	 * Returns true if the range for the input is set
	 * @return
	 */
	private boolean isRangeSet(){
		return !(minValue == null || maxValue == null);
	}
	
	@Override
	public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
		
		// Get the whole string from the view to inspect it if it matches the regex
		String input = dest.subSequence(0, dstart) + source.toString() + dest.subSequence(dend, dest.length());

		// Because the software keyboard allows to input only point, we replace with the one
		// for the current locale
		
		// TODO the production version must be able to handle different locale
		input = input.replace('.', DecimalFormatSymbols.getInstance().getDecimalSeparator());

		// Check if it matches the regex
		Matcher matcher = mPattern.matcher(input);

		// If it does not match, then do not output the modified part
		if (!matcher.matches()){
			return dest.subSequence(dstart, dend);
		}
		
		// check the values for min and max if needed
		try {
			if (isRangeSet() && !isInRange(minValue, maxValue, parseNumber(input))) {
				return dest.subSequence(dstart, dend);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		// At this point of execution, the input (with the replaced separator) matches the regex, but
		// we also need to replace it in the view as well
		
		// Get the source sequence that will be replaced
		CharSequence result = source.subSequence(start, end);
		
		// Replace the decimal separator
		String strResString = result.toString();
		strResString = strResString.replace('.', DecimalFormatSymbols.getInstance().getDecimalSeparator());
		result = (CharSequence) strResString;
		
		// Return the filtered sequence
		return result;
	}
	
	private boolean isInRange(BigDecimal a, BigDecimal b, BigDecimal c) {
		// TODO check if this still works, was: return b > a ? c >= a && c <= b : c >= b && c <= a;
		return b.compareTo(a) > 0 ? c.compareTo(a) >= 0 && c.compareTo(b) <= 0 : c.compareTo(b) >= 0 && c.compareTo(a) <= 0;
	}
	
	public static BigDecimal parseNumber(String strNumber){
		BigDecimal ret = null;
		DecimalFormat df = new DecimalFormat();		
		df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance());
		try {
			Number parsedNumber = df.parse(strNumber);
			ret =  new BigDecimal(parsedNumber.doubleValue());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
}