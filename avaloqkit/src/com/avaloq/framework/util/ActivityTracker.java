package com.avaloq.framework.util;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.util.Log;

/**
 * This class will track active activities using WeakReferences, and give you
 * access to them, avoiding already gc'ed activities.
 * 
 * @author bachi
 *
 */
public class ActivityTracker {

	private static final String TAG = ActivityTracker.class.getSimpleName();
	private static ActivityTracker mInstance;
	private List<WeakReference<Activity>> mActivityList = new ArrayList<WeakReference<Activity>>();
	
	private ActivityTracker() {}
	
	public static ActivityTracker getInstance() {
		if (mInstance == null) {
			mInstance = new ActivityTracker();
		}
		return mInstance;
	}
	
	public void add(Activity activity) {
		mActivityList.add(new WeakReference<Activity>(activity));
		Log.v(TAG, "Added activity to stack: " + activity);
		//Log.v(TAG, "Current activity stack: " + this.toString());
	}
	
	public Activity getLastActive() {
		
		for (int i=mActivityList.size()-1; i>=0; i--) {
			Activity act = mActivityList.get(i).get();
			if (act != null) {
				Log.v(TAG, "Returning active activity: " + act);
				return act;
			
			} else {
				Log.w(TAG, "Got an already null'ed activity on the stack, trying previous one.");	
			}
		}
		
		Log.e(TAG, "Have no active activity on the stack to return!");
		return null;
	}
	
	/**
	 * Return the stack content
	 */
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int i=mActivityList.size()-1; i>=0; i--) {
			Activity act = mActivityList.get(i).get();
			str.append((act != null) ? " | " + act : " | <NULL>");
		}
		str.append(" | ");
		return str.toString();
	}
	
	/**
	 * Get the number of alive (not nulled) activities on the stack
	 */
	public int getStackSize() {
		int count = 0;
		for (WeakReference<Activity> ref : mActivityList) {
			if (ref.get() != null) count++;
		}
		
		return count;
	}
	
}
