package com.avaloq.framework.util;import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.Currency;

import com.avaloq.framework.AvaloqApplication;

/**
 * @author zahariev
 *  Base class for Currency formatters, Banklets can extend this to improve on functionality (like the Wealth Banklet, there a currency id can be passed).   
 */
public class CurrencyUtil {
	
	private static final DecimalFormat NUMBER_FORMAT = new DecimalFormat();
	
	static {
		NUMBER_FORMAT.setGroupingUsed(true);
		NUMBER_FORMAT.setGroupingSize(3);
	}
	
	private static final DecimalFormat MONEY_INPUT_FORMAT = new DecimalFormat();
	
	static {
		MONEY_INPUT_FORMAT.setGroupingUsed(false);
		MONEY_INPUT_FORMAT.setMinimumFractionDigits(2);
		MONEY_INPUT_FORMAT.setMaximumFractionDigits(2);
	}
	
	private static final DecimalFormat MONEY_FORMAT = new DecimalFormat();
	
	static {
		MONEY_FORMAT.setGroupingUsed(true);
		MONEY_FORMAT.setGroupingSize(3);		
		MONEY_FORMAT.setMinimumFractionDigits(2);
		MONEY_FORMAT.setMaximumFractionDigits(2);
	}

	/**
	 * Formats an input number
	 * @param amount The amount to format
	 */
	public static String formatInputNumber(BigDecimal amount) {
		return MONEY_INPUT_FORMAT.format(amount);
	}
	
	/**
	 * Formats a number, that is not money
	 * @param amount The amount to format
	 */
	public static String formatNumber(Double amount) {
		return NUMBER_FORMAT.format(amount);
	}
	
	/**
	 * Formats a number, that is not money
	 * @param amount The amount to format
	 */
	public static String formatNumber(BigDecimal amount) {
		return NUMBER_FORMAT.format(amount);
	}
	
	/**
	 * Formats a number, that is not money, with the number of decimal places.
	 * @param amount The amount to format
	 */
	public static String formatNumber(BigDecimal amount, int decimalPlaces) {
		if (amount == null) return "";		
		NUMBER_FORMAT.setMaximumFractionDigits(decimalPlaces);
		return NUMBER_FORMAT.format(amount);
	}
	
	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @return The formatted amount
	 */
	public static String formatMoney(Double amount) {
		return formatMoney(new BigDecimal(amount), null);
	}
	
	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @return The formatted amount
	 */
	public static String formatMoney(Integer amount) {
		return formatMoney(new BigDecimal(amount), null);
	}
	
	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @return The formatted amount
	 */
	public static String formatMoney(Long amount) {
		return formatMoney(new BigDecimal(amount), null);
	}
	
	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @return The formatted amount
	 */
	public static String formatMoney(BigDecimal amount) {
		return formatMoney(amount, null);
	}

	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @param currencyId The currency id
	 * @return The formatted amount
	 */
	public static String formatMoney(BigDecimal amount, long currencyId) {
		if (currencyId == 0)
			return formatMoney(amount, null);
		else
			try {
				return formatMoney(amount, AvaloqApplication.getInstance().findCurrencyById(currencyId).getIsoCode());
			}
			catch (Exception e){
				return formatMoney(amount, null);
			}
	}

	/**
	 * Formats money without a currency.
	 * @param amount The amount of money
	 * @param currencyId The currency id
	 * @return The formatted amount
	 */
	public static String formatMoney(BigDecimal amount, long currencyId, int decimals) {
		if (currencyId == 0)
			return formatNumber(amount, decimals);
		else
			try {
				return formatMoney(amount, AvaloqApplication.getInstance().findCurrencyById(currencyId).getIsoCode(), decimals);
			}
			catch (Exception e){
				return formatNumber(amount, decimals);
			}
	}
	
	/**
	 * Formats money with a currency.
	 * @param amount The amount of money
	 * @param isoCode The ISO-code for this currency
	 * @return The formatted amount
	 * 
	 */
	public static String formatMoney(BigDecimal amount, String isoCode) {
		if(isoCode != null) {
			return MessageFormat.format("{0} {1}", isoCode, MONEY_FORMAT.format(amount));
		} else {
			return MONEY_FORMAT.format(amount);
		}
	}
	
	/**
	 * Formats money with a currency.
	 * @param amount The amount of money
	 * @param isoCode The ISO-code for this currency
	 * @return The formatted amount
	 * 
	 */
	public static String formatMoney(BigDecimal amount, String isoCode, int decimals) {
		if (amount == null) return "";
		if(isoCode != null) {
			return MessageFormat.format("{0} {1}", isoCode, formatNumber(amount, decimals));
		} else {
			return MONEY_FORMAT.format(amount);
		}
	}

}