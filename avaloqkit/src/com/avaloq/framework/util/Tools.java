package com.avaloq.framework.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

/**
 * General Tools class
 * @author bachi
 */
public class Tools {

	/**
	 * MD5 generation ought to be 1 line of code.
	 * @param input The Input String
	 * @return The String's md5 digest
	 */
	public static String md5(String input) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] messageDigest = md.digest(input.getBytes());
			BigInteger number = new BigInteger(1,messageDigest);
			String md5 = number.toString(16);
			while (md5.length() < 32)
				md5 = "0" + md5;
			return md5;
		} catch(NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	/**
	 * Returns the screen size
	 * 
	 * @return Point - containing the X and Y size of the screen in pixels
	 */
	public static Point getDisplaySize(Context context){
		Point size = new Point();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		ApiSpecificCalls.getDisplaySize(display, size);
		return size;
	}

}