package com.avaloq.framework.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * Util to show "inline" messages. This means that a TextView is added to the passed ViewGroup 
 */
public class InlineMessageUtil {

	/**
	 * Add and show a TextView to the passed parent ViewGroup
	 * @param parent The parent ViewGroup the view is added to
	 * @param resId The text string's ressource id
	 * @return the message view
	 */
	public static View display(ViewGroup parent, int resId) {
		return show(parent, resId, null);
	}

	/**
	 * Add and show a TextView to the passed parent ViewGroup
	 * @param parent The parent ViewGroup the view is added to
	 * @param resId The text string's ressource id
	 * @param formatArgs The format arguments that will be used for substitution.
	 * @return the message view 
	 */
	public static View show(ViewGroup parent, int resId, Object [] formatArgs) {
		Context ctx = parent.getContext();
		View view = LayoutInflater.from(ctx).inflate(R.layout.avq_inline_message, parent, false);
		if(formatArgs == null || formatArgs.length == 0) {
			((TextView)view.findViewById(R.id.avq_inline_message_text)).setText(ctx.getString(resId));
		} else {
			((TextView)view.findViewById(R.id.avq_inline_message_text)).setText(ctx.getString(resId, formatArgs));
		}
		parent.addView(view);
		return view;
	}

	/**
	 * Add the default 'no connection' msg as TextView to the passed parent ViewGroup
	 * @param parent The parent ViewGroup the view is added to 
	 * @return the message view
	 */
	public static View showNetworkError(ViewGroup parent) {
		return display(parent, R.string.avq_error_no_connection);
	}



}
