package com.avaloq.framework.util;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;

/**
 * Central class to store all the API-specific calls for certain older build versions
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ApiSpecificCalls {

	private final static String TAG = ApiSpecificCalls.class.getSimpleName();

	/**
	 * Removes a {@link OnGlobalLayoutListener} from a {@link ViewTreeObserver}
	 * @param viewTreeObserver The ViewTreeObserver
	 * @param listener The OnGlobalLayoutListener
	 */
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void removeOnGlobalLayoutListener(ViewTreeObserver viewTreeObserver, ViewTreeObserver.OnGlobalLayoutListener listener) {
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
			viewTreeObserver.removeGlobalOnLayoutListener(listener);			
		} else {
			viewTreeObserver.removeOnGlobalLayoutListener(listener);
		}
	}

	/**
	 * Retuns the Display size.
	 * Before Honeycomb MR2, the methods {@link Display#getWidth()} and {@link Display#getHeight()} have to be used.
	 * @param display The display
	 * @param size The size will be written into this variable
	 */
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public static void getDisplaySize(Display display, Point size) {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
			size.y = display.getWidth();  // deprecated
			size.x = display.getHeight();  // deprecated
		} else {
			display.getSize(size);
		}
	}

	/**
	 * Sets the view's "activated" state.
	 * @param view The view
	 * @param activated Whether the view is activated or not
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void setViewActivated(View view, boolean activated) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			view.setActivated(activated);
		}
	}

	/**
	 * Set the Intent.FLAG_ACTIVITY_CLEAR_TASK flag to an intent.
	 * @param intent
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void setIntentActivityClearTaskFlag(Intent intent) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			Log.i(TAG, "Clear task (activity instances).");
		}				
	}
	
	@SuppressWarnings("deprecation")
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public static void setViewBackground(View view, Drawable background) {
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
		    view.setBackgroundDrawable(background);
		} else {
		    view.setBackground(background);
		}
	}

}
