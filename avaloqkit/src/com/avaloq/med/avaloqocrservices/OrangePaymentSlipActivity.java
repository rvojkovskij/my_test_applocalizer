package com.avaloq.med.avaloqocrservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.avaloq.framework.R;
import com.avaloq.med.avaloqocrservices.OrangePaymentSlipFragment.OrangePaymentSlipDelegate;

public class OrangePaymentSlipActivity extends FragmentActivity implements OrangePaymentSlipDelegate {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.avq_activity_orange_payment_slip);
	} 

	@Override
	public void scanningCanceledByUser() {	
		finish();
	}

	/**
	 * 
	 * This is a simple activity that uses OrangePaymentSlipFragment in a full screen manner.
	 * The result (RESULT_OK intent) of the activity contains the recognized data if the scanning was successful.
	 * Extra parameter keys/values on the result intent are:
	 * "codeLine"	- Value contains the full code line from the payment slip.
	 * i.e. "0100003949753>210000000003139471430009017+010001628>"
	 * "accountNumber"	- Value contains the account number from the payment slip.
	 * i.e. "010001628"
	 * "referenceNumber"	- Value contains the reference number from the payment slip.
	 * i.e. "210000000003139471430009017"
	 * "amount"	- Value contains the amount from the payment slip. Last two digits are always decimal values.
	 * i.e. "00003949753"
	 * "slipType" - Value contains the type of the payment slip:
	 * i.e. "01"
	 *	
	 * Possible values:
	 * "01" = ISR in CHF
	 * "03" = COD-ISR in CHF (cash-on-delivery) 
	 * "04" = ISR+ in CHF
	 * "11" = ISR in CHF for credit to own account 
	 * "14" = ISR+ in CHF for credit to own account
	 * "21" = ISR in EUR
	 * "23" = ISR in EUR for credit to own account 
	 * "31" = ISR+ in EUR
	 * "33" = ISR+ in EUR for credit to own account
	 * 
	 * Reference: https://www.postfinance.ch/content/dam/pf/de/doc/consult/manual/dlserv/inpayslip_isr_man_en.pdf
	 * 
	 * 
	 * @author danilo.medic
	 * 
	 */
	@Override
	public void scanningFinnished(OrangePaymentSlipData resultData) {
		if(resultData != null) {
			
			Intent intent = new Intent();
			
			intent.putExtra("accountNumber", resultData.accountNumber);
			intent.putExtra("referenceNumber", resultData.referenceNumber);
			intent.putExtra("amount", resultData.amount);
			intent.putExtra("slipType", resultData.slipType);
			
			intent.putExtra("data", resultData.codeLine);
			setResult(RESULT_OK, intent);
			
		}
		finish();
	}
}
