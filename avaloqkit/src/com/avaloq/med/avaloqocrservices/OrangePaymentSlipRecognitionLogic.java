package com.avaloq.med.avaloqocrservices;

public class OrangePaymentSlipRecognitionLogic {

	public native static boolean isAlgorithmTrained ();

	public native static void trainAlgorithm (float[] trainInputData, float[] trainInputValues, int numOfTrainImages, int imageSize);

	public native static boolean applyRecognitionLogic (
			/* Input Values */
			int sourceImageWidth, 
			int sourceImageHeight, 
			byte[] sourceImageData, 
			int previewWidth, 
			int previewHeight, 
			int deviceOrientation, 
			float deviceAccelerometerCoordinateZ,

			/* Output Values */
			int[] markerCoordinates,
			char[] accountNumber,
			char[] referenceNumber,
			char[] amount,
			char[] slipType,
			char[] codeLine
			);

	public static boolean applyRecognitionLogic (
				/* Input Values */
				int sourceImageWidth, 
				int sourceImageHeight, 
				byte[] sourceImageData, 
				int previewWidth, 
				int previewHeight, 
				int deviceOrientation, 
				float deviceAccelerometerCoordinateZ,

				/* Output Values */
				int[] markerCoordinates,
				OrangePaymentSlipData result
				) {
		
		char[] accountNumber = new char[20];
		char[] referenceNumber = new char[40];
		char[] amount = new char[20];
		char[] slipType = new char[5];
		char[] codeLine = new char[60];
		
		boolean recognized = applyRecognitionLogic	(
														sourceImageWidth, 
														sourceImageHeight, 
														sourceImageData, 
														previewWidth, 
														previewHeight, 
														deviceOrientation, 
														deviceAccelerometerCoordinateZ, 
														markerCoordinates, 
														accountNumber, 
														referenceNumber, 
														amount, 
														slipType, 
														codeLine
													);
		if(result != null) {
			result.accountNumber = makeStringFromCharArray(accountNumber);
			result.referenceNumber = makeStringFromCharArray(referenceNumber);
			result.amount = makeStringFromCharArray(amount);
			result.slipType = makeStringFromCharArray(slipType);
			result.codeLine = makeStringFromCharArray(codeLine);
		}
		
		return recognized;
	}
	
	private static String makeStringFromCharArray(char[] charArray) {
		int indexOfEndCharacter = 0;
		
		for (int i = 0; i<charArray.length; i++) {
			if(charArray[i] == '\0') {
				indexOfEndCharacter = i;
				break;
			}
		}
		
		return new String(charArray, 0, indexOfEndCharacter);
	}
	
	static{
		System.loadLibrary("opencv_java");
		System.loadLibrary("avaloq_ocr_services_orange_payment_slip");
	}

}
