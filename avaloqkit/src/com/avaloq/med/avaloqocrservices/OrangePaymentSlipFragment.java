package com.avaloq.med.avaloqocrservices;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.avaloq.framework.R;

public class OrangePaymentSlipFragment extends Fragment implements OnClickListener {
	private OrangePaymentSlipSurfaceView orangePaymentSlipSurfaceView;
	public FrameLayout parentLayout = null;
	public View rootView;
	public ImageView previewImageView;
	
	public interface OrangePaymentSlipDelegate {
		void scanningCanceledByUser();
		void scanningFinnished(OrangePaymentSlipData resultData);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.avq_fragment_orange_payment_slip, container, false);
		previewImageView = (ImageView)rootView.findViewById(R.id.previewImageView);
		Button btn = (Button)rootView.findViewById(R.id.autoFocusButton);
		btn.setOnClickListener(this);
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		parentLayout = (FrameLayout)rootView.findViewById(R.id.previewFrameLayout);
		orangePaymentSlipSurfaceView = new OrangePaymentSlipSurfaceView(getActivity(), getActivity(), this);
		parentLayout.addView(orangePaymentSlipSurfaceView);
	}

	@Override
	public void onPause() {
		super.onPause();
		
		orangePaymentSlipSurfaceView = null;
		parentLayout.removeAllViews();
	}
	
	public void scanningFinnished(OrangePaymentSlipData resultData) {
		((OrangePaymentSlipDelegate)getActivity()).scanningFinnished(resultData);
	}
	
	public static boolean deviceIsCapableOfScanning(Context context) {
		PackageManager pm = context.getPackageManager();
		return pm.hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	@Override
	public void onClick(View v) {
		orangePaymentSlipSurfaceView.autofocus();
	}
}