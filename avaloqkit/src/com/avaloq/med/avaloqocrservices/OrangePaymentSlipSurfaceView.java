package com.avaloq.med.avaloqocrservices;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.avaloq.framework.R;

public class OrangePaymentSlipSurfaceView extends SurfaceView implements SurfaceHolder.Callback , SensorEventListener, AutoFocusCallback {
	private Activity parentActivity;
	private OrangePaymentSlipFragment parentFragment;
	private byte[] FrameData = null;
	private double imageWidth = 800;
	private double imageHeight = 600;
	private int bProcessing = 0;
	
	private int[] markerCoordinates = new int[4];
	private int[] oldMarkerCoordinates = new int[4];
	
	private ImageView leftIndicatorImageView = null;
	private ImageView rightIndicatorImageView = null;

	private float z = 1;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;

	SurfaceHolder mHolder;
	public Camera camera;
	
	private int autofocusCounter = 30;
	private boolean shouldDoAutoFocus = false;
	
	boolean recognized = false;
	OrangePaymentSlipData recognizedData = null;
	
	private String chosenFocusMode = null;

	public OrangePaymentSlipSurfaceView(Context context){
		super(context);
	}
	
	public OrangePaymentSlipSurfaceView(Context context, Activity pa, OrangePaymentSlipFragment pf) {
		super(context);
		
		// Setting delegates.
		parentActivity = pa;
		parentFragment = pf;
		
		// Install a SurfaceHolder.Callback so we get notified when the
		// underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
		mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}
	
	public static int focusModeRank(String aFocusMode) {
		if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
			return 3;
		} else if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
			return 2;
		} else if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_MACRO)) {
			return 1;
		} else if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_AUTO)) {
			return 0;
		} else if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_FIXED)) {
			return 0;
		} else if(aFocusMode.equals(Camera.Parameters.FOCUS_MODE_INFINITY)) {
			return 0;
		} 
		return -1;
	}

	public void surfaceCreated(SurfaceHolder holder) {
		// Show message to the user what to do.
		Toast.makeText(parentActivity, R.string.pmt_scan_message, Toast.LENGTH_LONG).show();
		
		// Register for accelerometer change notifications.
		mSensorManager = (SensorManager) parentActivity.getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
		
		// Initialize red indicator images.
		leftIndicatorImageView = (ImageView)parentFragment.rootView.findViewById(R.id.leftIndicatorImageView);
		rightIndicatorImageView = (ImageView)parentFragment.rootView.findViewById(R.id.rightIndicatorImageView);
		
		// Clear old position markers.
		oldMarkerCoordinates[0] = oldMarkerCoordinates[1] = oldMarkerCoordinates[2] = oldMarkerCoordinates[3] = 0;
		
		// Set up camera.
		camera = Camera.open();
		if(camera == null) {
			return;
		}
		
		// Get current camera parameters that needs to be changed.
		Parameters params = camera.getParameters();
		
		// Choose the camera preview size so that the height is closest to 720 pixels.
		List<Size> sizes = params.getSupportedPreviewSizes();
		Camera.Size result = null, chosenSize = null;
		for (int i=0;i<sizes.size();i++){
			result = (Size) sizes.get(i);
			if(	chosenSize == null || 
				Math.abs(result.height - 720) < Math.abs(chosenSize.height - 720) || 
				(result.height == chosenSize.height && result.width > chosenSize.width)
			  ) {
				chosenSize = result;
			}
			Log.i("SupportedPreviewSize", "Supported preview size: " + result.width + " X " + result.height); 
		}
		imageWidth = chosenSize.width;
		imageHeight = chosenSize.height;
		Log.d("SupportedPreviewSize", String.format("Chosen: %d x %d", (int)imageWidth, (int)imageHeight));
		params.setPreviewSize(chosenSize.width, chosenSize.height);
		
		// Choose the focus mode with maximum rank.
		List<String> focusModes = params.getSupportedFocusModes();
		String currentFocusMode = null;
		chosenFocusMode = null;
		for (int i=0;i<focusModes.size();i++){
			currentFocusMode = (String) focusModes.get(i);
			if(chosenFocusMode == null || focusModeRank(currentFocusMode) > focusModeRank(chosenFocusMode)) {
				chosenFocusMode = currentFocusMode;
			}
			Log.i("SupportedFocusModes", "Supported focus mode: " + currentFocusMode); 
		}
		Log.d("SupportedFocusModes", "Chosen: " + chosenFocusMode);
		params.setFocusMode(chosenFocusMode);
		
		// Choose the preview format.
		List<Integer> formats = params.getSupportedPreviewFormats();
		Integer currentFormat = null, chosenFormat = null;
		for (int i=0;i<formats.size();i++){
			currentFormat = (Integer) formats.get(i);
			Log.i("SupportedFormat", "Supported format: " + currentFormat.toString()); 
		}
		// Set the preview format to NV21 (it is always supported).
		params.setPreviewFormat(ImageFormat.NV21);
		Log.i("SupportedPreviewFormats", "Preview format is always set to NV21 (" + (new Integer(ImageFormat.NV21)).toString() + ")"); 
		
		// Update the camera with the new parameters.
		camera.setParameters(params);
		
		// Train the NN algorithm from the file (orange_payments_reference_data).
		InputStream input = getResources().openRawResource(R.raw.pmt_orange_payments_reference_data);
		DataInputStream dataInput = new DataInputStream(input);
		float numOfImages = -12, cols = -12;
		try {
			if(!OrangePaymentSlipRecognitionLogic.isAlgorithmTrained()) {
				numOfImages =  Float.intBitsToFloat(readLittleInt(dataInput));  
				cols = Float.intBitsToFloat(readLittleInt(dataInput)); 

				float data[] = new float[(int)(numOfImages*cols)];
				float values[] = new float[(int)numOfImages];

				for(int i=0; i<numOfImages; i++) {
					for(int j=0; j<cols; j++) {
						data[i * (int)cols + j] = Float.intBitsToFloat(readLittleInt(dataInput));
					}
					values[i] = Float.intBitsToFloat(readLittleInt(dataInput));
				}
				
				OrangePaymentSlipRecognitionLogic.trainAlgorithm(data, values, (int)numOfImages, (int)cols);

				Log.i("TrainingAlgorithm", "Success.");
				Log.i("TrainingAlgorithm", String.format("Number of images: %.0f", numOfImages));
				Log.i("TrainingAlgorithm", String.format("Number of columns: %.0f", cols));
			}
		}catch(IOException e) {
			Log.e("TrainingAlgorithm", "Failed!");
			e.printStackTrace();
		}
		
		
		try {
			camera.setPreviewDisplay(holder);
			camera.setPreviewCallback(new PreviewCallback() {
				// Called for each frame that is shown.
				OrangePaymentSlipSurfaceView parent;
				
				PreviewCallback changeParent(OrangePaymentSlipSurfaceView p) {
					parent = p;
					return this;
				}
				
				public void onPreviewFrame(byte[] data, Camera camera) {
					// Some devices do not have auto focus so it has to be called manually.
					if(shouldDoAutoFocus && parentActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)) {
						shouldDoAutoFocus = false;
						autofocusCounter = 30;
						try {
							camera.autoFocus(parent);
						} catch (Exception e) {
							Log.e("CAMERA", "Manual focus failed.");
							e.printStackTrace();
						}
					}
					
					if(bProcessing-- == 0 && !recognized) {
						FrameData = data;

						new AsyncTask<OrangePaymentSlipSurfaceView, Void, OrangePaymentSlipData>() {
							OrangePaymentSlipSurfaceView parentSurfaceView;
							@Override
							protected OrangePaymentSlipData doInBackground(OrangePaymentSlipSurfaceView ...arg0) {
								// Get parent reference.
								parentSurfaceView = arg0[0];
								
								OrangePaymentSlipData resultData = new OrangePaymentSlipData();
										
								recognized = OrangePaymentSlipRecognitionLogic.applyRecognitionLogic(
										(int)imageWidth,
										(int)imageHeight,
										FrameData,
										parentFragment.parentLayout.getWidth(),
										parentFragment.parentLayout.getHeight(),
										getScreenOrientation(),
										z,
										markerCoordinates,
										resultData
										);
								
								Log.i("Frame","Frame");
								
								bProcessing = 0;

								if(recognized) {
									return resultData;
								} else {
									return null;
								}
							}

							@Override
							protected void onPostExecute(OrangePaymentSlipData result) {
								super.onPostExecute(result);
								if(
										oldMarkerCoordinates[0] == markerCoordinates[0]-20 && 
										oldMarkerCoordinates[1] == markerCoordinates[1]-20 &&
										oldMarkerCoordinates[2] == markerCoordinates[2]-20 &&
										oldMarkerCoordinates[3] == markerCoordinates[3]-20 &&
										!recognized
										)
								{
									shouldDoAutoFocus = false;
								} else {
									Animation moveLeftIndicator = new TranslateAnimation(oldMarkerCoordinates[0], markerCoordinates[0]-20, oldMarkerCoordinates[1], markerCoordinates[1]-20);
									moveLeftIndicator.setDuration(300);
									moveLeftIndicator.setFillAfter(true);
									
									
									Animation moveRightIndicator = new TranslateAnimation(oldMarkerCoordinates[2], markerCoordinates[2]-20, oldMarkerCoordinates[3], markerCoordinates[3]-20);
									moveRightIndicator.setDuration(300);
									moveRightIndicator.setFillAfter(true);
									
									oldMarkerCoordinates[0] = markerCoordinates[0]-20;
									oldMarkerCoordinates[1] = markerCoordinates[1]-20;
									oldMarkerCoordinates[2] = markerCoordinates[2]-20;
									oldMarkerCoordinates[3] = markerCoordinates[3]-20;
									
									leftIndicatorImageView.clearAnimation();
									rightIndicatorImageView.clearAnimation();

									if(recognized) {
										int w = (int)imageWidth;
										int h = (int)imageHeight;
										Camera.Parameters params = parentSurfaceView.camera.getParameters();
										int format = params.getPreviewFormat();
										YuvImage image = new YuvImage(parentSurfaceView.FrameData, format, w, h, null);

										ByteArrayOutputStream out = new ByteArrayOutputStream();
										Rect area = new Rect(0, 0, w, h);
										image.compressToJpeg(area, 50, out);
										Bitmap bm = BitmapFactory.decodeByteArray(out.toByteArray(), 0, out.size());
										parentSurfaceView.parentFragment.previewImageView.setImageBitmap(bm);
										
										
										moveLeftIndicator.setAnimationListener(new Animation.AnimationListener() {
											OrangePaymentSlipFragment parent;
											OrangePaymentSlipData result;
											
											Animation.AnimationListener changeParentAndResult(OrangePaymentSlipFragment p, OrangePaymentSlipData r) {
												parent = p;
												result = r;
												return this;
											}
											
											@Override
											public void onAnimationStart(Animation animation) {}
											
											@Override
											public void onAnimationRepeat(Animation animation) {}
											
											@Override
											public void onAnimationEnd(Animation animation) {
												Animation moveSuccessfulyRecognized = new TranslateAnimation(oldMarkerCoordinates[0], oldMarkerCoordinates[2], oldMarkerCoordinates[1], oldMarkerCoordinates[3]);
												moveSuccessfulyRecognized.setDuration(1000);
												moveSuccessfulyRecognized.setFillAfter(true);
												moveSuccessfulyRecognized.setStartOffset(200);
												
												rightIndicatorImageView.setVisibility(View.INVISIBLE);
												LayoutParams params = (LayoutParams) leftIndicatorImageView.getLayoutParams();
												int width = params.width;
												int height = params.height;
												leftIndicatorImageView.setImageResource(R.drawable.pmt_searching);
												params = (LayoutParams) leftIndicatorImageView.getLayoutParams();
												params.width = (int)(width*1.5);
												params.height = (int)(height*1.5);
												// existing height is ok as is, no need to edit it
												leftIndicatorImageView.setLayoutParams(params);
												
												moveSuccessfulyRecognized.setAnimationListener(new Animation.AnimationListener() {
													OrangePaymentSlipFragment parent;
													OrangePaymentSlipData result;
													
													Animation.AnimationListener changeParentAndResult(OrangePaymentSlipFragment p, OrangePaymentSlipData r) {
														parent = p;
														result = r;
														return this;
													}
													
													@Override
													public void onAnimationStart(Animation animation) {}
													
													@Override
													public void onAnimationRepeat(Animation animation) {}

													@Override
													public void onAnimationEnd(Animation animation) {
														final Handler handler = new Handler();
														handler.postDelayed(new Runnable() {
															@Override
															public void run() {
																LayoutParams params = (LayoutParams) leftIndicatorImageView.getLayoutParams();
																int width = params.width;
																int height = params.height;
																leftIndicatorImageView.setImageResource(R.drawable.pmt_ok);
																params = (LayoutParams) leftIndicatorImageView.getLayoutParams();
																params.width = (int)(width*1.1);
																params.height = (int)(height*1.1);
																// existing height is ok as is, no need to edit it
																leftIndicatorImageView.setLayoutParams(params);
															}
														}, 250);
														handler.postDelayed(new Runnable() {
															@Override
															public void run() {
																closeCamera();
																parent.scanningFinnished(result);
															}
														}, 750);
													}
												}.changeParentAndResult(parent, result));
												leftIndicatorImageView.startAnimation(moveSuccessfulyRecognized);
											}
										}.changeParentAndResult(parentFragment, result));
										
									}
									
									if(recognized) {
										moveRightIndicator.setFillAfter(false);
									}
									
									leftIndicatorImageView.startAnimation(moveLeftIndicator);
									rightIndicatorImageView.startAnimation(moveRightIndicator);
								}
								
								if(recognized) {
									parentSurfaceView.camera.stopPreview();
								}
							}

						}.execute(parent);
					} else {
						data = null;
					}
				}
			}.changeParent(this));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void autofocus() {
		shouldDoAutoFocus = true;
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		camera.startPreview();

		int rotation  = getScreenOrientation();
		int degrees = 0;

		switch (rotation) {
			case ActivityInfo.SCREEN_ORIENTATION_PORTRAIT: degrees = 90; break;
			case ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE: degrees = 0; break;
			case ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT: degrees = 270; break;
			case ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE: degrees = 180; break;
		}

		camera.setDisplayOrientation(degrees); 

		double height_width = imageHeight/imageWidth;
		double width_height = imageWidth/imageHeight;

		double tmp = height_width;

		if(rotation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
			height_width = width_height;
			width_height = tmp;
		}

		double verticalCrop = 0;
		double horizontalCrop = 0;

		if(parentFragment.parentLayout.getWidth() * 1.0 / parentFragment.parentLayout.getHeight()  < width_height) {
			horizontalCrop = (parentFragment.parentLayout.getHeight() * width_height - parentFragment.parentLayout.getWidth())/2.0;
		} else if (parentFragment.parentLayout.getWidth() * 1.0 / parentFragment.parentLayout.getHeight() > width_height) {
			verticalCrop = (parentFragment.parentLayout.getWidth() * height_width - parentFragment.parentLayout.getHeight())/2.0;
		}

		(this).layout(-(int)horizontalCrop, -(int)verticalCrop, parentFragment.parentLayout.getWidth() + (int)horizontalCrop, parentFragment.parentLayout.getHeight() + (int)verticalCrop);
		
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
		lp.setMargins(-(int)horizontalCrop, 0, 0, 0);
		lp.width = parentFragment.parentLayout.getWidth() + 2*(int)horizontalCrop;
		lp.height = parentFragment.parentLayout.getHeight() + 2*(int)verticalCrop;
		parentFragment.previewImageView.setLayoutParams(lp);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1) {}


	@Override
	public void onSensorChanged(SensorEvent arg0) {
		z = arg0.values[2];
		z = (float)(z / 10.3);
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		closeCamera();
	}
	
	public void closeCamera() {
		if(mSensorManager != null) {
			mSensorManager.unregisterListener(this);
		}
		
		if(camera != null) {
			camera.setPreviewCallback(null);
			camera.stopPreview();
			camera.release();
			camera = null;
		}
	}
	
	
	private int getScreenOrientation() {
		int rotation = parentActivity.getWindowManager().getDefaultDisplay().getRotation();
		DisplayMetrics dm = new DisplayMetrics();
		parentActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width = dm.widthPixels;
		int height = dm.heightPixels;
		int orientation;
		// if the device's natural orientation is portrait:


		if ((rotation == Surface.ROTATION_0
				|| rotation == Surface.ROTATION_180) && height > width ||
				(rotation == Surface.ROTATION_90
				|| rotation == Surface.ROTATION_270) && width > height) {
			switch(rotation) {
			case Surface.ROTATION_0:
				orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;
			case Surface.ROTATION_90:
				orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				break;
			case Surface.ROTATION_180:
				orientation =
				ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
			case Surface.ROTATION_270:
				orientation =
				ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			default:
				Log.e("Preview", "Unknown screen orientation. Defaulting to " + "portrait.");
				orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;              
			}
		}
		// if the device's natural orientation is landscape or if the device
		// is square:
		else {
			switch(rotation) {
			case Surface.ROTATION_0:
				orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				break;
			case Surface.ROTATION_90:
				orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
				break;
			case Surface.ROTATION_180:
				orientation =
				ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
				break;
			case Surface.ROTATION_270:
				orientation =
				ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
				break;
			default:
				Log.e("Preview", "Unknown screen orientation. Defaulting to " +
						"landscape.");
				orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
				break;              
			}
		}

		return orientation;
	}
	
	public final int readLittleInt(DataInputStream dataInStream) throws IOException {
		byte byteBuffer[];
		byteBuffer = new byte[8];
		dataInStream.readFully(byteBuffer, 0, 4);
		return (byteBuffer[3]) << 24 | (byteBuffer[2] & 0xff) << 16 |
				(byteBuffer[1] & 0xff) << 8 | (byteBuffer[0] & 0xff);
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera) {
		camera.cancelAutoFocus();
	}

}
