package com.avaloq.banklet.wealth;

import android.app.Activity;

import com.avaloq.banklet.wealth.activity.WealthStartActivity;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.Banklet;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.ActivityNavigationItem;

public class WealthBanklet extends Banklet {
	
	private static final ActivityNavigationItem[] items = new ActivityNavigationItem[] {
		new ActivityNavigationItem(AvaloqApplication.getContext(), WealthStartActivity.class, R.string.wea_banklet_name, R.attr.IconWealth, R.style.Theme_Banklet_WEA)
	};

	@Override
	public void onCreate() {
	}

	@Override
	public ActivityNavigationItem[] getMainNavigationItems() {
		return items;
	}

	@Override
	public ActivityNavigationItem getInitialActivity() {
		return items[0];
	}
	
	public static String getProgressColor(Activity activity, int colorPosition) {
		String[] array = activity.getResources().getStringArray(R.array.wea_graph_colors);
		return array[colorPosition % array.length];
	}

	@Override
	public void onEmptyCache() {
		// TODO avq_activity_empty all models (null their instances?)
	}
	
	/**
	 * Get the WealthBanklet singleton instance
	 */
	public static WealthBanklet getInstance() {
		return (WealthBanklet) Banklet.getInstanceOf(WealthBanklet.class);
	}

}