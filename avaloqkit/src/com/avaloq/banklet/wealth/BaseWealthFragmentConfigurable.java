package com.avaloq.banklet.wealth;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;

public abstract class BaseWealthFragmentConfigurable<T> extends BaseWealthFragment<T> {
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		setTableHeaderView(view);
		super.onViewCreated(view, savedInstanceState);
	}
	
	protected void setTableHeaderView(View view){
		ViewGroup tableHeader = (ViewGroup)view.findViewById(R.id.tableHeader);
		if (tableHeader != null){
			tableHeader.removeAllViews();
			int resId = getActivity().getResources().getIdentifier(getTitleLayoutName(), "layout", getActivity().getPackageName());
			ViewGroup titleRow = (ViewGroup)getActivity().getLayoutInflater().inflate(resId, null, false);
			
			int[] weights = WealthListTableConfigurable.getWeightsArray(getActivity(), getConfigurationName());
			WealthListTableConfigurable.applyWeightsToView(titleRow, weights);
		
			tableHeader.addView(titleRow);
		}
	}
	
	/**
	 * Can be overridden by the subclass to specify another layout file.
	 * @return
	 */
	public String getTitleLayoutName(){
		return "wea_conf_overview_title";
	}
	
	/**
	 * Can be overridden by the subclass to specify another configuration array.
	 * @return
	 */
	public String getConfigurationName(){
		return "wea_conf_overview";
	}
}
