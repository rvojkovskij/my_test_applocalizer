package com.avaloq.banklet.wealth;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;

/** @author: Ventsislav Zahariev <v.zahariev@insign.ch>
 * 
 * This class is built as a Composite pattern and encompasses the functionality
 * of a (possibly nested) table. It is intended to be used as the base class for
 * an adapter for the ListView in the standard Wealth Layout (consisting of 
 * a chart and a list) - see WealthStandardLayout. Since the lists in the Wealth
 * Banklet can not only have different layouts or varying amount of columns
 * that should be displayed, but more importantly operate on different types of data,
 * the class makes no assumptions about the underlying structure of the data.
 * Instead, the class requires the user to extend the class and specify the
 * methods which will be used to retrieve column information from the data.  
 * 
 * Usage: The WealthListRow is always supposed to be subclassed. In the constructor
 * one should specify the columns, which will be displayed. For each column, the 
 * user should define a method fillColumn<column_name>, which then will be called
 * from the base class via reflection. Some basic fillColumn-functions are already
 * implemented, which means that the user can simply define model-getters, which
 * will then be automatically called from the predefined fillColumn-functions.
 * 
 * This class is also the interface, that will be used to retrieve the data for
 * the chart view in WealthStandardLayout. For that reason, the method getProgress
 * (and optionally getProgressMax) must be implemented in the subclass. TODO: Maybe 
 * WealthListRow can extend from a real interface to force the implementation of
 * that method.
 * 
 */

public class WealthListTable<T extends Object> extends BaseAdapter{
	
	private static final String TAG = WealthListTable.class.getSimpleName();
	
	private List<FieldInterface<T>> mFields;
	
	// List of items that should be rendered as rows (top level)
	protected List<T> items;
	protected Activity activity;
	protected int layoutId;
	
	// List of nested tables. The id in the list corresponds to the id in the
	// this.items-List (which in this case represents a table header).
	protected List<? extends WealthListTable<?>> rows;
	
	// Used for easier distinguishing between row types when given the overall position in the list
	private List<Integer> headerPositions = new ArrayList<Integer>();

	// Used to designate a continuation of nested tables. Especially important
	// for the assignment of progress bar colors to the list items. 
	protected int startId = 0;
	protected double maxProgress;
	protected int maxScrollPosition = 0;
	
	/**
	 * @param layout - The xml-layout to be used for the rendering of this.items
	 * @param columnNames - identifiers used for the reflection methods
	 * @param items - top level rows (headers)
	 * @param rows - lower level rows (could also be nested tables)
	 */
	public WealthListTable(Activity activity, int layout, List<FieldInterface<T>> aFields, List<T> items, List<? extends WealthListTable<?>> rows){
		this.mFields = aFields;
		this.activity = activity;
		this.layoutId = layout;
		initializeAdapter(items, rows);
	}
	
	
	// TODO: This is a really complicated logic. It must be simplified -vz
	private void checkForEndResults(List<T> items, List<? extends WealthListTable<?>> rows){
		if (items == null && rows == null){
			onEndScrollToBottom();
			return;
		}
		if (items.size() == 0){
			onEndScrollToBottom();
			return;
		}
		if (this.items != null && this.rows != null){
			if (items.size() == this.items.size() && rows.size() == this.rows.size()){
				onEndScrollToBottom();
				return;
			}
		}
	}
	
	public void initializeAdapter(WealthListTable<T> aAdapter){
		initializeAdapter(aAdapter.items, aAdapter.rows);
	}
	
	public void initializeAdapter(List<T> items, List<? extends WealthListTable<?>> rows){
		checkForEndResults(items, rows);
		this.items = items;
		this.rows = rows;
		
		int headerPosition = 0;
		int count = 0;
//		double currentMaxProgress = 0;

//		currentMaxProgress = 0;
		
		/**
		 * This block traverses the items and does several computations at once.
		 * Determining (and setting) the maximum progress level in each sub-table, because
		 * the charts could (and usually do) comprise of several sub-items (this.rows).
		 * Setting the startId of each sub-item - important for the color assignment of each
		 * sub-table.
		 */
		for (int i=0; i<items.size(); i++){
			headerPositions.add(headerPosition);
			if (rows != null){
				WealthListTable<?> row = rows.get(i);
				headerPosition+= 1+row.getCount();
				row.setStartId(count);
//				if (row.getCount()>0){
//					Object obj = row.getElement(0);
//					double progressValue = 0;
//					/** Trying to find user-generated getProgressMax method, otherwise fallback
//					 * to the predefined method. This is necessary due to the used generic type
//					 * - apparently this makes Java's reflection a little cranky.
//					 */
//					try {
//						Method methodAmount = row.getClass().getMethod("getProgressMax", obj.getClass());
//						progressValue = (Double)methodAmount.invoke(row, obj);
//					} catch (Exception e) {
//						try {
//							Method methodAmount = row.getClass().getMethod("getProgressMax", Object.class);
//							progressValue = (Double)methodAmount.invoke(row, obj);
//						} catch (Exception exc){
//							exc.printStackTrace();
//						}
//					}
//					if (currentMaxProgress < progressValue)
//						currentMaxProgress = progressValue;
//				}
				count+=row.getCount();
			}
			else
				headerPosition+= 1;
		}
		
		onCreate();
	}
	
	/** 
	 * Gets a specific subtable, not a specific row from the composite table.
	 * This method is only used by the WealthStandardLayout as a part of the 
	 * interface used by the Chart classes. It is not intended to be used outside
	 * of the WealthStandardLayout.
	 */
	public WealthListTable<?> getRow(int location){
		return rows.get(location);
	}
	
	/** 
	 * Also supposed to be used only by the WealthStandardLayout as a part of 
	 * the Chart interface. Gets a top-level row of the table (a header in the
	 * general case).
	 */
	public T getElement(int location){
		Log.d(TAG, "getElement: count = " + items.size());
		return items.get(location);
	}
	
	/** 
	 * Also supposed to be used only by the WealthStandardLayout as a part of 
	 * the Chart interface. Gets the amount of headers in the current table.
	 */
	public int getElementCount(){
		return items.size();
	}
	
	private void setStartId(int start){
		startId = start;
	}
	
	/** 
	 * BaseAdapter method. Gets the amount of rows in the whole composite table
	 * including sub-tables.
	 */
	@Override
	public int getCount() {
		int count = 0;
		if (rows != null){
			for (int i=0; i<items.size(); i++)
				// 1 for the header in the current table, getCount for the sub-tables.
				count+= 1+rows.get(i).getCount();
			return count;
		}
		else
			return items.size();
		
	}
	/**
	 * Gets an item by id relative to the whole composite table. This function
	 * may have to be redirected to sub-tables, if the item is not a header in
	 * the current object. The function getItemPositionFromAdapterPosition is
	 * used to determine which sub-table is needed.
	 */
	@Override
	public Object getItem(int position) {
		int headerPosition = headerPositions.indexOf(position);
		if (headerPosition != -1)
			return items.get(headerPosition);
		
		int[] itemPosition = getItemPositionFromAdapterPosition(position);
		if (itemPosition.length != 2 || itemPosition[0] == -1)
			return null;
		else
			return rows.get(itemPosition[0]).getItem(itemPosition[1]);
	}
	
	/** 
	 * Implementation of a BaseAdapter method.
	 */
	@Override
	public long getItemId(int position) {
		return startId+position;
	}
	
	/** 
	 * Implementation of a BaseAdapter method. If the position corresponds
	 * to an element from the current object, it gets drawn, otherwise redirect
	 * the method call to the appropriate sub-table.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (position+1 == getCount())
			scrolledToBottom(position, convertView, parent);
		
		int[] itemPos = getItemPositionFromAdapterPosition(position);
		if (itemPos.length != 2 || itemPos[0] == -1)
			return null;
		if (itemPos[1] == -1)
			return drawRow(itemPos[0], convertView, parent);
		return getRowView(itemPos[0], itemPos[1], convertView, parent);
	}
	
	/**
	 * Traverse all columnNames and call get<column_name> and fillColumn<colum_name> 
	 * to render the column. Set onClickListener to each row.
	 */
	protected View drawRow(final int position, View convertView, ViewGroup parent) {
		View view = activity.getLayoutInflater().inflate(layoutId, parent, false);
		T item = items.get(position);
		for(FieldInterface<T> field : mFields) {
			field.fillView(view, position, item);
		}
		view.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				WealthListTable.this.onClick(position, WealthListTable.this.items.get(position));
			}
		});
		return view;
	}
	
	// Redirect the draw row method call to the corresponding sub-table.
	private View getRowView(int headerPosition, int itemPosition, View convertView, ViewGroup parent) {
		return rows.get(headerPosition).getView(itemPosition, convertView, parent);
	}
	
	/**
	 * The location parameter in the getView method refers to rows in the composite table.
	 * This function is used to map this location to the corresponding subtable or item in 
	 * the current table.
	 * 
	 * first element is the index of the header
	 * second element is the index of the row within that header
	 * if the second element is -1, it is the header itself
	 * if the first element is -1, nothing was found
	 */
	private int[] getItemPositionFromAdapterPosition(int position){
		int count = 0;
		int header = 0;
		int previous = 0;
		if (rows != null){
			for (WealthListTable<?> list: rows){
				count+=list.getCount()+1;
				if (position<count){
					int[] a = {header, position-previous-1};
					return a;
				}
				header++;
				previous = count;
			}
			int[] a = {-1, -1};
			return a;
		}
		else{
			int[] a = {position, -1};
			return a;
		}
	}
	
	public void onCreate(){}
	public void onClick(int position, T item){}
	
	private ChartDataProvider<T> mChartDataProvider = null;

	public ChartDataProvider<T> getChartDataProvider() {
		return mChartDataProvider;
	}
	
	public void setChartDataProvider(ChartDataProvider<T> aChartDataProvider) {
		mChartDataProvider = aChartDataProvider;
	}

	public PagerAdapter getChartAdapter(Activity activity, int chartWidth, int chartHeight, int ... positions) {
		if(positions == null || positions.length == 0) {
			return new ChartPagerAdapter<T>(activity, this, this.getChartDataProvider(), chartWidth, chartHeight);
		} else {
			int nextPosition = positions[0];
			int[] restPositions = new int[positions.length-1];
			for(int i = 1;i < positions.length;i++) {
				restPositions[i-1] = positions[i];
			}
			return this.rows.get(nextPosition).getChartAdapter(activity, chartWidth, chartHeight, restPositions);
		}
	}
	
	protected void scrolledToBottom(int position, View convertView, ViewGroup parent) {
		if (maxScrollPosition < position){
			maxScrollPosition = position;
			onScrollToBottom(position, convertView, parent);
		}
	}
	
	public void onScrollToBottom(int position, View convertView, ViewGroup parent) {}
	public void onEndScrollToBottom() {}

}