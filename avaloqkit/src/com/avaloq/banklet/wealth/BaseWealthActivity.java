package com.avaloq.banklet.wealth;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.util.InlineMessageUtil;

/**
 * 
 * @author zahariev
 * This class is the top level abstraction for all Wealth views, that are not tabbable
 * and are not tabs of tabbable views. This class handles the models, the WealthStandardLayout,
 * the adapters and the observers for the developer.
 *
 * Usage: The Activities should extend this class and implement the abstract methods.
 *  
 */

public abstract class BaseWealthActivity<T> extends BankletActivity implements Observer{
	private enum Mode {
		LOAD, REFRESH;
	}
	
	/**
	 * created by the developer in the overridden createAdapter()
	 */
	protected WealthListTable<T> adapter;
	
	/**
	 * created by the developer in the overridden createModel()
	 */
	protected Observable model;
	
	/**
	 * created dynamically in update() based on the specified WealthListTable<?> adapter.
	 * Alter this object to force changes in the UI (for example refresh the activity).
	 */
	protected WealthStandardLayout<T> standardLayout;
	
	/**
	 * Used to differentiate between the initial loading of the model data
	 * and the refreshing of the model.
	 */
	Mode mode = Mode.LOAD;
	
	/**
	 * This method does the basic initialization of the activity. It creates the specified
	 * model for the activity and binds an observer to it, so that once the web service
	 * request is finished the update method gets called automatically.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setupExtras();
		model = this.createModel();
		model.deleteObservers();
		model.addObserver(this);
		try{
			this.loadModelData();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of parameters, the model
	 * classes may require different initialization routines. The activities should override this
	 * method, initialize the required model and return it, so that the BaseWealthActivity can
	 * handle it further for the developer.
	 * 
	 */
	public abstract Observable createModel();
	
	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of parameters, the model
	 * classes may require different initialization routines. The Wealth activities should override this
	 * method, and use the 
	 */
	public abstract void loadModelData() throws IOException;
	
	/**
	 * 
	 * The activities should override this method, create an instance of WealthListTable and return it,
	 * so that it can be handled by the BaseActivity (rendering lists and charts).
	 */
	public abstract WealthListTable<T> createAdapter();
	
	/**
	 * If there are any activity parameters passed as extras, override this method to initilize attributes.
	 */
	public abstract void setupExtras();
	
	/**
	 * This method should return the id of a view from the layout, so that once the web-service request is done,
	 * the BaseActivity could post() to it (and thus return to the UI thread). 
	 * 
	 * See update(Observable observable, Object data) for a reference.
	 */
	public abstract int getPostElement();
	
	/**
	 * This method is called automatically after the web-service request is processed. It returns
	 * to the UI-thread, so that the UI can be updated.
	 * 
	 * IMPORTANT! Do not override this method directly. Instead, override the method update(),
	 * which will be executed within the scope of the UI-Thread.
	 * 
	 * // TODO CR: Why not make it final if you want to protect it from overwriting.
	 * 
	 */
	@Override
	public void update(Observable observable, Object data) {
		if (data == null){
			onCommunicationBreakdown();
		}
		else {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (mode == Mode.REFRESH)
						postRefresh();
					else
						update();
				}
			});
		}
		
		//regardless of the state, get back to the LOAD mode
		mode = Mode.LOAD;
	}
	/**
	 * This method gets called once the web-service request is processed. It is executed within the
	 * scope of the UI-Thread, so that UI elements can be updated directly. The method initializes
	 * the standard Wealth Layout (chart + list).
	 * 
	 * IMPORTANT! If you override this method, make sure to call super.update() first, so that the
	 * WealthStandardLayout gets populated and inflated.
	 * 
	 * // TODO CR: I generally don't like superclass-callthru-requirements, as they tend to be sources of bugs
	 * // Why not having a final update() method which does all the necessary stuff and an overwriteable, avq_activity_empty
	 * // (or even abstract) 'onUpdate()' method which is called from update()? See handler/callbacks in the Code Style Guide
	 * 
	 */
	public void update() {
		adapter = createAdapter();
		standardLayout = new WealthStandardLayout<T>(BaseWealthActivity.this, adapter, R.id.chart, R.id.report_list);
		View footer = getListFooterView();
		standardLayout.setChartPlaceholder();
		standardLayout.setListPlaceholder(footer);
	}
	
	/**
	 * Call this function when you want to trigger a refresh of the list.
	 * Do the actual calling of method-functions in doRefreshModelData().
	 */
	public final void refresh(){
		mode = Mode.REFRESH;
		doRefreshModelData();
	}
	
	private void postRefresh(){
		adapter.initializeAdapter(createAdapter());
		adapter.notifyDataSetChanged();
		onListRefreshed();
	}
	
	/**
	 * This function can be overridden and is called right after the list has been refreshed
	 */
	public void onListRefreshed(){}
	
	/**
	 * This function can be overridden with your implementation of the model data refresh
	 */
	public void doRefreshModelData(){}
	
	public WealthStandardLayout<T> getStandardLayout(){
		return this.standardLayout;
	}
	
	public void onCommunicationBreakdown(){
		ViewGroup vg = (ViewGroup)findViewById(android.R.id.content);
		InlineMessageUtil.showNetworkError(vg);
	}
	
	public ListView getListView(){
		return standardLayout.getListView();
	}
	
	public View getListFooterView(){
		return null;
	}
	
}
