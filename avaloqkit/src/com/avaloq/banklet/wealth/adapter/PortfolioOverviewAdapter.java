package com.avaloq.banklet.wealth.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragment;
import com.avaloq.afs.server.bsp.client.ws.PortfolioAllocationDeepItemTO;
import com.avaloq.banklet.wealth.WealthBanklet;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.WealthListTableConfigurable;
import com.avaloq.banklet.wealth.activity.PortfolioOverviewActivity;
import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.banklet.wealth.adapter.fields.ColorCodeField;
import com.avaloq.banklet.wealth.adapter.fields.CurrencyTextField;
import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;
import com.avaloq.banklet.wealth.adapter.fields.PercentageTextField;
import com.avaloq.banklet.wealth.adapter.fields.ProgressBarPortfolioField;
import com.avaloq.banklet.wealth.adapter.fields.TextField;
import com.avaloq.framework.R;

public class PortfolioOverviewAdapter {
	
	private static final String TAG = PortfolioOverviewAdapter.class.getSimpleName();
	
	public static WealthListTable<PortfolioAllocationDeepItemTO> getAdapter(final SherlockFragment aFragment, List<PortfolioAllocationDeepItemTO> allocations, ArrayList<Integer> aAllocIds, ArrayList<Integer> aItemIds){
		
		final PositionProvider<PortfolioAllocationDeepItemTO> portfolioAllocationDeepPositionProvider = new PositionProvider<PortfolioAllocationDeepItemTO>();
		
		List<FieldInterface<PortfolioAllocationDeepItemTO>> fields = new ArrayList<FieldInterface<PortfolioAllocationDeepItemTO>>();
		final ArrayList<Integer> allocIds = aAllocIds;
		final ArrayList<Integer> itemIds = aItemIds;
		
		fields.add(new TextField<PortfolioAllocationDeepItemTO>(R.id.name) {
			@Override
			public CharSequence getText(PortfolioAllocationDeepItemTO item) {
				return item.getName();
			}
		});
		fields.add(new TextField<PortfolioAllocationDeepItemTO>(R.id.description) {
			@Override
			public CharSequence getText(PortfolioAllocationDeepItemTO item) {
				return item.getShortName();
			}
		});
		fields.add(new CurrencyTextField<PortfolioAllocationDeepItemTO>(R.id.amount) {
			@Override
			public BigDecimal getAmount(PortfolioAllocationDeepItemTO item) {
				return item.getTotalValueInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(PortfolioAllocationDeepItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		
		
		
		
		
		
		fields.add(new CurrencyTextField<PortfolioAllocationDeepItemTO>(R.id.currentAmount) {
			@Override
			public BigDecimal getAmount(PortfolioAllocationDeepItemTO item) {
				return item.getTotalValueExclAccrualsInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(PortfolioAllocationDeepItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		
		fields.add(new CurrencyTextField<PortfolioAllocationDeepItemTO>(R.id.interest) {
			@Override
			public BigDecimal getAmount(PortfolioAllocationDeepItemTO item) {
				return item.getTotalAccruedInterestInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(PortfolioAllocationDeepItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		
		
		
		
		
		
		
		
		fields.add(new PercentageTextField<PortfolioAllocationDeepItemTO>(R.id.weight) {
			@Override
			public Double getPercentage(PortfolioAllocationDeepItemTO item) {
				return item.getRatio().doubleValue() * 100;
			}
		});
		/*fields.add(new ProgressBarField<PortfolioAllocationDeepItemTO>(R.id.progressBar, allocations) {
			@Override
			public int getProgress(PortfolioAllocationDeepItemTO item) {
				return Double.valueOf(item.getRatio() * 100).intValue();
			}
			@Override
			public int getProgressBarColor(PortfolioAllocationDeepItemTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), portfolioAllocationDeepPositionProvider.getPosition(item)));
			}
		});*/
		fields.add(new ProgressBarPortfolioField<PortfolioAllocationDeepItemTO>(aFragment.getActivity(), R.id.progressBar, allocations) {
			@Override
			public int getProgress(PortfolioAllocationDeepItemTO item) {
				return Double.valueOf(item.getRatio().doubleValue() * 100).intValue();
			}
			@Override
			public int getProgressBarColor(PortfolioAllocationDeepItemTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), portfolioAllocationDeepPositionProvider.getPosition(item)));
			}
			@Override
			public int getMinProgress(PortfolioAllocationDeepItemTO item) {
				if (item.getMinimumWeight() == null)
					return 0;
				return Double.valueOf(item.getMinimumWeight().doubleValue() * 100).intValue();
			}
			@Override
			public int getMaxProgress(PortfolioAllocationDeepItemTO item) {
				if (item.getMaximumWeight() == null)
					return 0;
				return Double.valueOf(item.getMaximumWeight().doubleValue() * 100).intValue();
			}
			@Override
			public int getOptimalProgress(PortfolioAllocationDeepItemTO item) {
				if (item.getOptimalWeight() == null)
					return 0;
				return Double.valueOf(item.getOptimalWeight().doubleValue() * 100).intValue();
			}
			@Override
			public boolean toShowPortfolioVals(PortfolioAllocationDeepItemTO item) {
				if (getOptimalProgress(item) == 0)
					return false;
				return true;
			}
			
		});
		fields.add(new ColorCodeField<PortfolioAllocationDeepItemTO>(R.id.colorCode, allocations) {
			@Override
			public int getProgressBarColor(PortfolioAllocationDeepItemTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), portfolioAllocationDeepPositionProvider.getPosition(item)));
			}
		});
		
		WealthListTableConfigurable<PortfolioAllocationDeepItemTO> wealthListTable = new WealthListTableConfigurable<PortfolioAllocationDeepItemTO>(aFragment.getActivity(), R.layout.wea_conf_overview_row, "wea_conf_overview", fields, allocations, null) {
			public void onClick(int position, PortfolioAllocationDeepItemTO item) {
				ArrayList<Integer> tmp = new ArrayList<Integer>(itemIds);
				tmp.add(position);
				Log.d(TAG, "Test: "+allocIds.toString());
				Log.d(TAG, "Test: "+tmp.toString());
				Intent intent = new Intent(activity, PortfolioOverviewActivity.class);
				intent.putIntegerArrayListExtra(PortfolioOverviewActivity.EXTRA_ALLOC_LIST, allocIds);
				intent.putExtra(PortfolioOverviewActivity.EXTRA_PORTFOLIO_NAME, item.getName());
				intent.putIntegerArrayListExtra(PortfolioOverviewActivity.EXTRA_ITEM_LIST, tmp);
				activity.startActivity(intent);
			};
		};
		wealthListTable.setChartDataProvider(new ChartDataProvider<PortfolioAllocationDeepItemTO>() {
			public boolean showLabel(PortfolioAllocationDeepItemTO item){
				int min = aFragment.getActivity().getResources().getInteger(R.integer.avq_min_chart_ration_to_show_label);
				return getRatio(item).doubleValue() > min;
			}
			
			@Override
			public BigDecimal getRatio(PortfolioAllocationDeepItemTO item) {
				return item.getRatio();
			}
			
			@Override
			public String getLabel(PortfolioAllocationDeepItemTO item) {
				/*if (showLabel(item))
					return item.getShortName();
				else
					return "";*/
				return item.getShortName();
			}
			
			@Override
			public int getColor(PortfolioAllocationDeepItemTO item) {
				return portfolioAllocationDeepPositionProvider.getPosition(item);
			}

			@Override
			public String getSubLabel(PortfolioAllocationDeepItemTO item) {
				/*if (showLabel(item))
					return new DecimalFormat("#.##").format(getRatio(item)) + "%";
				else
					return "";*/
				return new DecimalFormat("#.##").format(getRatio(item).movePointRight(2)) + "%";
			}
		});
		
		return wealthListTable;
	}
}
