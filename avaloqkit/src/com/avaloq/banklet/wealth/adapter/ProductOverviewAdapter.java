package com.avaloq.banklet.wealth.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;

import com.avaloq.afs.server.bsp.client.ws.PortfolioPositionTO;
import com.avaloq.banklet.trading.TradingSellDetailActivity;
import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.banklet.wealth.WealthBanklet;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.WealthListTableConfigurable;
import com.avaloq.banklet.wealth.activity.MoneyAccountActivity;
import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.banklet.wealth.adapter.fields.ColorCodeField;
import com.avaloq.banklet.wealth.adapter.fields.CurrencyTextField;
import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;
import com.avaloq.banklet.wealth.adapter.fields.PercentageTextField;
import com.avaloq.banklet.wealth.adapter.fields.ProgressBarField;
import com.avaloq.banklet.wealth.adapter.fields.TextField;
import com.avaloq.framework.R;
// TODO REMOVE ALL THE DIRECT REFERENCES TO THE TRADING BANKLET

public class ProductOverviewAdapter {
	public static WealthListTable<PortfolioPositionTO> getAdapter(final Activity activity, List<PortfolioPositionTO> positions){
		
		final PositionProvider<PortfolioPositionTO> positionProvider = new PositionProvider<PortfolioPositionTO>();
		
		List<FieldInterface<PortfolioPositionTO>> portfolioPositions = new ArrayList<FieldInterface<PortfolioPositionTO>>();
		portfolioPositions.add(new TextField<PortfolioPositionTO>(R.id.name) {
			@Override
			public CharSequence getText(PortfolioPositionTO item) {
				return item.getInstrument().getTitle();
			}
		});
		portfolioPositions.add(new TextField<PortfolioPositionTO>(R.id.description) {
			@Override
			public CharSequence getText(PortfolioPositionTO item) {
				if (item.isMoneyAccount())
					return item.getIban();
				else
					return item.getInstrument().getIsin();
			}
		});
		portfolioPositions.add(new CurrencyTextField<PortfolioPositionTO>(R.id.count) {
			@Override
			public BigDecimal getAmount(PortfolioPositionTO item) {
				return item.getQuantity();
			}
			@Override
			public Long getCurrencyId(PortfolioPositionTO item) {
				return 0l;
			}
		});
		portfolioPositions.add(new CurrencyTextField<PortfolioPositionTO>(R.id.interest) {
			@Override
			public BigDecimal getAmount(PortfolioPositionTO item) {
				return item.getAccruedInterest();
			}
			@Override
			public Long getCurrencyId(PortfolioPositionTO item) {
				return item.getCurrencyId();
			}
		});
		portfolioPositions.add(new CurrencyTextField<PortfolioPositionTO>(R.id.amount) {
			@Override
			public BigDecimal getAmount(PortfolioPositionTO item) {
				return item.getValueInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(PortfolioPositionTO item) {
				return item.getValuationCurrencyId();
			}
		});
		portfolioPositions.add(new CurrencyTextField<PortfolioPositionTO>(R.id.valuationAmount) {
			@Override
			public BigDecimal getAmount(PortfolioPositionTO item) {
				return item.getCurrentValue();
			}
			@Override
			public Long getCurrencyId(PortfolioPositionTO item) {
				return item.getCurrencyId();
			}
			@Override
			public boolean toShowValue(PortfolioPositionTO item){
				return item.getCurrencyId() != item.getValuationCurrencyId();
			}
		});
		portfolioPositions.add(new ColorCodeField<PortfolioPositionTO>(R.id.colorCode, positions) {
			@Override
			public int getProgressBarColor(PortfolioPositionTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(activity, positionProvider.getPosition(item)));
			}
		});
		portfolioPositions.add(new PercentageTextField<PortfolioPositionTO>(R.id.weight) {
			@Override
			public Double getPercentage(PortfolioPositionTO item) {
				return item.getRatio().doubleValue() * 100;
			}
		});
		portfolioPositions.add(new ProgressBarField<PortfolioPositionTO>(R.id.progressBar, positions) {
			@Override
			public int getProgress(PortfolioPositionTO item) {
				return Double.valueOf(item.getRatio().doubleValue() * 10000).intValue();
			}
			@Override
			public int getProgressBarColor(PortfolioPositionTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(activity, positionProvider.getPosition(item)));
			}
		});
		
		WealthListTableConfigurable<PortfolioPositionTO> wlt = new WealthListTableConfigurable<PortfolioPositionTO>(activity, R.layout.wea_conf_product_row, "wea_conf_product", portfolioPositions, positions, null){
			@Override
			public void onClick(int position, PortfolioPositionTO item) {
				if (item.isMoneyAccount()){
					Intent intent = new Intent(activity, MoneyAccountActivity.class);
					intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_ID, item.getId());
					intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_NAME, item.getInstrument().getTitle());
					intent.putExtra(MoneyAccountActivity.EXTRA_CURRENCY_ID, item.getCurrencyId());
					//intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_BALANCE, item.getCurrentValue());
					intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_BALANCE, item.getCurrentValueInclAccruals());
					intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_INTEREST, item.getAccruedInterest());
					intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_CURRENT, item.getCurrentValue());
					activity.startActivity(intent);
				}
				else {
					Intent intent = new Intent(activity, TradingSellDetailActivity.class);

					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_ID, item.getInstrument().getId());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_NAME, item.getInstrument().getTitle());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_TYPE, item.getInstrument().getStexGroupType().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_SUB_TYPE, item.getInstrument().getIsin());
					intent.putExtra(TradingSellDetailActivity.EXTRA_BANKING_POSITION_ID, item.getId());
					intent.putExtra(TradingSellDetailActivity.EXTRA_IS_BUY, ButtonSellBuyStatus.NONE);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_CURRENCY_ID, item.getCurrencyId());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_NAME, item.getInstrument().getDefaultListing().getMarketName());

					activity.startActivity(intent);
				}
			}
		};
		
		
		
		
		
		
		
		if (positions.size() <= activity.getResources().getInteger(R.integer.avq_max_amount_elements_in_charts) ) {
			wlt.setLayoutPrefix("wea_conf_product_with_chart");
			wlt.setChartDataProvider(new ChartDataProvider<PortfolioPositionTO>() {
				public boolean showLabel(PortfolioPositionTO item){
					int min = activity.getResources().getInteger(R.integer.avq_min_chart_ration_to_show_label);
					return getRatio(item).doubleValue() > min;
				}
				
				@Override
				public BigDecimal getRatio(PortfolioPositionTO item) {
					return item.getRatio();
				}
				
				@Override
				public String getLabel(PortfolioPositionTO item) {
					String label = "";
					
					if (item.isMoneyAccount())
						label = item.getIban();
					else
						label = item.getInstrument().getIsin();
					
					/*if (showLabel(item))
						return label;
					else
						return "";*/
					return label;
				}
				
				@Override
				public String getSubLabel(PortfolioPositionTO item){
					/*if (showLabel(item))
						return new DecimalFormat("#.##").format(getRatio(item)) + "%";
					else
						return "";*/
					return new DecimalFormat("#.##").format(getRatio(item).movePointRight(2)) + "%";
				}
				
				@Override
				public int getColor(PortfolioPositionTO item) {
					return positionProvider.getPosition(item);
				}
			});
		}
		
		
		
		
		
		
		
		
		return wlt;
	}
}
