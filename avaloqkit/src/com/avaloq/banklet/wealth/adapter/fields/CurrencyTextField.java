package com.avaloq.banklet.wealth.adapter.fields;

import java.math.BigDecimal;

import com.avaloq.framework.util.CurrencyUtil;

/**
 * Sets an amount of money with or without the currency in a TextView
 * @author Timo Schmid <t.schmid@insign.ch>
 * @param <T> The Type of object
 */
public abstract class CurrencyTextField<T> extends TextField<T> {

	/**
	 * Creates a new instance
	 * @param resId The ID of the TextView
	 */
	public CurrencyTextField(int resId) {
		super(resId);
	}
	
	@Override
	public CharSequence getText(T item) {
		if (toShowValue(item)){
			if (getAmount(item) != null)
				return CurrencyUtil.formatMoney(getAmount(item), getCurrencyId(item));
			else
				return "";
		} else {
			return "";
		}
	}
	
	/**
	 * Returns the amount of money
	 * @param item The item to get the data from
	 * @return The amount of money
	 */
	public abstract BigDecimal getAmount(T item);
	
	/**
	 * Returns the CurrencyId for the currency. You can return 0 in case you want no currency to be displayed.
	 * @param item The item to get the data from
	 * @return The CurrencyId or 0 if there should not be a currency displayed
	 */
	public abstract Long getCurrencyId(T item);
	
	public boolean toShowValue(T item){
		return true;
	}

}
