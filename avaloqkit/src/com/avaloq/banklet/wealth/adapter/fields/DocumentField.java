package com.avaloq.banklet.wealth.adapter.fields;

import java.io.File;

import com.avaloq.framework.DownloadClickListener;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

public abstract class DocumentField<T> implements FieldInterface<T>{
	protected int mResId;
	
	public DocumentField(int aResId) {
		mResId = aResId;
	}
	
	public abstract Long getDocumentId(final T item);
	public abstract Activity getActivity();
	public abstract void onPdfFileSet(File file);
	public abstract void displayPdf(Intent intent, int result_code);

	@Override
	public void fillView(View row, int position, T item) {
		View view = (View)row.findViewById(mResId);
		Long id = getDocumentId(item);
		if (view != null){
			if (id != null && !id.equals(0)){
				view.setVisibility(View.VISIBLE);
				
				view.setOnClickListener(new DownloadClickListener(getActivity(), getDocumentId(item)) {

					@Override
					protected void displayDocument(Intent intent, int result_code) {
						displayPdf(intent, result_code);
					}

					@Override
					protected Runnable getOnSetPdfCallback() {
						return new Runnable() {
							@Override
							public void run() {
								onPdfFileSet(getPdfFile());
							}
						};
					}
				});
			}
			else {
				view.setVisibility(View.INVISIBLE);
			}
		}
	}
}
