package com.avaloq.banklet.wealth.adapter.fields;

import java.util.List;

import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.PaintDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Sets the values for a ProgressBar
 * @author Timo Schmid <t.schmid@insign.ch>
 * @param <T> The type of the data item
 */
public abstract class ProgressBarField<T> implements FieldInterface<T> {
	
	private static final String TAG = ProgressBarField.class.getSimpleName();
	
	protected int mResId;
	
	protected int mMaxVal;

	/**
	 * Creates a new instance
	 * @param aResId The resource ID of the ProgressBar
	 * @param aAllItems All items of the List (to determine the highest progress bar value)
	 */
	public ProgressBarField(int aResId, List<T> aAllItems) {
		this.mResId = aResId;
		this.mMaxVal = 0;
		initializeMaxVal(aAllItems);
	}
	
	protected void initializeMaxVal(List<T> aAllItems){
		for(T item : aAllItems) {
			this.mMaxVal = Math.max(this.mMaxVal, getProgress(item));
		}
	}
	
	@Override
	public void fillView(View row, int position, T item) {
		ProgressBar pb = (ProgressBar)row.findViewById(mResId);
		if(pb != null) {
			pb.setMax(mMaxVal);			
			
			PaintDrawable pgDrawable = new PaintDrawable();
			pgDrawable.setCornerRadius(2);
			pgDrawable.getPaint().setColor(getProgressBarColor(item));
			
			ClipDrawable clip = new ClipDrawable(pgDrawable, Gravity.LEFT, ClipDrawable.HORIZONTAL);

			LayerDrawable ld = (LayerDrawable)pb.getProgressDrawable();
			ld.mutate();
			ld.setLayerInset(0, 5, 4, 5, 6);
			ld.setDrawableByLayerId(android.R.id.progress, clip);
			
			//pb.setProgressDrawable(ld);
			//pb.setProgressDrawable(clip);
			//pb.setBackgroundDrawable(row.getContext().getResources().getDrawable(R.drawable.wea_status_bar_bg));
			// pb.setProgressDrawable(row.getContext().getResources().getDrawable(R.drawable.abs__list_pressed_holo_light));
			pb.setProgress(getProgress(item));
		}
	}
	
	/**
	 * Returns the Progress for an item
	 * @param item The data item
	 * @return The progress
	 */
	public abstract int getProgress(T item);
	
	/**
	 * Returns the color for this ProgressBar
	 * @param item The data item
	 * @return The int Color for the progress bar
	 */
	public abstract int getProgressBarColor(T item);

}
