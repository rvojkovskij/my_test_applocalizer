package com.avaloq.banklet.wealth.adapter.fields;

import java.text.DecimalFormat;

/**
 * A field that displays a percentage.
 * @author Timo Schmid <t.schmid@insign.ch>
 * @param <T> The type of the data item
 */
public abstract class PercentageTextField<T> extends TextField<T> {

	/**
	 * Creates a new instance
	 * @param resId The id of the TextView
	 */
	public PercentageTextField(int resId) {
		super(resId);
	}
	
	@Override
	public CharSequence getText(T item) {
		Double percentage = getPercentage(item);
		if(percentage == null) {
			return "";
		} else {
			return new DecimalFormat("#.##").format(percentage) + "%";
		}
	}
	
	/**
	 * Returns the percentage to be displayed
	 * @param item The data item
	 * @return The percentage
	 */
	public abstract Double getPercentage(T item);

}
