package com.avaloq.banklet.wealth.adapter.fields;

import java.util.List;

import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ProgressBar;

import com.avaloq.framework.util.ApiSpecificCalls;

public abstract class ColorCodeField<T> extends ProgressBarField<T>{
	protected int width, height;
	
	public ColorCodeField(int aResId, List<T> aAllItems) {
		super(aResId, aAllItems);
	}
	
	@Override
	public int getProgress(T item){
		return 100;
	}
	
	@Override
	public void fillView(View row, int position, T item) {
		/*final ProgressBar pb = (ProgressBar)row.findViewById(mResId);
		//Log.d("Test", "cccccc isVisible "+(pb.getVisibility() == View.VISIBLE));
		if (pb != null){
			pb.post(new Runnable() {
				@Override
				public void run() {
					Log.d("Test", "ccccccc aaaaaaa");
					width = pb.getWidth();
					height = pb.getHeight();
					adjustLayoutParams(pb, true);
					pb.requestLayout();
				}
			});
			if (pb.getVisibility() != View.GONE)
				pb.setVisibility(View.INVISIBLE);
		}*/
		
		super.fillView(row, position, item);

		/*if (pb != null)
			pb.setVisibility(View.INVISIBLE);*/
		
		/*if (pb != null){
			if (height > 0 && width > 0){
				adjustLayoutParams(pb, false);
				pb.setVisibility(View.VISIBLE);
			}
			else {
				Log.d("Test", "cccccc vto");
				ViewTreeObserver vto = pb.getViewTreeObserver();
				vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						Log.d("Test", "cccccc aaaa");
						width = pb.getWidth();
						height = pb.getHeight();
						if (width > 0){
							ApiSpecificCalls.removeOnGlobalLayoutListener(pb.getViewTreeObserver(), this);
							adjustLayoutParams(pb, true);
							Log.d("Test", "ccccc rerere: "+width+" "+height);
						}
					}
				});
			}
		}*/
	}
	
	private void adjustLayoutParams(final ProgressBar pb, boolean withListener){
		if (width > height){
			pb.getLayoutParams().width = height;
		}
		else{
			pb.getLayoutParams().height = width;
		}

		pb.requestLayout();
		
		if (withListener){
			// That's necessary because the LayoutParams don't get updated straight away and there is
			// a nasty flicker otherwise, when it changes the width or height
			ViewTreeObserver vto2 = pb.getViewTreeObserver();
			vto2.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					Log.d("Test", "ccccc in here");
					if (pb.getVisibility() != View.GONE)
						pb.setVisibility(View.VISIBLE);
					ApiSpecificCalls.removeOnGlobalLayoutListener(pb.getViewTreeObserver(), this);
				}
			});
		}
		else
			if (pb.getVisibility() != View.GONE)
				pb.setVisibility(View.VISIBLE);
	}

}
