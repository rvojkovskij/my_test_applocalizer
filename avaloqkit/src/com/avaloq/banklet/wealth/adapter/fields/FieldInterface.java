package com.avaloq.banklet.wealth.adapter.fields;

import android.view.View;

import com.avaloq.banklet.wealth.WealthListTable;

/**
 * The Interface for a field.
 * This is used by the adapters, they will pass implementations of this interface to {@link WealthListTable} 
 * @author Timo Schmid <t.schmid@insign.ch>
 *
 * @param <T>
 */
public interface FieldInterface<T> {

	/**
	 * Abstract method to be implemented by subclasses: Can take views from row and fill them with data.
	 * @param row The row where the value should be filled in.
	 * @param position The position of the item in the list.
	 * @param item The item holding the data.
	 */
	void fillView(View row, int position, T item);

}
