package com.avaloq.banklet.wealth.adapter.fields;

import android.view.View;

public class EmptyField<T> implements FieldInterface<T>{
	protected int mResId;
	
	public EmptyField(int aResId) {
		mResId = aResId;
	}

	@Override
	public void fillView(View row, int position, T item) {
		View view = (View)row.findViewById(mResId);
		if (view != null)
			view.setVisibility(View.INVISIBLE);
	}
}
