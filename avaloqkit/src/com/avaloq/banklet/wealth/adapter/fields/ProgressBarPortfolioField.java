package com.avaloq.banklet.wealth.adapter.fields;

import java.util.List;

import android.app.Activity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.avaloq.framework.R;
import com.avaloq.framework.util.ApiSpecificCalls;

public abstract class ProgressBarPortfolioField<T> extends ProgressBarField<T>{
	
	private Activity mActivity;
	private int pbWidth = 0;
	private int pbHeight = 0;
	
	public ProgressBarPortfolioField(Activity aActivity, int aResId, List<T> aAllItems) {
		super(aResId, aAllItems);
		mActivity = aActivity;
	}
	
	@Override
	protected void initializeMaxVal(List<T> aAllItems){
		for(T item : aAllItems) {
			if (mMaxVal < getProgress(item))
				mMaxVal = getProgress(item);
			if (mMaxVal < getMaxProgress(item))
				mMaxVal = getMaxProgress(item);
		}
	}
	
	/**
	 * Returns the Progress for an item
	 * @param item The data item
	 * @return The progress
	 */
	public abstract int getProgress(T item);
	
	/**
	 * Returns the color for this ProgressBar
	 * @param item The data item
	 * @return The int Color for the progress bar
	 */
	public abstract int getProgressBarColor(T item);
	
	public abstract int getMinProgress(T item);
	public abstract int getMaxProgress(T item);
	public abstract int getOptimalProgress(T item);
	
	public boolean toShowPortfolioVals(T item) {
		return true;
	}
	
	@Override
	public void fillView(final View row, int position, final T item) {
		super.fillView(row, position, item);
		final ProgressBar pb = (ProgressBar)row.findViewById(mResId);
		if (pb != null && toShowPortfolioVals(item)){
			// This is necessary, because the ViewTreeObserver is not always called again when the row gets discarded 
			drawAdditionalItems(row, item);
			ViewTreeObserver vto = pb.getViewTreeObserver();
			vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
			    public void onGlobalLayout() {
					pbWidth = pb.getWidth();
					pbHeight = pb.getHeight();
					if (pbWidth > 0){
						ApiSpecificCalls.removeOnGlobalLayoutListener(pb.getViewTreeObserver(), this);
						drawAdditionalItems(row, item);
					}
				}
			});
		}
		
	}
	
	private void drawAdditionalItems(View row, T item){
		if (pbWidth > 0){
			
			int dividerWidth = 4;
			RelativeLayout rl = (RelativeLayout)row.findViewById(R.id.portfolioProgress);
			
			int optCoord = (int)(pbWidth*getOptimalProgress(item)/mMaxVal);
			int minCoord = (int)(pbWidth*getMinProgress(item)/mMaxVal);
			int maxCoord = (int)(pbWidth*getMaxProgress(item)/mMaxVal);
			
			
			ImageView imgOpt = new ImageView(mActivity);
			imgOpt.setImageResource(R.drawable.wea_status_caret_up);
			LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			layoutParams.setMargins(optCoord, pbHeight+3, 0, 0);
			rl.addView(imgOpt, layoutParams);
			
			ImageView maxMin = new ImageView(mActivity);
			maxMin.setBackgroundResource(R.drawable.wea_status_bar_indicator);
			LayoutParams layoutParamsMaxMin = new LayoutParams(maxCoord - minCoord, pbHeight);
			layoutParamsMaxMin.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			layoutParamsMaxMin.setMargins(minCoord, 0, 0, 0);
			rl.addView(maxMin, layoutParamsMaxMin);
			
			ImageView imgMax = new ImageView(mActivity);
			imgMax.setImageResource(R.drawable.wea_status_bar_indicatorline);
			LayoutParams layoutParamsMax = new LayoutParams(dividerWidth, LayoutParams.WRAP_CONTENT);
			layoutParamsMax.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			layoutParamsMax.setMargins(maxCoord-dividerWidth, pbHeight+3, 0, 0);
			rl.addView(imgMax, layoutParamsMax);
			
			ImageView imgMin = new ImageView(mActivity);
			imgMin.setImageResource(R.drawable.wea_status_bar_indicatorline);
			LayoutParams layoutParamsMin = new LayoutParams(dividerWidth, LayoutParams.WRAP_CONTENT);
			layoutParamsMin.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
			layoutParamsMin.setMargins(minCoord, pbHeight+3, 0, 0);
			rl.addView(imgMin, layoutParamsMin);
		}
	}
}
