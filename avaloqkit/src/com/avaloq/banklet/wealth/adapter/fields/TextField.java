package com.avaloq.banklet.wealth.adapter.fields;

import android.view.View;
import android.widget.TextView;

/**
 * Sets the Text for a Textfield
 * @author Timo Schmid <t.schmid@insign.ch>
 * @param <T> The Type of the data item
 */
public abstract class TextField<T> implements FieldInterface<T> {
	
	private int resId;

	/**
	 * Creates a new instance
	 * @param resId The ID of the TextView resource
	 */
	public TextField(int resId) {
		this.resId = resId;
	}
	
	@Override
	public final void fillView(View row, int position, T item) {
		TextView tv = (TextView)row.findViewById(resId);
		if(tv != null) {
			CharSequence text = getText(item);
			if (text == null || text.equals("") ){
				tv.setVisibility(View.INVISIBLE);
			}
			tv.setText(getText(item));
		}
	}
	
	/**
	 * Returns the Text for an item
	 * @param item The data item
	 * @return The Text for this field
	 */
	public abstract CharSequence getText(T item);

}
