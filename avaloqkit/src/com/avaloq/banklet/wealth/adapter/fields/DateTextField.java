package com.avaloq.banklet.wealth.adapter.fields;

import java.text.DateFormat;
import java.util.Date;

import android.util.Log;

/**
 * Set the value for TextViews by using a date
 * @author Timo Schmid <t.schmid@insign.ch>
 * @param <T> The type for the data item
 */
public abstract class DateTextField<T> extends TextField<T> {
	
	private static final String TAG = DateTextField.class.getSimpleName();

	/**
	 * Creates a new instance
	 * @param resId The ID of the resource in the layout
	 */
	public DateTextField(int resId) {
		super(resId);
	}

	@Override
	public CharSequence getText(T item) {
		Date date = getDate(item);
		if(date != null) {
			Log.d(TAG, date.toString());
			DateFormat df = getDateFormat();
			return df.format(date);
		} else {
			return "";
		}
	}
	
	/**
	 * Returns the Date object
	 * @param item The data item
	 * @return A Date object
	 */
	public abstract Date getDate(T item);
	
	/**
	 * Returns the format to be used in this field
	 * @return The DateFormat to format the date. 
	 */
	public abstract DateFormat getDateFormat();
	
}
