package com.avaloq.banklet.wealth.adapter.fields;

import android.view.View;
import android.widget.ImageView;

import com.avaloq.afs.server.bsp.client.ws.WealthOverviewItemType;
import com.avaloq.framework.R;

public abstract class IconField<T> implements FieldInterface<T> {
	private int resId;
	
	public IconField(int resId){
		this.resId = resId;
	}

	@Override
	public void fillView(View row, int position, T item) {
		ImageView image = (ImageView)row.findViewById(resId);
		if(image != null) {
			if (getType(item) == WealthOverviewItemType.POSITION)
				image.setImageResource(R.drawable.wea_ico_position);
			else if (getType(item) == WealthOverviewItemType.PORTFOLIO)
				image.setImageResource(R.drawable.wea_ico_portfolio);
			else if (getType(item) == WealthOverviewItemType.MONEY_ACCOUNT)
				image.setImageResource(R.drawable.wea_ico_money_account);
			else if (getType(item) == WealthOverviewItemType.PRODUCT)
				image.setImageResource(R.drawable.wea_ico_product);
		}
	}
	
	public abstract WealthOverviewItemType getType(T item);

}
