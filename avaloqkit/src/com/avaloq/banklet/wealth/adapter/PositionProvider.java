package com.avaloq.banklet.wealth.adapter;

import java.util.HashMap;
import java.util.Map;

public class PositionProvider<T> {
	
	Map<T,Integer> positionMap = new HashMap<T,Integer>();
	
	private int position = 0;
	
	int getPosition(T item) {
		if(!positionMap.containsKey(item)) {
			positionMap.put(item, position++);
		}
		return positionMap.get(item);
	}
	
}