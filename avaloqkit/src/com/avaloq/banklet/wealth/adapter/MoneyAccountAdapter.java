package com.avaloq.banklet.wealth.adapter;

import java.io.File;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.afs.server.bsp.client.ws.BookingTO;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.activity.MoneyAccountActivity;
import com.avaloq.banklet.wealth.adapter.fields.CurrencyTextField;
import com.avaloq.banklet.wealth.adapter.fields.DateTextField;
import com.avaloq.banklet.wealth.adapter.fields.DocumentField;
import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;
import com.avaloq.banklet.wealth.adapter.fields.TextField;
import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class MoneyAccountAdapter {
	public static WealthListTable<Date> getAdapter(final MoneyAccountActivity activity, List<BookingTO> positions, final Runnable scrollCallback, final Runnable noMoreContent) {
		
		List<FieldInterface<BookingTO>> bookingFields = new ArrayList<FieldInterface<BookingTO>>();
		bookingFields.add(new DateTextField<BookingTO>(R.id.date) {
			@Override
			public Date getDate(BookingTO item) {
				return item.getBookingDate();
			}
			@Override
			public DateFormat getDateFormat() {
				// TODO We might have to take care of the timezone here, because by default android shows dates in the user's timezone.
				return new SimpleDateFormat("dd\nMMM", activity.getResources().getConfiguration().locale);
			}
		});
		bookingFields.add(new TextField<BookingTO>(R.id.description) {
			@Override
			public CharSequence getText(BookingTO item) {
				return item.getNarrative();
			}
		});
		bookingFields.add(new CurrencyTextField<BookingTO>(R.id.debit) {
			@Override
			public BigDecimal getAmount(BookingTO item) {
				return item.getAmount();
			}
			@Override
			public Long getCurrencyId(BookingTO item) {
				return 0l;
			}
			@Override
			public boolean toShowValue(BookingTO item){
				if (item.getAmount().doubleValue() < 0)
					return false;
				else
					return true;
			}
		});
		bookingFields.add(new CurrencyTextField<BookingTO>(R.id.credit) {
			@Override
			public BigDecimal getAmount(BookingTO item) {
				return new BigDecimal(0).subtract(item.getAmount());
			}
			@Override
			public Long getCurrencyId(BookingTO item) {
				return 0l;
			}
			@Override
			public boolean toShowValue(BookingTO item){
				if (item.getAmount().doubleValue() > 0)
					return false;
				else
					return true;
			}
		});
		bookingFields.add(new CurrencyTextField<BookingTO>(R.id.saldo) {
			@Override
			public BigDecimal getAmount(BookingTO item) {
				return item.getBalance();
			}
			@Override
			public Long getCurrencyId(BookingTO item) {
				return 0l;
			}
		});
		
		bookingFields.add(new DocumentField<BookingTO>(R.id.document) {
			@Override
			public Long getDocumentId(BookingTO item) {
				return item.getAdviceDocumentId();
			}

			@Override
			public Activity getActivity() {
				return activity;
			}

			@Override
			public void displayPdf(Intent intent, int result_code) {
				activity.startActivityForResult(intent, result_code);
			}

			@Override
			public void onPdfFileSet(File file) {
				activity.setPdfFile(file);
			}
		});
		
		int currentMonth = -1;
		int currentYear = -1;
		Date currentDate = null;
		List<WealthListTable<BookingTO>> monthBookingTables = new ArrayList<WealthListTable<BookingTO>>();
		List<BookingTO> monthBookings = new ArrayList<BookingTO>();
		List<Date> headerItems = new ArrayList<Date>();
		List<List<BookingTO>> monthBookingsList = new ArrayList<List<BookingTO>>();
		
		List<FieldInterface<Date>> headerFields = new ArrayList<FieldInterface<Date>>();
		headerFields.add(new DateTextField<Date>(R.id.date) {
			@Override
			public Date getDate(Date item) {
				return item;
			}
			@Override
			public DateFormat getDateFormat() {
				return new SimpleDateFormat("MMMM yyyy", activity.getResources().getConfiguration().locale);
			}
		});
		
		if (positions.size() == 0){
			return new WealthListTable<Date>(activity, R.layout.wea_money_account_header, headerFields, headerItems, monthBookingTables);
		}
		
		for(BookingTO booking: positions){
			Calendar bookingDate = DateUtil.getCalendar(booking.getBookingDate());
			if (currentMonth != bookingDate.get(Calendar.MONTH) || currentYear != bookingDate.get(Calendar.YEAR)){
				if (currentMonth != -1) {
					monthBookingsList.add(monthBookings);
					WealthListTable<BookingTO> bookingTable = new WealthListTable<BookingTO>(activity, R.layout.wea_money_account_row, bookingFields, monthBookings, null);
					monthBookingTables.add(bookingTable);
					headerItems.add(currentDate);
				}
				currentMonth = bookingDate.get(Calendar.MONTH);
				currentYear = bookingDate.get(Calendar.YEAR);
				currentDate = booking.getBookingDate();
				monthBookings = new ArrayList<BookingTO>();
			}
			monthBookings.add(booking);
		}
		
		// The last month doesn't get added because there is no element after it.
		// In this case, it should be added manually.
		headerItems.add(currentDate);
		WealthListTable<BookingTO> bookingTable = new WealthListTable<BookingTO>(activity, R.layout.wea_money_account_row, bookingFields, monthBookings, null);
		monthBookingTables.add(bookingTable);
		
		return new WealthListTable<Date>(activity, R.layout.wea_money_account_header, headerFields, headerItems, monthBookingTables){
			@Override
			public void onScrollToBottom(int position, View convertView, ViewGroup parent) {
				if (scrollCallback != null)
					scrollCallback.run();
			}
			
			@Override
			public void onEndScrollToBottom() {
				if (noMoreContent != null)
					noMoreContent.run();
			}
		};
	}
}
