package com.avaloq.banklet.wealth.adapter;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import com.avaloq.afs.server.bsp.client.ws.WealthOverviewAllocationItemTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewItemTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewItemType;
import com.avaloq.banklet.trading.TradingSellDetailActivity;
import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.banklet.wealth.WealthBanklet;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.WealthListTableConfigurable;
import com.avaloq.banklet.wealth.activity.MoneyAccountActivity;
import com.avaloq.banklet.wealth.activity.PortfolioOverviewActivity;
import com.avaloq.banklet.wealth.activity.ProductOverviewActivity;
import com.avaloq.banklet.wealth.activity.WealthReportFragment;
import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.banklet.wealth.adapter.fields.ColorCodeField;
import com.avaloq.banklet.wealth.adapter.fields.CurrencyTextField;
import com.avaloq.banklet.wealth.adapter.fields.EmptyField;
import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;
import com.avaloq.banklet.wealth.adapter.fields.IconField;
import com.avaloq.banklet.wealth.adapter.fields.PercentageTextField;
import com.avaloq.banklet.wealth.adapter.fields.ProgressBarField;
import com.avaloq.banklet.wealth.adapter.fields.TextField;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
// TODO REMOVE ALL THE DIRECT REFERENCES TO THE TRADING BANKLET

/**
 * @author Ventsislav Zahariev <v.zahariev@insign.ch>
 * 
 * This adapter is used to instantiate an WealthStandardLayout, used at the 
 * current starting activity of the Wealth banklet (overview of all asset types).
 */

public class WealthStartAdapter {

private static final String TAG = WealthStartAdapter.class.getSimpleName();
	
	public static WealthListTable<WealthOverviewAllocationItemTO> getAdapter(final WealthReportFragment aFragment, List<WealthOverviewAllocationItemTO> allocations, Integer aPosition) {
		
		final PositionProvider<WealthOverviewItemTO> positionProvider = new PositionProvider<WealthOverviewItemTO>();
		
		List<WealthListTable<WealthOverviewItemTO>> wealthOverviewItemTables = new ArrayList<WealthListTable<WealthOverviewItemTO>>();
		
		List<WealthOverviewItemTO> allOverviewItems = new ArrayList<WealthOverviewItemTO>();
		for(WealthOverviewAllocationItemTO allocation: allocations) {
			allOverviewItems.addAll(allocation.getWealthItems());
		}
		
		int index = -1;
		for (WealthOverviewAllocationItemTO allocation: allocations) {
			index++;
			
			ArrayList<FieldInterface<WealthOverviewItemTO>> wealthOverviewItemFields = new ArrayList<FieldInterface<WealthOverviewItemTO>>();
			wealthOverviewItemFields.add(new TextField<WealthOverviewItemTO>(R.id.name) {
				@Override
				public CharSequence getText(WealthOverviewItemTO item) {
					return item.getName();
				}
			});
			wealthOverviewItemFields.add(new TextField<WealthOverviewItemTO>(R.id.description) {
				@Override
				public CharSequence getText(WealthOverviewItemTO item) {
					return item.getDescription();
				}
			});
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.amount) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getValueInValuationCurrency();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getValuationCurrencyId();
				}
			});
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.valuationAmount) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getValue();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getCurrencyId();
				}
				@Override
				public boolean toShowValue(WealthOverviewItemTO item){
					return !item.getCurrencyId().equals(item.getValuationCurrencyId());
				}
			});
			
			
			
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.currentAmount) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getValueExclAccrualsInValuationCurrency();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getValuationCurrencyId();
				}
			});
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.valuationCurrentAmount) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getValueExclAccruals();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getCurrencyId();
				}
				@Override
				public boolean toShowValue(WealthOverviewItemTO item){
					return !item.getCurrencyId().equals(item.getValuationCurrencyId());
				}
			});
			
			wealthOverviewItemFields.add(new IconField<WealthOverviewItemTO>(R.id.icon) {
				@Override
				public WealthOverviewItemType getType(WealthOverviewItemTO item) {
					return item.getType();
				}
			});
			
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.interest) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getAccruedInterestInValuationCurrency();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getValuationCurrencyId();
				}
			});
			wealthOverviewItemFields.add(new CurrencyTextField<WealthOverviewItemTO>(R.id.valuationInterest) {
				@Override
				public BigDecimal getAmount(WealthOverviewItemTO item) {
					return item.getAccruedInterest();
				}
				@Override
				public Long getCurrencyId(WealthOverviewItemTO item) {
					return item.getCurrencyId();
				}
				@Override
				public boolean toShowValue(WealthOverviewItemTO item){
					return !item.getCurrencyId().equals(item.getValuationCurrencyId());
				}
			});
			
			
			
			
			
			
			wealthOverviewItemFields.add(new PercentageTextField<WealthOverviewItemTO>(R.id.weight) {
				@Override
				public Double getPercentage(WealthOverviewItemTO item) {
					return item.getRatio().doubleValue() * 100;
				}
			});
			wealthOverviewItemFields.add(new ProgressBarField<WealthOverviewItemTO>(R.id.progressBar, allOverviewItems) {
				@Override
				public int getProgress(WealthOverviewItemTO item) {
					return Double.valueOf(item.getRatio().doubleValue() * 100).intValue();
				}
				@Override
				public int getProgressBarColor(WealthOverviewItemTO item) {
					return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), positionProvider.getPosition(item)));
				}
			});
			if (aPosition != null && aPosition == index){
				wealthOverviewItemFields.add(new ColorCodeField<WealthOverviewItemTO>(R.id.colorCode, allOverviewItems) {
					@Override
					public int getProgressBarColor(WealthOverviewItemTO item) {
						return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), positionProvider.getPosition(item)));
					}
				});
			}
			else {
				wealthOverviewItemFields.add(new EmptyField<WealthOverviewItemTO>(R.id.colorCode));
			}
			
			//WealthListTable<WealthOverviewItemTO> wealthListElement = new WealthListTable<WealthOverviewItemTO>(aFragment.getActivity(), R.layout.wea_report_row, wealthOverviewItemFields, allocation.getWealthItems(), null) {
			WealthListTableConfigurable<WealthOverviewItemTO> wealthListElement = new WealthListTableConfigurable<WealthOverviewItemTO>(aFragment.getActivity(), R.layout.wea_conf_overview_with_icon_row, "wea_conf_overview_with_icon", wealthOverviewItemFields, allocation.getWealthItems(), null) {
				@Override
				public void onClick(int position, WealthOverviewItemTO item) {
					Log.d(TAG, "onClick " + item.getType());
					if (item.getType() == WealthOverviewItemType.PRODUCT){
						Intent intent = new Intent(activity, ProductOverviewActivity.class);
						intent.putExtra(ProductOverviewActivity.EXTRA_PRODUCT_ID, item.getProductId());
						intent.putExtra(ProductOverviewActivity.EXTRA_CURRENCY_ID, item.getCurrencyId());
						intent.putExtra(ProductOverviewActivity.EXTRA_PRODUCT_NAME, item.getName());
						intent.putExtra(ProductOverviewActivity.EXTRA_PRODUCT_TOTAL_VALUE, item.getValue().toString());
						activity.startActivity(intent);
					} else if (item.getType() == WealthOverviewItemType.MONEY_ACCOUNT){
						Intent intent = new Intent(activity, MoneyAccountActivity.class);
						intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_ID, item.getId());
						intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_NAME, item.getName());
						intent.putExtra(MoneyAccountActivity.EXTRA_CURRENCY_ID, item.getCurrencyId());
						intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_BALANCE, item.getValue());
						intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_INTEREST, item.getAccruedInterest());
						intent.putExtra(MoneyAccountActivity.EXTRA_ACCOUNT_CURRENT, item.getValueExclAccruals());
						activity.startActivity(intent);
					} else if (item.getType() == WealthOverviewItemType.PORTFOLIO){
						Intent intent = new Intent(activity, PortfolioOverviewActivity.class);
						intent.putExtra(PortfolioOverviewActivity.EXTRA_PRODUCT_ID, item.getProductId());
						intent.putExtra(PortfolioOverviewActivity.EXTRA_PORTFOLIO_ID, item.getPortfolioId().getId());
						intent.putExtra(PortfolioOverviewActivity.EXTRA_PORTFOLIO_NAME, item.getName());
						intent.putExtra(PortfolioOverviewActivity.EXTRA_TYPE, item.getPortfolioId().getType().toString());
						activity.startActivity(intent);
					} else if (item.getType() == WealthOverviewItemType.POSITION){
						Intent intent = new Intent(activity, TradingSellDetailActivity.class);

						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_ID, item.getProductId());
						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_NAME, item.getDescription());
						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_TYPE, "");
						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_SUB_TYPE, "");
						intent.putExtra(TradingSellDetailActivity.EXTRA_BANKING_POSITION_ID, item.getId());
						intent.putExtra(TradingSellDetailActivity.EXTRA_IS_BUY, ButtonSellBuyStatus.TRANSACTIONS_ONLY);
						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_CURRENCY_ID, item.getCurrencyId());
						intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_NAME, "");


						activity.startActivity(intent);
					}
				}
			};
			
			wealthListElement.setChartDataProvider(new ChartDataProvider<WealthOverviewItemTO>() {
				public boolean showLabel(WealthOverviewItemTO item){
					int min = aFragment.getActivity().getResources().getInteger(R.integer.avq_min_chart_ration_to_show_label);
					return getRatio(item).doubleValue() > min;
				}
				
				@Override
				public BigDecimal getRatio(WealthOverviewItemTO item) {
					return item.getRatio();
				}
				
				@Override
				public String getLabel(WealthOverviewItemTO item) {
					String label = "";
					if (item.getDescription().equals(""))
						label = item.getName();
					else
						label = item.getDescription();
					
					/*if (showLabel(item))
						return label;
					else
						return "";*/
					return label;
				}
				
				@Override
				public String getSubLabel(WealthOverviewItemTO item){
					/*if (showLabel(item))
						return new DecimalFormat("#.##").format(getRatio(item)) + "%";
					else
						return "";*/
					return new DecimalFormat("#.##").format(getRatio(item).movePointRight(2)) + "%";
				}
				
				@Override
				public int getColor(WealthOverviewItemTO item) {
					return positionProvider.getPosition(item);
				}
			});
			wealthOverviewItemTables.add(wealthListElement);
		}

		final PositionProvider<WealthOverviewAllocationItemTO> wealthOverviewAllocationItemPositionProvider = new PositionProvider<WealthOverviewAllocationItemTO>();
		
		List<FieldInterface<WealthOverviewAllocationItemTO>> wealthOverviewAllocationFields = new ArrayList<FieldInterface<WealthOverviewAllocationItemTO>>();
		wealthOverviewAllocationFields.add(new TextField<WealthOverviewAllocationItemTO>(R.id.name) {
			@Override
			public CharSequence getText(WealthOverviewAllocationItemTO item) {
				return item.getName();
			}
		});
		wealthOverviewAllocationFields.add(new CurrencyTextField<WealthOverviewAllocationItemTO>(R.id.amount) {
			@Override
			public BigDecimal getAmount(WealthOverviewAllocationItemTO item) {
				return item.getTotalValueInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(WealthOverviewAllocationItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		
		
		
		wealthOverviewAllocationFields.add(new CurrencyTextField<WealthOverviewAllocationItemTO>(R.id.currentAmount) {
			@Override
			public BigDecimal getAmount(WealthOverviewAllocationItemTO item) {
				return item.getTotalValueExclAccrualsInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(WealthOverviewAllocationItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		wealthOverviewAllocationFields.add(new CurrencyTextField<WealthOverviewAllocationItemTO>(R.id.interest) {
			@Override
			public BigDecimal getAmount(WealthOverviewAllocationItemTO item) {
				return item.getTotalAccruedInterestInValuationCurrency();
			}
			@Override
			public Long getCurrencyId(WealthOverviewAllocationItemTO item) {
				return item.getValuationCurrencyId();
			}
		});
		
		
		
		
		
		wealthOverviewAllocationFields.add(new PercentageTextField<WealthOverviewAllocationItemTO>(R.id.weight) {
			@Override
			public Double getPercentage(WealthOverviewAllocationItemTO item) {
				return item.getRatio().doubleValue() * 100;
			}
		});
		/*wealthOverviewAllocationFields.add(new ProgressBarField<WealthOverviewAllocationItemTO>(R.id.progressBar, allocations) {
			@Override
			public int getProgress(WealthOverviewAllocationItemTO item) {
				return Double.valueOf(item.getRatio().doubleValue() * 100).intValue();
			}
			@Override
			public int getProgressBarColor(WealthOverviewAllocationItemTO item) {
				return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), wealthOverviewAllocationItemPositionProvider.getPosition(item)));
			}
		});*/
		if (aPosition == null){
			wealthOverviewAllocationFields.add(new ColorCodeField<WealthOverviewAllocationItemTO>(R.id.colorCode, allocations) {
				@Override
				public int getProgressBarColor(WealthOverviewAllocationItemTO item) {
					return Color.parseColor(WealthBanklet.getProgressColor(aFragment.getActivity(), wealthOverviewAllocationItemPositionProvider.getPosition(item)));
				}
			});
		}
		else {
			wealthOverviewAllocationFields.add(new EmptyField<WealthOverviewAllocationItemTO>(R.id.colorCode));
		}
		
		//WealthListTable<WealthOverviewAllocationItemTO> wealthListTable = new WealthListTable<WealthOverviewAllocationItemTO>(aFragment.getActivity(), R.layout.wea_report_header, wealthOverviewAllocationFields, allocations, wealthOverviewItemTables) {
		WealthListTableConfigurable<WealthOverviewAllocationItemTO> wealthListTable = new WealthListTableConfigurable<WealthOverviewAllocationItemTO>(aFragment.getActivity(), R.layout.wea_conf_overview_header, "wea_conf_overview", wealthOverviewAllocationFields, allocations, wealthOverviewItemTables) {
			@Override
			public void onClick(int position, WealthOverviewAllocationItemTO item) {
				if (AvaloqApplication.getInstance().getConfiguration().showWealthCharts()){
					Log.d(TAG, "Clicked on allocation "+position);
					if (aFragment.getAdapterPositions() != null && aFragment.getAdapterPositions() == position)
						aFragment.setAdapterPositions(null);
					else
						aFragment.setAdapterPositions(position);
				}
			}
		};
		wealthListTable.setChartDataProvider(new ChartDataProvider<WealthOverviewAllocationItemTO>() {
			
			public boolean showLabel(WealthOverviewAllocationItemTO item){
				int min = aFragment.getActivity().getResources().getInteger(R.integer.avq_min_chart_ration_to_show_label);
				return getRatio(item).doubleValue() > min;
			}
			
			@Override
			public BigDecimal getRatio(WealthOverviewAllocationItemTO item) {
				return item.getRatio();
			}
			
			@Override
			public String getLabel(WealthOverviewAllocationItemTO item) {
				String label = "";
				if (item.getShortName() == null || item.getShortName().equals(""))
					label = item.getName();
				else
					label = item.getShortName();
				
				/*if (showLabel(item))
					return label;
				else
					return "";*/
				return label;
			}
			
			@Override
			public String getSubLabel(WealthOverviewAllocationItemTO item){
				/*if (showLabel(item))
					return new DecimalFormat("#.##").format(getRatio(item)) + "%";
				else
					return "";*/
				return new DecimalFormat("#.##").format(getRatio(item).movePointRight(2)) + "%";
			}
			
			@Override
			public int getColor(WealthOverviewAllocationItemTO item) {
				return wealthOverviewAllocationItemPositionProvider.getPosition(item);
			}
		});
		
		return wealthListTable;
	}
}
