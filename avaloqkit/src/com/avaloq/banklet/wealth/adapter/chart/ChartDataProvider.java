package com.avaloq.banklet.wealth.adapter.chart;

import java.math.BigDecimal;

public interface ChartDataProvider<T> {
	
	public abstract String getLabel(T item);
	
	public abstract String getSubLabel(T item);
	
	public abstract BigDecimal getRatio(T item);
	
	public abstract int getColor(T item);

}
