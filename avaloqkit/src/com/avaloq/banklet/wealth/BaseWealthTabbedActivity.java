package com.avaloq.banklet.wealth;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;
import com.avaloq.framework.util.InlineMessageUtil;

/**
 * 
 * This class is the top level abstraction for all Wealth tabbed activities. This class handles
 *  the models, the WealthStandardLayout, the adapters and the observers for the developer.
 *
 * Usage: The Activities should extend this class and implement the abstract methods.
 * 
 * @author zahariev
 *  
 */

public abstract class BaseWealthTabbedActivity extends TabbedBankletActivity implements Observer {
	
	private static final String TAG = BaseWealthTabbedActivity.class.getSimpleName();
	
	/**
	 * created by the developer in the overridden createModel(). As opposed to
	 * BaseWealthActivity and BaseWealthFragment, BaseWealthTabbedActivity serves only as a container
	 * for tabs, so it doesn't need adapter or WealthStandardLayout elements.
	 */
	protected Observable model;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	/**
	 * The tabs in the Wealth Banklet are generated after web-service calls. This method initiates
	 * the request and binds an observer to it, so that once the request is processed, the tabs can be
	 * created - see setupTabs().
	 */
	@Override
	public void prepareNavigationItems() {
		this.setupExtras();
		Log.d(TAG, "prepareNaviagationItems");
		model = this.createModel();
		model.deleteObservers();
		model.addObserver(this);
		try{
			Log.d(TAG, "observers: "+ model.countObservers());
			this.loadModelData();
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	/**
	 * Frees the observer, so that it doesn't get called again, in case the web-service request gets
	 * executed again. Apparently this is necessary even though the activity has been destroyed.
	 */
	protected void onDestroy (){
		model.deleteObserver(this);
		super.onDestroy();
	}
	
	/**
	 * This method is NOT supposed to be overridden. You probably want to use setupTabs() instead.
	 */
	@Override
	public final void update(Observable arg0, Object arg1) {
		if (arg1 == null){
			onCommunicationBreakdown();
		}
		else {
			this.update();
		}
	}
	
	/**
	 * This method is NOT supposed to be overridden. You probably want to use setupTabs() instead.
	 */
	public final void update() {
		onBeforeUpdate();
		showNavigationItems(setupTabs());
		onAfterUpdate();
	}
	
	public void onBeforeUpdate(){}
	
	public void onAfterUpdate(){}
	
	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of parameters, the model
	 * classes may require different initialization routines. The activities should override this
	 * method, initialize the required model and return it, so that the BaseWealthFragment can
	 * handle it further for the developer.
	 * 
	 */
	public abstract Observable createModel();
	
	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of parameters, the model
	 * classes may require different initialization routines. The Wealth activities should override this
	 * method, and use the 
	 */
	public abstract void loadModelData() throws IOException;
	
	public abstract void setupExtras();
	
	/**
	 * This method gets called once the web-service request is processed and all required information
	 * is available. Override it to create the tabs - as specified by TabbedBankletActivity.
	 */
	public abstract FragmentNavigationItem[] setupTabs();
	
	/**
	 * This method gets called if the web service request from the model fails
	 */
	public void onCommunicationBreakdown(){
		getViewPager().removeAllViews();
		findViewById(R.id.avq_tabbed_activity_loading).setVisibility(View.GONE);
		findViewById(R.id.avq_tabbed_activity_progress_bar).setVisibility(View.GONE);
		ViewGroup vg = (ViewGroup)findViewById(android.R.id.content);
		InlineMessageUtil.showNetworkError(vg);
	}
}
