package com.avaloq.banklet.wealth;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.framework.chart.BarChart;
import com.avaloq.framework.chart.Chart;
import com.avaloq.framework.chart.PieChart;

/**
 * 
 * @author zahariev
 * This is a temporary adapter for all Charts displayed in the Wealth Activities. Generally there are
 * two charts for each activity - pie chart and bar chart.
 * 
 *  TODO: Currently only the pie chart is implemented the rest of the charts are just dummies to test
 *  the view pager.
 */
class ChartPagerAdapter<T> extends PagerAdapter{
	
	private Activity context;
	private WealthListTable<T> mWealthListTable;
	private ChartDataProvider<T> mDataProvider;
	private int width, height;
	
	List<Double> ratios = new ArrayList<Double>();
	List<String> labels = new ArrayList<String>();
	List<String> subLabels = new ArrayList<String>();
	List<String> colors = new ArrayList<String>();
	
	private boolean hasNegativeValues = false;

	public ChartPagerAdapter(Activity activity, WealthListTable<T> aWealthListTable, ChartDataProvider<T> aDataProvider, int width, int height){
		context = activity;
		this.mWealthListTable = aWealthListTable;
		this.mDataProvider = aDataProvider;
		this.width = width;
		this.height = height;
		initializeElements();
	}
	@Override
	public int getCount() {
		if (hasNegativeValues)
			return 1;
		else
			return 2;
	}
	
	public void initializeElements(){
		int multiplier = 2+checkForZeroes();
		for (int i = 0;i < mWealthListTable.getElementCount();i++) {
			//if ((mDataProvider.getRatio(mWealthListTable.getElement(i)).movePointRight(multiplier)).doubleValue() >= 0.0){
				ratios.add(mDataProvider.getRatio(mWealthListTable.getElement(i)).movePointRight(multiplier).doubleValue());
				labels.add(mDataProvider.getLabel(mWealthListTable.getElement(i)));
				subLabels.add(mDataProvider.getSubLabel(mWealthListTable.getElement(i)));
				colors.add(WealthBanklet.getProgressColor(context, mDataProvider.getColor(mWealthListTable.getElement(i))));
			//}
				
			if ((mDataProvider.getRatio(mWealthListTable.getElement(i)).movePointRight(multiplier)).doubleValue() < 0.0){
				hasNegativeValues = true;
			}
		}
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return (view == (View)object);
	}
	
	@Override
	public void destroyItem (ViewGroup container, int position, Object object){
		container.removeView((View)object);
	}
	
	@Override
	public Object instantiateItem (ViewGroup container, int position){		
		Chart chart = null;
		
		if (position==0 && !hasNegativeValues){
			chart = new PieChart(context, ratios, labels, subLabels, colors, width, height);
		} else {
			chart = new BarChart(context, ratios, labels, subLabels, colors, width, height);
		}
		
		((ViewPager)container).addView(chart.getView());
		return chart.getView();
	}
	
	public int checkForZeroes(){
		List<BigDecimal> list = new ArrayList<BigDecimal>();
		List<BigDecimal> tmp = new ArrayList<BigDecimal>();
		for (int i = 0;i < mWealthListTable.getElementCount();i++) {
			BigDecimal decimal = mDataProvider.getRatio(mWealthListTable.getElement(i));
			list.add(decimal);
		}
		
		int counter = 0;
		
		while (true){
			for (BigDecimal check: list){
				if (check.doubleValue() > 1)
					return counter;
				tmp.add(check.movePointRight(1));
			}
			
			list = new ArrayList<BigDecimal>(tmp);
			tmp = new ArrayList<BigDecimal>();
			
			counter++;
			if (counter > 15)
				break;
		}
		return counter;
	}
	
}