package com.avaloq.banklet.wealth.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.avaloq.afs.aggregation.to.wealth.PortfolioDetailResult;
import com.avaloq.afs.server.bsp.client.ws.AssetAllocationDeepReportTO;
import com.avaloq.afs.server.bsp.client.ws.PortfolioAllocationDeepItemTO;
import com.avaloq.afs.server.bsp.client.ws.PortfolioIdTO;
import com.avaloq.afs.server.bsp.client.ws.PortfolioPositionTO;
import com.avaloq.afs.server.bsp.client.ws.PortfolioType;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.wealth.PortfolioDetailRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;
import com.avaloq.framework.util.CurrencyUtil;

public class PortfolioOverviewModel extends java.util.Observable {
	
	private static final String TAG = PortfolioOverviewModel.class.getSimpleName();
	
	private static PortfolioOverviewModel instance = null;
	
	PortfolioDetailResult result;

	public static PortfolioOverviewModel getInstance() {
		if(instance == null) {
			instance = new PortfolioOverviewModel();
		}
		return instance;
	}
	
	public void loadData(long portfolioId, PortfolioType type, long productId) throws IOException{
		PortfolioIdTO port = new PortfolioIdTO();
		port.setId(portfolioId);
		port.setType(type);
		PortfolioDetailRequest request = WealthService.getProductPortfolioDetails(port, productId, new RequestStateEvent<PortfolioDetailRequest>(){
			@Override
			public void onRequestCompleted(PortfolioDetailRequest aRequest) {
				result = aRequest.getResponse().getData();
				PortfolioOverviewModel.this.setChanged();
				PortfolioOverviewModel.this.notifyObservers(result);
			}
			
			@Override
			public void onRequestFailed(PortfolioDetailRequest aRequest) {
				PortfolioOverviewModel.this.setChanged();
				PortfolioOverviewModel.this.notifyObservers(null);
			}
		});
		request.initiateServerRequest();
	}
	
	public List<AssetAllocationDeepReportTO> getAllocations(List<Integer> allocIds, List<Integer>itemIds){
		
		if (allocIds.size() != itemIds.size())
			throw new IllegalArgumentException("The alloc-list should be at least as big as the item-list");
		
		List<AssetAllocationDeepReportTO> current = result.getInitialAllocationReportList();
		if (allocIds.size() == 0)
			return current;
		
		int index = 0;
		for (Integer allocId: allocIds){
			current = current.get(allocId).getAllocationItemList().get(itemIds.get(index)).getSubReportList();
			index++;
		}
		if (current == null)
			return new ArrayList<AssetAllocationDeepReportTO>();
		else
			return current;
	}
	
	public List<AssetAllocationDeepReportTO> getAllocations(){
		return getAllocations(new ArrayList<Integer>(), new ArrayList<Integer>());
	}
	
	public List<PortfolioPositionTO> getStockPositions(){
		return result.getPositionList();
	}
	
	public List<PortfolioPositionTO> getStockPositions(List<Integer> allocIds, List<Integer>itemIds){
		List<Long> ids = getAllocationItem(allocIds, itemIds).getPositionIdList();
		List<PortfolioPositionTO> stocks = new ArrayList<PortfolioPositionTO>();
		for (PortfolioPositionTO position: getStockPositions()){
			if (ids.contains(position.getId()) ){
				stocks.add(position);
			}
		}
		return stocks;
	}
	
	public List<PortfolioAllocationDeepItemTO> getAllocationItems(List<Integer> allocIds, List<Integer> itemIds){
		if (allocIds.size() != itemIds.size()+1)
			throw new IllegalArgumentException("The alloc-list should contain exactly one element more than the item-list");
		if (allocIds.size() == 0)
			throw new IllegalArgumentException("The alloc-list should contain at least one element.");
		List<PortfolioAllocationDeepItemTO> current = getAllocations().get(allocIds.get(0)).getAllocationItemList();
		
		int index = 0;
		for (Integer itemId: itemIds){
			current = current.get(itemId).getSubReportList().get(allocIds.get(index+1)).getAllocationItemList();
		}
		
		return current;
	}
	
	public AssetAllocationDeepReportTO getAllocation(List<Integer> allocIds, List<Integer> itemIds){
		// if it is the top level, return null and take the portfolio name
		if (itemIds == null || itemIds.size() == 0)
			return null;
		if (allocIds.size() != itemIds.size()+1)
			throw new IllegalArgumentException("The alloc-list should contain exactly one element more than the item-list");
		if (allocIds.size() == 0)
			throw new IllegalArgumentException("The alloc-list should contain at least one element.");
		AssetAllocationDeepReportTO current = getAllocations().get(allocIds.get(0));
		
		int index = 0;
		for (Integer itemId: itemIds){
			current = current.getAllocationItemList().get(itemId).getSubReportList().get(allocIds.get(index+1));
		}
		
		return current;
	}
	
	public PortfolioAllocationDeepItemTO getAllocationItem(List<Integer> allocIds, List<Integer> itemIds){
		if (allocIds.size() != itemIds.size())
			throw new IllegalArgumentException("The alloc-list should be exactly as big as the item-list");
		if (allocIds.size() == 0)
			throw new IllegalArgumentException("The alloc-list should contain at least one element.");
		PortfolioAllocationDeepItemTO current =
				getAllocations().get(allocIds.get(0)).getAllocationItemList().get(itemIds.get(0));
		for (int i=1; i<allocIds.size(); i++){
			current = current.getSubReportList().get(allocIds.get(i)).getAllocationItemList().get(itemIds.get(i));
		}
		
		return current;
	}
	
	public String getFormattedTotalValue(){
		return CurrencyUtil.formatMoney(result.getTotalValueInValuationCurrency(), this.result.getValuationCurrencyId());
	}
	
	public String getFormattedTotalValue(List<Integer> allocIds, List<Integer> itemIds){
		if (itemIds == null || itemIds.size() == 0){
			return getFormattedTotalValue();
		}
		
		if (allocIds.size() != itemIds.size()+1 && allocIds.size() != itemIds.size())
			throw new IllegalArgumentException("The alloc-list should be as big as the item-list or contain one element more");
		
		List<Integer> tmp = new ArrayList<Integer>(allocIds);
		
		if (itemIds.size()+1 == tmp.size())
			tmp.remove(tmp.size()-1);
		
		Log.d(TAG, "Test: "+tmp.toString());
		Log.d(TAG, "Test: "+itemIds.toString());
		
		PortfolioAllocationDeepItemTO item = getAllocationItem(tmp, itemIds);
		return CurrencyUtil.formatMoney(item.getTotalValueInValuationCurrency(), item.getValuationCurrencyId());
	}

}
