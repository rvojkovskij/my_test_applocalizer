package com.avaloq.banklet.wealth.model;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

import com.avaloq.afs.aggregation.to.wealth.WealthOverviewResult;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewAllocationItemTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewAllocationReportTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewGroupTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewItemTO;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.wealth.WealthOverviewRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;
import com.avaloq.framework.util.CurrencyUtil;

/**
 * 
 * @author Ventsislav Zahariev <v.zahariev@insign.ch>
 * 
 * Model class for the general overview of all asset types. Used in the
 * current starting activity for the Wealth banklet.
 */

public class WealthOverviewModel extends java.util.Observable {
	
	private static final String TAG = WealthOverviewModel.class.getSimpleName();
	
	private static WealthOverviewModel instance = null;
	
	private WealthOverviewResult result;

	private Map<String, String> groupings = new HashMap<String,String>();
	
	public static WealthOverviewModel getInstance() {
		if(instance == null) {
			instance = new WealthOverviewModel();
		}
		return instance;
	}
	
	public void loadData() throws IOException {
		Log.d(TAG, "loadData");
		WealthOverviewRequest wealthOverviewRequest = WealthService.getWealthOverview(new RequestStateEvent<WealthOverviewRequest>() {
			@Override
			public void onRequestCompleted(WealthOverviewRequest request) {
				result = request.getResponse().getData();
				for(WealthOverviewAllocationReportTO report : result.getWealthOverviewAllocationReportList()) {
					WealthOverviewGroupTO firstGroup = report.getWealthOverviewGroupList().get(0);
					groupings.put(report.getName(), firstGroup.getName());
				}
				WealthOverviewModel.this.setChanged();
				Log.d(TAG, "notifyObservers (in loadData)");
				WealthOverviewModel.this.notifyObservers(result);
			}
			
			@Override
			public void onRequestFailed(WealthOverviewRequest aRequest) {
				WealthOverviewModel.this.setChanged();
				WealthOverviewModel.this.notifyObservers(null);
			}
		});
		wealthOverviewRequest.initiateServerRequest();
	}
	
	public void onScreenRotation() {
		this.setChanged();
		Log.d(TAG, "notifyObservers (in onScreenRotation)");
		this.notifyObservers(null);
	}
	
	public List<WealthOverviewAllocationReportTO> getReports() {
		// FIXME: Can throw NPE if result is null (which can be caused by other problems)
		return result.getWealthOverviewAllocationReportList();
	}
	
	public List<WealthOverviewGroupTO> getGroups(String reportName) {
		for (WealthOverviewAllocationReportTO item: getReports()){
			if (item.getName().equals(reportName)){
				return item.getWealthOverviewGroupList();
			}
		}
		return null;
	}
	
	public List<WealthOverviewAllocationItemTO> getAllocations(String reportName, String groupName) {
		for(WealthOverviewGroupTO group: getGroups(reportName)){
			if (group.getName().equals(groupName)){
				return group.getAllocationItemList();
			}
		}
		return null;
	}
	
	public List<WealthOverviewItemTO> getItems(String reportName, String groupName, String allocationName) {
		for (WealthOverviewAllocationItemTO overview: getAllocations(reportName, groupName)){
			if (overview.getName().equals(allocationName)){
				return overview.getWealthItems();
			}
		}
		return null;
	}
	
	public void setGroupings(String reportName, String grouping) {
		this.groupings.put(reportName, grouping);
		//setChanged();
		//notifyObservers();
	}
	
	public Map<String,String> getGroupings() {
		return this.groupings;
	}
	
	public String getFormattedTotalValue(String reportName){
		for (WealthOverviewAllocationReportTO report: this.getReports()){
			if (report.getName().equals(reportName)){
				return CurrencyUtil.formatMoney(report.getTotalValueInValuationCurrency(), this.result.getValuationCurrencyId());
			}
		}
		
		// TODO: As already mentioned a couple of times, we should decide what to do
		// if the currency ID is not found. -vz
		return "0 CHF";
	}
}