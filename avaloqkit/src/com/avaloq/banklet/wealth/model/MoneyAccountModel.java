package com.avaloq.banklet.wealth.model;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.avaloq.afs.aggregation.to.wealth.BookingsResult;
import com.avaloq.afs.server.bsp.client.ws.BookingDateType;
import com.avaloq.afs.server.bsp.client.ws.BookingListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.BookingTO;
import com.avaloq.afs.server.bsp.client.ws.BookingType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.wealth.BookingsRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;
import com.avaloq.framework.util.CurrencyUtil;

public class MoneyAccountModel extends java.util.Observable {
	private static MoneyAccountModel instance = null;
	
	BookingsResult result;
	Long currentAccountId;
	int currentPage;
	
	BookingListQueryTO currentQuery;

	public static MoneyAccountModel getInstance() {
		if(instance == null) {
			instance = new MoneyAccountModel();
		}
		return instance;
	}
	
	public void refresh() throws IOException{
		currentPage++;
		currentQuery.setMaxResultSize(currentPage * AvaloqApplication.getContext().getResources().getInteger(R.integer.wea_count_money_transactions));
		BookingsRequest request = WealthService.findBookings(currentQuery, new RequestStateEvent<BookingsRequest>(){
			@Override
			public void onRequestCompleted(BookingsRequest aRequest) {
				result = aRequest.getResponse().getData();
				MoneyAccountModel.this.setChanged();
				MoneyAccountModel.this.notifyObservers(result);
			}
			
			@Override
			public void onRequestFailed(BookingsRequest aRequest) {
				MoneyAccountModel.this.setChanged();
				MoneyAccountModel.this.notifyObservers(null);
			}
		});
		request.initiateServerRequest();
	}
	
	public void loadData(Long accountId) throws IOException{
		currentAccountId = accountId;
		currentPage = 0;
		currentQuery = createEmptyQuery();
		refresh();
	}

	public List<BookingTO> getBookings() {
		return result.getBookings();
	}
	
	public void setResult(BookingsResult aResult){
		result = aResult;
		currentPage = 0;
		MoneyAccountModel.this.setChanged();
		MoneyAccountModel.this.notifyObservers(result);
	}
	
	public BookingListQueryTO createEmptyQuery(){
		BookingListQueryTO query = new BookingListQueryTO();
		query.setMoneyAccountId(currentAccountId);
		query.setMaxResultSize(AvaloqApplication.getContext().getResources().getInteger(R.integer.wea_count_money_transactions));
		query.setDateType(BookingDateType.BOOKING);
		query.setBookingType(BookingType.ALL);
		return query;
	}
	
	public void setQuery(BookingListQueryTO query){
		currentQuery = query;
	}
	
	public String getAccountTotalBalance(Long currencyId){
		try{
			return CurrencyUtil.formatMoney(this.result.getLatestBalance(), currencyId);
		}
		catch (Exception e){
			// TODO: Handle exception. To clear up: what do we do if we don't find the currency ID?
			return "Error";
		}
		
	}
}