package com.avaloq.banklet.wealth.model;

import java.io.IOException;
import java.util.List;

import com.avaloq.afs.aggregation.to.wealth.PortfolioPositionsResult;
import com.avaloq.afs.server.bsp.client.ws.PortfolioPositionTO;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.wealth.PortfolioPositionsRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;

public class ProductOverviewModel extends java.util.Observable {
	
	private static ProductOverviewModel instance = null;
	
	PortfolioPositionsResult result;

	public static ProductOverviewModel getInstance() {
		if(instance == null) {
			instance = new ProductOverviewModel();
		}
		return instance;
	}
	
	public void loadData(long productId, long valuationCurrencyId) throws IOException{
		PortfolioPositionsRequest request = WealthService.getProductDetails(productId, valuationCurrencyId, new RequestStateEvent<PortfolioPositionsRequest>(){
			@Override
			public void onRequestCompleted(PortfolioPositionsRequest aRequest) {
				result = aRequest.getResponse().getData();
				ProductOverviewModel.this.setChanged();
				ProductOverviewModel.this.notifyObservers(result);
			}
			
			@Override
			public void onRequestFailed(PortfolioPositionsRequest aRequest) {
				ProductOverviewModel.this.setChanged();
				ProductOverviewModel.this.notifyObservers(null);
			}
		});
		request.initiateServerRequest();
	}
	
	public List<PortfolioPositionTO> getPositions(){
		return result.getPortfolioPositions();
	}
	
	public String getFormattedTotalValue(){
		//return CurrencyUtil.formatWealth(result.getTotalValueInValuationCurrency(), this.result.getValuationCurrencyId());
		return "";
	}
}