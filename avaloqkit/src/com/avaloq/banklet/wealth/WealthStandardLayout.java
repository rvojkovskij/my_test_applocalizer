package com.avaloq.banklet.wealth;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.banklet.wealth.adapter.chart.ChartDataProvider;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.util.ApiSpecificCalls;
import com.viewpagerindicator.PageIndicator;

/**
 * @author Ventsislav Zahariev <v.zahariev@insign.ch>
 * 
 * This class is used as an abstraction layer for a very common layout in
 * the Wealth banklet, consisting of a chart and list of items. The 
 * WealthListTable adapter is used as a common data retrieval entity for both
 * elements. 
 * 
 *  Usage: instantiate an object of this class via a subclass of WealthListTable
 *  and use setChartPlaceholder and setListPlaceholder to place the elements
 *  in specific views.
 */

public class WealthStandardLayout<T> {
	
	// private String TAG = WealthStandardLayout.class.getSimpleName();
	
	WealthListTable<T> adapter;
	Fragment fragment;
	Activity activity;
	int chartWidth, chartHeight;
	int mChartPlaceholder;
	int mListPlaceholder;
	
	public WealthStandardLayout(Fragment aFragment, WealthListTable<T> aAdapter, int chartId, int listId){
		this.adapter = aAdapter;
		this.fragment = aFragment;
		this.activity = aFragment.getActivity();
		mChartPlaceholder = chartId;
		mListPlaceholder = listId;
	}
	
	public WealthStandardLayout(Activity activity, WealthListTable<T> aAdapter, int chartId, int listId){
		this.adapter = aAdapter;
		this.activity = activity;
		mChartPlaceholder = chartId;
		mListPlaceholder = listId;
	}
	
	private PagerAdapter getChartAdapter(int ... positions) {
		return adapter.getChartAdapter(getActivity(), chartWidth, chartHeight, positions);
	}
	
	private Activity getActivity() {
		if(fragment != null) {
			return fragment.getActivity();
		} else {
			return activity;
		}
	} 
	
	private View findViewById(int id){
		if (fragment != null){
			return fragment.getView().findViewById(id);
		}
		else
			return activity.findViewById(id);
	}
	
	/**
	 * Renders a chart of the table level or sub-level specified by the positions array
	 * at the specified layout id.
	 */
	public void setChartPlaceholder(final int ... positions){
		final ViewPager chartPager = (ViewPager)findViewById(mChartPlaceholder);
		
		if (chartPager != null){
			if (AvaloqApplication.getInstance().getConfiguration().showWealthCharts()){
				ViewTreeObserver vto = chartPager.getViewTreeObserver();
				vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
					// If the GlobalLayoutListener has been triggered by a programmatic
					// change in the size, then you don't need to react
					boolean hasRefreshedPositionsProgrammatically = false;
					
					// If the current dimensions are the same as the previous ones
					// no need to refresh as well
					int previousHeight = 0, previousWidth = 0;
					
					@Override
				    public void onGlobalLayout() {
						if (hasRefreshedPositionsProgrammatically){
							hasRefreshedPositionsProgrammatically = false;
							return;
						}
						ChartDataProvider<T> dp = adapter.getChartDataProvider();
						if(dp != null) {
							chartHeight = chartPager.getHeight();
							chartWidth = chartPager.getWidth();
							if (chartHeight > 0 && chartWidth > 0 && (previousHeight!= chartHeight || previousWidth != chartWidth)){
								//ApiSpecificCalls.removeOnGlobalLayoutListener(chartPager.getViewTreeObserver(), this);
								updateChartPagerPositions(positions);
								previousHeight = chartHeight;
								previousWidth = chartWidth;
								hasRefreshedPositionsProgrammatically = true;
							}
						}
						else{
							chartPager.setVisibility(View.GONE);
							ApiSpecificCalls.removeOnGlobalLayoutListener(chartPager.getViewTreeObserver(), this);
						}
					}
				});
			}
			else {
				chartPager.setVisibility(View.GONE);
				View tiles = findViewById(R.id.tiles);
				if (tiles != null)
					tiles.setVisibility(View.GONE);
			}
		}
	}
	
	public void updateChartPagerPositions(int ... positions){
		final ViewPager chartPager = (ViewPager)findViewById(mChartPlaceholder);
		if (chartPager != null){
			chartPager.setAdapter(getChartAdapter(positions));
			PageIndicator indicator = (PageIndicator)findViewById(R.id.tiles);
			if (indicator != null){
				if (chartPager.getAdapter().getCount() > 1)
					indicator.setViewPager(chartPager);
				else
					((View)indicator).setVisibility(View.GONE);
			}
		}
	}
	
	/**
	 * Currently refreshes only the list view
	 */
	public void refreshAdapter(WealthListTable<T> aAdapter){
		adapter = aAdapter;
		adapter.notifyDataSetChanged();
	}
	
	/**
	 * Renders the composite table (ListView) at the specified layout id.
	 */
	public void setListPlaceholder(View footer){
		ListView list = (ListView)findViewById(mListPlaceholder);
		if (list != null){
			if (footer != null && list.getFooterViewsCount() == 0)
				list.addFooterView(footer);
			
			int index = list.getFirstVisiblePosition();
			View v = list.getChildAt(0);
			int top = (v == null) ? 0 : v.getTop();

			list.setAdapter(null);
			list.setAdapter(adapter);
			
			list.setSelectionFromTop(index, top);
			// Empty views don't need infinite scroll
			if (footer != null && adapter.getCount() == 0)
				list.removeFooterView(footer);
		}
	}
	
	public void setListPlaceholder(){
		setListPlaceholder(null);
	}
	
	public ListView getListView(){
		return (ListView)findViewById(mListPlaceholder);
	}
	
	public WealthListTable<T> getAdapter() {
		return adapter;
	}
	
	/**
	 * 
	 * @param title
	 * Sets the left main title of the standard wealth layout to the specified string.
	 */
	public void setLeftMainTitle(String title){
		TextView text = (TextView)findViewById(R.id.titleLeftMain);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the right main title of the standard wealth layout to the specified string.
	 */
	public void setRightMainTitle(String title){
		TextView text = (TextView)findViewById(R.id.titleRightMain);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the left subtitle of the standard wealth layout to the specified string.
	 */
	public void setLeftSubTitle(String title){
		TextView text = (TextView)findViewById(R.id.titleLeftSub);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the right subtitle of the standard wealth layout to the specified string.
	 */
	public void setRightSubTitle(String title){
		TextView text = (TextView)findViewById(R.id.titleRightSub);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the left main title of the standard wealth layout to the string characterized with the
	 * specified String-ID.
	 */
	public void setLeftMainTitle(int title){
		TextView text = (TextView)findViewById(R.id.titleLeftMain);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the right main title of the standard wealth layout to the string characterized with the
	 * specified String-ID.
	 */
	public void setRightMainTitle(int title){
		TextView text = (TextView)findViewById(R.id.titleRightMain);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the left subtitle of the standard wealth layout to the string characterized with the
	 * specified String-ID.
	 */
	public void setLeftSubTitle(int title){
		TextView text = (TextView)findViewById(R.id.titleLeftSub);
		if (text != null)
			text.setText(title);
	}
	
	/**
	 * 
	 * @param title
	 * Sets the right subtitle of the standard wealth layout to the string characterized with the
	 * specified String-ID.
	 */
	public void setRightSubTitle(int title){
		TextView text = (TextView)findViewById(R.id.titleRightSub);
		if (text != null)
			text.setText(title);
	}

}
