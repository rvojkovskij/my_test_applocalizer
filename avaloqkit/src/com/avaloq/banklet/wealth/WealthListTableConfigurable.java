package com.avaloq.banklet.wealth;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.avaloq.banklet.wealth.adapter.fields.FieldInterface;
import com.avaloq.framework.R;

public abstract class WealthListTableConfigurable<T> extends WealthListTable<T>{
	
	protected String layoutName;
	
	public WealthListTableConfigurable(Activity activity, int layoutId, String layout, List<FieldInterface<T>> aFields, List<T> items, List<? extends WealthListTable<?>> rows) {
		super(activity, layoutId, aFields, items, rows);
		layoutName = layout;
	}
	
	public void setLayoutPrefix(String name){
		layoutName = name;
	}
	
	protected View drawRow(final int position, View convertView, ViewGroup parent) {
		ViewGroup view = (ViewGroup)super.drawRow(position, convertView, parent);
		ViewGroup container = (ViewGroup)view.findViewById(R.id.conf_container);
		if (container == null)
			return adjustView(view);
		else{
			adjustView(container);
			return view;
		}
	}
	
	protected View adjustView(ViewGroup container) {
		applyWeightsToView(container, getWeightsArray(activity, layoutName));
		return container;
	}
	
	public static int[] getWeightsArray(Context context, String layoutName){
		Resources res = context.getResources();
		//ViewGroup fragmentView = (ViewGroup)inflater.inflate(getLayoutResourceId(), container, false);
		int configId = res.getInteger(R.integer.wea_report_configuration);
		Integer confGroup = null;
		int resId = res.getIdentifier(layoutName+"_weights_groups", "array", context.getPackageName());
		try{
			int[] arr = res.getIntArray(resId);
			confGroup = res.getIntArray(resId)[configId-1];
		}
		catch (Exception e){}
		
		
		resId = res.getIdentifier(layoutName+"_weights_type_"+configId, "array", context.getPackageName());
		try{
			return res.getIntArray(resId);
		}
		catch (Exception e){}
		// If the type-specific configuration is not found, fall back to the group one
		if (confGroup != null){
			try {
				resId = res.getIdentifier(layoutName+"_weights_group_"+confGroup, "array", context.getPackageName());
				return res.getIntArray(resId);
			}
			catch (Exception e) {}
		}
		// If the group-specific configuration is not found, fall back to the default one
		resId = res.getIdentifier(layoutName+"_weights", "array", context.getPackageName());
		try {
			resId = res.getIdentifier(layoutName+"_weights_group"+confGroup, "array", context.getPackageName());
			return res.getIntArray(resId);
		}
		catch (Exception e) {}
		// If even the default configuration is not found, then we can't continue
		throw new IllegalArgumentException("Cannot find weights configuration for layout "+layoutName+" and type "+configId+" and group "+confGroup);
	}
	
	public static void applyWeightsToView(ViewGroup container, int[] weights){
		for (int i=0; i<weights.length; i++){
			View child = container.getChildAt(i);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)child.getLayoutParams();
			if (weights[i] > 0){
				if (weights[i] <= 100){
					params.weight = weights[i];
					child.setLayoutParams(params);
				}
			}
			else
				child.setVisibility(View.GONE);
		}
	}
	
}
