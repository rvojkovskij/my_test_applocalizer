package com.avaloq.banklet.wealth.activity;

import java.io.IOException;
import java.util.Observable;

import android.os.Bundle;

import com.avaloq.afs.server.bsp.client.ws.WealthOverviewAllocationReportTO;
import com.avaloq.banklet.wealth.BaseWealthTabbedActivity;
import com.avaloq.banklet.wealth.model.WealthOverviewModel;
import com.avaloq.framework.ui.FragmentNavigationItem;

public class WealthStartActivity extends BaseWealthTabbedActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public Observable createModel() {
		return WealthOverviewModel.getInstance();
	}

	@Override
	public void loadModelData() throws IOException{
		((WealthOverviewModel)model).loadData();
	}

	@Override
	public void setupExtras() {}
	
	public FragmentNavigationItem[] setupTabs() {
		WealthOverviewModel mod = (WealthOverviewModel)model;
		FragmentNavigationItem[] items = new FragmentNavigationItem[mod.getReports().size()];
		for(int i = 0;i < mod.getReports().size();i++){
			WealthOverviewAllocationReportTO report = mod.getReports().get(i);
			Bundle bundle = new Bundle();
			bundle.putString(WealthReportFragment.EXTRA_REPORT_INDEX, report.getName());
			bundle.putString(WealthReportFragment.EXTRA_GROUP_INDEX, mod.getGroups(report.getName()).get(0).getName());
			items[i] = new FragmentNavigationItem(WealthReportFragment.class, report.getName(), bundle);
		}
		return items;
	}

}
