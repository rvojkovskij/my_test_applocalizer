package com.avaloq.banklet.wealth.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;

import android.os.Bundle;

import com.avaloq.afs.server.bsp.client.ws.AssetAllocationDeepReportTO;
import com.avaloq.afs.server.bsp.client.ws.PortfolioType;
import com.avaloq.banklet.wealth.BaseWealthTabbedActivity;
import com.avaloq.banklet.wealth.model.PortfolioOverviewModel;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;

public class PortfolioOverviewActivity extends BaseWealthTabbedActivity{
	
//	private static final String TAG = PortfolioOverviewActivity.class.getSimpleName();
	
	public static final String EXTRA_PORTFOLIO_ID = "extra_portfolio_id";
	public static final String EXTRA_PORTFOLIO_NAME = "extra_portfolio_name";
	public static final String EXTRA_PRODUCT_ID = "extra_product_id";
	public static final String EXTRA_TYPE = "extra_type";
	public static final String EXTRA_ALLOC_LIST = "extra_alloc_list";
	public static final String EXTRA_ITEM_LIST = "extra_item_list";
	
	private long portfolioId, productId;
	private String type, portfolioName;
	private ArrayList<Integer> allocIdList, itemIdList;
	
	@Override
	public Observable createModel() {
		return PortfolioOverviewModel.getInstance();
	}

	@Override
	public void loadModelData() throws IOException {
		if (portfolioId != 0 && productId != 0 && !type.equals(""))
			((PortfolioOverviewModel)model).loadData(portfolioId, PortfolioType.fromValue(getIntent().getStringExtra(EXTRA_TYPE)), productId);
		else
			this.update();
	}

	@Override
	public void setupExtras() {
		portfolioId = getIntent().getLongExtra(EXTRA_PORTFOLIO_ID, 0);
		productId = getIntent().getLongExtra(EXTRA_PRODUCT_ID, 0);
		portfolioName = getIntent().getStringExtra(EXTRA_PORTFOLIO_NAME);
		if (portfolioName == null)
			throw new IllegalArgumentException("The name of the portfolio can not be avq_activity_empty");
		setTitle(portfolioName);
		type = getIntent().getStringExtra(EXTRA_TYPE);
		if (type == null)
			type = "";
		allocIdList = getIntent().getIntegerArrayListExtra(EXTRA_ALLOC_LIST);
		if (allocIdList == null)
			allocIdList = new ArrayList<Integer>();
		itemIdList = getIntent().getIntegerArrayListExtra(EXTRA_ITEM_LIST);
		if (itemIdList == null)
			itemIdList = new ArrayList<Integer>();
		
	}

	@Override
	public FragmentNavigationItem[] setupTabs() {
		PortfolioOverviewModel mod = (PortfolioOverviewModel)model;
		int allocSize = mod.getAllocations(allocIdList, itemIdList).size();
		FragmentNavigationItem[] items = new FragmentNavigationItem[allocSize+1];
		for(int i = 0;i < allocSize;i++){
			AssetAllocationDeepReportTO report = mod.getAllocations(allocIdList, itemIdList).get(i);
			Bundle bundle = new Bundle();
			ArrayList<Integer> tmp = new ArrayList<Integer>(allocIdList);
			tmp.add(i);
			bundle.putIntegerArrayList(PortfolioFragment.EXTRA_ALLOC_LIST, tmp);
			bundle.putIntegerArrayList(PortfolioFragment.EXTRA_ITEM_LIST, itemIdList);
			items[i] = new FragmentNavigationItem(PortfolioFragment.class, report.getName(), bundle);
		}
		
		Bundle bundle = new Bundle();
		bundle.putIntegerArrayList(PortfolioFragment.EXTRA_ALLOC_LIST, allocIdList);
		bundle.putIntegerArrayList(PortfolioFragment.EXTRA_ITEM_LIST, itemIdList);
		items[allocSize] = new FragmentNavigationItem(StocksFragment.class, getResources().getString(R.string.wea_tab_label_stocks), bundle);
		
		return items;
	}
	

}
