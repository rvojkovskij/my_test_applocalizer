package com.avaloq.banklet.wealth.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.avaloq.afs.server.bsp.client.ws.WealthOverviewAllocationItemTO;
import com.avaloq.afs.server.bsp.client.ws.WealthOverviewGroupTO;
import com.avaloq.banklet.wealth.BaseWealthFragmentConfigurable;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.adapter.WealthStartAdapter;
import com.avaloq.banklet.wealth.model.WealthOverviewModel;
import com.avaloq.framework.R;

public class WealthReportFragment extends BaseWealthFragmentConfigurable<WealthOverviewAllocationItemTO> {
	
	private static final String TAG = WealthReportFragment.class.getSimpleName();
	
	public static String EXTRA_REPORT_INDEX = "extra_report_index";
	public static String EXTRA_GROUP_INDEX = "extra_group_index";
	
	protected String reportName, groupExtra, groupName;
	protected Integer mAdapterPosition;
	
	@Override
	public String getTitleLayoutName(){
		return "wea_conf_report_title";
	}
	
	public String getConfigurationName(){
		return "wea_conf_overview";
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView => " + reportName);
		View fragmentView = inflater.inflate(R.layout.wea_report_activity, container, false);
		if(getArguments() == null || getArguments().getString(EXTRA_REPORT_INDEX) == null || getArguments().getString(EXTRA_GROUP_INDEX) == null) {
			throw new IllegalArgumentException("The Arguments EXTRA_REPORT_INDEX and EXTRA_GROUP_INDEX must be set.");
		}
		return fragmentView;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate => " + reportName);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, "onActivityCreated. => " + reportName);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		Log.d(TAG, "onAttach => " + reportName);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d(TAG, "onViewCreated => " + reportName);
		this.update();
		
		View conf = getView().findViewById(R.id.configurationIcon);
		int configId = getActivity().getResources().getInteger(R.integer.wea_report_configuration);
		/*if (configId == 1 || configId == 5){
			conf = conf_current_group;
			if (conf_market != null)
				conf_market.setVisibility(View.GONE);
		}
		else{
			conf = conf_market_group;
			if (conf_current != null)
				conf_current.setVisibility(View.GONE);
		}*/
		
		if (conf != null){
			OnClickListener listener = new OnClickListener(){
				@Override
				public void onClick(View v) {
					String[] array = {};
					List<String> items = new ArrayList<String>();
					for(WealthOverviewGroupTO item: WealthOverviewModel.getInstance().getGroups(reportName)){
						items.add(item.getName());
					}
					AlertDialog.Builder builder = new AlertDialog.Builder(WealthReportFragment.this.getActivity());
					builder.setSingleChoiceItems(items.toArray(array), items.indexOf(groupName), new DialogInterface.OnClickListener() {
			               public void onClick(DialogInterface dialog, int which) {
			            	   dialog.dismiss();
			            	   WealthReportFragment.this.groupName = WealthOverviewModel.getInstance().getGroups(reportName).get(which).getName();
			            	   WealthReportFragment.this.update();
			               }
			        });
					// TODO: Don't create any dialogs directly,
					// Use the DialogFragment.create().show() which is 'managed' -mb
					builder.setNegativeButton(R.string.wea_button_cancel, null);
					builder.setTitle(R.string.wealth_config_title);
					Dialog dialog = builder.create();
					dialog.show();
				}
			};
			conf.setOnClickListener(listener);
		}
	}
	
	@Override
    public Observable createModel(){
		return WealthOverviewModel.getInstance();
    }

    @Override
    public void loadModelData(){}

    @Override
    public WealthListTable<WealthOverviewAllocationItemTO> createAdapter() {
    	return WealthStartAdapter.getAdapter(this, WealthOverviewModel.getInstance().getAllocations(reportName, groupName), mAdapterPosition);
    }

    @Override
    public void setupExtras() {
    	reportName = getArguments().getString(EXTRA_REPORT_INDEX);
    	groupName = getArguments().getString(EXTRA_GROUP_INDEX);
    }

    @Override
    public int getPostElement() {
    	return R.id.tableHeader;
    }
    
    @Override
    public void update(){
    	Log.d(TAG, "update => " + reportName);
    	super.update();
    	this.getStandardLayout().setLeftMainTitle(R.string.wea_title_overview_market_value);
    	this.getStandardLayout().setLeftSubTitle(WealthOverviewModel.getInstance().getFormattedTotalValue(reportName));
    	this.getStandardLayout().setRightMainTitle(R.string.wea_title_overview_asset);
    	this.getStandardLayout().setRightSubTitle(reportName);
    }
    
    public void setAdapterPositions(Integer position){
    	mAdapterPosition = position;
    	if (position == null){
    		getStandardLayout().updateChartPagerPositions();
    		update();
    		getStandardLayout().setChartPlaceholder();
    	}
    	else {
    		getStandardLayout().updateChartPagerPositions(position);
    		update();
    		getStandardLayout().setChartPlaceholder(position);
    	}
    	
    }
    
    public Integer getAdapterPositions(){
    	return mAdapterPosition;
    }
	
}
