package com.avaloq.banklet.wealth.activity;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Observable;

import android.os.Bundle;
import android.util.Log;

import com.avaloq.afs.server.bsp.client.ws.PortfolioPositionTO;
import com.avaloq.banklet.wealth.BaseWealthActivityConfigurable;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.adapter.ProductOverviewAdapter;
import com.avaloq.banklet.wealth.model.ProductOverviewModel;
import com.avaloq.framework.R;
import com.avaloq.framework.util.CurrencyUtil;

public class ProductOverviewActivity extends BaseWealthActivityConfigurable<PortfolioPositionTO> {
	
	public static String EXTRA_PRODUCT_ID = "extra_product_id";
	public static String EXTRA_CURRENCY_ID = "extra_currency_id";
	public static String EXTRA_PRODUCT_NAME = "extra_product_name";
	public static String EXTRA_PRODUCT_TOTAL_VALUE = "extra_product_total_value";
	
	long productId, currencyId;
	String productName;
	BigDecimal productValue;
	
	@Override
	public String getConfigurationName(){
		return "wea_conf_product";
	}
	
	@Override
	public String getTitleLayoutName(){
		return "wea_conf_product_title";
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d("ProductOverviewActivity", "Start the product overview");
		setContentView(R.layout.wea_report_activity);
	}

	@Override
	public Observable createModel() {
		return ProductOverviewModel.getInstance();
	}
		
		
	public void loadModelData() throws IOException {
		((ProductOverviewModel) model).loadData(productId, currencyId);
	}

	@Override
	public void setupExtras() {
		productId = getIntent().getLongExtra(EXTRA_PRODUCT_ID, 0);
		currencyId = getIntent().getLongExtra(EXTRA_CURRENCY_ID, 0);
		productName = getIntent().getStringExtra(EXTRA_PRODUCT_NAME);
		if (productName == null)
			throw new IllegalArgumentException("The product name can not be null");
		productValue = new BigDecimal(getIntent().getStringExtra(EXTRA_PRODUCT_TOTAL_VALUE));
		setTitle(productName);
	}

	@Override
	public WealthListTable<PortfolioPositionTO> createAdapter() {
		return ProductOverviewAdapter.getAdapter(this, ((ProductOverviewModel)model).getPositions());
	}

	@Override
	public int getPostElement() {
		return R.id.tableHeader;
	}
	
	@Override
	public void update() {
		super.update();
		this.getStandardLayout().setLeftMainTitle(R.string.wea_title_overview_market_value);
    	this.getStandardLayout().setLeftSubTitle(CurrencyUtil.formatMoney(productValue, currencyId));
	}
	
	
}
