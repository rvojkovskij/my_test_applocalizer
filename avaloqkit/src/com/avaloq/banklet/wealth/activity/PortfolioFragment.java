package com.avaloq.banklet.wealth.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.afs.server.bsp.client.ws.PortfolioAllocationDeepItemTO;
import com.avaloq.banklet.wealth.BaseWealthFragmentConfigurable;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.adapter.PortfolioOverviewAdapter;
import com.avaloq.banklet.wealth.model.PortfolioOverviewModel;
import com.avaloq.framework.R;

public class PortfolioFragment extends BaseWealthFragmentConfigurable<PortfolioAllocationDeepItemTO> {
	
	public static String EXTRA_ALLOC_LIST = "extra_alloc_list";
	public static String EXTRA_ITEM_LIST = "extra_item_list";
	
	private ArrayList<Integer> allocIdList, itemIdList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View fragmentView = inflater.inflate(R.layout.wea_report_activity, container, false);
		return fragmentView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.update();
	}

	@Override
	public Observable createModel() {
		return PortfolioOverviewModel.getInstance();
	}

	@Override
	public void loadModelData() throws IOException {}

	@Override
	public WealthListTable<PortfolioAllocationDeepItemTO> createAdapter() {
		return PortfolioOverviewAdapter.getAdapter(this, PortfolioOverviewModel.getInstance().getAllocationItems(allocIdList, itemIdList), allocIdList, itemIdList);
	}

	@Override
	public void setupExtras() {
		allocIdList = getArguments().getIntegerArrayList(EXTRA_ALLOC_LIST);
		if (allocIdList == null || allocIdList.size() == 0)
			throw new IllegalArgumentException("The alloc-list cannot be avq_activity_empty");
		itemIdList = getArguments().getIntegerArrayList(EXTRA_ITEM_LIST);
		if (itemIdList == null)
			itemIdList = new ArrayList<Integer>();
		//portfolioName = getArguments().getInt(EXTRA_PORTFOLIO_INDEX, 0);
	}

	@Override
	public int getPostElement() {
		return R.id.tableHeader;
	}
	
	@Override
	public void update() {
		super.update();
		this.getStandardLayout().setLeftMainTitle(R.string.wea_title_overview_market_value);
    	this.getStandardLayout().setLeftSubTitle(PortfolioOverviewModel.getInstance().getFormattedTotalValue(allocIdList, itemIdList));
	}
	
	

}
