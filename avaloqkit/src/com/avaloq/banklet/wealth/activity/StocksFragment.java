package com.avaloq.banklet.wealth.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.afs.server.bsp.client.ws.PortfolioPositionTO;
import com.avaloq.banklet.wealth.BaseWealthFragmentConfigurable;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.adapter.ProductOverviewAdapter;
import com.avaloq.banklet.wealth.model.PortfolioOverviewModel;
import com.avaloq.framework.R;

public class StocksFragment extends BaseWealthFragmentConfigurable<PortfolioPositionTO> {
	
	public static String EXTRA_ALLOC_LIST = "extra_alloc_list";
	public static String EXTRA_ITEM_LIST = "extra_item_list";
	
	private ArrayList<Integer> allocIdList, itemIdList;
	
	private String confName = "wea_conf_product";
	
	@Override
	public String getConfigurationName(){
		return confName;
	}
	
	@Override
	public String getTitleLayoutName(){
		return "wea_conf_product_title";
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View fragmentView = inflater.inflate(R.layout.wea_report_activity, container, false);
		return fragmentView;
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.update();
	}
	
	@Override
	public Observable createModel() {
		return PortfolioOverviewModel.getInstance();
	}

	@Override
	public void loadModelData() throws IOException {}

	@Override
	public WealthListTable<PortfolioPositionTO> createAdapter() {
		List<PortfolioPositionTO> positions = new ArrayList<PortfolioPositionTO>();
		if (allocIdList.size()==0 && itemIdList.size() == 0)
			positions = ((PortfolioOverviewModel)model).getStockPositions();
		else
			positions = ((PortfolioOverviewModel)model).getStockPositions(allocIdList, itemIdList);
		return ProductOverviewAdapter.getAdapter(this.getActivity(), positions);
	}

	@Override
	public void setupExtras() {
		allocIdList = getArguments().getIntegerArrayList(EXTRA_ALLOC_LIST);
		if (allocIdList == null)
			allocIdList = new ArrayList<Integer>();
		itemIdList = getArguments().getIntegerArrayList(EXTRA_ITEM_LIST);
		if (itemIdList == null)
			itemIdList = new ArrayList<Integer>();
	}

	@Override
	public int getPostElement() {
		return R.id.tableHeader;
	}
	
	@Override
	public void update() {
		super.update();
		this.getStandardLayout().setLeftMainTitle(R.string.wea_title_overview_market_value);
    	this.getStandardLayout().setLeftSubTitle(PortfolioOverviewModel.getInstance().getFormattedTotalValue(allocIdList, itemIdList));
    	/*if (getStandardLayout().getAdapter().getChartDataProvider() != null){
			confName = "wea_conf_product_with_chart";
			setTableHeaderView(getView());
		}*/
	}

}
