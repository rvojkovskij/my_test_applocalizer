package com.avaloq.banklet.wealth.activity;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Observable;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.BookingListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.BookingType;
import com.avaloq.banklet.wealth.BaseWealthActivity;
import com.avaloq.banklet.wealth.WealthListTable;
import com.avaloq.banklet.wealth.WealthStandardLayout;
import com.avaloq.banklet.wealth.adapter.MoneyAccountAdapter;
import com.avaloq.banklet.wealth.model.MoneyAccountModel;
import com.avaloq.framework.DownloadClickListener;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.wealth.BookingsRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;
import com.avaloq.framework.tools.searchwidget.AmountCriterium;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.DateCriterium;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;
import com.avaloq.framework.util.CurrencyUtil;

public class MoneyAccountActivity extends BaseWealthActivity<Date> {
	public static String EXTRA_ACCOUNT_ID = "extra_account_id";
	public static String EXTRA_ACCOUNT_NAME = "extra_account_name";
	public static String EXTRA_CURRENCY_ID = "extra_currency_id";
	public static String EXTRA_ACCOUNT_BALANCE = "extra_account_balance";
	public static String EXTRA_ACCOUNT_INTEREST = "extra_account_interest";
	public static String EXTRA_ACCOUNT_CURRENT = "extra_account_current";
	
	private Long accountId, currencyId;
	private String accountName;
	private BigDecimal accountBalance, accountInterest, accountCurrent;
	
	private View footerView;
	
	BookingListQueryTO query;
	SearchView searchView;
	File pdfFile = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wea_money_account_activity);
		setTitle("");
		footerView = getLayoutInflater().inflate(R.layout.avq_list_loading, null);
		
		searchView = (SearchView)findViewById(R.id.search_widget);
		searchView.init(this);
		searchView.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				query = MoneyAccountModel.getInstance().createEmptyQuery();
				for (Criterium crit: searchView.getCriteria())
					crit.addToQuery();
				query.setNarrative(searchView.getText());
				BookingsRequest request = WealthService.findBookings(query, new RequestStateEvent<BookingsRequest>(){
					@Override
					public void onRequestCompleted(BookingsRequest aRequest) {
						MoneyAccountModel.getInstance().setQuery(query);
						MoneyAccountModel.getInstance().setResult(aRequest.getResponse().getData());
					}
					
					@Override
					public void onRequestFailed(BookingsRequest aRequest) {
						// TODO
					}
				});
				request.initiateServerRequest();
			}
		});
		
		searchView.addCriterium(new AmountCriterium(this){
			@Override
			public void addToQuery() {
				if (mFrom != 0)
					query.setFromAmount(BigDecimal.valueOf(mFrom));
				if (mTo != 0)
					query.setToAmount(BigDecimal.valueOf(mTo));
			}
		});
		searchView.addCriterium(new DateCriterium(this){
			@Override
			public void addToQuery() {
				if (mFrom != null)
					query.setFromDate(mFrom);
				if (mTo != null)
					query.setToDate(mTo);
			}
		});
		
		searchView.addCriterium(new SingleChoiceCriterium<BookingType>(this) {
			
			@Override
			public BookingType getDefault() {
				return BookingType.ALL;
			}

			@Override
			public LinkedHashMap<BookingType, String> getTypeLabelMapping() {
				LinkedHashMap<BookingType, String> map = new LinkedHashMap<BookingType, String>();
				map.put(BookingType.ALL, getString(R.string.criterium_booking_all));
				map.put(BookingType.DEBIT, getString(R.string.criterium_booking_debit));
				map.put(BookingType.CREDIT, getString(R.string.criterium_booking_credit));
				return map;
			}

			@Override
			public void addToQuery() {
				if (mElement == null)
					query.setBookingType(BookingType.ALL);
				else
					query.setBookingType(mElement);
			}

			@Override
			public String getName() {
				return mActivity.getString(R.string.criterium_booking_type);
			}

			@Override
			public ArrayList<Integer> getIcons() {
				// TODO Auto-generated method stub
				return null;
			}
		});
	}

	@Override
	public Observable createModel() {
		return MoneyAccountModel.getInstance();
	}

	@Override
	public void loadModelData() throws IOException {
		((MoneyAccountModel)model).loadData(accountId);
	}
	
	@Override
	public void doRefreshModelData(){
		try {
			((MoneyAccountModel)model).refresh();
		} catch (IOException e) {
			// TODO: render an error message
			e.printStackTrace();
		}
	}

	@Override
	public WealthListTable<Date> createAdapter() {
		Runnable callback = new Runnable() {
			@Override
			public void run() {
				refresh();
			}
		};
		
		Runnable noMoreContent = new Runnable() {
			@Override
			public void run() {
				getListView().removeFooterView(footerView);
			}
		};
		return MoneyAccountAdapter.getAdapter(this, ((MoneyAccountModel)model).getBookings(), callback, noMoreContent);
	}

	@Override
	public void setupExtras() {
		accountId = getIntent().getLongExtra(EXTRA_ACCOUNT_ID, 0);
		accountName = getIntent().getStringExtra(EXTRA_ACCOUNT_NAME);
		currencyId = getIntent().getLongExtra(EXTRA_CURRENCY_ID, 0);
		accountBalance = (BigDecimal)getIntent().getSerializableExtra(EXTRA_ACCOUNT_BALANCE);
		accountInterest = (BigDecimal)getIntent().getSerializableExtra(EXTRA_ACCOUNT_INTEREST);
		accountCurrent = (BigDecimal)getIntent().getSerializableExtra(EXTRA_ACCOUNT_CURRENT);
	}

	@Override
	public int getPostElement() {
		return R.id.tableHeader;
	}
	
	@Override
    public void update(){
    	super.update();
    	setTitles();
    }
	
	public File getPdfFile(){
		return pdfFile;
	}
	
	public void setPdfFile(File file){
		pdfFile = file;
	}
	
	public void setTitles(){
		this.getStandardLayout().setLeftMainTitle(R.string.wea_title_money_account_balance);
		if (accountBalance == null){
			MoneyAccountModel mod = (MoneyAccountModel)model;
			this.getStandardLayout().setLeftSubTitle(mod.getAccountTotalBalance(currencyId));
		} else {
			this.getStandardLayout().setLeftSubTitle(CurrencyUtil.formatMoney(accountBalance, currencyId));
		}
    	this.getStandardLayout().setRightMainTitle(R.string.wea_title_money_account);
    	this.getStandardLayout().setRightSubTitle(accountName);
    	
    	TextView interest = (TextView)findViewById(R.id.valueLeftInterest);
    	TextView current = (TextView)findViewById(R.id.valueLeftCurrent);
    	if (interest != null)
    		interest.setText(CurrencyUtil.formatMoney(accountInterest, currencyId));
    	if (current != null)
    		current.setText(CurrencyUtil.formatMoney(accountCurrent, currencyId));
    	
    	//setTitle(accountName);
    	setTitle(getString(R.string.wea_money_account_title));
	}
	
	@Override
	public void onCommunicationBreakdown(){
		super.onCommunicationBreakdown();
		standardLayout = new WealthStandardLayout<Date>(MoneyAccountActivity.this, null, 0, 0);
		setTitles();
	}
	
	@Override
	public View getListFooterView() {
		return footerView;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			searchView.onActivityResult(requestCode, resultCode, data);
		else if (requestCode == DownloadClickListener.RESULT_PDF){
			if(pdfFile != null) {
				Log.d("MoneyAccountActivity", "Deleting PDF-File...");
				pdfFile.delete();
			}
		}
	}

}
