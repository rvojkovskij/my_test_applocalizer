package com.avaloq.banklet.wealth;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.InlineMessageUtil;

/**
 * 
 * 		   This class is the top
 *         level abstraction for all Wealth activities, that are displayed as
 *         tabs of tabbable activities. This class handles the models, the
 *         WealthStandardLayout, the adapters and the observers for the
 *         developer.
 * 
 *         Usage: The Fragments should extend this class and implement the
 *         abstract methods.
 *         
 *         @author zahariev 
 * 
 */

public abstract class BaseWealthFragment<T> extends BankletFragment implements Observer {
	/**
	 * created by the developer in the overridden createAdapter()
	 */
	protected WealthListTable<T> adapter;

	/**
	 * created by the developer in the overridden createModel()
	 */
	protected Observable model;

	/**
	 * created dynamically in update() based on the specified WealthListTable<?>
	 * adapter. Alter this object to force changes in the UI (for example
	 * refresh the activity).
	 */
	protected WealthStandardLayout<T> standardLayout;
	
	int mChartId = R.id.chart;
	int mListId = R.id.report_list;

	/**
	 * This method does the basic initialization of the activity. It creates the
	 * specified model and if necessary (see needsObserver()) binds an observer
	 * to it, so that once the web service request is finished the update method
	 * gets called automatically.
	 */
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		this.setupExtras();
		model = this.createModel();
		if (needsObserver()) {
			model.addObserver(this);
		}
		try {
			this.loadModelData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of
	 * parameters, the model classes may require different initialization
	 * routines. The activities should override this method, initialize the
	 * required model and return it, so that the BaseWealthFragment can handle
	 * it further for the developer.
	 * 
	 */
	public abstract Observable createModel();

	/**
	 * Since Wealth-Banklet Web-Service calls require a different set of
	 * parameters, the model classes may require different initialization
	 * routines. The Wealth activities should override this method, and use the
	 */
	 public abstract void loadModelData() throws IOException;

	/**
	 * 
	 * The activities should override this method, create an instance of
	 * WealthListTable and return it, so that it can be handled by the
	 * BaseActivity (rendering lists and charts).
	 */
	public abstract WealthListTable<T> createAdapter();

	/**
	 * If there are any activity parameters passed as extras, override this
	 * method to initialize attributes.
	 */
	public abstract void setupExtras();

	/**
	 * This method should return the id of a view from the layout, so that once
	 * the web-service request is done, the BaseActivity could post() to it (and
	 * thus return to the UI thread).
	 * 
	 * See update(Observable observable, Object data) for a reference.
	 */
	public abstract int getPostElement();

	/**
	 * Usually the model of the BaseWealthFragment subclasses has already been
	 * created in the tabbed activity that hosts them. In this case, a second
	 * initialization of the model and dispatching a web-service request and
	 * binding an observer to it is not necessary. This method should return
	 * false if that's the case or true in case there is a different model with
	 * a separate web-service call.
	 */
	public boolean needsObserver() {
		return false;
	}

	/**
	 * This method is called automatically after the web-service request is
	 * processed. It returns to the UI-thread, so that the UI can be updated.
	 * 
	 * IMPORTANT! Do not override this method directly. Instead, override the
	 * method update(), which will be executed within the scope of the
	 * UI-Thread.
	 * 
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		if (arg1 == null){
			onCommunicationBreakdown();
		}
		else{
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					BaseWealthFragment.this.update();
				}
			});
		}
	}

	/**
	 * This method gets called once the web-service request is processed. It is
	 * executed within the scope of the UI-Thread, so that UI elements can be
	 * updated directly. The method initializes the standard Wealth Layout
	 * (chart + list).
	 * 
	 * IMPORTANT! If you override this method, make sure to call super.update()
	 * first, so that the WealthStandardLayout gets populated and inflated.
	 * 
	 * // TODO CR: As mentioned in the other Base class..
	 * 
	 */
	public void update() {
		standardLayout = new WealthStandardLayout<T>(BaseWealthFragment.this, BaseWealthFragment.this.createAdapter(), mChartId, mListId);
		standardLayout.setChartPlaceholder();
		standardLayout.setListPlaceholder();
	}

	public WealthStandardLayout<T> getStandardLayout() {
		return standardLayout;
	}
	
	public void setChartId(int id){
		mChartId = id;
	}
	
	public int getChartId(){
		return mChartId;
	}
	
	public void setListId(int id){
		mListId = id;
	}
	
	public int getListId(){
		return mListId;
	}
	
	public void onCommunicationBreakdown(){
		ViewGroup vg = (ViewGroup)getView().getParent();
		InlineMessageUtil.showNetworkError(vg);
	}
	
	

}
