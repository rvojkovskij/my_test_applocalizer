package com.avaloq.banklet.wealth;

import android.view.ViewGroup;

import com.avaloq.framework.R;

public abstract class BaseWealthActivityConfigurable<T> extends BaseWealthActivity<T> {
	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		ViewGroup tableHeader = (ViewGroup)findViewById(R.id.tableHeader);
		if (tableHeader != null){
			int resId = getResources().getIdentifier(getTitleLayoutName(), "layout", getPackageName());
			ViewGroup titleRow = (ViewGroup)getLayoutInflater().inflate(resId, null, false);
			
			int[] weights = WealthListTableConfigurable.getWeightsArray(this, getConfigurationName());
			WealthListTableConfigurable.applyWeightsToView(titleRow, weights);
		
			tableHeader.addView(titleRow);
		}
	}
	
	/**
	 * Can be overridden by the subclass to specify another layout file.
	 * @return
	 */
	public String getTitleLayoutName(){
		return "wea_conf_overview_title";
	}
	
	/**
	 * Can be overridden by the subclass to specify another configuration array.
	 * @return
	 */
	public String getConfigurationName(){
		return "wea_conf_overview";
	}
}
