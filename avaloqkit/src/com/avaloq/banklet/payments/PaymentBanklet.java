package com.avaloq.banklet.payments;

import java.util.List;

import android.util.Log;

import com.avaloq.afs.aggregation.to.payment.PaymentInfo;
import com.avaloq.afs.aggregation.to.payment.StandingPaymentInfo;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.Banklet;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentApprovalInfoListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.ActivityNavigationItem;

/**
 * The Banklet for the Payment
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class PaymentBanklet extends Banklet {
	
	private static final String TAG = PaymentBanklet.class.getSimpleName();
	
	private TradingDefaultsResult mTradingDefaults = null;
	private List<PaymentInfo> mPendingPayments;
	private List<StandingPaymentInfo> mPendingStanding;

	/**
	 * The Array of Navigation Items
	 */
	private static final ActivityNavigationItem[] items = new ActivityNavigationItem[] {
		new ActivityNavigationItem(AvaloqApplication.getContext(), PaymentActivity.class, R.string.pmt_banklet_name, R.attr.IconPayments, R.style.Theme_Banklet_PMT)
	};

	@Override
	public void onCreate() {
	}

	@Override
	public ActivityNavigationItem getInitialActivity() {
		return items[0];
	}

	@Override
	public ActivityNavigationItem[] getMainNavigationItems() {
		return items;
	}

	@Override
	public void onEmptyCache() {
		// TODO avq_activity_empty all model data
	}
	
	/**
	 * Get the PaymentBanklet singleton instance
	 */
	public static PaymentBanklet getInstance() {
		return (PaymentBanklet) Banklet.getInstanceOf(PaymentBanklet.class);
	}
	
	public void setPengingPayments(List<PaymentInfo> objects){
		mPendingPayments  = objects;
	}
	
	public List<PaymentInfo> getPengingPayments(){
		return mPendingPayments;
	}
	
	public int getPendingApprovalsNumber(){
		int count = 0;
		if (hasPendingApprovals()){
			if (mPendingPayments != null){
				count += mPendingPayments.size();
			}
			if (mPendingStanding != null){
				count += mPendingStanding.size();
			}
		}
		return count;
	}
	
	public void setPendingStandingOrders(List<StandingPaymentInfo> objects){
		mPendingStanding  = objects;
	}
	
	public List<StandingPaymentInfo> getPendingStandingOrders(){
		return mPendingStanding;
	}
	
	public boolean hasPendingApprovals(){
		return (mPendingPayments != null && mPendingPayments.size() > 0 ) || (mPendingStanding != null && mPendingStanding.size() > 0);
	}
}
