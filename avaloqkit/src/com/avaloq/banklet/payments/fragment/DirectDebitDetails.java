package com.avaloq.banklet.payments.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.DirectDebitMandateTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.directdebitmandateoverview.DirectDebitMandateOverviewService;
import com.avaloq.framework.comms.webservice.directdebitmandateoverview.DirectDebitMandateRequest;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.util.CurrencyUtil;

public class DirectDebitDetails extends BankletActivity {
	public static final String EXTRA_MANDATE = "extra_mandate";
	public static final String EXTRA_ACCOUNT_MANDATE = "extra_account_mandate";
	MoneyAccountTO mAccount;
	DirectDebitMandateTO mMandate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getIntent().getExtras() == null || !getIntent().getExtras().containsKey(EXTRA_MANDATE) || !getIntent().getExtras().containsKey(EXTRA_ACCOUNT_MANDATE))
			throw new IllegalArgumentException("The extras EXTRA_MANDATE and/or EXTRA_ACCOUNT_MANDATE not set");
		mAccount = (MoneyAccountTO)getIntent().getExtras().getSerializable(EXTRA_ACCOUNT_MANDATE);
		mMandate = (DirectDebitMandateTO)getIntent().getExtras().getSerializable(EXTRA_MANDATE);
		
		setContentView(R.layout.pmt_direct_debit_details);
		
		fillTextView(R.id.mandate_name, mMandate.getOwnerText());
		int id = getResources().getIdentifier("pmt_direct_debit_status_"+mMandate.getStatus().toString().toLowerCase(), "string", getPackageName());
		fillTextView(R.id.mandate_status, getString(id));
		fillTextView(R.id.mandate_account_name, mAccount.getAccountType());
		fillTextView(R.id.mandate_account_iban, mAccount.getAccountIban());
		fillTextView(R.id.mandate_account_value, CurrencyUtil.formatMoney(mAccount.getAmount(), mAccount.getCurrencyId()));
		fillTextView(R.id.mandate_beneficiary_name, mMandate.getOwnerText());
		fillTextView(R.id.mandate_beneficiary_id, mMandate.getCreditorId());
		
		LinearLayout buttonContainer = (LinearLayout)findViewById(R.id.button_container);
		
		LinearLayout buttonLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.pmt_payment_warning_button, buttonContainer, false);
		Button button = (Button)buttonLayout.findViewById(R.id.styled_button);
		button.setText(R.string.direct_debit_button_cancel);
		button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DirectDebitDetails.this);
				alertDialogBuilder.setTitle(R.string.pmt_please_confirm)
					.setMessage(R.string.pmt_approve_confirm_direct_debit_cancel)
					.setNegativeButton(R.string.avq_no, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							
						}
					})
					.setPositiveButton(R.string.avq_yes, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							DirectDebitMandateRequest request = DirectDebitMandateOverviewService.cancelDirectDebitMandates(mMandate.getId(), new RequestStateEvent<DirectDebitMandateRequest>(){
								@Override
								public void onRequestCompleted(DirectDebitMandateRequest aRequest) {
									// TODO Auto-generated method stub
								}
								
								@Override
								public void onRequestFailed(DirectDebitMandateRequest aRequest) {
									// TODO Auto-generated method stub
								}
							});
							
							request.setCachePolicy(CachePolicy.NO_CACHE);
							request.initiateServerRequest();
						}
					}).create().show();
			}
		});
		buttonContainer.addView(buttonLayout);
	}
	
	protected void fillTextView(int resId, String text){
		TextView view = (TextView)findViewById(resId);
		if (view != null)
			view.setText(text);
	}
}
