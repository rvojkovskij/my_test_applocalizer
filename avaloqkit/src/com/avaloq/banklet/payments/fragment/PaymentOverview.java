package com.avaloq.banklet.payments.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.banklet.payments.adapter.PaymentTypeAdapter;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.ui.BankletFragment;

public class PaymentOverview extends BankletFragment {
	
	public static final String EXTRA_IS_TEMPLATE_OVERVIEW = "EXTRA_IS_TEMPLATE_OVERVIEW";
	public static final String EXTRA_IS_STANDING_ORDER = "EXTRA_IS_STANDING_ORDER";
	
	private boolean mIsTemplateOverview = false;
	private boolean mIsStandingOrder = false;
	
	public void setIsTemplateOverview(boolean isTemplateOverview){
		mIsTemplateOverview = isTemplateOverview;
	}
	
	public boolean isTemplateOverview(){
		return mIsTemplateOverview;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (getArguments() != null){
			mIsTemplateOverview = getArguments().getBoolean(EXTRA_IS_TEMPLATE_OVERVIEW);
			mIsStandingOrder = getArguments().getBoolean(EXTRA_IS_STANDING_ORDER, false);
		}
		return inflater.inflate(R.layout.pmt_fragment_overview, container, false);
	}
	
	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ViewPager pager = (ViewPager)view.findViewById(R.id.pager);
		pager.setOffscreenPageLimit(10);
		
		OrderType orderType = OrderType.PAYMENT;
		if (isTemplateOverview())
			orderType = OrderType.TEMPLATE;
		else if (mIsStandingOrder)
			orderType = OrderType.STANDING;
		
		if (BankletActivityDelegate.isLargeDevice(getActivity())){
			pager.setAdapter(new PaymentTypeAdapter(getActivity(), pager, orderType));
		}
		else {
			pager.setVisibility(View.GONE);
		}
		
		FragmentManager manager;
		
		if (getChildFragmentManager() == null){
			manager = getFragmentManager();
		}
		else
			manager = getChildFragmentManager();
		
		if(manager.findFragmentById(R.id.pmt_overview_favorite_templates) == null) {
			if (isTemplateOverview()){
				// In the Templates tab
				manager.beginTransaction().add(R.id.pmt_overview_favorite_templates, new Templates()).commit();
			} else if (mIsStandingOrder){
				// In the Standing Orders tab
				manager.beginTransaction().add(R.id.pmt_overview_favorite_templates, new StandingOrders()).commit();
			} else {
				// In the Single Payments tab
				manager.beginTransaction().add(R.id.pmt_overview_favorite_templates, new SinglePayment()).commit();
				//manager.beginTransaction().add(R.id.pmt_overview_favorite_templates, new FavoriteTemplates()).commit();
			}
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		FragmentManager manager;
		
		if (getChildFragmentManager() == null){
			manager = getFragmentManager();
		}
		else
			manager = getChildFragmentManager();
		
		BankletFragment frag = (BankletFragment)manager.findFragmentById(R.id.pmt_overview_favorite_templates);
		
		if (requestCode == SearchView.RESULT_CODE && frag != null)
			frag.onActivityResult(requestCode, resultCode, data);
	}

}