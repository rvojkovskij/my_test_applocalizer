package com.avaloq.banklet.payments.fragment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;

import com.avaloq.afs.aggregation.to.payment.PaymentInfo;
import com.avaloq.afs.aggregation.to.payment.PaymentInfoListResult;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentQueryTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentSearchStateType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.adapter.PaymentListAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentInfoListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.tools.searchwidget.AccountCriterium;
import com.avaloq.framework.tools.searchwidget.AmountCriterium;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.DateCriterium;
import com.avaloq.framework.tools.searchwidget.MultipleChoiceCriterium;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;

public class PaymentList extends AbstractLoadingPaymentFragment {
	
	PaymentQueryTO query;
	SearchView search;
	String lastRequestId = "";
	ProgressiveListView listView;
	
	@Override
	public int getLayoutId() {
		return R.layout.pmt_fragment_payment_list;
	}
	
	@Override
	protected void doInitialLayout(View view) {
		search = (SearchView)getView().findViewById(R.id.search_widget);
		search.init(this);
		search.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				query = new PaymentQueryTO();
				for (Criterium crit: search.getCriteria())
					crit.addToQuery();
				query.setBeneficiary(search.getText());
				listView.refresh();
			}
		});
		
		search.addCriterium(new AccountCriterium(getActivity()){
			@Override
			public void addToQuery() {
				query.getMoneyAccountIdList().clear();
				for (MoneyAccountTO account: mElements)
					query.getMoneyAccountIdList().add(account.getId());
			}
		});
		
		search.addCriterium(new MultipleChoiceCriterium<PaymentType>(getActivity()){
			@Override
			public LinkedHashMap<PaymentType, String> getTypeLabelMapping() {
				LinkedHashMap<PaymentType, String> map = new LinkedHashMap<PaymentType, String>();
				map.put(PaymentType.SWISS_ORANGE_PAYMENT_SLIP, getResources().getString(R.string.pmt_overview_orange_slip));
				map.put(PaymentType.SWISS_RED_PAYMENT_SLIP, getResources().getString(R.string.pmt_overview_red_slip));
				map.put(PaymentType.INTERNAL_PAYMENT, getResources().getString(R.string.pmt_overview_account_transfer));
				map.put(PaymentType.DOMESTIC_PAYMENT, getResources().getString(R.string.pmt_overview_domestic));
				map.put(PaymentType.INTERNATIONAL_PAYMENT, getResources().getString(R.string.pmt_overview_international));
				return map;
			}

			@Override
			public void addToQuery() {
				query.getPaymentTypeList().clear();
				for (PaymentType type: mElements)
					query.getPaymentTypeList().add(type);
			}

			@Override
			public String getName() {
				return getResources().getString(R.string.criterium_payment_type);
			}
		});
	
	
		search.addCriterium(new AmountCriterium(getActivity()){
			@Override
			public void addToQuery() {
				if (mFrom != 0)
					query.setFromAmount(BigDecimal.valueOf(mFrom));
				if (mTo != 0)
					query.setToAmount(BigDecimal.valueOf(mTo));
			}
		});
		
		search.addCriterium(new DateCriterium(getActivity()){
			@Override
			public void addToQuery() {
				if (mFrom != null)
					query.setFromDate(mFrom);
				if (mTo != null)
					query.setToDate(mTo);
			}
		});
		
		search.addCriterium(new SingleChoiceCriterium<PaymentSearchStateType>(getActivity()) {

			@Override
			public LinkedHashMap<PaymentSearchStateType, String> getTypeLabelMapping() {
				LinkedHashMap<PaymentSearchStateType, String> map =  new LinkedHashMap<PaymentSearchStateType, String>();
				map.put(PaymentSearchStateType.TO_BE_APPROVED, getString(R.string.payment_state_type_to_be_approved));
				map.put(PaymentSearchStateType.ERROR, getString(R.string.payment_state_type_error));
				map.put(PaymentSearchStateType.APPROVED, getString(R.string.payment_state_type_approved));
				map.put(PaymentSearchStateType.PARTIAL_APPROVED, getString(R.string.payment_state_type_partial_approve));
				map.put(PaymentSearchStateType.PROCESSED, getString(R.string.payment_state_type_processed));
				map.put(PaymentSearchStateType.REJECTED, getString(R.string.payment_state_type_rejected));
				map.put(PaymentSearchStateType.DELETED, getString(R.string.payment_state_type_deleted));
				return map;
			}

			@Override
			public void addToQuery() {
				query.setPaymentSearchStateType(mElement);
			}

			@Override
			public String getName() {
				return getString(R.string.criterium_state);
			}
		});
		
		listView = (ProgressiveListView)getView().findViewById(R.id.pmt_payment_list_list);
		if(BankletActivityDelegate.isLargeDevice(getActivity())) {
			listView.addHeaderView(LayoutInflater.from(getActivity()).inflate(R.layout.pmt_row_payment_header, listView, false));
		}
		listView.setAdapter(new PaymentListAdapter(getActivity(), listView, new ArrayList<PaymentInfo>()));
		
		listView.init(new Runnable(){
			@Override
			public void run() {
				if (!search.areAllCriteriaReady())
					return;
				PaymentInfoListRequest request = PaymentOverviewService.getPayments(query, (long)listView.getStartIndex(), (long)listView.getEndIndex(), new RequestStateEvent<PaymentInfoListRequest>() {
					@Override
					public void onRequestCompleted(PaymentInfoListRequest aRequest) {
						if (getActivity() != null){
							PaymentInfoListResult result = aRequest.getResponse().getData();
							if (result == null) {
								onRequestFailed(aRequest);
								return;
							}
							if (aRequest.getRequestIdentifier().equals(lastRequestId)){
								listView.setElements(result.getPaymentInfoList());
								setContentReady();
							}
						}
					}
					
					@Override
					public void onRequestFailed(PaymentInfoListRequest aRequest) {
						showError(R.string.avq_error_no_connection);
					}
				});
				lastRequestId = request.getRequestIdentifier();
				request.initiateServerRequest();
			}
			
		});
		
	}
	
	@Override
	public void loadData() {
		
	}
	
	@Override
	protected boolean refreshOnResume(){
		return true;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			search.onActivityResult(requestCode, resultCode, data);
	}
}
