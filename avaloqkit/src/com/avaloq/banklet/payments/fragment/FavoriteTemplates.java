package com.avaloq.banklet.payments.fragment;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfoListResult;
import com.avaloq.afs.server.bsp.client.ws.PaymentTemplateQueryTO;
import com.avaloq.banklet.payments.TemplatesActivity;
import com.avaloq.banklet.payments.adapter.PaymentTemplateAdapter;
import com.avaloq.banklet.payments.adapter.PaymentTemplateListAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoListRequest;
import com.viewpagerindicator.CirclePageIndicator;

public class FavoriteTemplates extends AbstractLoadingPaymentFragment {

	private static final String TAG = FavoriteTemplates.class.getSimpleName();
	private static final int RESULT_CODE = 1;
	public static final String EXTRA_IS_REFRESH_NEEDED = "extra_is_refresh_needed";
	private boolean disallowCache = false;
	
	/*public FavoriteTemplates() {
		super();
		setRetainInstance(true);
	}*/

	@Override
	public int getLayoutId() {
		return R.layout.pmt_fragment_favorite_templates;
	}

	@Override
	public void loadData() {
		View showAllRow = getView().findViewById(R.id.pmt_favorite_titlebar);
		showAllRow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivityForResult(new Intent(getActivity(), TemplatesActivity.class), RESULT_CODE);
			}
		});
		
		final ViewPager viewPager = (ViewPager) getView().findViewById(R.id.pmt_favorite_templates_pager);
		final ListView listView = (ListView) getView().findViewById(R.id.pmt_favorite_templates_pager_list);
		if (viewPager != null){
			viewPager.setSaveEnabled(false);
			CirclePageIndicator circlePagerIndicator = (CirclePageIndicator) getView().findViewById(R.id.pmt_favorite_templates_pager_indicator);
			viewPager.setAdapter(PaymentTemplateAdapter.EMPTY);
			circlePagerIndicator.setViewPager(viewPager);
		}
		
		PaymentTemplateQueryTO query = new PaymentTemplateQueryTO();
		query.setFavorite(true);
		PaymentTemplateInfoListRequest request = PaymentOverviewService.getPaymentTemplates(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<PaymentTemplateInfoListRequest>() {
			@Override
			public void onRequestCompleted(PaymentTemplateInfoListRequest aRequest) {
				PaymentTemplateInfoListResult result = aRequest.getResponse().getData();
				List<PaymentTemplateInfo> templates = result.getPaymentTemplateInfoList();
				if (viewPager != null)
					viewPager.setAdapter(new PaymentTemplateAdapter(getChildFragmentManager(), templates));
				if (listView != null && getActivity() != null)
					listView.setAdapter(new PaymentTemplateListAdapter(getActivity(), templates));
				setContentReady();
			}

			@Override
			public void onRequestFailed(PaymentTemplateInfoListRequest aRequest) {
				showError(R.string.avq_error_no_connection);
			}
		});
		
		/*if (disallowCache){
			disallowCache = false;
			request.setCachePolicy(CachePolicy.REFRESH_CACHE);
		}*/
		request.setCachePolicy(CachePolicy.REFRESH_CACHE);
		request.initiateServerRequest();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULT_CODE) {
			if(resultCode == Activity.RESULT_OK){
				if (data.hasExtra(EXTRA_IS_REFRESH_NEEDED)){
					if (data.getBooleanExtra(EXTRA_IS_REFRESH_NEEDED, false)){
						disallowCache = true;
						loadData();
					}
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}
	
	@Override
	protected boolean refreshOnResume() {
		return true;
	}

}
