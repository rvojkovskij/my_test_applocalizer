package com.avaloq.banklet.payments.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfoListResult;
import com.avaloq.afs.server.bsp.client.ws.PaymentTemplateQueryTO;
import com.avaloq.banklet.payments.AccountTransferTemplateActivity;
import com.avaloq.banklet.payments.DomesticPaymentTemplateActivity;
import com.avaloq.banklet.payments.InternationalPaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissRedPaymentTemplateActivity;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.adapter.PaymentTemplateFullAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoListRequest;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.SearchView;

public class Templates extends AbstractLoadingPaymentFragment {

	private static final String TAG = Templates.class.getSimpleName();
	private static final int RESULT_CODE = 1;
	public static final String EXTRA_IS_REFRESH_NEEDED = "extra_is_refresh_needed";
	private boolean disallowCache = false;
	protected PaymentTemplateFullAdapter mAdapter;
	
	PaymentTemplateQueryTO query = createEmptyQuery();
	SearchView search;
	String lastRequestId = "";

	@Override
	public int getLayoutId() {
		return R.layout.pmt_templates_fragment;
	}
	
	protected PaymentTemplateQueryTO createEmptyQuery(){
		PaymentTemplateQueryTO q = new PaymentTemplateQueryTO();
		q.setFavorite(false);
		return q;
	}
	
	@Override
	protected void doInitialLayout(View view) {
		final ProgressiveListView list = (ProgressiveListView) getView().findViewById(R.id.templates_list);
		if (BankletActivityDelegate.isSmallDevice(getActivity())){
			View header = PaymentUtil.getOverviewHeader(getActivity(), list, OrderType.TEMPLATE);
			search = (SearchView)header.findViewById(R.id.search_widget);
			list.addHeaderView(header);
		}
		else {
			search = (SearchView)getView().findViewById(R.id.search_widget);
			search.setVisibility(View.VISIBLE);
		}
		
		mAdapter = new PaymentTemplateFullAdapter(getActivity(), list, new ArrayList<PaymentTemplateInfo>());
		list.setAdapter(mAdapter);
		list.init(new Runnable() {
			
			@Override
			public void run() {
				PaymentTemplateInfoListRequest request = PaymentOverviewService.getPaymentTemplates(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<PaymentTemplateInfoListRequest>() {
					@Override
					public void onRequestCompleted(PaymentTemplateInfoListRequest aRequest) {
						if (getActivity() != null){
							PaymentTemplateInfoListResult result = aRequest.getResponse().getData();
							List<PaymentTemplateInfo> templates = result.getPaymentTemplateInfoList();
							
							// reorder the list such that all the favorite templates are on top
							List<PaymentTemplateInfo> templatesTemp = new ArrayList<PaymentTemplateInfo>();
							int favoriteindex = 0;
							for (PaymentTemplateInfo paymentTemplateInfo : templates) {
								if (paymentTemplateInfo.getFavorite()){
									templatesTemp.add(favoriteindex, paymentTemplateInfo);
									favoriteindex++;
								}else{
									templatesTemp.add(paymentTemplateInfo);
								}
							}
							
							templates = templatesTemp;
							
							if (aRequest.getRequestIdentifier().equals(lastRequestId))
								list.setElements(templates);
							
							list.setOnItemClickListener(new OnItemClickListener() {
	
								@Override
								public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
									
									PaymentTemplateInfo template = mAdapter.getItem(position);
									Intent intent;
									switch(template.getPaymentType()) {
									case SWISS_ORANGE_PAYMENT_SLIP:
										intent = new Intent(getActivity(), SwissOrangePaymentTemplateActivity.class);
										intent.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
										intent.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
										startActivity(intent);
										break;
									case SWISS_RED_PAYMENT_SLIP:
										intent = new Intent(getActivity(), SwissRedPaymentTemplateActivity.class);
										intent.putExtra(SwissRedPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
										intent.putExtra(SwissRedPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
										startActivity(intent);
										break;
									case DOMESTIC_PAYMENT:
										intent = new Intent(getActivity(), DomesticPaymentTemplateActivity.class);
										intent.putExtra(DomesticPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
										intent.putExtra(DomesticPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
										startActivity(intent);
										break;
									case INTERNAL_PAYMENT:
										intent = new Intent(getActivity(), AccountTransferTemplateActivity.class);
										intent.putExtra(AccountTransferTemplateActivity.EXTRA_ID, template.getPaymentId());
										intent.putExtra(AccountTransferTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
										startActivity(intent);
										break;
									case INTERNATIONAL_PAYMENT:
										intent = new Intent(getActivity(), InternationalPaymentTemplateActivity.class);
										intent.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
										intent.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
										startActivity(intent);
										break;
									default:
										break;
									}
								}
							});
						}
					}

					@Override
					public void onRequestFailed(PaymentTemplateInfoListRequest aRequest) {
						//TODO: Show Error
					}
				});
				lastRequestId = request.getRequestIdentifier();
				request.initiateServerRequest();
				
			}
		});
		
		search.init(this);
		search.setOnCriteriaChangedCallback(new Runnable() {
			@Override
			public void run() {
				query = createEmptyQuery();
				for (Criterium crit: search.getCriteria())
					crit.addToQuery();
				query.setAliasOrBeneficiary(search.getText());
				list.refresh();
			}
		});
		if (search.areAllCriteriaReady())
			search.initiateSearch();
		
		
	}

	@Override
	public void loadData() {
		setContentReady();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULT_CODE) {
			if(resultCode == Activity.RESULT_OK){
				if (data.hasExtra(EXTRA_IS_REFRESH_NEEDED)){
					if (data.getBooleanExtra(EXTRA_IS_REFRESH_NEEDED, false)){
						disallowCache = true;
						loadData();
					}
				}
			}
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy");
	}

}
