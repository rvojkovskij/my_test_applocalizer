package com.avaloq.banklet.payments.fragment;

import java.util.List;

import android.view.View;
import android.widget.ListView;

import com.avaloq.afs.server.bsp.client.ws.DirectDebitMandateQueryTO;
import com.avaloq.afs.server.bsp.client.ws.DirectDebitMandateTO;
import com.avaloq.banklet.payments.adapter.DirectDebitListAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.directdebitmandateoverview.DirectDebitMandateListRequest;
import com.avaloq.framework.comms.webservice.directdebitmandateoverview.DirectDebitMandateOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMoneyAccountListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;

public class DirectDebitAgreements extends AbstractLoadingPaymentFragment {
	
	@Override
	public int getLayoutId() {
		return R.layout.pmt_fragment_direct_debit;
	}
	
	@Override
	public void loadData() {
		DirectDebitMandateQueryTO query = new DirectDebitMandateQueryTO();
		DirectDebitMandateOverviewService.getDirectDebitMandates(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<DirectDebitMandateListRequest>() {
			@Override
			public void onRequestCompleted(DirectDebitMandateListRequest aRequest) {
				final List<DirectDebitMandateTO> payments = aRequest.getResponse().getData().getDirectDebitMandateList();
				PaymentOverviewService.getPaymentMoneyAccounts(new RequestStateEvent<PaymentMoneyAccountListRequest>() {
					@Override
					public void onRequestCompleted(PaymentMoneyAccountListRequest aRequest) {
						View view = getView();
						if(view != null) {
							ListView listView = (ListView)view.findViewById(R.id.pmt_direct_debit_list);
							listView.setAdapter(new DirectDebitListAdapter(getActivity(), payments, aRequest.getResponse().getData().getMoneyAccounts()));
							setContentReady();
							setNoResultsMessage(getSherlockActivity().getString(R.string.pmt_no_direct_debits));
							setNoResultsMessageVisibility( (payments == null || payments.size() == 0) );
						}
					}
					public void onRequestFailed(PaymentMoneyAccountListRequest aRequest) {
						showError(R.string.avq_error_no_connection);
					};
				}).initiateServerRequest();
			}
			@Override
			public void onRequestFailed(DirectDebitMandateListRequest aRequest) {
				showError(R.string.avq_error_no_connection);
			}
		}).initiateServerRequest();
	}

}
