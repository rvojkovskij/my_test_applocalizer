package com.avaloq.banklet.payments.fragment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.avaloq.afs.aggregation.to.payment.StandingPaymentInfo;
import com.avaloq.afs.aggregation.to.payment.StandingPaymentInfoListResult;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.StandingOrderQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StandingOrderSearchStateType;
import com.avaloq.banklet.payments.adapter.StandingOrderListAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.StandingPaymentInfoListRequest;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.tools.searchwidget.AccountCriterium;
import com.avaloq.framework.tools.searchwidget.AmountCriterium;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.MultipleChoiceCriterium;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;
import com.avaloq.framework.ui.BankletFragment;

public class StandingOrders extends AbstractLoadingPaymentFragment {
	
	StandingOrderQueryTO query;
	SearchView search;
	String lastRequestId = "";
	
	@Override
	public int getLayoutId() {
		return R.layout.pmt_fragment_standing_orders;
	}
	
	@Override
	protected void doInitialLayout(View view) {
		final ProgressiveListView listView = (ProgressiveListView)getView().findViewById(R.id.pmt_standing_orders_list);
		if (BankletActivityDelegate.isSmallDevice(getActivity())){
			View header = PaymentUtil.getOverviewHeader(getActivity(), listView, OrderType.STANDING);
			search = (SearchView)header.findViewById(R.id.search_widget);
			listView.addHeaderView(header);
		}
		else {
			search = (SearchView)getView().findViewById(R.id.search_widget);
			search.setVisibility(View.VISIBLE);
		}
		
		
		listView.setAdapter(new StandingOrderListAdapter(getActivity(), listView, new ArrayList<StandingPaymentInfo>()));
		listView.init(new Runnable(){
			@Override
			public void run() {
				StandingPaymentInfoListRequest request = PaymentOverviewService.getStandingOrders(query, (long)listView.getStartIndex(), (long)listView.getEndIndex(), new RequestStateEvent<StandingPaymentInfoListRequest>() {
					@Override
					public void onRequestCompleted(StandingPaymentInfoListRequest aRequest) {
						if (getActivity() != null){
							StandingPaymentInfoListResult result = aRequest.getResponse().getData();
							if (result == null) {
								onRequestFailed(aRequest);
								return;
							}
							if (aRequest.getRequestIdentifier().equals(lastRequestId))
								listView.setElements(result.getstandingPaymentInfoList());
						}
					}
					
					@Override
					public void onRequestFailed(StandingPaymentInfoListRequest aRequest) {
						showError(R.string.avq_error_no_connection);
					}
				});
				lastRequestId = request.getRequestIdentifier();
				//request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
			}
		});
		
		search.init((BankletFragment)getParentFragment());
		search.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				query = new StandingOrderQueryTO();
				for (Criterium crit: search.getCriteria())
					crit.addToQuery();
				query.setBeneficiary(search.getText());
				listView.refresh();
			}
		});
		
		search.addCriterium(new AccountCriterium(getActivity()){
			@Override
			public void addToQuery() {
				query.getMoneyAccountIdList().clear();
				for (MoneyAccountTO account: mElements)
					query.getMoneyAccountIdList().add(account.getId());
			}
		});
		
		search.addCriterium(new MultipleChoiceCriterium<PaymentType>(getActivity()){
			@Override
			public LinkedHashMap<PaymentType, String> getTypeLabelMapping() {
				LinkedHashMap<PaymentType, String> map = new LinkedHashMap<PaymentType, String>();
				map.put(PaymentType.SWISS_ORANGE_PAYMENT_SLIP, getResources().getString(R.string.pmt_overview_orange_slip));
				map.put(PaymentType.SWISS_RED_PAYMENT_SLIP, getResources().getString(R.string.pmt_overview_red_slip));
				map.put(PaymentType.INTERNAL_PAYMENT, getResources().getString(R.string.pmt_overview_account_transfer));
				map.put(PaymentType.DOMESTIC_PAYMENT, getResources().getString(R.string.pmt_overview_domestic));
				map.put(PaymentType.INTERNATIONAL_PAYMENT, getResources().getString(R.string.pmt_overview_international));
				return map;
			}

			@Override
			public void addToQuery() {
				query.getPaymentTypeList().clear();
				for (PaymentType type: mElements)
					query.getPaymentTypeList().add(type);
			}

			@Override
			public String getName() {
				return getResources().getString(R.string.criterium_payment_type);
			}
		});
	
	
		search.addCriterium(new AmountCriterium(getActivity()){
			@Override
			public void addToQuery() {
				if (mFrom != 0)
					query.setFromAmount(BigDecimal.valueOf(mFrom));
				if (mTo != 0)
					query.setToAmount(BigDecimal.valueOf(mTo));
			}
		});
		
		search.addCriterium(new SingleChoiceCriterium<StandingOrderSearchStateType>(getActivity()) {

			@Override
			public LinkedHashMap<StandingOrderSearchStateType, String> getTypeLabelMapping() {
				LinkedHashMap<StandingOrderSearchStateType, String> map =  new LinkedHashMap<StandingOrderSearchStateType, String>();
				map.put(StandingOrderSearchStateType.TO_BE_APPROVED, getString(R.string.payment_state_type_deleted));
				map.put(StandingOrderSearchStateType.ACTIVE, getString(R.string.payment_state_type_active));
				map.put(StandingOrderSearchStateType.INACTIVE, getString(R.string.payment_state_type_inactive));
				map.put(StandingOrderSearchStateType.PARTIAL_APPROVED, getString(R.string.payment_state_type_partial_approve));
				map.put(StandingOrderSearchStateType.REJECTED, getString(R.string.payment_state_type_rejected));
				map.put(StandingOrderSearchStateType.DELETED, getString(R.string.payment_state_type_deleted));
				map.put(StandingOrderSearchStateType.EXPIRED, getString(R.string.payment_state_type_expired));
				return map;
			}

			@Override
			public void addToQuery() {
				query.setStandingOrderSearchStateType(mElement);
			}

			@Override
			public String getName() {
				return getString(R.string.criterium_state);
			}
		});
	}
	
	@Override
	public void loadData() {
		setContentReady();
		/*StandingOrderQueryTO query = new StandingOrderQueryTO();
		StandingPaymentInfoListRequest request = PaymentOverviewService.getStandingOrders(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<StandingPaymentInfoListRequest>() {
			public void onRequestCompleted(StandingPaymentInfoListRequest aRequest) {
				List<StandingPaymentInfo> payments = aRequest.getResponse().getData().getstandingPaymentInfoList();
				
				if (getView() != null){
					ListView listView = (ListView)getView().findViewById(R.id.pmt_standing_orders_list);
					listView.setAdapter(new StandingOrderListAdapter(getActivity(), payments));
					setContentReady();
					setNoResultsMessage(getSherlockActivity().getString(R.string.pmt_no_standing_orders));
					setNoResultsMessageVisibility( (payments == null || payments.size() == 0) );
				}
			};
			public void onRequestFailed(StandingPaymentInfoListRequest aRequest) {
				showError(R.string.avq_error_no_connection);
			};
		});
		request.setCachePolicy(CachePolicy.NO_CACHE);
		request.initiateServerRequest();*/
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			search.onActivityResult(requestCode, resultCode, data);
	}

}
