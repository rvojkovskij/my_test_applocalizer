package com.avaloq.banklet.payments.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avaloq.banklet.payments.HolderActivity;
import com.avaloq.banklet.payments.PaymentBanklet;
import com.avaloq.banklet.payments.adapter.GriddableViewPagerAdapter;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class StartupSmallDevice extends BankletFragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.pmt_startup_small, container, false);
		ViewPager pager = (ViewPager)view.findViewById(R.id.pager);
		
		if (PaymentBanklet.getInstance().hasPendingApprovals()){
			View pendingApprovalsContainer = view.findViewById(R.id.pmt_pending_container);
			TextView pendingApprovals = (TextView) view.findViewById(R.id.pmt_pending);
			
			pendingApprovals.setText(Integer.toString(PaymentBanklet.getInstance().getPendingApprovalsNumber()));
			pendingApprovalsContainer.setVisibility(View.VISIBLE);
			pendingApprovalsContainer.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), HolderActivity.class);
					intent.putExtra(HolderActivity.EXTRA_ACTION_TYPE, R.id.pmt_pending);
					startActivity(intent);
				}
			});
		}
		
		int width = getActivity().getResources().getInteger(R.integer.payment_actions_width);
		int height = getActivity().getResources().getInteger(R.integer.payment_actions_height);
		int buttonHeight = (int)getActivity().getResources().getDimension(R.dimen.payment_action_button_height);
		pager.setAdapter(new GriddableViewPagerAdapter(getActivity(), R.array.payment_actions_buttons, width, height, buttonHeight, pager){
			@Override
			protected View prepareView(final int resId, View view) {
				view.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getActivity(), HolderActivity.class);
						intent.putExtra(HolderActivity.EXTRA_ACTION_TYPE, resId);
						mActivity.startActivity(intent);
					}
				});
				return view;
			}
		});
		return view;
	}
}
