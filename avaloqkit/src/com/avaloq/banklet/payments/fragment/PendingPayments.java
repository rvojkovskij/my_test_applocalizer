package com.avaloq.banklet.payments.fragment;

import android.os.Bundle;
import android.view.View;

import com.avaloq.banklet.payments.PaymentBanklet;
import com.avaloq.banklet.payments.adapter.PendingPaymentsAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.ProgressiveListView;

public class PendingPayments extends AbstractLoadingPaymentFragment {

	@Override
	public int getLayoutId() {
		return R.layout.pmt_pending_payments;
	}
	
	@Override
	protected void doInitialLayout(View view) {
		ProgressiveListView list = (ProgressiveListView)view.findViewById(R.id.pmt_pending_list);
		if (list == null) return;
		list.setAdapter(new PendingPaymentsAdapter(getActivity(), list, PaymentBanklet.getInstance().getPengingPayments(), PaymentBanklet.getInstance().getPendingStandingOrders()));
	}

	@Override
	public void loadData() {
		setContentReady();
	}

}
