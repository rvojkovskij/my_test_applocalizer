package com.avaloq.banklet.payments.fragment;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.afs.server.bsp.client.ws.PaymentTemplateQueryTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.AccountTransferTemplateActivity;
import com.avaloq.banklet.payments.DomesticPaymentTemplateActivity;
import com.avaloq.banklet.payments.InternationalPaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissRedPaymentTemplateActivity;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoListRequest;
import com.avaloq.framework.ui.BankletFragment;

public class FavoriteTemplatesPage extends BankletFragment {

	private static final String TAG = FavoriteTemplatesPage.class.getSimpleName();

	private static final String ARGUMENT_TEMPLATES = "templates";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		setRetainInstance(false);
		return inflater.inflate(R.layout.pmt_fragment_favorite_templates_page, container, false);
	}

	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d(TAG, "onViewCreated");
		PaymentTemplateQueryTO query = new PaymentTemplateQueryTO();
		query.setFavorite(true);
		PaymentOverviewService.getPaymentTemplates(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<PaymentTemplateInfoListRequest>() {
			@Override
			public void onRequestCompleted(PaymentTemplateInfoListRequest aRequest) {
				Log.d(TAG, "onRequestCompleted");
				int[] templateIndexes = getArguments().getIntArray(ARGUMENT_TEMPLATES);
				if (getActivity()== null)
					return;
				for(int i = 1;i <= 9; i++) {
					if(i <= templateIndexes.length) {
						final PaymentTemplateInfo template = aRequest.getResponse().getData().getPaymentTemplateInfoList().get(templateIndexes[i-1]);
						((TextView)view.findViewById(findId("pmt_favorite_templates_alias_" + i))).setText(template.getAlias());
						((TextView)view.findViewById(findId("pmt_favorite_templates_debit_account_label_" + i))).setText(template.getDebitAccountLabel());
						((TextView)view.findViewById(findId("pmt_favorite_templates_amount_" + i))).setText(template.getAmount().toString());
						((ImageView)view.findViewById(findId("pmt_favorite_templates_icon_" + i))).setImageResource(PaymentUtil.getPaymentDrawable(template.getPaymentType()));

						view.findViewById(findId("pmt_favorite_templates_container_" + i)).setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View view) {
								Log.d("DDD", template.getPaymentType().name());
								Intent i;
								switch(template.getPaymentType()) {
								
								case SWISS_ORANGE_PAYMENT_SLIP:									
									i = new Intent(getActivity(), SwissOrangePaymentTemplateActivity.class);
									i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
									i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
									startActivity(i);
									break;
								case SWISS_RED_PAYMENT_SLIP:
									i = new Intent(getActivity(), SwissRedPaymentTemplateActivity.class);
									i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
									i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
									startActivity(i);
									break;
								case DOMESTIC_PAYMENT:
									i = new Intent(getActivity(), DomesticPaymentTemplateActivity.class);
									i.putExtra(DomesticPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
									i.putExtra(DomesticPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
									startActivity(i);									
									break;
								case INTERNAL_PAYMENT:
									i = new Intent(getActivity(), AccountTransferTemplateActivity.class);
									i.putExtra(AccountTransferTemplateActivity.EXTRA_ID, template.getPaymentId());
									i.putExtra(AccountTransferTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
									startActivity(i);
									break;									
								case INTERNATIONAL_PAYMENT:
									i = new Intent(getActivity(), InternationalPaymentTemplateActivity.class);
									i.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
									i.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
									startActivity(i);									
									break;								
								default:
									break;
								}
							}
						});
					} else {
						((TextView)view.findViewById(findId("pmt_favorite_templates_alias_" + i))).setText("");
						((TextView)view.findViewById(findId("pmt_favorite_templates_debit_account_label_" + i))).setText("");
						((TextView)view.findViewById(findId("pmt_favorite_templates_amount_" + i))).setText("");
						// TODO set the "none" icon, or hide the views or smth
						((ImageView)view.findViewById(findId("pmt_favorite_templates_icon_" + i))).setVisibility(View.GONE);
						view.findViewById(findId("pmt_favorite_templates_container_" + i)).setClickable(false);
					}
				}
			}
		}).initiateServerRequest();
	}

	public static final FavoriteTemplatesPage newInstance(ArrayList<Integer> templates) {
		int[] intList = new int[templates.size()];
		for(int i = 0;i < templates.size();i++) {
			intList[i] = templates.get(i);
		}
		FavoriteTemplatesPage instance = new FavoriteTemplatesPage();
		Bundle arguments = new Bundle();
		arguments.putIntArray(ARGUMENT_TEMPLATES, intList);
		instance.setArguments(arguments);
		return instance;
	}

	private int findId(String idName) {
		return getActivity().getResources().getIdentifier(idName, "id", getActivity().getPackageName());
	}

}
