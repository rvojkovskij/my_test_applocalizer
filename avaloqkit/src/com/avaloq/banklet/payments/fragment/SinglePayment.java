package com.avaloq.banklet.payments.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.view.View;

import com.avaloq.afs.aggregation.to.payment.PaymentMaskListResult;
import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentMaskQueryTO;
import com.avaloq.banklet.payments.adapter.SinglePaymentListAdapter;
import com.avaloq.banklet.payments.util.AbstractLoadingPaymentFragment;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMaskListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.ui.BankletFragment;

public class SinglePayment extends AbstractLoadingPaymentFragment {
	
	PaymentMaskQueryTO query;
	SearchView search;
	String lastRequestId = "";
	
	@Override
	public int getLayoutId() {
		return R.layout.pmt_fragment_standing_orders;
	}
	
	@Override
	protected void doInitialLayout(View view) {
		final ProgressiveListView listView = (ProgressiveListView)getView().findViewById(R.id.pmt_standing_orders_list);
		if (BankletActivityDelegate.isSmallDevice(getActivity())){
			View header = PaymentUtil.getOverviewHeader(getActivity(), listView, OrderType.PAYMENT);
			search = (SearchView)header.findViewById(R.id.search_widget);
			listView.addHeaderView(header);
		}
		else {
			search = (SearchView)getView().findViewById(R.id.search_widget);
			search.setVisibility(View.VISIBLE);
		}
		
		listView.setAdapter(new SinglePaymentListAdapter(getActivity(), listView, new ArrayList<PaymentMaskWrapperTO>()));
		
		listView.init(new Runnable(){
			@Override
			public void run() {
				query.setMaxResultSize(listView.getEndIndex());
				PaymentMaskListRequest request = PaymentOverviewService.getPaymentMaskList(query, (long)listView.getStartIndex(), (long)listView.getEndIndex(), new RequestStateEvent<PaymentMaskListRequest>() {
					@Override
					public void onRequestCompleted(PaymentMaskListRequest aRequest) {
						if (getActivity() != null){
							PaymentMaskListResult result = aRequest.getResponse().getData();
							if (result == null) {
								onRequestFailed(aRequest);
								return;
							}
							
							List<PaymentMaskWrapperTO> list = new ArrayList<PaymentMaskWrapperTO>();
							if (result.getMaskList() != null){
								for (PaymentMaskWrapperTO wrapper: result.getMaskList()){
									list.add(wrapper);
								}
							}
							if (result.getSuggestionList() != null){ 
								for (PaymentMaskWrapperTO wrapper: result.getSuggestionList()){
									list.add(wrapper);
								}
							}	
							if (aRequest.getRequestIdentifier().equals(lastRequestId)){
								listView.setElements(list);
								//listView.setAdapter(new SinglePaymentListAdapter(getActivity(), listView, list));
								setContentReady();
							}
						}
					}
					
					@Override
					public void onRequestFailed(PaymentMaskListRequest aRequest) {
						showError(R.string.avq_error_no_connection);
					}
				});
				lastRequestId = request.getRequestIdentifier();
				//request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
			}
		});
		
		search.init((BankletFragment)getParentFragment());
		search.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				query = new PaymentMaskQueryTO();
				for (Criterium crit: search.getCriteria())
					crit.addToQuery();
				query.setMaxResultSize(listView.getElementsPerPage());
				query.setSearchText(search.getText());
				listView.refresh();
			}
		});
		if (search.areAllCriteriaReady())
			search.initiateSearch();
	}
	
	@Override
	public void loadData() {
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			search.onActivityResult(requestCode, resultCode, data);
	}

}
