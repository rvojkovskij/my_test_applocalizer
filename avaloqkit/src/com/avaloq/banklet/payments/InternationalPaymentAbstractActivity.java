package com.avaloq.banklet.payments;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentDefaultsResult;
import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentResult;
import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateResult;
import com.avaloq.afs.aggregation.to.payment.international.InternationalPaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.international.InternationalStandingOrderTO;
import com.avaloq.afs.aggregation.to.payment.international.InternationalStandingPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.InternationalPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.VerifyWithSubmissionMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.banklet.payments.views.BeneficiaryField.BeneficiaryFieldPaymentType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalPaymentDefaultsRequest;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalPaymentRequest;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalPaymentService;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalPaymentTemplateRequest;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalStandingPaymentRequest;

abstract public class InternationalPaymentAbstractActivity
	extends AbstractPaymentActivity<
	InternationalPaymentResult, InternationalPaymentRequest, InternationalPaymentOrderTO, InternationalPaymentTO,
	InternationalStandingPaymentResult, InternationalStandingPaymentRequest, InternationalStandingOrderTO,
	InternationalPaymentTemplateResult, InternationalPaymentTemplateRequest, InternationalPaymentTemplateTO,
	InternationalPaymentDefaultsResult, InternationalPaymentDefaultsRequest
	> {

	/**
	 * Override in child classes if something special is needed for the header
	 */
	@Override
	HeaderData getHeaderData() {
		return new HeaderData() {

			@Override
			public int getIconResId() {
				return R.drawable.pmt_new_international;
			}

			@Override
			public String getTitle() {
				return getResources().getString(R.string.pmt_payment_type_international_payment);
			}

			@Override
			public String getSubtitle() {
				switch (getViewDataMethod().getViewType()){
					case NEW:					
					case NEW_FROM_TEMPLATE:					
					case NEW_FROM_VIEW:
					case VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);				
					case TEMPLATE:					
					case TEMPLATE_FROM_VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_template);
					default:					
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);				
				}	
			}
		};
	}
	
	@Override
	public void populateFields(){
		mFieldBeneficiary.setPaymentType(BeneficiaryFieldPaymentType.INTERNATIONAL);
		mFieldBeneficiary.setCountries(getViewDataMethod().getDefaults().getPaymentDefaults().getCountries());
		
		if (getViewDataMethod().getTemplateResult() != null && !isConfirmMode) {			
			mFieldBeneficiary.setmBankBIC(getViewDataMethod().getSlipFromTemplateRequest().getBic());
			mFieldBeneficiary.setCountryId(getViewDataMethod().getSlipFromTemplateRequest().getCountryId());
		}
		
		InternationalPaymentTO payment = getViewDataMethod().getPaymentFromRequest();
		
		if (payment != null && !isConfirmMode) {
			mFieldBeneficiary.setmBankBIC(payment.getBic());
			mFieldBeneficiary.setCountryId(payment.getCountryId());
		}
	}

	abstract List<ButtonDef> getButtonDefs();
	
	@Override
	ContentData getContentData() {
		return new ContentData() {

			public boolean hasDebitMoneyAccount() {
				return true;
			}

			@Override
			public boolean hasBeneficiaryBankDetails() {
				return true;
			}

			@Override
			public boolean hasBeneficiary() {
				return true;
			}

			public boolean hasAccountNumber() {
				return true;
			}

			@Override
			public boolean hasReferenceNumber() {
				return false;
			}

			@Override
			public boolean hasAmountField() {
				return true;
			}

			@Override
			public boolean hasSalaryPayment() {
				return true;
			}

			@Override
			public boolean hasExecutionDateField() {
				return true;
			}

			@Override
			public boolean hasChargeOptionType() {
				return true;
			}

			@Override
			public boolean hasStandingOrderField() {
				return true;
			}

			@Override
			public boolean hasSaveAsTemplateField() {
				return getViewDataMethod().getViewType() == PaymentViewType.NEW || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_TEMPLATE || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_VIEW;
			}

			@Override
			public boolean hasDebitAdviceField() {
				return true;
			}

			@Override
			public boolean hasPaymentReasonField() {				
				return true;
			}

			@Override
			public boolean hasCreditMoneyAccount() {
				return false;
			}

			@Override
			public boolean hasScanField() {
				// TODO Auto-generated method stub
				return false;
			}
        };
	}
	
	@Override
	public VerifyMethod<InternationalPaymentOrderTO, InternationalPaymentRequest, InternationalStandingOrderTO, InternationalStandingPaymentRequest> getVerifyMethod() {
		return new VerifyWithSubmissionMethod<InternationalPaymentOrderTO, InternationalPaymentRequest, InternationalStandingOrderTO, InternationalStandingPaymentRequest>(this) {

			@Override
			protected InternationalPaymentRequest getPaymentRequest(InternationalPaymentOrderTO order, RequestStateEvent<InternationalPaymentRequest> rse) {
				return InternationalPaymentService.verifyPayment(order, rse);
			}

			@Override
			protected InternationalStandingPaymentRequest getStandingRequest(InternationalStandingOrderTO order, RequestStateEvent<InternationalStandingPaymentRequest> rse) {
				return InternationalPaymentService.verifyStandingPayment(order, rse);
			}
		};
	}

	@Override
	public ConfirmMethod<InternationalPaymentOrderTO, InternationalPaymentRequest, InternationalStandingOrderTO, InternationalStandingPaymentRequest> getConfirmMethod() {
		return new ConfirmMethod<InternationalPaymentOrderTO, InternationalPaymentRequest, InternationalStandingOrderTO, InternationalStandingPaymentRequest>(this) {

			@Override
			protected InternationalPaymentRequest getPaymentRequest(InternationalPaymentOrderTO order, RequestStateEvent<InternationalPaymentRequest> rse) {
				return InternationalPaymentService.submitPayment(order, rse);
			}

			@Override
			protected InternationalStandingPaymentRequest getStandingRequest(InternationalStandingOrderTO order, RequestStateEvent<InternationalStandingPaymentRequest> rse) {
				return InternationalPaymentService.submitStandingPayment(order, rse);
			}
		};
	}

	@Override
	public SubmissionResultMethod<InternationalPaymentResult, InternationalPaymentRequest, InternationalStandingPaymentResult, InternationalStandingPaymentRequest> getSubmissionResultMethod() {
		return new SubmissionResultMethod<InternationalPaymentResult, InternationalPaymentRequest, InternationalStandingPaymentResult, InternationalStandingPaymentRequest>(this) {
			@Override
			public SaveTemplateType getSaveTemplateType() {
				return SaveTemplateType.INTERNATIONAL;
			}
		};
	}

	@Override
	public ViewDataMethod<InternationalPaymentResult, InternationalPaymentRequest, InternationalPaymentOrderTO, InternationalPaymentTO, InternationalStandingPaymentResult, InternationalStandingPaymentRequest, InternationalStandingOrderTO, InternationalPaymentTemplateResult, InternationalPaymentTemplateRequest, InternationalPaymentTemplateTO, InternationalPaymentDefaultsResult, InternationalPaymentDefaultsRequest> createViewDataMethod() {
		return new ViewDataMethod<InternationalPaymentResult, InternationalPaymentRequest, InternationalPaymentOrderTO, InternationalPaymentTO, InternationalStandingPaymentResult, InternationalStandingPaymentRequest, InternationalStandingOrderTO, InternationalPaymentTemplateResult, InternationalPaymentTemplateRequest, InternationalPaymentTemplateTO, InternationalPaymentDefaultsResult, InternationalPaymentDefaultsRequest>(this) {

			@Override
			protected InternationalPaymentRequest getPaymentRequest(long paymentId, RequestStateEvent<InternationalPaymentRequest> rse) {
				return InternationalPaymentService.getPayment(paymentId, rse);
			}

			@Override
			protected InternationalStandingPaymentRequest getStandingRequest(long paymentId, RequestStateEvent<InternationalStandingPaymentRequest> rse) {
				return InternationalPaymentService.getStandingPayment(paymentId, rse);
			}

			@Override
			protected InternationalPaymentTemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
				return InternationalPaymentService.getPaymentTemplate(paymentId, rse);
			}

			@Override
			protected InternationalPaymentDefaultsRequest getDefaultsRequest(RequestStateEvent<InternationalPaymentDefaultsRequest> rse) {
				return InternationalPaymentService.getDefaults(rse);
			}

			@Override
			protected InternationalPaymentOrderTO getPaymentOrder(InternationalPaymentResult result) {
				return result.getInternationalPaymentOrder();
			}

			@Override
			protected InternationalStandingOrderTO getStandingOrder(InternationalStandingPaymentResult result) {
				return result.getInternationalStandingOrder();
			}

			@Override
			protected InternationalPaymentTemplateTO getPaymentTemplate(InternationalPaymentTemplateResult result) {
				return result.getInternationalPaymentTemplate();
			}

			@Override
			protected InternationalPaymentTO getSlipFromPaymentOrder(InternationalPaymentOrderTO result) {
				return result.getInternationalPayment();
			}

			@Override
			protected InternationalPaymentTO getSlipFromStandingOrder(InternationalStandingOrderTO result) {
				return result.getInternationalPayment();
			}

			@Override
			protected InternationalPaymentOrderTO createEmptyPaymentOrder() {
				return new InternationalPaymentOrderTO();
			}

			@Override
			protected InternationalStandingOrderTO createEmptyStandingOrder() {
				return new InternationalStandingOrderTO();
			}
			
			@Override
			protected PaymentDefaults getPaymentDefaults(InternationalPaymentDefaultsResult template) {
				return template.getPaymentDefaults();
			}
			
			@Override
			protected InternationalPaymentTO getSlipFromTemplate(InternationalPaymentTemplateTO template) {
				return template.getInternationalPayment();
			}
			
			@Override
			public InternationalPaymentTO createEmptyPaymentSlip() {
				return new InternationalPaymentTO();
			}
		};
	}
	
	@Override
	public TemplateMethod<InternationalPaymentTO, InternationalPaymentTemplateResult, InternationalPaymentTemplateRequest, InternationalPaymentTemplateTO> getTemplateMethod() {
		return new TemplateMethod<InternationalPaymentTO, InternationalPaymentTemplateResult, InternationalPaymentTemplateRequest, InternationalPaymentTemplateTO>(this){

			@Override
			protected void setPayment(InternationalPaymentTemplateTO template, InternationalPaymentTO payment) {
				template.setPayment(payment);
			}

			@Override
			protected InternationalPaymentTemplateRequest getVerifyRequest(InternationalPaymentTemplateTO template, RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
				return InternationalPaymentService.verifyPaymentTemplate(template, rse);
			}

			@Override
			protected InternationalPaymentTemplateRequest getSubmitRequest(InternationalPaymentTemplateTO template, RequestStateEvent<InternationalPaymentTemplateRequest> rse) {
				return InternationalPaymentService.savePaymentTemplate(template, rse);
			}

			@Override
			protected InternationalPaymentTemplateTO createEmptyTemplate() {
				return new InternationalPaymentTemplateTO();
			}
			
		};
	}

	@Override
	public InternationalPaymentOrderTO getPaymentOrder() {
		InternationalPaymentOrderTO order = getViewDataMethod().getPaymentOrder();
		order.setPayment(getPayment());
		return order;
	}

	@Override
	public InternationalStandingOrderTO getStandingOrder() {
		InternationalStandingOrderTO order = getViewDataMethod().getStandingOrder();
		order.setPayment(getPayment());
		return order;
	}
	
	@Override
	public InternationalPaymentTO fillPayment(InternationalPaymentTO payment) {
		if(payment != null) {
			if (mFieldBeneficiary.getBIC() != null && !mFieldBeneficiary.getBIC().equals("")) {
				payment.setBic(mFieldBeneficiary.getBIC());
			}
			if(mFieldBeneficiary != null && mFieldBeneficiary.getCountry() != null) {
				payment.setCountryId(mFieldBeneficiary.getCountry().getId());
			}
			payment.setPaymentType(PaymentType.INTERNATIONAL_PAYMENT);
		}
		return payment;
	}

	@Override
	public PaymentType getPaymentType(){
		return PaymentType.INTERNATIONAL_PAYMENT;
	}
	
}
