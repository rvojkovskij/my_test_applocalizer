package com.avaloq.banklet.payments;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.avaloq.banklet.payments.fragment.DirectDebitAgreements;
import com.avaloq.banklet.payments.fragment.PaymentList;
import com.avaloq.banklet.payments.fragment.PaymentOverview;
import com.avaloq.banklet.payments.fragment.PendingPayments;
import com.avaloq.banklet.payments.fragment.StandingOrders;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.BankletFragment;

public class HolderActivity extends BankletActivity {
	
	public static final String EXTRA_ACTION_TYPE = "extra_action_type";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pmt_activity_holder);
		int type = getIntent().getIntExtra(EXTRA_ACTION_TYPE, 0);
		
		if (getSupportFragmentManager().findFragmentById(R.id.fragment) == null){
		
			FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
			
			BankletFragment fragment = null;
			if (type == R.layout.pmt_action_new_payment) {
				fragment = new PaymentOverview();
				setTitle(R.string.pmt_new_payment);
			} else if (type == R.layout.pmt_action_list_payments) {
				fragment = new PaymentList();
				setTitle(R.string.pmt_created_payments);
			} else if (type == R.layout.pmt_action_standing_orders) {
				fragment = new PaymentOverview();
				Bundle arguments = new Bundle();
				arguments.putBoolean(PaymentOverview.EXTRA_IS_STANDING_ORDER, true);
				fragment.setArguments(arguments);
				setTitle(R.string.pmt_standing_orders);
			} else if (type == R.layout.pmt_action_direct_debits) {
				fragment = new DirectDebitAgreements();
				setTitle(R.string.pmt_direct_debit);
			} else if (type == R.layout.pmt_action_new_template){
				fragment = new PaymentOverview();
				((PaymentOverview)fragment).setIsTemplateOverview(true);
				Bundle arguments = new Bundle();
				arguments.putBoolean(PaymentOverview.EXTRA_IS_TEMPLATE_OVERVIEW, true);
				fragment.setArguments(arguments);
				setTitle(R.string.pmt_new_template);
			}else if (type == R.id.pmt_pending){
				fragment = new PendingPayments();
				setTitle(R.string.pmt_pending_title);
			}
			
			View view = findViewById(R.id.fragment);
			if (view != null && fragment != null)
				tx.replace(R.id.fragment, fragment);
			
			tx.commit();
		}
		
		
	}
}
