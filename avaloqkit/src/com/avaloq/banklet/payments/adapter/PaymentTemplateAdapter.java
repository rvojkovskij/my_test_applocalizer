package com.avaloq.banklet.payments.adapter;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.banklet.payments.fragment.FavoriteTemplatesPage;

public class PaymentTemplateAdapter extends FragmentStatePagerAdapter {
	
	public static final PaymentTemplateAdapter EMPTY = new PaymentTemplateAdapter(null, new ArrayList<PaymentTemplateInfo>());
	
	private static final int ITEMS_PER_PAGE = 6;
	
	private List<PaymentTemplateInfo> mTemplates;
	
	public PaymentTemplateAdapter(FragmentManager fm, List<PaymentTemplateInfo> aTemplates) {
		super(fm);
		this.mTemplates = aTemplates;
	}
	
	@Override
	public int getCount() {
		return Double.valueOf(Math.ceil(Double.valueOf(this.mTemplates.size()) / Double.valueOf(ITEMS_PER_PAGE))).intValue();
	}
	
	@Override
	public Fragment getItem(int position) {
		ArrayList<Integer> pageTemplates = new ArrayList<Integer>();
		int offset = position * ITEMS_PER_PAGE;
		for(int i = offset;i < offset + ITEMS_PER_PAGE && i < this.mTemplates.size();i++) {
			pageTemplates.add(i);
		}
		return FavoriteTemplatesPage.newInstance(pageTemplates);
	}
	
}