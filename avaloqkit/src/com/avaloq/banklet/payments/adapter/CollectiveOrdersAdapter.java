package com.avaloq.banklet.payments.adapter;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.BasePaymentInfo;
import com.avaloq.framework.R;
import com.avaloq.framework.util.CurrencyUtil;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class CollectiveOrdersAdapter extends ArrayAdapter<BasePaymentInfo>{

	public CollectiveOrdersAdapter(Context context, List<BasePaymentInfo> objects) {
		super(context, R.layout.pmt_collective_orders_item, R.id.collective_order_beneficiary, objects);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		BasePaymentInfo item = getItem(position);
		
		fillTextView(view, R.id.collective_order_beneficiary, item.getBeneficiary());
		fillTextView(view, R.id.collective_order_reference, item.getReference());
		fillTextView(view, R.id.collective_order_account, item.getDebitAccountLabel());
		fillTextView(view, R.id.collective_order_type, item.getPaymentType().toString());
		fillTextView(view, R.id.collective_order_amount, CurrencyUtil.formatMoney(item.getAmount(), item.getCurrencyIsoCode()));
		
		return view;
	}
	
	protected void fillTextView(View container, int resId, String text){
		TextView view = (TextView)container.findViewById(resId);
		if (view != null)
			view.setText(text);
	}

}
