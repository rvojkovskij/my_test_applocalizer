package com.avaloq.banklet.payments.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO;
import com.avaloq.banklet.payments.fragment.FavoriteTemplates;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoRequest;
import com.avaloq.framework.tools.ProgressiveListAdapter;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.CurrencyUtil;

public class PaymentTemplateFullAdapter extends ProgressiveListAdapter<PaymentTemplateInfo>{
	
	private static final String TAG = PaymentTemplateFullAdapter.class.getSimpleName();
	
	private boolean refreshNeeded = false;

	public PaymentTemplateFullAdapter(Context context, ProgressiveListView listView, List<PaymentTemplateInfo> objects) {
		super(context, listView, R.layout.pmt_templates_all_item, R.id.pmt_favorite_templates_alias, objects);
	}

	@Override
	public View createView(final int position, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(getContext()).inflate(R.layout.pmt_templates_all_item, null);
		
		final PaymentTemplateInfo item = getItem(position);
		
		((TextView)view.findViewById(R.id.pmt_favorite_templates_alias)).setText(item.getAlias());
		((TextView)view.findViewById(R.id.pmt_favorite_templates_debit_account_label)).setText(item.getDebitAccountLabel());
		((TextView)view.findViewById(R.id.pmt_favorite_templates_amount)).setText(CurrencyUtil.formatMoney(item.getAmount(), item.getCurrencyIsoCode()));
		((ImageView)view.findViewById(R.id.pmt_favorite_templates_icon)).setImageResource(PaymentUtil.getPaymentDrawable(item.getPaymentType()));
		
		final CheckBox check = (CheckBox)view.findViewById(R.id.pmt_favorite);
		check.setOnCheckedChangeListener (null);
		check.setChecked(item.getFavorite());
		check.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
				/*Intent returnIntent = new Intent();
				returnIntent.putExtra(FavoriteTemplates.EXTRA_IS_REFRESH_NEEDED, true);
				mActivity.setResult(Activity.RESULT_OK, returnIntent);*/
				
				BaseBankingObjectTO object = new BaseBankingObjectTO();
				object.setVersionId(item.getVersionId());
				object.setId(item.getPaymentId());
				PaymentTemplateInfoRequest request = PaymentOverviewService.savePaymentTemplateFavorite(object, isChecked, new RequestStateEvent<PaymentTemplateInfoRequest>(){
					@Override
					public void onRequestCompleted(PaymentTemplateInfoRequest aRequest) {
						PaymentTemplateInfo template = aRequest.getResponse().getData().getPaymentTemplateInfo();
						if (template == null){
							//Some error has occurred, change the state of the button and show error message
						} else {
							// Replace the old template with the new one we got from the request
							Log.d(TAG, "refreshing: " + template.getFavorite());
							refreshNeeded = true;
							remove(item);
							insert(template, position);
							notifyDataSetChanged();
						}
					}
					
					@Override
					public void onRequestFailed(PaymentTemplateInfoRequest aRequest) {
						check.setChecked(!isChecked);
						DialogFragment.showNetworkError();
					}
				});
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
			}
		});
		
		return view;
		
	}

}
