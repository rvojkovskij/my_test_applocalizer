package com.avaloq.banklet.payments.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.avaloq.framework.R;

public abstract class GriddableViewPagerAdapter extends PagerAdapter{
	protected Activity mActivity;
	protected List<Integer> buttons = new ArrayList<Integer>();
	protected int width, height;
	protected int verticalPadding, horizontalPadding, buttonHeight;
	
	public GriddableViewPagerAdapter(Activity activity, int gridArrayId, int aWidth, int aHeight, int aButtonHeight, ViewPager pager) {
		mActivity = activity;
		TypedArray array = mActivity.getResources().obtainTypedArray(gridArrayId);
		
		for (int i=0;i<array.length(); i++){
			buttons.add(array.getResourceId(i, 0));
		}
		
		/*width = mActivity.getResources().getInteger(R.integer.payment_types_width);
		height = mActivity.getResources().getInteger(R.integer.payment_types_height);
		buttonHeight = (int)mActivity.getResources().getDimension(R.dimen.payment_button_height);*/
		verticalPadding = (int)mActivity.getResources().getDimension(R.dimen.payment_button_vertical_padding);
		horizontalPadding = (int)mActivity.getResources().getDimension(R.dimen.payment_button_horizontal_padding);
		
		width = aWidth;
		height = aHeight;
		buttonHeight = aButtonHeight;
		
		int pagerHeight = height*buttonHeight + (height-1)*verticalPadding*2;
		if (pager.getLayoutParams() != null)
			pager.getLayoutParams().height = pagerHeight;
		else
			pager.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, pagerHeight));
		
		array.recycle();
	}
	
	public void setVerticalPadding(int padding){
		verticalPadding = padding;
	}
	
	public void setHorizontalPadding(int padding){
		horizontalPadding = padding;
	}

	@Override
	public int getCount() {
		int count = buttons.size();
		if (count % (width*height) == 0)
			return count / (width*height);
		else
			return count / (width*height) + 1;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == (View)arg1;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		final LinearLayout table = new LinearLayout(mActivity);
		table.setOrientation(LinearLayout.VERTICAL);
		container.addView(table);
		
		for (int i=0;i<height;i++){
			LinearLayout row = new LinearLayout(mActivity);
			row.setLayoutParams(new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, android.widget.LinearLayout.LayoutParams.WRAP_CONTENT));
			row.setOrientation(LinearLayout.HORIZONTAL);
			table.addView(row);
			row.setWeightSum(width);
			for (int j=0; j<width; j++){
				int paddingLeft = 0, paddingRight = 0, paddingTop = 0, paddingBottom = 0;
				int location = position*height*width + i*width+j;
				View element;
				if (location < buttons.size())
					element = getView(buttons.get(location));
				else
					element = new TextView(mActivity);
					if (j != width -1)
						paddingRight = horizontalPadding;
					if (j != 0)
						paddingLeft = horizontalPadding;
					if (i != height-1)
						paddingBottom = verticalPadding;
					if (i != 0)
						paddingTop = verticalPadding;
					android.widget.LinearLayout.LayoutParams params = new android.widget.LinearLayout.LayoutParams(android.widget.LinearLayout.LayoutParams.MATCH_PARENT, buttonHeight, 1);
					params.setMargins(paddingLeft, paddingTop, paddingRight, paddingBottom);
					element.setLayoutParams(params);
					row.addView(element);
			}
		}
		table.addView(new TableRow(mActivity));
		
		return table;
	}
	
	private final View getView(int resId) {
		View view = LayoutInflater.from(mActivity).inflate(resId, null, false);
		return prepareView(resId, view);
	}
	
	protected abstract View prepareView(int resId, View view);
	
	/*private View prepareView(int resId, View view){
		Class<? extends AbstractPaymentActivity> klass = null;
		switch (resId){
			case R.layout.pmt_button_account:
				// TODO: Add code here
				break;
			case R.layout.pmt_button_domestic:
				klass = DomesticPaymentActivity.class;
				break;
			case R.layout.pmt_button_international:
				klass = DomesticPaymentActivity.class;
				break;
			case R.layout.pmt_button_red:
				klass = DomesticPaymentActivity.class;
				break;
			case R.layout.pmt_button_orange:
				klass = DomesticPaymentActivity.class;
				break;
		}
		if (klass != null){
			final Intent intent = new Intent(mActivity, klass);
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mActivity.startActivity(intent);
				}
			});
		}
		return view;
	}*/
}
