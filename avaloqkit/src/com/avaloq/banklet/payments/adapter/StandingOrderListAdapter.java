package com.avaloq.banklet.payments.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.StandingPaymentInfo;
import com.avaloq.afs.server.bsp.client.ws.DdMandateStatusType;
import com.avaloq.banklet.payments.AccountTransferViewActivity;
import com.avaloq.banklet.payments.DomesticPaymentViewActivity;
import com.avaloq.banklet.payments.InternationalPaymentViewActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentViewActivity;
import com.avaloq.banklet.payments.SwissRedPaymentViewActivity;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.ProgressiveListAdapter;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.DateUtil;

public class StandingOrderListAdapter extends ProgressiveListAdapter<StandingPaymentInfo> {
	
	private static final String TAG = StandingOrderListAdapter.class.getSimpleName();
	
	private class ViewHolder {
		private ViewHolder() {} 
		public LinearLayout statusContainer;
		public TextView textBeneficiary;
		public TextView textDebitAccount;
		public TextView textPeriodicity;
		public TextView textDuration;
		public TextView textAmount;
		public TextView textDate;
	}
	
	private PaymentUtil mUtil;

	public StandingOrderListAdapter(Context context, ProgressiveListView listView, List<StandingPaymentInfo> objects) {
		super(context, listView, R.layout.pmt_row_standing_order, R.id.pmt_row_standing_order_beneficiary, objects);
		this.mUtil = new PaymentUtil(getContext());
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		View row = null;
		ViewHolder holder;
		DateUtil dateUtil = new DateUtil((Activity)getContext());
		
		// In case we have a convertView, reuse it instead of taking more memory.
		//if (true) {
			row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_row_standing_order, null);
			holder = new ViewHolder();
			holder.textBeneficiary = (TextView) row.findViewById(R.id.pmt_row_standing_order_beneficiary);
			holder.statusContainer = (LinearLayout) row.findViewById(R.id.status_container);
			holder.textPeriodicity = (TextView) row.findViewById(R.id.pmt_row_standing_order_periodicity);
			holder.textAmount = (TextView) row.findViewById(R.id.pmt_row_standing_order_amount);
			holder.textDuration = (TextView) row.findViewById(R.id.pmt_row_standing_order_duration);
			if(row.findViewById(R.id.pmt_row_standing_order_large) != null) {
				holder.textDebitAccount = (TextView) row.findViewById(R.id.pmt_row_standing_order_debit_account);
				holder.textDate = (TextView) row.findViewById(R.id.pmt_row_standing_order_date);
			}
//			row.setTag(holder);
//		} else {
//			row = convertView;
//			holder = (ViewHolder)convertView.getTag();
//		}
		
		final StandingPaymentInfo standingOrder = getItem(position);

		holder.textBeneficiary.setText(standingOrder.getBeneficiary());
		holder.textPeriodicity.setText(standingOrder.getPeriodLabel());
		if (standingOrder.getEndDate() != null)
			holder.textDuration.setText(dateUtil.format(standingOrder.getEndDate()));
		else if (standingOrder.getPeriodCount() != null && standingOrder.getPeriodCount() != 0){
			if (standingOrder.getPeriodCount() == 1)
				holder.textDuration.setText(getContext().getResources().getString(R.string.one_time));
			else
				holder.textDuration.setText(getContext().getResources().getString(R.string.many_times).replace("%Count%", standingOrder.getPeriodCount().toString()));
		}
		else
			holder.textDuration.setText(getContext().getResources().getString(R.string.till_further_notice));
		
		TextView status;
		if (standingOrder.isDeactivated()){
			status = (TextView)LayoutInflater.from(getContext()).inflate(R.layout.avq_status_canceled, holder.statusContainer, false);
		}
		else{
			status = (TextView)LayoutInflater.from(getContext()).inflate(R.layout.avq_status_open, holder.statusContainer, false);
		}
		status.setText(mUtil.getStandingOrderStatusString(standingOrder.getStandingOrderSearchStateType()));
		
		holder.statusContainer.addView(status);

		holder.textAmount.setText(CurrencyUtil.formatMoney(standingOrder.getAmount(), standingOrder.getCurrencyIsoCode()));
		if (standingOrder.getNextExecutionDate() != null && holder.textDate != null)
			holder.textDate.setText(dateUtil.format(standingOrder.getNextExecutionDate()));
		
		row.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent i;
				switch(standingOrder.getPaymentType()) {

				case SWISS_ORANGE_PAYMENT_SLIP:									
					i = new Intent(getContext(), SwissOrangePaymentViewActivity.class);
					i.putExtra(SwissOrangePaymentViewActivity.EXTRA_ID, standingOrder.getPaymentId());
					i.putExtra(SwissOrangePaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
					i.putExtra(SwissOrangePaymentViewActivity.EXTRA_IS_FROM_STANDING, true);
					getContext().startActivity(i);
					break;
				case SWISS_RED_PAYMENT_SLIP:
					i = new Intent(getContext(), SwissRedPaymentViewActivity.class);
					i.putExtra(SwissRedPaymentViewActivity.EXTRA_ID, standingOrder.getPaymentId());
					i.putExtra(SwissRedPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
					i.putExtra(SwissRedPaymentViewActivity.EXTRA_IS_FROM_STANDING, true);
					getContext().startActivity(i);
					break;
				case DOMESTIC_PAYMENT:
					i = new Intent(getContext(), DomesticPaymentViewActivity.class);
					i.putExtra(DomesticPaymentViewActivity.EXTRA_ID, standingOrder.getPaymentId());
					i.putExtra(DomesticPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
					i.putExtra(DomesticPaymentViewActivity.EXTRA_IS_FROM_STANDING, true);
					getContext().startActivity(i);
					break;
				case INTERNAL_PAYMENT:
					i = new Intent(getContext(), AccountTransferViewActivity.class);
					i.putExtra(AccountTransferViewActivity.EXTRA_ID, standingOrder.getPaymentId());
					i.putExtra(AccountTransferViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
					i.putExtra(AccountTransferViewActivity.EXTRA_IS_FROM_STANDING, true);
					getContext().startActivity(i);
					break;
				case INTERNATIONAL_PAYMENT:
					i = new Intent(getContext(), InternationalPaymentViewActivity.class);
					i.putExtra(InternationalPaymentViewActivity.EXTRA_ID, standingOrder.getPaymentId());
					i.putExtra(InternationalPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
					i.putExtra(InternationalPaymentViewActivity.EXTRA_IS_FROM_STANDING, true);
					getContext().startActivity(i);				
					break;								
				default:
					break;
				}
			}
		});
		
		return row;
	}

}
