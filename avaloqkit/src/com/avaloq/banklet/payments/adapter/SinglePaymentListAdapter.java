package com.avaloq.banklet.payments.adapter;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentMaskType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.banklet.payments.AccountTransferActivity;
import com.avaloq.banklet.payments.DomesticPaymentActivity;
import com.avaloq.banklet.payments.InternationalPaymentActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentActivity;
import com.avaloq.banklet.payments.SwissRedPaymentActivity;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.tools.ProgressiveListAdapter;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.util.CurrencyUtil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.framework.R;

public class SinglePaymentListAdapter extends ProgressiveListAdapter<PaymentMaskWrapperTO> {
	
	PaymentUtil mUtil;

	public SinglePaymentListAdapter(Context context, ProgressiveListView listView, List<PaymentMaskWrapperTO> objects) {
		super(context, listView, R.layout.pmt_row_single_payment, R.id.pmt_alias, objects);
		mUtil = new PaymentUtil(context);
	}
	
	private class ViewHolder {
		private ViewHolder() {}
		public ImageView image;
		public TextView title;
		public TextView subtitle;
		public TextView amount;
	}

	@Override
	protected View createView(int position, View convertView, ViewGroup parent) {
		View row = null;
		ViewHolder holder;
		//if (convertView == null) {
			row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_row_single_payment, null);
			holder = new ViewHolder();
			holder.image = (ImageView)row.findViewById(R.id.pmt_icon);
			holder.title = (TextView)row.findViewById(R.id.pmt_alias);
			holder.subtitle = (TextView)row.findViewById(R.id.pmt_label);
			holder.amount = (TextView)row.findViewById(R.id.pmt_amount);
			
//			row.setTag(holder);
//		}
//		else {
//			row = convertView;
//			holder = (ViewHolder)convertView.getTag();
//		}
		
		final PaymentMaskWrapperTO wrapper = getItem(position);
		if (wrapper.getMaskType() == PaymentMaskType.EXECUTED_PAYMENT || wrapper.getMaskType() == PaymentMaskType.PAYMENT_TEMPLATE)
			holder.title.setText(wrapper.getPayment().getBeneficiary1());
		else
			holder.title.setText(getContext().getString(R.string.any_beneficiary));
		
		holder.subtitle.setText(mUtil.getPaymentTypeString(wrapper.getPayment().getPaymentType()));
		if (wrapper.getPayment().getAmount() != null)
			holder.amount.setText(CurrencyUtil.formatMoney(wrapper.getPayment().getAmount(), wrapper.getPayment().getCurrencyId()));
		
		holder.image.setImageResource(PaymentUtil.getPaymentDrawable(wrapper.getPayment().getPaymentType()));
		
		row.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				PaymentType type = wrapper.getPayment().getPaymentType();
				Class<? extends BankletActivity> klass = null;
				switch (type){
				case DOMESTIC_PAYMENT:
					klass = DomesticPaymentActivity.class;
					break;
				case INTERNAL_PAYMENT:
					klass = AccountTransferActivity.class;
					break;
				case INTERNATIONAL_PAYMENT:
					klass = InternationalPaymentActivity.class;
					break;
				case SWISS_ORANGE_PAYMENT_SLIP:
					klass = SwissOrangePaymentActivity.class;
					break;
				case SWISS_RED_PAYMENT_SLIP:
					klass = SwissRedPaymentActivity.class;
					break;
				default:
					return;
				
				}
				Activity activity = (Activity)getContext();
				Intent intent = new Intent(activity, klass);
				intent.putExtra(AbstractPaymentActivity.EXTRA_MASK, wrapper);
				
				activity.startActivity(intent);
			}
		});
		
		return row;
	}

}
