package com.avaloq.banklet.payments.adapter;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentInfo;
import com.avaloq.afs.aggregation.to.payment.StandingPaymentInfo;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.ProgressiveListAdapter;
import com.avaloq.framework.tools.ProgressiveListView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PendingPaymentsAdapter extends BaseAdapter {
	
	Activity mActivity;
	ProgressiveListView mListView;
	
	ProgressiveListAdapter<PaymentInfo> paymentAdapter;
	ProgressiveListAdapter<StandingPaymentInfo> standingAdapter;
	
	public PendingPaymentsAdapter(Activity context, ProgressiveListView listView, List<PaymentInfo> payments, List<StandingPaymentInfo> standingOrders){
		mActivity = context;
		mListView = listView;
		
		paymentAdapter = new PaymentListAdapter(mActivity, listView, payments);
		standingAdapter = new StandingOrderListAdapter(mActivity, listView, standingOrders);
	}

	@Override
	public int getCount() {
		int count = 0;
		if (hasPayments())
			count+= paymentAdapter.getCount()+1; // +1 for the header
		if (hasStandingOrders())
			count+= standingAdapter.getCount()+1; // +1 for the header
			
		return count;
		
	}

	@Override
	public Object getItem(int position) {		
		if (position == 0){
			return new AdapterItem(Type.HEADER, 0);
		}
		if (hasPayments() && position < paymentAdapter.getCount()+1)
			return new AdapterItem(Type.PAYMENT, position-1);
		if (!hasPayments() && hasStandingOrders() && position < standingAdapter.getCount()+1)
			return new AdapterItem(Type.STANDING, position-1);
		if (hasPayments() && position == paymentAdapter.getCount()+1)
			return new AdapterItem(Type.HEADER, 1);
		if (hasPayments() && hasStandingOrders() && position < paymentAdapter.getCount()+standingAdapter.getCount()+2)
			return new AdapterItem(Type.STANDING, position-2-paymentAdapter.getCount());
		
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		AdapterItem item = (AdapterItem)getItem(position);
		if (item != null && item.type != null){
			switch (item.type) {
				case HEADER:
					return getHeader(item.position);
				case PAYMENT:
					return paymentAdapter.getView(item.position, null, parent);
				case STANDING:
					return standingAdapter.getView(item.position, null, parent);
			}
		}
		
		return null;
	}
	
	public View getHeader(int number){
		if (number == 0)
			return LayoutInflater.from(mActivity).inflate(R.layout.pmt_row_payment_header, null, false);
		
		return LayoutInflater.from(mActivity).inflate(R.layout.pmt_standing_orders_header, null, false);
	}
	
	public boolean hasPayments(){
		return paymentAdapter != null && paymentAdapter.getCount() > 0;
	}
	
	public boolean hasStandingOrders(){
		return standingAdapter != null && standingAdapter.getCount() > 0;
	}
	
	public enum Type {
		HEADER, PAYMENT, STANDING;
	}
	
	public class AdapterItem {
		public Type type;
		public int position;
		
		public AdapterItem(Type aType, int aPosition){
			type = aType;
			position  = aPosition;
		}
	}

}
