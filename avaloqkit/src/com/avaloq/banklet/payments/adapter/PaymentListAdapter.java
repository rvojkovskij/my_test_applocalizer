package com.avaloq.banklet.payments.adapter;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.PaymentInfo;
import com.avaloq.afs.server.bsp.client.ws.PaymentStateType;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.AccountTransferViewActivity;
import com.avaloq.banklet.payments.CollectiveOrders;
import com.avaloq.banklet.payments.DomesticPaymentViewActivity;
import com.avaloq.banklet.payments.InternationalPaymentViewActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentViewActivity;
import com.avaloq.banklet.payments.SwissRedPaymentViewActivity;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.ProgressiveListAdapter;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.DateUtil;

public class PaymentListAdapter extends ProgressiveListAdapter<PaymentInfo> {
	
	private static final String TAG = PaymentListAdapter.class.getSimpleName();
	
	private class ViewHolder {
		private ViewHolder() {} 
		public TextView textStatus;
		public TextView textBeneficiary;
		public TextView textPaymentType;
		public TextView textAmount;
		public TextView textPaymentDate;
		public TextView textReference;
		public TextView textDebitAccount;
		public View collectiveOrderSign;
	}
	
	private PaymentUtil mUtil;

	public PaymentListAdapter(Context context, ProgressiveListView listView, List<PaymentInfo> objects) {
		super(context, listView, R.layout.pmt_row_payment, R.id.pmt_row_payment_beneficiary, objects);
		this.mUtil = new PaymentUtil(getContext());
	}

	@Override
	public View createView(int position, View convertView, ViewGroup parent) {
		View row = null;
		ViewHolder holder;
		
		// In case we have a convertView, reuse it instead of taking more memory.
		//if (convertView == null) {
			row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_row_payment, null);
			holder = new ViewHolder();
			holder.textStatus = (TextView) row.findViewById(R.id.pmt_row_payment_status);
			holder.textBeneficiary = (TextView) row.findViewById(R.id.pmt_row_payment_beneficiary);
			holder.textPaymentType = (TextView) row.findViewById(R.id.pmt_row_payment_type);
			holder.textAmount = (TextView) row.findViewById(R.id.pmt_row_payment_amount);
			holder.textPaymentDate = (TextView) row.findViewById(R.id.pmt_row_payment_date);
			holder.collectiveOrderSign = row.findViewById(R.id.collective_order_sign);
			if(row.findViewById(R.id.pmt_row_payment_large) != null) {
				holder.textReference = (TextView) row.findViewById(R.id.pmt_row_payment_reference);
				holder.textDebitAccount = (TextView) row.findViewById(R.id.pmt_row_payment_debit_account);
			}
//			row.setTag(holder);
//		} else {
//			row = convertView;
//			holder = (ViewHolder)convertView.getTag();
//		}
		
		Log.d(TAG, "position: "+position);
		
		final PaymentInfo payment = getItem(position);
		
		Log.d(TAG, "item("+position+"): "+payment);
		
		int paddingTop = holder.textStatus.getPaddingTop();
		int paddingLeft = holder.textStatus.getPaddingLeft();
		int paddingRight = holder.textStatus.getPaddingRight();
		int paddingBottom = holder.textStatus.getPaddingBottom();
		
		holder.textStatus.setText(mUtil.getPaymentStatusString(payment.getStatus()));
		holder.textStatus.setTextColor(getContext().getResources().getColor(PaymentUtil.getStateTextColor(PaymentStateType.fromValue(payment.getStatus().toUpperCase()))));
		holder.textStatus.setBackgroundResource(PaymentUtil.getStateBackground(PaymentStateType.fromValue(payment.getStatus().toUpperCase())));
		
		holder.textStatus.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
		
		if (payment.getPaymentType() == null)
			holder.textBeneficiary.setText(R.string.collective_order_list_title);
		else
			holder.textBeneficiary.setText(payment.getBeneficiary());
		holder.textPaymentType.setText(mUtil.getPaymentTypeString(payment.getPaymentType())); // TODO read the strings from the xml
		holder.textPaymentDate.setText(new DateUtil(getContext()).format(payment.getTransactionDate()));
		holder.textAmount.setText(CurrencyUtil.formatMoney(payment.getAmount(), payment.getCurrencyIsoCode()));
		
		if (payment.getPaymentType() == null)
			holder.collectiveOrderSign.setVisibility(View.VISIBLE);
		else
			holder.collectiveOrderSign.setVisibility(View.GONE);
			
		if(holder.textReference != null && payment.getPaymentType() != null) 
			holder.textReference.setText(payment.getReference());
		if(holder.textDebitAccount != null) 
			holder.textDebitAccount.setText(payment.getDebitAccountLabel());
		
		row.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent i;
				if (payment.getPaymentType() != null){
					switch(payment.getPaymentType()) {
	
					case SWISS_ORANGE_PAYMENT_SLIP:									
						i = new Intent(getContext(), SwissOrangePaymentViewActivity.class);
						i.putExtra(SwissOrangePaymentViewActivity.EXTRA_ID, payment.getPaymentId());
						i.putExtra(SwissOrangePaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
						getContext().startActivity(i);
						break;
					case SWISS_RED_PAYMENT_SLIP:
						i = new Intent(getContext(), SwissRedPaymentViewActivity.class);
						i.putExtra(SwissRedPaymentViewActivity.EXTRA_ID, payment.getPaymentId());
						i.putExtra(SwissRedPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
						getContext().startActivity(i);
						break;
					case DOMESTIC_PAYMENT:
						i = new Intent(getContext(), DomesticPaymentViewActivity.class);
						i.putExtra(DomesticPaymentViewActivity.EXTRA_ID, payment.getPaymentId());
						i.putExtra(DomesticPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
						getContext().startActivity(i);					
						break;
					case INTERNAL_PAYMENT:
						i = new Intent(getContext(), AccountTransferViewActivity.class);
						i.putExtra(AccountTransferViewActivity.EXTRA_ID, payment.getPaymentId());
						i.putExtra(AccountTransferViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
						getContext().startActivity(i);
						break;							
					case INTERNATIONAL_PAYMENT:
						i = new Intent(getContext(), InternationalPaymentViewActivity.class);
						i.putExtra(InternationalPaymentViewActivity.EXTRA_ID, payment.getPaymentId());
						i.putExtra(InternationalPaymentViewActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.VIEW);
						getContext().startActivity(i);					
						break;								
					default:
						break;
					}
				}
				else {
					i = new Intent(getContext(), CollectiveOrders.class);
					i.putExtra(CollectiveOrders.EXTRA_COLLECTIVE_ORDER, payment.getPaymentId());
					getContext().startActivity(i);
				}
			}
		});
		
		return row;
	}

}
