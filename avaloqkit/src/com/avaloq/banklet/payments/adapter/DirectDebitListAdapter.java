package com.avaloq.banklet.payments.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.DdMandateStatusType;
import com.avaloq.afs.server.bsp.client.ws.DirectDebitMandateTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.banklet.payments.fragment.DirectDebitDetails;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class DirectDebitListAdapter extends ArrayAdapter<DirectDebitMandateTO> {

	private static final String TAG = DirectDebitListAdapter.class.getSimpleName();

	private class ViewHolder {
		private ViewHolder() {
		}

		public TextView textName;
		public TextView textOwnerTxt;
		public TextView textDebitAccount;
		public TextView textDebitAccountNumber;
		public TextView textDate;
		public LinearLayout statusContainer;
	}

	private PaymentUtil mUtil;
	private DateUtil mDateUtil;
	private List<MoneyAccountTO> mMoneyAccounts;

	public DirectDebitListAdapter(Context context, List<DirectDebitMandateTO> objects, List<MoneyAccountTO> moneyAccounts) {
		super(context, R.layout.pmt_row_direct_debit, objects);
		this.mUtil = new PaymentUtil(getContext());
		this.mDateUtil = new DateUtil(getContext());
		this.mMoneyAccounts = moneyAccounts;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = null;
		ViewHolder holder;
		
		final DirectDebitMandateTO directDebitMandate = getItem(position);
		final MoneyAccountTO moneyAccount = findMoneyAccountById(directDebitMandate.getMoneyAccountId());

		// In case we have a convertView, reuse it instead of taking more
		// memory.
		if (convertView == null) {
			row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_row_direct_debit, null);
			row.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Activity activity = ((Activity)getContext());
					Intent intent = new Intent(activity, DirectDebitDetails.class);
					intent.putExtra(DirectDebitDetails.EXTRA_MANDATE, directDebitMandate);
					intent.putExtra(DirectDebitDetails.EXTRA_ACCOUNT_MANDATE, moneyAccount);
					activity.startActivity(intent);
				}
			});
			holder = new ViewHolder();
			holder.textName = (TextView) row.findViewById(R.id.pmt_row_direct_debit_beneficiary);
			holder.textDebitAccount = (TextView) row.findViewById(R.id.pmt_row_direct_debit_account);
			holder.statusContainer = (LinearLayout) row.findViewById(R.id.status_container);
			if (BankletActivityDelegate.isLargeDevice((Activity)getContext())){
			//if(row.findViewById(R.id.pmt_row_standing_order_large) != null) 
				holder.textDebitAccountNumber = (TextView) row.findViewById(R.id.pmt_row_direct_debit_account_number);
				holder.textOwnerTxt = (TextView) row.findViewById(R.id.pmt_row_direct_debit_owner_txt);
				holder.textDate = (TextView) row.findViewById(R.id.pmt_row_direct_debit_closed_date);
			}
			row.setTag(holder);
		} else {
			row = convertView;
			holder = (ViewHolder) convertView.getTag();
		}

		Log.d(TAG, "position: " + position);

		holder.textName.setText(directDebitMandate.getText());
		TextView status;
		if (directDebitMandate.getStatus() == DdMandateStatusType.OPEN)
			status = (TextView)((Activity)getContext()).getLayoutInflater().inflate(R.layout.avq_status_open, holder.statusContainer, false);
		else
			status = (TextView)((Activity)getContext()).getLayoutInflater().inflate(R.layout.avq_status_canceled, holder.statusContainer, false);
		status.setText(mUtil.getDirectDebitStatusString(directDebitMandate.getStatus()));
		holder.statusContainer.addView(status);

		if (BankletActivityDelegate.isSmallDevice((Activity)getContext())) {
			holder.textDebitAccount.setText(moneyAccount.getAccountType() + " - " + moneyAccount.getAccountNo()); // TODO iban is most likely wrong here, what else can we set?
		} else {
			holder.textDebitAccount.setText(moneyAccount.getAccountType()); // TODO avq_activity_empty
			holder.textDebitAccountNumber.setText(moneyAccount.getAccountNo()); // TODO same as above
			if (directDebitMandate.getCloseDate() != null)
				holder.textDate.setText(mDateUtil.format(directDebitMandate.getCloseDate()));
			// holder.textOwnerTxt.setText(directDebitMandate.getOwnerText());
		}

		return row;
	}
	
	private MoneyAccountTO findMoneyAccountById(long id) {
		for(MoneyAccountTO moneyAccount : mMoneyAccounts) {
			if(moneyAccount.getId() == id) {
				return moneyAccount;
			}
		}
		throw new RuntimeException("Money account not found: "+id);
	}

}
