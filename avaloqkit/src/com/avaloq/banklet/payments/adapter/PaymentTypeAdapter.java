package com.avaloq.banklet.payments.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.avaloq.afs.aggregation.to.payment.PaymentMaskListResult;
import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentMaskQueryTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.banklet.payments.AccountTransferActivity;
import com.avaloq.banklet.payments.AccountTransferTemplateActivity;
import com.avaloq.banklet.payments.DomesticPaymentActivity;
import com.avaloq.banklet.payments.DomesticPaymentTemplateActivity;
import com.avaloq.banklet.payments.InternationalPaymentActivity;
import com.avaloq.banklet.payments.InternationalPaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissRedPaymentActivity;
import com.avaloq.banklet.payments.SwissRedPaymentTemplateActivity;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMaskListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;

public class PaymentTypeAdapter extends GriddableViewPagerAdapter{
	
	private Activity mActivity;
	private OrderType mOrderType;
	
	public PaymentTypeAdapter(Activity activity, ViewPager pager, OrderType orderType) {
		super(activity, 
				R.array.payment_buttons, 
				activity.getResources().getInteger(R.integer.payment_types_width), 
				activity.getResources().getInteger(R.integer.payment_types_height), 
				(int)activity.getResources().getDimension(R.dimen.payment_button_height), 
				pager);
		mActivity = activity;
		mOrderType = orderType;
	}
	
	public Activity getActivity(){
		return mActivity;
	}

	@Override
	protected View prepareView(int resId, View view) {
		Class<? extends AbstractPaymentActivity> klass = null;
		ImageView image = ((ImageView)view.findViewById(R.id.image));
		
		if (resId == R.layout.pmt_button_account) {
			image.setImageDrawable(PaymentUtil.getOverlayedPaymentDrawable(getActivity(), PaymentType.INTERNAL_PAYMENT, mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				klass = AccountTransferTemplateActivity.class;
			}else{
				klass = AccountTransferActivity.class;
			}
		} else if (resId == R.layout.pmt_button_domestic) {
			image.setImageDrawable(PaymentUtil.getOverlayedPaymentDrawable(getActivity(), PaymentType.DOMESTIC_PAYMENT, mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				klass = DomesticPaymentTemplateActivity.class;
			}else{
				klass = DomesticPaymentActivity.class;
			}
		} else if (resId == R.layout.pmt_button_international) {
			image.setImageDrawable(PaymentUtil.getOverlayedPaymentDrawable(getActivity(), PaymentType.INTERNATIONAL_PAYMENT, mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				klass = InternationalPaymentTemplateActivity.class;
			}else{
				klass = InternationalPaymentActivity.class;
			}
		} else if (resId == R.layout.pmt_button_red) {
			image.setImageDrawable(PaymentUtil.getOverlayedPaymentDrawable(getActivity(), PaymentType.SWISS_RED_PAYMENT_SLIP, mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				klass = SwissRedPaymentTemplateActivity.class;
			}else{
				klass = SwissRedPaymentActivity.class;
			}
		} else if (resId == R.layout.pmt_button_orange) {
			image.setImageDrawable(PaymentUtil.getOverlayedPaymentDrawable(getActivity(), PaymentType.SWISS_ORANGE_PAYMENT_SLIP, mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				klass = SwissOrangePaymentTemplateActivity.class;
			}else{
				klass = SwissOrangePaymentActivity.class;
			}
		} else if (resId == R.layout.pmt_scan_and_pay) {
			image.setImageDrawable(PaymentUtil.getOverlayedScanAndPayDrawable(getActivity(), mOrderType));
			if (mOrderType == OrderType.TEMPLATE){
				// TODO
			}else if (mOrderType == OrderType.STANDING){
				// TODO
			}else {
				startScanAndPayActivity();
			}
		}
		if (klass != null){
			final Intent intent = new Intent(getActivity(), klass);
			
			// when creating a new template, pass additional arguments
			if (mOrderType == OrderType.TEMPLATE){
				intent.putExtra(AbstractPaymentActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_NEW);
			}
			if (mOrderType == OrderType.STANDING)
				intent.putExtra(AbstractPaymentActivity.EXTRA_IS_FROM_STANDING, true);
			
			view.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					mActivity.startActivity(intent);
				}
			});
		}
		return view;
	}
	
	private void startScanAndPayActivity(){
		// TODO: actually start the activity and return the result
		
		scanReady("000000000011000883052000005+ 070070057>", "800001514>");
	}
	
	private void scanReady(String searchText, String extraCodeLine){
		int maxResults = getActivity().getResources().getInteger(R.integer.list_count_elements_per_page);
		
		PaymentMaskQueryTO query = new PaymentMaskQueryTO();
		query.setExtraCodeLine(extraCodeLine);
		query.setSearchText(searchText);
		query.setMaxResultSize(maxResults);

		PaymentMaskListRequest request = PaymentOverviewService.getPaymentMaskList(query, 0l, Long.valueOf(maxResults), new RequestStateEvent<PaymentMaskListRequest>(){
			@Override
			public void onRequestCompleted(PaymentMaskListRequest aRequest) {
				PaymentMaskListResult result = aRequest.getResponse().getData();
				if (result == null) {
					onRequestFailed(aRequest);
					return;
				}
				
				PaymentMaskWrapperTO mask = result.getSuggestionList().get(0);
				startPaymentActivityFromMask(mask);
				
			}
			
			@Override
			public void onRequestFailed(PaymentMaskListRequest aRequest) {
				
			}
		});
		request.setCachePolicy(CachePolicy.NO_CACHE);
		request.initiateServerRequest();
	}
	
	private void startPaymentActivityFromMask(PaymentMaskWrapperTO mask){
		
	}
}	
