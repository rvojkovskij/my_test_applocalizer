package com.avaloq.banklet.payments.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.AccountTransferTemplateActivity;
import com.avaloq.banklet.payments.DomesticPaymentTemplateActivity;
import com.avaloq.banklet.payments.InternationalPaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissOrangePaymentTemplateActivity;
import com.avaloq.banklet.payments.SwissRedPaymentTemplateActivity;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.R;

public class PaymentTemplateListAdapter extends ArrayAdapter<PaymentTemplateInfo>{
	
	Activity mActivity;

	public PaymentTemplateListAdapter(Activity context, List<PaymentTemplateInfo> objects) {
		super(context, R.layout.pmt_fragment_favorite_templates_item, R.id.pmt_favorite_templates_alias, objects);
		mActivity = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(getContext()).inflate(R.layout.pmt_fragment_favorite_templates_item, null, false);
		final PaymentTemplateInfo template = getItem(position);
		
		((TextView)view.findViewById(R.id.pmt_favorite_templates_alias)).setText(template.getAlias());
		((TextView)view.findViewById(R.id.pmt_favorite_templates_debit_account_label)).setText(template.getDebitAccountLabel());
		((TextView)view.findViewById(R.id.pmt_favorite_templates_amount)).setText(template.getAmount().toString());
		((ImageView)view.findViewById(R.id.pmt_favorite_templates_icon)).setImageResource(PaymentUtil.getPaymentDrawable(template.getPaymentType()));

		view.findViewById(R.id.pmt_favorite_templates_container).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Log.d("DDD", template.getPaymentType().name());
				Intent i;
				switch(template.getPaymentType()) {
				
				case SWISS_ORANGE_PAYMENT_SLIP:									
					i = new Intent(mActivity, SwissOrangePaymentTemplateActivity.class);
					i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
					i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
					mActivity.startActivity(i);
					break;
				case SWISS_RED_PAYMENT_SLIP:
					i = new Intent(mActivity, SwissRedPaymentTemplateActivity.class);
					i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
					i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
					mActivity.startActivity(i);
					break;
				case DOMESTIC_PAYMENT:
					i = new Intent(mActivity, DomesticPaymentTemplateActivity.class);
					i.putExtra(DomesticPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
					i.putExtra(DomesticPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
					mActivity.startActivity(i);									
					break;
				case INTERNAL_PAYMENT:									
					i = new Intent(mActivity, AccountTransferTemplateActivity.class);
					i.putExtra(AccountTransferTemplateActivity.EXTRA_ID, template.getPaymentId());
					i.putExtra(AccountTransferTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
					mActivity.startActivity(i);
					break;						
				case INTERNATIONAL_PAYMENT:
					i = new Intent(mActivity, InternationalPaymentTemplateActivity.class);
					i.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
					i.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
					mActivity.startActivity(i);									
					break;								
				default:
					break;
				}
			}
		});
		
		return view;
	}
	
}
