package com.avaloq.banklet.payments.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.aggregation.to.payment.PaymentTO;
import com.avaloq.afs.server.bsp.client.ws.BasePaymentTO;
import com.avaloq.afs.server.bsp.client.ws.BeneficiaryPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.DdMandateStatusType;
import com.avaloq.afs.server.bsp.client.ws.DomesticPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.InternalPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.InternationalPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentStateType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.ReasonedPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.StandingOrderSearchStateType;
import com.avaloq.afs.server.bsp.client.ws.SwissOrangePaymentSlipTO;
import com.avaloq.afs.server.bsp.client.ws.SwissRedPaymentSlipTO;
import com.avaloq.banklet.payments.adapter.PaymentTypeAdapter;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.CustomViewPager;

public class PaymentUtil {
	
	private Context mContext;

	public PaymentUtil(Context aContext) {
		this.mContext = aContext;
	}
	
	public String getPaymentTypeString(PaymentType paymentType) {
		if(paymentType == null) {
			return mContext.getResources().getString(R.string.collective_order_list_transation);
		}
		return getString("pmt_payment_type_" + paymentType.toString().toLowerCase());
	}

	public String getPaymentStatusString(String status) {
		return getString("pmt_payment_status_" + status.toLowerCase());
	}
	
	public String getStandingOrderStatusString(StandingOrderSearchStateType type){
		switch (type){
		case ACTIVE:
			return mContext.getString(R.string.payment_state_type_active);
		case DELETED:
			return mContext.getString(R.string.payment_state_type_deleted);
		case EXPIRED:
			return mContext.getString(R.string.payment_state_type_expired);
		case INACTIVE:
			return mContext.getString(R.string.payment_state_type_inactive);
		case PARTIAL_APPROVED:
			return mContext.getString(R.string.payment_state_type_partial_approve);
		case REJECTED:
			return mContext.getString(R.string.payment_state_type_rejected);
		case TO_BE_APPROVED:
			return mContext.getString(R.string.payment_state_type_deleted);
		default:
			return "";
		}
	}
	
	public String getDirectDebitStatusString(DdMandateStatusType status) {
		return getString("pmt_direct_debit_status_"+status.toString().toLowerCase());
	}
	
	private String getString(String identifier) {
		int resId = mContext.getResources().getIdentifier(identifier, "string", mContext.getPackageName());
		if(resId == 0) {
			throw new RuntimeException("Failed to load a string: " + identifier);
		}
		return mContext.getResources().getString(resId);
	}
	
	public static int getPaymentDrawable(PaymentType type){
		switch(type) {
			case SWISS_ORANGE_PAYMENT_SLIP:
				return R.drawable.pmt_ico_list_slip_orange;
			case SWISS_RED_PAYMENT_SLIP:
				return R.drawable.pmt_ico_list_slip_red;
			case DOMESTIC_PAYMENT:
				return R.drawable.pmt_ico_list_domestic;
			case INTERNATIONAL_PAYMENT:
				return R.drawable.pmt_ico_list_international;
			case INTERNAL_PAYMENT:
				return R.drawable.pmt_ico_list_transfer;
			default:
				return R.drawable.avq_ic_action_appicon;
		}
	}
	
	public static int getPaymentDrawableGrid(Activity context, PaymentType type){
		if (BankletActivityDelegate.isLargeDevice(context)){
			switch(type) {
				case SWISS_ORANGE_PAYMENT_SLIP:
					return R.drawable.pmt_new_slip_orange_tablet;
				case SWISS_RED_PAYMENT_SLIP:
					return R.drawable.pmt_new_slip_red_tablet;
				case DOMESTIC_PAYMENT:
					return R.drawable.pmt_new_domestic_tablet;
				case INTERNATIONAL_PAYMENT:
					return R.drawable.pmt_new_international_tablet;
				case INTERNAL_PAYMENT:
					return R.drawable.pmt_new_transfer_tablet;
				default:
					return R.drawable.avq_ic_action_appicon;
			}
		} else {
			switch(type) {
				case SWISS_ORANGE_PAYMENT_SLIP:
					return R.drawable.pmt_new_slip_orange;
				case SWISS_RED_PAYMENT_SLIP:
					return R.drawable.pmt_new_slip_red;
				case DOMESTIC_PAYMENT:
					return R.drawable.pmt_new_domestic;
				case INTERNATIONAL_PAYMENT:
					return R.drawable.pmt_new_international;
				case INTERNAL_PAYMENT:
					return R.drawable.pmt_new_transfer;
				default:
					return R.drawable.avq_ic_action_appicon;
			}
		}
	}
	
	public static int getStateTextColor(PaymentStateType mPaymentState){
		
		int textColor = R.color.blue;
		
		switch (mPaymentState) {
		case APPROVED:
			textColor = R.color.white;
			break;
		case DELETED_ARCHIVED:
			textColor = R.color.gray;
			break;
		case EDIT:
			textColor = R.color.white;
			break;
		case EXPIRED_STANDING_ORDER:
			textColor = R.color.gray;
			break;
		case INIT:
			textColor = R.color.white;
			break;
		case PARTIAL_APPROVED:
			textColor = R.color.white;
			break;
		case PROCESSED_ARCHIVED:
			textColor = R.color.gray;
			break;
		case PROCESSING:
			textColor = R.color.white;
			break;
		case REJECTED_ARCHIVED:
			textColor = R.color.white;
			break;

		default:
			textColor = R.color.white;
			break;
		}
		
		return textColor;
	}
	
	public static int getStateBackground(PaymentStateType mPaymentState){
		
		int backgroundResource = R.drawable.avq_order_status_open;
		switch (mPaymentState) {
		case APPROVED:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		case DELETED_ARCHIVED:
			backgroundResource = R.drawable.avq_order_status_done;
			break;
		case EDIT:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		case EXPIRED_STANDING_ORDER:
			backgroundResource = R.drawable.avq_order_status_done;
			break;
		case INIT:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		case PARTIAL_APPROVED:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		case PROCESSED_ARCHIVED:
			backgroundResource = R.drawable.avq_order_status_done;
			break;
		case PROCESSING:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		case REJECTED_ARCHIVED:
			backgroundResource = R.drawable.avq_order_status_error;
			break;
		default:
			backgroundResource = R.drawable.avq_order_status_open;
			break;
		}
		return backgroundResource;
	}
	
	public static BasePaymentTO getPaymentSlipFromMask(PaymentMaskWrapperTO mask){
		PaymentTO payment = mask.getPayment();
		BasePaymentTO output;
		
		switch (payment.getPaymentType()){
		case DOMESTIC_PAYMENT:
			DomesticPaymentTO tmpDom = new DomesticPaymentTO();
			tmpDom.setPaymentType(PaymentType.DOMESTIC_PAYMENT);
			
			output = tmpDom;
			break;
		case INTERNAL_PAYMENT:
			InternalPaymentTO tmpInternal = new InternalPaymentTO();
			tmpInternal.setCreditMoneyAccountId(payment.getCreditMoneyAccountId());
			tmpInternal.setPaymentType(PaymentType.INTERNAL_PAYMENT);
					
			// Payment Reason because AccountTransfer is not inheriting from ReasonedPaymentTO
			tmpInternal.setPaymentReason1(payment.getPaymentReason1());
			tmpInternal.setPaymentReason2(payment.getPaymentReason2());
			tmpInternal.setPaymentReason3(payment.getPaymentReason3());
			tmpInternal.setPaymentReason4(payment.getPaymentReason4());
			
			output = tmpInternal;
			break;
		case INTERNATIONAL_PAYMENT:
			InternationalPaymentTO tmpInternational = new InternationalPaymentTO();
			if (payment.getBic() != null && !payment.getBic().equals("")) {
				tmpInternational.setBic(payment.getBic());
			}
			if(payment.getCountryId() != null) {
				tmpInternational.setCountryId(payment.getCountryId());
			}
			tmpInternational.setPaymentType(PaymentType.INTERNATIONAL_PAYMENT);
			
			output = tmpInternational;
			break;
		case SWISS_ORANGE_PAYMENT_SLIP:
			SwissOrangePaymentSlipTO tmpOrange = new SwissOrangePaymentSlipTO();
			tmpOrange.setPcAccount(payment.getPcAccount());
			tmpOrange.setReferenceNumber(payment.getReferenceNumber());
			tmpOrange.setPaymentType(PaymentType.SWISS_ORANGE_PAYMENT_SLIP);
			tmpOrange.setPaymentSubType(payment.getPaymentSubType());
			
			output = tmpOrange;
			break;
		case SWISS_RED_PAYMENT_SLIP:
			SwissRedPaymentSlipTO tmpRed = new SwissRedPaymentSlipTO();
			tmpRed.setPcAccount(payment.getPcAccount());
			tmpRed.setPaymentType(PaymentType.SWISS_ORANGE_PAYMENT_SLIP);
			tmpRed.setPaymentSubType(payment.getPaymentSubType());
			
			output = tmpRed;
			break;
		default:
			return null;
		}
		
		if (payment.getDebitMoneyAccountId() != null)
			output.setDebitMoneyAccountId(payment.getDebitMoneyAccountId());
			

		output.setAmount(payment.getAmount());
		output.setCurrencyId(payment.getCurrencyId());
		output.setSalary(payment.isSalary());
		output.setDebitReference(payment.getDebitReference());
		
		
		if (output instanceof BeneficiaryPaymentTO){
			BeneficiaryPaymentTO beneficiarySlip = (BeneficiaryPaymentTO)output;
			
			beneficiarySlip.setBeneficiary1(payment.getBeneficiary1());
			beneficiarySlip.setBeneficiary2(payment.getBeneficiary2());
			beneficiarySlip.setBeneficiary3(payment.getBeneficiary3());
			beneficiarySlip.setBeneficiary4(payment.getBeneficiary4());
			// Bank details
			beneficiarySlip.setBeneficiaryBank1(payment.getBeneficiaryBank1());
			beneficiarySlip.setBeneficiaryBank2(payment.getBeneficiaryBank2());
			beneficiarySlip.setBeneficiaryBank3(payment.getBeneficiaryBank3());
			beneficiarySlip.setBeneficiaryBank4(payment.getBeneficiaryBank4());
			
			beneficiarySlip.setAdviceOption(payment.getAdviceOption());

			//slip = (PaymentSlip)beneficiarySlip;
		}
		
		if (output instanceof ReasonedPaymentTO){
			ReasonedPaymentTO reasonedSlip = (ReasonedPaymentTO)output;
			reasonedSlip.setBeneficiaryAccountNo(payment.getBeneficiaryAccountNo());
			reasonedSlip.setBeneficiaryIban(payment.getBeneficiaryIban());
			
			reasonedSlip.setPaymentReason1(payment.getPaymentReason1());
			reasonedSlip.setPaymentReason2(payment.getPaymentReason2());
			reasonedSlip.setPaymentReason3(payment.getPaymentReason3());
			reasonedSlip.setPaymentReason4(payment.getPaymentReason4());
			
			if (payment.getNtnl() != null && !payment.getNtnl().equals(""))
				reasonedSlip.setNtnl(payment.getNtnl());
			
			reasonedSlip.setChargeOption(payment.getChargeOption());
			
			//slip = (PaymentSlip)reasonedSlip;
		}

		return output;
	}
	
	public static int getOverlayIcon(Activity context, OrderType order){
		if (order == null)
			return 0;
		if (BankletActivityDelegate.isLargeDevice(context)){
			switch (order){
				case STANDING:
					return R.drawable.pmt_overlay_standing_tablet;
				case TEMPLATE:
					return R.drawable.pmt_overlay_template_tablet;
				case PAYMENT:
					return R.drawable.pmt_overlay_payment_tablet;
				default:
					return 0;
			}
		}
		else {
			switch (order){
				case STANDING:
					return R.drawable.pmt_overlay_standing;
				case TEMPLATE:
					return R.drawable.pmt_overlay_template;
				case PAYMENT:
					return R.drawable.pmt_overlay_payment;
				default:
					return 0;
			}
		}
	}
	
	public static Drawable getOverlayedPaymentDrawable(Activity context, PaymentType type, OrderType order){
		int overlay = getOverlayIcon(context, order);
		if (overlay == 0)
			return context.getResources().getDrawable(getPaymentDrawable(type));
		
		Drawable[] layers = new Drawable[2];
		layers[0] = context.getResources().getDrawable(getPaymentDrawableGrid(context, type));
		layers[1] = context.getResources().getDrawable(overlay);
		LayerDrawable layerDrawable = new LayerDrawable(layers);
		return layerDrawable;
	}
	
	public static Drawable getOverlayedScanAndPayDrawable(Activity context, OrderType order){
		int resId = (BankletActivityDelegate.isLargeDevice(context)) ? R.drawable.pmt_new_scan_tablet : R.drawable.pmt_new_scan;
		int overlay = getOverlayIcon(context, order);
		if (overlay == 0)
			return context.getResources().getDrawable(resId);
		
		Drawable[] layers = new Drawable[2];
		layers[0] = context.getResources().getDrawable(resId);
		layers[1] = context.getResources().getDrawable(overlay);
		LayerDrawable layerDrawable = new LayerDrawable(layers);
		return layerDrawable;
	}
	
	public enum OrderType {
		PAYMENT, STANDING, TEMPLATE;
	};
	
	public enum OrderAction {
		APPROVE, EDIT, DELETE, COPY, CONFIRM, VERIFY
	}
	
	public static View getOverviewHeader(Activity activity, ListView list, OrderType type){
		View view = LayoutInflater.from(activity).inflate(R.layout.pmt_payment_overview_pager, null, false);
		CustomViewPager pager = (CustomViewPager)view.findViewById(R.id.pager);
		pager.setScrollView(list);
		pager.setAdapter(new PaymentTypeAdapter(activity, pager, type));
		return view;
	}
	
	public int getPaymentStringId(OrderAction action, OrderType type){
		String key = "pmt_template_"+action.toString().toLowerCase()+"_"+type.toString().toLowerCase();
		int resId = mContext.getResources().getIdentifier(key, "string", mContext.getPackageName());
		if (resId == 0)
			Log.d("Test", "ccccccc not found: "+key);
		return resId;
	}
	
}
