package com.avaloq.banklet.payments.util;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public abstract class AbstractLoadingPaymentFragment extends BankletFragment {

	private LinearLayout contentView = null;
	private LinearLayout errorLayout = null;
	private TextView errorText = null;
	private Button refreshButton = null;
	private ProgressBar progressBar = null;
	private TextView noResultsText = null;
	
	protected boolean isInitialResume = true;
	
	@Override
	public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.pmt_fragment_abstract, container, shouldAttachToRoot());
	}
	
	@Override
	public final void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		isInitialResume = true;
		contentView = (LinearLayout)view.findViewById(R.id.pmt_fragment_abstract_content);
		errorLayout = (LinearLayout)view.findViewById(R.id.pmt_fragment_abstract_error);
		errorText = (TextView)view.findViewById(R.id.pmt_fragment_abstract_error_text);
		refreshButton = (Button)view.findViewById(R.id.pmt_fragment_abstract_error_refresh);
		progressBar = (ProgressBar)view.findViewById(R.id.pmt_fragment_abstract_loader);
		noResultsText = (TextView) view.findViewById(R.id.pmt_fragment_abstract_no_results);
		refreshButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				refreshData();
			}
		});
		
		
		doInitialLayout(getLayoutInflater(savedInstanceState).inflate(getLayoutId(), contentView, true));
		refreshData();
	}
	
	protected boolean refreshOnResume(){
		return false;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (refreshOnResume() && !isInitialResume){
			loadData();
		}
		isInitialResume = false;
	}
	
	public abstract int getLayoutId();
	
	protected void doInitialLayout(View view) {
	}
	
	protected boolean shouldAttachToRoot() {
		return false;
	}

	public abstract void loadData();
	
	public final void refreshData() {
		contentView.setVisibility(View.GONE);
		errorLayout.setVisibility(View.GONE);
		progressBar.setVisibility(View.VISIBLE);
		loadData();
	}
	
	protected final void showError(String message) {
		contentView.setVisibility(View.GONE);
		progressBar.setVisibility(View.GONE);
		errorLayout.setVisibility(View.VISIBLE);
		errorText.setText(message);
	}
	
	protected final void showError(final int resId) {
		showError(getActivity().getResources().getString(resId));
	}
	
	protected final void showError(final int resId, Object ... formatArgs) {
		showError(getActivity().getResources().getString(resId, formatArgs));
	}
	
	protected final void setContentReady() {
		contentView.setVisibility(View.VISIBLE);
		progressBar.setVisibility(View.GONE);
		errorLayout.setVisibility(View.GONE);
	}
	
	protected final void setNoResultsMessage(String noResultsMessage){
		noResultsText.setText(noResultsMessage);
	}
	
	protected final void setNoResultsMessageVisibility(boolean visible){
		if (visible) showNoResultsMessage(); else hideNoResultsMessage();
	}
	
	protected final void showNoResultsMessage(){
		noResultsText.setVisibility(View.VISIBLE);
	}
	
	protected final void hideNoResultsMessage(){
		noResultsText.setVisibility(View.GONE);
	}
}
