package com.avaloq.banklet.payments;

import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;

public class Configuration {
	
	private static CurrencyTO defaultCurrency = null;
	private static Long defaultCurrencyId = null;
	
	/**
	 * Gets the default currency as an ISO
	 */
	public static String getDefaultCurrencyIsoCode(){
		return AvaloqApplication.getContext().getResources().getString(R.string.pmt_config_default_currency_iso_code);
	}
	
	public static long getDefaultCurrencyId(){
		if (defaultCurrencyId == null){
			defaultCurrencyId = AvaloqApplication.getInstance().findCurrencyByISOCode(getDefaultCurrencyIsoCode());
		}
		
		return defaultCurrencyId;
	}
	
	public static CurrencyTO getDefaultCurrency(){
		if (defaultCurrency == null){
			defaultCurrency = AvaloqApplication.getInstance().findCurrencyById(getDefaultCurrencyId());
		}
		
		return defaultCurrency;
	}
}
