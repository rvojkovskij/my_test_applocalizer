package com.avaloq.banklet.payments;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ListView;

import com.avaloq.afs.aggregation.to.payment.PaymentCordOrderResult;
import com.avaloq.afs.aggregation.to.payment.PaymentCordOrderTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentCordQueryTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentStateType;
import com.avaloq.banklet.payments.adapter.CollectiveOrdersAdapter;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentCordOrderRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.DateUtil;
import com.avaloq.framework.R;

public class CollectiveOrders extends BankletActivity{
	
	public static final String EXTRA_COLLECTIVE_ORDER = "extra_collective_order";
	public static final int ENTRIES_PER_PAGE = 25;
	Long orderId;
	int currentPage = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pmt_collective_order);
		
		if (getIntent().getExtras() == null || !getIntent().getExtras().containsKey(EXTRA_COLLECTIVE_ORDER))
			throw new IllegalArgumentException("The extra EXTRA_COLLECTIVE_ORDER is not set.");
		
		orderId = getIntent().getExtras().getLong(EXTRA_COLLECTIVE_ORDER);
		
		loadData();
	}
	
	protected void loadData(){
		PaymentCordQueryTO query = new PaymentCordQueryTO();
		PaymentCordOrderRequest request = PaymentOverviewService.getCordPayment(orderId, query, (long)(currentPage-1)*ENTRIES_PER_PAGE, (long)ENTRIES_PER_PAGE, new RequestStateEvent<PaymentCordOrderRequest>(){
			@Override
			public void onRequestCompleted(PaymentCordOrderRequest aRequest) {
				PaymentCordOrderResult result = aRequest.getResponse().getData();
				if (result != null){
					findViewById(R.id.loading_container).setVisibility(View.GONE);
					
					PaymentCordOrderTO order = result.getPaymentCordOrder();
					if (order != null) {
						DateUtil dateUtil = new DateUtil(CollectiveOrders.this);
						ImageView image = (ImageView)findViewById(R.id.image);
						if (image != null)
							image.setImageResource(R.drawable.pmt_ico_entered_payments);
						((TextView)findViewById(R.id.collective_order_amount)).setText(CurrencyUtil.formatMoney(order.getAmount(), result.getCurrency().getId()));
						TextView count_large = (TextView)findViewById(R.id.collective_order_count);
						TextView count_small = (TextView)findViewById(R.id.collective_order_count_small);
						if (count_large != null && order.getNumberOfPayments() != null)
							count_large.setText(String.valueOf(order.getNumberOfPayments()));
						else if (count_small != null && order.getNumberOfPayments() != null){
							if (order.getNumberOfPayments() > 1)
								count_small.setText(getString(R.string.pmt_count_many_payments).replace("%Count%", String.valueOf(order.getNumberOfPayments())));
							else
								count_small.setText(getString(R.string.pmt_count_one_payment));
						}
						((TextView)findViewById(R.id.collective_order_date)).setText(dateUtil.format(order.getTransactionDate()));
						View status = getStateView(order.getPaymentState());
						((LinearLayout)findViewById(R.id.collective_order_status_container)).addView(status);
						ListView list = (ListView)findViewById(R.id.collective_order_list);
						list.setAdapter(new CollectiveOrdersAdapter(CollectiveOrders.this, order.getPaymentInfoList()));
						
						((Button)findViewById(R.id.collective_order_button_allow)).setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
							}
						});
						
						((Button)findViewById(R.id.collective_order_button_delete)).setOnClickListener(new OnClickListener() {
							
							@Override
							public void onClick(View v) {
								
							}
						});
					}
					else
						onRequestFailed(aRequest);
				}
				else {
					onRequestFailed(aRequest);
				}
			}
			
			@Override
			public void onRequestFailed(PaymentCordOrderRequest aRequest) {
				Log.d("Test", "ccccccc fail");
			}
		});
		request.setCachePolicy(CachePolicy.NO_CACHE);
		request.initiateServerRequest();
	}
	
	protected View getStateView(PaymentStateType type){
		TextView view = (TextView)getLayoutInflater().inflate(R.layout.avq_status_open, null, false);
		
		// TODO: Add translations
		view.setText(type.toString());
		
		return view;
	}
}
