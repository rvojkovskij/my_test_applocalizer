package com.avaloq.banklet.payments.methods;

import com.avaloq.afs.aggregation.to.payment.PaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.StandingOrderTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;

public abstract class VerifyMethod<PaymentOrder extends PaymentOrderTO, PaymentRequest extends AbstractServerRequest<?>, StandingOrder extends StandingOrderTO, StandingRequest extends AbstractServerRequest<?>> {
	protected AbstractPaymentActivity<?, PaymentRequest, PaymentOrder, ?, ?, StandingRequest, StandingOrder, ?, ?, ?, ?, ?> mActivity;
	
	public VerifyMethod(AbstractPaymentActivity<?, PaymentRequest, PaymentOrder, ?, ?, StandingRequest, StandingOrder, ?, ?, ?, ?, ?> activity){
		mActivity = activity;
	}
	
	public void run(){
		if (!mActivity.validatePayment()){
			return;
		}
		
		if (mActivity.isViewFromStanding()){
			StandingOrder order = getStandingOrderForMethod();
			
			RequestStateEvent<StandingRequest> rse = new RequestStateEvent<StandingRequest>() {
				@Override
				public void onRequestCompleted(StandingRequest request) {
					submissionResultStanding(request);
				}
			};
			
			// Initiate the request
			StandingRequest request = getStandingRequest(order, rse);
			request.setCachePolicy(CachePolicy.NO_CACHE);
			request.initiateServerRequest();
		}
		else {
			PaymentOrder order = getPaymentOrderForMethod();
			
			RequestStateEvent<PaymentRequest> rse = new RequestStateEvent<PaymentRequest>() {
				@Override
				public void onRequestCompleted(PaymentRequest request) {	
					submissionResultPayment(request);
				}			
			};
			
			// Initiate the request 
			PaymentRequest request = getPaymentRequest(order, rse);
			request.setCachePolicy(CachePolicy.NO_CACHE);
			request.initiateServerRequest();
		}
		
		onFinished();
	}
	
	protected void submissionResultStanding(StandingRequest request){}
	protected void submissionResultPayment(PaymentRequest request){}
	protected void onFinished(){
		mActivity.showProgress();
	}
	
	protected abstract PaymentRequest getPaymentRequest(PaymentOrder order, RequestStateEvent<PaymentRequest> rse);
	protected abstract StandingRequest getStandingRequest(StandingOrder order, RequestStateEvent<StandingRequest> rse);
	protected abstract PaymentOrder getPaymentOrderForMethod();
	protected abstract StandingOrder getStandingOrderForMethod();
}
