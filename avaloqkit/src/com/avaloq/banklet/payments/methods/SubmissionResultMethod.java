package com.avaloq.banklet.payments.methods;

import java.util.List;

import com.avaloq.afs.aggregation.to.LocalizedNotification;
import com.avaloq.afs.aggregation.to.payment.BasePaymentResult;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.banklet.payments.AbstractPaymentActivity.SaveTemplateType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;

public abstract class SubmissionResultMethod
	<
		PaymentResult extends BasePaymentResult, 
		PaymentRequest extends AbstractServerRequest<PaymentResult>,
		StandingResult extends BasePaymentResult, 
		StandingRequest extends AbstractServerRequest<StandingResult>
	> {
	
	AbstractPaymentActivity<PaymentResult, PaymentRequest, ?, ?, StandingResult, StandingRequest, ?, ?, ?, ?, ?, ?> mActivity;
	
	public SubmissionResultMethod(AbstractPaymentActivity<PaymentResult, PaymentRequest, ?, ?, StandingResult, StandingRequest, ?, ?, ?, ?, ?, ?> activity){
		mActivity = activity;
	}
	
	protected void run(List<LocalizedNotification> notifications, final Long orderId, final boolean isVerification){
		
		mActivity.displayErrors(notifications, new Runnable() {
			
			@Override
			public void run() {
				if (isVerification){
					// remove the error fields
					mActivity.resetErrors();
					
					// set all the fields as read-only
					mActivity.setConfirmMode();
				}else{
					mActivity.resetErrors();
					// after confirming an order, open the view to save this order as a template
					if (mActivity.isSaveFromTemplate()){
						if (orderId == null){
							mActivity.showSuccessfulSubmission("", mActivity.getString(R.string.pmt_view_field_success), orderId, getSaveTemplateType());
						}else{
							mActivity.showSuccessfulSubmission("", mActivity.getString(R.string.pmt_view_field_success_updated), orderId, getSaveTemplateType());
						}
					}else{						
						mActivity.showSuccessfulSubmission("", mActivity.getString(R.string.pmt_view_field_success), orderId, null);
					}
				}	
			}
		});
	}
	
	public void submissionPaymentResult(PaymentRequest request, boolean isVerification){
		mActivity.hideProgress();
		
		if (request.getResponse().getData() != null) {
			Long orderId = null;
			try {
				orderId = getPaymentOrderId(request.getResponse().getData());
			}
			catch (Exception e){}
			run(request.getResponse().getData().getNotificationList(), orderId, isVerification);
		}		
	}
	
	public void submissionResultStanding(StandingRequest request, boolean isVerification){
		mActivity.hideProgress();
		
		if (request.getResponse().getData() != null) {
			Long orderId = null;
			try {
				orderId = getStandingOrderId(request.getResponse().getData());
			}
			catch (Exception e){}
			run(request.getResponse().getData().getNotificationList(), orderId, isVerification);
		}
	}
	
	public Long getPaymentOrderId(PaymentResult result) {
		return mActivity.getViewDataMethod().getPaymentOrder(result).getId();
	}
	
	public Long getStandingOrderId(StandingResult result) {
		return mActivity.getViewDataMethod().getStandingOrder(result).getId();
	}
	
	public abstract SaveTemplateType getSaveTemplateType();
}
