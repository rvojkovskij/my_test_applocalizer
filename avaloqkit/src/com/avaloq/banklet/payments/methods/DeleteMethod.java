package com.avaloq.banklet.payments.methods;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;

import com.avaloq.afs.aggregation.to.payment.BasePaymentOrderTO;
import com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentInfoRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;

public abstract class DeleteMethod
	<
		PaymentOrder extends BasePaymentOrderTO,
		StandingOrder extends BasePaymentOrderTO
	> {
	
	protected AbstractPaymentActivity<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> mActivity;
	
	public DeleteMethod(AbstractPaymentActivity activity){
		mActivity = activity;
	}
	
	public void run(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
		alertDialogBuilder.setTitle(R.string.pmt_please_confirm)
			.setMessage(R.string.pmt_delete_confirm_message)
			.setNegativeButton(R.string.avq_no, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					
				}
			})
			.setPositiveButton(R.string.avq_yes, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					PaymentOrder order = getPaymentOrderForMethod();
					
					Long paymentId = null;
					if (order != null)
						paymentId = order.getId();
					if (paymentId != null && paymentId != 0){
						mActivity.showProgress();
						BaseBankingObjectTO id = new BaseBankingObjectTO();
						id.setId(paymentId);
						id.setVersionId(order.getVersionId());
						PaymentInfoRequest request = PaymentOverviewService.deleteOpenPayment(id, new RequestStateEvent<PaymentInfoRequest>() {
							@Override
							public void onRequestCompleted(PaymentInfoRequest aRequest) {
								mActivity.hideProgress();
								
								mActivity.displayErrors(aRequest.getResponse().getData().getNotificationList(), new Runnable() {
									
									@Override
									public void run() {
										AlertDialog.Builder readyBuilder = new AlertDialog.Builder(mActivity);
										readyBuilder.setTitle(R.string.pmt_template_delete_payment)
											.setMessage(R.string.pmt_deleted_successfully)
											.setPositiveButton(R.string.avq_ok, new OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													mActivity.finish();
												}
											})
											.setOnCancelListener(new OnCancelListener() {
												@Override
												public void onCancel(DialogInterface dialog) {
													mActivity.finish();
												}
											});
										readyBuilder.create().show();
									}
								});								
							}
						});
						
						request.setCachePolicy(CachePolicy.NO_CACHE);
						request.initiateServerRequest();
					}
				}
			}).create().show();
	}
	
	protected abstract PaymentOrder getPaymentOrderForMethod();
	protected abstract StandingOrder getStandingOrderForMethod();
}
