package com.avaloq.banklet.payments.methods;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.avaloq.afs.aggregation.to.payment.BasePaymentResult;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateTO;
import com.avaloq.afs.server.bsp.client.ws.BaseBankingObjectTO;
import com.avaloq.afs.server.bsp.client.ws.BasePaymentTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoRequest;

public abstract class TemplateMethod
<
	PaymentSlip extends BasePaymentTO,
	TemplateResult extends BasePaymentResult,
	TemplateRequest extends AbstractServerRequest<TemplateResult>,
	Template extends PaymentTemplateTO
> {
	
	private static final String TAG = TemplateMethod.class.getSimpleName();
	
	AbstractPaymentActivity<?, ?, ?, PaymentSlip, ?, ?, ?, TemplateResult, TemplateRequest, Template, ?, ?> mActivity;
	
	public TemplateMethod(AbstractPaymentActivity<?, ?, ?, PaymentSlip, ?, ?, ?, TemplateResult, TemplateRequest, Template, ?, ?> activity){
		mActivity = activity;
	}
	
	public void save(){
		final Template template;
		if (mActivity.getViewDataMethod().getTemplateResult() == null)
			template = createEmptyTemplate();
		else
			template = mActivity.getViewDataMethod().getPaymentTemplateFromRequest();
		
		setPayment(template, mActivity.getPayment());
		template.setAlias(mActivity.mFieldAlias.getValue());
		template.setFavorite(mActivity.mFieldTemplateFavorite.isChecked());
		
		TemplateRequest verify = getVerifyRequest(template, new RequestStateEvent<TemplateRequest>() {
            @Override
            public void onRequestCompleted(TemplateRequest aRequest) {

                mActivity.hideProgress();
                mActivity.displayErrors(aRequest.getResponse().getData().getNotificationList(), new Runnable() {
					
					@Override
					public void run() {
	                	TemplateRequest submit = getSubmitRequest(template, new RequestStateEvent<TemplateRequest>() {
	                        @Override
	                        public void onRequestCompleted(TemplateRequest aRequest) {
	                        	TemplateResult result = aRequest.getResponse().getData();

	                        	mActivity.hideProgress();
	                        	if (result != null) {
	                    			
	                    			mActivity.displayErrors(result.getNotificationList(), new Runnable() {
										
										@Override
										public void run() {
											if (template.getId() != null){
												mActivity.showSuccessfulSubmission("", mActivity.getString(R.string.pmt_view_field_template_updated_successfully), 0, null);
											}else{
												mActivity.showSuccessfulSubmission("", mActivity.getString(R.string.pmt_view_field_template_created_successfully), 0, null);
											}
										}
									});
	                        	}
	                        }
	                        @Override
	                        public void onRequestFailed(TemplateRequest aRequest) {
	                        	mActivity.hideProgress();
	                        }
	                    });
	                	submit.initiateServerRequest();
					}
				});                	
            }
            @Override
            public void onRequestFailed(TemplateRequest aRequest) {
                Log.d(TAG, "request failed.");
            }
        });
		
		verify.initiateServerRequest();
        mActivity.showProgress();
	}
	
	public void delete(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivity);
		alertDialogBuilder.setTitle(R.string.pmt_please_confirm)
		.setMessage(R.string.pmt_delete_template_confirm_message)
		.setNegativeButton(R.string.avq_no, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		})
		.setPositiveButton(R.string.avq_yes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Template template = mActivity.getViewDataMethod().getPaymentTemplateFromRequest();
				BaseBankingObjectTO param = new BaseBankingObjectTO();
				param.setId(template.getId());
				param.setVersionId(template.getVersionId());
				
				mActivity.showProgress();
				
				PaymentTemplateInfoRequest request = PaymentOverviewService.deletePaymentTemplate(param, new RequestStateEvent<PaymentTemplateInfoRequest>() {
					@Override
					public void onRequestCompleted(PaymentTemplateInfoRequest aRequest) {
						mActivity.hideProgress();
						
						mActivity.displayErrors(aRequest.getResponse().getData().getNotificationList(), new Runnable() {
							
							@Override
							public void run() {
								AlertDialog.Builder readyBuilder = new AlertDialog.Builder(mActivity);
								readyBuilder.setTitle(R.string.pmt_template_delete_template)
									.setMessage(R.string.pmt_template_deleted_successfully)
									.setPositiveButton(R.string.avq_ok, new OnClickListener() {
										@Override
										public void onClick(DialogInterface dialog, int which) {
											mActivity.finish();
										}
									})
									.setOnCancelListener(new OnCancelListener() {
										@Override
										public void onCancel(DialogInterface dialog) {
											mActivity.finish();
										}
									});
								readyBuilder.create().show();
							}
						});						
					}
					
					@Override
					public void onRequestFailed(PaymentTemplateInfoRequest aRequest) {
						
					}
				});
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
			}
		}).create().show();
	}
	
	protected abstract void setPayment(Template template, PaymentSlip payment);
	protected abstract TemplateRequest getVerifyRequest(Template template, RequestStateEvent<TemplateRequest> rse);
	protected abstract TemplateRequest getSubmitRequest(Template template, RequestStateEvent<TemplateRequest> rse);
	protected abstract Template createEmptyTemplate();
}
