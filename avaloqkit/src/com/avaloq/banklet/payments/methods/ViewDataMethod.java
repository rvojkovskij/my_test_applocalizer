package com.avaloq.banklet.payments.methods;

import android.view.View;

import com.avaloq.afs.aggregation.to.Result;
import com.avaloq.afs.aggregation.to.payment.BasePaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.BasePaymentResult;
import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.PaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.PaymentTO;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.StandingOrderTO;
import com.avaloq.afs.server.bsp.client.ws.BasePaymentTO;
import com.avaloq.afs.server.bsp.client.ws.BeneficiaryPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentStateType;
import com.avaloq.afs.server.bsp.client.ws.ReasonedPaymentTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.banklet.payments.Configuration;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.views.StandingOrderField.ExecuteUntilType;
import com.avaloq.banklet.payments.views.StandingOrderField.ExecutionType;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;

public abstract class ViewDataMethod
<
	PaymentResult extends BasePaymentResult, 
	PaymentRequest extends AbstractServerRequest<PaymentResult>, 
	PaymentOrder extends PaymentOrderTO,
	PaymentSlip extends BasePaymentTO,
	StandingResult extends BasePaymentResult, 
	StandingRequest extends AbstractServerRequest<StandingResult>,
	StandingOrder extends StandingOrderTO,
	TemplateResult extends BasePaymentResult,
	TemplateRequest extends AbstractServerRequest<TemplateResult>,
	Template extends PaymentTemplateTO,
	DefaultsResult extends Result,
	DefaultsRequest extends AbstractServerRequest<DefaultsResult>
>{
	protected AbstractPaymentActivity<PaymentResult, PaymentRequest, PaymentOrder, PaymentSlip, StandingResult, StandingRequest, StandingOrder, TemplateResult, TemplateRequest, Template, DefaultsResult, DefaultsRequest> mActivity;
	protected PaymentResult viewResult;
	protected StandingResult viewStandingResult;
	protected TemplateResult templateResult;
	protected DefaultsResult defaultsResult;
	protected PaymentViewType mPaymentViewType;
	protected PaymentStateType mPaymentState = PaymentStateType.INIT;
	
	protected PaymentMaskWrapperTO mMask;
	
	MoneyAccountTO defaultDebitAccount = null;
	
	public ViewDataMethod(AbstractPaymentActivity<PaymentResult, PaymentRequest, PaymentOrder, PaymentSlip, StandingResult, StandingRequest, StandingOrder, TemplateResult, TemplateRequest, Template, DefaultsResult, DefaultsRequest> aActivity){
		mActivity = aActivity;
	}
	
	public PaymentViewType getViewType(){
		return mPaymentViewType;
	}
	
	public void setViewType(PaymentViewType type){
		mPaymentViewType = type;
	}
	
	public void setMask(PaymentMaskWrapperTO mask){
		mMask = mask;
	}
	
	public PaymentStateType getState(){
		return mPaymentState;
	}
	
	public void setState(PaymentStateType state){
		mPaymentState = state;
	}
	
	public void populateFields(){
		if (mActivity.getIsConfirmMode())
			return;
		// Used only to determine if the payment slip inherits from one of the additional
		// base payment classes - BeneficiaryPaymentTO and ReasonedPaymentTO
		PaymentSlip emptySlip = createEmptyPaymentSlip();
		
		mActivity.mFieldAccount.setMoneyAccounts(getPaymentDefaultsFromRequest().getDebitMoneyAccounts());
		mActivity.mFieldAmount.setCurrencyId(Configuration.getDefaultCurrencyId());
		/*mActivity.mFieldStandingOrder.setOnConfirmOrderDialog(new Runnable(){
			@Override
			public void run() {
				if (mActivity.isStandingOrderMode())
					mActivity.mFieldExecutionDate.setVisibility(View.GONE);
				else
					mActivity.mFieldExecutionDate.setVisibility(View.VISIBLE);
			}
		});*/
		
		if (mPaymentViewType == PaymentViewType.TEMPLATE || mPaymentViewType == PaymentViewType.TEMPLATE_FROM_VIEW || mActivity.isViewFromStanding()){
			mActivity.mFieldExecutionDate.setVisibility(View.GONE);
		}
		
		// Set the debit account		 
		// Find which is the default debit account, having the id of that account
		if (mActivity.mPaymentSettings != null){
			// Loop through the debit accounts to find the one that has the ID equalto the default one
			if (mActivity.mPaymentSettings.getDefaultPaymentAccount() != null){
				for (MoneyAccountTO moneyAccount : getPaymentDefaultsFromRequest().getDebitMoneyAccounts()) {
					if (moneyAccount.getId().longValue() == mActivity.mPaymentSettings.getDefaultPaymentAccount().longValue()) {
						defaultDebitAccount = moneyAccount;
						break;
					}
				}
			}
			mActivity.mFieldAccount.setDefaultAccountId(mActivity.mPaymentSettings.getDefaultPaymentAccount());

			// if the account was found, assign it to the field
			if (defaultDebitAccount != null && mActivity.mFieldAccount.getSelectedMoneyAccountTO() == null) {
				mActivity.mFieldAccount.setSelectedMoneyAccountTO(defaultDebitAccount);
			}
		}
		
		if (mActivity.mFieldAccount.getSelectedMoneyAccountTO() == null){
			if (getPaymentDefaultsFromRequest().getCreditMoneyAccounts() != null && getPaymentDefaultsFromRequest().getCreditMoneyAccounts().size() > 0){
				defaultDebitAccount = getPaymentDefaultsFromRequest().getCreditMoneyAccounts().get(0);
				mActivity.mFieldAccount.setSelectedMoneyAccountTO(defaultDebitAccount);
			}
		}
		
		mActivity.mFieldAmount.setCurrencies(getPaymentDefaultsFromRequest().getPaymentCurrencies());
		
		// if there is some data available, put it in the fields
		if (templateResult != null && !mActivity.getIsConfirmMode()) {
			
			mActivity.mFieldAlias.setValue(getPaymentTemplateFromRequest().getAlias());
			mActivity.mFieldTemplateFavorite.setChecked(getPaymentTemplateFromRequest().isFavorite());
			mActivity.mFieldAccount.setSelectedMoneyAccountTO(templateResult.getDebitMoneyAccount());
			
			mActivity.mFieldAmount.setAmount(getSlipFromTemplateRequest().getAmount());
			mActivity.mFieldAmount.setCurrencyId(getSlipFromTemplateRequest().getCurrencyId());
			mActivity.mFieldAmount.setCurrencies(getPaymentDefaultsFromRequest().getPaymentCurrencies());

			mActivity.mFieldSalaryPayment.setChecked(getSlipFromTemplateRequest().isSalary());
			mActivity.mFieldDebitReference.setValue(getSlipFromTemplateRequest().getDebitReference());
			
			if (emptySlip instanceof BeneficiaryPaymentTO){
				BeneficiaryPaymentTO beneficiarySlip = (BeneficiaryPaymentTO)getSlipFromTemplateRequest();
				
				mActivity.mFieldBeneficiary.show();
				
				mActivity.mFieldBeneficiary.setBankDetails(new String[] { 
					beneficiarySlip.getBeneficiaryBank1(), beneficiarySlip.getBeneficiaryBank2(),
					beneficiarySlip.getBeneficiaryBank3(), beneficiarySlip.getBeneficiaryBank4() });

				// IN FAVOR OF (BENEFICIARY)
				mActivity.mFieldBeneficiary.setBeneficiary(new String[] { 
					beneficiarySlip.getBeneficiary1(), beneficiarySlip.getBeneficiary2(),
					beneficiarySlip.getBeneficiary3(), beneficiarySlip.getBeneficiary4() });
				
				mActivity.mFieldDebitAdvice.setAdviceOption(beneficiarySlip.getAdviceOption());
			}
			
			if (emptySlip instanceof ReasonedPaymentTO){
				ReasonedPaymentTO reasonedSlip = (ReasonedPaymentTO)getSlipFromTemplateRequest();
				
				if (reasonedSlip.getNtnl() != null && !reasonedSlip.getNtnl().equals(""))
					mActivity.mFieldBeneficiary.setNtnl(reasonedSlip.getNtnl());
				mActivity.mFieldBeneficiary.setAccount(reasonedSlip.getBeneficiaryAccountNo());
				mActivity.mFieldBeneficiary.setAccountIBAN(reasonedSlip.getBeneficiaryIban());
				
				mActivity.mFieldChargeOptions.setChargeOption(reasonedSlip.getChargeOption());
				
				mActivity.mFieldPaymentDetails.setPaymentReasons(new String[]{
					reasonedSlip.getPaymentReason1(), reasonedSlip.getPaymentReason2(),
					reasonedSlip.getPaymentReason3(), reasonedSlip.getPaymentReason4()
				});
			}
		}
		
		PaymentSlip paymentSlip = getPaymentFromRequest();
		if (paymentSlip != null  && !mActivity.getIsConfirmMode()){
			mActivity.mFieldAmount.setAmount(paymentSlip.getAmount());
			mActivity.mFieldAmount.setCurrencyId(paymentSlip.getCurrencyId());

			mActivity.mFieldSalaryPayment.setChecked(paymentSlip.isSalary());
			mActivity.mFieldDebitReference.setValue(paymentSlip.getDebitReference());
			
			if (paymentSlip instanceof BeneficiaryPaymentTO){
				BeneficiaryPaymentTO beneficiarySlip = (BeneficiaryPaymentTO)paymentSlip;
				mActivity.mFieldBeneficiary.setBankDetails(new String[] {
					beneficiarySlip.getBeneficiaryBank1(), beneficiarySlip.getBeneficiaryBank2(),
					beneficiarySlip.getBeneficiaryBank3(), beneficiarySlip.getBeneficiaryBank4() });

				// IN FAVOR OF (BENEFICIARY)
				mActivity.mFieldBeneficiary.setBeneficiary(new String[] { 
					beneficiarySlip.getBeneficiary1(), beneficiarySlip.getBeneficiary2(),
					beneficiarySlip.getBeneficiary3(), beneficiarySlip.getBeneficiary4() });	
				
				mActivity.mFieldBeneficiary.show();
				
				mActivity.mFieldDebitAdvice.setAdviceOption(beneficiarySlip.getAdviceOption());
			}
			
			if (paymentSlip instanceof ReasonedPaymentTO){
				ReasonedPaymentTO reasonedSlip = (ReasonedPaymentTO)paymentSlip;
				
				mActivity.mFieldPaymentDetails.setPaymentReasons(new String[]{
					reasonedSlip.getPaymentReason1(), reasonedSlip.getPaymentReason2(),
					reasonedSlip.getPaymentReason3(), reasonedSlip.getPaymentReason4() });
				
				mActivity.mFieldBeneficiary.setNtnl(reasonedSlip.getNtnl());
				mActivity.mFieldBeneficiary.setAccount(reasonedSlip.getBeneficiaryAccountNo());
				mActivity.mFieldBeneficiary.setAccountIBAN(reasonedSlip.getBeneficiaryIban());
				
				mActivity.mFieldChargeOptions.setChargeOption(reasonedSlip.getChargeOption());
			}
		}
		
		if (viewResult != null && !mActivity.getIsConfirmMode()) {
			mActivity.mFieldAccount.setSelectedMoneyAccountTO(viewResult.getDebitMoneyAccount());
			PaymentOrder porder = getPaymentOrderFromRequest();
			if (porder != null)
				mActivity.mFieldExecutionDate.setSelectedDate(getPaymentOrderFromRequest().getTransactionDate());
		}
		
		if (viewStandingResult != null && !mActivity.getIsConfirmMode()) {
			mActivity.mFieldAccount.setSelectedMoneyAccountTO(viewStandingResult.getDebitMoneyAccount());
			
			StandingOrder order = getStandingOrderFromRequest();
			if (order != null){
				mActivity.mFieldStandingOrderFirstDate.setSelectedDate(order.getPeriodStart());
				mActivity.mFieldPeriods.setSelectedValue(order.getStandingOrderPeriodId());
				
				if (order.isBeforeHoliday())
					mActivity.mFieldExecutionType.setSelectedValue(ExecutionType.BEFORE);
				else
					mActivity.mFieldExecutionType.setSelectedValue(ExecutionType.AFTER);
				
				if (order.getEndDate() != null){
					mActivity.mFieldEndDate.setSelectedDate(order.getEndDate());
					mActivity.mFiledValidity.setSelectedValue(ExecuteUntilType.END_DATE_REACHED);
				}
				
				if (order.getPeriodCount() != null){
					mActivity.mFieldExecutionCount.setValue(order.getPeriodCount().toString());
					mActivity.mFiledValidity.setSelectedValue(ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED);
				}
				
				if (order.getNextExecutionDate() != null)
					mActivity.mFieldStandingOrderNextDate.setSelectedDate(order.getNextExecutionDate());
				if (order.getLastExecutionDate() != null)
					mActivity.mFieldStandingOrderLastDate.setSelectedDate(order.getLastExecutionDate());
				mActivity.mFieldStandingOrderActive.setChecked(!order.isDeactivated());
				
				if (getViewType() != PaymentViewType.TEMPLATE_FROM_VIEW && getViewType() != PaymentViewType.NEW_FROM_VIEW){
					mActivity.mFieldStandingOrderNextDate.setVisibility(View.VISIBLE);
					mActivity.mFieldStandingOrderLastDate.setVisibility(View.VISIBLE);
					mActivity.mFieldStandingOrderActive.setVisibility(View.VISIBLE);
				}
			}
		}
		
		mActivity.populateFields();
	}
	
	public void requestViewData(long paymentId){
		if (mActivity.isViewFromStanding()){
			StandingRequest request = getStandingRequest(paymentId, new RequestStateEvent<StandingRequest>() {
				@Override
				public void onRequestCompleted(StandingRequest aRequest) {
					viewStandingResult = aRequest.getResponse().getData();
					if (mPaymentViewType == PaymentViewType.NEW_FROM_TEMPLATE ||
							mPaymentViewType == PaymentViewType.NEW_FROM_VIEW ||
							mPaymentViewType == PaymentViewType.NEW)
					{
						mPaymentState = PaymentStateType.INIT;
					}else{
						mPaymentState = getStandingOrderFromRequest().getPaymentState();
					}
					
					mActivity.setContentReady();
				}

				@Override
				public void onRequestFailed(StandingRequest aRequest) {
					mActivity.showError();
				}
			});
			
			request.setCachePolicy(CachePolicy.NO_CACHE);
			request.initiateServerRequest();
		}
		else {
			PaymentRequest request = getPaymentRequest(paymentId, new RequestStateEvent<PaymentRequest>() {
				@Override
				public void onRequestCompleted(PaymentRequest aRequest) {
					viewResult = aRequest.getResponse().getData();
					if (mPaymentViewType == PaymentViewType.NEW_FROM_TEMPLATE ||
							mPaymentViewType == PaymentViewType.NEW_FROM_VIEW ||
							mPaymentViewType == PaymentViewType.NEW ||
							mPaymentViewType == PaymentViewType.TEMPLATE_NEW)
					{
						mPaymentState = PaymentStateType.INIT;
					}else{
						mPaymentState = getPaymentOrderFromRequest().getPaymentState();
					}
					
					mActivity.setContentReady();
				}
	
				@Override
				public void onRequestFailed(PaymentRequest aRequest) {
					mActivity.showError();
				}
			});
			
			request.setCachePolicy(CachePolicy.NO_CACHE);
			request.initiateServerRequest();
		}
	}
	
	public void requestTemplateData(long templateId) {
		TemplateRequest request = getTemplateRequest(templateId, new RequestStateEvent<TemplateRequest>() {
			@Override
			public void onRequestCompleted(TemplateRequest aRequest) {
				templateResult = aRequest.getResponse().getData();
				mPaymentState = getPaymentTemplate(templateResult).getPaymentState();
				mActivity.setContentReady();
			}

			@Override
			public void onRequestFailed(TemplateRequest aRequest) {
				mActivity.showError();
			}
		});
		request.setCachePolicy(CachePolicy.NO_CACHE);
		request.initiateServerRequest();
	}
	
	public void loadData(final long id){
		getDefaultsRequest(new RequestStateEvent<DefaultsRequest>() {
			@Override
			public void onRequestCompleted(DefaultsRequest aRequest) {
				
				defaultsResult = aRequest.getResponse().getData();

				// if data from template has to be loaded
				if (mPaymentViewType == PaymentViewType.NEW_FROM_TEMPLATE || mPaymentViewType == PaymentViewType.TEMPLATE) {
					requestTemplateData(id);
				} else if (mPaymentViewType == PaymentViewType.NEW_FROM_VIEW || mPaymentViewType == PaymentViewType.TEMPLATE_FROM_VIEW || mPaymentViewType == PaymentViewType.VIEW) {
					// data for a view has to be loaded
					requestViewData(id);
				} else {
					mActivity.setContentReady();
				}
			}

			@Override
			public void onRequestFailed(DefaultsRequest aRequest) {
				mActivity.showError();
			}
		}).initiateServerRequest();
	}
	
	/**
	 * IMPORTANT!!!!!!!!!!!!! No payment set yet
	 * @return
	 */
	public PaymentOrder getPaymentOrder(){
		PaymentOrder order;
		
		if (viewResult != null && getPaymentOrderFromRequest() != null){
			order = getPaymentOrderFromRequest();
		}
		else {
			order = createEmptyPaymentOrder();
		}
		
		// Execution date
		if (mActivity.isViewFromStanding())
			order.setTransactionDate(null);
		else
			order.setTransactionDate(mActivity.mFieldExecutionDate.getSelectedDate());
		
		return order;
		
	}
	
	/**
	 * IMPORTANT!!!!!!!!!!!!! No payment set yet
	 * @return
	 */
	public StandingOrder getStandingOrder(){
		StandingOrder order;
		
		if (viewStandingResult != null && getStandingOrderFromRequest() != null)
			order = getStandingOrderFromRequest();
		else
			order = createEmptyStandingOrder();
		
		order.setPeriodStart(mActivity.mFieldStandingOrderFirstDate.getSelectedDate());
		order.setStandingOrderPeriodId(mActivity.mFieldPeriods.getSelectedValue().getId());
		if (mActivity.mFiledValidity.getSelectedValue() == ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED){
			if (mActivity.mFieldExecutionCount.getValue() != null){
				//order.setNumberOfExecutions(Integer.valueOf(mActivity.mFieldExecutionCount.getValue()));
				Long periodCount;
				try{
					periodCount = Long.valueOf(mActivity.mFieldExecutionCount.getValue());
				}
				catch (Exception e){
					periodCount = 0l;
				}
				order.setPeriodCount(periodCount);
			}
		}
		if (mActivity.mFieldExecutionType.getSelectedValue() == ExecutionType.BEFORE)
			order.setBeforeHoliday(true);
		else
			order.setBeforeHoliday(false);
			
		if (mActivity.mFiledValidity.getSelectedValue() == ExecuteUntilType.END_DATE_REACHED)
			order.setEndDate(mActivity.mFieldEndDate.getSelectedDate());
		
		order.setDeactivated(!mActivity.mFieldStandingOrderActive.isChecked());
		
		return order;
	}
	
	public DefaultsResult getDefaults(){
		return defaultsResult;
	}
	
	public TemplateResult getTemplateResult(){
		return templateResult;
	}
	
	public PaymentResult getPaymentResult(){
		return viewResult;
	}
	
	public StandingResult getStandingResult(){
		return viewStandingResult;
	}
	
	public MoneyAccountTO getDefaultDebitAccount(){
		return defaultDebitAccount;
	}
	
	public PaymentSlip getPaymentFromRequest(){
		PaymentSlip slip = null;
		
		PaymentTO pay;
		
		if (mMask != null && mPaymentViewType == PaymentViewType.NEW_FROM_MASK){
			return (PaymentSlip)PaymentUtil.getPaymentSlipFromMask(mMask);
		}
		
		PaymentOrder paymentOrder = getPaymentOrderFromRequest();
		StandingOrder standingOrder = getStandingOrderFromRequest();
		if (paymentOrder != null)
			slip = getSlipFromPaymentOrder(paymentOrder);
		if (slip == null && standingOrder != null){
			slip = getSlipFromStandingOrder(standingOrder);
		}
		
		return slip;
		/*try {
			return getSlipFromPaymentOrder(getPaymentOrderFromRequest());
		}
		catch (NullPointerException e){
			try {
				return getSlipFromStandingOrder(getStandingOrderFromRequest());
			}
			catch (NullPointerException e1){
				return null;
			}
		}*/
	}
	
	public PaymentOrder getPaymentOrderFromRequest(){
		if (viewResult != null)
			return getPaymentOrder(viewResult);
		else
			return null;
	}
	
	public StandingOrder getStandingOrderFromRequest(){
		if (viewStandingResult != null)
			return getStandingOrder(viewStandingResult);
		else
			return null;
	}
	
	public BasePaymentOrderTO getOrderFromRequest(){
		if (mActivity.isViewFromStanding()){
			return getStandingOrderFromRequest();
		}
		else
			return getPaymentOrderFromRequest();
	}
	
	public Template getPaymentTemplateFromRequest(){
		return getPaymentTemplate(templateResult);
	}
	
	public PaymentDefaults getPaymentDefaultsFromRequest(){
		return getPaymentDefaults(defaultsResult);
	}
	
	public PaymentSlip getSlipFromTemplateRequest(){
		return getSlipFromTemplate(getPaymentTemplateFromRequest());
	}
	
	public boolean hasSaveAsTemplateField(){
		switch (mPaymentViewType){
			case NEW:
			case NEW_FROM_MASK:
			case NEW_FROM_TEMPLATE:
			case NEW_FROM_VIEW:
				return true;
			default:
				return false;
		}
	}
	
	protected abstract PaymentRequest getPaymentRequest(long paymentId, RequestStateEvent<PaymentRequest> rse);
	protected abstract StandingRequest getStandingRequest(long paymentId, RequestStateEvent<StandingRequest> rse);
	protected abstract TemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<TemplateRequest> rse);
	protected abstract DefaultsRequest getDefaultsRequest(RequestStateEvent<DefaultsRequest> rse);
	
	protected abstract PaymentOrder getPaymentOrder(PaymentResult result);
	protected abstract StandingOrder getStandingOrder(StandingResult result);
	protected abstract Template getPaymentTemplate(TemplateResult result);
	protected abstract PaymentDefaults getPaymentDefaults(DefaultsResult template);
	
	protected abstract PaymentSlip getSlipFromPaymentOrder(PaymentOrder result);
	protected abstract PaymentSlip getSlipFromStandingOrder(StandingOrder result);
	protected abstract PaymentSlip getSlipFromTemplate(Template template);
	
	protected abstract PaymentOrder createEmptyPaymentOrder();
	protected abstract StandingOrder createEmptyStandingOrder();
	public abstract PaymentSlip createEmptyPaymentSlip();
}
