package com.avaloq.banklet.payments.methods;
import com.avaloq.afs.aggregation.to.payment.BasePaymentResult;
import com.avaloq.afs.aggregation.to.payment.PaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.StandingOrderTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.comms.AbstractServerRequest;

public abstract class ConfirmMethod
	<
		PaymentOrder extends PaymentOrderTO, 
		PaymentRequest extends AbstractServerRequest<? extends BasePaymentResult>, 
		StandingOrder extends StandingOrderTO, 
		StandingRequest extends AbstractServerRequest<? extends BasePaymentResult>
	>
	extends VerifyMethod<PaymentOrder, PaymentRequest, StandingOrder, StandingRequest> {
	
	public ConfirmMethod(AbstractPaymentActivity<?, PaymentRequest, PaymentOrder, ?, ?, StandingRequest, StandingOrder, ?, ?, ?, ?, ?> activity){
		super(activity);
	}
	
	@Override
	protected void submissionResultStanding(StandingRequest request){
		getSubmissionMethod().submissionResultStanding(request, false);
	}
	
	@Override
	protected void submissionResultPayment(PaymentRequest request){
		getSubmissionMethod().submissionPaymentResult(request, false);
	}
	
	@Override
	protected PaymentOrder getPaymentOrderForMethod() {
		return mActivity.getPaymentOrder();
	}

	@Override
	protected StandingOrder getStandingOrderForMethod() {
		return mActivity.getStandingOrder();
	}
	
	protected  SubmissionResultMethod<?, PaymentRequest, ?, StandingRequest> getSubmissionMethod(){
		return mActivity.getSubmissionResultMethod();
	}
}
