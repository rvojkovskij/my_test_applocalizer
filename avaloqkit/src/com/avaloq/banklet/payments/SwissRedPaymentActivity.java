package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import com.avaloq.framework.R;

public class SwissRedPaymentActivity extends SwissRedPaymentAbstractActivity {

	@Override
	List<ButtonDef> getButtonDefs() {
		
		List<ButtonDef> buttonList = new ArrayList<ButtonDef>();
		
		if (isEditable()){
		
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return R.string.pmt_template_verify_payment;
				}
				@Override
				public void onClick() {
					verifyPayment();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					return VERIFY_BUTTON_TAG;
				}
			});
		}else{
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return R.string.pmt_template_confirm_payment;
				}
				@Override
				public void onClick() {
					confirmPayment();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}

		return buttonList;
	}
}

