package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.PaymentDefaultsResult;
import com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentResult;
import com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateResult;
import com.avaloq.afs.aggregation.to.payment.internal.InternalPaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.internal.InternalStandingOrderTO;
import com.avaloq.afs.aggregation.to.payment.internal.InternalStandingPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.afs.server.bsp.client.ws.InternalPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.VerifyWithSubmissionMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.internalpayment.InternalPaymentRequest;
import com.avaloq.framework.comms.webservice.internalpayment.InternalPaymentService;
import com.avaloq.framework.comms.webservice.internalpayment.InternalPaymentTemplateRequest;
import com.avaloq.framework.comms.webservice.internalpayment.InternalStandingPaymentRequest;
import com.avaloq.framework.comms.webservice.internalpayment.PaymentDefaultsRequest;

public abstract class AccountTransferAbstractActivity
	extends AbstractPaymentActivity<
	InternalPaymentResult, InternalPaymentRequest, InternalPaymentOrderTO, InternalPaymentTO,
	InternalStandingPaymentResult, InternalStandingPaymentRequest, InternalStandingOrderTO,
	InternalPaymentTemplateResult, InternalPaymentTemplateRequest, InternalPaymentTemplateTO,
	PaymentDefaultsResult, PaymentDefaultsRequest
	> {

	/**
	 * Override in child classes if something special is needed for the header
	 */
	@Override
	HeaderData getHeaderData() {
		return new HeaderData() {

			@Override
			public int getIconResId() {
				return R.drawable.pmt_new_transfer;
			}

			@Override
			public String getTitle() {				
				return getResources().getString(R.string.pmt_payment_type_internal_payment);
			}
			
			@Override
			public String getSubtitle() {
				switch (getViewDataMethod().getViewType()){
					case NEW:					
					case NEW_FROM_TEMPLATE:					
					case NEW_FROM_VIEW:
					case VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
					case TEMPLATE:					
					case TEMPLATE_FROM_VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_template);
					default:					
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
				}	
			}
		};
	}

	/**
	 * Depending on what kind of data is available (existing order or a template), it will
	 * set the available values for the fields
	 */
	@Override
	public void populateFields() {
	
		// Set the list of accounts
		mFieldCreditAccount.setMoneyAccounts(getViewDataMethod().getDefaults().getPaymentDefaults().getCreditMoneyAccounts());		

		// if the account was found, assign it to the field
		if (mFieldCreditAccount.getSelectedMoneyAccountTO() == null && getViewDataMethod().getDefaults().getPaymentDefaults().getDebitMoneyAccounts().size() > 0) {
			mFieldCreditAccount.setDefaultAccountId(getViewDataMethod().getDefaults().getPaymentDefaults().getDebitMoneyAccounts().get(0).getId());
			if (getViewDataMethod().getDefaultDebitAccount() != null && getViewDataMethod().getDefaultDebitAccount().equals(getViewDataMethod().getDefaults().getPaymentDefaults().getCreditMoneyAccounts().get(0)) && getViewDataMethod().getDefaults().getPaymentDefaults().getDebitMoneyAccounts().size() > 1)
				mFieldCreditAccount.setSelectedMoneyAccountTO(getViewDataMethod().getDefaults().getPaymentDefaults().getCreditMoneyAccounts().get(1));
			else
				mFieldCreditAccount.setSelectedMoneyAccountTO(getViewDataMethod().getDefaults().getPaymentDefaults().getCreditMoneyAccounts().get(0));
		}
		
		mFieldCreditAccount.setOnSelectedCallback(new Runnable(){
			@Override
			public void run() {
				if (mFieldCreditAccount.getSelectedMoneyAccountTO() != null)
					mFieldAccount.setMoneyAccounts(removeElementFromList(getViewDataMethod().getDefaults().getPaymentDefaults().getDebitMoneyAccounts(), mFieldCreditAccount.getSelectedMoneyAccountTO()));
				adjustCurrencies();
			}
		});
		
		mFieldAccount.setOnSelectedCallback(new Runnable(){
			@Override
			public void run() {
				if (mFieldAccount.getSelectedMoneyAccountTO() != null)
					mFieldCreditAccount.setMoneyAccounts(removeElementFromList(getViewDataMethod().getDefaults().getPaymentDefaults().getCreditMoneyAccounts(), mFieldAccount.getSelectedMoneyAccountTO()));
				adjustCurrencies();
			}
		});
		
		
		if (getViewDataMethod().getTemplateResult() != null && !isConfirmMode) {		
			mFieldCreditAccount.setSelectedMoneyAccountTO(getViewDataMethod().getTemplateResult().getCreditMoneyAccount());
		}

		if (getViewDataMethod().getPaymentResult() != null && !isConfirmMode) {
			mFieldCreditAccount.setSelectedMoneyAccountTO(getViewDataMethod().getPaymentResult().getCreditMoneyAccount());
		}
		
		if (getViewDataMethod().getStandingResult() != null && !isConfirmMode) {
			mFieldCreditAccount.setSelectedMoneyAccountTO(getViewDataMethod().getStandingResult().getCreditMoneyAccount());
		}
		
		adjustCurrencies();
	}

	abstract List<ButtonDef> getButtonDefs();

	@Override
	ContentData getContentData() {
		return new ContentData() {

			public boolean hasDebitMoneyAccount() {
				return true;
			}

			@Override
			public boolean hasBeneficiaryBankDetails() {
				return false;
			}

			@Override
			public boolean hasBeneficiary() {
				return false;
			}

			public boolean hasAccountNumber() {
				return false;
			}

			@Override
			public boolean hasReferenceNumber() {
				return false;
			}

			@Override
			public boolean hasAmountField() {
				return true;
			}

			@Override
			public boolean hasSalaryPayment() {
				return false;
			}

			@Override
			public boolean hasExecutionDateField() {
				return true;
			}

			@Override
			public boolean hasChargeOptionType() {
				return false;
			}

			@Override
			public boolean hasStandingOrderField() {
				return false;
			}

			@Override
			public boolean hasSaveAsTemplateField() {
				return getViewDataMethod().getViewType() == PaymentViewType.NEW || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_TEMPLATE || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_VIEW;
			}

			@Override
			public boolean hasDebitAdviceField() {
				return false;
			}

			@Override
			public boolean hasPaymentReasonField() {
				return true;
			}

			@Override
			public boolean hasCreditMoneyAccount() {				
				return true;
			}

			@Override
			public boolean hasScanField() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}
	
	/**
	 * Returns the payment object by reading the values in the fields
	 * @return
	 */
	@Override
	public InternalPaymentTO fillPayment(InternalPaymentTO paymentSlip){		
		paymentSlip.setCreditMoneyAccountId(mFieldCreditAccount.getSelectedMoneyAccountTO().getId());
		paymentSlip.setPaymentType(PaymentType.INTERNAL_PAYMENT);
				
		// Payment Reason because AccountTransfer is not inheriting from ReasonedPaymentTO
		paymentSlip.setPaymentReason1(mFieldPaymentDetails.getPaymentReasons()[0]);
		paymentSlip.setPaymentReason2(mFieldPaymentDetails.getPaymentReasons()[1]);
		paymentSlip.setPaymentReason3(mFieldPaymentDetails.getPaymentReasons()[2]);
		paymentSlip.setPaymentReason4(mFieldPaymentDetails.getPaymentReasons()[3]);

		return paymentSlip;
	}
	
	/**
	 * Returns a new list containing all the elements from the first but the one given in "remove"
	 * @return
	 */
	public List<MoneyAccountTO> removeElementFromList(List<MoneyAccountTO> list, MoneyAccountTO account){
		List<MoneyAccountTO> output = new ArrayList<MoneyAccountTO>();
		for (MoneyAccountTO acc: list){
			if (!acc.getId().equals(account.getId()))
				output.add(acc);
		}
		return output;
	}
	
	/**
	 * Makes sure that the list of currencies contains only the debit and credit account currencies
	 */
	public void adjustCurrencies(){
		Long current = mFieldAmount.getCurrencyId();
		Long debit = null, credit = null;
		if (mFieldAccount.getSelectedMoneyAccountTO() != null){
			debit = mFieldAccount.getSelectedMoneyAccountTO().getCurrencyId();
		}
		if (mFieldCreditAccount.getSelectedMoneyAccountTO() != null){
			credit = mFieldCreditAccount.getSelectedMoneyAccountTO().getCurrencyId();
		}
		
		if (credit == null || debit == null){
			mFieldAmount.setCurrencies(getViewDataMethod().getDefaults().getPaymentDefaults().getPaymentCurrencies());
			return;
		}
		
		List<CurrencyTO> output = new ArrayList<CurrencyTO>();
		for (CurrencyTO cur: getViewDataMethod().getDefaults().getPaymentDefaults().getPaymentCurrencies()){
			if (cur.getId().equals(debit) || cur.getId().equals(credit))
				output.add(cur);
		}
		if (current != debit && current != credit)
			mFieldAmount.setCurrencyId(debit);
		mFieldAmount.setCurrencies(output);
	}
	
	@Override
	public VerifyMethod<InternalPaymentOrderTO, InternalPaymentRequest, InternalStandingOrderTO, InternalStandingPaymentRequest> getVerifyMethod() {
		return new VerifyWithSubmissionMethod<InternalPaymentOrderTO, InternalPaymentRequest, InternalStandingOrderTO, InternalStandingPaymentRequest>(this) {

			@Override
			protected InternalPaymentRequest getPaymentRequest(InternalPaymentOrderTO order, RequestStateEvent<InternalPaymentRequest> rse) {
				return InternalPaymentService.verifyPayment(order, rse);
			}

			@Override
			protected InternalStandingPaymentRequest getStandingRequest(InternalStandingOrderTO order, RequestStateEvent<InternalStandingPaymentRequest> rse) {
				return InternalPaymentService.verifyStandingPayment(order, rse);
			}
		};
	}

	@Override
	public ConfirmMethod<InternalPaymentOrderTO, InternalPaymentRequest, InternalStandingOrderTO, InternalStandingPaymentRequest> getConfirmMethod() {
		return new ConfirmMethod<InternalPaymentOrderTO, InternalPaymentRequest, InternalStandingOrderTO, InternalStandingPaymentRequest>(this) {

			@Override
			protected InternalPaymentRequest getPaymentRequest(InternalPaymentOrderTO order, RequestStateEvent<InternalPaymentRequest> rse) {
				return InternalPaymentService.submitPayment(order, rse);
			}

			@Override
			protected InternalStandingPaymentRequest getStandingRequest(InternalStandingOrderTO order, RequestStateEvent<InternalStandingPaymentRequest> rse) {
				return InternalPaymentService.submitStandingPayment(order, rse);
			}
		};
	}

	@Override
	public SubmissionResultMethod<InternalPaymentResult, InternalPaymentRequest, InternalStandingPaymentResult, InternalStandingPaymentRequest> getSubmissionResultMethod() {
		return new SubmissionResultMethod<InternalPaymentResult, InternalPaymentRequest, InternalStandingPaymentResult, InternalStandingPaymentRequest>(this) {

			@Override
			public SaveTemplateType getSaveTemplateType() {
				return SaveTemplateType.TRANSFER;
			}
		};
	}

	@Override
	public ViewDataMethod<InternalPaymentResult, InternalPaymentRequest, InternalPaymentOrderTO, InternalPaymentTO, InternalStandingPaymentResult, InternalStandingPaymentRequest, InternalStandingOrderTO, InternalPaymentTemplateResult, InternalPaymentTemplateRequest, InternalPaymentTemplateTO, PaymentDefaultsResult, PaymentDefaultsRequest> createViewDataMethod() {
		return new ViewDataMethod<InternalPaymentResult, InternalPaymentRequest, InternalPaymentOrderTO, InternalPaymentTO, InternalStandingPaymentResult, InternalStandingPaymentRequest, InternalStandingOrderTO, InternalPaymentTemplateResult, InternalPaymentTemplateRequest, InternalPaymentTemplateTO, PaymentDefaultsResult, PaymentDefaultsRequest>(this) {

			@Override
			protected InternalPaymentRequest getPaymentRequest(long paymentId,RequestStateEvent<InternalPaymentRequest> rse) {
				return InternalPaymentService.getPayment(paymentId, rse);
			}

			@Override
			protected InternalStandingPaymentRequest getStandingRequest(long paymentId, RequestStateEvent<InternalStandingPaymentRequest> rse) {
				return InternalPaymentService.getStandingPayment(paymentId, rse);
			}

			@Override
			protected InternalPaymentTemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<InternalPaymentTemplateRequest> rse) {
				return InternalPaymentService.getPaymentTemplate(paymentId, rse);
			}

			@Override
			protected PaymentDefaultsRequest getDefaultsRequest(RequestStateEvent<PaymentDefaultsRequest> rse) {
				return InternalPaymentService.getDefaults(rse);
			}

			@Override
			protected InternalPaymentOrderTO getPaymentOrder(InternalPaymentResult result) {
				return result.getInternalPaymentOrder();
			}

			@Override
			protected InternalStandingOrderTO getStandingOrder(InternalStandingPaymentResult result) {
				return result.getInternalStandingOrder();
			}

			@Override
			protected InternalPaymentTemplateTO getPaymentTemplate(InternalPaymentTemplateResult result) {
				return result.getInternalPaymentTemplate();
			}

			@Override
			protected InternalPaymentTO getSlipFromPaymentOrder(InternalPaymentOrderTO result) {
				return result.getInternalPayment();
			}

			@Override
			protected InternalPaymentTO getSlipFromStandingOrder(InternalStandingOrderTO result) {
				return result.getInternalPayment();
			}

			@Override
			protected InternalPaymentOrderTO createEmptyPaymentOrder() {
				return new InternalPaymentOrderTO();
			}

			@Override
			protected InternalStandingOrderTO createEmptyStandingOrder() {
				return new InternalStandingOrderTO();
			}
			
			@Override
			protected PaymentDefaults getPaymentDefaults(PaymentDefaultsResult template) {
				return template.getPaymentDefaults();
			}

			@Override
			protected InternalPaymentTO getSlipFromTemplate(InternalPaymentTemplateTO template) {
				return template.getInternalPayment();
			}
			
			@Override
			public InternalPaymentTO createEmptyPaymentSlip() {
				return new InternalPaymentTO();
			}
		};
	}
	
	@Override
	public TemplateMethod<InternalPaymentTO, InternalPaymentTemplateResult, InternalPaymentTemplateRequest, InternalPaymentTemplateTO> getTemplateMethod() {
		return new TemplateMethod<InternalPaymentTO, InternalPaymentTemplateResult, InternalPaymentTemplateRequest, InternalPaymentTemplateTO>(this){

			@Override
			protected void setPayment(InternalPaymentTemplateTO template, InternalPaymentTO payment) {
				template.setPayment(payment);
			}

			@Override
			protected InternalPaymentTemplateRequest getVerifyRequest(InternalPaymentTemplateTO template, RequestStateEvent<InternalPaymentTemplateRequest> rse) {
				return InternalPaymentService.verifyPaymentTemplate(template, rse);
			}

			@Override
			protected InternalPaymentTemplateRequest getSubmitRequest(InternalPaymentTemplateTO template, RequestStateEvent<InternalPaymentTemplateRequest> rse) {
				return InternalPaymentService.savePaymentTemplate(template, rse);
			}

			@Override
			protected InternalPaymentTemplateTO createEmptyTemplate() {
				return new InternalPaymentTemplateTO();
			}
			
		};
	}

	@Override
	public InternalPaymentOrderTO getPaymentOrder() {
		InternalPaymentOrderTO order = getViewDataMethod().getPaymentOrder();
		order.setPayment(getPayment());
		return order;
	}

	@Override
	public InternalStandingOrderTO getStandingOrder() {
		InternalStandingOrderTO order = getViewDataMethod().getStandingOrder();
		order.setPayment(getPayment());
		return order;
	}
	
	@Override
	public PaymentType getPaymentType(){
		return PaymentType.INTERNAL_PAYMENT;
	}
}
