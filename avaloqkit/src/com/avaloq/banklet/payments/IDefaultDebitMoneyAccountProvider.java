package com.avaloq.banklet.payments;

import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;

public interface IDefaultDebitMoneyAccountProvider {
	
	MoneyAccountTO getDefaultDebitMoneyAccount();
	
}
