package com.avaloq.banklet.payments;

/**
 * Constants for the Payment Banklet.
 * Migrated from PMTPaymentsBanklet.h
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public interface PaymentConstants {
//
//	/**
//	 * Payment types
//	 */
//	enum PaymentType {
//		INTERNAL_PAYMENT,
//		INTERNATIONAL_PAYMENT,
//		SWISS_ORANGE_PAYMENT_SLIP,
//		SWISS_RED_PAYMENT_SLIP,
//		DOMESTIC_PAYMENT
//	}
//
//	/** 
//	 * Payment subtypes
//	 */
//	enum PaymentSubtype {
//		DOMESTIC_PAYMENT,
//		BANK_ACCOUNT,
//		BANK_UNDEFINED
//	}
//
//	/** 
//	 * Payment States
//	 */
//	enum PaymentState {
//		INIT,
//		EDIT,
//		PARTIAL_APPROVED,
//		APPROVED,
//		PROCESSING,
//		PROCESSED_ARCHIVED,
//		DELETED_ARCHIVED,
//		REJECTED_ARCHIVED
//	}
//
//	/**
//	 * Direct Debit Mandate States
//	 */
//	enum DirectDebitState {
//		OPEN,
//		CLOSE_REQUEST,
//		CLOSED
//	}
//
//	/**
//	 * Payment Search State Type
//	 */
//	enum PaymentSearchStateType {
//		OPEN,
//		PROCESSED,
//		REJECTED,
//		DELETED
//	}
//
//	/** 
//	 * Payment Search Date Type
//	 */
//	enum PaymentSearchDateType {
//		CREATED,
//		APPROVED,
//		VALUTA,
//		TRANSACTION
//	}
//
//	/**
//	 * Payment Action Types
//	 */
//	enum PaymentActionType {
//		READ_ONLY,
//		EDIT_DELETE_APPROVE,
//		EDIT_DELETE,
//		DELETE_APPROVE,
//		DELETE
//	}
//
//	/**
//	 * Payment Advice Types
//	 */
//	enum PaymentAdviceType {
//		NO_ADVICE,
//		DETAIL_ADVICE_SINGLE,
//		DETAIL_ADVICE_COLLECTIVE,
//		COLLECTIVE_ADVICE
//	}
//
//	/**
//	 * Standing order validity
//	 */
//	enum PaymentStandingOrderValidity {
//		
//		UNTIL_FURTHER_NOTICE(0),
//		UNTIL_NUM_EXECS_REACHED(1),
//		UNTIL_END_DATE(2);
//		
//		private final int value;
//		
//		private PaymentStandingOrderValidity(final int v) {
//			value = v;
//		}
//		
//		public int getValue() {
//			return value;
//		}
//		
//	}
//
}