package com.avaloq.banklet.payments;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.SwissPaymentDefaultsResult;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentResult;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentTemplateResult;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedPaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedStandingOrderTO;
import com.avaloq.afs.aggregation.to.payment.red.SwissRedStandingPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.PaymentSubType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.SwissRedPaymentSlipTO;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.VerifyWithSubmissionMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.banklet.payments.views.BeneficiaryField.BeneficiaryFieldPaymentType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissPaymentDefaultsRequest;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissRedPaymentRequest;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissRedPaymentService;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissRedPaymentTemplateRequest;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissRedStandingPaymentRequest;

public abstract class SwissRedPaymentAbstractActivity 
	extends AbstractPaymentActivity<
		SwissRedPaymentResult, SwissRedPaymentRequest, SwissRedPaymentOrderTO, SwissRedPaymentSlipTO,
		SwissRedStandingPaymentResult, SwissRedStandingPaymentRequest, SwissRedStandingOrderTO,
		SwissRedPaymentTemplateResult, SwissRedPaymentTemplateRequest, SwissRedPaymentTemplateTO,
		SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest
	> {

	/**
	 * Override in child classes if something special is needed for the header
	 */
	@Override
	HeaderData getHeaderData() {
		return new HeaderData() {

			@Override
			public int getIconResId() {
				return R.drawable.pmt_new_slip_red;
			}

			@Override
			public String getTitle() {
				return getResources().getString(R.string.pmt_payment_type_swiss_red_payment_slip);
			}

			@Override
			public String getSubtitle() {
				switch (getViewDataMethod().getViewType()){
					case NEW:					
					case NEW_FROM_TEMPLATE:					
					case NEW_FROM_VIEW:
					case VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
					case TEMPLATE:					
					case TEMPLATE_FROM_VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_template);
					default:					
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
				}					
			}
		};
	}	
	
	@Override
	public void populateFields() {
		mFieldBeneficiary.setPaymentType(BeneficiaryFieldPaymentType.RED);
		mFieldAmount.setCurrencies(getViewDataMethod().getDefaults().getPaymentDefaults().getPaymentCurrencies());

		// if there is some data available, put it in the fields
		if (getViewDataMethod().getTemplateResult() != null && !isConfirmMode) {
			mFieldBeneficiary.setPostAccountNumber(getViewDataMethod().getTemplateResult().getSwissRedPaymentTemplate().getSwissRedPayment().getPcAccount());
			mFieldBeneficiary.showPostAccountNumber();	
			
			mFieldBeneficiary.setBankDetails(new String[]{
					getViewDataMethod().getSlipFromTemplateRequest().getBeneficiary1(),
					getViewDataMethod().getSlipFromTemplateRequest().getBeneficiary2(),
					getViewDataMethod().getSlipFromTemplateRequest().getBeneficiary3(),
					getViewDataMethod().getSlipFromTemplateRequest().getBeneficiary4()
				});
			//getViewDataMethod().getSlipFromTemplateRequest().getPaymentSubType().		
			//mFieldBeneficiary.showBankDetails();
			mFieldBeneficiary.hideBeneficiaryDetails();
		}
		
		SwissRedPaymentSlipTO paymentSlip = getViewDataMethod().getPaymentFromRequest();
		
		if (paymentSlip != null && !isConfirmMode) {
			mFieldBeneficiary.setPostAccountNumber(paymentSlip.getPcAccount());
			mFieldBeneficiary.showPostAccountNumber();
			
			mFieldBeneficiary.setBankDetails(new String[]{
					paymentSlip.getBeneficiary1(),
					paymentSlip.getBeneficiary2(),
					paymentSlip.getBeneficiary3(),
					paymentSlip.getBeneficiary4()
				});
						
			//mFieldBeneficiary.showBankDetails();
			mFieldBeneficiary.hideBeneficiaryDetails();
		}
	}

	abstract List<ButtonDef> getButtonDefs();

	@Override
	ContentData getContentData() {
		return new ContentData() {

			public boolean hasDebitMoneyAccount() {
				return true;
			}

			@Override
			public boolean hasBeneficiaryBankDetails() {
				return true;
			}

			@Override
			public boolean hasBeneficiary() {
				return true;
			}

			public boolean hasAccountNumber() {
				return true;
			}

			@Override
			public boolean hasReferenceNumber() {
				return false;
			}

			@Override
			public boolean hasAmountField() {
				return true;
			}

			@Override
			public boolean hasSalaryPayment() {
				return true;
			}

			@Override
			public boolean hasExecutionDateField() {
				return true;
			}

			@Override
			public boolean hasChargeOptionType() {
				return false;
			}

			@Override
			public boolean hasStandingOrderField() {
				return true;
			}

			@Override
			public boolean hasSaveAsTemplateField() {
				return getViewDataMethod().getViewType() == PaymentViewType.NEW || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_TEMPLATE || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_VIEW;
			}

			@Override
			public boolean hasDebitAdviceField() {
				return true;
			}

			@Override
			public boolean hasPaymentReasonField() {
				return true;
			}

			@Override
			public boolean hasCreditMoneyAccount() {
				return false;
			}

			@Override
			public boolean hasScanField() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}
	
	/**
	 * Returns the payment object by reading the values in the fields
	 * @return
	 */
	@Override
	public SwissRedPaymentSlipTO fillPayment(SwissRedPaymentSlipTO paymentSlip){
		
		if (mFieldBeneficiary.getPaymentSubType() != PaymentSubType.BANK_ACCOUNT){
			paymentSlip.setBeneficiary1(mFieldBeneficiary.getBankDetails()[0]);
			paymentSlip.setBeneficiary2(mFieldBeneficiary.getBankDetails()[1]);
			paymentSlip.setBeneficiary3(mFieldBeneficiary.getBankDetails()[2]);
			paymentSlip.setBeneficiary4(mFieldBeneficiary.getBankDetails()[3]);
			
			paymentSlip.setBeneficiaryBank1("");
			paymentSlip.setBeneficiaryBank2("");
			paymentSlip.setBeneficiaryBank3("");
			paymentSlip.setBeneficiaryBank4("");
		}else{
			paymentSlip.setBeneficiaryIban(mFieldBeneficiary.getAccountIBAN());
		}
		
		paymentSlip.setPcAccount(mFieldBeneficiary.getPostAccountNumber());
		paymentSlip.setPaymentType(PaymentType.SWISS_RED_PAYMENT_SLIP);
		paymentSlip.setPaymentSubType(mFieldBeneficiary.getPaymentSubType());
		
		
		return paymentSlip;
	}
	
	@Override
	public VerifyMethod<SwissRedPaymentOrderTO, SwissRedPaymentRequest, SwissRedStandingOrderTO, SwissRedStandingPaymentRequest> getVerifyMethod() {
		return new VerifyWithSubmissionMethod<SwissRedPaymentOrderTO, SwissRedPaymentRequest, SwissRedStandingOrderTO, SwissRedStandingPaymentRequest>(this) {
			@Override
			protected SwissRedPaymentRequest getPaymentRequest(SwissRedPaymentOrderTO order, RequestStateEvent<SwissRedPaymentRequest> rse) {
				return SwissRedPaymentService.verifyPayment(order, rse);
			}

			@Override
			protected SwissRedStandingPaymentRequest getStandingRequest(SwissRedStandingOrderTO order, RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
				return SwissRedPaymentService.verifyStandingPayment(order, rse);
			}
		};
	}

	@Override
	public ConfirmMethod<SwissRedPaymentOrderTO, SwissRedPaymentRequest, SwissRedStandingOrderTO, SwissRedStandingPaymentRequest> getConfirmMethod() {
		return new ConfirmMethod<SwissRedPaymentOrderTO, SwissRedPaymentRequest, SwissRedStandingOrderTO, SwissRedStandingPaymentRequest>(this) {
			@Override
			protected SwissRedPaymentRequest getPaymentRequest(SwissRedPaymentOrderTO order, RequestStateEvent<SwissRedPaymentRequest> rse) {
				return SwissRedPaymentService.submitPayment(order, rse);
			}

			@Override
			protected SwissRedStandingPaymentRequest getStandingRequest(SwissRedStandingOrderTO order, RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
				return SwissRedPaymentService.submitStandingPayment(order, rse);
			}
		};
	}

	@Override
	public SubmissionResultMethod<SwissRedPaymentResult, SwissRedPaymentRequest, SwissRedStandingPaymentResult, SwissRedStandingPaymentRequest> getSubmissionResultMethod() {
		return new SubmissionResultMethod<SwissRedPaymentResult, SwissRedPaymentRequest, SwissRedStandingPaymentResult, SwissRedStandingPaymentRequest>(this) {
			@Override
			public SaveTemplateType getSaveTemplateType() {
				return SaveTemplateType.RED;
			}
		};
	}

	@Override
	public ViewDataMethod<SwissRedPaymentResult, SwissRedPaymentRequest, SwissRedPaymentOrderTO, SwissRedPaymentSlipTO, SwissRedStandingPaymentResult, SwissRedStandingPaymentRequest, SwissRedStandingOrderTO, SwissRedPaymentTemplateResult, SwissRedPaymentTemplateRequest, SwissRedPaymentTemplateTO, SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest> createViewDataMethod() {
		return new ViewDataMethod<SwissRedPaymentResult, SwissRedPaymentRequest, SwissRedPaymentOrderTO, SwissRedPaymentSlipTO, SwissRedStandingPaymentResult, SwissRedStandingPaymentRequest, SwissRedStandingOrderTO, SwissRedPaymentTemplateResult, SwissRedPaymentTemplateRequest, SwissRedPaymentTemplateTO, SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest>(this) {

			@Override
			protected SwissRedPaymentRequest getPaymentRequest(long paymentId, RequestStateEvent<SwissRedPaymentRequest> rse) {
				return SwissRedPaymentService.getPayment(paymentId, rse);
			}

			@Override
			protected SwissRedStandingPaymentRequest getStandingRequest( long paymentId, RequestStateEvent<SwissRedStandingPaymentRequest> rse) {
				return SwissRedPaymentService.getStandingPayment(paymentId, rse);
			}

			@Override
			protected SwissRedPaymentTemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
				return SwissRedPaymentService.getPaymentTemplate(paymentId, rse);
			}

			@Override
			protected SwissPaymentDefaultsRequest getDefaultsRequest(RequestStateEvent<SwissPaymentDefaultsRequest> rse) {
				return SwissRedPaymentService.getDefaults(rse);
			}

			@Override
			protected SwissRedPaymentOrderTO getPaymentOrder(SwissRedPaymentResult result) {
				return result.getSwissRedPaymentOrder();
			}

			@Override
			protected SwissRedStandingOrderTO getStandingOrder(SwissRedStandingPaymentResult result) {
				return result.getSwissRedStandingOrder();
			}

			@Override
			protected SwissRedPaymentTemplateTO getPaymentTemplate(SwissRedPaymentTemplateResult result) {
				return result.getSwissRedPaymentTemplate();
			}

			@Override
			protected SwissRedPaymentSlipTO getSlipFromPaymentOrder(SwissRedPaymentOrderTO result) {
				return result.getSwissRedPayment();
			}

			@Override
			protected SwissRedPaymentSlipTO getSlipFromStandingOrder(SwissRedStandingOrderTO result) {
				return result.getSwissRedPayment();
			}

			@Override
			protected SwissRedPaymentOrderTO createEmptyPaymentOrder() {
				return new SwissRedPaymentOrderTO();
			}

			@Override
			protected SwissRedStandingOrderTO createEmptyStandingOrder() {
				return new SwissRedStandingOrderTO();
			}
			
			@Override
			protected PaymentDefaults getPaymentDefaults(SwissPaymentDefaultsResult template) {
				return template.getPaymentDefaults();
			}

			@Override
			protected SwissRedPaymentSlipTO getSlipFromTemplate(SwissRedPaymentTemplateTO template) {
				return template.getSwissRedPayment();
			}
			
			@Override
			public SwissRedPaymentSlipTO createEmptyPaymentSlip() {
				return new SwissRedPaymentSlipTO();
			}
		};
	}
	
	@Override
	public TemplateMethod<SwissRedPaymentSlipTO, SwissRedPaymentTemplateResult, SwissRedPaymentTemplateRequest, SwissRedPaymentTemplateTO> getTemplateMethod() {
		return new TemplateMethod<SwissRedPaymentSlipTO, SwissRedPaymentTemplateResult, SwissRedPaymentTemplateRequest, SwissRedPaymentTemplateTO>(this){

			@Override
			protected void setPayment(SwissRedPaymentTemplateTO template, SwissRedPaymentSlipTO payment) {
				template.setPayment(payment);
			}

			@Override
			protected SwissRedPaymentTemplateRequest getVerifyRequest(SwissRedPaymentTemplateTO template, RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
				return SwissRedPaymentService.verifyPaymentTemplate(template, rse);
			}

			@Override
			protected SwissRedPaymentTemplateRequest getSubmitRequest(SwissRedPaymentTemplateTO template, RequestStateEvent<SwissRedPaymentTemplateRequest> rse) {
				return SwissRedPaymentService.savePaymentTemplate(template, rse);
			}

			@Override
			protected SwissRedPaymentTemplateTO createEmptyTemplate() {
				return new SwissRedPaymentTemplateTO();
			}
			
		};
	}

	@Override
	public SwissRedPaymentOrderTO getPaymentOrder() {
		SwissRedPaymentOrderTO order = getViewDataMethod().getPaymentOrder();
		order.setPayment(getPayment());
		return order;
	}

	@Override
	public SwissRedStandingOrderTO getStandingOrder() {
		SwissRedStandingOrderTO order = getViewDataMethod().getStandingOrder();
		order.setPayment(getPayment());
		return order;
	}	

	@Override
	public PaymentType getPaymentType(){
		return PaymentType.SWISS_RED_PAYMENT_SLIP;
	}
	
}
