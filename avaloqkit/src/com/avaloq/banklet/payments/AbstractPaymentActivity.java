package com.avaloq.banklet.payments;

import java.math.BigDecimal;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.avaloq.afs.aggregation.to.LocalizedNotification;
import com.avaloq.afs.aggregation.to.LocalizedValidationResult;
import com.avaloq.afs.aggregation.to.Result;
import com.avaloq.afs.aggregation.to.payment.BasePaymentResult;
import com.avaloq.afs.aggregation.to.payment.PaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.StandingOrderTO;
import com.avaloq.afs.aggregation.to.user.settings.UserSettingsPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.BasePaymentTO;
import com.avaloq.afs.server.bsp.client.ws.BeneficiaryPaymentTO;
import com.avaloq.afs.aggregation.to.payment.PaymentMaskWrapperTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentSettingsTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentStateType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.ReasonedPaymentTO;
import com.avaloq.banklet.payments.methods.ApproveMethod;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.DeleteMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.views.AccountField;
import com.avaloq.banklet.payments.views.AliasField;
import com.avaloq.banklet.payments.views.AmountField;
import com.avaloq.banklet.payments.views.BankDetailsField;
import com.avaloq.banklet.payments.views.BeneficiaryField;
import com.avaloq.banklet.payments.views.ChargeOptionsField;
import com.avaloq.banklet.payments.views.CreditAccountField;
import com.avaloq.banklet.payments.views.DebitAdviceField;
import com.avaloq.banklet.payments.views.DebitReferenceField;
import com.avaloq.banklet.payments.views.ExecutionDateField;
import com.avaloq.banklet.payments.views.FavoriteTemplateField;
import com.avaloq.banklet.payments.views.PaymentField;
import com.avaloq.banklet.payments.views.PriorityField;
import com.avaloq.banklet.payments.views.StandingOrderActiveField;
import com.avaloq.banklet.payments.views.StandingOrderFirstDateField;
import com.avaloq.banklet.payments.views.StandingOrderEndDate;
import com.avaloq.banklet.payments.views.StandingOrderExecutionCountField;
import com.avaloq.banklet.payments.views.StandingOrderExecutionTypeField;
import com.avaloq.banklet.payments.views.StandingOrderLastDateField;
import com.avaloq.banklet.payments.views.StandingOrderNextDateField;
import com.avaloq.banklet.payments.views.StandingOrderValidityField;
import com.avaloq.banklet.payments.views.PaymentField.PaymentFieldInterface;
import com.avaloq.banklet.payments.views.StandingOrderField.ExecuteUntilType;
import com.avaloq.banklet.payments.views.PaymentReasonField;
import com.avaloq.banklet.payments.views.ReferenceNumberField;
import com.avaloq.banklet.payments.views.SalaryPaymentField;
import com.avaloq.banklet.payments.views.StandingOrderPeriodsField;
import com.avaloq.banklet.payments.views.TextCheckboxField;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsPaymentRequest;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsService;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.BlockingRunnable;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.CurrencyFormatInputFilter;
import com.avaloq.front.common.util.validation.ValidationSeverity;
import com.avaloq.med.avaloqocrservices.OrangePaymentSlipActivity;
import com.avaloq.med.avaloqocrservices.OrangePaymentSlipFragment;


public abstract class AbstractPaymentActivity<
	PaymentResult extends BasePaymentResult, 
	PaymentRequest extends AbstractServerRequest<PaymentResult>, 
	PaymentOrder extends PaymentOrderTO,
	PaymentSlip extends BasePaymentTO,
	StandingResult extends BasePaymentResult, 
	StandingRequest extends AbstractServerRequest<StandingResult>,
	StandingOrder extends StandingOrderTO,
	TemplateResult extends BasePaymentResult,
	TemplateRequest extends AbstractServerRequest<TemplateResult>,
	Template extends PaymentTemplateTO,
	DefaultsResult extends Result,
	DefaultsRequest extends AbstractServerRequest<DefaultsResult>
> extends BankletActivity implements PaymentFieldInterface{
	
	public PaymentSettingsTO mPaymentSettings = null;
	
	public enum SaveTemplateType{
		NONE, ORANGE, RED, DOMESTIC, INTERNATIONAL, TRANSFER 
	}
	
	public enum PaymentViewType{
		NEW, NEW_FROM_TEMPLATE, NEW_FROM_VIEW,
		VIEW,
		TEMPLATE, TEMPLATE_FROM_VIEW, TEMPLATE_NEW, NEW_FROM_MASK
	}
	
	public static final int REQUEST_CODE = 1;
	
	private PaymentUtil mUtil;
	
	private boolean isEditable = true;
	
	protected boolean isConfirmMode = false;
	
	public AliasField mFieldAlias;
	public AccountField mFieldAccount;
	public CreditAccountField mFieldCreditAccount;
	public BankDetailsField mFieldBankDetails;
	public BeneficiaryField mFieldBeneficiary;	
	public ReferenceNumberField mFieldReferenceNumber;
	public AmountField mFieldAmount;
	public SalaryPaymentField mFieldSalaryPayment;
	public ChargeOptionsField mFieldChargeOptions;
	public ExecutionDateField mFieldExecutionDate;
	public DebitReferenceField mFieldDebitReference;
	public TextCheckboxField mFieldSaveAsTemplate;
	public DebitAdviceField mFieldDebitAdvice;
	public PaymentReasonField mFieldPaymentDetails;
	public FavoriteTemplateField mFieldTemplateFavorite;
	public PriorityField mFieldPriority;
	
	public StandingOrderExecutionTypeField mFieldExecutionType;
	public StandingOrderPeriodsField mFieldPeriods;
	public StandingOrderFirstDateField mFieldStandingOrderFirstDate;
	public StandingOrderNextDateField mFieldStandingOrderNextDate;
	public StandingOrderLastDateField mFieldStandingOrderLastDate;
	public StandingOrderValidityField mFiledValidity;
	public StandingOrderExecutionCountField mFieldExecutionCount;
	public StandingOrderEndDate mFieldEndDate;
	public StandingOrderActiveField mFieldStandingOrderActive;
	
	protected TextView mTitleView;
	protected TextView mSubtitleView;
	
	private boolean mSuccessDialogVisible = false;
	private String mSuccessDialogTitle = "";
	private String mSuccessDialogMessage = "";
	private long mOrderId = 0;
	private SaveTemplateType mSaveTemplateType = SaveTemplateType.NONE;
	
	public static final String EXTRA_PAYMENT_VIEW_TYPE = "extra_payment_view_type";	
	public static final String EXTRA_ID = "extra_id";
	protected long mExtraId;
	public static final String EXTRA_IS_FROM_STANDING = "extra_is_from_standing";
	public static final String EXTRA_MASK = "extra_mask";
	protected Boolean mIsFromStanding;
	
	protected String mScannedPostAccount = null;
	protected String mScannedReferenceNumber = null;
	protected BigDecimal mScannedAmount = null;
	protected String mSlipType = "";
	
	protected boolean mPaymentSlipScanned = false;
	
	protected BlockingRunnable mSpinner;
	
	protected ViewDataMethod<PaymentResult, PaymentRequest, PaymentOrder, PaymentSlip, StandingResult, StandingRequest, StandingOrder, TemplateResult, TemplateRequest, Template, DefaultsResult, DefaultsRequest> mViewDataMethod;
	
	protected final String VERIFY_BUTTON_TAG = "verify_button";
	
	public interface HeaderData {
		
		public int getIconResId();
		
		public String getTitle();
		
		public String getSubtitle();
	}
	
	public interface ContentData {

		public boolean hasScanField();
		
        public boolean hasDebitMoneyAccount();
        
        public boolean hasCreditMoneyAccount();
        
        public boolean hasBeneficiaryBankDetails();

        public boolean hasBeneficiary();

        public boolean hasAccountNumber();

        public boolean hasReferenceNumber();
        
        public boolean hasAmountField();

        public boolean hasSalaryPayment();
                
        public boolean hasExecutionDateField();                
                
        public boolean hasChargeOptionType();                
        
        public boolean hasStandingOrderField();
                        
        public boolean hasSaveAsTemplateField();        
        
        public boolean hasDebitAdviceField();        

        public boolean hasPaymentReasonField();
    }
	
	public interface ButtonDef {
		
		public int getTextResId();
		
		public void onClick();
		
		public ButtonType getType();
		
		public String getTag();
	}
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pmt_activity_abstract);

		mUtil = new PaymentUtil(AbstractPaymentActivity.this);

		mSpinner = new BlockingRunnable(AbstractPaymentActivity.this, "", getString(R.string.pmt_processing));


		setTitle(getHeaderData().getTitle());
		if (savedInstanceState != null){
			mSuccessDialogVisible = savedInstanceState.getBoolean("mSuccessDialogVisible");
		}
		checkShowDialog();

		try {
			getDisplayType();

			loadUserPreferences();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns true, if the view should be created from a standing order.
	 * Returns false, if the view should be created from a normal payment order.
	 *
	 */
	public boolean isViewFromStanding(){
		if (mIsFromStanding == null){
			mIsFromStanding = getIntent().getBooleanExtra(EXTRA_IS_FROM_STANDING, false);
		}
		
		return mIsFromStanding;
	}
	
	/**
	 * 
	 * @param isoCode
	 */
	public void setCurrency(String isoCode){
		mFieldAmount.setCurrencyId(AvaloqApplication.getInstance().findCurrencyByISOCode(isoCode));
	}
	
	/**
	 * 
	 */
	public void showProgress(){
		mSpinner.show();
	}
	
	/**
	 * 
	 */
	public void hideProgress(){
		mSpinner.hide();
	}
	
	/**
	 * Checks the extra arguments to how to display the data, whether new payment, 
	 * template or just to display an executed order
	 * @throws Exception
	 */
	protected void getDisplayType() throws Exception{
		// what should be displayed
		PaymentMaskWrapperTO mask = null;
		if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(EXTRA_MASK))
			mask = (PaymentMaskWrapperTO) getIntent().getSerializableExtra(EXTRA_MASK);
		if (mask != null){
			getViewDataMethod().setMask(mask);
			getViewDataMethod().setViewType(PaymentViewType.NEW_FROM_MASK);
		}
		else {
			if (getIntent().getSerializableExtra(EXTRA_PAYMENT_VIEW_TYPE) != null){
				getViewDataMethod().setViewType((PaymentViewType) getIntent().getSerializableExtra(EXTRA_PAYMENT_VIEW_TYPE));
			}else{
				getViewDataMethod().setViewType(PaymentViewType.NEW);
			}
		}

		mExtraId = getIntent().getLongExtra(EXTRA_ID, 0);
			
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putBoolean("mSuccessDialogVisible", mSuccessDialogVisible);
	}
	
	public void setContentReady() {
		hideSpinner();
		showTitle();
		showHeaderArea();
		showContentArea();
		showButtonArea();
	}
	
	/**
	 * Hides the loading spinner
	 */
	private void hideSpinner(){
		findViewById(R.id.loading_container).setVisibility(View.GONE);
	}
	
	public void showError() {
		// TODO
		throw new RuntimeException("Error getting the underlying data.");
	}
	
	private void showTitle() {
		if(isConfirmMode) {
			setTitle(getString(R.string.pmt_payment_slip_confirm));
		} else {
			setTitle(getHeaderData().getTitle());
		}
	}
	
	private void showHeaderArea() {
		mTitleView = ((TextView)findViewById(R.id.pmt_activity_abstract_header_title));
		mSubtitleView = ((TextView)findViewById(R.id.pmt_activity_abstract_header_subtitle)); 
				
		HeaderData h = getHeaderData();
		((ImageView)findViewById(R.id.pmt_activity_abstract_header_icon)).setImageDrawable(getResources().getDrawable(h.getIconResId()));
		mTitleView.setText(h.getTitle());
		mSubtitleView.setText(h.getSubtitle());
		TextView badge = (TextView)findViewById(R.id.pmt_activity_abstract_header_badge);
		
		// keep the paddings that were set in xml
		int paddingTop = badge.getPaddingTop();
		int paddingLeft = badge.getPaddingLeft();
		int paddingRight = badge.getPaddingRight();
		int paddingBottom = badge.getPaddingBottom();
		
		if (getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE_FROM_VIEW){
			badge.setText(mUtil.getPaymentStatusString(PaymentStateType.INIT.toString().toLowerCase()));
		}else{
			badge.setText(mUtil.getPaymentStatusString(getViewDataMethod().getState().value().toLowerCase()));
		}

		badge.setTextColor(getResources().getColor(PaymentUtil.getStateTextColor(getViewDataMethod().getState())));
		badge.setBackgroundResource(PaymentUtil.getStateBackground(getViewDataMethod().getState()));
		
		// set the paddings to the values specified in xml, because the 9patch will overwrite them
		badge.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE) {
			if (resultCode == RESULT_OK) {		
				
				String strAccountNumber = data.getStringExtra("accountNumber");
				String strReferenceNumber = data.getStringExtra("referenceNumber");
				String strAmount = data.getStringExtra("amount");
				String strSlipType = data.getStringExtra("slipType");
				
				mScannedPostAccount = strAccountNumber; 
				mScannedReferenceNumber = strReferenceNumber;
				mScannedAmount = CurrencyFormatInputFilter.parseNumber(strAmount).divide(new BigDecimal(100.0));
				mSlipType = strSlipType;
				
				mPaymentSlipScanned = true;
				setValuesAfterScanning();
			} else if (resultCode == RESULT_CANCELED) {
				Toast.makeText(this, R.string.pmt_scan_cancelled, Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	protected void setValuesAfterScanning(){
		if (mFieldBeneficiary != null && mPaymentSlipScanned){
			
			mFieldBeneficiary.setPostAccountNumber(mScannedPostAccount);
			
			mFieldBeneficiary.validatePostAccount(null, true);
			
			mFieldReferenceNumber.setReferenceNumber(mScannedReferenceNumber);
			mFieldAmount.setAmount(mScannedAmount);
			
			/*
			 * Possible values:
			 * "01" = ISR in CHF
			 * "03" = COD-ISR in CHF (cash-on-delivery) 
			 * "04" = ISR+ in CHF
			 * "11" = ISR in CHF for credit to own account 
			 * "14" = ISR+ in CHF for credit to own account
			 * "21" = ISR in EUR
			 * "23" = ISR in EUR for credit to own account 
			 * "31" = ISR+ in EUR
			 * "33" = ISR+ in EUR for credit to own account
			 */
			
			// depending on the type, set the currency
			if (
					mSlipType.compareTo("01") == 0 ||
					mSlipType.compareTo("03") == 0 ||
					mSlipType.compareTo("04") == 0 ||
					mSlipType.compareTo("11") == 0 ||
					mSlipType.compareTo("14") == 0){
				//Set currency to CHF
				mFieldAmount.setCurrencyId(AvaloqApplication.getInstance().findCurrencyByISOCode("CHF"));
			}else if (
					mSlipType.compareTo("21") == 0 ||
					mSlipType.compareTo("23") == 0 ||
					mSlipType.compareTo("31") == 0 ||
					mSlipType.compareTo("33") == 0){
				// set currency to EUR
				mFieldAmount.setCurrencyId(AvaloqApplication.getInstance().findCurrencyByISOCode("EUR"));
			}
			
			mScannedPostAccount = null;
			mScannedReferenceNumber = null;
			mScannedAmount = null;
			
			
			mPaymentSlipScanned = false;
			
			findViewById(R.id.pmt_view_scan).setVisibility(View.GONE);
		}
	}
	
	protected void showContentArea() {
		ContentData c = getContentData();

		if (c.hasScanField() &&
				OrangePaymentSlipFragment.deviceIsCapableOfScanning(this) && 
				!isConfirmMode && 
				(getViewDataMethod().getViewType() == PaymentViewType.NEW ||
				 getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_TEMPLATE ||
				 getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_VIEW || 
				 getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_MASK)){
			findViewById(R.id.pmt_view_scan).setVisibility(View.VISIBLE);
			findViewById(R.id.pmt_view_scan).setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(AbstractPaymentActivity.this, OrangePaymentSlipActivity.class);
					startActivityForResult(intent, 1);
				}
			});			
		}else{
			findViewById(R.id.pmt_view_scan).setVisibility(View.GONE);
		}
		
		mFieldAlias = (AliasField)findViewById(R.id.pmt_activity_abstract_alias);
		mFieldAlias.setVisibility((getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE || getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE_FROM_VIEW) ? View.VISIBLE : View.GONE);
		mFieldAlias.setReadOnly(!isEditable());
		
		mFieldTemplateFavorite = (FavoriteTemplateField)findViewById(R.id.pmt_activity_abstract_favorite_template);
		mFieldTemplateFavorite.setVisibility((getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE || getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE_FROM_VIEW) ? View.VISIBLE : View.GONE);
		mFieldTemplateFavorite.setReadOnly(!isEditable());
		
		mFieldExecutionType = (StandingOrderExecutionTypeField)findViewById(R.id.pmt_activity_abstract_execution_type);
		mFieldExecutionType.setVisibility(isViewFromStanding() && getViewDataMethod().getViewType() != PaymentViewType.TEMPLATE_FROM_VIEW ? View.VISIBLE : View.GONE);
		mFieldExecutionType.setReadOnly(!isEditable());
		
		mFieldPeriods = (StandingOrderPeriodsField)findViewById(R.id.pmt_activity_abstract_periods);
		mFieldPeriods.setVisibility(isViewFromStanding() && getViewDataMethod().getViewType() != PaymentViewType.TEMPLATE_FROM_VIEW ? View.VISIBLE : View.GONE);
		mFieldPeriods.setReadOnly(!isEditable());
		
		mFieldStandingOrderFirstDate = (StandingOrderFirstDateField)findViewById(R.id.pmt_activity_abstract_standing_order_execution);
		mFieldStandingOrderFirstDate.setVisibility(isViewFromStanding() && getViewDataMethod().getViewType() != PaymentViewType.TEMPLATE_FROM_VIEW ? View.VISIBLE : View.GONE);
		mFieldStandingOrderFirstDate.setReadOnly(!isEditable());
		
		mFieldStandingOrderNextDate = (StandingOrderNextDateField)findViewById(R.id.pmt_activity_abstract_standing_order_next_execution);
		mFieldStandingOrderNextDate.setReadOnly(true);
		
		mFieldStandingOrderLastDate = (StandingOrderLastDateField)findViewById(R.id.pmt_activity_abstract_standing_order_last_execution);
		mFieldStandingOrderLastDate.setReadOnly(true);
		
		mFieldStandingOrderActive = (StandingOrderActiveField)findViewById(R.id.pmt_activity_abstract_standing_order_active);
		mFieldStandingOrderActive.setReadOnly(!isEditable());
		
		mFiledValidity = (StandingOrderValidityField)findViewById(R.id.pmt_activity_abstract_validity);
		mFiledValidity.setVisibility(isViewFromStanding() && getViewDataMethod().getViewType() != PaymentViewType.TEMPLATE_FROM_VIEW ? View.VISIBLE : View.GONE);
		mFiledValidity.setReadOnly(!isEditable());
		if (mFiledValidity.getVisibility() == View.VISIBLE){
			mFiledValidity.setOnItemChangedCallback(new Runnable(){
				@Override
				public void run() {
					if (mFiledValidity.getSelectedValue() == ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED)
						mFieldExecutionCount.setVisibility(View.VISIBLE);
					else
						mFieldExecutionCount.setVisibility(View.GONE);
					
					if (mFiledValidity.getSelectedValue() == ExecuteUntilType.END_DATE_REACHED)
						mFieldEndDate.setVisibility(View.VISIBLE);
					else
						mFieldEndDate.setVisibility(View.GONE);
				}
			});
		}
		
		mFieldExecutionCount = (StandingOrderExecutionCountField)findViewById(R.id.pmt_activity_abstract_execution_count);
		mFieldExecutionCount.setReadOnly(!isEditable());
		
		mFieldEndDate = (StandingOrderEndDate)findViewById(R.id.pmt_activity_abstract_end_date);
		mFieldEndDate.setReadOnly(!isEditable());
		
        mFieldAccount = (AccountField)findViewById(R.id.pmt_activity_abstract_select_account);
        mFieldAccount.setVisibility(c.hasDebitMoneyAccount() ? View.VISIBLE : View.GONE);
        mFieldAccount.setReadOnly(!isEditable());
        mFieldAccount.setDefaultAccountId(mPaymentSettings != null ? mPaymentSettings.getDefaultPaymentAccount() : null);
        mFieldAccount.setChangeListener(this);
        
        mFieldCreditAccount = (CreditAccountField)findViewById(R.id.pmt_activity_abstract_select_credit_account);
        mFieldCreditAccount.setVisibility(c.hasCreditMoneyAccount() ? View.VISIBLE : View.GONE);
        mFieldCreditAccount.setReadOnly(!isEditable());
        mFieldCreditAccount.setDefaultAccountId(mPaymentSettings != null ? mPaymentSettings.getDefaultPaymentAccount() : null);
        mFieldCreditAccount.setChangeListener(this);
        
        mFieldBeneficiary = (BeneficiaryField)findViewById(R.id.pmt_activity_abstract_beneficiary);
        mFieldBeneficiary.setDelegate(this);
        mFieldBeneficiary.setVisibility(c.hasBeneficiary() ? View.VISIBLE : View.GONE);
        mFieldBeneficiary.setActivity(AbstractPaymentActivity.this);
        mFieldBeneficiary.setReadOnly(!isEditable());
        mFieldBeneficiary.setChangeListener(this);
        
        mFieldReferenceNumber = (ReferenceNumberField)findViewById(R.id.pmt_activity_abstract_reference_number);
        mFieldReferenceNumber.setVisibility(c.hasReferenceNumber() ? View.VISIBLE : View.GONE);
        mFieldReferenceNumber.setReadOnly(!isEditable());
        mFieldReferenceNumber.setChangeListener(this);
        
        mFieldPriority = (PriorityField) findViewById(R.id.pmt_activity_abstract_priority);
        mFieldPriority.setVisibility(View.GONE);
        mFieldPriority.setReadOnly(!isEditable());

        mFieldAmount = (AmountField)findViewById(R.id.pmt_activity_abstract_amount);
        mFieldAmount.setVisibility(c.hasAmountField() ? View.VISIBLE : View.GONE);
        mFieldAmount.setReadOnly(!isEditable());
        mFieldAmount.setOnChangeCallback(new Runnable(){
			@Override
			public void run() {
				if (AvaloqApplication.shouldDisplayPriorityView(mFieldAmount.getSelectedCurrencyISO(), getPayment().getPaymentType()))
					mFieldPriority.setVisibility(View.VISIBLE);
				else
					mFieldPriority.setVisibility(View.GONE);
			}
        });
        mFieldAmount.setChangeListener(this);

        mFieldSalaryPayment = (SalaryPaymentField)findViewById(R.id.pmt_activity_abstract_salary_payment);
        mFieldSalaryPayment.setVisibility(c.hasSalaryPayment() ? View.VISIBLE : View.GONE);
        mFieldSalaryPayment.setReadOnly(!isEditable());
        
        mFieldChargeOptions = (ChargeOptionsField) findViewById(R.id.pmt_activity_abstract_chargeoptions);
        mFieldChargeOptions.setVisibility(c.hasChargeOptionType() ? View.VISIBLE : View.GONE);
        mFieldChargeOptions.setReadOnly(!isEditable());

        mFieldExecutionDate = (ExecutionDateField) findViewById(R.id.pmt_activity_abstract_executiondate);
        mFieldExecutionDate.setVisibility(c.hasExecutionDateField() && ! isViewFromStanding() ? View.VISIBLE : View.GONE);
        mFieldExecutionDate.setReadOnly(!isEditable());
        
        mFieldDebitReference = (DebitReferenceField) findViewById(R.id.pmt_activity_abstract_debitreference);
        mFieldDebitReference.setVisibility(View.VISIBLE);
        mFieldDebitReference.setReadOnly(!isEditable());
        
        mFieldSaveAsTemplate = (TextCheckboxField) findViewById(R.id.pmt_activity_abstract_saveastemplate);
        mFieldSaveAsTemplate.setText(getString(R.string.pmt_view_field_save_as_template));
        mFieldSaveAsTemplate.setVisibility(c.hasSaveAsTemplateField() ? View.VISIBLE : View.GONE);
        mFieldSaveAsTemplate.setReadOnly(!isEditable());

        mFieldDebitAdvice = (DebitAdviceField) findViewById(R.id.pmt_activity_abstract_debitadvice);
        mFieldDebitAdvice.setVisibility(c.hasDebitAdviceField() ? View.VISIBLE : View.GONE);
        mFieldDebitAdvice.setReadOnly(!isEditable());
        
        mFieldPaymentDetails = (PaymentReasonField) findViewById(R.id.pmt_activity_abstract_paymentdetails);
        mFieldPaymentDetails.setVisibility(c.hasPaymentReasonField() ? View.VISIBLE : View.GONE);
        mFieldPaymentDetails.setReadOnly(!isEditable());
        
        getViewDataMethod().populateFields();
                
        validatePayment();
	}

	public void fieldUpdated(PaymentField field){
		validatePayment();
	}
	
	public enum ButtonType{
		PRIMARY, SECONDARY, WARNING
	}
	
	protected void showButtonArea() {
		LinearLayout buttonArea = (LinearLayout) findViewById(R.id.pmt_activity_abstract_button_area);
		buttonArea.removeAllViews();
		for(final ButtonDef buttonDef : getButtonDefs()) {
			Button button;
			View containerView;
			switch (buttonDef.getType()){
			case SECONDARY:
				containerView = getLayoutInflater().inflate(R.layout.pmt_payment_secondary_button, null, false);
				button = (Button)containerView.findViewById(R.id.styled_button);
				break;
			case WARNING:
				containerView = getLayoutInflater().inflate(R.layout.pmt_payment_warning_button, null, false);
				button = (Button)containerView.findViewById(R.id.styled_button);
				break;
			case PRIMARY:
				containerView = getLayoutInflater().inflate(R.layout.pmt_payment_primary_button, null, false);
				button = (Button)containerView.findViewById(R.id.styled_button);
				break;
			default:
				containerView = getLayoutInflater().inflate(R.layout.pmt_payment_primary_button, null, false);
				button = (Button)containerView.findViewById(R.id.styled_button);
				break;
			}
			button.setText(buttonDef.getTextResId());
	        
			if (buttonDef.getTag() != null){
				button.setTag(buttonDef.getTag());
			}
			
			button.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					buttonDef.onClick();
				}
			});
			buttonArea.addView(containerView);
		}
	}
	
	public Button getVerifyButton(){
		LinearLayout buttonArea = (LinearLayout) findViewById(R.id.pmt_activity_abstract_button_area);
		return (Button) buttonArea.findViewWithTag(VERIFY_BUTTON_TAG);
	}
	
	public void resetErrors(){
		mFieldBeneficiary.setErrorText("");
		mFieldAmount.setErrorText("");
		mFieldAmount.setErrorCurrency("");
		mFieldExecutionDate.setErrorText("");
		mFieldReferenceNumber.setErrorText("");
	}
	
	/**
	 * Call this method in subclasses to show the error messages for the
	 * fields
	 * @param notificationList
	 */
	public void displayErrors(List<LocalizedNotification> notificationList, final Runnable executeIfNoErrors){
		
		resetErrors();
		
		boolean errorsPresent = false;
		
		// if there are no notifications
		if (notificationList == null || notificationList.size() == 0){
			executeIfNoErrors.run();
			return;
		}
				
		String errorString = "";
		String infoString = "";

		for (LocalizedNotification notification: notificationList){

			if (notification.getValidationResult().getValidationSeverity() == ValidationSeverity.ERROR)
			{
				String strError = ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage();

				// check for which field the notification has to be displayed
				if (
						notification.getValidationResult().getFieldName().compareTo("beneficiary1") == 0
						|| notification.getValidationResult().getFieldName().compareTo("pcAccount") == 0
						|| notification.getValidationResult().getFieldName().compareTo("beneficiaryIban") == 0
						|| notification.getValidationResult().getFieldName().compareTo("bic") == 0
						|| notification.getValidationResult().getFieldName().compareTo("beneficiaryBank1") == 0
						|| notification.getValidationResult().getFieldName().compareTo("beneficiary2") == 0
						){
					mFieldBeneficiary.setErrorText(
							mFieldBeneficiary.getErrorText()+								// previous message
							(mFieldBeneficiary.getErrorText().length() == 0 ? "" : "\n")+ 	// add new line if necessary
							strError);

				}else if (notification.getValidationResult().getFieldName().compareTo("debitMoneyAccount") == 0){
					mFieldAccount.setErrorText(strError);
				}else if (notification.getValidationResult().getFieldName().compareTo("amount") == 0){
					mFieldAmount.setErrorText(strError);
				}else if (notification.getValidationResult().getFieldName().compareTo("executionDate") == 0 || notification.getValidationResult().getFieldName().compareTo("transactionDate") == 0){
					mFieldExecutionDate.setErrorText(strError);
				}else if (notification.getValidationResult().getFieldName().compareTo("referenceNumber") == 0){
					mFieldReferenceNumber.setErrorText(strError);
				}else if (notification.getValidationResult().getFieldName().compareTo("currency") == 0){
					mFieldAmount.setErrorCurrency(strError);
				}

				errorString += ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage()+'\n';
				errorsPresent = true;
			}else{					
				infoString += ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage()+'\n';
			}

		}

		if (errorsPresent){
			DialogFragment.createAlert(getString(R.string.pmt_view_field_error), errorString, this).show(this);
		}else{
			AlertDialog.Builder readyBuilder = new AlertDialog.Builder(this);
			readyBuilder
				.setMessage(infoString)
				.setPositiveButton(R.string.avq_ok, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						executeIfNoErrors.run();
					}					
				});
				
			readyBuilder.create().show();
		}	
	}
	
	public void setConfirmMode(){
		isConfirmMode = true;
		isEditable = false;
		setContentReady();
		try{
			((ScrollView) findViewById(R.id.pmt_payment_main_scrollview)).fullScroll(ScrollView.FOCUS_UP);
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public boolean getIsConfirmMode(){
		return isConfirmMode;
	}
	
	public void setEditableMode(){
		isConfirmMode = false;
		isEditable = true;
		setContentReady();
	}
	
	public boolean isEditable() {
		return isEditable;
	}
	
	public void setIsEditable(boolean editable) {
		isEditable = editable;
	}

	
	@Override
	public void onBackPressed(){
		if (isConfirmMode){
			setEditableMode();
		}else{
			super.onBackPressed();
		}
	}
	
	public void showSuccessfulSubmission(){
		showSuccessfulSubmission(mOrderId, mSaveTemplateType);
	}
	
	public void showSuccessfulSubmission(long orderId, SaveTemplateType saveTemplateType){
		
		mOrderId = orderId;
		mSaveTemplateType = saveTemplateType; 
		
//		if (mSuccessDialogTitle.isEmpty()){
//			mSuccessDialogTitle = getString(R.string.pmt_view_field_success);
//		}
		
		if (mSuccessDialogMessage.isEmpty()){
			mSuccessDialogMessage = getString(R.string.pmt_view_field_success);
		}
		
		showSuccessfulSubmission(mSuccessDialogTitle, mSuccessDialogMessage, mOrderId, saveTemplateType);
	}
	
	public void showSuccessfulSubmission(String title, String message, long orderId, final SaveTemplateType saveTemplateType){
		
		mSuccessDialogTitle = title;
		mSuccessDialogMessage = message;
		
		
		mSuccessDialogVisible = true;
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this); 
		alertDialogBuilder
			.setTitle(title)
			.setMessage(message)
			.setOnCancelListener(new OnCancelListener() {				
				@Override
				public void onCancel(DialogInterface dialog) {
					clearSuccessDialog();
					finish();
					
					// create template using this order id
					if (mOrderId != 0){
						triggerCreateTemplate();				
					}
				}
			})
			.setPositiveButton(R.string.avq_ok, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) {
		            // Finish activity
		            finish();
		            
		         // create template using this order id
					if (mOrderId != 0){
						triggerCreateTemplate();
					}
		        }
		    });
		alertDialogBuilder.create().show();	
	}
	
	private void triggerCreateTemplate(){
		// create template using this order id
		if (mOrderId != 0){
			Intent i;
			switch (mSaveTemplateType){
			case DOMESTIC:
				i = new Intent(getBaseContext(), DomesticPaymentTemplateActivity.class);
				i.putExtra(DomesticPaymentTemplateActivity.EXTRA_ID, mOrderId);
				i.putExtra(DomesticPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
				if (isViewFromStanding())
					i.putExtra(DomesticPaymentTemplateActivity.EXTRA_IS_FROM_STANDING, true);
				startActivity(i);
				break;
			case INTERNATIONAL:
				i = new Intent(getBaseContext(), InternationalPaymentTemplateActivity.class);
				i.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, mOrderId);
				i.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
				if (isViewFromStanding())
					i.putExtra(InternationalPaymentTemplateActivity.EXTRA_IS_FROM_STANDING, true);
				startActivity(i);
				break;
			case NONE:
				break;
			case ORANGE:
				i = new Intent(getBaseContext(), SwissOrangePaymentTemplateActivity.class);
				i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_ID, mOrderId);
				i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
				if (isViewFromStanding()){
					i.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_IS_FROM_STANDING, true);
				}
				startActivity(i);
				break;
			case RED:
				i = new Intent(getBaseContext(), SwissRedPaymentTemplateActivity.class);
				i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_ID, mOrderId);
				i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
				if (isViewFromStanding())
					i.putExtra(SwissRedPaymentTemplateActivity.EXTRA_IS_FROM_STANDING, true);
				startActivity(i);
				break;
			case TRANSFER:
				i = new Intent(getBaseContext(), AccountTransferTemplateActivity.class);
				i.putExtra(AccountTransferTemplateActivity.EXTRA_ID, mOrderId);
				i.putExtra(AccountTransferTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
				if (isViewFromStanding())
					i.putExtra(AccountTransferTemplateActivity.EXTRA_IS_FROM_STANDING, true);
				startActivity(i);
				break;
			default:
				break;
			
			}						
		}
	}
	
	public boolean isSaveFromTemplate(){
		return mFieldSaveAsTemplate.isChecked();
	}
	
	private void clearSuccessDialog(){
		mSuccessDialogVisible = false;
	}
	
	private void checkShowDialog(){
		if (mSuccessDialogVisible){
			showSuccessfulSubmission();
		}
	}
		
	private void loadUserPreferences(){		
		UserSettingsPaymentRequest request = UserSettingsService.getPaymentSettings(new RequestStateEvent<UserSettingsPaymentRequest>() {
			@Override
			public void onRequestCompleted(UserSettingsPaymentRequest aRequest) {
				UserSettingsPaymentResult result = aRequest.getResponse().getData();
				
				if (result != null){
					mPaymentSettings = result.getPaymentSettings();		
					loadData();
				}
			}
		});
		
		request.initiateServerRequest();
	}
	
	void loadData(){
		getViewDataMethod().loadData(mExtraId);
	}
	
	public abstract void populateFields();
	
	abstract HeaderData getHeaderData();
	
	abstract ContentData getContentData();
	
	abstract List<ButtonDef> getButtonDefs();
	
	public abstract VerifyMethod<PaymentOrder, PaymentRequest, StandingOrder, StandingRequest> getVerifyMethod();
	
	public abstract ConfirmMethod<PaymentOrder, PaymentRequest, StandingOrder, StandingRequest> getConfirmMethod();
	
	public abstract SubmissionResultMethod<PaymentResult, PaymentRequest, StandingResult, StandingRequest> getSubmissionResultMethod();
	
	public DeleteMethod<PaymentOrder, StandingOrder> getDeleteMethod(){
		return new DeleteMethod<PaymentOrder, StandingOrder>(this) {
			@Override
			protected PaymentOrder getPaymentOrderForMethod() {
				return getPaymentOrder();
			}

			@Override
			protected StandingOrder getStandingOrderForMethod() {
				return getStandingOrder();
			}
		};
	}
	
	public ApproveMethod<PaymentOrder, StandingOrder> getApproveMethod(){
		return new ApproveMethod<PaymentOrder, StandingOrder>(this) {
			@Override
			protected PaymentOrder getPaymentOrderForMethod() {
				return getPaymentOrder();
			}

			@Override
			protected StandingOrder getStandingOrderForMethod() {
				return getStandingOrder();
			}
		};
	}
	
	public abstract TemplateMethod<PaymentSlip, TemplateResult, TemplateRequest, Template> getTemplateMethod();
	
	public abstract ViewDataMethod<PaymentResult, PaymentRequest, PaymentOrder, PaymentSlip, StandingResult, StandingRequest, StandingOrder, TemplateResult, TemplateRequest, Template, DefaultsResult, DefaultsRequest> createViewDataMethod();
	
	public final ViewDataMethod<PaymentResult, PaymentRequest, PaymentOrder, PaymentSlip, StandingResult, StandingRequest, StandingOrder, TemplateResult, TemplateRequest, Template, DefaultsResult, DefaultsRequest>  
	getViewDataMethod(){
		if (mViewDataMethod == null)
			mViewDataMethod = createViewDataMethod();
		return mViewDataMethod;
	}
	
	public final PaymentSlip getPayment(){
		PaymentSlip slip = getViewDataMethod().getPaymentFromRequest();
		if (slip == null)
			slip = getViewDataMethod().createEmptyPaymentSlip();
		
		if (mFieldAccount.getSelectedMoneyAccountTO() != null)
			slip.setDebitMoneyAccountId(mFieldAccount.getSelectedMoneyAccountTO().getId());

		slip.setAmount(mFieldAmount.getAmount());
		slip.setCurrencyId(mFieldAmount.getCurrencyId());
		slip.setSalary(mFieldSalaryPayment.isChecked());
		slip.setDebitReference(mFieldDebitReference.getValue());
		if (mFieldPriority.getVisibility() == View.VISIBLE)
			slip.setPaymentPriority(mFieldPriority.getSelectedValue());
		
		if (slip instanceof BeneficiaryPaymentTO){
			BeneficiaryPaymentTO beneficiarySlip = (BeneficiaryPaymentTO)slip;
			
			beneficiarySlip.setBeneficiary1(mFieldBeneficiary.getBeneficiary()[0]);
			beneficiarySlip.setBeneficiary2(mFieldBeneficiary.getBeneficiary()[1]);
			beneficiarySlip.setBeneficiary3(mFieldBeneficiary.getBeneficiary()[2]);
			beneficiarySlip.setBeneficiary4(mFieldBeneficiary.getBeneficiary()[3]);
			// Bank details
			beneficiarySlip.setBeneficiaryBank1(mFieldBeneficiary.getBankDetails()[0]);
			beneficiarySlip.setBeneficiaryBank2(mFieldBeneficiary.getBankDetails()[1]);
			beneficiarySlip.setBeneficiaryBank3(mFieldBeneficiary.getBankDetails()[2]);
			beneficiarySlip.setBeneficiaryBank4(mFieldBeneficiary.getBankDetails()[3]);
			
			beneficiarySlip.setAdviceOption(mFieldDebitAdvice.getAdviceOption());

			//slip = (PaymentSlip)beneficiarySlip;
		}
		
		if (slip instanceof ReasonedPaymentTO){
			ReasonedPaymentTO reasonedSlip = (ReasonedPaymentTO)slip;
			reasonedSlip.setBeneficiaryAccountNo(mFieldBeneficiary.getAccount());
			reasonedSlip.setBeneficiaryIban(mFieldBeneficiary.getAccountIBAN());
			
			reasonedSlip.setPaymentReason1(mFieldPaymentDetails.getPaymentReasons()[0]);
			reasonedSlip.setPaymentReason2(mFieldPaymentDetails.getPaymentReasons()[1]);
			reasonedSlip.setPaymentReason3(mFieldPaymentDetails.getPaymentReasons()[2]);
			reasonedSlip.setPaymentReason4(mFieldPaymentDetails.getPaymentReasons()[3]);
			
			if (mFieldBeneficiary.getNtnl() != null && !mFieldBeneficiary.getNtnl().equals(""))
				reasonedSlip.setNtnl(mFieldBeneficiary.getNtnl());
			
			reasonedSlip.setChargeOption(mFieldChargeOptions.getChargeOptionType());
			
			//slip = (PaymentSlip)reasonedSlip;
		}
		
		return fillPayment(slip);
	}
	
	protected abstract PaymentSlip fillPayment(PaymentSlip paymentSlip);
	
	public abstract PaymentOrder getPaymentOrder();
	
	public abstract StandingOrder getStandingOrder();
	
	public abstract PaymentType getPaymentType();
	
	/**
	 * Before submitting the payment for verifications, the fields have to be validated
	 * @return
	 */
	public boolean validatePayment(){
		
		Log.d("DDD", "Validate payment called");
		
		boolean errorsExist = false;
		
		// depending on the payment type, check if the fields are valid
		
		// Checks common for all payments;
		
		// Debit account
		if (mFieldAccount.getSelectedMoneyAccountTO() == null){
			mFieldAccount.setErrorText("Select a debit account");
			errorsExist = true;
		}else{
			mFieldAccount.setErrorText("");
		}
		// Beneficiary field
		if (!mFieldBeneficiary.isSet()){
			mFieldBeneficiary.setErrorText("Select a beneficiary");
			errorsExist = true;
		}else{
			mFieldBeneficiary.setErrorText("");
		}
		// Amount field
		if (mFieldAmount.getAmount() == null || mFieldAmount.getAmount().equals(BigDecimal.ZERO)){
			mFieldAmount.setErrorText("Amount to pay must be greater than 0");
			errorsExist = true;
		}else{
			mFieldAmount.setErrorText("");
		}
		
		switch (getPaymentType()){
		case DOMESTIC_PAYMENT:			
			break;
		case INTERNAL_PAYMENT:
			// Credit account
			if (mFieldCreditAccount.getSelectedMoneyAccountTO() == null){
				mFieldCreditAccount.setErrorText("Select a credit account");
				errorsExist = true;
			}else{
				mFieldCreditAccount.setErrorText("");
			}
			break;			
		case INTERNATIONAL_PAYMENT:
			break;
		case SWISS_ORANGE_PAYMENT_SLIP:
			// Orange - payment reference field
			if (mFieldReferenceNumber.getReferenceNumber() == null || mFieldReferenceNumber.getReferenceNumber().isEmpty()){
				mFieldReferenceNumber.setErrorText("The reference number must be set");
				errorsExist = true;
			}else{
				mFieldReferenceNumber.setErrorText("");
			}
			break;
		case SWISS_RED_PAYMENT_SLIP:
			break;
		default:
			break;
		
		}
		
		// disable/enable the validate button if there are or if there are no errors.
		if (errorsExist){			
			if (getVerifyButton() != null){
				getVerifyButton().setEnabled(false);
			}
		}else{
			if (getVerifyButton() != null){
				getVerifyButton().setEnabled(true);
			}
		}
		
		return !errorsExist;
	}
	
	public void verifyPayment(){
		getVerifyMethod().run();
	}
	
	protected void confirmPayment(){
		getConfirmMethod().run();
	}
	
	protected void deletePayment(){
		getDeleteMethod().run();
	}
	
	protected void approvePayment(){
		getApproveMethod().run();
	}
	
	protected void savePaymentTemplate(){
		getTemplateMethod().save();
	}
	
	protected void deletePaymentTemplate(){
		getTemplateMethod().delete();
	}
	
}
