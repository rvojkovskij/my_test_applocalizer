package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;

import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.banklet.payments.fragment.DirectDebitAgreements;
import com.avaloq.banklet.payments.fragment.PaymentList;
import com.avaloq.banklet.payments.fragment.PaymentOverview;
import com.avaloq.banklet.payments.fragment.PendingPayments;
import com.avaloq.banklet.payments.fragment.StandingOrders;
import com.avaloq.banklet.payments.fragment.StartupSmallDevice;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentApprovalInfoListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;

/**
 * Shows the trading overview in the viewpager and loads the defaults.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class PaymentActivity extends TabbedBankletActivity {
	
	@Override
	public void prepareNavigationItems() {
		
		PaymentOverviewService.getPaymentApprovals(0l, 2147483647l, new RequestStateEvent<PaymentApprovalInfoListRequest>() {
			@Override
			public void onRequestCompleted(PaymentApprovalInfoListRequest aRequest) {
				PaymentBanklet.getInstance().setPengingPayments(aRequest.getResponse().getData().getPaymentInfoList());
				PaymentBanklet.getInstance().setPendingStandingOrders(aRequest.getResponse().getData().getstandingPaymentInfoList());

				if (BankletActivityDelegate.isSmallDevice(PaymentActivity.this)){
					showNavigationItems(new FragmentNavigationItem[] {
							new FragmentNavigationItem(StartupSmallDevice.class, getResources().getString(R.string.pmt_new_payment))
					});
				}
				else {
					Bundle bundleTemplate = new Bundle();
					bundleTemplate.putBoolean(PaymentOverview.EXTRA_IS_TEMPLATE_OVERVIEW, true);
					Bundle bundleStandingOrder = new Bundle();
					bundleStandingOrder.putBoolean(PaymentOverview.EXTRA_IS_STANDING_ORDER, true);
					ArrayList<FragmentNavigationItem> items =  new ArrayList<FragmentNavigationItem>();
		
					items.add(new FragmentNavigationItem(PaymentOverview.class, getResources().getString(R.string.pmt_new_payment)));
					items.add(new FragmentNavigationItem(PaymentList.class, getResources().getString(R.string.pmt_created_payments)));
					items.add(new FragmentNavigationItem(PaymentOverview.class, getResources().getString(R.string.pmt_standing_orders), bundleStandingOrder));
					items.add(new FragmentNavigationItem(PaymentOverview.class, getResources().getString(R.string.pmt_templates), bundleTemplate));
					items.add(new FragmentNavigationItem(DirectDebitAgreements.class, getResources().getString(R.string.pmt_direct_debit)));
		
					if (PaymentBanklet.getInstance().hasPendingApprovals())
						items.add(new FragmentNavigationItem(PendingPayments.class, getString(R.string.pmt_pending_title), R.drawable.pmt_pending_icon));
		
					FragmentNavigationItem[] itemsArray = new FragmentNavigationItem[items.size()];
					showNavigationItems(items.toArray(itemsArray));
				}
			}
		}).initiateServerRequest();
	}

}
