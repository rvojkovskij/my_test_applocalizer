package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;

import com.avaloq.afs.server.bsp.client.ws.PaymentActionType;
import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderAction;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.R;

public class InternationalPaymentViewActivity extends InternationalPaymentAbstractActivity{

	OrderType orderType = OrderType.PAYMENT;
	
	@Override
	List<ButtonDef> getButtonDefs() {
		
		List<ButtonDef> buttonList = new ArrayList<ButtonDef>();
		final PaymentUtil util = new PaymentUtil(this);
		PaymentActionType type;
		
		if (isViewFromStanding()){
			type = getViewDataMethod().getStandingResult().getInternationalStandingOrder().getPaymentActionType();
			orderType = OrderType.STANDING;
		}
		else
			type = getViewDataMethod().getPaymentResult().getInternationalPaymentOrder().getPaymentActionType();
		
		if (isEditable()){
			// Add the APPROVE button if necessary
			if (type == PaymentActionType.EDIT_DELETE_APPROVE || type == PaymentActionType.DELETE_APPROVE){
				buttonList.add(new ButtonDef(){
					@Override
					public int getTextResId() {
						return util.getPaymentStringId(OrderAction.APPROVE, orderType);
					}
					@Override
					public void onClick() {
						approvePayment();
					}
					@Override
					public ButtonType getType() {
						return ButtonType.PRIMARY;
					}
					@Override
					public String getTag() {
						// TODO Auto-generated method stub
						return null;
					}
				});
			}
			
			// Add the EDIT button if necessary
			if (type == PaymentActionType.EDIT_DELETE || type == PaymentActionType.EDIT_DELETE_APPROVE){
				buttonList.add(new ButtonDef(){
					@Override
					public int getTextResId() {
						return util.getPaymentStringId(OrderAction.EDIT, orderType);
					}
					@Override
					public void onClick() {
						verifyPayment();
					}
					@Override
					public ButtonType getType() {
						return ButtonType.PRIMARY;
					}
					@Override
					public String getTag() {
						// TODO Auto-generated method stub
						return null;
					}
				});
			}
			
			// Add the DELETE button if necessary
			if (type == PaymentActionType.EDIT_DELETE || type == PaymentActionType.EDIT_DELETE_APPROVE 
					|| type == PaymentActionType.DELETE_APPROVE || type == PaymentActionType.DELETE){
				buttonList.add(new ButtonDef(){
					@Override
					public int getTextResId() {
						return util.getPaymentStringId(OrderAction.DELETE, orderType);
					}
					@Override
					public void onClick() {
						deletePayment();
					}
					@Override
					public ButtonType getType() {
						return ButtonType.WARNING;
					}
					@Override
					public String getTag() {
						// TODO Auto-generated method stub
						return null;
					}
				});
			}
	
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.COPY, orderType);
				}
				@Override
				public void onClick() {
					Intent i = new Intent(InternationalPaymentViewActivity.this, InternationalPaymentActivity.class);
					i.putExtra(InternationalPaymentActivity.EXTRA_ID, mExtraId);
					if (isViewFromStanding())
						i.putExtra(InternationalPaymentActivity.EXTRA_IS_FROM_STANDING, true);
					i.putExtra(InternationalPaymentActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.NEW_FROM_VIEW);
					startActivity(i);
				}
				@Override
				public ButtonType getType() {
					return ButtonType.SECONDARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
			
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return R.string.pmt_template_save_template;
				}
				@Override
				public void onClick() {
					Intent i = new Intent(InternationalPaymentViewActivity.this, InternationalPaymentTemplateActivity.class);
					i.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, mExtraId);
					if (isViewFromStanding())
						i.putExtra(InternationalPaymentTemplateActivity.EXTRA_IS_FROM_STANDING, true);
					i.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE_FROM_VIEW);
					startActivity(i);
				}
				@Override
				public ButtonType getType() {
					return ButtonType.SECONDARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}
		else {
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.CONFIRM, orderType);
				}
				@Override
				public void onClick() {
					confirmPayment();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}

		return buttonList;
	}
	
}
