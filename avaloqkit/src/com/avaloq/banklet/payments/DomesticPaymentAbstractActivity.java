package com.avaloq.banklet.payments;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentDefaultsResult;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentResult;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentTemplateResult;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticPaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingOrderTO;
import com.avaloq.afs.aggregation.to.payment.domestic.DomesticStandingPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.DomesticPaymentTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.VerifyWithSubmissionMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.banklet.payments.views.BeneficiaryField.BeneficiaryFieldPaymentType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticPaymentDefaultsRequest;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticPaymentRequest;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticPaymentService;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticPaymentTemplateRequest;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticStandingPaymentRequest;

abstract public class DomesticPaymentAbstractActivity 
	extends AbstractPaymentActivity<
		DomesticPaymentResult, DomesticPaymentRequest, DomesticPaymentOrderTO, DomesticPaymentTO,
		DomesticStandingPaymentResult, DomesticStandingPaymentRequest, DomesticStandingOrderTO,
		DomesticPaymentTemplateResult, DomesticPaymentTemplateRequest, DomesticPaymentTemplateTO,
		DomesticPaymentDefaultsResult, DomesticPaymentDefaultsRequest
	> {

	/**
	 * Override in child classes if something special is needed for the header
	 */
	@Override
	HeaderData getHeaderData() {
		return new HeaderData() {

			@Override
			public int getIconResId() {
				return R.drawable.pmt_new_domestic;
			}

			@Override
			public String getTitle() {
				return getResources().getString(R.string.pmt_payment_type_domestic_payment);
			}

			@Override
			public String getSubtitle() {
				switch (getViewDataMethod().getViewType()) {
				case NEW:
				case NEW_FROM_TEMPLATE:
				case NEW_FROM_VIEW:
				case VIEW:
					return getResources().getString(R.string.pmt_payment_subtype_single_payment);
				case TEMPLATE:
				case TEMPLATE_FROM_VIEW:
					return getResources().getString(R.string.pmt_payment_subtype_template);
				default:
					return getResources().getString(R.string.pmt_payment_subtype_single_payment);
				}
			}
		};
	}

	@Override
	public void populateFields() {
		mFieldBeneficiary.setPaymentType(BeneficiaryFieldPaymentType.DOMESTIC);
		mFieldBeneficiary.setCountries(getViewDataMethod().getDefaults().getPaymentDefaults().getCountries());
	}

	abstract List<ButtonDef> getButtonDefs();

	@Override
	ContentData getContentData() {
		return new ContentData() {

			public boolean hasDebitMoneyAccount() {
				return true;
			}

			@Override
			public boolean hasBeneficiaryBankDetails() {
				return true;
			}

			@Override
			public boolean hasBeneficiary() {
				return true;
			}

			public boolean hasAccountNumber() {
				return true;
			}

			@Override
			public boolean hasReferenceNumber() {
				return false;
			}

			@Override
			public boolean hasAmountField() {
				return true;
			}

			@Override
			public boolean hasSalaryPayment() {
				return true;
			}

			@Override
			public boolean hasExecutionDateField() {
				return true;
			}

			@Override
			public boolean hasChargeOptionType() {
				return true;
			}

			@Override
			public boolean hasStandingOrderField() {
				return true;
			}

			@Override
			public boolean hasSaveAsTemplateField() {
				return getViewDataMethod().getViewType() == PaymentViewType.NEW || getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_TEMPLATE
						|| getViewDataMethod().getViewType() == PaymentViewType.NEW_FROM_VIEW;
			}

			@Override
			public boolean hasDebitAdviceField() {
				return true;
			}

			@Override
			public boolean hasPaymentReasonField() {
				return true;
			}

			@Override
			public boolean hasCreditMoneyAccount() {
				return false;
			}

			@Override
			public boolean hasScanField() {
				// TODO Auto-generated method stub
				return false;
			}
		};
	}
	
	@Override
	public DomesticPaymentTO fillPayment(DomesticPaymentTO payment){
		payment.setPaymentType(PaymentType.DOMESTIC_PAYMENT);
		return payment;
	}
	
	@Override
	public VerifyMethod<DomesticPaymentOrderTO, DomesticPaymentRequest, DomesticStandingOrderTO, DomesticStandingPaymentRequest> getVerifyMethod() {
		return new VerifyWithSubmissionMethod<DomesticPaymentOrderTO, DomesticPaymentRequest, DomesticStandingOrderTO, DomesticStandingPaymentRequest>(this) {

			@Override
			protected DomesticPaymentRequest getPaymentRequest(DomesticPaymentOrderTO order, RequestStateEvent<DomesticPaymentRequest> rse) {
				return DomesticPaymentService.verifyPayment(order, rse);
			}

			@Override
			protected DomesticStandingPaymentRequest getStandingRequest(DomesticStandingOrderTO order, RequestStateEvent<DomesticStandingPaymentRequest> rse) {
				return DomesticPaymentService.verifyStandingPayment(order, rse);
			}
		};
	}

	@Override
	public ConfirmMethod<DomesticPaymentOrderTO, DomesticPaymentRequest, DomesticStandingOrderTO, DomesticStandingPaymentRequest> getConfirmMethod() {
		return new ConfirmMethod<DomesticPaymentOrderTO, DomesticPaymentRequest, DomesticStandingOrderTO, DomesticStandingPaymentRequest>(this) {

			@Override
			protected DomesticPaymentRequest getPaymentRequest(DomesticPaymentOrderTO order, RequestStateEvent<DomesticPaymentRequest> rse) {
				return DomesticPaymentService.submitPayment(order, rse);
			}

			@Override
			protected DomesticStandingPaymentRequest getStandingRequest(DomesticStandingOrderTO order, RequestStateEvent<DomesticStandingPaymentRequest> rse) {
				return DomesticPaymentService.submitStandingPayment(order, rse);
			}
		};
	}

	@Override
	public SubmissionResultMethod<DomesticPaymentResult, DomesticPaymentRequest, DomesticStandingPaymentResult, DomesticStandingPaymentRequest> getSubmissionResultMethod() {
		return new SubmissionResultMethod<DomesticPaymentResult, DomesticPaymentRequest, DomesticStandingPaymentResult, DomesticStandingPaymentRequest>(this) {

			@Override
			public SaveTemplateType getSaveTemplateType() {
				return SaveTemplateType.DOMESTIC;
			}
		};
	}

	@Override
	public ViewDataMethod<DomesticPaymentResult, DomesticPaymentRequest, DomesticPaymentOrderTO, DomesticPaymentTO, DomesticStandingPaymentResult, DomesticStandingPaymentRequest, DomesticStandingOrderTO, DomesticPaymentTemplateResult, DomesticPaymentTemplateRequest, DomesticPaymentTemplateTO, DomesticPaymentDefaultsResult, DomesticPaymentDefaultsRequest> createViewDataMethod() {
		return new ViewDataMethod<DomesticPaymentResult, DomesticPaymentRequest, DomesticPaymentOrderTO, DomesticPaymentTO, DomesticStandingPaymentResult, DomesticStandingPaymentRequest, DomesticStandingOrderTO, DomesticPaymentTemplateResult, DomesticPaymentTemplateRequest, DomesticPaymentTemplateTO, DomesticPaymentDefaultsResult, DomesticPaymentDefaultsRequest>(this) {

			@Override
			protected DomesticPaymentRequest getPaymentRequest(long paymentId,RequestStateEvent<DomesticPaymentRequest> rse) {
				return DomesticPaymentService.getPayment(paymentId, rse);
			}

			@Override
			protected DomesticStandingPaymentRequest getStandingRequest(long paymentId, RequestStateEvent<DomesticStandingPaymentRequest> rse) {
				return DomesticPaymentService.getStandingPayment(paymentId, rse);
			}

			@Override
			protected DomesticPaymentTemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
				return DomesticPaymentService.getPaymentTemplate(paymentId, rse);
			}

			@Override
			protected DomesticPaymentDefaultsRequest getDefaultsRequest(RequestStateEvent<DomesticPaymentDefaultsRequest> rse) {
				return DomesticPaymentService.getDefaults(rse);
			}

			@Override
			protected DomesticPaymentOrderTO getPaymentOrder(DomesticPaymentResult result) {
				return result.getDomesticPaymentOrder();
			}

			@Override
			protected DomesticStandingOrderTO getStandingOrder(DomesticStandingPaymentResult result) {
				return result.getDomsticStandingOrder();
			}

			@Override
			protected DomesticPaymentTemplateTO getPaymentTemplate(DomesticPaymentTemplateResult result) {
				return result.getSwissDomesticPaymentTemplate();
			}

			@Override
			protected DomesticPaymentTO getSlipFromPaymentOrder(DomesticPaymentOrderTO result) {
				return result.getDomesticPayment();
			}

			@Override
			protected DomesticPaymentTO getSlipFromStandingOrder(DomesticStandingOrderTO result) {
				return result.getDomesticPayment();
			}

			@Override
			protected DomesticPaymentOrderTO createEmptyPaymentOrder() {
				return new DomesticPaymentOrderTO();
			}

			@Override
			protected DomesticStandingOrderTO createEmptyStandingOrder() {
				return new DomesticStandingOrderTO();
			}
			
			@Override
			protected PaymentDefaults getPaymentDefaults(DomesticPaymentDefaultsResult template) {
				return template.getPaymentDefaults();
			}
			
			@Override
			protected DomesticPaymentTO getSlipFromTemplate(DomesticPaymentTemplateTO template) {
				return template.getDomesticPayment();
			}
			
			@Override
			public DomesticPaymentTO createEmptyPaymentSlip() {
				return new DomesticPaymentTO();
			}
		};
	}
	
	@Override
	public TemplateMethod<DomesticPaymentTO, DomesticPaymentTemplateResult, DomesticPaymentTemplateRequest, DomesticPaymentTemplateTO> getTemplateMethod() {
		return new TemplateMethod<DomesticPaymentTO, DomesticPaymentTemplateResult, DomesticPaymentTemplateRequest, DomesticPaymentTemplateTO>(this){

			@Override
			protected void setPayment(DomesticPaymentTemplateTO template, DomesticPaymentTO payment) {
				template.setPayment(payment);
			}

			@Override
			protected DomesticPaymentTemplateRequest getVerifyRequest(DomesticPaymentTemplateTO template, RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
				return DomesticPaymentService.verifyPaymentTemplate(template, rse);
			}

			@Override
			protected DomesticPaymentTemplateRequest getSubmitRequest(DomesticPaymentTemplateTO template, RequestStateEvent<DomesticPaymentTemplateRequest> rse) {
				return DomesticPaymentService.savePaymentTemplate(template, rse);
			}

			@Override
			protected DomesticPaymentTemplateTO createEmptyTemplate() {
				return new DomesticPaymentTemplateTO();
			}
			
		};
	}

	@Override
	public DomesticPaymentOrderTO getPaymentOrder() {
		DomesticPaymentOrderTO order = getViewDataMethod().getPaymentOrder();
		order.setPayment(getPayment());
		return order;
	}

	@Override
	public DomesticStandingOrderTO getStandingOrder() {
		DomesticStandingOrderTO order = getViewDataMethod().getStandingOrder();
		order.setPayment(getPayment());
		return order;
	}

	@Override
	public PaymentType getPaymentType(){
		return PaymentType.DOMESTIC_PAYMENT;
	}
	
}
