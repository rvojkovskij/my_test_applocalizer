package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

/**
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class SalaryPaymentField extends TextCheckboxField {

    public SalaryPaymentField(Context context) {
        super(context);
        init(context);
    }

    public SalaryPaymentField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SalaryPaymentField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    protected void init(Context context) {
    	super.init(context);
    	setText(getContext().getString(R.string.pmt_view_field_salary_payment));
    }



}
