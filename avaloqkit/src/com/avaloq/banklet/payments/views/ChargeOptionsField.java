package com.avaloq.banklet.payments.views;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.afs.server.bsp.client.ws.ChargeOptionType;
import com.avaloq.framework.R;

public class ChargeOptionsField extends PaymentField{

	private class ChargeOption{
		public ChargeOptionType type;
		public String text;
	}
	
	private static final String TAG = ChargeOptionsField.class.getSimpleName();
	
	private TextView mTextChargeOption = null;
	
	private int mSelectedOptionIndex = 0;
		
	public ChargeOptionsField(Context context) {
		super(context);
		init(context);
	}
	
	public ChargeOptionsField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public ChargeOptionsField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
				
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_chargeoption, this, true);
		mTextChargeOption = (TextView)view.findViewById(R.id.pmt_view_field_chargeoption);		
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_chargeoption_error);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
	                SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
	                new SelectChargeoptionDialogFragment().show(ft, null);
            	}
            }
        });
		
		show();
	}
	
	private void show(){
		mTextChargeOption.setText(getItems().get(mSelectedOptionIndex).text);
		
		if (getErrorText() != null){		
			mTextError.setText(getErrorText());
		}
        mTextError.setVisibility(TextUtils.isEmpty(getErrorText()) ? View.GONE : View.VISIBLE);
	}
	
	public ChargeOptionType getChargeOptionType(){
		return getItems().get(mSelectedOptionIndex).type;
	}
	
	public void setChargeOption(ChargeOptionType type){
		
		// find the index of this option
		int position = 0;
		for(ChargeOption co: getItems()){			
			if (type == co.type){
				mSelectedOptionIndex = position;
				break;
			}
			position++;
		}
		
		show();
	}
	
	public List<ChargeOption> getItems(){
		
		List<ChargeOption> items = new ArrayList<ChargeOption>();
		
		for (int i = 0; i < ChargeOptionType.values().length; i++){
			
			if (ChargeOptionType.values()[i] != ChargeOptionType.EMPTY){			
				ChargeOption co = new ChargeOption();
				co.type = ChargeOptionType.values()[i];
				
				int id = getResources().getIdentifier("pmt_chargeoption_"+ChargeOptionType.values()[i].name(), "string", getContext().getPackageName());
				co.text = id == 0 ? ChargeOptionType.values()[i].name() : (String) getResources().getText(id);								 
				items.add(co);
			}
		}
		
		return items;
	}
	
	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();		
	}

	// TODO check if it needs to be private
    @SuppressLint("ValidFragment")
	private class SelectChargeoptionDialogFragment extends DialogFragment {

        private class ChargeOptionAdapter extends ArrayAdapter<ChargeOption> {
        	
            private ChargeOptionAdapter(Context context, List<ChargeOption> objects) {
                super(context, 0, objects);                
            }

            private class ViewHolder {
                TextView mTextChargeOption;
                ImageView buttonFavorite;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_chargeoption_row, null);
                    ViewHolder h = new ViewHolder();
                    h.mTextChargeOption = (TextView)row.findViewById(R.id.pmt_tvChargeoption);                    
                    h.buttonFavorite = (ImageView)row.findViewById(R.id.pmt_view_field_chargeoption_select);
                    row.setTag(h);
                }
                final ViewHolder holder = (ViewHolder)row.getTag();
                final ChargeOption account = getItem(position);
                holder.mTextChargeOption.setText(account.text);
                
                holder.buttonFavorite.setVisibility((position == mSelectedOptionIndex ? View.VISIBLE : View.INVISIBLE));
                                
                row.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      
                    	if(position == mSelectedOptionIndex) {
                            return;
                        }
                        holder.buttonFavorite.setImageDrawable(getResources().getDrawable(android.R.drawable.btn_radio));
                        
                        mSelectedOptionIndex = position;
                        notifyDataSetChanged();
                        getDialog().dismiss();
                        ChargeOptionsField.this.show();
                    }
                });
                return row;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        	
        	// TODO move to strings        	
            getDialog().setTitle("Select charge option type");
            View view = inflater.inflate(R.layout.pmt_view_field_chargeoption_list, container, true);
            final ListView listView = (ListView)view.findViewById(R.id.pmt_view_field_chargeoption_listview);
            
            listView.setAdapter(new ChargeOptionAdapter(getActivity(), getItems()));
            
            return view;
        }

    }
	
}
