package com.avaloq.banklet.payments.views;

import java.util.LinkedHashMap;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;
import com.avaloq.afs.server.bsp.client.ws.PaymentPriority;

public class PriorityField extends SingleChoiceField<PaymentPriority>{
	public PriorityField(Context context) {
		super(context);
	}
	
	public PriorityField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public PriorityField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getResources().getString(R.string.payment_priority_label);
	}

	@Override
	public LinkedHashMap<PaymentPriority, String> getElements() {
		LinkedHashMap<PaymentPriority, String> map = new LinkedHashMap<PaymentPriority, String>();
		map.put(PaymentPriority.NORMAL, getResources().getString(R.string.payment_priority_normal));
		map.put(PaymentPriority.URGENT, getResources().getString(R.string.payment_priority_urgent));
		return map;
	}

	@Override
	public PaymentPriority getDefaultValue() {
		return PaymentPriority.NORMAL;
	}
}
