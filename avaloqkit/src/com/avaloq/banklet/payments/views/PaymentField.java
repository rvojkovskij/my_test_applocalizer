package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class PaymentField extends LinearLayout{
	
	FragmentActivity mFragmentActivity;
	
	public interface PaymentFieldInterface{
		public void fieldUpdated(PaymentField field);
	}
	
	protected PaymentFieldInterface mChangeListener;
	
	protected boolean mIsReadOnly; 
	
	protected String mErrorText = "";
	
	protected TextView mTextError = null;
	
	public void setActivity(FragmentActivity fragmentActivity){
		mFragmentActivity = fragmentActivity;
	}
	
	public FragmentActivity getActivity(){
		return mFragmentActivity;
	}
	
	public PaymentField(Context context) {
		super(context);
	}

	public PaymentField(Context context, AttributeSet attrs) {		
		super(context, attrs);		
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public PaymentField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}
	
	public void setChangeListener(PaymentFieldInterface changeListener){
		mChangeListener = changeListener;
	}
	
	public boolean isReadOnly() {
		return mIsReadOnly;
	}

	public void setReadOnly(boolean isReadOnly) {
		this.mIsReadOnly = isReadOnly;
		readOnlyStateSet();
	}
	
	public String getErrorText() {
        return mErrorText;
    }

    public void setErrorText(String errorText) {
        mErrorText = errorText;
        errorTextSet();
    }
    
    public void stateChanged(){
    	if (mChangeListener != null) mChangeListener.fieldUpdated(this);
    }
    
    public abstract void errorTextSet();
    
    public abstract void readOnlyStateSet();
    
}
