package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

public class StandingOrderExecutionCountField extends TextField{
	public StandingOrderExecutionCountField(Context context) {
		super(context);
	}
	
	public StandingOrderExecutionCountField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderExecutionCountField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getResources().getString(R.string.pmt_view_field_number_of_executions);
	}
	
	@Override
	protected boolean isNumber() {
		return true;
	}
}
