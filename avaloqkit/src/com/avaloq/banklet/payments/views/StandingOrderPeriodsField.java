package com.avaloq.banklet.payments.views;

import java.util.LinkedHashMap;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.avaloq.afs.aggregation.to.payment.StandingOrderPeriodListResult;
import com.avaloq.afs.server.bsp.client.ws.StandingOrderPeriodTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.StandingOrderPeriodListRequest;

public class StandingOrderPeriodsField extends SingleChoiceField<StandingOrderPeriodTO>{
	List<StandingOrderPeriodTO> mPeriodList;
	
	public StandingOrderPeriodsField(Context context) {
		super(context);
	}
	
	public StandingOrderPeriodsField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderPeriodsField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getResources().getString(R.string.pmt_view_field_recurrence);
	}

	@Override
	public LinkedHashMap<StandingOrderPeriodTO, String> getElements() {
		LinkedHashMap<StandingOrderPeriodTO, String> map = new LinkedHashMap<StandingOrderPeriodTO, String>();
		
		if (mPeriodList != null){
			for (StandingOrderPeriodTO period: mPeriodList){
				map.put(period, period.getName());
			}
		}
		
		return map;
	}
	
	@Override
	public void init(final Context context) {
		requestOrderPeriods(new Runnable() {
			@Override
			public void run() {
				StandingOrderPeriodsField.super.init(context);
			}
		});
	}

	@Override
	public StandingOrderPeriodTO getDefaultValue() {
		if (mPeriodList != null)
			return mPeriodList.get(0);
		else
			return null;
	}
	
	public void setSelectedValue(Long id){
		if (mPeriodList != null){
			for (StandingOrderPeriodTO period: mPeriodList){
				if (period.getId().equals(id)){
					mSelectedValue = period;
					show();
					return;
				}
			}
		}
	}
	
	private void requestOrderPeriods(final Runnable callback){
		if (getVisibility() == View.VISIBLE){
			PaymentOverviewService.getStandingOrderPeriods(new RequestStateEvent<StandingOrderPeriodListRequest>() {
				@Override
				public void onRequestCompleted(StandingOrderPeriodListRequest request) {
					StandingOrderPeriodListResult response = request.getResponse().getData();
					if (response == null)
						onRequestFailed(request);
					else
						mPeriodList = response.getStandingOrderPeriodList();
					callback.run();
				}
	
				@Override
				public void onRequestFailed(StandingOrderPeriodListRequest request) {
					
				}
			}).initiateServerRequest();
		}
		else
			callback.run();
	}
}
