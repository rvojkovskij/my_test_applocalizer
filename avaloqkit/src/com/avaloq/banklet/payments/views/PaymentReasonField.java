package com.avaloq.banklet.payments.views;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.R;

public class PaymentReasonField extends PaymentField {

	private TextView textValue = null;    

    private String[] mPaymentReasons;

    public PaymentReasonField(Context context) {
		super(context);
		init(context);
	}

	public PaymentReasonField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public PaymentReasonField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_payment_reason, this, true);
		textValue = (TextView)view.findViewById(R.id.pmt_view_field_bank_details_value);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_bank_details_error);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
	                SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
	                new PaymentReasonsDialogFragment().show(ft, null);
            	}
            }
        });
		showPaymentReasons();
	}
	
	private void showPaymentReasons() {
		if(mPaymentReasons == null) {
			textValue.setText(getContext().getString(R.string.pmt_view_field_payment_reason));
            mTextError.setText(mErrorText);
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		} else {
            textValue.setText(TextUtils.join("\n", filterEmptyStrings(mPaymentReasons)));
            mTextError.setText("");
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		}
	}

    public String [] getPaymentReasons() {
    	if (mPaymentReasons == null){
    		mPaymentReasons = new String[4];
    	}
        return mPaymentReasons;
    }

    public void setPaymentReasons(String [] bankDetails) {
        mPaymentReasons = bankDetails;
        showPaymentReasons();
    }

    // TODO check if needs to be private
    @SuppressLint("ValidFragment")
	private class PaymentReasonsDialogFragment extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().setTitle(getString(R.string.pmt_view_field_payment_reason));
            View view = inflater.inflate(R.layout.pmt_view_field_payment_reason_dialog, container, true);
            final EditText textPaymentReason1 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_name);
            final EditText textPaymentReason2 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse1);
            final EditText textPaymentReason3 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse2);
            final EditText textPaymentReason4 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_city);
            
            if (mPaymentReasons == null){
            	mPaymentReasons = new String[4];
            }
            
            textPaymentReason1.setText(mPaymentReasons[0]);
            textPaymentReason2.setText(mPaymentReasons[1]);
            textPaymentReason3.setText(mPaymentReasons[2]);
            textPaymentReason4.setText(mPaymentReasons[3]);
            
            
            Button buttonSave = (Button)view.findViewById(R.id.pmt_view_field_bank_details_save);
            buttonSave.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPaymentReasons = new String[] {
                        textPaymentReason1.getText().toString(),
                        textPaymentReason2.getText().toString(),
                        textPaymentReason3.getText().toString(),
                        textPaymentReason4.getText().toString()
                    };
                    showPaymentReasons();
                    getDialog().dismiss();
                }
            });
            return view;
        }

    }

    private String[] filterEmptyStrings(String[] in) {
		ArrayList<String> nonEmptyStrings = new ArrayList<String>();
		for(String i : in) {
			if(i != null && !i.trim().equals("")) {
				nonEmptyStrings.add(i);
			}
		}
		return nonEmptyStrings.toArray(new String[nonEmptyStrings.size()]);
	}

	@Override
	public void errorTextSet() {		
		showPaymentReasons();
	}

	@Override
	public void readOnlyStateSet() {
		showPaymentReasons();
	}

}