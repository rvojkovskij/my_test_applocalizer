package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

public abstract class TextField extends PaymentField{
	protected TextView textValue = null;
	protected TextView textValueReadonly = null;
	protected TextView textLabel = null;
	
	protected String fieldValue = "";
	

    public TextField(Context context) {
		super(context);
		init(context);
	}

	public TextField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public TextField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	protected boolean isNumber(){
		return false;
	}
	
	private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_text, this, true);
        textValue = (EditText)view.findViewById(R.id.pmt_view_field_value);
        textValue.addTextChangedListener(new TextWatcher() {
			
        	String b;
        	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				b = textValue.getText().toString();
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s.toString().compareTo(b) != 0) stateChanged();
			}
		});
        textValueReadonly = (TextView) view.findViewById(R.id.pmt_view_field_value_readonly);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_error);
        textLabel = (TextView)view.findViewById(R.id.pmt_view_field_label);
        setLabel(getLabel());
        if (isNumber()){
        	textValue.setInputType(InputType.TYPE_CLASS_NUMBER);
        }
    }
    
	public void show() {
		textValue.setText(fieldValue);
        mTextError.setText(mErrorText);
        mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
        
        if (isReadOnly()){
        	// hide input, show textview
        	textValue.setVisibility(View.GONE);
        	textValueReadonly.setText(textValue.getText());
        	textValueReadonly.setVisibility(View.VISIBLE);
        }else{
        	// hide textview, show input        	
        	textValueReadonly.setVisibility(View.GONE);
        	textValue.setVisibility(View.VISIBLE);
        }
        
        textValue.setEnabled(!isReadOnly());        
    }

    public String getValue() {
    	fieldValue = textValue.getText().toString();
        return fieldValue;
    }

    public void setValue(String referenceNumber) {
    	
    	String prev = fieldValue;
    	
    	if (referenceNumber == null)
    		fieldValue = "";
    	else
    		fieldValue = referenceNumber;
    	
        show();
        
        if (fieldValue.compareTo(prev) != 0) stateChanged();
    }
    
    public abstract String getLabel();
    
    public void setLabel(int id){
    	textLabel.setText(id);
    }
    
    public void setLabel(String text){
    	textLabel.setText(text);
    }

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}
}
