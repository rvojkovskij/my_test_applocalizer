package com.avaloq.banklet.payments.views;

import java.util.Calendar;
import java.util.Date;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.R;

import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.TextView;

public abstract class DateField extends PaymentField{
	protected TextView mTextDate = null;
	protected TextView textLabel = null;
	
	protected Date mSelectedDate;
	
	protected final int MAX_TRIES = 10;
	
	public DateField(Context context) {
		super(context);
		init(context);
	}
	
	public DateField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public DateField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	protected Date getDefaultDate(){
		mSelectedDate = Calendar.getInstance().getTime();
		// FIXME move these hard-coded values to the app config. As of 17.09.2013, it is not clear yet how these values have to be set
		int cutOffHour = 16;
		int cutOffMinute = 30;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mSelectedDate);
		
		// next day if it is after the cutoff time
		if (calendar.get(Calendar.HOUR_OF_DAY) > cutOffHour || (calendar.get(Calendar.HOUR_OF_DAY) == cutOffHour && calendar.get(Calendar.MINUTE)>cutOffMinute)){
			calendar.add(Calendar.DATE, 1);
		}
		
		return calendar.getTime();
	}
	
	public abstract String getLabel();
	
	private void init(Context context) {
		
		mSelectedDate = getDefaultDate();
		
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_execution_date, this, true);
		mTextDate = (TextView)view.findViewById(R.id.pmt_view_field_date);		
		mTextError = (TextView)view.findViewById(R.id.pmt_view_field_date_error);
		textLabel = (TextView)view.findViewById(R.id.pmt_view_field_amount_label);
		setLabel(getLabel());
		
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
    				Calendar calendar = Calendar.getInstance();
    				calendar.setTime(mSelectedDate);

    				DatePickerDialog dpd = new DatePickerDialog(getContext(), new OnDateSetListener() {
						
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							Calendar cal = Calendar.getInstance();
							cal.set(Calendar.YEAR, year);
							cal.set(Calendar.MONTH, monthOfYear);
							cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							mSelectedDate = cal.getTime();
							if (!validateDate())
								setErrorText(getContext().getString(R.string.pmt_validate_date_weekend_or_holiday_fail));
							else{
								setErrorText("");
								show();
							}
						}
					}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

    				dpd.show();
            	}
            }
        });
		
		show();
	}	
	
	private void show(){
		String strDate = formatDate();
		mTextDate.setText(strDate);
		
		mTextError.setText(getErrorText());
        mTextError.setVisibility(TextUtils.isEmpty(getErrorText()) ? View.GONE : View.VISIBLE);
	}
	
	protected String formatDate(){
		return DateFormat.getDateFormat(getContext().getApplicationContext()).format(mSelectedDate);
	}

	public Date getSelectedDate() {
		return mSelectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		mSelectedDate = selectedDate;
		show();
	}

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}
	
	public void setLabel(String string){
    	textLabel.setText(string);
    }
	
	protected boolean validateDate(){
		PaymentDefaults defaults = ((AbstractPaymentActivity<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?>)getContext()).getViewDataMethod().getPaymentDefaultsFromRequest();
		Calendar calSelected = Calendar.getInstance();
		calSelected.setTime(mSelectedDate);
		
		if (calSelected.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calSelected.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			return false;
		
		for (Date holiday: defaults.getBusinessUnitHolidays()){
			Calendar calHoliday = Calendar.getInstance();
			calHoliday.setTime(holiday);
			if (	calHoliday.get(Calendar.DAY_OF_MONTH) == calSelected.get(Calendar.DAY_OF_MONTH) &&
					calHoliday.get(Calendar.YEAR) == calSelected.get(Calendar.YEAR) &&
					calHoliday.get(Calendar.MONTH) == calSelected.get(Calendar.MONTH))
				return false;
		}
		return true;
	}
}
