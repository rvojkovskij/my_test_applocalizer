package com.avaloq.banklet.payments.views;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.R;

public class BankDetailsField extends PaymentField {

	private TextView textValue = null;    

    private String[] mBankDetails;

    public BankDetailsField(Context context) {
		super(context);
		init(context);
	}

	public BankDetailsField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public BankDetailsField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_bank_details, this, true);
		textValue = (TextView)view.findViewById(R.id.pmt_view_field_bank_details_value);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_bank_details_error);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
	                SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
	                new BankDetailsDialogFragment().show(ft, null);
            	}
            }
        });
		showBankDetails();
	}
	
	private void showBankDetails() {
		if(mBankDetails == null) {
			textValue.setText("");
            mTextError.setText(mErrorText);
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		} else {
            textValue.setText(TextUtils.join("\n", filterEmptyStrings(mBankDetails)));
            mTextError.setText("");
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		}
	}

    public String [] getBankDetails() {
        return mBankDetails;
    }

    public void setBankDetails(String [] bankDetails) {
        mBankDetails = bankDetails;
        showBankDetails();
    }

    // TODO check if needs to be private
    @SuppressLint("ValidFragment")
	private class BankDetailsDialogFragment extends DialogFragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().setTitle(R.string.pmt_view_field_bank_details_dialog);
            View view = inflater.inflate(R.layout.pmt_view_field_bank_details_dialog, container, true);
            final EditText textName = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_name);
            final EditText textAdress1 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse1);
            final EditText textAdress2 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse2);
            final EditText textCity = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_city);
            Button buttonSave = (Button)view.findViewById(R.id.pmt_view_field_bank_details_save);
            buttonSave.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBankDetails = new String[] {
                        textName.getText().toString(),
                        textAdress1.getText().toString(),
                        textAdress2.getText().toString(),
                        textCity.getText().toString()
                    };
                    showBankDetails();
                    getDialog().dismiss();
                }
            });
            return view;
        }

    }

    private String[] filterEmptyStrings(String[] in) {
        ArrayList<String> nonEmptyStrings = new ArrayList<String>();
        for(String i : in) {
            if(i != null) {
                nonEmptyStrings.add(i);
            }
        }
        return nonEmptyStrings.toArray(new String[nonEmptyStrings.size()]);
    }

	@Override
	public void errorTextSet() {		
		showBankDetails();
	}

	@Override
	public void readOnlyStateSet() {
		showBankDetails();
	}

}