package com.avaloq.banklet.payments.views;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.afs.server.bsp.client.ws.AdviceOptionType;
import com.avaloq.framework.R;

public class DebitAdviceField extends PaymentField{

	private class AdviceOption{
		public AdviceOptionType type;
		public String text;
	}
	
	private static final String TAG = ChargeOptionsField.class.getSimpleName();
	
	private TextView mTextChargeOption = null;
	
	private int mSelectedOptionIndex;
		
	public DebitAdviceField(Context context) {
		super(context);
		init(context);
	}
	
	public DebitAdviceField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public DebitAdviceField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
		
	}
	
	private void init(Context context) {
				
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_debitadvice, this, true);
		mTextChargeOption = (TextView)view.findViewById(R.id.pmt_view_field_chargeoption);		
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_chargeoption_error);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
	                SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
	                new SelectAdviceoptionDialogFragment().show(ft, null);
            	}
            }
        });
		
		show();
	}
	
	private void show(){
		mTextChargeOption.setText(getItems().get(mSelectedOptionIndex).text);
		
		if (getErrorText() != null){		
			mTextError.setText(getErrorText());
		}
        mTextError.setVisibility(TextUtils.isEmpty(getErrorText()) ? View.GONE : View.VISIBLE);
	}
	
	public AdviceOptionType getAdviceOption(){
		return getItems().get(mSelectedOptionIndex).type;
	}
	
	public void setAdviceOption(AdviceOptionType type){
		
		// find the index of this option
		int position = 0;
		for(AdviceOption ao: getItems()){			
			if (type == ao.type){
				mSelectedOptionIndex = position;
				break;
			}
			position++;
		}
		
		show();
	}
	
	public List<AdviceOption> getItems(){
		
		List<AdviceOption> items = new ArrayList<AdviceOption>();
		
		for (int i = 0; i < AdviceOptionType.values().length; i++){
									
			AdviceOption co = new AdviceOption();
			co.type = AdviceOptionType.values()[i];
			
			int id = getResources().getIdentifier("pmt_adviceoption_"+AdviceOptionType.values()[i].name(), "string", getContext().getPackageName());
			co.text = id == 0 ? AdviceOptionType.values()[i].name() : (String) getResources().getText(id);								 
			items.add(co);
			
		}
		
		return items;
	}
	
	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();		
	}

	// TODO check if it needs to be private
    @SuppressLint("ValidFragment")
	private class SelectAdviceoptionDialogFragment extends DialogFragment {

        private class ChargeOptionAdapter extends ArrayAdapter<AdviceOption> {
        	
            private ChargeOptionAdapter(Context context, List<AdviceOption> objects) {
                super(context, 0, objects);                
            }

            private class ViewHolder {
                TextView mTextChargeOption;
                ImageView buttonFavorite;
            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_debitadvice_row, null);
                    ViewHolder h = new ViewHolder();
                    h.mTextChargeOption = (TextView)row.findViewById(R.id.pmt_tvChargeoption);                    
                    h.buttonFavorite = (ImageView)row.findViewById(R.id.pmt_view_field_chargeoption_select);
                    row.setTag(h);
                }
                final ViewHolder holder = (ViewHolder)row.getTag();
                final AdviceOption account = getItem(position);
                holder.mTextChargeOption.setText(account.text);
                
                holder.buttonFavorite.setVisibility((position == mSelectedOptionIndex ? View.VISIBLE : View.INVISIBLE));
                                
                row.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      
                    	if(position == mSelectedOptionIndex) {
                            return;
                        }
                        holder.buttonFavorite.setImageDrawable(getResources().getDrawable(android.R.drawable.btn_radio));
                        
                        mSelectedOptionIndex = position;
                        notifyDataSetChanged();
                        getDialog().dismiss();
                        DebitAdviceField.this.show();
                    }
                });
                return row;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        	
        	// TODO move to strings        	
            getDialog().setTitle(R.string.pmt_view_field_select_advice);
            View view = inflater.inflate(R.layout.pmt_view_field_debitadvice_list, container, true);
            final ListView listView = (ListView)view.findViewById(R.id.pmt_view_field_chargeoption_listview);
            
            listView.setAdapter(new ChargeOptionAdapter(getActivity(), getItems()));
            
            return view;
        }

    }
	
}
