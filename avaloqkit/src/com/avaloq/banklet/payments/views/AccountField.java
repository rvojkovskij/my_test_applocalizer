package com.avaloq.banklet.payments.views;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentSettingsTO;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentMoneyAccountListRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsPaymentRequest;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsService;
import com.avaloq.framework.util.CurrencyUtil;

public class AccountField extends PaymentField {

    private static final String TAG = AccountField.class.getSimpleName();
	
	private TextView textAlias = null;
	
	private TextView textIban = null;

    private TextView textAmount = null;

    private MoneyAccountTO mSelectedMoneyAccount;

    private Long mDefaultAccount;
    
    private ListView listView;
    
    private List<MoneyAccountTO> mListMoneyAccounts = null;
    
    private Runnable onSelectedCallback = null;
    
	public AccountField(Context context) {
		super(context);
		init(context);
	}
	
	public AccountField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public AccountField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_account, this, true);
		textAlias = (TextView)view.findViewById(R.id.pmt_view_field_account_alias);
		textIban = (TextView)view.findViewById(R.id.pmt_view_field_account_iban);
        textAmount = (TextView)view.findViewById(R.id.pmt_view_account_field_amount);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_account_error);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
	                SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
	                
	                // If the list of accounts is not ready, the field has to request it from the server
	                if (mListMoneyAccounts== null){
	                	new SelectAccountDialogFragment().requestMoneyAccounts().show(ft, null);
	                }else{
	                	new SelectAccountDialogFragment().show(ft, null);
	                }
            	}
            }
        });
		show();
	}
	
	public void show() {		
		if(mSelectedMoneyAccount == null) {
			textAlias.setText(getContext().getString(R.string.pmt_view_field_from));
			textIban.setVisibility(View.GONE);
			textIban.setText("");
			textAmount.setVisibility(View.GONE);
			textAmount.setText("");
            mTextError.setText(mErrorText);
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		} else {
			textIban.setVisibility(View.VISIBLE);
			textAmount.setVisibility(View.VISIBLE);
			textAlias.setText(new MessageFormat("{0} {1}").format(new String[]{ getContext().getResources().getString(R.string.pmt_current_account), AvaloqApplication.getInstance().findCurrencyById(mSelectedMoneyAccount.getCurrencyId()).getIsoCode()}));
			textIban.setText(mSelectedMoneyAccount.getAccountIban());
			textAmount.setText(CurrencyUtil.formatMoney(mSelectedMoneyAccount.getAmount(), mSelectedMoneyAccount.getCurrencyId()));
            mTextError.setText(mErrorText);
            mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
		}
	}

    public MoneyAccountTO getSelectedMoneyAccountTO() {
        return mSelectedMoneyAccount;
    }

    public void setSelectedMoneyAccountTO(MoneyAccountTO moneyAccountTO) {
        mSelectedMoneyAccount = moneyAccountTO;
        stateChanged();
        if (onSelectedCallback != null)
        	onSelectedCallback.run();
        show();
    }

    
    // TODO check if it needs to be private
    @SuppressLint("ValidFragment")
	private class SelectAccountDialogFragment extends DialogFragment {

        private class AccountAdapter extends ArrayAdapter<MoneyAccountTO> {

            private long mDefaultDebitMoneyAccount;

            private AccountAdapter(Context context, long defaultDebitMoneyAccount, List<MoneyAccountTO> objects) {
                super(context, 0, objects);
                mDefaultDebitMoneyAccount = defaultDebitMoneyAccount;
            }

            private class ViewHolder {
                TextView textAlias;
                TextView textIban;
                TextView textAmount;
                ImageButton buttonFavorite;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_account_select_account_row, null);
                    ViewHolder h = new ViewHolder();
                    h.textAlias = (TextView)row.findViewById(R.id.pmt_view_field_account_select_account_row_alias);
                    h.textIban = (TextView)row.findViewById(R.id.pmt_view_field_account_select_account_row_iban);
                    h.textAmount = (TextView)row.findViewById(R.id.pmt_view_field_account_select_account_row_amount);
                    h.buttonFavorite = (ImageButton)row.findViewById(R.id.pmt_view_field_account_select_account_row_default);
                    row.setTag(h);
                }
                final ViewHolder holder = (ViewHolder)row.getTag();
                final MoneyAccountTO account = getItem(position);
                holder.textAlias.setText(account.getAccountAlias());
                holder.textIban.setText(account.getAccountIban());
                holder.textAmount.setText(CurrencyUtil.formatMoney(account.getAmount(), account.getCurrencyId()));
                holder.buttonFavorite.setImageDrawable(getResources().getDrawable(account.getId() == mDefaultDebitMoneyAccount ? R.drawable.pmt_ico_favorite_checked : R.drawable.pmt_ico_favorite_unchecked));
                holder.buttonFavorite.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(account.getId() == mDefaultDebitMoneyAccount) {
                            return;
                        }
                        holder.buttonFavorite.setImageDrawable(getResources().getDrawable(R.drawable.pmt_favorite));
                        PaymentSettingsTO paymentSettings = new PaymentSettingsTO();
                        
                        paymentSettings.setDefaultPaymentAccount(account.getId());
                        
                        UserSettingsPaymentRequest req =  UserSettingsService.updatePaymentSettings(paymentSettings, new RequestStateEvent<UserSettingsPaymentRequest>() {
                            @Override
                            public void onRequestCompleted(UserSettingsPaymentRequest aRequest) {
                            	com.avaloq.afs.aggregation.to.user.settings.UserSettingsPaymentResult result = aRequest.getResponse().getData();
                            	
                                mDefaultDebitMoneyAccount = result.getPaymentSettings().getDefaultPaymentAccount();
                                mDefaultAccount = mDefaultDebitMoneyAccount;
                                notifyDataSetChanged();
                            }
                        });
                        req.setCachePolicy(AbstractServerRequest.CachePolicy.NO_CACHE);
                        req.initiateServerRequest();
                    }
                });
                row.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setSelectedMoneyAccountTO(account);
                        getDialog().dismiss();
                    }
                });
                return row;
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            getDialog().setTitle(R.string.pmt_view_account_field_select_account);
            View view = inflater.inflate(R.layout.pmt_view_field_account_select_account, container, true);
            listView = (ListView)view.findViewById(R.id.pmt_view_field_account_select_account_list);
            listView.setAdapter(new AccountAdapter(getActivity(), 0, new ArrayList<MoneyAccountTO>()));
            
            // If the list of debit accounts and the selected account are ready, just set them
            if (mListMoneyAccounts != null){
            	listView.setAdapter(new AccountAdapter(getActivity(), (mDefaultAccount != null ? mDefaultAccount : 0), mListMoneyAccounts));
            }
            
            return view;
        }

        public SelectAccountDialogFragment requestMoneyAccounts(){
        	
        	if (mListMoneyAccounts == null){        	
	            PaymentOverviewService.getPaymentMoneyAccounts(new RequestStateEvent<PaymentMoneyAccountListRequest>() {
	                @Override
	                public void onRequestCompleted(PaymentMoneyAccountListRequest aRequest) {
	                    final List<MoneyAccountTO> moneyAccountList = aRequest.getResponse().getData().getMoneyAccounts();
	                    mListMoneyAccounts = moneyAccountList;
	                }
	            }).initiateServerRequest();
        	}else{        		
                listView.setAdapter(new AccountAdapter(getActivity(), (mDefaultAccount != null ? mDefaultAccount : 0), mListMoneyAccounts));
        	}
            return SelectAccountDialogFragment.this;
        }
    }

    
    /**
     * Sets the list of debit money accounts. If these are available already, then set the list using this method,
     * otherwise the field will load the debit money accounts from the webservice
     * @param listMoneyAccounts
     */
    public void setMoneyAccounts(List<MoneyAccountTO> listMoneyAccounts) {
		mListMoneyAccounts = listMoneyAccounts;
	}
    
    public void setDefaultAccountId(Long accountId){
    	mDefaultAccount = accountId;
    }
    
	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}
	
	public void setOnSelectedCallback(Runnable callback){
		onSelectedCallback = callback;
	}

}