package com.avaloq.banklet.payments.views;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.afs.aggregation.to.payment.StandingOrderPeriodListResult;
import com.avaloq.afs.server.bsp.client.ws.StandingOrderPeriodTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.StandingOrderPeriodListRequest;
import com.avaloq.framework.util.DateUtil;

public class StandingOrderField extends PaymentField {

	private Boolean isStandingOrderActive;
	private StandingOrderPeriodTO mStandingOrderPeriod;
	private Date mExecutionDate;
	private ExecutionType mExecutionType = ExecutionType.BEFORE;
	private ExecuteUntilType mExecuteUntil = ExecuteUntilType.FURTHER_NOTICE;
	private Integer mNumberOfExecutions;
	private Date mEndExecuteDate;
	
	Runnable onConfirmOrderDialog = null;
	
	public enum ExecuteUntilType{
		FURTHER_NOTICE, NUMBER_OF_EXECUTIONS_REACHED, END_DATE_REACHED
	}
	
	public enum ExecutionType{
		BEFORE, AFTER
	}
	
	public StandingOrderField(Context context) {
		super(context);
		init(context);
	}

	public StandingOrderField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_standingorder, this, true);
		mTextError = (TextView) view.findViewById(R.id.pmt_view_field_standing_order_error);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()) {
					SherlockFragmentActivity a = (SherlockFragmentActivity) getContext();
					FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
					new StandingOrderDialogFragment().show(ft, null);
				}
			}
		});
		show();
	}

	/**
	 * Displays the form field, not the dialog.
	 */
	private void show() {
		TextView line1 = ((TextView)findViewById(R.id.pmt_view_field_standing_order_line1));
		TextView line2 = ((TextView)findViewById(R.id.pmt_view_field_standing_order_line2));
		TextView line3 = ((TextView)findViewById(R.id.pmt_view_field_standing_order_line3));
		TextView line4 = ((TextView)findViewById(R.id.pmt_view_field_standing_order_line4));
		
		if (isStandingOrderActive == null || isStandingOrderActive == false){
			((TextView)findViewById(R.id.pmt_view_field_standing_order_line1)).setText("No");
			line2.setVisibility(View.GONE);
			line3.setVisibility(View.GONE);
			line4.setVisibility(View.GONE);
		}else{
			DateUtil dateUtil = new DateUtil(getContext());
			
			line1.setText(getContext().getString(R.string.pmt_standing_order_next_execution).replace("%Date%", dateUtil.format(mExecutionDate)));
			
			if (mStandingOrderPeriod != null)
				line2.setText(mStandingOrderPeriod.getName());
			line2.setVisibility(View.VISIBLE);
			
			switch (mExecuteUntil){
			case END_DATE_REACHED:
				line3.setText(getContext().getString(R.string.pmt_view_field_until_end_date_reached));
				break;
			case FURTHER_NOTICE:
				line3.setText(getContext().getString(R.string.pmt_view_field_until_further_notice));
				break;
			case NUMBER_OF_EXECUTIONS_REACHED:
				line3.setText(getContext().getString(R.string.pmt_view_field_until_number_of_executions_reached));
				break;
			
			}
			line3.setVisibility(View.VISIBLE);
			if (mExecuteUntil == ExecuteUntilType.END_DATE_REACHED || mExecuteUntil == ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED ){
				line4.setVisibility(View.VISIBLE);
				if (mExecuteUntil.equals(ExecuteUntilType.END_DATE_REACHED)){
					if (mEndExecuteDate != null)
						line4.setText(getContext().getString(R.string.pmt_standing_order_end_date).replace("%Date%", dateUtil.format(mEndExecuteDate)));
				}
				else{
					if (mNumberOfExecutions != null)
						line4.setText(getContext().getString(R.string.pmt_standing_order_executions_remaining).replace("%Count%", mNumberOfExecutions.toString()));
				}
					
			}
			else{
				line4.setVisibility(View.GONE);
			}
		}
		
		mTextError.setText(mErrorText);
		mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);

	}
	
	private void onConfirmOrderDialog(){
		show();
		Log.d("Test", "cccccc asd");
		if (onConfirmOrderDialog != null)
			onConfirmOrderDialog.run();
	}
	
	public void setOnConfirmOrderDialog(Runnable callback){
		onConfirmOrderDialog = callback;
	}

	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class StandingOrderDialogFragment extends DialogFragment {

		CheckBox cbStandingOrder;
		RadioButton rbExecuteBefore;
		RadioButton rbExecuteAfter;
		TextView tvNextExecutionDate;
		Spinner spinnerRecurrence;
		RadioButton rbUntilFurtherNotice;
		RadioButton rbUntilNumberReached;
		RadioButton rbUntilEndDate;
		TextView tvEndExecutionDate;
		EditText edtNumberOfExecutions;
		View containerEndExecutionDate;
		View containerNumberOfExecutions;
		
		View containerFields;
		
		public List<StandingOrderPeriodTO> mStandingOrderPeriods;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_bank_details_dialog);
			View view = inflater.inflate(R.layout.pmt_view_field_standingorder_dialog, container, true);
			
			cbStandingOrder = (CheckBox) view.findViewById(R.id.pmt_view_field_checkbox_standing_order);
			rbExecuteBefore = (RadioButton) view.findViewById(R.id.pmt_view_field_radio_execute_before);
			rbExecuteAfter = (RadioButton) view.findViewById(R.id.pmt_view_field_radio_execute_after);
			tvNextExecutionDate = (TextView) view.findViewById(R.id.pmt_view_field_textview_next_execution);
			spinnerRecurrence = (Spinner) view.findViewById(R.id.pmt_view_field_spinner_recurrence);
			rbUntilFurtherNotice = (RadioButton) view.findViewById(R.id.pmt_view_field_radio_until_further_notice);
			rbUntilNumberReached = (RadioButton) view.findViewById(R.id.pmt_view_field_radio_until_number_reached);
			rbUntilEndDate = (RadioButton) view.findViewById(R.id.pmt_view_field_radio_until_end_date);			
			tvEndExecutionDate = (TextView) view.findViewById(R.id.pmt_view_field_textview_end_execution);
			edtNumberOfExecutions = (EditText) view.findViewById(R.id.pmt_view_field_edittext_number_of_executions);
			
			edtNumberOfExecutions.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try{
						mNumberOfExecutions = Integer.valueOf(s.toString());
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					
				}
			});
			
			containerEndExecutionDate = view.findViewById(R.id.pmt_view_field_llEndExecutionDate);
			containerNumberOfExecutions = view.findViewById(R.id.pmt_view_field_llNumberOfExecutions);
			
			containerFields = view.findViewById(R.id.pmt_view_field_llFields);

			
			// display the defaults
			showExecutionDate();
			
			showEndDate();
			
			showRadioButtons();
			
			// Attach click listeners
			cbStandingOrder.setOnClickListener(clickListenerForStandingOrderCheckbox());
			
			rbExecuteBefore.setOnClickListener(clickListenerForExecution());
			rbExecuteAfter.setOnClickListener(clickListenerForExecution());
			
			rbUntilEndDate.setOnClickListener(clickListenerForLimit());
			rbUntilFurtherNotice.setOnClickListener(clickListenerForLimit());
			rbUntilNumberReached.setOnClickListener(clickListenerForLimit());
						
			tvNextExecutionDate.setOnClickListener(clickListenerForNextExecutionDate());
			tvEndExecutionDate.setOnClickListener(clickListenerForEndExecutionDate());
				
			if (isStandingOrderActive == null) isStandingOrderActive = false;
			cbStandingOrder.setChecked(isStandingOrderActive);
			cbStandingOrder.callOnClick();
			
			Button buttonSave = (Button) view.findViewById(R.id.pmt_view_field_bank_details_save);
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					onConfirmOrderDialog();
					getDialog().dismiss();
				}
			});

			requestOrderPeriods();
			
			return view;
		}
		
		@Override
		public void onCancel(DialogInterface dialog) {
			super.onCancel(dialog);
			onConfirmOrderDialog();
		}
		
		private void showRadioButtons(){
			if (mExecutionType == ExecutionType.BEFORE){				
				rbExecuteBefore.setChecked(true);
				rbExecuteAfter.setChecked(false);						
			}else{				
				rbExecuteBefore.setChecked(false);
				rbExecuteAfter.setChecked(true);
			}
			
			
			if (mExecuteUntil == ExecuteUntilType.FURTHER_NOTICE){
				
				rbUntilFurtherNotice.setChecked(true);
				rbUntilNumberReached.setChecked(false);
				rbUntilEndDate.setChecked(false);
				
				// hide number limit and end date
				containerEndExecutionDate.setVisibility(View.GONE);
				containerNumberOfExecutions.setVisibility(View.GONE);
				
			}else if (mExecuteUntil == ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED){				
				rbUntilFurtherNotice.setChecked(false);
				rbUntilNumberReached.setChecked(true);
				rbUntilEndDate.setChecked(false);
				
				// show number limit, hide end date
				containerEndExecutionDate.setVisibility(View.GONE);
				containerNumberOfExecutions.setVisibility(View.VISIBLE);
				
				if (mNumberOfExecutions != null && mNumberOfExecutions >0){
					edtNumberOfExecutions.setText(Integer.toString(mNumberOfExecutions));
				}
				
			}else{				
				rbUntilFurtherNotice.setChecked(false);
				rbUntilNumberReached.setChecked(false);
				rbUntilEndDate.setChecked(true);
				
				// hide number limit, show end date
				containerEndExecutionDate.setVisibility(View.VISIBLE);
				containerNumberOfExecutions.setVisibility(View.GONE);
			}
		}
		
		private void showExecutionDate(){
			// set the defaults
			if (mExecutionDate == null){
				mExecutionDate = Calendar.getInstance().getTime();
			}
			String strDate = DateFormat.getDateFormat(getContext().getApplicationContext()).format(mExecutionDate); 		
			
			tvNextExecutionDate.setText(strDate);
		}
		
		private void showEndDate(){
			// set the defaults
			if (mEndExecuteDate == null){
				mEndExecuteDate = Calendar.getInstance().getTime();
			}
			String strDate = DateFormat.getDateFormat(getContext().getApplicationContext()).format(mEndExecuteDate); 		
			
			tvEndExecutionDate.setText(strDate);
		}
		
		/**
		 * Click listener for the Standing Order checkbox
		 * @return
		 */
		public OnClickListener clickListenerForStandingOrderCheckbox(){
			OnClickListener ocl = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (((CheckBox)v).isChecked()){
						// show all the other fields
						containerFields.setVisibility(View.VISIBLE);
					}else{
						// hide all fields
						containerFields.setVisibility(View.INVISIBLE);
					}
					isStandingOrderActive = ((CheckBox)v).isChecked();
					StandingOrderField.this.show();
					StandingOrderField.this.stateChanged();
				}
			};
			return ocl;
		}
		
		/**
		 * Click listener for the Next execution date
		 * @return
		 */
		public OnClickListener clickListenerForNextExecutionDate(){
			OnClickListener ocl = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Calendar calendar = Calendar.getInstance();
    				calendar.setTime(mExecutionDate);

    				DatePickerDialog dpd = new DatePickerDialog(getContext(), new OnDateSetListener() {
						
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							Calendar cal = Calendar.getInstance();
							cal.set(Calendar.YEAR, year);
							cal.set(Calendar.MONTH, monthOfYear);
							cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							mExecutionDate = cal.getTime();
							showExecutionDate();
						}
					}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

    				dpd.show();
				}
			};
			return ocl;
		}
		
		/**
		 * Click listener for the End execution date
		 * @return
		 */
		public OnClickListener clickListenerForEndExecutionDate(){
			OnClickListener ocl = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Calendar calendar = Calendar.getInstance();
    				calendar.setTime(mEndExecuteDate);
					
					DatePickerDialog dpd = new DatePickerDialog(getContext(), new OnDateSetListener() {
						
						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							Calendar cal = Calendar.getInstance();
							cal.set(Calendar.YEAR, year);
							cal.set(Calendar.MONTH, monthOfYear);
							cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							mEndExecuteDate = cal.getTime();							
							showEndDate();
						}
					}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

    				dpd.show();
				}
			};
			return ocl;
		}
		
		/**
		 * Click listener for the "Execution" group
		 * @return
		 */
		public OnClickListener clickListenerForExecution(){
			OnClickListener ocl = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (v.getId() == R.id.pmt_view_field_radio_execute_before){
						mExecutionType = ExecutionType.BEFORE;					
					}else{
						mExecutionType = ExecutionType.AFTER;
					}
					showRadioButtons();
				}
			};
			return ocl;
		}
		
		/**
		 * Click listener for the "Limit" group
		 * @return
		 */
		public OnClickListener clickListenerForLimit(){
			OnClickListener ocl = new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (v.getId() == R.id.pmt_view_field_radio_until_further_notice){
						mExecuteUntil = ExecuteUntilType.FURTHER_NOTICE;
						
						
					}else if (v.getId() == R.id.pmt_view_field_radio_until_number_reached){
						mExecuteUntil = ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED;
						
					}else{
						mExecuteUntil = ExecuteUntilType.END_DATE_REACHED;
						
					}
					showRadioButtons();
				}
			};
			return ocl;
		}
		
		/**
		 * Makes a WS request to get the options for the spinner Recurrence
		 */
		private void requestOrderPeriods(){
			
			// Define the callback
			RequestStateEvent<StandingOrderPeriodListRequest> rse = new RequestStateEvent<StandingOrderPeriodListRequest>() {
				@Override
				public void onRequestCompleted(StandingOrderPeriodListRequest request) {
					receiveStandingOrderPeriods(request);
				}

				@Override
				public void onRequestFailed(StandingOrderPeriodListRequest request) {
					
				}
			};

			StandingOrderPeriodListRequest request = PaymentOverviewService.getStandingOrderPeriods(rse);

			// Add the request to the request queue
			request.initiateServerRequest();
		}
		
		/**
		 * Callback when the Recurrenc eoptions are received from the WS
		 * @param request
		 */
		private void receiveStandingOrderPeriods(final AbstractServerRequest<StandingOrderPeriodListResult> request){	

			StandingOrderPeriodListResult sopList = request.getResponse().getData();
			if (sopList != null) {

				List<StandingOrderPeriodTO> standingOrderPeriods = sopList.getStandingOrderPeriodList();
				mStandingOrderPeriods = new ArrayList<StandingOrderPeriodTO>();
				mStandingOrderPeriods.clear();
				mStandingOrderPeriods.addAll(standingOrderPeriods);
				setSpinnerValues();
			}
		}
		
		/**
		 * Populates the Recurrence spinner with values
		 */
		private void setSpinnerValues(){
			List<String> list = new ArrayList<String>();
			
			int selectedIndex = -1;
			int i=0;
			for(StandingOrderPeriodTO period: mStandingOrderPeriods){
				// set the selection
				if (mStandingOrderPeriod != null){
					if (period.getId().longValue() == mStandingOrderPeriod.getId().longValue()){
						selectedIndex = i;
					}
				}
				list.add(period.getName());
				i++;
			}
			
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, list);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerRecurrence.setAdapter(dataAdapter);
			spinnerRecurrence.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
					mStandingOrderPeriod = mStandingOrderPeriods.get(position);
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {}
			});
			
			if (selectedIndex >= 0){
				spinnerRecurrence.setSelection(selectedIndex);
			}
		}
	}

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}

	public Boolean getIsStandingOrderActive() {
		if (isStandingOrderActive == null) isStandingOrderActive = false;
		return isStandingOrderActive;
	}

	public void setIsStandingOrderActive(Boolean isStandingOrderActive) {
		this.isStandingOrderActive = isStandingOrderActive;
		show();
	}

	public StandingOrderPeriodTO getStandingOrderPeriod() {
		return mStandingOrderPeriod;
	}

	public void setStandingOrderPeriod(StandingOrderPeriodTO standingOrderPeriod) {
		mStandingOrderPeriod = standingOrderPeriod;
	}

	public Date getExecutionDate() {
		return mExecutionDate;
	}

	public void setExecutionDate(Date executionDate) {
		mExecutionDate = executionDate;
	}

	public ExecutionType getExecutionType() {
		return mExecutionType;
	}

	public void setExecutionType(ExecutionType executionType) {
		mExecutionType = executionType;
	}

	public ExecuteUntilType getExecuteUntil() {
		return mExecuteUntil;
	}

	public void setExecuteUntil(ExecuteUntilType executeUntil) {
		mExecuteUntil = executeUntil;
	}

	public Integer getNumberOfExecutions() {
		return mNumberOfExecutions;
	}

	public void setNumberOfExecutions(Integer numberOfExecutions) {
		mNumberOfExecutions = numberOfExecutions;
	}

	public Date getEndExecuteDate() {
		return mEndExecuteDate;
	}

	public void setEndExecuteDate(Date endExecuteDate) {
		mEndExecuteDate = endExecuteDate;
	}
}