package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.avaloq.framework.R;

public class TextCheckboxField extends PaymentField {

    private CheckBox checkboxSalaryPayment;
    private TextView checkBoxReadonly;
    private TextView mTvText;
    private String mText = "";
    private View mLayout;

    public TextCheckboxField(Context context) {
        super(context);
        init(context);
    }

    public TextCheckboxField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public TextCheckboxField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    protected void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_text_checkbox, this, true);
        checkboxSalaryPayment = (CheckBox)view.findViewById(R.id.pmt_view_checkbox);
        checkBoxReadonly = (TextView)view.findViewById(R.id.pmt_view_checkbox_readonly);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_text_checkbox_error);
        mTvText = (TextView)view.findViewById(R.id.pmt_view_text_field_label);
        mLayout = view.findViewById(R.id.pmt_view_llCheckbox);
    }

    protected void show() {
    	mTvText.setText(mText);
        mTextError.setText(mErrorText);
        mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
        
        if (getContext() != null){
        	checkBoxReadonly.setText(checkboxSalaryPayment.isChecked() ? getContext().getString(R.string.pmt_view_field_yes) : getContext().getString(R.string.pmt_view_field_no));
        }
        
        mLayout.setVisibility(isReadOnly() ? View.GONE : View.VISIBLE);
        checkboxSalaryPayment.setVisibility(isReadOnly() ? View.GONE : View.VISIBLE);        
        checkBoxReadonly.setVisibility(isReadOnly() ? View.VISIBLE : View.GONE);
    }

    public void setText(String text){
    	mText = text;
    	show();
    }
    
    public boolean isChecked() {
        return checkboxSalaryPayment.isChecked();
    }

    public void setChecked(boolean checked) {
        checkboxSalaryPayment.setChecked(checked);
        show();
    }

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}

}
