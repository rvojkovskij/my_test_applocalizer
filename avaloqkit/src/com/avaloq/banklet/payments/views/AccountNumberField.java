package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.avaloq.framework.R;

/**
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class AccountNumberField extends PaymentField {

    private EditText textAccountNumber = null;

    private String mAccountNumber = "";    

    public AccountNumberField(Context context) {
        super(context);
        init(context);
    }

    public AccountNumberField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public AccountNumberField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_account_number, this, true);
        textAccountNumber = (EditText)view.findViewById(R.id.pmt_view_field_account_number_value);
        mTextError = (TextView)view.findViewById(R.id.pmt_view_field_account_number_error);
    }

    public void show() {
        textAccountNumber.setText(mAccountNumber);
        mTextError.setText(mErrorText);
        mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
        
        textAccountNumber.setEnabled(!isReadOnly());
    }

    public String getAccountNumber() {
        return mAccountNumber;
    }

    public void setAccountNumber(String referenceNumber) {
        mAccountNumber = referenceNumber;
        show();
    }

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}

}
