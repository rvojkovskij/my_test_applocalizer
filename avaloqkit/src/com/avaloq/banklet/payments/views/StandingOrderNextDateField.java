package com.avaloq.banklet.payments.views;

import java.util.Date;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

public class StandingOrderNextDateField extends DateField{
	public StandingOrderNextDateField(Context context) {
		super(context);
	}
	
	public StandingOrderNextDateField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderNextDateField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getContext().getResources().getString(R.string.pmt_view_field_next_execution_date);
	}
	
	@Override
	protected String formatDate() {
		if (getSelectedDate() == null)
			return getResources().getString(R.string.pmt_not_set);
		else
			return super.formatDate();
	}
	
	@Override
	protected Date getDefaultDate() {
		return null;
	}
}