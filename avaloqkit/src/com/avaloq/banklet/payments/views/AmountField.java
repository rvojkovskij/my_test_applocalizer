package com.avaloq.banklet.payments.views;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CurrencyTO;
import com.avaloq.framework.R;
import com.avaloq.framework.util.CurrencyFormatInputFilter;
import com.avaloq.framework.util.CurrencyUtil;

public class AmountField extends PaymentField {

    private static final String TAG = AmountField.class.getSimpleName();

	private TextView textAmount = null;
    private Spinner spinnerCurrency = null;
    
    private TextView textAmmountReadOnly = null;
    private TextView textCurrencyReadonly = null;

    private BigDecimal mAmount;

    private Long mCurrencyId;

    private List<CurrencyTO> mCurrencies;

    private TextView mTextErrorCurrency;
    private String mCurrencyError = "";
    
    private Runnable onChangeCallback;
    
	public AmountField(Context context) {
		super(context);
		init(context);
	}

	public AmountField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public AmountField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_amount, this, true);
        textAmount = (EditText)findViewById(R.id.pmt_view_field_amount_value);
        
        textAmount.setHint("0" + DecimalFormatSymbols.getInstance().getDecimalSeparator() + "00");
        textAmount.setFilters(new InputFilter[] { new CurrencyFormatInputFilter() });

        textAmount.addTextChangedListener(new TextWatcher() {

        	String b;
        	
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				b = textAmount.getText().toString();
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				if (s.toString().compareTo(b) != 0){					
					stateChanged();
				}
			}
		});
        
        spinnerCurrency = (Spinner)findViewById(R.id.pmt_view_field_currency_value);

        textAmmountReadOnly = (TextView)findViewById(R.id.pmt_view_field_amount_value_readonly);
        textCurrencyReadonly = (TextView) findViewById(R.id.pmt_view_field_currency_value_readonly);
        mTextError = (TextView)findViewById(R.id.pmt_view_field_amount_error);
        
        mTextErrorCurrency = (TextView)findViewById(R.id.pmt_view_field_amount_currency_error);
        
		showAmount();
	}
	
	public void setOnChangeCallback(Runnable callback){
		onChangeCallback = callback;
	}
	
	private void showAmount() {
		if(mAmount == null) {
			textAmount.setText("");
		} else {
            textAmount.setText(CurrencyUtil.formatInputNumber(mAmount));
		}

		mTextErrorCurrency.setVisibility(TextUtils.isEmpty(mCurrencyError) ? View.GONE : View.VISIBLE);
		mTextErrorCurrency.setText(mCurrencyError);
		
		textAmount.setVisibility(isReadOnly() ? View.GONE : View.VISIBLE);
		spinnerCurrency.setVisibility(isReadOnly() ? View.GONE : View.VISIBLE);
		textAmmountReadOnly.setVisibility(isReadOnly() ? View.VISIBLE : View.GONE);
        textCurrencyReadonly.setVisibility(isReadOnly() ? View.VISIBLE : View.GONE);
		
		setCurrencySpinnerValues();
		
		textAmmountReadOnly.setText(textAmount.getText());
		textCurrencyReadonly.setText((String)spinnerCurrency.getSelectedItem());
		
		// Enable/disable fields		
		textAmount.setEnabled(!isReadOnly());
		spinnerCurrency.setEnabled(!isReadOnly());				
	}

	/**
	 * Sets the values for the currency spinner and selects the currency 
	 * specified with setCurrencyId()
	 */
	private void setCurrencySpinnerValues(){
		
		if (mCurrencies == null || mCurrencies.size() == 0) return;
		
		// prepare an array for the adapter
		String[] mCurrenciesString = new String[mCurrencies.size()];
		int index=0;
		for(CurrencyTO currency: mCurrencies){
			mCurrenciesString[index++] = currency.getIsoCode();
		}
		
		
		if (mCurrenciesString != null && mCurrenciesString.length > 0){			
			spinnerCurrency.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, mCurrenciesString));
			
			// Set the selected currency. If nothing was selected until now, or the current currency code is not found,
			// select the first element.
			if (getCurrencyId() == null){
				mCurrencyId = mCurrencies.get(0).getId();
				spinnerCurrency.setSelection(0);
			}else{
				// We have already a currency ID that has to be selected
				
				// First step - find the position of this currency
				int currencyPosition = -1;
				for (int i=0;i<mCurrencies.size();i++){
					if (getCurrencyId().longValue() == mCurrencies.get(i).getId().longValue()){
						currencyPosition = i;
						break;
					}
				}
				
				// If we did not find the currency in the list, it means that it is missing, so 
				// just select the first one in the list and update the currency id
				if (currencyPosition == -1){
					mCurrencyId = mCurrencies.get(0).getId();
					spinnerCurrency.setSelection(0);
				}else{
					// The currency was found in the list and needs to be selected
					spinnerCurrency.setSelection(currencyPosition);
				}
			}
			
			// Because there are values in the spinner, a OnItemSelectedListener is needed
			spinnerCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

				@Override
				public void onItemSelected (AdapterView<?> parent, View view, int position, long id) {
					mCurrencyId = mCurrencies.get(position).getId();
					if (onChangeCallback != null)
						onChangeCallback.run();
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// not needed
				}
			});
			
		}else{
			spinnerCurrency.setAdapter(new ArrayAdapter<CurrencyTO>(getContext(), android.R.layout.simple_spinner_dropdown_item));
			spinnerCurrency.setOnItemSelectedListener(null);			
		}
	}
	
    public BigDecimal getAmount() {
    	try {
			mAmount = CurrencyFormatInputFilter.parseNumber(textAmount.getText().toString());
		} catch (Exception e) {
			e.printStackTrace();
			mAmount = BigDecimal.ZERO;
		}
    	
    	if (mAmount == null) return BigDecimal.ZERO;
    	
        return mAmount;
    }

    public void setAmount(BigDecimal amount) {
        mAmount = amount;
        showAmount();
    }

    public Long getCurrencyId() {
        return mCurrencyId;
    }

    public void setCurrencyId(Long currencyId) {
        mCurrencyId = currencyId;
        showAmount();
    }

    public List<CurrencyTO> getCurrencies() {
		return mCurrencies;
	}
    
	public void setCurrencies(List<CurrencyTO> currencies) {		
		mCurrencies = currencies;
		setCurrencySpinnerValues();
	}
	
	public void setErrorCurrency(String text){
		mCurrencyError = text;
		showAmount();
	}
	
	@Override
	public void errorTextSet() {
		mTextError.setText(getErrorText());
        mTextError.setVisibility(TextUtils.isEmpty(getErrorText()) ? View.GONE : View.VISIBLE);
	}

	@Override
	public void readOnlyStateSet() {
		showAmount();
	}
	
	public String getSelectedCurrencyISO(){
		for (CurrencyTO cur: mCurrencies){
			if (cur.getId().equals(mCurrencyId))
				return cur.getIsoCode();
		}
		
		return "";
	}
}