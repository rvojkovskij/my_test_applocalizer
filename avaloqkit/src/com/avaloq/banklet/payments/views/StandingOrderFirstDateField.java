package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

public class StandingOrderFirstDateField extends DateField{
	public StandingOrderFirstDateField(Context context) {
		super(context);
	}
	
	public StandingOrderFirstDateField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderFirstDateField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getContext().getResources().getString(R.string.pmt_view_field_first_execution_date);
	}
}
