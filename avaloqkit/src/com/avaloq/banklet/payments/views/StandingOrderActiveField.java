package com.avaloq.banklet.payments.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

public class StandingOrderActiveField extends TextCheckboxField{
	public StandingOrderActiveField(Context context) {
        super(context);
    }

    public StandingOrderActiveField(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public StandingOrderActiveField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    protected void init(Context context) {
    	super.init(context);
    	setText(getContext().getString(R.string.pmt_view_field_standing_order_status));
    }
}
