package com.avaloq.banklet.payments.views;

import com.avaloq.framework.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

/**
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class ReferenceNumberField extends TextField {

    public ReferenceNumberField(Context context) {
        super(context);
    }

    public ReferenceNumberField(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ReferenceNumberField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    protected boolean isNumber() {
    	return true;
    }
    
    @Override
    public String getLabel() {
    	return getContext().getString(R.string.pmt_view_field_reference_number);
    }
    
    private void formatReferenceNumber(){
    	// Format the string in blocks of five characters.
    	String tString = "";
    	fieldValue = fieldValue.replace(" ", "");
    	
    	for (int i = fieldValue.length()-1; i>=0; i--){
    		tString += fieldValue.charAt(i);

    		if ((fieldValue.length()-i) % 5 == 0){
    			tString += " ";
    		}
    	}
    	fieldValue = new StringBuilder(tString).reverse().toString();
    }
    
    @Override
	public void show() {   
    	textValue.setHint(R.string.pmt_view_field_reference_number_hint); 
		formatReferenceNumber();
        super.show();
    }

    public String getReferenceNumber() {
    	return getValue();
    }

    public void setReferenceNumber(String referenceNumber) {
        setValue(referenceNumber);
    }
}
