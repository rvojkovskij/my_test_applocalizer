package com.avaloq.banklet.payments.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.R;

public class SaveAsTemplateField extends PaymentField {

	private Boolean isSaveAsTemplate;
	private Boolean isFavorite;


	private String mTemplateAlias; 

	private TextView textValue = null;

	public SaveAsTemplateField(Context context) {
		super(context);
		init(context);
	}

	public SaveAsTemplateField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SaveAsTemplateField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}


	private void init(Context context) {
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_saveastemplate, this, true);
		textValue = (TextView) view.findViewById(R.id.pmt_view_field_standing_order_value);
		mTextError = (TextView) view.findViewById(R.id.pmt_view_field_standing_order_error);
		setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()) {
					SherlockFragmentActivity a = (SherlockFragmentActivity) getContext();
					FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
					new SaveAsTemplateDialogFragment().show(ft, null);
				}
			}
		});
		show();
	}

	/**
	 * Displays the form field, not the dialog.
	 */
	private void show() {

		if (isSaveAsTemplate == null || isSaveAsTemplate == false){
			textValue.setText("No");
		}else{
			textValue.setText("Yes");
		}

		mTextError.setText(mErrorText);
		mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
	}

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}

	public Boolean getIsStandingOrderActive() {
		return isSaveAsTemplate;
	}

	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class SaveAsTemplateDialogFragment extends DialogFragment {

		CheckBox cbSaveAsTemplate;		
		CheckBox cbFavoriteTemplate;
		EditText edtAlias;

		View containerFields;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_save_as_template);
			View view = inflater.inflate(R.layout.pmt_view_field_saveastemplate_dialog, container, true);

			cbSaveAsTemplate = (CheckBox) view.findViewById(R.id.pmt_view_field_checkbox_save_as_template);
			cbFavoriteTemplate = (CheckBox) view.findViewById(R.id.pmt_view_field_checkbox_is_favorite);

			edtAlias = (EditText) view.findViewById(R.id.pmt_view_field_edittext_alias);				
			edtAlias.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					try{
						mTemplateAlias = s.toString();
					}
					catch(Exception e){
						e.printStackTrace();
					}
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {

				}

				@Override
				public void afterTextChanged(Editable s) {

				}
			});

			containerFields = view.findViewById(R.id.pmt_view_field_llFields);


			// display the defaults
			edtAlias.setText((mTemplateAlias != null) ? mTemplateAlias : "");

			// Attach click listeners
			cbSaveAsTemplate.setOnClickListener(clickListenerForSaveAsTemplateCheckbox());

			if (isSaveAsTemplate == null) isSaveAsTemplate = false;
			cbSaveAsTemplate.setChecked(isSaveAsTemplate);
			cbSaveAsTemplate.callOnClick();

			if (isFavorite == null) isFavorite = false;
			cbFavoriteTemplate.setChecked(isFavorite);

			cbFavoriteTemplate.setOnClickListener(clickListenerForSetAsFavoriteCheckbox());

			Button buttonSave = (Button) view.findViewById(R.id.pmt_view_field_bank_details_save);
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					getDialog().dismiss();
				}
			});


			return view;
		}		



		/**
		 * Click listener for the Save as Template checkbox
		 * @return
		 */
		public OnClickListener clickListenerForSaveAsTemplateCheckbox(){
			OnClickListener ocl = new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (((CheckBox)v).isChecked()){
						// show all the other fields
						containerFields.setVisibility(View.VISIBLE);
					}else{
						// hide all fields
						containerFields.setVisibility(View.INVISIBLE);
					}
					isSaveAsTemplate = ((CheckBox)v).isChecked();
					SaveAsTemplateField.this.show();
				}
			};
			return ocl;
		}

		/**
		 * Click listener for the set as favorite checkbox
		 * @return
		 */
		public OnClickListener clickListenerForSetAsFavoriteCheckbox(){
			OnClickListener ocl = new OnClickListener() {					
				@Override
				public void onClick(View v) {						
					isFavorite = ((CheckBox)v).isChecked();						
				}
			};
			return ocl;
		}
	}

	public Boolean getIsSaveAsTemplate() {
		return isSaveAsTemplate;
	}

	public void setIsSaveAsTemplate(Boolean isSaveAsTemplate) {
		this.isSaveAsTemplate = isSaveAsTemplate;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public String getTemplateAlias() {
		return mTemplateAlias;
	}

	public void setTemplateAlias(String templateAlias) {
		mTemplateAlias = templateAlias;
	}
}