package com.avaloq.banklet.payments.views;

import java.util.LinkedHashMap;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.framework.R;

import com.avaloq.banklet.payments.views.StandingOrderField.ExecutionType;

public class StandingOrderExecutionTypeField extends SingleChoiceField<ExecutionType>{
	
	public StandingOrderExecutionTypeField(Context context) {
		super(context);
	}
	
	public StandingOrderExecutionTypeField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderExecutionTypeField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getResources().getString(R.string.pmt_view_field_weekend_rule);
	}

	@Override
	public LinkedHashMap<ExecutionType, String> getElements() {
		LinkedHashMap<ExecutionType, String> map = new LinkedHashMap<ExecutionType, String>();
		map.put(ExecutionType.AFTER, getResources().getString(R.string.pmt_view_field_execute_after));
		map.put(ExecutionType.BEFORE, getResources().getString(R.string.pmt_view_field_execute_before));
		return map;
	}

	@Override
	public ExecutionType getDefaultValue() {
		return ExecutionType.AFTER;
	}
	
	
}
