package com.avaloq.banklet.payments.views;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.framework.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public abstract class SingleChoiceField<Type> extends PaymentField{
	protected Type mSelectedValue;
	protected TextView mTextLabel = null;
	protected TextView mTextValue = null;
	
	protected Runnable onItemChangedCallback;
	
	protected final String TAG_PREFIX = "choice";
	
	public SingleChoiceField(Context context) {
		super(context);
		init(context);
	}
	
	public SingleChoiceField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public SingleChoiceField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	
	public void init(Context context){
		mSelectedValue = getDefaultValue();
		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_execution_date, this, true);
		mTextValue = (TextView)view.findViewById(R.id.pmt_view_field_date);
		mTextError = (TextView)view.findViewById(R.id.pmt_view_field_date_error);
		mTextLabel = (TextView)view.findViewById(R.id.pmt_view_field_amount_label);
		setLabel(getLabel());
		
		setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
            	if (!isReadOnly()){
            		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
	                FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
            		new Dialog().show(ft, null);
            	}
            }
		});
		
		show();
	}
	
	protected void show(){		
		if (mSelectedValue != null)
			mTextValue.setText(getElements().get(mSelectedValue));
		
		mTextError.setText(getErrorText());
        mTextError.setVisibility(TextUtils.isEmpty(getErrorText()) ? View.GONE : View.VISIBLE);
	}
	
	public Type getSelectedValue(){
		return mSelectedValue;
	}

	public void setSelectedValue(Type value) {
		mSelectedValue = value;
		show();
		onItemChanged();
	}

	@Override
	public void errorTextSet() {
		show();
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}
	
	public void setLabel(String string){
		mTextLabel.setText(string);
    }
	
	protected void onItemChanged(){
		if (onItemChangedCallback != null)
			onItemChangedCallback.run();
	}
	
	public void setOnItemChangedCallback(Runnable callback){
		onItemChangedCallback = callback;
	}
	
	public abstract String getLabel();
	public abstract LinkedHashMap<Type, String> getElements();
	public abstract Type getDefaultValue();
	
	
	
	
	protected class Dialog extends SherlockDialogFragment{
		LinearLayout container;
		Type element;
		
		@Override
        public View onCreateView(LayoutInflater inflater, ViewGroup aContainer, Bundle savedInstanceState) {
			getDialog().setTitle(getLabel());
			element = mSelectedValue;
			View view = inflater.inflate(R.layout.avq_criterium_single_choice, null, false);
			container = (LinearLayout)view.findViewById(R.id.inputFields);
			
			Iterator<Entry<Type, String>> iterator = getElements().entrySet().iterator();
			while (iterator.hasNext()){
				Entry<Type, String> element = iterator.next();
				View item = inflater.inflate(R.layout.avq_single_choice_criterium_item, null, false);
				final Type key = element.getKey();
				final String value = element.getValue();
				item.setTag(TAG_PREFIX + key);
				((TextView)item.findViewById(R.id.booking_type_value)).setText(value);
				if (isSelected(key))
					item.findViewById(R.id.booking_type_selected).setVisibility(View.VISIBLE);
				
				item.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						itemSelected(v, key);
					}
				});
				
				container.addView(item);
				((LinearLayout.LayoutParams)item.getLayoutParams()).bottomMargin = (int)getResources().getDimension(R.dimen.list_padding);
			}
			
			Button button = (Button)view.findViewById(R.id.button_criterium_ok);
			button.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					setSelectedValue(element);
					dismiss();
				}
			});
			
			return view;
		}
		
		protected boolean isSelected(View view){
			if (view.getTag().equals(TAG_PREFIX + element))
				return true;
			else
				return false;
		}
		
		protected boolean isSelected(Type key){
			if (element != null && key.equals(element))
				return true;
			else
				return false;
		}
		
		protected void itemSelected(View view, Type key){
			view.findViewById(R.id.booking_type_selected).setVisibility(View.VISIBLE);
			element = key;
			
			for (int i=0; i<container.getChildCount(); i++){
				View child = container.getChildAt(i);
				if (!isSelected(child))
					child.findViewById(R.id.booking_type_selected).setVisibility(View.INVISIBLE);
			}
		}
	}
	
}
