package com.avaloq.banklet.payments.views;

import java.util.LinkedHashMap;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import com.avaloq.banklet.payments.views.StandingOrderField.ExecuteUntilType;
import com.avaloq.framework.R;

public class StandingOrderValidityField extends SingleChoiceField<ExecuteUntilType>{
	
	public StandingOrderValidityField(Context context) {
		super(context);
	}
	
	public StandingOrderValidityField(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public StandingOrderValidityField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public String getLabel() {
		return getResources().getString(R.string.pmt_view_field_validity);
	}

	@Override
	public LinkedHashMap<ExecuteUntilType, String> getElements() {
		LinkedHashMap<ExecuteUntilType, String> map = new LinkedHashMap<ExecuteUntilType, String>();
		map.put(ExecuteUntilType.FURTHER_NOTICE, getResources().getString(R.string.pmt_view_field_until_further_notice));
		map.put(ExecuteUntilType.END_DATE_REACHED, getResources().getString(R.string.pmt_view_field_until_end_date_reached));
		map.put(ExecuteUntilType.NUMBER_OF_EXECUTIONS_REACHED, getResources().getString(R.string.pmt_view_field_until_number_of_executions_reached));
		return map;
	}

	@Override
	public ExecuteUntilType getDefaultValue() {
		return ExecuteUntilType.FURTHER_NOTICE;
	}
}
