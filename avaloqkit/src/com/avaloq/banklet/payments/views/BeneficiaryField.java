package com.avaloq.banklet.payments.views;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.afs.aggregation.to.LocalizedValidationResult;
import com.avaloq.afs.aggregation.to.payment.BankInfoListResult;
import com.avaloq.afs.aggregation.to.payment.BankInfoResult;
import com.avaloq.afs.aggregation.to.payment.IbanResult;
import com.avaloq.afs.aggregation.to.payment.SwiftBankInfoListResult;
import com.avaloq.afs.aggregation.to.payment.SwiftBankInfoResult;
import com.avaloq.afs.aggregation.to.payment.SwissPostCheckInfoResult;
import com.avaloq.afs.server.bsp.client.ws.BankInfoQueryTO;
import com.avaloq.afs.server.bsp.client.ws.BankInfoTO;
import com.avaloq.afs.server.bsp.client.ws.BeneficiaryTO;
import com.avaloq.afs.server.bsp.client.ws.CountryTO;
import com.avaloq.afs.server.bsp.client.ws.NotificationTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentSubType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.PostCheckInfoOutputTO;
import com.avaloq.afs.server.bsp.client.ws.SwiftBankInfoQueryTO;
import com.avaloq.afs.server.bsp.client.ws.SwiftBankInfoTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.domesticpayment.BankInfoListRequest;
import com.avaloq.framework.comms.webservice.domesticpayment.BankInfoRequest;
import com.avaloq.framework.comms.webservice.domesticpayment.DomesticPaymentService;
import com.avaloq.framework.comms.webservice.internationalpayment.InternationalPaymentService;
import com.avaloq.framework.comms.webservice.internationalpayment.SwiftBankInfoListRequest;
import com.avaloq.framework.comms.webservice.internationalpayment.SwiftBankInfoRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.IbanRequest;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissOrangePaymentService;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissPostCheckInfoRequest;
import com.avaloq.framework.comms.webservice.swissredpayment.SwissRedPaymentService;
import com.avaloq.framework.ui.DialogFragment;

public class BeneficiaryField extends PaymentField {

	private final String DIALOG_POST_ACCOUNT = "postaccountdialog";
	private final String DIALOG_ACCOUNT = "accountdialog";
	private final String DIALOG_BANK_DETAILS = "bankdetails";
	private final String DIALOG_BANK_DETAILS_EX = "bankdetailex";	
	private final String DIALOG_BENEFICIARY = "beneficiary";
	private final String DIALOG_BENEFICIARY_IBAN = "beneficiary_iban";
	
	private AbstractPaymentActivity<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> mDelegate;
	
	/*
	 * The containers for the fields. Will be used
	 * to show or hide the fields depending on the
	 * values that are set
	 */
	private View containerTo = null;
	private View containerBeneficiary = null;
	private View containerBankingDetails = null;
	private View containerAccount = null;
	private View containerPostAccount = null;
	private View containerIBAN = null;

	/*
	 * The views that will display the values for 
	 * the specific fields 
	 */
	private TextView textValueBeneficiary = null;
	private TextView textValueBankingDetails = null;
	private TextView textValuePostAccount = null;
	private TextView textValueAccount = null;
	private TextView textValueIban = null;

	/*
	 * Holders for the field values
	 */
	private String[] mBeneficiary = null;
	private String[] mBankDetails = null;
	private String mBankBIC = null;
	private String mBankNtnl = null;
	private String mAccountPost = "";
	private String mAccountIBAN = "";
	private String mAccount = "";		

	private PaymentSubType mPaymentSubType;
	
	public enum PcAccountValidationType{
		ORANGE, RED, DOMESTIC, INTERNATIONAL
	}
	
	/*
	 * Type of payment
	 */
	private BeneficiaryFieldPaymentType mPaymentType = BeneficiaryFieldPaymentType.ORANGE;
	
	private List<CountryTO> mCountries;
	
	private CountryTO mCountry;
	private Long mCountryId;

	public enum BeneficiaryFieldPaymentType{
		ORANGE, RED, TRANSFER, DOMESTIC, INTERNATIONAL
	}

	private boolean mIsSet = false;
	
	/**
	 * 
	 * @param context
	 */
	public BeneficiaryField(Context context) {
		super(context);
		init(context);
	}

	/**
	 * 
	 * @param context
	 * @param attrs
	 */
	public BeneficiaryField(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	/**
	 * 
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public BeneficiaryField(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	/**
	 * Called when the field is initialized
	 * @param context
	 */
	private void init(Context context) {

		View view = LayoutInflater.from(context).inflate(R.layout.pmt_view_field_beneficiary, this, true);
		
		clearValues();
		
		// Get all the fields and containers
		textValueBeneficiary = (TextView)view.findViewById(R.id.pmt_view_field_beneficiary_value);
		textValueBankingDetails = (TextView)view.findViewById(R.id.pmt_view_field_bank_details_value);
		textValueAccount = (TextView)view.findViewById(R.id.pmt_view_field_account_number_value);
		textValuePostAccount = (TextView) view.findViewById(R.id.pmt_view_field_postaccount_number_value);
		textValueIban = (TextView) view.findViewById(R.id.pmt_view_field_iban_value);	
		
		mTextError = (TextView)view.findViewById(R.id.pmt_view_field_beneficiary_error);
		containerTo = view.findViewById(R.id.pmt_view_paymentto);
		containerBeneficiary = view.findViewById(R.id.pmt_view_beneficiary);
		containerBankingDetails = view.findViewById(R.id.pmt_view_bankdetails);
		containerAccount = view.findViewById(R.id.pmt_view_accountnumber);
		containerPostAccount = view.findViewById(R.id.pmt_view_postaccountnumber);
		containerIBAN = view.findViewById(R.id.pmt_view_iban);
		
		// Each of the field has its own click listener
		attachClickListenersToFields();

		// Display the field
		show();
	}

	public void setDelegate(AbstractPaymentActivity<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> delegate){
		this.mDelegate = delegate;
	}
	
	/**
	 * Attached the click listeners to the Account, Beneficiary and
	 * Banking detail fields.
	 * Each field has to have a different dialog opened when clicked.
	 */
	private void attachClickListenersToFields(){
		
		findViewById(R.id.pmt_view_paymentto).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){
					if(mPaymentType == BeneficiaryFieldPaymentType.ORANGE || mPaymentType == BeneficiaryFieldPaymentType.RED) {
						showPostAccountSelectDialog(true);
					}else if(mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC || mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
						showAccountDialog(true);
					}					
				}
			}
		});
		
		findViewById(R.id.pmt_view_beneficiary).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){					
					showBeneficiaryDialog(false);					
				}
			}
		});
		findViewById(R.id.pmt_view_accountnumber).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){					
					showAccountDialog(false);
				}
			}
		});
		findViewById(R.id.pmt_view_postaccountnumber).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){					
					showPostAccountSelectDialog(true);					
				}
			}
		});
		findViewById(R.id.pmt_view_iban).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){
					if (mPaymentType == BeneficiaryFieldPaymentType.RED){
						showBeneficiaryIBANDialog(false);
					}else{
						showAccountDialog(false);
					}
				}
			}
		});
		findViewById(R.id.pmt_view_bankdetails).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (!isReadOnly()){					
					if(mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC || mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
						showBankDetailsExDialog(false);
					}else{
						// for Orange and Red payments, this field is not editable if the sub payment type is BANK_ACCOUNT
						if ((mPaymentType == BeneficiaryFieldPaymentType.ORANGE || mPaymentType == BeneficiaryFieldPaymentType.RED) 
								&& mPaymentSubType == PaymentSubType.BANK_ACCOUNT){
							return;
						}
						showBankDetailsDialog(false); 
					}		
				}
			}
		});
	}

	/**
	 * Display the Dialog for entering the beneficiary information
	 */
	private void showBeneficiaryDialog(boolean isWizard){
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();	
		Fragment prevPA;
		Fragment prevBD;
		Fragment prevBDE;
		Fragment prevIban;
		prevPA = a.getSupportFragmentManager().findFragmentByTag(DIALOG_POST_ACCOUNT);		
		prevBD = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BANK_DETAILS);		
		prevIban = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BENEFICIARY_IBAN);
		
		if (prevPA != null && prevBD != null && prevIban != null){
			ft.remove(prevIban);
		}else if (prevPA != null && prevBD != null){
			ft.remove(prevBD);
		}else{
			//if (prevPA != null){ft.remove(prevPA);}
			if (prevBD != null){ft.remove(prevBD);}
		}		
				
		prevBDE = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BANK_DETAILS_EX);if (prevBDE != null) {ft.remove(prevBDE);}	    
		ft.addToBackStack(null);
		BeneficiaryDetailsDialogFragment bddf = new BeneficiaryDetailsDialogFragment();
		bddf.setIsWizard(isWizard);
		bddf.show(ft, DIALOG_BENEFICIARY);
	}
	
	/**
	 * Display the Dialog for entering the beneficiary information + IBAN
	 */
	private void showBeneficiaryIBANDialog(boolean isWizard){
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();	
		Fragment prevPA;
		Fragment prevBD;
		
		prevPA = a.getSupportFragmentManager().findFragmentByTag(DIALOG_POST_ACCOUNT);		
		prevBD = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BANK_DETAILS);
		
		if (prevPA != null && prevBD != null){
			ft.remove(prevBD);
		}else{
			if (prevPA != null){ft.remove(prevPA);}
			if (prevBD != null){ft.remove(prevBD);}
		}
		ft.addToBackStack(null);
		IbanDialogFragment bdidf = new IbanDialogFragment();
		bdidf.setIsWizard(isWizard);
		bdidf.show(ft, DIALOG_BENEFICIARY_IBAN);
	}	

	/**
	 * Display the Dialog for entering the account. Depending
	 * on the type of mSelectAccountType, the dialog will allow
	 * to input the Post account or both Post and IBAN
	 */
	private void showPostAccountSelectDialog(boolean isWizard){
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
		ft.addToBackStack(null);
		PostAccountDialogFragment padf = new PostAccountDialogFragment();
		padf.setIsWizard(isWizard);
		padf.show(ft, DIALOG_POST_ACCOUNT);
	}

	/**
	 * Display the Dialog for entering the bank details
	 */
	private void showBankDetailsDialog(boolean isWizard){
		
		// For red and orange payments, if the beneficiary data is present, go to 
		if (mPaymentType == BeneficiaryFieldPaymentType.ORANGE && isWizard){
			if (!isArrayEmpty(mBankDetails)){
				showBeneficiaryDialog(isWizard);
				return;
			}
		}
//		if (mPaymentType == BeneficiaryFieldPaymentType.RED){
//			if (!isArrayEmpty(mBankDetails)){
//				showBeneficiaryIBANDialog(isWizard);
//				return;
//			}
//		}

		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
		Fragment prev;
		prev = a.getSupportFragmentManager().findFragmentByTag(DIALOG_POST_ACCOUNT);if (prev != null) {ft.remove(prev);}
		ft.addToBackStack(null);
		BankDetailsDialogFragment bddf = new BankDetailsDialogFragment();	
		bddf.setIsWizard(isWizard);
		bddf.show(ft, DIALOG_BANK_DETAILS);
	}

	/**
	 * Display the Dialog for entering the bank details
	 */
	private void showBankDetailsExDialog(boolean isWizard){
		if (!isArrayEmpty(mBankDetails)) {
			// data is available, show next step
			showBankDetails();
			
			// check if needs to show the beneficiary dialog
			if(isArrayEmpty(mBeneficiary) && isWizard){
				showBeneficiaryDialog(isWizard);
			}
		}
		
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
		Fragment prev = a.getSupportFragmentManager().findFragmentByTag(DIALOG_ACCOUNT);
	    if (prev != null) {
	        ft.remove(prev);
	    }
		ft.addToBackStack(null);
		BankDetailsExDialogFragment bddf = new BankDetailsExDialogFragment();		
		bddf.setIsWizard(isWizard);
		bddf.show(ft, DIALOG_BANK_DETAILS_EX);
	}
	
	/**
	 * Display the Dialog for entering the account details
	 */
	private void showAccountDialog(boolean isWizard){
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();
		ft.addToBackStack(null);
		AccountDialogFragment adf = new AccountDialogFragment();	
		adf.setIsWizard(isWizard);
		adf.show(ft, DIALOG_ACCOUNT);
	}
	
	private void clearBackStack(){
		SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
		a.getSupportFragmentManager().beginTransaction();
		
		
		a.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		
		//ft.commit();
	}
	
	
	/**
	 * Main method to display the Beneficiary field
	 */
	public void show(){

//		showBeneficiaryDetails();
//		showBankDetails();
//		showPostAccountNumber();
//		showAccountNumber();
//		showIban();
		
		mTextError.setText(mErrorText);
		mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
	}

	private void clearValues(){
		mBeneficiary = new String[4];
		mBankDetails = new String[4];
		mAccountPost = "";
		mAccountIBAN = "";
		mAccount = "";	
	}
	
	private void hideToField(){
		stateChanged();
		mIsSet = true;
		containerTo.setVisibility(View.GONE);
	}
	
	private void showBeneficiaryDetails() {
		hideToField();
		containerBeneficiary.setVisibility(View.VISIBLE);
		textValueBeneficiary.setText(TextUtils.join("\n", filterEmptyStrings(mBeneficiary)));
	}
	
	public void hideBeneficiaryDetails() {
		hideToField();
		containerBeneficiary.setVisibility(View.GONE);		
	}

	public void showBankDetails(boolean showEvenIfEmpty) {
		hideToField();
		if (!isArrayEmpty(mBankDetails) || showEvenIfEmpty){
			containerBankingDetails.setVisibility(View.VISIBLE);
			textValueBankingDetails.setText(TextUtils.join("\n", filterEmptyStrings(mBankDetails)));
			if (mBankNtnl != null && !mBankNtnl.isEmpty()){
				textValueBankingDetails.setText(textValueBankingDetails.getText()+"\n"+getActivity().getString(R.string.pmt_view_clearing_number)+" "+mBankNtnl);
			}else{
				textValueBankingDetails.setText(TextUtils.join("\n", filterEmptyStrings(mBankDetails)));
			}
		}
	}
	
	public void showBankDetails() {
		hideToField();
		if (!isArrayEmpty(mBankDetails)){
			containerBankingDetails.setVisibility(View.VISIBLE);
			textValueBankingDetails.setText(TextUtils.join("\n", filterEmptyStrings(mBankDetails)));
			if (mBankNtnl != null && !mBankNtnl.isEmpty()){
				textValueBankingDetails.setText(textValueBankingDetails.getText()+"\n"+getActivity().getString(R.string.pmt_view_clearing_number)+" "+mBankNtnl);
			}else{
				textValueBankingDetails.setText(TextUtils.join("\n", filterEmptyStrings(mBankDetails)));
			}
		}
	}
	
	public void showPostAccountNumber(){		
		// When displaying the post account, set the correct currency
		
		if (mAccountPost != null && !mAccountPost.isEmpty()){
			hideToField();
			containerPostAccount.setVisibility(View.VISIBLE);
			textValuePostAccount.setText(mAccountPost);
			
			if (mPaymentType == BeneficiaryFieldPaymentType.ORANGE){
				if (mAccountPost.startsWith("1") || mAccountPost.startsWith("01")){
					mDelegate.setCurrency("CHF");
				}else if (mAccountPost.startsWith("3") || mAccountPost.startsWith("03")){
					mDelegate.setCurrency("EUR");
				}
			}else if (mPaymentType == BeneficiaryFieldPaymentType.RED){
				if (mAccountPost.startsWith("3") || mAccountPost.startsWith("03")){
					mDelegate.setCurrency("CHF");
				}else if (mAccountPost.startsWith("91")){
					mDelegate.setCurrency("EUR");
				}
			}
		}
	}
	
	public void showAccountNumber(){
		hideToField();
		if (mAccount != null && !mAccount.isEmpty()){
			containerAccount.setVisibility(View.VISIBLE);
			textValueAccount.setText(mAccount); 
		}else{
			textValueAccount.setText("");
			containerAccount.setVisibility(View.GONE);
		}
	}

	public void showIban(){
		hideToField();
		
		if (mAccountIBAN != null && !mAccountIBAN.isEmpty()){
			containerIBAN.setVisibility(View.VISIBLE);
			textValueIban.setText(mAccountIBAN);
		}else{
			textValueIban.setText("");
			containerIBAN.setVisibility(View.GONE);			
		}
	}
	
	/**
	 * Validates the account/iban entered on the form. Depending
	 * on the result of the validation, it should return back to the form
	 * and display the error message, or to open the next dialog if needed 
	 */
	public void validatePostAccount(Dialog dialog, boolean isWizard){
		if (mPaymentType == BeneficiaryFieldPaymentType.ORANGE){
			validateOrangePaymentAccount(dialog, isWizard);
		}else if(mPaymentType == BeneficiaryFieldPaymentType.RED){
			validateRedPaymentAccount(dialog, isWizard);
		}	
	}
	
	private void accountOrangeValidationReceived(Dialog dialog, SwissPostCheckInfoRequest request, boolean isWizard){
		SwissPostCheckInfoResult result = request.getResponse().getData();		
		
		if (result != null) {

			// If the result has a notification list, there are errors			
			if (result.getPostCheckInfo() != null){

				// get the account number from the WS				
				if (result.getPostCheckInfo().getPcAccount() != null && result.getPostCheckInfo().getPcAccount().length()>0){
					mAccountPost = result.getPostCheckInfo().getPcAccount();
				}

				if (
						result.getPostCheckInfo().getNotificationList() != null &&					
						result.getPostCheckInfo().getNotificationList().size() > 0){
					// there are errors for the account validation
					String errorString = getContext().getString(R.string.pmt_pcaccount_validation_error);

					/*for (NotificationTO notification: result.getPostCheckInfo().getNotificationList()){
						// FIXME translation
						errorString += notification.getValidationResult().getTranslationKey()+'\n';
					}*/

					DialogFragment.createAlert(getActivity().getString(R.string.pmt_view_field_error), errorString, getActivity()).show(getActivity());

				}
				else {
					
					if (result.getPostCheckInfo().getBeneficiaryList() != null && getListByPaymentType(result.getPostCheckInfo().getBeneficiaryList(), PaymentType.SWISS_ORANGE_PAYMENT_SLIP).size() > 0){
						BeneficiaryListDialogFragment bldf = new BeneficiaryListDialogFragment();
						bldf.setBeneficiaryList(getListByPaymentType(result.getPostCheckInfo().getBeneficiaryList(), PaymentType.SWISS_ORANGE_PAYMENT_SLIP));
						bldf.setPostCheckInfo(result.getPostCheckInfo());
						bldf.setAdditionalParams(dialog, isWizard, PcAccountValidationType.ORANGE);
						bldf.show();
						return;
					}else{
						processOrangeValidationResult(dialog, isWizard, result.getPostCheckInfo(), null);
					}
				}
			} 
		}
		if (dialog != null && dialog.findViewById(R.id.pmt_view_field_beneficiary_save) != null)
		((Button)dialog.findViewById(R.id.pmt_view_field_beneficiary_save)).setEnabled(true);
		
	}
	
	
	/**
	 * 
	 * @param dialog
	 * @param isWizard
	 * @param postCheckInfoOutput
	 * @param beneficiary
	 */
	private void processOrangeValidationResult(Dialog dialog, boolean isWizard, PostCheckInfoOutputTO postCheckInfoOutput, BeneficiaryTO beneficiary){
		
		if (beneficiary != null){
			mPaymentSubType = beneficiary.getPaymentSubType();
		}else{
			mPaymentSubType = postCheckInfoOutput.getPaymentSubType();
			// clear all the data that might exists already
			mBankDetails = new String[4];
			mBeneficiary = new String[4];
		}
		
		// get the data from the response
		if (mBankDetails == null) mBankDetails = new String[4];
		if (beneficiary != null){
			mBankDetails[0] = beneficiary.getBeneficiaryBank1() != null ? beneficiary.getBeneficiaryBank1() : "";
			mBankDetails[1] = beneficiary.getBeneficiaryBank2() != null ? beneficiary.getBeneficiaryBank2() : "";
			mBankDetails[2] = beneficiary.getBeneficiaryBank3() != null ? beneficiary.getBeneficiaryBank3() : "";
			mBankDetails[3] = beneficiary.getBeneficiaryBank4() != null ? beneficiary.getBeneficiaryBank4() : "";
		}else{
			mBankDetails[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
			mBankDetails[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
			mBankDetails[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
			mBankDetails[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
		}
		
		if (mBeneficiary == null) mBeneficiary = new String[4];
		if (beneficiary != null){
			mBeneficiary[0] = beneficiary.getBeneficiary1() != null ? beneficiary.getBeneficiary1() : "";
			mBeneficiary[1] = beneficiary.getBeneficiary2() != null ? beneficiary.getBeneficiary2() : "";
			mBeneficiary[2] = beneficiary.getBeneficiary3() != null ? beneficiary.getBeneficiary3() : "";
			mBeneficiary[3] = beneficiary.getBeneficiary4() != null ? beneficiary.getBeneficiary4() : "";
		}
		
		if (isWizard){
			// If it is a direct customer account, the "In favor of" field
			// must not be displayed
			
			if (mPaymentSubType == PaymentSubType.POSTCHECK_ACCOUNT){							
				showPostAccountNumber();
				showBankDetails();
				hideBeneficiaryDetails();
				if (dialog != null){
					dialog.dismiss();
				}		
				// keep this data for the beneficiary details
				if (mBeneficiary == null) mBeneficiary = new String[4];
				
				if (beneficiary == null)	
				{
					mBeneficiary[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
					mBeneficiary[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
					mBeneficiary[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
					mBeneficiary[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
				}
			}else if (mPaymentSubType == PaymentSubType.BANK_ACCOUNT){				
				// If it is a bank account, the data for the bank is not editable.
				// The user has to input the "In favor of" data
				if (isArrayEmpty(mBeneficiary)){
					showBeneficiaryDialog(isWizard);
				}else{
					if (dialog != null){
						dialog.dismiss();
					}
					clearBackStack();
					hideToField();
					showBankDetails();
					showBeneficiaryDetails();
					showPostAccountNumber();
				}
			}else{
				// If this is of an unknown type, go to the next step
				if (isArrayEmpty(mBankDetails)){
					showBankDetailsDialog(isWizard);
				}else{
					if (dialog != null){
						dialog.dismiss();
					}
					clearBackStack();
					hideToField();
					showBankDetails();
					showBeneficiaryDetails();
					showPostAccountNumber();
				}
			}
		}else{
			showPostAccountNumber();
			showBankDetails();
			if (mPaymentSubType == PaymentSubType.POSTCHECK_ACCOUNT){							
				hideBeneficiaryDetails();
				// keep this data for the beneficiary details
				if (mBeneficiary == null) mBeneficiary = new String[4];
				if (beneficiary == null)
				{
					mBeneficiary[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
					mBeneficiary[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
					mBeneficiary[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
					mBeneficiary[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
				}
			}else{
				showBeneficiaryDetails();
			}
									
			if (dialog != null){
				dialog.dismiss();
			}
		}
	}
	
	
	/**
	 * 
	 */
	private void validateOrangePaymentAccount(final Dialog dialog, final boolean isWizard){
		
		RequestStateEvent<SwissPostCheckInfoRequest> rse = new RequestStateEvent<SwissPostCheckInfoRequest>() {
			@Override
			public void onRequestCompleted(SwissPostCheckInfoRequest request) {	
				accountOrangeValidationReceived(dialog, request, isWizard);				
			}			
		};
		
		// Initiate the request 
		SwissPostCheckInfoRequest request = SwissOrangePaymentService.getPostCheckInfo(mAccountPost, rse);
			
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	/**
	 * 
	 */
	private void validateRedPaymentAccount(final Dialog dialog, final boolean isWizard){
		RequestStateEvent<com.avaloq.framework.comms.webservice.swissredpayment.SwissPostCheckInfoRequest> rse = new RequestStateEvent<com.avaloq.framework.comms.webservice.swissredpayment.SwissPostCheckInfoRequest>() {
			@Override
			public void onRequestCompleted(com.avaloq.framework.comms.webservice.swissredpayment.SwissPostCheckInfoRequest request) {	
				accountRedValidationReceived(dialog, request, isWizard);				
			}			
		};
		
		// Initiate the request 
		com.avaloq.framework.comms.webservice.swissredpayment.SwissPostCheckInfoRequest request = SwissRedPaymentService.getPostCheckInfo(mAccountPost, rse);
			
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	private void accountRedValidationReceived(Dialog dialog, com.avaloq.framework.comms.webservice.swissredpayment.SwissPostCheckInfoRequest request, boolean isWizard){
		SwissPostCheckInfoResult result = request.getResponse().getData();

		if (result != null) {

			// If the result has a notification list, there are errors			
			if (result.getPostCheckInfo() != null){
				
				// get the account number from the WS				
				if (result.getPostCheckInfo().getPcAccount() != null && result.getPostCheckInfo().getPcAccount().length()>0){
					mAccountPost = result.getPostCheckInfo().getPcAccount();
				}
				
				if (
						result.getPostCheckInfo().getNotificationList() != null &&					
						result.getPostCheckInfo().getNotificationList().size() > 0){
					// there are errors for the account validation
					String errorString = getContext().getString(R.string.pmt_pcaccount_validation_error);
					
					DialogFragment.createAlert(getActivity().getString(R.string.pmt_view_field_error), errorString, getActivity()).show(getActivity());
				}
				else {
					if (result.getPostCheckInfo().getBeneficiaryList() != null && getListByPaymentType(result.getPostCheckInfo().getBeneficiaryList(), PaymentType.SWISS_RED_PAYMENT_SLIP).size() > 0){
						BeneficiaryListDialogFragment bldf = new BeneficiaryListDialogFragment();
						bldf.setBeneficiaryList(getListByPaymentType(result.getPostCheckInfo().getBeneficiaryList(), PaymentType.SWISS_RED_PAYMENT_SLIP));
						bldf.setPostCheckInfo(result.getPostCheckInfo());
						bldf.setAdditionalParams(dialog, isWizard, PcAccountValidationType.RED);
						bldf.show();
						return;
					}else{
						processRedValidationResult(dialog, isWizard, result.getPostCheckInfo(), null);
					}
				}
			} 
		}
		
		((Button)dialog.findViewById(R.id.pmt_view_field_beneficiary_save)).setEnabled(true);
	}
	
	
	/**
	 * 
	 * @param dialog
	 * @param isWizard
	 * @param postCheckInfoOutput
	 * @param beneficiary
	 */
	private void processRedValidationResult(Dialog dialog, boolean isWizard, PostCheckInfoOutputTO postCheckInfoOutput, BeneficiaryTO beneficiary){
		if (beneficiary != null){
			mPaymentSubType = beneficiary.getPaymentSubType();
		}else{
			mPaymentSubType = postCheckInfoOutput.getPaymentSubType();
		}
		
		// get the data from the response
		if (mBankDetails == null) mBankDetails = new String[4];
		if (beneficiary != null){
			mBankDetails[0] = beneficiary.getBeneficiaryBank1() != null ? beneficiary.getBeneficiaryBank1() : "";
			mBankDetails[1] = beneficiary.getBeneficiaryBank2() != null ? beneficiary.getBeneficiaryBank2() : "";
			mBankDetails[2] = beneficiary.getBeneficiaryBank3() != null ? beneficiary.getBeneficiaryBank3() : "";
			mBankDetails[3] = beneficiary.getBeneficiaryBank4() != null ? beneficiary.getBeneficiaryBank4() : "";
		}else{
			mBankDetails[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
			mBankDetails[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
			mBankDetails[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
			mBankDetails[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
		}
		
		if (mBeneficiary == null) mBeneficiary = new String[4];
		if (beneficiary != null){
			mBeneficiary[0] = beneficiary.getBeneficiary1() != null ? beneficiary.getBeneficiary1() : "";
			mBeneficiary[1] = beneficiary.getBeneficiary2() != null ? beneficiary.getBeneficiary2() : "";
			mBeneficiary[2] = beneficiary.getBeneficiary3() != null ? beneficiary.getBeneficiary3() : "";
			mBeneficiary[3] = beneficiary.getBeneficiary4() != null ? beneficiary.getBeneficiary4() : "";
			
			if (beneficiary.isIban()){
				mAccountIBAN = beneficiary.getAccount();
			}else{
				mAccount = beneficiary.getAccount();
			}
			mBankNtnl = beneficiary.getNtnl();
		}else{
			mAccount = "";
			mAccountIBAN = "";
			mBankNtnl = "";
		}
		
		if (isWizard){
			// If it is a direct customer account, the "In favor of" field
			// must not be displayed
			if (mPaymentSubType == PaymentSubType.POSTCHECK_ACCOUNT){							
				showPostAccountNumber();
				showBankDetails();
				hideBeneficiaryDetails();
				mAccountIBAN = "";
				showIban();
				if (dialog != null){
					dialog.dismiss();
				}		
				// keep this data for the beneficiary details
				if (mBeneficiary == null) mBeneficiary = new String[4];
				if (beneficiary == null)	
				{
					mBeneficiary[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
					mBeneficiary[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
					mBeneficiary[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
					mBeneficiary[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
				}
				mAccount = "";
				mAccountIBAN = "";
				mBankNtnl = "";
			}else if (mPaymentSubType == PaymentSubType.BANK_ACCOUNT || mPaymentSubType == PaymentSubType.BANK_UNDEFINED){
				// If it is a bank account, the data for the bank is not editable.
				// The user has to input the "In favor of" data, if it is not available
				if (isArrayEmpty(mBankDetails)){
					showBankDetailsDialog(isWizard);
				}else if (isArrayEmpty(mBeneficiary) || mAccountIBAN.length() == 0){
					showBeneficiaryIBANDialog(isWizard);
				}else if (dialog != null){
					dialog.dismiss();
					clearBackStack();
					hideToField();
					showBankDetails();
					showBeneficiaryDetails();
					showIban();
					showPostAccountNumber();
				}
			}else{
				// If this is of an unknown type, go to the next step 
				
				if (beneficiary != null){
					mBankDetails[0] = beneficiary.getBeneficiary1() != null ? beneficiary.getBeneficiary1() : "";
					mBankDetails[1] = beneficiary.getBeneficiary2() != null ? beneficiary.getBeneficiary2() : "";
					mBankDetails[2] = beneficiary.getBeneficiary3() != null ? beneficiary.getBeneficiary3() : "";
					mBankDetails[3] = beneficiary.getBeneficiary4() != null ? beneficiary.getBeneficiary4() : "";
				}
				
				mAccount = "";
				mAccountIBAN = "";
				mBankNtnl = "";
				
				if (isArrayEmpty(mBankDetails)){
					showBankDetailsDialog(isWizard);
				}else if (dialog != null){
					dialog.dismiss();
					clearBackStack();
					hideToField();
					showBankDetails();
					showPostAccountNumber();
				}
			}
		}else{
			showPostAccountNumber();
			showBankDetails();
			if (mPaymentSubType == PaymentSubType.POSTCHECK_ACCOUNT){							
				hideBeneficiaryDetails();
				mAccountIBAN = "";
				mBankNtnl = "";
				showIban();
				// keep this data for the beneficiary details
				if (mBeneficiary == null) mBeneficiary = new String[4];
				if (beneficiary == null)
				{
					mBeneficiary[0] = postCheckInfoOutput.getAddress1() != null ? postCheckInfoOutput.getAddress1() : "";
					mBeneficiary[1] = postCheckInfoOutput.getAddress2() != null ? postCheckInfoOutput.getAddress2() : "";
					mBeneficiary[2] = postCheckInfoOutput.getAddress3() != null ? postCheckInfoOutput.getAddress3() : "";
					mBeneficiary[3] = postCheckInfoOutput.getAddress4() != null ? postCheckInfoOutput.getAddress4() : "";
				}
			}else if (mPaymentSubType == PaymentSubType.BANK_ACCOUNT){
				// If it is a bank account, the data for the bank is not editable.
				// The user has to input the "In favor of" data, if it is not available
				if (isArrayEmpty(mBeneficiary) || mAccountIBAN.length() == 0){
					showBeneficiaryIBANDialog(isWizard);
				}else if (dialog != null){
					dialog.dismiss();
					clearBackStack();
					hideToField();
					showBankDetails();
					showBeneficiaryDetails();
					showIban();
					showPostAccountNumber();
				}
			}else{
				mAccountIBAN = "";
				mBankNtnl = "";
				showIban();
				mBeneficiary = new String[4];
				showBankDetails(true);
				hideBeneficiaryDetails();
			}
									
			if (dialog != null){
				dialog.dismiss();
			}
		}
	}
	
	
	
	/**
	 * 
	 * @param iban
	 */
	private void requestBankInfoByIBANDomestic(String iban, final Dialog dialog, final boolean isWizard){
		RequestStateEvent<BankInfoRequest> rse = new RequestStateEvent<BankInfoRequest>() {
			@Override
			public void onRequestCompleted(BankInfoRequest request) {	
				bankInfoByIBANReceivedDomestic(request, dialog, isWizard);				
			}			
		};
		
		// Initiate the request 
		BankInfoRequest request = DomesticPaymentService.findDomesticBankInfoByIban(iban, rse);
			
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	/**
	 * 
	 */
	private void bankInfoByIBANReceivedDomestic(BankInfoRequest request, Dialog dialog, boolean isWizard){
		BankInfoResult result = request.getResponse().getData();
		
		if(result != null){
			// check if there is any data
			if (result.getBankInfo() != null){
				mBankDetails[0] = result.getBankInfo().getLine1();
				mBankDetails[1] = result.getBankInfo().getLine2();
				mBankDetails[2] = result.getBankInfo().getLine3();
				mBankDetails[3] = result.getBankInfo().getLine4();
				
				mBankNtnl = result.getBankInfo().getNtnl();
				mBankBIC = result.getBankInfo().getBic();
			}
			
			// if there are beneficiaries returned by the server, display them, otherwise validate the IBAN
			if (result.getBeneficiaries() != null && getListByPaymentType(result.getBeneficiaries(), PaymentType.DOMESTIC_PAYMENT).size() > 0){
				BeneficiaryListDialogFragment bldf = new BeneficiaryListDialogFragment();
				bldf.setBeneficiaryList(getListByPaymentType(result.getBeneficiaries(), PaymentType.DOMESTIC_PAYMENT));
				bldf.setAdditionalParams(dialog, isWizard, PcAccountValidationType.DOMESTIC);
				bldf.show();
				
				// ****** filter the beneficiaries
			}else{
				requestIBANValidation(mAccountIBAN, dialog, isWizard);
			}	
		}
	}
	
	/**
	 * 
	 * @param iban
	 */
	private void requestBankInfoByIBANInternational(String iban, final Dialog dialog, final boolean isWizard){
		RequestStateEvent<SwiftBankInfoRequest> rse = new RequestStateEvent<SwiftBankInfoRequest>() {
			@Override
			public void onRequestCompleted(SwiftBankInfoRequest request) {	
				bankInfoByIBANReceivedInternational(request, dialog, isWizard);				
			}			
		};
		
		// Initiate the request 
		SwiftBankInfoRequest request = InternationalPaymentService.findSwiftBankInfoByIban(iban, rse);
			
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	/**
	 * 
	 */
	private void bankInfoByIBANReceivedInternational(SwiftBankInfoRequest request, Dialog dialog, boolean isWizard){
		SwiftBankInfoResult result = request.getResponse().getData();
		
		if(result != null){
			// check if there is any data
			if (result.getSwiftBankInfo() != null){
				mBankDetails[0] = result.getSwiftBankInfo().getLine1();
				mBankDetails[1] = result.getSwiftBankInfo().getLine2();
				mBankDetails[2] = result.getSwiftBankInfo().getLine3();
				mBankDetails[3] = result.getSwiftBankInfo().getLine4();
				
				mBankNtnl = result.getSwiftBankInfo().getNtnl();
				mBankBIC = result.getSwiftBankInfo().getBic();
				
				if (result.getSwiftBankInfo().getCountryId() != null){
					setCountryId(result.getSwiftBankInfo().getCountryId());
				}
			}
			
			// if there are beneficiaries returned by the server, display them, otherwise validate the IBAN
			if (result.getBeneficiaries() != null && result.getBeneficiaries().size() > 0){
				BeneficiaryListDialogFragment bldf = new BeneficiaryListDialogFragment();
				bldf.setBeneficiaryList(result.getBeneficiaries());
				bldf.setAdditionalParams(dialog, isWizard, PcAccountValidationType.INTERNATIONAL);
				bldf.show();

				// ****** filter the beneficiaries
			}else{
				requestIBANValidation(mAccountIBAN, dialog, isWizard);
			}	
		}
	}
	
	/**
	 * 
	 * @param iban
	 */
	private void requestIBANValidation(String iban, final Dialog dialog, final boolean isWizard){
		RequestStateEvent<IbanRequest> rse = new RequestStateEvent<IbanRequest>() {
			@Override
			public void onRequestCompleted(IbanRequest request) {	
				ibanValidationReceived(request, dialog, isWizard);				
			}			
		};
		
		// Initiate the request 
		IbanRequest request = PaymentOverviewService.validateIban(iban, rse);
			
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	private void ibanValidationReceived(IbanRequest request, Dialog dialog, boolean isWizard){
		IbanResult result = request.getResponse().getData();
		
		if (result != null){
			if ( result.getNotificationList().size() > 0 ){
				DialogFragment.createAlert("", getActivity().getString(R.string.pmt_view_invalid_iban_number), getActivity()).show(getActivity());
				return;
			}else{
				// get the data
				if (result.getIban() != null && !result.getIban().isEmpty()){
					mAccountIBAN = result.getIban();	
					mBankNtnl = result.getNtnl();
				}			
			}
		}else{
			DialogFragment.createAlert("", getActivity().getString(R.string.pmt_view_invalid_iban_number), getActivity()).show(getActivity());
			return;
		}
		
		if (mPaymentType == BeneficiaryFieldPaymentType.RED){			
			if(isWizard){
				showBeneficiaryDialog(isWizard);
			}else{
				showBeneficiaryDetails();
				showBankDetails();
				showIban();
				dialog.dismiss();
				clearBackStack();
			}
		}else if (mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC || mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
			if (isWizard){
				showBankDetailsExDialog(isWizard);
			}else{
				showIban();
				showBankDetails();
				dialog.dismiss();
			}
		}
	}
	
	/*************************************************************************************************************
	 * 
	 * Displays a dialog for insertion of the beneficiary information
	 * @author victor
	 *
	 *************************************************************************************************************/
	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class BeneficiaryDetailsDialogFragment extends WizardDialogFragment {

		EditText textName;
		EditText textAdress1;
		EditText textAdress2;
		EditText textCity;
		
		Button buttonSave;
		Button buttonReset;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_beneficiary_dialog);
			View view = inflater.inflate(R.layout.pmt_view_field_beneficiary_dialog, container, true);
			textName = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_name);
			textAdress1 = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_adresse1);
			textAdress2 = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_adresse2);
			textCity = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_city);
			
			if (mBeneficiary == null) mBeneficiary = new String[4];
			
			buttonSave = (Button)view.findViewById(R.id.pmt_view_field_beneficiary_save);
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					
					buttonSave.setEnabled(false);
					
					mBeneficiary = new String[] {
							textName.getText().toString(),
							textAdress1.getText().toString(),
							textAdress2.getText().toString(),
							textCity.getText().toString()
					};
					
					showBeneficiaryDetails();
					
					if (isWizard()){
						showBankDetails();
						showBeneficiaryDetails();
						if(mPaymentType == BeneficiaryFieldPaymentType.ORANGE || mPaymentType == BeneficiaryFieldPaymentType.RED){
							showPostAccountNumber();
							// also show iban if needed
							if (mPaymentType == BeneficiaryFieldPaymentType.RED && mPaymentSubType == PaymentSubType.BANK_ACCOUNT){
								showIban();
							}
						}else{
							showAccountNumber();
							showIban();
						}
					}
					
					
					getDialog().dismiss();
					clearBackStack();					
				}
			});
			
			buttonReset = (Button)view.findViewById(R.id.pmt_view_field_beneficiary_reset);
			
			buttonReset.setOnClickListener(clickListenerResetForm());
			
			
			textName.addTextChangedListener(textWatcherForInputs());
			textAdress1.addTextChangedListener(textWatcherForInputs());
			textAdress2.addTextChangedListener(textWatcherForInputs());
			textCity.addTextChangedListener(textWatcherForInputs());

			// Set the values
			if (mBeneficiary != null){
				textName.setText(mBeneficiary[0] != null ? mBeneficiary[0] : "");
				textAdress1.setText(mBeneficiary[1] != null ? mBeneficiary[1] : "");
				textAdress2.setText(mBeneficiary[2] != null ? mBeneficiary[2] : "");
				textCity.setText(mBeneficiary[3] != null ? mBeneficiary[3] : "");
			}
			
			return view;
		}

		public TextWatcher textWatcherForInputs(){
			return new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not used
				}

				@Override
				public void afterTextChanged(Editable s) {
					checkInputFields();	
				}
			};
		}

		public void checkInputFields(){
			if( 
					(textName.getText().toString().length() == 0 &&
					textAdress1.getText().toString().length() == 0 &&
					textAdress2.getText().toString().length() == 0 &&
					textCity.getText().toString().length() == 0) 
				){
				// button is disabled
				buttonSave.setEnabled(false);
			}else{
				buttonSave.setEnabled(true);
			}
		}
		
		/**
		 * 
		 * @return
		 */
		private OnClickListener clickListenerResetForm(){
			return new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					textName.setText("");
					textAdress1.setText("");
					textAdress2.setText("");
					textCity.setText("");
					
					mBeneficiary[0] = "";
					mBeneficiary[1] = "";
					mBeneficiary[2] = "";
					mBeneficiary[3] = "";	
				}
			};
		}
		
		@Override
		public void onResume(){
			super.onResume();
			checkInputFields();
		}
		
	}

	/*************************************************************************************************************
	 * 
	 * Displays a dialog for insertion of the beneficiary + IBAN information
	 * @author victor
	 *
	 *************************************************************************************************************/
	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class IbanDialogFragment extends WizardDialogFragment {

		private Button buttonSave;		
		private EditText textIBAN;		

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_beneficiary_iban);
			View view = inflater.inflate(R.layout.pmt_view_field_beneficiary_iban_dialog, container, true);			
			textIBAN = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_iban);
			buttonSave = (Button)view.findViewById(R.id.pmt_view_field_beneficiary_save);			
			textIBAN.addTextChangedListener(textWatcherForInputs());

			textIBAN.setText((mAccountIBAN) != null ? mAccountIBAN : "" );

			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					
					onSave();			
				}
			});

			return view;
		}

		private void onSave(){
			buttonSave.setEnabled(false);

			//validate iban
			mAccountIBAN = textIBAN.getText().toString();
			requestIBANValidation(mAccountIBAN, getDialog(), isWizard());	
		}
		
		public TextWatcher textWatcherForInputs(){
			return new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not used
				}

				@Override
				public void afterTextChanged(Editable s) {
					checkInputFields();	
				}
			};
		}

		public void checkInputFields(){
			if( textIBAN.getText().toString().length() == 0){
				// button is disabled
				buttonSave.setEnabled(false);
			}else{
				buttonSave.setEnabled(true);
			}
		}		
		
		@Override
		public void onResume(){
			super.onResume();
			checkInputFields();
		}
	}

	/*************************************************************************************************************
	 * 
	 * Displays a dialog for insertion of the Bank details information
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class BankDetailsDialogFragment extends WizardDialogFragment {

		View mFragmentview;
		
		EditText textName;
		EditText textAdress1;
		EditText textAdress2;
		EditText textCity;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_bank_details_dialog);
			mFragmentview = inflater.inflate(R.layout.pmt_view_field_bank_details_dialog, container, true);
			textName = (EditText)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_dialog_name);
			textAdress1 = (EditText)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse1);
			textAdress2 = (EditText)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse2);
			textCity = (EditText)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_dialog_city);
			final Button buttonSave = (Button)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_save);
			
			final Button buttonReset = (Button)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_reset);
			
			buttonReset.setOnClickListener(clickListenerResetForm());
			
			buttonSave.setEnabled(true);
			
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					
					buttonSave.setEnabled(false);
					
					mBankDetails = new String[] {
							textName.getText().toString(),
							textAdress1.getText().toString(),
							textAdress2.getText().toString(),
							textCity.getText().toString()
					};
					
					if (isWizard()){
						
						if(mPaymentType == BeneficiaryFieldPaymentType.ORANGE && (mPaymentSubType == PaymentSubType.BANK_ACCOUNT || mPaymentSubType == PaymentSubType.BANK_UNDEFINED || mPaymentSubType == PaymentSubType.EMPTY)) 
						{
							showBeneficiaryDialog(isWizard());
						}else if (mPaymentType == BeneficiaryFieldPaymentType.RED && (mPaymentSubType == PaymentSubType.BANK_ACCOUNT || mPaymentSubType == PaymentSubType.BANK_UNDEFINED)){
							showBeneficiaryIBANDialog(isWizard());						
						}else{
							showBankDetails();
							showPostAccountNumber();
							getDialog().dismiss();
							clearBackStack();
						}
					}else{
						showBankDetails();
						getDialog().dismiss();
					}			
				}
			});
			
			// Set the values
			if (mBankDetails != null){
				textName.setText(mBankDetails[0] != null ? mBankDetails[0] : "");
				textAdress1.setText(mBankDetails[1] != null ? mBankDetails[1] : "");
				textAdress2.setText(mBankDetails[2] != null ? mBankDetails[2] : "");
				textCity.setText(mBankDetails[3] != null ? mBankDetails[3] : "");
			}
			
			return mFragmentview;
		}

		/**
		 * 
		 * @return
		 */
		private OnClickListener clickListenerResetForm(){
			return new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					
					textName.setText("");
					textAdress1.setText("");
					textAdress2.setText("");
					textCity.setText("");
					
					mBankDetails[0] = "";
					mBankDetails[1] = "";
					mBankDetails[2] = "";
					mBankDetails[3] = "";	
				}
			};
		}
		
		@Override
		public void onResume(){
			super.onResume();
			((Button)mFragmentview.findViewById(R.id.pmt_view_field_bank_details_save)).setEnabled(true);			
		}
		
	}

	/**
	 * 
	 * Holds bank info depending on payment type
	 *
	 */
	private class BankInfo{
		public BankInfoTO bankInfo;
		public SwiftBankInfoTO swiftBankInfo;
	}
	
	/*************************************************************************************************************
	 * 
	 * Displays a dialog for insertion of the bank details information. It also has input fields and 
	 * possibility to search for a bank
	 * 
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class BankDetailsExDialogFragment extends WizardDialogFragment {

		Button buttonSearchCode;
		Button buttonSearchForm;
		Button buttonResetForm;
		Button buttonSave;
		EditText textBIC;
		
		EditText textName;
		EditText textAdress1;
		EditText textAdress2;
		EditText textCity;
		
		TextView tvCountry;
		
		
		
		@Override
		public void onResume(){
			super.onResume();
			// Set the values
			if (mBankDetails != null){
				textName.setText(mBankDetails[0] != null ? mBankDetails[0] : "");
				textAdress1.setText(mBankDetails[1] != null ? mBankDetails[1] : "");
				textAdress2.setText(mBankDetails[2] != null ? mBankDetails[2] : "");
				textCity.setText(mBankDetails[3] != null ? mBankDetails[3] : "");
			}

			if (mBankBIC != null){
				textBIC.setText(mBankBIC);
			}
			
			if (mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
				// show country select
				tvCountry.setVisibility(View.VISIBLE);
				if(mCountry == null){
					tvCountry.setText(R.string.pmt_view_field_country);
				}else{
					enableSearchButtons();
					tvCountry.setText(mCountry.getName());
				}
			}else{				
				tvCountry.setVisibility(View.GONE);
			}
			checkIfEnableSaveButton();
		}
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			
			getDialog().setTitle(R.string.pmt_view_field_bank_details_dialog);
			View view = inflater.inflate(R.layout.pmt_view_field_bank_details_ex_dialog, container, true);
			textName = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_name);
			textAdress1 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse1);
			textAdress2 = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_adresse2);
			textCity = (EditText)view.findViewById(R.id.pmt_view_field_bank_details_dialog_city);
			
			textBIC = (EditText) view.findViewById(R.id.pmt_view_field_bank_details_dialog_bankcode);
			
			tvCountry = (TextView) view.findViewById(R.id.pmt_view_field_bank_details_country);
			
			textAdress1.addTextChangedListener(new TextWatcher() {				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				@Override
				public void afterTextChanged(Editable s) {
					checkIfEnableSaveButton();
				}
			});
			textName.addTextChangedListener(new TextWatcher() {				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				@Override
				public void afterTextChanged(Editable s) {
					checkIfEnableSaveButton();
				}
			});
			
			
			buttonSearchCode = (Button) view.findViewById(R.id.pmt_view_field_bank_details_search_code);
			buttonSearchCode.setOnClickListener(clickListenerSearchCode());
			
			buttonSearchForm = (Button) view.findViewById(R.id.pmt_view_field_bank_details_search_bank);
			buttonSearchForm.setOnClickListener(clickListenerSearchForm());
			
			buttonResetForm = (Button) view.findViewById(R.id.pmt_view_field_bank_details_reset);
			buttonResetForm.setOnClickListener(clickListenerResetForm());
			
			buttonSave = (Button)view.findViewById(R.id.pmt_view_field_bank_details_save);			
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					onSave();
								
				}
			});
			
			tvCountry.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// open dialog to select bank data
					SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
					FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();	
					Fragment prev;					
					prev = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BANK_DETAILS_EX);if (prev != null) {ft.remove(prev);}	    
					ft.addToBackStack(null);
					CountryListDialogFragment countryListDialog = new CountryListDialogFragment();					
					countryListDialog.show(ft, null);
				}
			});
			
			disableSearchButtons();
			enableSearchButtons();
			
			
			return view;
		}

		private void onSave(){
			buttonSave.setEnabled(false);
			
			mBankDetails = new String[] {
					textName.getText().toString(),
					textAdress1.getText().toString(),
					textAdress2.getText().toString(),
					textCity.getText().toString()
			};
			
			if(isWizard()){
				if(isArrayEmpty(mBeneficiary)){
					showBeneficiaryDialog(isWizard());
				}else{
					showBankDetails();
					getDialog().dismiss();
				}	
			}else{
				showBankDetails();
				getDialog().dismiss();
			}
		}
		
		/**
		 * 
		 */
		private void checkIfEnableSaveButton(){
			// first check if all the info is there
			// 2 lines from bank, ntnl, and country
			if (
					textName.getText().toString().isEmpty() ||
					textAdress1.getText().toString().isEmpty() ||
					mBankNtnl == null ||
					mBankNtnl.isEmpty() || 
					(mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL && mCountryId == null)
		
				){
				// disable the button
				buttonSave.setEnabled(false);
			}else{
				// enable the button
				buttonSave.setEnabled(true);
			}
		}
		
		/**
		 * 
		 * @return
		 */
		private OnClickListener clickListenerResetForm(){
			return new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					mCountry = null;
					tvCountry.setText(R.string.pmt_view_field_country);
					textName.setText("");
					textAdress1.setText("");
					textAdress2.setText("");
					textCity.setText("");
					textBIC.setText("");
					
					mBankDetails[0] = "";
					mBankDetails[1] = "";
					mBankDetails[2] = "";
					mBankDetails[3] = "";	
					mBankBIC = "";
					mBankNtnl = "";
					mCountryId = null;
					checkIfEnableSaveButton();
				}
			};
		}
		
		/**
		 * 
		 * @return
		 */
		private OnClickListener clickListenerSearchCode(){
			return new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					buttonSearchCode.setEnabled(false);
					buttonSearchForm.setEnabled(false);
					if (mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC){
						searchByBIC(textBIC.getText().toString());
					}else if (mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
						searchByBic(textBIC.getText().toString());
					}
				}
			};
		}
		
		/**
		 * 
		 * @return
		 */
		private OnClickListener clickListenerSearchForm(){
			return new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					disableSearchButtons();
					searchByCriteria();
				}
			};
		}
		
		/**
		 * 		
		 */
		private void searchByCriteria(){			
			if (mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC){
				searchByCriteriaDomestic();
			}else if(mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL){
				searchByCriteriaInternational();
			}
		}
		
		/**
		 * 		
		 */
		private void searchByCriteriaDomestic(){
			BankInfoQueryTO query = new BankInfoQueryTO();
			
			query.setName(textName.getText().toString());
			query.setCity(textCity.getText().toString());
			
			RequestStateEvent<BankInfoListRequest> rse = new RequestStateEvent<BankInfoListRequest>() {
				@Override
				public void onRequestCompleted(BankInfoListRequest request) {	
					bankListReceived(request, null, false);				
				}			
			};
			
			// Initiate the request
			BankInfoListRequest request = DomesticPaymentService.findDomesticBankInfos(query, rse);						
			// Add the request to the request queue
			request.initiateServerRequest();	
		}
		
		private void searchByCriteriaInternational(){
			SwiftBankInfoQueryTO query = new SwiftBankInfoQueryTO();
			
			query.setName(textName.getText().toString());
			query.setCity(textCity.getText().toString());
			query.setCountryId(mCountry != null ? mCountry.getId() : null);
			
			RequestStateEvent<SwiftBankInfoListRequest> rse = new RequestStateEvent<SwiftBankInfoListRequest>() {
				@Override
				public void onRequestCompleted(SwiftBankInfoListRequest request) {	
					bankListReceived(null, request, false);				
				}			
			};
			
			// Initiate the request
			SwiftBankInfoListRequest request = InternationalPaymentService.findSwiftBankInfos(query, rse);	
			// Add the request to the request queue
			request.initiateServerRequest();
		}
		
		/**
		 * 
		 * @param ntnl
		 */
		private void searchByBic(String bic){			
			
			SwiftBankInfoQueryTO query = new SwiftBankInfoQueryTO();
			
			query.setBic(bic);
			
			RequestStateEvent<SwiftBankInfoListRequest> rse = new RequestStateEvent<SwiftBankInfoListRequest>() {
				@Override
				public void onRequestCompleted(SwiftBankInfoListRequest request) {	
					bankListReceived(null, request, true);				
				}			
			};
			
			// Initiate the request
			SwiftBankInfoListRequest request = InternationalPaymentService.findSwiftBankInfos(query, rse);	
			// Add the request to the request queue
			request.initiateServerRequest();
		}
		
		/**
		 * 
		 * @param ntnl
		 */
		private void searchByBIC(String ntnl){
			
			RequestStateEvent<BankInfoListRequest> rse = new RequestStateEvent<BankInfoListRequest>() {
				@Override
				public void onRequestCompleted(BankInfoListRequest request) {	
					bankListReceived(request, null, true);				
				}			
			};
			
			// Initiate the request
			BankInfoListRequest request = DomesticPaymentService.findDomesticBankInfoByNtnl(ntnl, rse);			
				
			// Add the request to the request queue
			request.initiateServerRequest();	
		}
		
		private void disableSearchButtons(){			
			buttonSearchCode.setEnabled(false);
			buttonSearchForm.setEnabled(false);
			tvCountry.setClickable(false);
		}
		
		private void enableSearchButtons(){
			if  (
					(mCountry != null && mPaymentType == BeneficiaryFieldPaymentType.INTERNATIONAL) || 
					(mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC) 
				){
				buttonSearchCode.setEnabled(true);
				buttonSearchForm.setEnabled(true);
			}
			tvCountry.setClickable(true);
		}
		
		/**
		 * 
		 * @param request
		 */
		private void bankListReceived(BankInfoListRequest bankInfoRequest, SwiftBankInfoListRequest swiftBankInfoRequest, boolean searchByBIC){
			
			enableSearchButtons();
			
			List<BankInfo> list = new ArrayList<BankInfo>();
			
			if (bankInfoRequest != null){
				BankInfoListResult resultBankInfo = bankInfoRequest.getResponse().getData();
				for(BankInfoTO bankInfo: resultBankInfo.getBankInfos()){
					BankInfo bi = new BankInfo();
					bi.bankInfo = bankInfo;
					list.add(bi);
				}
			}else if (swiftBankInfoRequest != null){
				SwiftBankInfoListResult resultSwiftBankInfo = swiftBankInfoRequest.getResponse().getData();				
				for(SwiftBankInfoTO bankInfo: resultSwiftBankInfo.getSwiftBankInfos()){
					BankInfo bi = new BankInfo();
					bi.swiftBankInfo = bankInfo;
					list.add(bi);
				}
			}else{
				return;
			}

			if (list.size() > 0){
				// open dialog to select bank data
				SherlockFragmentActivity a = (SherlockFragmentActivity)getContext();
				FragmentTransaction ft = a.getSupportFragmentManager().beginTransaction();	
				Fragment prev;					
				prev = a.getSupportFragmentManager().findFragmentByTag(DIALOG_BANK_DETAILS_EX);if (prev != null) {ft.remove(prev);}	    
				ft.addToBackStack(null);
				BankInfoListDialogFragment infoListDialog = new BankInfoListDialogFragment();
				infoListDialog.setBankInfoList(list);
				infoListDialog.show(ft, null);
			}else{
				showNoDataFound(searchByBIC);
			}

		}
		
		private void showNoDataFound(boolean searchByBIC){
			if (searchByBIC){
				DialogFragment.createAlert("", getActivity().getString(R.string.pmt_view_field_no_bank_info_found_bic), getActivity()).show(getActivity());
			}else{
				DialogFragment.createAlert("", getActivity().getString(R.string.pmt_view_field_no_bank_info_found), getActivity()).show(getActivity());
			}
		}
	}
	
	/*************************************************************************************************************
	 * 
	 * Displays a dialog with the bank info list 
	 * 
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class BankInfoListDialogFragment extends DialogFragment {

		private ListView list; 
		private List<BankInfo> bankInfoList;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			getDialog().setTitle(R.string.pmt_view_field_select_bank);
			View view = inflater.inflate(R.layout.pmt_view_field_account_bank_info_list, container, true);
			
			list = (ListView) view.findViewById(R.id.pmt_view_field_bank_info_list);
			if (bankInfoList != null){
				list.setAdapter(new BankInfoAdapter(getContext(), bankInfoList));
			}
			return view;
		}

		/**
		 * 
		 * @param bankInfoList
		 */
		public void setBankInfoList(List<BankInfo> bankInfoList){
			this.bankInfoList = bankInfoList;
		}
		
		/**
		 * 
		 * @author victor
		 *
		 */
		public class BankInfoAdapter extends ArrayAdapter<BankInfo> {

            private BankInfoAdapter(Context context, List<BankInfo> objects) {
                super(context, 0, objects);
            }

            public class BankInfoViewHolder {
                TextView textName;
                TextView textAddr1;
                TextView textAddr2;
                TextView textAddr3;
                TextView textAddr4;   
                String ntnl;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_account_bank_info_row, null);
                    BankInfoViewHolder h = new BankInfoViewHolder();
                    h.textName = (TextView) row.findViewById(R.id.pmt_view_field_bank_info_name);
                    h.textAddr1 = (TextView) row.findViewById(R.id.pmt_view_field_bank_info_addr_1);
                    h.textAddr2 = (TextView) row.findViewById(R.id.pmt_view_field_bank_info_addr_2);
                    h.textAddr3 = (TextView) row.findViewById(R.id.pmt_view_field_bank_info_addr_3);
                    h.textAddr4 = (TextView) row.findViewById(R.id.pmt_view_field_bank_info_addr_4);
                    
                    row.setTag(h);
                }
                final BankInfoViewHolder holder = (BankInfoViewHolder)row.getTag();
                final BankInfo account = getItem(position);
                
                BankInfo item = getItem(position);
                
                // for domestic and international payments, the object for the bank info is different
                if (mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC){
	                holder.textName.setText(item.bankInfo.getLine1());
	                holder.textAddr1.setText(item.bankInfo.getLine2());
	                holder.textAddr2.setText(item.bankInfo.getLine3());
	                holder.textAddr3.setText(item.bankInfo.getLine4());
	                
	                holder.textAddr4.setText(item.bankInfo.getBic());
	                holder.ntnl = item.bankInfo.getNtnl();
                }else{
                	holder.textName.setText(item.swiftBankInfo.getLine1());
	                holder.textAddr1.setText(item.swiftBankInfo.getLine2());
	                holder.textAddr2.setText(item.swiftBankInfo.getLine3());
	                holder.textAddr3.setText(item.swiftBankInfo.getLine4());
	                
	                holder.textAddr4.setText(item.swiftBankInfo.getBic());
	                holder.ntnl = item.swiftBankInfo.getNtnl();
                }
                
                row.setOnClickListener(clickListenerList());
                
                return row;
            }
            
            /**
    		 * 
    		 * @return
    		 */
    		private OnClickListener clickListenerList(){
    			return new OnClickListener() {
    				
    				@Override
    				public void onClick(View v) {
    					BankInfoViewHolder bankInfoViewHolder = (BankInfoViewHolder) v.getTag();
    					if (mBankDetails == null) mBankDetails = new String[4];
    					mBankDetails[0] = bankInfoViewHolder.textName.getText().toString();
    					mBankDetails[1] = bankInfoViewHolder.textAddr1.getText().toString();
    					mBankDetails[2] = bankInfoViewHolder.textAddr2.getText().toString();
    					mBankDetails[3] = bankInfoViewHolder.textAddr3.getText().toString();
    					
    					mBankBIC = bankInfoViewHolder.textAddr4.getText().toString();
    					
    					mBankNtnl = bankInfoViewHolder.ntnl;
    					
    					getDialog().dismiss();
    				}
    			};
    		}
        }
	}
	
	
	/*************************************************************************************************************
	 * 
	 * Displays a dialog with the beneficiary list 
	 * 
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class BeneficiaryListDialogFragment extends DialogFragment {

		private ListView list; 
		
		private List<BeneficiaryTO> listBeneficiaries;
		
		private Dialog PostAccountDialog;
		private boolean IsWizard;
		private PostCheckInfoOutputTO PostCheckInfoOutput;
		private PcAccountValidationType ValidationType;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_select_beneficiary);
			View view = inflater.inflate(R.layout.pmt_view_field_beneficiary_list, container, true);
			
			list = (ListView) view.findViewById(R.id.pmt_view_field_beneficiary_list);
			if (listBeneficiaries != null){
				list.setAdapter(new BeneficiaryInfoAdapter(getContext(), listBeneficiaries));
			}
			return view;
		}

		public void setBeneficiaryList(List<BeneficiaryTO> beneficiaryList){			
			listBeneficiaries = beneficiaryList;
		}
		
		public void setPostCheckInfo(PostCheckInfoOutputTO postCheckInfoOutput){
			PostCheckInfoOutput = postCheckInfoOutput;
		}
		
		public void setAdditionalParams(Dialog postAccountDialog, boolean isWizard, PcAccountValidationType validationType){
			PostAccountDialog = postAccountDialog;
			IsWizard = isWizard;	
			ValidationType = validationType;
		}
		
		/**
		 * 
		 * @author victor
		 *
		 */
		public class BeneficiaryInfoAdapter extends ArrayAdapter<BeneficiaryTO> {

			private BeneficiaryInfoAdapter(Context context, List<BeneficiaryTO> objects) {
                super(context, 0, objects);
            }

            public class BeneficiaryInfoViewHolder {
                TextView textBank;
                TextView textBeneficiary;
                View labelPaymentFor;
                View labelBeneficiary;
                View labelOther;
                BeneficiaryTO beneficiary;
                boolean optionOther = false;
            }

            @Override
            public int getCount(){
            	return super.getCount() + 1;
            }
            
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_beneficiary_row, null);
                    BeneficiaryInfoViewHolder h = new BeneficiaryInfoViewHolder();
                    h.textBank = (TextView) row.findViewById(R.id.pmt_view_field_bank);
                    h.textBeneficiary = (TextView) row.findViewById(R.id.pmt_view_field_beneficiary);
                    h.labelBeneficiary = row.findViewById(R.id.pmt_view_field_label_payment_for);
                    h.labelPaymentFor = row.findViewById(R.id.pmt_view_field_payment_label_beneficiary);
                    h.labelOther = row.findViewById(R.id.pmt_view_field_label_other);
                    row.setTag(h);
                }
                final BeneficiaryInfoViewHolder holder = (BeneficiaryInfoViewHolder)row.getTag();                
                
                if (position == 0){
                	// this is the "Other" row
                	holder.labelOther.setVisibility(View.VISIBLE);
                	holder.beneficiary = null;
                	holder.textBank.setVisibility(View.GONE);
                	holder.textBeneficiary.setVisibility(View.GONE);
                	holder.labelBeneficiary.setVisibility(View.GONE);
                	holder.labelPaymentFor.setVisibility(View.GONE);
                }else{
                
                	// for orange payment, personal account
                	// for red payment, subtype other than bank account:
                	// put the beneficiary data in the Payment for field and hide the other field
                	
                	// Otherwise, show both fields
                	// TODO check and implement the above if needed
                	
	                BeneficiaryTO item = getItem(position-1);
	                
	                holder.textBank.setText(
	                		( (item.getBeneficiaryBank1() != null && item.getBeneficiaryBank1().length() > 0) ? item.getBeneficiaryBank1() + "\n" : "") +
	                		( (item.getBeneficiaryBank2() != null && item.getBeneficiaryBank2().length() > 0) ? item.getBeneficiaryBank2() + "\n" : "") +
	                		( (item.getBeneficiaryBank3() != null && item.getBeneficiaryBank3().length() > 0) ? item.getBeneficiaryBank3() + "\n" : "") +
	                		( (item.getBeneficiaryBank4() != null && item.getBeneficiaryBank4().length() > 0) ? item.getBeneficiaryBank4() + "\n" : "")
	                		);    
	                holder.textBeneficiary.setText(
	                		( (item.getBeneficiary1() != null && item.getBeneficiary1().length() > 0) ? item.getBeneficiary1() + "\n" : "") +
	                		( (item.getBeneficiary2() != null && item.getBeneficiary2().length() > 0) ? item.getBeneficiary2() + "\n" : "") +
	                		( (item.getBeneficiary3() != null && item.getBeneficiary3().length() > 0) ? item.getBeneficiary3() + "\n" : "") +
	                		( (item.getBeneficiary4() != null && item.getBeneficiary4().length() > 0) ? item.getBeneficiary4() + "\n" : "")
	                		); 
	                holder.beneficiary = item;
	                
	                holder.textBank.setVisibility(View.VISIBLE);
	                holder.textBeneficiary.setVisibility(View.VISIBLE);
                	holder.labelBeneficiary.setVisibility(View.VISIBLE);
                	holder.labelPaymentFor.setVisibility(View.VISIBLE);
	                
                }
                row.setOnClickListener(clickListenerList());
                
                return row;
            }
            
            /**
    		 * 
    		 * @return
    		 */
    		private OnClickListener clickListenerList(){
    			return new OnClickListener() {
    				
    				@Override
    				public void onClick(View v) {
    					BeneficiaryInfoViewHolder recordViewHolder = (BeneficiaryInfoViewHolder) v.getTag();
    					
    					if (ValidationType == PcAccountValidationType.ORANGE){
    						processOrangeValidationResult(PostAccountDialog, IsWizard, PostCheckInfoOutput, recordViewHolder.beneficiary);
    					}else if (ValidationType == PcAccountValidationType.RED){
    						processRedValidationResult(PostAccountDialog, IsWizard, PostCheckInfoOutput, recordViewHolder.beneficiary);
    					}else if (ValidationType == PcAccountValidationType.DOMESTIC || ValidationType == PcAccountValidationType.INTERNATIONAL){
    						
    						// get all the data available from the response
    						
    						if (recordViewHolder.beneficiary.isIban()){
    							mAccountIBAN = recordViewHolder.beneficiary.getAccount();
    						}else{
    							mAccount = recordViewHolder.beneficiary.getAccount();
    						}
    						 
    						mBankBIC = recordViewHolder.beneficiary.getBic();
    						mBankNtnl = recordViewHolder.beneficiary.getNtnl();
    								
    						mBankDetails[0] = recordViewHolder.beneficiary.getBeneficiaryBank1();
    						mBankDetails[1] = recordViewHolder.beneficiary.getBeneficiaryBank2();
    						mBankDetails[2] = recordViewHolder.beneficiary.getBeneficiaryBank3();
    						mBankDetails[3] = recordViewHolder.beneficiary.getBeneficiaryBank4();
    						
    						mBeneficiary[0] = recordViewHolder.beneficiary.getBeneficiary1();
    						mBeneficiary[1] = recordViewHolder.beneficiary.getBeneficiary2();
    						mBeneficiary[2] = recordViewHolder.beneficiary.getBeneficiary3();
    						mBeneficiary[3] = recordViewHolder.beneficiary.getBeneficiary4();
    						
    						if (ValidationType == PcAccountValidationType.INTERNATIONAL){
    							mCountryId = recordViewHolder.beneficiary.getCountryId();
    						}
    						
    						//****** Check what else to take from the response
    						
    						
    						
    						// proceed further with the logic
    						if (IsWizard){
    							showBankDetailsExDialog(IsWizard);
    						}else{
    							showIban();
    							showBankDetails();    							
    						}
    					}    					
    					getDialog().dismiss();
    				}
    			};
    		}
        }
	}
	
	
	/*************************************************************************************************************
	 * 
	 * Displays a dialog with the country list 
	 * 
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class CountryListDialogFragment extends DialogFragment {

		private ListView list; 
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_select_country);
			View view = inflater.inflate(R.layout.pmt_view_field_account_country_list, container, true);
			
			list = (ListView) view.findViewById(R.id.pmt_view_field_country_list);
			if (mCountries != null){
				list.setAdapter(new BankInfoAdapter(getContext(), mCountries));
			}
			return view;
		}

		/**
		 * 
		 * @author victor
		 *
		 */
		public class BankInfoAdapter extends ArrayAdapter<CountryTO> {

            private BankInfoAdapter(Context context, List<CountryTO> objects) {
                super(context, 0, objects);
            }

            public class CountryInfoViewHolder {
                TextView textName;    
                CountryTO country;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View row = convertView;
                if(row == null) {
                    row = LayoutInflater.from(getContext()).inflate(R.layout.pmt_view_field_account_country_row, null);
                    CountryInfoViewHolder h = new CountryInfoViewHolder();
                    h.textName = (TextView) row.findViewById(R.id.pmt_view_field_country);
                    
                    row.setTag(h);
                }
                final CountryInfoViewHolder holder = (CountryInfoViewHolder)row.getTag();                
                
                CountryTO item = getItem(position);
                
                holder.textName.setText(item.getName());                                
                holder.country = item;
                
                row.setOnClickListener(clickListenerList());
                
                return row;
            }
            
            /**
    		 * 
    		 * @return
    		 */
    		private OnClickListener clickListenerList(){
    			return new OnClickListener() {
    				
    				@Override
    				public void onClick(View v) {
    					CountryInfoViewHolder countryViewHolder = (CountryInfoViewHolder) v.getTag();
    					
    					mCountry = countryViewHolder.country;
    					mCountryId = mCountry.getId();
    					getDialog().dismiss();
    				}
    			};
    		}
        }
	}
	
	
	/*************************************************************************************************************
	 * 
	 * Input for the Post account. Used for Orange and Red payments.
	 * @author victor
	 *
	 *************************************************************************************************************/
	@SuppressLint("ValidFragment")
	private class PostAccountDialogFragment extends WizardDialogFragment {

		private Button buttonSave;
		private EditText textAccountPost;		
				
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			
			getDialog().setTitle(R.string.pmt_post_account_number);
			
			View view = inflater.inflate(R.layout.pmt_view_field_account_number_dialog, container, true);
			
			textAccountPost = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_postaccount);			
			buttonSave = (Button)view.findViewById(R.id.pmt_view_field_beneficiary_save);

			textAccountPost.setText((mAccountPost) != null ? mAccountPost : "");
			
			attachClickListenerToSubmitButton();
			
			return view;
		}

		private void attachClickListenerToSubmitButton(){
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) { 
					
					buttonSave.setEnabled(false);
					
					mAccountPost = textAccountPost.getText().toString();
					
					// The post account has to be validated. If the
					// validation is ok, go to the next step, and
					// return otherwise
					validatePostAccount(getDialog(), isWizard());
				}
			});
		}
		
		@Override
		public void onResume(){
			super.onResume();			
			buttonSave.setEnabled(true);
		}
	}

	/*************************************************************************************************************
	 * 
	 * Input for the Post account. Used for Orange and Red payments.
	 * @author victor
	 *
	 *************************************************************************************************************/
	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class AccountDialogFragment extends WizardDialogFragment {

		private Button buttonSave;
		private EditText textAccount;
		private EditText textAccountIBAN;
		private View containerButtons;
		private View containerAccountPost;
		private View containerAccountIBAN;
		
		private Button buttonSelectIBAN;
		private Button buttonSelectAccount;
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			getDialog().setTitle(R.string.pmt_view_field_beneficiary_account);
			View view = inflater.inflate(R.layout.pmt_view_field_account_dialog, container, true);
			textAccount = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_postaccount);
			textAccountIBAN = (EditText)view.findViewById(R.id.pmt_view_field_beneficiary_dialog_iban);
			containerButtons = view.findViewById(R.id.pmt_accountselect_dialog_buttons);
			containerAccountPost = view.findViewById(R.id.pmt_accountselect_dialog_post);
			containerAccountIBAN = view.findViewById(R.id.pmt_accountselect_dialog_iban);
			
			buttonSelectAccount = (Button) view.findViewById(R.id.pmt_view_account_btnAccount);
			buttonSelectIBAN = (Button) view.findViewById(R.id.pmt_view_account_btnIBAN);

			buttonSave = (Button)view.findViewById(R.id.pmt_view_field_beneficiary_save);
			
			setVisibleAccountOptions();
			
			if (mAccount.length() == 0 && mAccountIBAN.length() == 0){
				buttonSave.setEnabled(false);
			}else{
				buttonSave.setEnabled(true);
			}
			
			return view;
		}
		
		@Override
		public void onResume(){
			super.onResume();
			if (!mAccountIBAN.isEmpty()){
				textAccountIBAN.setText(mAccountIBAN);
			}			

			if (!mAccount.isEmpty()){
				textAccount.setText(mAccount);
			}
			attachIBANTextWatcher();
			buttonSave.setEnabled(true);
		}
		
		private void attachIBANTextWatcher(){

			TextWatcher textWatcher = new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not used						
				}

				@Override
				public void afterTextChanged(Editable s) {
					mAccount = textAccount.getText().toString();
					mAccountIBAN = textAccountIBAN.getText().toString();
					if (mAccount.length() > 0 || mAccountIBAN.length() > 0){
						buttonSave.setEnabled(true);
					}else{
						buttonSave.setEnabled(false);
					}
				}
			};
			
			textAccountIBAN.addTextChangedListener(textWatcher);
			textAccount.addTextChangedListener(textWatcher);
		}

		/**
		 * Depending on the account select type, show the correct options
		 */
		private void setVisibleAccountOptions(){

			attachClickListenersToSelectButtons();

			attachClickListenerToSubmitButton();


			// hide the IBAN container
			containerAccountIBAN.setVisibility(View.VISIBLE);

			// show the Post container
			containerAccountPost.setVisibility(View.VISIBLE);

			containerButtons.setVisibility(View.VISIBLE);

			
			
			// hide IBAN
			containerAccountIBAN.setVisibility(View.GONE);			
			// Show account
			containerAccountPost.setVisibility(View.VISIBLE);
		}
		
		/**
		 * If the account select has to show both options for account and IBAN
		 * then the select buttons are displayed and their click listeners must
		 * show/hide the respective containers
		 */
		private void attachClickListenersToSelectButtons(){
			buttonSelectAccount.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// hide IBAN
					containerAccountIBAN.setVisibility(View.GONE);
					
					// Show account
					containerAccountPost.setVisibility(View.VISIBLE);
				}
			});
			
			buttonSelectIBAN.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					// show IBAN
					containerAccountIBAN.setVisibility(View.VISIBLE);
					
					// hide account
					containerAccountPost.setVisibility(View.GONE);
				}
			});
		}

		private void attachClickListenerToSubmitButton(){
			buttonSave.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					
					buttonSave.setEnabled(false);
					
					if(mAccountIBAN.length() > 0 && mAccount.length() == 0){
						if(mPaymentType == BeneficiaryFieldPaymentType.DOMESTIC){
							requestBankInfoByIBANDomestic(mAccountIBAN, getDialog(), isWizard());
						}else{
							requestBankInfoByIBANInternational(mAccountIBAN, getDialog(), isWizard());
						}
					}else{
						if (isWizard()){
							showBankDetailsExDialog(isWizard());
						}else{
							showAccountNumber();
							showIban();
							getDialog().dismiss();
						}
							
					}
				}
			});
		}
	}
	
	
	/*************************************************************************************************************
	 * 
	 * Wizard dialog fragment
	 * @author victor
	 *
	 *************************************************************************************************************/
	// TODO check if needs to be private
	@SuppressLint("ValidFragment")
	private class WizardDialogFragment extends DialogFragment {
	
		protected boolean mIsWizard = true;

		public boolean isWizard() {
			return mIsWizard;
		}

		public void setIsWizard(boolean isWizard) {
			mIsWizard = isWizard;
		}

	}
	
	
	
	
	private boolean isArrayEmpty(String[] array){
		if (array == null) return true;
		for(String s : array) {
			if(s != null && s.length() > 0) {
				return false;
			}
		}
		return true;
	}

	private String[] filterEmptyStrings(String[] in) {
		ArrayList<String> nonEmptyStrings = new ArrayList<String>();
		for(String i : in) {
			if(i != null && !i.trim().equals("")) {
				nonEmptyStrings.add(i);
			}
		}
		return nonEmptyStrings.toArray(new String[nonEmptyStrings.size()]);
	}

	@Override
	public void errorTextSet() {
		mTextError.setText(mErrorText);
		mTextError.setVisibility(TextUtils.isEmpty(mErrorText) ? View.GONE : View.VISIBLE);
	}

	@Override
	public void readOnlyStateSet() {
		show();
	}

	public String [] getBeneficiary() {		
		return mBeneficiary;
	}

	public void setBeneficiary(String[] bankDetails) {
		mBeneficiary = bankDetails;
		showBeneficiaryDetails();
	}

	public String getPostAccountNumber() {
		return mAccountPost;
	}

	public void setPostAccountNumber(String accountNumber) {
		mAccountPost = accountNumber;		
	}

	public void setAccountIBAN(String accountIBAN) {
		mAccountIBAN = accountIBAN;
		showIban();
	}

	
	public String [] getBankDetails() {
		return mBankDetails;
	}

	public void setBankDetails(String [] bankDetails) {
		mBankDetails = bankDetails;
		showBankDetails();
	}
	
	public BeneficiaryFieldPaymentType getSelectAccountType() {
		return mPaymentType;
	}

	public void setPaymentType(BeneficiaryFieldPaymentType selectAccountType) {
		mPaymentType = selectAccountType;
	}
	
	public String getAccount() {
		return mAccount;
	}

	public String getNtnl() {
		return mBankNtnl;
	}
	
	public void setNtnl(String ntnl) {
		mBankNtnl = ntnl;
	}
	
	public String getBIC(){
		return mBankBIC;
	}

	public void setAccount(String account) {
		mAccount = account;
		showAccountNumber();
	}

	public String getAccountIBAN() {
		return mAccountIBAN;
	}
	
	public void setCountries(List<CountryTO> countries) {
		
		Comparator <CountryTO> comp = new Comparator<CountryTO>() {

			@Override
			public int compare(CountryTO lhs, CountryTO rhs) {
				return lhs.getName().compareTo(rhs.getName());				
			}
		};
		
		Collections.sort(countries, comp);
		
		mCountries = countries;
	}
	
	public PaymentSubType getPaymentSubType() {
		return mPaymentSubType;
	}	

	public CountryTO getCountry() {
		return mCountry;
	}

	public void setCountry(CountryTO mCountry) {
		this.mCountry = mCountry;
	}
	
	public Long getCountryId() {
		return mCountryId;
	}

	public void setCountryId(Long mCountryId) {
		this.mCountryId = mCountryId;
		
		if (mCountryId == null) return;
		
		// find the countryTO object by Id and assign to mCountry
		for (CountryTO country: mCountries){
			if (country.getId().longValue() == mCountryId){
				mCountry = country;
				break;
			}
		}
	}
	
	public String getmBankBIC() {
		return mBankBIC;
	}

	public void setmBankBIC(String mBankBIC) {
		this.mBankBIC = mBankBIC;
	}
	
	public ArrayList<BeneficiaryTO> getListByPaymentType(List<BeneficiaryTO> beneficiaries, PaymentType paymentType){
		ArrayList<BeneficiaryTO> returnList = new ArrayList<BeneficiaryTO>();
		
		for(BeneficiaryTO beneficiary: beneficiaries){
			if (beneficiary.getPaymentType() == paymentType){
				returnList.add(beneficiary);
			}
		}
		return returnList;		
	}
	
	public boolean isSet(){
		return mIsSet;
	}
}
