package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;

import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderAction;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.R;

public class DomesticPaymentTemplateActivity extends DomesticPaymentAbstractActivity{
	
	@Override
	List<ButtonDef> getButtonDefs() {
		
		List<ButtonDef> buttonList = new ArrayList<ButtonDef>();
		final PaymentUtil util = new PaymentUtil(this);
		
		if (getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE_FROM_VIEW || getViewDataMethod().getViewType() == PaymentViewType.TEMPLATE_NEW){
			// add only confirm button
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.CONFIRM, OrderType.TEMPLATE);
				}
				@Override
				public void onClick() {
	                savePaymentTemplate();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}else{
			// add 3 buttons, Update, Create payment and delete
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.EDIT, OrderType.TEMPLATE);
				}
				@Override
				public void onClick() {                
	                savePaymentTemplate();
	            }
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
			
			
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.DELETE, OrderType.TEMPLATE);
				}
				@Override
				public void onClick() {
					deletePaymentTemplate();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.WARNING;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
			
			
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return R.string.pmt_template_create_payment;
				}
				@Override
				public void onClick() {
					Intent i = new Intent(DomesticPaymentTemplateActivity.this, DomesticPaymentActivity.class);
					i.putExtra(DomesticPaymentActivity.EXTRA_ID, mExtraId);				
					i.putExtra(DomesticPaymentActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.NEW_FROM_TEMPLATE);
					startActivity(i);
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}
				
		return buttonList;
	}
	
}
