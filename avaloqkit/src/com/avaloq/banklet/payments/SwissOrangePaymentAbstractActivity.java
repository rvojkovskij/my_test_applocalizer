package com.avaloq.banklet.payments;

import java.util.List;

import com.avaloq.afs.aggregation.to.payment.PaymentDefaults;
import com.avaloq.afs.aggregation.to.payment.SwissPaymentDefaultsResult;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentOrderTO;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentResult;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentTemplateResult;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangePaymentTemplateTO;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangeStandingOrderTO;
import com.avaloq.afs.aggregation.to.payment.orange.SwissOrangeStandingPaymentResult;
import com.avaloq.afs.server.bsp.client.ws.PaymentSubType;
import com.avaloq.afs.server.bsp.client.ws.PaymentType;
import com.avaloq.afs.server.bsp.client.ws.SwissOrangePaymentSlipTO;
import com.avaloq.banklet.payments.methods.ConfirmMethod;
import com.avaloq.banklet.payments.methods.TemplateMethod;
import com.avaloq.banklet.payments.methods.SubmissionResultMethod;
import com.avaloq.banklet.payments.methods.VerifyMethod;
import com.avaloq.banklet.payments.methods.VerifyWithSubmissionMethod;
import com.avaloq.banklet.payments.methods.ViewDataMethod;
import com.avaloq.banklet.payments.views.BeneficiaryField.BeneficiaryFieldPaymentType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissOrangePaymentRequest;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissOrangePaymentService;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissOrangePaymentTemplateRequest;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissOrangeStandingPaymentRequest;
import com.avaloq.framework.comms.webservice.swissorangepayment.SwissPaymentDefaultsRequest;

public abstract class SwissOrangePaymentAbstractActivity 
	extends AbstractPaymentActivity<
		SwissOrangePaymentResult, SwissOrangePaymentRequest, SwissOrangePaymentOrderTO, SwissOrangePaymentSlipTO,
		SwissOrangeStandingPaymentResult, SwissOrangeStandingPaymentRequest, SwissOrangeStandingOrderTO,
		SwissOrangePaymentTemplateResult, SwissOrangePaymentTemplateRequest, SwissOrangePaymentTemplateTO,
		SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest
	> {

	/**
	 * Override in child classes if something special is needed for the header
	 */
	@Override
	HeaderData getHeaderData() {
		return new HeaderData() {

			@Override
			public int getIconResId() {
				return R.drawable.pmt_new_slip_orange;
			}

			@Override
			public String getTitle() {				
				return getResources().getString(R.string.pmt_payment_type_swiss_orange_payment_slip);
			}
			
			@Override
			public String getSubtitle() {
				switch (getViewDataMethod().getViewType()){
					case NEW:					
					case NEW_FROM_TEMPLATE:					
					case NEW_FROM_VIEW:
					case VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
					case TEMPLATE:					
					case TEMPLATE_FROM_VIEW:
						return getResources().getString(R.string.pmt_payment_subtype_template);
					default:					
						return getResources().getString(R.string.pmt_payment_subtype_single_payment);					
				}	
			}
		};
	}

	/**
	 * Depending on what kind of data is available (existing order or a template), it will
	 * set the available values for the fields
	 */
	@Override
	public void populateFields() {

		mFieldBeneficiary.setPaymentType(BeneficiaryFieldPaymentType.ORANGE);
		mFieldAmount.setCurrencies(getViewDataMethod().getDefaults().getPaymentDefaults().getPaymentCurrencies());

		// If there is some template data available, put it in the fields
		// In confirm mode, do not overwrite the values in the fields
		if (getViewDataMethod().getTemplateResult() != null && !isConfirmMode) {
			mFieldBeneficiary.setPostAccountNumber(getViewDataMethod().getTemplateResult().getSwissOrangePaymentTemplate().getSwissOrangePayment().getPcAccount());
			mFieldBeneficiary.showPostAccountNumber();
			mFieldReferenceNumber.setReferenceNumber(getViewDataMethod().getTemplateResult().getSwissOrangePaymentTemplate().getSwissOrangePayment().getReferenceNumber());
		}
		
		SwissOrangePaymentSlipTO paymentSlip = getViewDataMethod().getPaymentFromRequest();
		
		// In confirm mode, do not overwrite the values in the fields
		if (paymentSlip != null && !isConfirmMode) {
			mFieldBeneficiary.setPostAccountNumber(paymentSlip.getPcAccount());
			mFieldBeneficiary.showPostAccountNumber();
			mFieldReferenceNumber.setReferenceNumber(paymentSlip.getReferenceNumber());
		}
		
		setValuesAfterScanning();

	}

	abstract List<ButtonDef> getButtonDefs();

	@Override
	ContentData getContentData() {
		return new ContentData() {

			public boolean hasDebitMoneyAccount() {
				return true;
			}

			@Override
			public boolean hasBeneficiaryBankDetails() {
				return true;
			}

			@Override
			public boolean hasBeneficiary() {
				return true;
			}

			public boolean hasAccountNumber() {
				return true;
			}

			@Override
			public boolean hasReferenceNumber() {
				return true;
			}

			@Override
			public boolean hasAmountField() {
				return true;
			}

			@Override
			public boolean hasSalaryPayment() {
				return false;
			}

			@Override
			public boolean hasExecutionDateField() {
				return true;
			}

			@Override
			public boolean hasChargeOptionType() {
				return false;
			}

			@Override
			public boolean hasStandingOrderField() {
				return true;
			}

			@Override
			public boolean hasSaveAsTemplateField() {
				return getViewDataMethod().hasSaveAsTemplateField();
			}

			@Override
			public boolean hasDebitAdviceField() {
				return true;
			}

			@Override
			public boolean hasPaymentReasonField() {
				return false;
			}

			@Override
			public boolean hasCreditMoneyAccount() {
				return false;
			}

			@Override
			public boolean hasScanField() {
				return true;
			}
		};
	}
	
	public SubmissionResultMethod<SwissOrangePaymentResult, SwissOrangePaymentRequest, SwissOrangeStandingPaymentResult, SwissOrangeStandingPaymentRequest> getSubmissionResultMethod(){
		return new SubmissionResultMethod<SwissOrangePaymentResult, SwissOrangePaymentRequest, SwissOrangeStandingPaymentResult, SwissOrangeStandingPaymentRequest>(SwissOrangePaymentAbstractActivity.this) {
			@Override
			public SaveTemplateType getSaveTemplateType() {
				return SaveTemplateType.ORANGE;
			}
		};
	}
	
	public VerifyMethod<SwissOrangePaymentOrderTO, SwissOrangePaymentRequest, SwissOrangeStandingOrderTO, SwissOrangeStandingPaymentRequest> getVerifyMethod(){
		return new VerifyWithSubmissionMethod<SwissOrangePaymentOrderTO, SwissOrangePaymentRequest, SwissOrangeStandingOrderTO, SwissOrangeStandingPaymentRequest>(this){
			@Override
			protected SwissOrangePaymentRequest getPaymentRequest(SwissOrangePaymentOrderTO order, RequestStateEvent<SwissOrangePaymentRequest> rse) {
				return SwissOrangePaymentService.verifyPayment(order, rse);
			}

			@Override
			protected SwissOrangeStandingPaymentRequest getStandingRequest(SwissOrangeStandingOrderTO order, RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
				return SwissOrangePaymentService.verifyStandingPayment(order, rse);
			}
		};
	}
	
	public ConfirmMethod<SwissOrangePaymentOrderTO, SwissOrangePaymentRequest, SwissOrangeStandingOrderTO, SwissOrangeStandingPaymentRequest> getConfirmMethod(){
		return new ConfirmMethod<SwissOrangePaymentOrderTO, SwissOrangePaymentRequest, SwissOrangeStandingOrderTO, SwissOrangeStandingPaymentRequest>(this) {

			@Override
			protected SwissOrangePaymentRequest getPaymentRequest(SwissOrangePaymentOrderTO order, RequestStateEvent<SwissOrangePaymentRequest> rse) {
				return SwissOrangePaymentService.submitPayment(order, rse);
			}

			@Override
			protected SwissOrangeStandingPaymentRequest getStandingRequest(SwissOrangeStandingOrderTO order, RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
				return SwissOrangePaymentService.submitStandingPayment(order, rse);
			}
		};
	}
	
	@Override
	public ViewDataMethod<
		SwissOrangePaymentResult, SwissOrangePaymentRequest, SwissOrangePaymentOrderTO, SwissOrangePaymentSlipTO,
		SwissOrangeStandingPaymentResult, SwissOrangeStandingPaymentRequest, SwissOrangeStandingOrderTO,
		SwissOrangePaymentTemplateResult, SwissOrangePaymentTemplateRequest, SwissOrangePaymentTemplateTO,
		SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest
	> createViewDataMethod() {
		return new ViewDataMethod
		<
			SwissOrangePaymentResult, SwissOrangePaymentRequest, SwissOrangePaymentOrderTO, SwissOrangePaymentSlipTO,
			SwissOrangeStandingPaymentResult, SwissOrangeStandingPaymentRequest, SwissOrangeStandingOrderTO,
			SwissOrangePaymentTemplateResult, SwissOrangePaymentTemplateRequest, SwissOrangePaymentTemplateTO,
			SwissPaymentDefaultsResult, SwissPaymentDefaultsRequest
		>(this){

			@Override
			protected SwissOrangePaymentRequest getPaymentRequest(long paymentId, RequestStateEvent<SwissOrangePaymentRequest> rse) {
				return SwissOrangePaymentService.getPayment(paymentId, rse);
			}

			@Override
			protected SwissOrangeStandingPaymentRequest getStandingRequest(long paymentId, RequestStateEvent<SwissOrangeStandingPaymentRequest> rse) {
				return SwissOrangePaymentService.getStandingPayment(paymentId, rse);
			}

			@Override
			protected SwissOrangePaymentTemplateRequest getTemplateRequest(long paymentId, RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
				return SwissOrangePaymentService.getPaymentTemplate(paymentId, rse);
			}

			@Override
			protected SwissPaymentDefaultsRequest getDefaultsRequest(RequestStateEvent<SwissPaymentDefaultsRequest> rse) {
				return SwissOrangePaymentService.getDefaults(rse);
			}

			@Override
			protected SwissOrangePaymentOrderTO getPaymentOrder(SwissOrangePaymentResult result) {
				return result.getSwissOrangePaymentOrder();
			}

			@Override
			protected SwissOrangeStandingOrderTO getStandingOrder(SwissOrangeStandingPaymentResult result) {
				return result.getSwissOrangeStandingOrderTO();
			}

			@Override
			protected SwissOrangePaymentTemplateTO getPaymentTemplate(SwissOrangePaymentTemplateResult result) {
				return result.getSwissOrangePaymentTemplate();
			}

			@Override
			protected SwissOrangePaymentOrderTO createEmptyPaymentOrder() {
				return new SwissOrangePaymentOrderTO();
			}

			@Override
			protected SwissOrangeStandingOrderTO createEmptyStandingOrder() {
				return new SwissOrangeStandingOrderTO();
			}

			@Override
			protected SwissOrangePaymentSlipTO getSlipFromPaymentOrder(SwissOrangePaymentOrderTO result) {
				return result.getSwissOrangePayment();
			}

			@Override
			protected SwissOrangePaymentSlipTO getSlipFromStandingOrder(SwissOrangeStandingOrderTO result) {
				return result.getSwissOrangePayment();
			}

			@Override
			protected PaymentDefaults getPaymentDefaults(SwissPaymentDefaultsResult template) {
				return template.getPaymentDefaults();
			}

			@Override
			protected SwissOrangePaymentSlipTO getSlipFromTemplate(SwissOrangePaymentTemplateTO template) {
				return template.getSwissOrangePayment();
			}

			@Override
			public SwissOrangePaymentSlipTO createEmptyPaymentSlip() {
				return new SwissOrangePaymentSlipTO();
			}
			
		};
	}
	
	@Override
	public TemplateMethod<SwissOrangePaymentSlipTO, SwissOrangePaymentTemplateResult, SwissOrangePaymentTemplateRequest, SwissOrangePaymentTemplateTO> getTemplateMethod() {
		return new TemplateMethod<SwissOrangePaymentSlipTO, SwissOrangePaymentTemplateResult, SwissOrangePaymentTemplateRequest, SwissOrangePaymentTemplateTO>(this){

			@Override
			protected void setPayment(SwissOrangePaymentTemplateTO template, SwissOrangePaymentSlipTO payment) {
				template.setPayment(payment);
			}

			@Override
			protected SwissOrangePaymentTemplateRequest getVerifyRequest(SwissOrangePaymentTemplateTO template, RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
				return SwissOrangePaymentService.verifyPaymentTemplate(template, rse);
			}

			@Override
			protected SwissOrangePaymentTemplateRequest getSubmitRequest(SwissOrangePaymentTemplateTO template, RequestStateEvent<SwissOrangePaymentTemplateRequest> rse) {
				return SwissOrangePaymentService.savePaymentTemplate(template, rse);
			}

			@Override
			protected SwissOrangePaymentTemplateTO createEmptyTemplate() {
				return new SwissOrangePaymentTemplateTO();
			}
			
		};
	}
	
	/**
	 * Returns the payment object by reading the values in the fields
	 * @return
	 */
	public SwissOrangePaymentSlipTO fillPayment(SwissOrangePaymentSlipTO paymentSlip){
		
		if (mFieldBeneficiary.getPaymentSubType() == PaymentSubType.POSTCHECK_ACCOUNT){
			paymentSlip.setBeneficiary1(mFieldBeneficiary.getBankDetails()[0]);
			paymentSlip.setBeneficiary2(mFieldBeneficiary.getBankDetails()[1]);
			paymentSlip.setBeneficiary3(mFieldBeneficiary.getBankDetails()[2]);
			paymentSlip.setBeneficiary4(mFieldBeneficiary.getBankDetails()[3]);
			
			paymentSlip.setBeneficiaryBank1("");
			paymentSlip.setBeneficiaryBank2("");
			paymentSlip.setBeneficiaryBank3("");
			paymentSlip.setBeneficiaryBank4("");
		}
		paymentSlip.setPcAccount(mFieldBeneficiary.getPostAccountNumber());
		paymentSlip.setReferenceNumber(mFieldReferenceNumber.getReferenceNumber());
		paymentSlip.setPaymentType(PaymentType.SWISS_ORANGE_PAYMENT_SLIP);
		paymentSlip.setPaymentSubType(mFieldBeneficiary.getPaymentSubType());

		return paymentSlip;
	}
	
	@Override
	public SwissOrangePaymentOrderTO getPaymentOrder() {
		SwissOrangePaymentOrderTO order = getViewDataMethod().getPaymentOrder();
		order.setPayment(getPayment());
		return order;
	}
	
	@Override
	public SwissOrangeStandingOrderTO getStandingOrder() {
		SwissOrangeStandingOrderTO order = getViewDataMethod().getStandingOrder();
		order.setPayment(getPayment());
		return order;
	}
	
	@Override
	public PaymentType getPaymentType(){
		return PaymentType.SWISS_ORANGE_PAYMENT_SLIP;
	}
}
