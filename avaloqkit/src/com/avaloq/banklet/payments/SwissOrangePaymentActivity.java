package com.avaloq.banklet.payments;

import java.util.ArrayList;
import java.util.List;

import com.avaloq.banklet.payments.util.PaymentUtil;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderAction;
import com.avaloq.banklet.payments.util.PaymentUtil.OrderType;
import com.avaloq.framework.R;

public class SwissOrangePaymentActivity extends SwissOrangePaymentAbstractActivity {	
	
	OrderType orderType = OrderType.PAYMENT;
	
	@Override
	List<ButtonDef> getButtonDefs() {
		
		List<ButtonDef> buttonList = new ArrayList<ButtonDef>();
		final PaymentUtil util = new PaymentUtil(this);
		if (isViewFromStanding())
			orderType = OrderType.STANDING;
		
		if (isEditable()){
		
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.VERIFY, orderType);
				}
				@Override
				public void onClick() {
					verifyPayment();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {					
					return VERIFY_BUTTON_TAG;
				}
			});
		}else{
			buttonList.add(new ButtonDef() {
				@Override
				public int getTextResId() {
					return util.getPaymentStringId(OrderAction.CONFIRM, orderType);
				}
				@Override
				public void onClick() {
					confirmPayment();
				}
				@Override
				public ButtonType getType() {
					return ButtonType.PRIMARY;
				}
				@Override
				public String getTag() {
					// TODO Auto-generated method stub
					return null;
				}
			});
		}

		return buttonList;
	}
	
}
