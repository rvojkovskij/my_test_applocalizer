package com.avaloq.banklet.payments;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfo;
import com.avaloq.afs.aggregation.to.payment.PaymentTemplateInfoListResult;
import com.avaloq.afs.server.bsp.client.ws.PaymentTemplateQueryTO;
import com.avaloq.banklet.payments.AbstractPaymentActivity.PaymentViewType;
import com.avaloq.banklet.payments.adapter.PaymentTemplateFullAdapter;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentOverviewService;
import com.avaloq.framework.comms.webservice.paymentoverview.PaymentTemplateInfoListRequest;
import com.avaloq.framework.ui.BankletActivity;

public class TemplatesActivity extends BankletActivity{
	
	private PaymentTemplateFullAdapter mAdapter;
	
	/*@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pmt_templates_fragment);
		final ListView list = (ListView)findViewById(R.id.templates_list);

		PaymentTemplateQueryTO query = new PaymentTemplateQueryTO();
		query.setFavorite(false);
		PaymentOverviewService.getPaymentTemplates(query, 0l, Long.valueOf(Integer.MAX_VALUE), new RequestStateEvent<PaymentTemplateInfoListRequest>() {
			@Override
			public void onRequestCompleted(PaymentTemplateInfoListRequest aRequest) {
				PaymentTemplateInfoListResult result = aRequest.getResponse().getData();
				List<PaymentTemplateInfo> templates = result.getPaymentTemplateInfoList();
				mAdapter = new PaymentTemplateFullAdapter(TemplatesActivity.this, templates);
				list.setAdapter(mAdapter);

				list.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						
						PaymentTemplateInfo template = ((PaymentTemplateFullAdapter)list.getAdapter()).getItem(position);
						Intent intent;
						switch(template.getPaymentType()) {
						case SWISS_ORANGE_PAYMENT_SLIP:
							intent = new Intent(TemplatesActivity.this, SwissOrangePaymentTemplateActivity.class);
							intent.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
							intent.putExtra(SwissOrangePaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
							startActivity(intent);
							break;
						case SWISS_RED_PAYMENT_SLIP:
							intent = new Intent(TemplatesActivity.this, SwissRedPaymentTemplateActivity.class);
							intent.putExtra(SwissRedPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
							intent.putExtra(SwissRedPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
							startActivity(intent);
							break;
						case DOMESTIC_PAYMENT:
							intent = new Intent(TemplatesActivity.this, DomesticPaymentTemplateActivity.class);
							intent.putExtra(DomesticPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
							intent.putExtra(DomesticPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
							startActivity(intent);
							break;
						case INTERNAL_PAYMENT:
							intent = new Intent(TemplatesActivity.this, AccountTransferTemplateActivity.class);
							intent.putExtra(AccountTransferTemplateActivity.EXTRA_ID, template.getPaymentId());
							intent.putExtra(AccountTransferTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
							startActivity(intent);
							break;
						case INTERNATIONAL_PAYMENT:
							intent = new Intent(TemplatesActivity.this, InternationalPaymentTemplateActivity.class);
							intent.putExtra(InternationalPaymentTemplateActivity.EXTRA_ID, template.getPaymentId());
							intent.putExtra(InternationalPaymentTemplateActivity.EXTRA_PAYMENT_VIEW_TYPE, PaymentViewType.TEMPLATE);
							startActivity(intent);
							break;
						default:
							break;
						}
					}
				});
			}

			@Override
			public void onRequestFailed(PaymentTemplateInfoListRequest aRequest) {
				//TODO: Show Error
			}
		}).initiateServerRequest();
	}*/
}
