package com.avaloq.banklet.documentsafe;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import android.os.Bundle;

import com.avaloq.afs.aggregation.to.documents.DocumentCategory;
import com.avaloq.afs.aggregation.to.documents.DocumentCategoryListResult;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;

/**
 * Extends from the usual {@link TabbedBankletActivity}.
 * Fetches the data from the WS using the model and creates tabs for each document category.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class MainActivity extends TabbedBankletActivity {
	
	@Override
	public void prepareNavigationItems() {
		
		// Get the model instance
		final DocumentSafeModel documentSafeModel = DocumentSafeBanklet.getInstance().getModel();				
		
		// On update, show the categories as NavigationItems
		documentSafeModel.addObserver(new Observer() {
			@Override
			public void update(Observable observable, Object data) {
								
				// find the categories
				final DocumentCategoryListResult result = documentSafeModel.getCategoryList();
				final List<DocumentCategory> categories = result.getDocumentCategoryList();
				FragmentNavigationItem[] navigationItems = new FragmentNavigationItem[categories.size()];
				
				// creates an array of navigation items
				for(int i = 0;i < categories.size();i++) {
					DocumentCategory category = categories.get(i);
					Bundle bundle = new Bundle();
					bundle.putInt(Fragment.EXTRA_CATEGORY_INDEX, i);
					navigationItems[i] = new FragmentNavigationItem(Fragment.class, category.getName(), bundle);
				}
				// shows the navigation items
				showNavigationItems(navigationItems);
			}
		});
		documentSafeModel.loadData();
	}
		
}
