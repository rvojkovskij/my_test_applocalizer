package com.avaloq.banklet.documentsafe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Intent;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.avaloq.framework.DownloadClickListener;

import com.avaloq.afs.aggregation.to.documents.DocumentDescription;
import com.avaloq.framework.R;


class ListAdapter extends ArrayAdapter<DocumentDescription>{
	
	private List<DocumentListEntry> mResults = new ArrayList<DocumentListEntry>();
	
	/**
	 * The fragment using this adapter
	 */
	private final Fragment mDocumentSafeFragment;
	
	/**
	 * The List of documents
	 */
	private List<DocumentDescription> mDocumentList = null;
	
	/**
	 * Creates a new Instance of the list Adapter
	 * @param aDocumentSafeFragment The reference to the fragment
	 * @param documentList The List of Documents
	 */	
	public ListAdapter(final Fragment aDocumentSafeFragment, List<DocumentDescription> documentList) {
		super(aDocumentSafeFragment.getActivity(), R.layout.doc_list_item, documentList);
		mDocumentSafeFragment = aDocumentSafeFragment;
		this.mDocumentList = documentList;
		organizeData();
	}
	
	private void organizeData(){
		
		mResults.clear();

		/*
		 * Loop through the results and split to buckets for distinct
		 * YearMonth combinations
		 */
		SparseArray<List<DocumentDescription>> hashMap = new SparseArray<List<DocumentDescription>>();

		// save the keys to a list. We will sort this list later
		List<Integer> keyList = new ArrayList<Integer>();
		
		Collections.reverse(mDocumentList);

		for (DocumentDescription document : mDocumentList) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(document.getDisplayDate());
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH);

			// calculate the key for the bucket
			Integer key = year * 100 + month; // just get a number like
			// 2012*100+11=201211, which
			// we will use for sorting

			// store the key
			if (!keyList.contains(key)) {
				keyList.add(key);
			}

			List<DocumentDescription> bucket = new ArrayList<DocumentDescription>();

			// request the bucket contents using the key
			bucket = hashMap.get(key);

			if (bucket == null) {
				// we had no records at this key until now, then we add one
				bucket = new ArrayList<DocumentDescription>();

			}
			bucket.add(document);

			hashMap.put(key, bucket);
		}

		/*
		 * Sort the key list in descending order
		 */
		Collections.reverse(keyList);

		/*
		 * Add all the records to the main list, including the separators
		 */

		for (Integer key : keyList) {

			// get the entries from the Hashmap
			List<DocumentDescription> bucket = hashMap.get(key);

			// add a record for the separator
			DocumentListEntry dle = new DocumentListEntry();
			dle.date = bucket.get(0).getDisplayDate();
			dle.document = null;
			mResults.add(dle);

			// add all the other records
			for (DocumentDescription documentDesc : bucket) {

				dle = new DocumentListEntry();
				dle.date = null;
				dle.document = documentDesc;
				mResults.add(dle);
			}
		}
	}
	
	/**
	 * TODO might be able to remove this...
	 * also the mDocumentList...
	 */
	@Override
	public int getCount() {
		return mResults.size();
	}
	
	/**
	 * For the ViewHolder pattern
	 */
	private static class ViewHolder {
		public TextView tvName;
		public TextView tvDescription;
		public TextView tvStatus;
		public TextView date;
		public TextView month;
	}
	
	@Override
	public View getView(final int index, View convertView, ViewGroup parent) {
		
		View row;
		DocumentListEntry listEntry = mResults.get(index);
		DocumentDescription item = listEntry.document;
				
		ViewHolder holder = new ViewHolder();

	   // not a grouping entry
		if (item != null){
			row = mDocumentSafeFragment.getActivity().getLayoutInflater().inflate(R.layout.doc_list_item, null);

			holder.tvName = (TextView) row.findViewById(R.id.banklet_doc_list_item_title);
			holder.tvDescription = (TextView) row.findViewById(R.id.banklet_doc_list_item_description);
			
			holder.date = (TextView) row.findViewById(R.id.trading_sell_documents_tvDate);
			holder.month = (TextView) row.findViewById(R.id.trading_sell_documents_tvMonth);
		}else{
			// grouping entry
			row = mDocumentSafeFragment.getActivity().getLayoutInflater().inflate(R.layout.doc_list_item_group, null);
			holder.date = (TextView) row.findViewById(R.id.trading_sell_transactions_tvDate);
		}

		
		row.setTag(holder);


		// this is not a grouping entry
		if (item != null){					
			// Pick the Document from the layout
			DocumentDescription document = item;

			// Put the values in the layout
			holder.tvName.setText(document.getDisplayName());
			final String description = document.getDisplayDescription();
			if (holder.tvDescription != null) {
				holder.tvDescription.setText(TextUtils.isEmpty(description) ? getContext().getString(R.string.documentsafe_no_description) : description);
			}
			
			
			if (document.isNewDocument()){	
				holder.tvStatus = (TextView) row.findViewById(R.id.banklet_doc_list_item_status_unread);
			}else{
				holder.tvStatus = (TextView) row.findViewById(R.id.banklet_doc_list_item_status_read);
			}
			
			holder.tvStatus.setVisibility(View.VISIBLE);
			holder.tvStatus.setText(document.isNewDocument() ? getContext().getString(R.string.documentsafe_new) : getContext().getString(R.string.documentsafe_read));
			
			// display the day and the month in the correct format
			Calendar calendar = Calendar.getInstance();		
			calendar.setTime(item.getDisplayDate());
			int date = calendar.get(Calendar.DATE);
			
			SimpleDateFormat formatter = new SimpleDateFormat("MMM", Locale.getDefault());
			holder.date.setText(Integer.toString(date));
		    holder.month.setText(formatter.format(item.getDisplayDate()));

			// Add the click listener
			DownloadClickListener downloadClickListener = new DownloadClickListener(mDocumentSafeFragment.getActivity(), document.getId()){
				@Override
				protected void displayDocument(Intent intent, int result_code) {
					mDocumentSafeFragment.startActivityForResult(intent, result_code);
				}

				@Override
				protected Runnable getOnSetPdfCallback() {
					return new Runnable() {
						@Override
						public void run() {
							mDocumentSafeFragment.setPdfFile(getPdfFile());
						}
					};
				}
			};
			row.setOnClickListener(downloadClickListener);
			row.findViewById(R.id.documentsafe_list_item).setOnClickListener(downloadClickListener);
		}else{
			// this is a grouping row, just display the date			
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM, yyyy", Locale.getDefault());
			holder.date.setText(formatter.format(listEntry.date));
			row.setClickable(false);
//			row.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {
//				}
//			});
		}
		return row;
	}

	
	class DocumentListEntry{
		DocumentDescription document;
		Date date;
	}
}