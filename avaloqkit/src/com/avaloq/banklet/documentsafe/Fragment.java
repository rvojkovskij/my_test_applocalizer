package com.avaloq.banklet.documentsafe;

import java.io.File;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.avaloq.afs.aggregation.to.documents.DocumentCategory;
import com.avaloq.framework.DownloadClickListener;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class Fragment extends BankletFragment {	

	private static final String TAG = Fragment.class.getSimpleName();

	/**
	 * The extra used to specify the index of the category in the model.
	 */
	public static String EXTRA_CATEGORY_INDEX = "extra_category_index";
	
	private File pdfFile = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.doc_activity_list, container, false);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {				
		super.onActivityCreated(savedInstanceState);
		
		// fetch the document category
		int categoryIndex = getArguments().getInt(EXTRA_CATEGORY_INDEX, 0);
		
		//DocumentCategory category = Banklet.getInstance().    get.getDescriptionList().getDocumentCategoryList().get(categoryIndex); 
		DocumentCategory category = DocumentSafeBanklet.getInstance().getModel().getCategoryList().getDocumentCategoryList().get(categoryIndex);
		
		// find the listview, add the adapter.
		ListView listView = (ListView)getView().findViewById(R.id.documentsafe_list);
		listView.setAdapter(new ListAdapter(this, category.getDocumentDescriptionList()));
	}
	
	public File getPdfFile(){
		return pdfFile;
	}
	
	public void setPdfFile(File file){
		pdfFile = file;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode) {
			case DownloadClickListener.RESULT_PDF:
				if(pdfFile != null) {
					Log.d(TAG, "Deleting PDF-File...");
					pdfFile.delete();
				}
			default:
				super.onActivityResult(requestCode, resultCode, data);
		}
		
	}

}