package com.avaloq.banklet.documentsafe;

import com.avaloq.afs.aggregation.to.documents.DocumentCategoryListResult;
import com.avaloq.afs.aggregation.to.documents.DocumentDescriptionListResult;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.document.DocumentCategoryListRequest;
import com.avaloq.framework.comms.webservice.document.DocumentDescriptionListRequest;
import com.avaloq.framework.comms.webservice.document.DocumentService;
import com.avaloq.framework.ui.DialogActivity;

/**
 * The model to share the document list between fragments.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class DocumentSafeModel extends java.util.Observable {

	private DocumentDescriptionListResult mDescriptionList = null;
	private DocumentCategoryListResult mCategoryList = null;


	/**
	 * Loads the data using the {@link DocumentDescriptionListRequest}.
	 */
	public void loadData() {
		DocumentCategoryListRequest documentCategoryListRequest = DocumentService.getDocumentCategories(new RequestStateEvent<DocumentCategoryListRequest>() {
			@Override
			public void onRequestCompleted(DocumentCategoryListRequest aRequest) {
				mCategoryList = aRequest.getResponse().getData();
				
					DocumentSafeModel.this.setChanged();
					DocumentSafeModel.this.notifyObservers(mCategoryList);
				
			}
			
			@Override
			public void onRequestFailed(DocumentCategoryListRequest aRequest) {
				DialogActivity.showNetworkError();
			}
		});
		documentCategoryListRequest.initiateServerRequest();		
	}

	/**
	 * Accessor for the Result of the Request
	 * @return The result from the webservice
	 */
	public DocumentDescriptionListResult getDescriptionList() {
		return mDescriptionList;
	}
	
	public DocumentCategoryListResult getCategoryList(){
		return mCategoryList;
	}

}