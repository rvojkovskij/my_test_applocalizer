package com.avaloq.banklet.documentsafe;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.Banklet;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.ActivityNavigationItem;

/**
 * Banklet class for Document Safe
 * @author Timo Schmid <t.schmid@insign.ch>
 *
 */
public class DocumentSafeBanklet extends Banklet {
	
	private DocumentSafeModel mModel = new DocumentSafeModel();
	
	/**
	 * The array of navigation items for this banklet
	 */
	private static ActivityNavigationItem[] items = {
		new ActivityNavigationItem(AvaloqApplication.getContext(), MainActivity.class, R.string.doc_banklet_name, R.attr.IconDocumentSafe, R.style.Theme_Banklet_DOC)
	};
	
	/**
	 * Get the document safe local model
	 * @return
	 */
	public DocumentSafeModel getModel() {
		return mModel;
	}

	@Override
	public void onCreate() {
	}

	@Override
	public ActivityNavigationItem getInitialActivity() {
		return items[0];
	}

	@Override
	public ActivityNavigationItem[] getMainNavigationItems() {
		return items;
	}

	@Override
	public void onEmptyCache() {
		
		// Empty the local model
		mModel = new DocumentSafeModel();		
	}
	
	/**
	 * Get the DocumentSafeBanklet singleton instance
	 * 
	 * @return
	 */
	public static DocumentSafeBanklet getInstance() {
		return (DocumentSafeBanklet) Banklet.getInstanceOf(DocumentSafeBanklet.class);
	}

}
