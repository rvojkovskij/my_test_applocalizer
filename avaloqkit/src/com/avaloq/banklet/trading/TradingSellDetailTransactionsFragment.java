/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */
package com.avaloq.banklet.trading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.banklet.trading.TradingSellDetailMarketDataFragment.DetailMarketDataFragmentInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class TradingSellDetailTransactionsFragment extends BankletFragment {

	public interface DetailTransactionsFragmentInterface{
		public void onClick_ButtonSell();
		public void onClick_ButtonBuy();
		public ButtonSellBuyStatus getSellBuyButtonStatus();
		public String getInstrumentName();
		public String getInstrumentType();
		public String getInstrumentSubType();
		public Long getBankingPositionId();
		public Long getCurrencyId();
		public String getMarketName();
	}
	
	private static String instrumentName;
	private static String instrumentType;
//	private Integer instrumentId;
	private static String instrumentSubType;
	private static Long bankingPositionId;	
	private static String marketName;
	private Long listingCurrencyId;	
	
	private TradingSellTransactionsListAdapter mTransactionsAdapter; 
	private ListView mResultList;
	
	View mFragmentView;
	
//	private static String TAG = TradingSellDetailTransactionsFragment.class.getSimpleName();
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.trd_sell_detail_transactions_fragment, container, false);
		return mFragmentView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		/*
		 * Get the extra parameters from the calling activity
		 */
		try{
			instrumentName =  ((DetailTransactionsFragmentInterface) getActivity()).getInstrumentName();
			instrumentType = ((DetailTransactionsFragmentInterface) getActivity()).getInstrumentType();
			instrumentSubType = ((DetailTransactionsFragmentInterface) getActivity()).getInstrumentSubType();
			bankingPositionId = ((DetailTransactionsFragmentInterface) getActivity()).getBankingPositionId();			
			
			marketName = ((DetailTransactionsFragmentInterface) getActivity()).getMarketName();
			listingCurrencyId = ((DetailTransactionsFragmentInterface) getActivity()).getCurrencyId();
			
			((TextView) getView().findViewById(R.id.tvTitle)).setText(instrumentName);
			((TextView) getView().findViewById(R.id.tvInstrumentType)).setText(instrumentType);
			((TextView) getView().findViewById(R.id.tvInstrumentSubType)).setText(instrumentSubType);
						
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		mTransactionsAdapter = new TradingSellTransactionsListAdapter(this);		
		mResultList = (ListView) getView().findViewById(R.id.trading_sell_detail_transactions_list);
		mResultList.addHeaderView(mTransactionsAdapter.getHeaderView());
		mResultList.setAdapter(mTransactionsAdapter);
		
		mTransactionsAdapter.getDataFromServer(bankingPositionId);
						
		setBuyOrSellButton();
				
		setCorrectIcon();
		
		if (				
				!(((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY || ((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.TRANSACTIONS_ONLY)
					
				){	
			((TextView) mFragmentView.findViewById(R.id.tvInstrumentSubType)).setText(getString(R.string.trading_header_isin)+ " "+instrumentSubType+"\n"+marketName+" / "+AvaloqApplication.getInstance().findCurrencyById(listingCurrencyId).getIsoCode());
		}								
	}
	
	/**
	 * Sets the correct text for the button in the header (Buy/Sell) and attaches the on click event
	 */
	private void setBuyOrSellButton(){
		
		if (((DetailTransactionsFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
			// must set the button to Buy
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setText(R.string.trading_buy);
		}else if (((DetailTransactionsFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.SELL){
			// must set the button as Sell
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setText(R.string.trading_sell);
		}else{
			// hide the button
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setVisibility(View.INVISIBLE);
		}
		
		// attach click listener
		((Button) getView().findViewById(R.id.trading_detail_btnSell)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (((DetailTransactionsFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
					((DetailMarketDataFragmentInterface) getActivity()).onClick_ButtonBuy(0);
				}else if (((DetailTransactionsFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.SELL){
					((DetailMarketDataFragmentInterface) getActivity()).onClick_ButtonSell();
				}else{
					// do nothing, the button must not be visible in this case
				}
			}
		});		
	}
	
	/**
	 * Sets the correct value for Bonds, Equities or funds
	 */
	private void setCorrectIcon(){		
		if (instrumentType.toUpperCase().compareTo("BONDS") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_bonds));			
		}else if (instrumentType.toUpperCase().compareTo("EQUITIES") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_equities));
		}else if (instrumentType.toUpperCase().compareTo("FUNDS") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_funds));
		}
	}
		
}
