package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.marketdata.InstrumentsResult;
import com.avaloq.afs.server.bsp.client.ws.InstrumentQueryTO;
import com.avaloq.afs.server.bsp.client.ws.InstrumentTO;
import com.avaloq.afs.server.bsp.client.ws.MdsInstrumentKeyType;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.marketdata.InstrumentsRequest;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataService;
import com.avaloq.framework.tools.ProgressiveBaseAdapter;

public abstract class BuyAdapter extends ProgressiveBaseAdapter {

	protected static final String TAG = "BuyAdapter";

	private List<InstrumentTO> mResults = new ArrayList<InstrumentTO>();
	private BuyFragment mFragment;

	private String mCurrentRequestIdentifier = "";
	private boolean mRefreshResults = true;
	
	static class ViewHolder {
		public TextView name;
		public TextView ticker;
		public TextView isin;
		public TextView valor;
		public TextView instrumentType;
		public Long instrumentId;
		public String instrumentSubType;
		public Integer id;
		public Long currencyId;
		public Long marketId;
		public String marketName;
		public ImageView image;
		public boolean isMoneyTradable;
		public StexAssetGroupType stexAssetGroupType;
	}

	/**
	 * Constructor
	 * 
	 * @param context
	 */
	public BuyAdapter(BuyFragment activity) {
		super(activity.getActivity());
		this.mFragment = activity;
	}

	public void executeSearch(String search, boolean refresh, StexAssetGroupType instrumentType) {

		mRefreshResults = refresh;
		
		// Configure the instruments request
		InstrumentQueryTO query = new InstrumentQueryTO();
		query.setKeyValue(search);
		query.setKeyValueType(MdsInstrumentKeyType.ANY);
		query.setMaxResultSize(mResultPerPage+mResults.size());		
		query.setStexAssetGroup(instrumentType);

		// Define the callback
		RequestStateEvent<InstrumentsRequest> rse = new RequestStateEvent<InstrumentsRequest>() {
			@Override
			public void onRequestCompleted(InstrumentsRequest instrumentsRequest) {
				// if we expect a response, and if this is the correct response				
				if (!mCurrentRequestIdentifier.isEmpty() && mCurrentRequestIdentifier.compareTo(instrumentsRequest.getRequestIdentifier()) == 0){					
					receivedInstrumentResults(instrumentsRequest);
					mCurrentRequestIdentifier = "";
				}
				loadingStateChanged(false);
				showListLoading(false);
			}

			@Override
			public void onRequestFailed(InstrumentsRequest aRequest) {
				loadingStateChanged(false);
				showListLoading(false);
			}
		};

		// Initiate the request 
		InstrumentsRequest request = MarketDataService.getInstruments(
				query, 
				0L, 
				mRefreshResults ? Long.valueOf(mResultPerPage) : Long.valueOf((mResultPerPage+mResults.size())), rse);
		
		request.setRequestIdentifier(request.getRequestIdentifier() + " ('" + search + "')");
		
		mCurrentRequestIdentifier = request.getRequestIdentifier();
		
		// Add the request to the request queue
		request.initiateServerRequest();

		loadingStateChanged(true);
//		if (mResults.size() > 0){
//			showListLoading(true);	
//		}
	}

	public void cancelRequest(){
		mCurrentRequestIdentifier = "";
		loadingStateChanged(false);
		showListLoading(false);
	}

	/**
	 * Update the dataset and notify the listeners on the ui thread
	 * 
	 * @param aRequest
	 */
	protected void receivedInstrumentResults(final AbstractServerRequest<InstrumentsResult> aRequest) {

		InstrumentsResult instrumentResult = aRequest.getResponse().getData();
		if (instrumentResult != null) {

			List<InstrumentTO> instruments = instrumentResult.getInstruments();
			
			if (mRefreshResults){				
				reset();
				mRefreshResults = false;
			}

			mResults.clear();
			
			mResults.addAll(instruments);
			
			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return mResults.size();
	}

	@Override
	public Object getItem(int position) {
		return mResults.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		InstrumentTO item = (InstrumentTO) getItem(position);

		if (rowView == null) {
			LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
			rowView = inflater.inflate(R.layout.trd_buy_list_item, null);

			ViewHolder viewHolder = new ViewHolder();
			viewHolder.name = (TextView) rowView.findViewById(R.id.trd_buy_list_name);
			viewHolder.ticker = (TextView) rowView.findViewById(R.id.trd_buy_list_ticker);
			viewHolder.isin = (TextView) rowView.findViewById(R.id.trd_buy_list_isin);
			viewHolder.valor = (TextView) rowView.findViewById(R.id.trd_buy_list_valor);
			viewHolder.instrumentType = (TextView) rowView.findViewById(R.id.trd_buy_list_instrumenttype);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.trading_type_image);			
			
			rowView.setTag(viewHolder);
		}

		final ViewHolder holder = (ViewHolder) rowView.getTag();

		holder.instrumentId = item.getId();

		holder.isMoneyTradable = item.isMoneyDrivenTradingCapable();
		holder.currencyId = item.getDefaultListing().getCurrencyId();
		holder.marketId = item.getDefaultListing().getMarketId();
		holder.marketName = item.getDefaultListing().getMarketCode() == null ? item.getDefaultListing().getMarketName() : item.getDefaultListing().getMarketCode();

		holder.name.setText(item.getTitle());
		holder.ticker.setText(item.getSymbol());
		holder.isin.setText(item.getIsin());
		holder.valor.setText(item.getValor());
		
		holder.instrumentType.setText(item.getStexGroupType().toString().substring(0, 1).toUpperCase()+item.getStexGroupType().toString().toLowerCase().substring(1));
		holder.stexAssetGroupType = item.getStexGroupType();
		holder.instrumentSubType = item.getStexSubTypeName();

		if (getCorrectIcon(item.getStexGroupType()) != null) {
			holder.image.setImageDrawable(getCorrectIcon(item.getStexGroupType()));
		}

		// set the listener for the button
		if (rowView.findViewById(R.id.trading_list_btnBuy) != null) {
			rowView.findViewById(R.id.trading_list_btnBuy).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(mFragment.getActivity().getBaseContext(), PositionBuyActivity.class);

					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_TYPE, holder.stexAssetGroupType.toString());
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.isin.getText().toString());

					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);
					
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					
					mFragment.startActivity(intent);
				}
			});
		}
		
		checkIfMustLoadNextData(position);
		
		return rowView;
	}

	public View getHeaderView() {
		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		View header = inflater.inflate(R.layout.trd_buy_list_header, null);
		return header;
	}

	public View getFooterView() {
		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		View footer = inflater.inflate(R.layout.trd_buy_list_footer, null);
		return footer;
	}

	
	/**
	 * Returns the correct value for Bonds, Equities or funds
	 */
	private Drawable getCorrectIcon(StexAssetGroupType instrumentType){		
		if (instrumentType == StexAssetGroupType.BONDS){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_bonds_small);			
		}else if (instrumentType == StexAssetGroupType.EQUITIES){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_equities_small);
		}else if (instrumentType == StexAssetGroupType.FUNDS){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_funds_small);
		}
		return null;
	}
}
