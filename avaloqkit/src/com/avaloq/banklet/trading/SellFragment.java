/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */

package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.BankingPositionListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.banklet.trading.SellListAdapter.ViewHolder;
import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.searchwidget.AccountCriterium;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.CurrencyCriterion;
import com.avaloq.framework.tools.searchwidget.DateCriterium;
import com.avaloq.framework.tools.searchwidget.PortfolioCriterion;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;
import com.avaloq.framework.ui.BankletFragment;


public class SellFragment extends BankletFragment {

	/*
	 * The search field element where the user can enter a string to filter the results
	 */
	private SearchView mSearchField;

	/*
	 * The list adapter for the results
	 */
	private SellListAdapter mSellAdapter;

	/*
	 * Holder for the list of the results
	 */
	private ListView mResultList;

	private BankingPositionListQueryTO mQuery;
	private List<String> mQueryInstrumentIdentificationList;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View fragmentView = inflater.inflate(R.layout.trd_sell_activity, container, false);
		return fragmentView;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mQuery = new BankingPositionListQueryTO();
		
		/*
		 * When the criteria in the search field changes we have to filter the results
		 */
		mSearchField = (SearchView) getView().findViewById(R.id.trd_sell_search);		
		mSearchField.init(this);
		/*
		 * We get the list element, create a list adapter and set it as
		 * adapter for the list. Additionally, we set the header row
		 * for the list.
		 */		
		mSellAdapter = new SellListAdapter(this){

			@Override
			public void loadNext() {				
				showListLoading(true);
				mSellAdapter.requestSellListData(mQuery, false);
			}

			@Override
			public ListView getListView() {
				return mResultList;
			}

			@Override
			public void loadingStateChanged(boolean loading) {
				mSearchField.setProgressVisibility(loading);
			}
			
		};		
		mResultList = (ListView) getView().findViewById(R.id.trd_sell_list);
		mResultList.addHeaderView(mSellAdapter.getHeaderView());
		mResultList.setAdapter(mSellAdapter);

		mResultList.setClickable(true);
		/*
		 * When a list is clicked, the detail view for that element is opened. Because
		 * we already have the data for the instrument and we do not want to load it
		 * again in the detail view, we just pass it here as extra arguments
		 */
		mResultList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View listElement, int arg2, long arg3) {

				ViewHolder holder = (ViewHolder) listElement.getTag();

				// if this is null, probably this is the header
				if (holder == null){
					return;
				}
				
				if (!AvaloqApplication.getInstance().getConfiguration().hideMarketData()){
					Intent intent = new Intent();
					intent.setClass(getActivity(), TradingSellDetailActivity.class);
	
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT, holder.instrument);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_TYPE, holder.instrumentType.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.isin.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_BANKING_POSITION_ID, holder.id);
					intent.putExtra(TradingSellDetailActivity.EXTRA_IS_BUY, ButtonSellBuyStatus.SELL);
	
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_ISIN, holder.strIsin);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_PORTFOLIO_ID, holder.portfolioId);				
	
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					
					startActivity(intent);
				}
				else {
					Intent intent = new Intent();
					intent.setClass(getActivity(), PositionSellActivity.class);
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT, holder.instrument);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_TYPE, holder.instrumentType.getText().toString().toUpperCase());
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.isin.getText().toString());
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_PORTFOLIO_ID, holder.portfolioId);
					intent.putExtra(PositionSellActivity.EXTRA_BANKING_POSITION_ID, holder.id);
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					
					startActivity(intent);
				}
			}
		});

		/*
		 * This will make a call to the server to load the data
		 */
		mSellAdapter.requestSellListData(mQuery, true);		
		
		initializeSearch();
	}
	
	private void initializeSearch(){
		
		mSearchField.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				
				// get the reference to the lists
				mQueryInstrumentIdentificationList = mQuery.getInstrumentIdentificationList();
				
				mQueryInstrumentIdentificationList.clear();
				mQueryInstrumentIdentificationList.add(mSearchField.getText());
				
				for (Criterium crit: mSearchField.getCriteria())
					crit.addToQuery();
				
				/*
				 * This will make a call to the server to load the data
				 */
				mSellAdapter.requestSellListData(mQuery, true);		
			}
		});
		
		
		mSearchField.addCriterium(new DateCriterium(getActivity()){
			@Override
			public void addToQuery() {
				mQuery.setEndDate(null);
				if (mFrom != null)
					//mQuery.set
				if (mTo != null)
					mQuery.setEndDate(mTo);
			}
		});
		
		mSearchField.addCriterium(new PortfolioCriterion(getActivity()){
			@Override
			public void addToQuery() {
				mQuery.getContainerPortfolioIdList().clear();
				for (ContainerPortfolioTO portfolios: mElements)
					mQuery.getContainerPortfolioIdList().add(portfolios.getId());
			}
		});
		
		mSearchField.addCriterium(new CurrencyCriterion(getActivity()){
			@Override
			public void addToQuery() {
				if (mElement != null){
					mQuery.setCurrencyId(mElement.getId());
				}else{
					mQuery.setCurrencyId(null);
				}
			}
		});

	}	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			mSearchField.onActivityResult(requestCode, resultCode, data);
	}
	
}
