/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */
package com.avaloq.banklet.trading;

import android.content.Intent;
import android.os.Bundle;

import com.avaloq.afs.server.bsp.client.ws.BankingPositionTO;
import com.avaloq.banklet.trading.TradingSellDetailMarketDataFragment.DetailMarketDataFragmentInterface;
import com.avaloq.banklet.trading.TradingSellDetailTransactionsFragment.DetailTransactionsFragmentInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;

public class TradingSellDetailActivity extends TabbedBankletActivity implements 
	DetailMarketDataFragmentInterface, DetailTransactionsFragmentInterface{

	public static final String EXTRA_INSTRUMENT_ID = "instrumentID";
	public static final String EXTRA_INSTRUMENT = "instrument";
	public static final String EXTRA_INSTRUMENT_NAME = "instrumentName";
	public static final String EXTRA_INSTRUMENT_TYPE = "instrumentType";
	public static final String EXTRA_INSTRUMENT_SUB_TYPE = "instrumentSubType";
	public static final String EXTRA_BANKING_POSITION_ID = "bankingPositionId";
	public static final String EXTRA_IS_BUY = "isBuy";	
	
	public static final String EXTRA_INSTRUMENT_CURRENCY_ID = "currencyId";
	public static final String EXTRA_INSTRUMENT_MARKET_ID = "marketId";
	public static final String EXTRA_INSTRUMENT_MARKET_NAME = "marketName";
	public static final String EXTRA_INSTRUMENT_ISIN = "isin";
	public static final String EXTRA_INSTRUMENT_PORTFOLIO_ID = "portfolioId";
	public static final String EXTRA_INSTRUMENT_IS_MONEY_TRADABLE = "EXTRA_INSTRUMENT_IS_MONEY_TRADABLE";
	
	public enum ButtonSellBuyStatus {
	    BUY, SELL, NONE, TRANSACTIONS_ONLY
	}
	
	/*
	 * The following data will be requested by the created fragments
	 */
	private Long mBankingPositionId;
	private BankingPositionTO mInstrument;
	private Long mInstrumentId;
	private String mInstrumentName;
	private String mInstrumentType;
	private String mInstrumentSubType;
	private ButtonSellBuyStatus mSellBuyButtonStatus;
	
	private boolean mIsMoneyTradable;
	
	private Long mCurrencyId;
	private Long mMarketId;
	private String mMarketName;
	private String mIsin;
	private Integer mPortfolioId;
	
	public Long getBankingPositionId() {
		return mBankingPositionId;
	}

	public void setBankingPositionId(Long bankingPositionId) {
		mBankingPositionId = bankingPositionId;
	}

	public Long getInstrumentId() {
		return mInstrumentId;
	}

	public void setInstrumentId(Long instrumentId) {
		mInstrumentId = instrumentId;
	}

	public String getInstrumentName() {
		return mInstrumentName;
	}

	public void setInstrumentName(String instrumentName) {
		mInstrumentName = instrumentName;
	}

	public String getInstrumentType() {
		return mInstrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		mInstrumentType = instrumentType;
	}

	public String getInstrumentSubType() {
		return mInstrumentSubType;
	}

	public void setInstrumentSubType(String instrumentSubType) {
		mInstrumentSubType = instrumentSubType;
	}	

	public ButtonSellBuyStatus getSellBuyButtonStatus() {
		return mSellBuyButtonStatus;
	}

	public void setSellBuyButtonStatus(ButtonSellBuyStatus sellBuyButtonStatus) {
		mSellBuyButtonStatus = sellBuyButtonStatus;
	}

	@Override
	public void prepareNavigationItems() {
		
		/*
		 *  Get the additional parameters. The created fragments will use some of the data.
		 *  it is accessible in the fragments using getActivity().staticMember
		 */
		Bundle b = getIntent().getExtras();
		mInstrument = (BankingPositionTO) b.getSerializable(EXTRA_INSTRUMENT);
		mInstrumentId = b.getLong(EXTRA_INSTRUMENT_ID);
		mInstrumentName = b.getString(EXTRA_INSTRUMENT_NAME);
		mInstrumentType = b.getString(EXTRA_INSTRUMENT_TYPE);
		mInstrumentSubType = b.getString(EXTRA_INSTRUMENT_SUB_TYPE);
		mBankingPositionId = b.getLong(EXTRA_BANKING_POSITION_ID);
		
		mIsMoneyTradable = b.getBoolean(EXTRA_INSTRUMENT_IS_MONEY_TRADABLE);
		
		mCurrencyId = b.getLong(EXTRA_INSTRUMENT_CURRENCY_ID);
		mMarketId = b.getLong(EXTRA_INSTRUMENT_MARKET_ID);
		mMarketName = b.getString(EXTRA_INSTRUMENT_MARKET_NAME);
		mIsin = b.getString(EXTRA_INSTRUMENT_ISIN);
		mPortfolioId = b.getInt(EXTRA_INSTRUMENT_PORTFOLIO_ID);
		
		// Shows if the activity is called from the Buy list or from Sell list or from Wealth
		mSellBuyButtonStatus = (ButtonSellBuyStatus) b.getSerializable(EXTRA_IS_BUY);
		
		if (AvaloqApplication.getInstance().getConfiguration().hideMarketData()){
			// show only the transactions
			showNavigationItems(new FragmentNavigationItem[] {
					new FragmentNavigationItem(TradingSellDetailTransactionsFragment.class, getString(R.string.trading_transactions))					
			});
		}else{		
			if (mSellBuyButtonStatus == ButtonSellBuyStatus.SELL || mSellBuyButtonStatus == ButtonSellBuyStatus.NONE){			
				showNavigationItems(new FragmentNavigationItem[] {
						new FragmentNavigationItem(TradingSellDetailMarketDataFragment.class, getString(R.string.trading_market_data)),
						new FragmentNavigationItem(TradingSellDetailTransactionsFragment.class, getString(R.string.trading_transactions))
				});
			}else if (mSellBuyButtonStatus == ButtonSellBuyStatus.TRANSACTIONS_ONLY){			
				showNavigationItems(new FragmentNavigationItem[] {
						new FragmentNavigationItem(TradingSellDetailTransactionsFragment.class, getString(R.string.trading_transactions))					
				});
			}else{			
				showNavigationItems(new FragmentNavigationItem[] {
						new FragmentNavigationItem(TradingSellDetailMarketDataFragment.class, getString(R.string.trading_market_data))					
				});
			}
		}
		
		/*
		 * Set the title name to the instrument name
		 */
		setTitle(mInstrumentName);
	}

	public String getIsin() {
		return mIsin;
	}

	@Override
	public void onClick_ButtonSell() {

		Intent intent = new Intent();
		intent.setClass(getBaseContext(), PositionSellActivity.class);
		
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_ID, mInstrumentId);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT, mInstrument);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_NAME, mInstrumentName);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_TYPE, mInstrumentType.toUpperCase());
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_SUB_TYPE, mIsin);
		
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_CURRENCY_ID, mCurrencyId);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_ID, mMarketId);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_NAME, mMarketName);
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_PORTFOLIO_ID, mPortfolioId);
		intent.putExtra(PositionSellActivity.EXTRA_BANKING_POSITION_ID, mBankingPositionId);
		
		intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, mIsMoneyTradable);
		
		startActivity(intent);
		
	}

	@Override
	public void onClick_ButtonBuy(int selectedListingIndex) {
		
		Intent intent = new Intent();
		intent.setClass(getBaseContext(), PositionBuyActivity.class);
		
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_ID, mInstrumentId);
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_NAME, mInstrumentName);
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_TYPE, mInstrumentType.toUpperCase());
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_SUB_TYPE, mInstrumentSubType);
		
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_CURRENCY_ID, mCurrencyId);
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_ID, mMarketId);
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_NAME, mMarketName);
		
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, mIsMoneyTradable);
		
		intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_SELECTED_LISTING_INDEX, selectedListingIndex);
		
		
		startActivity(intent);
	}

	@Override
	public Long getCurrencyId() {
		return mCurrencyId;
	}

	@Override
	public String getMarketName() {
		return mMarketName;
	}

	@Override
	public Long getMarketId() {
		return mMarketId;
	}

	@Override
	public void onClick_ButtonBuy() {
		// TODO Auto-generated method stub		
	}
	
	@Override
	public void onBackPressed() {
		TradingSellDetailMarketDataFragment.resetChartPeriod();
		super.onBackPressed();
	}
}
