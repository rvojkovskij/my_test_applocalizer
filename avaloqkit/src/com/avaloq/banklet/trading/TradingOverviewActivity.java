package com.avaloq.banklet.trading;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;

// TODO Move strings to xml

public class TradingOverviewActivity extends TabbedBankletActivity {
	
	@Override
	public void prepareNavigationItems() {
		showNavigationItems(new FragmentNavigationItem[] {
				new FragmentNavigationItem(BuyFragment.class, getString(R.string.trading_buy)),
				new FragmentNavigationItem(SellFragment.class, getString(R.string.trading_sell)),
				new FragmentNavigationItem(OrdersFragment.class, getString(R.string.trading_orders))
		});
	}
	
}
