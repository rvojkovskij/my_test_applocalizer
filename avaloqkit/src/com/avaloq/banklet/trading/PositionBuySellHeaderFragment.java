package com.avaloq.banklet.trading;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.marketdata.MarketDataResult;
import com.avaloq.afs.aggregation.to.wealth.ListingListResult;
import com.avaloq.afs.server.bsp.client.ws.ListingTO;
import com.avaloq.afs.server.bsp.client.ws.MarketDataQueryTO;
import com.avaloq.afs.server.bsp.client.ws.MarketDataTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.banklet.trading.PositionBuySellHeaderFragmentElement.PositionBuySellHeaderFragmentElementInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataRequest;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataService;
import com.avaloq.framework.comms.webservice.trading.ListingListRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.tools.CustomViewPager;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.CurrencyUtil;

public class PositionBuySellHeaderFragment extends BankletFragment implements PositionBuySellHeaderFragmentElementInterface{

	private int TOTAL_PAGES = 4;
	
	private static int mSelectedPage = 0;
	
	private static String mSubtypeString = "";
	
	private final int DECIMALS_PERCENT = 5;
	private final int DECIMALS_NUMBER = 3;
	
	/**
	 * Interface that must be implemented by the parent activity.
	 * Contains methods to request the instrument data and action
	 * to react when the buttons are clicked 
	 * 	 
	 */
	public interface PositionBuyFragmentHeaderInterface{
		public Long getInstrumentId();
		public String getInstrumentName();
		public StexAssetGroupType getInstrumentType();
		public String getInstrumentSubType();
		public Long getCurrencyId();
		public String getMarketName();
		public boolean isBuy();
		public int getSelectedListingIndex();	
		public Long getMarketId();
		public void showEstimate(BigDecimal price, Long currencyId);
	}

	private Long mInstrumentId;
	
	private static List<ListingTO> listings = new ArrayList<ListingTO>();
	private static int selectedListingIndex = -1;
	private Long mMarketId; 
	
	private boolean isSell = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		final View fragmentView;
		
		
		
		if (this.getTag() != null && ((String)this.getTag()).compareTo("isSell") == 0)	{
			TOTAL_PAGES = 1;
			isSell = true;
			fragmentView = inflater.inflate(R.layout.trd_position_buy_sell_header_fragment_sell, container, false);
		}else{
			fragmentView = inflater.inflate(R.layout.trd_position_buy_sell_header_fragment_buy, container, false);
			selectedListingIndex = ((PositionBuyFragmentHeaderInterface) getActivity()).getSelectedListingIndex();
			mMarketId = ((PositionBuyFragmentHeaderInterface) getActivity()).getMarketId();
		}
		
		if (AvaloqApplication.getInstance().getConfiguration().hideMarketData()) {
			TOTAL_PAGES = 1;
			// TODO Find out the exact reason that we get a NPE here, and fix the cause.
			if(fragmentView != null) {
				View marketDataHeader = fragmentView.findViewById(R.id.trd_market_data_header);
				if(marketDataHeader != null) {
					marketDataHeader.setVisibility(View.GONE);
				}
			}
		}
		
		
		
		mInstrumentId = ((PositionBuyFragmentHeaderInterface) getActivity()).getInstrumentId();
		
		setTitleHeaderValues(fragmentView);
				
		// request whatever is needed
		if (!isSell){
			requestInstrumentListing();
		}else{
			requestMarketData();
		}
		
		return fragmentView;
	}

	
	/**
	 * Adds the listing information and currency to the header
	 */
	private void addListingToHeader(){	

		if (isSell) return;
		
		// add values to spinner
		Spinner spinnerListing = (Spinner) getView().findViewById(R.id.trading_detail_spinnerListing);	
		ViewPager vp =  (ViewPager) getView().findViewById(R.id.trading_header_pager);		
		if (spinnerListing == null || vp != null)
		{
			requestMarketData();
			return;
		}
		
		List<String> list = new ArrayList<String>();
		for (ListingTO listing: listings){
			list.add(listing.getMarketName()+"/"+AvaloqApplication.getInstance().findCurrencyById(listing.getCurrencyId()).getIsoCode());
		}
		final HighlightedSpinnerAdapter<String> dataAdapter = new HighlightedSpinnerAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerListing.setAdapter(dataAdapter);
		spinnerListing.setVisibility(View.VISIBLE);
		spinnerListing.setSelection(selectedListingIndex);
		spinnerListing.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				selectedListingIndex = pos;				
				dataAdapter.setHighlightedPosition(pos);
				// get the market data
				requestMarketData();				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// do nothing
			}
		});
	}

	/**
	 * Requests listing info
	 */
	private void requestInstrumentListing(){

		RequestStateEvent<ListingListRequest> rse = new RequestStateEvent<ListingListRequest>() {
			@Override
			public void onRequestCompleted(ListingListRequest stexDefaultsRequest) {								
				receiveInstrumentListing(stexDefaultsRequest);			
			}			
		};
		
		// Initiate the request 
		ListingListRequest request = TradingService.getInstrumentListingList(mInstrumentId, rse);		
		
		// Add the request to the request queue
		request.initiateServerRequest();		
	}
	
	/**
	 * Callback when receiving the stex defaults
	 */
	private void receiveInstrumentListing(ListingListRequest listingListRequest){
		
		if (getActivity() == null) return;
		
		ListingListResult listingListResult = listingListRequest.getResponse().getData();
		if (listingListResult != null) {

			listings.clear();
			
			// add only the listings with the same currency
			for(ListingTO listing: listingListResult.getListingList()){
				if (listing.getCurrencyId() != null && listing.getCurrencyId().longValue() == ((PositionBuyFragmentHeaderInterface) getActivity()).getCurrencyId().longValue()){
					listings.add(listing);
				}
			}
			
			// The default listing for this instrument has to be selected
			if (selectedListingIndex == -1){
				for (int i=0; i<listings.size(); i++){
					if (listings.get(i).getMarketId().longValue() == mMarketId.longValue()){
						selectedListingIndex = i;
						break;
					}
				}
				// if we did not find the listing, just default to 0
				if (selectedListingIndex == -1){
					selectedListingIndex = 0;
				}
			}		
		
			addListingToHeader();
		}	
	}
	
	
	/**
	 * Requests the market data
	 */
	private void requestMarketData(){		
		MarketDataQueryTO query = new MarketDataQueryTO();
		query.setCurrencyId(null);
		query.setInstrumentId(mInstrumentId);
		if (!isSell){
			query.setMarketId((listings.size() > 0) ? listings.get(selectedListingIndex).getMarketId() : null);
		}else{
			query.setMarketId(null);
		}
		
		RequestStateEvent<MarketDataRequest> rse = new RequestStateEvent<MarketDataRequest>() {
			@Override
			public void onRequestCompleted(MarketDataRequest positionsRequest) {	
				try{
					 showMarketData(positionsRequest);
				}catch(Exception e){
					
				}
			}			
		};
		
		// Initiate the request 
		MarketDataRequest request = MarketDataService.getMarketData(query, rse);					
	
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	
	/**
	 * Callback function. Called when the market data is received from the server. Used
	 * to display the data.
	 */
	private void showMarketData(final AbstractServerRequest<MarketDataResult> aRequest){
		MarketDataResult marketDataResult = aRequest.getResponse().getData();
		if (marketDataResult != null) {
			
			MarketDataTO marketData = marketDataResult.getMarketData();

			((PositionBuyFragmentHeaderInterface)getActivity()).showEstimate(marketData.getLastPrice(),marketData.getCurrencyId());
			
			
			String currencyCode = AvaloqApplication.getInstance().findCurrencyById(marketData.getCurrencyId()).getIsoCode();
			
			CustomViewPager vp =  (CustomViewPager) getView().findViewById(R.id.trading_header_pager);
			
			// If the viewpager is present, then this is the layout for the phone
			if (vp != null){	
				
				vp.setOffscreenPageLimit(3);
				
				ElementsSlidePagerAdapter pagerAdapter = new ElementsSlidePagerAdapter(getFragmentManager(), marketData, currencyCode);						
				vp.setAdapter(pagerAdapter);
				vp.setIndicator(getActivity(), R.id.trading_header_pager_tiles);
				//((com.avaloq.framework.tools.CustomViewPager)vp).setScrollView((ScrollView)getActivity().findViewById(R.id.trd_scrollview));
				vp.setDrawingCacheEnabled(true);
				vp.setSelected(true);
			}else{
				setBidValues(marketData, currencyCode);
									
				setAskValues(marketData, currencyCode);
				
				setLastValues(marketData, currencyCode);
			}

		}	
	}
	
	/**
	 * @param marketData
	 * @param currencyCode
	 */
	private void setBidValues(MarketDataTO marketData, String currencyCode){
		//************************************************************************************		
		
		// Set bid value					
		if (marketData.getBidPrice() != null){
			( (TextView) getView().findViewById(R.id.tvBidPrice)).setText(CurrencyUtil.formatNumber(marketData.getBidPrice(), DECIMALS_NUMBER));			
			( (TextView) getView().findViewById(R.id.tvBidPriceCurrency)).setText(currencyCode);
		}else{
			( (TextView) getView().findViewById(R.id.tvBidPrice)).setText(getActivity().getText(R.string.trd_unavailable));
			( (TextView) getView().findViewById(R.id.tvBidPriceCurrency)).setText("");
		}
		
		// Set bid date
		if (marketData.getBidDate() != null){
			( (TextView) getView().findViewById(R.id.tvBidPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getBidDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getBidDate()));
		}else{
			( (TextView) getView().findViewById(R.id.tvBidPriceDate)).setText(getActivity().getText(R.string.trd_unavailable));
		}		
		
		// Set bid volume
		if (marketData.getBidVolume() != null){
			( (TextView) getView().findViewById(R.id.tvBidPriceVolume)).setText(CurrencyUtil.formatNumber(marketData.getBidVolume()));
		}else{
			( (TextView) getView().findViewById(R.id.tvBidPriceVolume)).setText(getActivity().getText(R.string.trd_unavailable));
		}
	}
	
	/**
	 *
	 * @param marketData
	 * @param currencyCode
	 */
	private void setAskValues(MarketDataTO marketData, String currencyCode){
		//************************************************************************************
		
		// Set ask value
		if (marketData.getAskPrice() != null){
			( (TextView) getView().findViewById(R.id.tvAskPrice)).setText(CurrencyUtil.formatNumber(marketData.getAskPrice(), DECIMALS_NUMBER));
			( (TextView) getView().findViewById(R.id.tvAskPriceCurrency)).setText(currencyCode);
		}else{
			( (TextView) getView().findViewById(R.id.tvAskPrice)).setText(getActivity().getText(R.string.trd_unavailable));
			( (TextView) getView().findViewById(R.id.tvAskPriceCurrency)).setText("");
		}
		
		// Set ask date
		if (marketData.getAskDate() != null){
			( (TextView) getView().findViewById(R.id.tvAskPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getAskDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getAskDate()));
		}else{
			( (TextView) getView().findViewById(R.id.tvAskPriceDate)).setText(getActivity().getText(R.string.trd_unavailable));
		}
		
		// Set ask volume
		if (marketData.getAskVolume() != null){
			( (TextView) getView().findViewById(R.id.tvAskPriceVolume)).setText(CurrencyUtil.formatNumber(marketData.getAskVolume()));
		}else{
			( (TextView) getView().findViewById(R.id.tvAskPriceVolume)).setText(getActivity().getText(R.string.trd_unavailable));
		}
	}
	
	/**
	 *
	 * @param marketData
	 * @param currencyCode
	 */
	private void setLastValues(MarketDataTO marketData, String currencyCode){
		//************************************************************************************					

		// Set Last value
		if (marketData.getLastPrice() != null){
			( (TextView) getView().findViewById(R.id.tvLastPrice)).setText(CurrencyUtil.formatNumber(marketData.getLastPrice(), DECIMALS_NUMBER));
			( (TextView) getView().findViewById(R.id.tvLastPriceCurrency)).setText(currencyCode);
		}else{
			( (TextView) getView().findViewById(R.id.tvLastPrice)).setText(getActivity().getText(R.string.trd_unavailable));
			( (TextView) getView().findViewById(R.id.tvLastPriceCurrency)).setText("");
		} 
		
		// Set Last date
		if (marketData.getLastDate() != null){
			( (TextView) getView().findViewById(R.id.tvLastPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getLastDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getLastDate()));
		}else{
			( (TextView) getView().findViewById(R.id.tvLastPriceDate)).setText(getActivity().getText(R.string.trd_unavailable));			
		}
		
		// Set last volume
		if (marketData.getLastVolume() != null){
			( (TextView) getView().findViewById(R.id.tvLastPriceVolume)).setText(CurrencyUtil.formatNumber(marketData.getLastVolume()));
		}else{
			( (TextView) getView().findViewById(R.id.tvLastPriceVolume)).setText(getActivity().getText(R.string.trd_unavailable));			
		}
		 
		//************************************************************************************ 
		// Set Percent value
		if (marketData.getChangeLastClosedPercent() != null){
			( (TextView) getView().findViewById(R.id.tvPercent)).setText(CurrencyUtil.formatNumber(marketData.getChangeLastClosedPercent(), DECIMALS_PERCENT));
			( (TextView) getView().findViewById(R.id.tvPercentChar)).setText("%");
		}else{
			( (TextView) getView().findViewById(R.id.tvPercent)).setText(getActivity().getText(R.string.trd_unavailable));
			( (TextView) getView().findViewById(R.id.tvPercentChar)).setText(""); 
		}
	}
	
	
	
	/**
	 * Sets the correct value for Bonds, Equities or funds
	 */
	private void setCorrectIcon(View fragmentView, StexAssetGroupType instrumentType){
		
		int res = getIconByInstrumentType(instrumentType);
			
		if (res == 0) return;
		
		if (fragmentView.findViewById(R.id.trd_header_icon) != null){
			((ImageView) fragmentView.findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(res));
		}
	}
	
	/**
	 * Returns the resource to display
	 *
	 * @return
	 */
	private int getIconByInstrumentType(StexAssetGroupType instrumentType){
		int res = 0;
		if (instrumentType == StexAssetGroupType.BONDS){			
			res = R.drawable.trd_icon_bonds;			
		}else if (instrumentType == StexAssetGroupType.EQUITIES){			
			res = R.drawable.trd_icon_equities;
		}else if (instrumentType == StexAssetGroupType.FUNDS){			
			res = R.drawable.trd_icon_funds;
		}
		return res;
	}
	
	/**
	 * Displays the data about the instrument in the header of the fragment 
	 * 
	 * @param fragmentView
	 */
	private void setTitleHeaderValues(View fragmentView){
		
		if (fragmentView.findViewById(R.id.tvTitle) != null){
			((TextView) fragmentView.findViewById(R.id.tvTitle)).setText(((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentName());
		}
		if (fragmentView.findViewById(R.id.tvInstrumentType) != null){
			((TextView) fragmentView.findViewById(R.id.tvInstrumentType)).setText(((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentType().toString());
		}
		

		if (fragmentView.findViewById(R.id.tvInstrumentSubType) != null){		
			mSubtypeString = getString(R.string.trading_header_isin)+" "+((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentSubType(); 
			((TextView) fragmentView.findViewById(R.id.tvInstrumentSubType)).setText(mSubtypeString);
		}


		// hide the button
		if (fragmentView.findViewById(R.id.trading_detail_btnSell) != null){
			((View) fragmentView.findViewById(R.id.trading_detail_btnSell)).setVisibility(View.INVISIBLE);
		}

		setCorrectIcon(fragmentView, ((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentType());
	}
	
	/**
     * The pager adapter that represents the elements on the header
     */
    private class ElementsSlidePagerAdapter extends FragmentStatePagerAdapter {
    	
    	MarketDataTO mMarkedData;
    	String mCurrencyCode;
    	
        public ElementsSlidePagerAdapter(FragmentManager fm, MarketDataTO marketData, String currencyCode) {
            super(fm);
            
            mMarkedData = marketData;
            mCurrencyCode = currencyCode;
            
        }

        /**
         * Returns a banklet item
         */
        @Override
        public BankletFragment getItem(int position) {
        	BankletFragment bf = new PositionBuySellHeaderFragmentElement();
        	((PositionBuySellHeaderFragmentElement) bf).setParentFragment(PositionBuySellHeaderFragment.this);
        	Bundle args=null;
        	
    		if (position == 0) {    			
    			args = getArgsTitle();    			
    		}else 
    		if (position == 1) {    			
    			args = getArgsAsk();
    		}else    		
    		if (position == 2) {    			
    			args = getArgsBid();
    		}else    		
    		if (position == 3) {    			 
    			args = getArgsLast();
    		}
        	
    		if (args == null) return null;
    		
    		
            args.putInt("resourceId", position);
            
            bf.setArguments(args);
            
            return bf;
        }

        /**
         * Returns the arguments needed to display the "Title" fragment
         * @return
         */
        private Bundle getArgsTitle(){
        	Bundle args=new Bundle();

        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_TITLE, ((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentName());
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_INSTRUMENT_TYPE, ((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentType().toString());
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_INSTRUMENT_SUBTYPE, getString(R.string.trading_header_isin)+" "+((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentSubType());

        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_INSTRUMENT_ICON, getIconByInstrumentType(((PositionBuyFragmentHeaderInterface)getActivity()).getInstrumentType()));
        	
        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_RESOURCE_ID, R.layout.trd_position_buy_sell_header_fragment_element_title);

        	ArrayList<String> list = new ArrayList<String>();
        	for (ListingTO listing: listings){
        		list.add(listing.getMarketName()+"/"+AvaloqApplication.getInstance().findCurrencyById(listing.getCurrencyId()).getIsoCode() );
        	}
        	args.putStringArrayList(PositionBuySellHeaderFragmentElement.EXTRA_LISTING_LIST, list);
        	
        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_LISTING_SELECTED_INDEX, selectedListingIndex);
        	
        	args.putBoolean(PositionBuySellHeaderFragmentElement.EXTRA_LISTING_IS_SELL, isSell);
        	
        	return args;
        }
        
        /**
         * Returns the arguments needed to display the "Ask" fragment
         * @return
         */
        private Bundle getArgsAsk(){
        	Bundle args=new Bundle();
        	
        	String title = getString(R.string.trading_ask);
        	
        	String price = getActivity().getString(R.string.trd_unavailable_short);
        	String quantity = getActivity().getString(R.string.trd_unavailable_short);
        	String date = getActivity().getString(R.string.trd_unavailable_short);
        	
        	// Set ask value
    		if (mMarkedData.getAskPrice() != null){
    			price = CurrencyUtil.formatNumber(mMarkedData.getAskPrice(), DECIMALS_NUMBER)+" "+mCurrencyCode;    			
    		}
    		
    		// Set ask date
    		if (mMarkedData.getAskDate() != null){
    			date = DateFormat.getDateFormat(getActivity().getApplicationContext()).format(mMarkedData.getAskDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(mMarkedData.getAskDate());
    		}
    		
    		// Set ask volume
    		if (mMarkedData.getAskVolume() != null){
    			quantity = CurrencyUtil.formatNumber(mMarkedData.getAskVolume());
    		}

        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_TITLE, title);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_PRICE, price);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_QUANTITY, quantity);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_DATETIME, date);
        	
        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_RESOURCE_ID, R.layout.trd_position_buy_sell_header_fragment_element_data);        	
        	return args;
        }
        
        /**
         * Returns the arguments needed to display the "Bid" fragment
         * @return
         */
        private Bundle getArgsBid(){
        	Bundle args=new Bundle();
        	
        	String title = getString(R.string.trading_bid);
        	
        	String price = getActivity().getString(R.string.trd_unavailable_short);
        	String quantity = getActivity().getString(R.string.trd_unavailable_short);
        	String date = getActivity().getString(R.string.trd_unavailable_short);
        	
        	
        	// Set ask value
    		if (mMarkedData.getBidPrice() != null){
    			price = CurrencyUtil.formatNumber(mMarkedData.getBidPrice(), DECIMALS_NUMBER) + " " +mCurrencyCode;    			
    		}else{
    			price = getActivity().getString(R.string.trd_unavailable_short);    			
    		}
    		
    		// Set ask date
    		if (mMarkedData.getBidDate() != null){
    			date = DateFormat.getDateFormat(getActivity().getApplicationContext()).format(mMarkedData.getAskDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(mMarkedData.getBidDate());
    		}
    		
    		// Set ask volume
    		if (mMarkedData.getBidVolume() != null){
    			quantity = CurrencyUtil.formatNumber(mMarkedData.getBidVolume());
    		}
        	
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_TITLE, title);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_PRICE, price);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_QUANTITY, quantity);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_DATETIME, date);
        	
        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_RESOURCE_ID, R.layout.trd_position_buy_sell_header_fragment_element_data);        	
        	return args;
        }
        
        /**
         * Returns the arguments needed to display the "Last" fragment
         * @return
         */
        private Bundle getArgsLast(){
        	Bundle args=new Bundle();
        	
        	String title = getString(R.string.trading_last);
        	
        	String price = getActivity().getString(R.string.trd_unavailable_short);
        	String quantity = getActivity().getString(R.string.trd_unavailable_short);
        	String date = getActivity().getString(R.string.trd_unavailable_short);
        	
        	// Set Last value
    		if (mMarkedData.getLastPrice() != null){
    			price = CurrencyUtil.formatNumber(mMarkedData.getLastPrice(), DECIMALS_NUMBER)+" "+mCurrencyCode;    			
    		}
    		
    		// Set Last date
    		if (mMarkedData.getLastDate() != null){
    			date = DateFormat.getDateFormat(getActivity().getApplicationContext()).format(mMarkedData.getLastDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(mMarkedData.getLastDate());
    		}
    		
    		
    		// Set last volume
    		if (mMarkedData.getLastVolume() != null){
    			quantity = CurrencyUtil.formatNumber(mMarkedData.getLastVolume());
    		}
    		
    		    		
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_TITLE, title);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_PRICE, price);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_QUANTITY, quantity);
        	args.putString(PositionBuySellHeaderFragmentElement.EXTRA_DATETIME, date);
        	
        	args.putInt(PositionBuySellHeaderFragmentElement.EXTRA_RESOURCE_ID, R.layout.trd_position_buy_sell_header_fragment_element_data);        	
        	return args;
        }
        
        /**
         * 
         */
        @Override
        public int getCount() {
            return TOTAL_PAGES;
        }
    }

//	@Override
//	public List<String> getSpinnerValues() {
//		List<String> list = new ArrayList<String>();
//		for (ListingTO listing: listings){
//			list.add(listing.getMarketName()+"/"+currencies.get(listing.getCurrencyId()));
//		}		
//		return list;
//	}

	public void setSelectedListing(int pos) {
		if (selectedListingIndex != pos){
			selectedListingIndex = pos;
			mMarketId = (listings.size() > 0) ? listings.get(selectedListingIndex).getMarketId() : null;
			requestMarketData();		
		}
	}

	public Long getMarketId(){
		return mMarketId;
	}
}



