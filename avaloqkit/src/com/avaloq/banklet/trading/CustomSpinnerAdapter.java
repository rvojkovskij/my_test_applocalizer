package com.avaloq.banklet.trading;

import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

public abstract class CustomSpinnerAdapter<T> extends ArrayAdapter<T> implements SpinnerAdapter{

	private LayoutInflater layoutInflater;
	private String displayText;
	private boolean mIsString;
	
	public CustomSpinnerAdapter(Context context, int textViewResourceId, T[] objects, String displayText, boolean isString) {
		this(context, textViewResourceId, Arrays.asList(objects), displayText, isString);
	}
	
	public CustomSpinnerAdapter(Context context, int textViewResourceId, List<T> objects, String displayText, boolean isString) {
		super(context, textViewResourceId, objects);
		this.displayText = displayText;
		layoutInflater = LayoutInflater.from(context);
		mIsString = isString;
	}
	
	@Override
	public final View getView(int position, View convertView, ViewGroup parent) {
		if (displayText != null && position == 0) {
	        View view = getSelectedView(parent);
	        ((TextView) view.findViewById(android.R.id.text1)).setText(displayText);
	        return view;
	    } else {
	        View view = getSelectedView(parent);
	        if (!mIsString){
		        String stringId = getTextResourceName(getItem(position));
				int textId = getContext().getResources().getIdentifier(stringId, "string", getContext().getPackageName());
				if(textId == 0) {
					throw new RuntimeException("The string '"+stringId+"' was not found.");
				}
		        ((TextView) view.findViewById(android.R.id.text1)).setText(textId);
	        }else{
	        	((TextView) view.findViewById(android.R.id.text1)).setText((String)getItem(position));
	        }
	        return view;
	    }
	    // return super.getView(position, convertView, parent); 
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        if (!mIsString){
	        String stringId = getTextResourceName(getItem(position));
			int textId = getContext().getResources().getIdentifier(stringId, "string", getContext().getPackageName());
			if(textId == 0) {
				throw new RuntimeException("The string '"+stringId+"' was not found.");
			}
	        ((TextView) view.findViewById(android.R.id.text1)).setText(textId);
        }else{
        	((TextView) view.findViewById(android.R.id.text1)).setText((String)getItem(position));
        }
        return view;
	}

	protected View getSelectedView(ViewGroup parent) {
	    return layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
	}
	
	protected abstract String getTextResourceName(T t);

	

}


