package com.avaloq.banklet.trading;

import java.util.List;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class PositionBuySellHeaderFragmentElement extends BankletFragment {

	public enum PositionBuySellHeaderFragmentElementTypes{
		TITLE, ASK, BID, LAST
	}
	
	public interface PositionBuySellHeaderFragmentElementInterface{
		public void setSelectedListing(int pos);		
	}
	
	public static final String EXTRA_RESOURCE_ID = "EXTRA_RESOURCE_ID";
	
	public static final String EXTRA_TITLE = "EXTRA_TITLE";
	
	public static final String EXTRA_INSTRUMENT_TYPE = "EXTRA_INSTRUMENT_TYPE";
	public static final String EXTRA_INSTRUMENT_SUBTYPE = "EXTRA_INSTRUMENT_SUBTYPE";
	public static final String EXTRA_INSTRUMENT_ICON = "EXTRA_INSTRUMENT_ICON";
	
	public static final String EXTRA_PRICE = "EXTRA_PRICE";
	public static final String EXTRA_QUANTITY = "EXTRA_QUANTITY";
	public static final String EXTRA_DATETIME = "EXTRA_DATETIME";
	
	public static final String EXTRA_LISTING_LIST = "EXTRA_LISTING_LIST";
	public static final String EXTRA_LISTING_SELECTED_INDEX = "EXTRA_LISTING_SELECTED_INDEX";
	
	public static final String EXTRA_LISTING_IS_SELL = "EXTRA_LISTING_IS_SELL";
	
		
	private static List<String> list;
	private static Integer selectedSpinnerValue;
	
	PositionBuySellHeaderFragment parentFragment;
	
	public void setParentFragment(PositionBuySellHeaderFragment fragment){
		parentFragment = fragment;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
				
		int layoutResource = getArguments().getInt(EXTRA_RESOURCE_ID);
		
		final View fragmentView = inflater.inflate(layoutResource, container, false);
		
		if (layoutResource == R.layout.trd_position_buy_sell_header_fragment_element_title){
			drawTitleFragment(fragmentView);
		}else{
			drawData(fragmentView);
		}

		return fragmentView;
	}	
	
	
	
	private void drawTitleFragment(View fragmentView){ 
		
		// hide the button
		((View) fragmentView.findViewById(R.id.trading_detail_btnSell)).setVisibility(View.GONE);
		
		((TextView) fragmentView.findViewById(R.id.tvTitle)).setText(getArguments().getString(EXTRA_TITLE));
		((TextView) fragmentView.findViewById(R.id.tvInstrumentType)).setText(getArguments().getString(EXTRA_INSTRUMENT_TYPE));
		((TextView) fragmentView.findViewById(R.id.tvInstrumentSubType)).setText(getArguments().getString(EXTRA_INSTRUMENT_SUBTYPE));
		
		// set the icon
		((ImageView) fragmentView.findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(getArguments().getInt(EXTRA_INSTRUMENT_ICON)));
		
		if (getArguments().getBoolean(EXTRA_LISTING_IS_SELL)){
			
			return;
		}
		
		// add values to spinner
		Spinner spinnerListing = (Spinner) fragmentView.findViewById(R.id.trading_detail_spinnerListing);		
		if (spinnerListing == null) return;
		
		list = getArguments().getStringArrayList(EXTRA_LISTING_LIST);
		selectedSpinnerValue = getArguments().getInt(EXTRA_LISTING_SELECTED_INDEX);
		
		final HighlightedSpinnerAdapter<String> dataAdapter = new HighlightedSpinnerAdapter<String>(getActivity(),
			android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerListing.setAdapter(dataAdapter);
		spinnerListing.setVisibility(View.VISIBLE);		

		spinnerListing.setSelection(selectedSpinnerValue);
		spinnerListing.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				// cal the method on the activity
				((PositionBuySellHeaderFragmentElementInterface) getActivity()).setSelectedListing(pos);
				
				dataAdapter.setHighlightedPosition(pos);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// do nothing
			}
		});
	}
	
	private void drawData(View fragmentView){
		((TextView) fragmentView.findViewById(R.id.trading_buysell_header_title)).setText(getArguments().getString(EXTRA_TITLE));
		((TextView) fragmentView.findViewById(R.id.trading_buysell_header_price)).setText(getArguments().getString(EXTRA_PRICE));
		((TextView) fragmentView.findViewById(R.id.trading_buysell_header_quantity)).setText(getArguments().getString(EXTRA_QUANTITY));
		((TextView) fragmentView.findViewById(R.id.trading_buysell_header_datetime)).setText(getArguments().getString(EXTRA_DATETIME));
		
	}
	
}
