package com.avaloq.banklet.trading;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.trading.PositionsResult;
import com.avaloq.afs.aggregation.to.user.settings.UserSettingsTradingResult;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionTO;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.afs.server.bsp.client.ws.StexExecType;
import com.avaloq.banklet.trading.PositionBuyFragment.PositionBuyAccountRecordType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.PositionsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsService;
import com.avaloq.framework.comms.webservice.usersettings.UserSettingsTradingRequest;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.CurrencyFormatInputFilter;
import com.avaloq.framework.util.CurrencyUtil;

public class PositionSellFragment extends BankletFragment {

	/**
	 * Interface that must be implemented by the parent activity. Contains
	 * methods to request the instrument data and action to react when the
	 * buttons are clicked
	 * 
	 */
	public interface PositionSellFragmentInterface {
		public Long getInstrumentId();

		public void setConfirmButtonEnabled(boolean visible);

		public void accountRecordsReceived(List<PositionAccountRecord> records, PositionBuyAccountRecordType type);
		public boolean isInstrumentMoneyTradable();
		public Long getBankingPositionId();
		public StexAssetGroupType getInstrumentType();
		public BankingPositionTO getInstrument();
	}

	private String mErrorQuantity = "";
	private String mErrorAmount = "";
	private String mErrorLimitPrice = "";
	private String mErrorTriggerPrice = "";
	private String mErrorExecutionDate = "";	
	
	private TextView mEstimateField;
	
	private int mDecimalsForQuantity = 0;
	
	private static boolean isEditable = true;
	
	private Long mDefaultAccountId = null;
	private Long mDefaultPortfolioId = null;
	
	public enum DurationType {
		TODAY,
		THIS_MONTH,
		UNTIL_CANCELLED;
		public String getTextResourceId() {
			return "trading_duration_type_" + toString().toLowerCase();
		}
	}

	// private List<SpinnerIdValuePair> mExecutionTypes = new ArrayList<SpinnerIdValuePair>();
	private int mSelectedExecutionType;

	// private List<SpinnerIdValuePair> mDurationTypes = new ArrayList<SpinnerIdValuePair>();
	private int mSelectedDurationType;
	private Date mDurationDate;

	private View mFragmentView;	

	private static BankingPositionTO mBankingPosition;
	
	private StexExecType[] mExecTypes = {StexExecType.MARKET, StexExecType.LIMIT, StexExecType.STOP, StexExecType.STOP_LIMIT};
	
	private BigDecimal mUnitPrice;
	private Long mCurrencyId;

	/**
	 * 
	 * @param price
	 */
	public void setUnitPrice(BigDecimal price, Long currencyId){
		mCurrencyId = currencyId;
		mUnitPrice = price;
		showEstimate();
	}
	
	/**
	 * 
	 */
	private void showEstimate(){
		if (AvaloqApplication.getInstance().getConfiguration().hideMarketData()){
			return;
		}
		
		
		if (mUnitPrice != null && mExecTypes[mSelectedExecutionType] == StexExecType.MARKET){
			if (getOrderQuantity() != null){
				BigDecimal amount = mUnitPrice.multiply(getOrderQuantity());
				mEstimateField.setText(CurrencyUtil.formatMoney(amount, mCurrencyId));
			}else if(getOrderAmount() != null){
				mEstimateField.setText(CurrencyUtil.formatMoney(getOrderAmount(), mCurrencyId));
			}else{
				mEstimateField.setText(R.string.trd_n_a);
			}
		}else if (mExecTypes[mSelectedExecutionType] == StexExecType.LIMIT || mExecTypes[mSelectedExecutionType] == StexExecType.STOP_LIMIT){
			if (getOrderQuantity() != null && getLimitPrice() != null){
				BigDecimal amount = getLimitPrice().multiply(getOrderQuantity());
				mEstimateField.setText(CurrencyUtil.formatMoney(amount, mCurrencyId));
			}else{
				mEstimateField.setText(R.string.trd_n_a);
			}			
		}else if (mExecTypes[mSelectedExecutionType] == StexExecType.STOP){
			if (getOrderQuantity() != null && getTriggerPrice() != null){
				BigDecimal amount = getTriggerPrice().multiply(getOrderQuantity());
				mEstimateField.setText(CurrencyUtil.formatMoney(amount, mCurrencyId));
			}else{
				mEstimateField.setText(R.string.trd_n_a);
			}
		}else{
			mEstimateField.setText(R.string.trd_n_a);
		}
	}
	
	
	
	/**
	 * 
	 */
	private void setIsEditable(){
		if (getActivity() == null) return;
		if (isEditable){
			// enable the fields
			
			// Order quantity
			// hide the edittext
			EditText tvOrderQuantity = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity);
			tvOrderQuantity.setVisibility(View.VISIBLE);
			
			// Set value and show the textview
			TextView tvOrderQuantity_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvQuantity_ne);
			tvOrderQuantity_ne.setVisibility(View.GONE);
			tvOrderQuantity_ne.setText("");
			
			if ( ((PositionSellFragmentInterface) getActivity()).isInstrumentMoneyTradable()){
				// Order amount
				// hide the edittext
				EditText tvOrderAmount = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvAmount);
				tvOrderAmount.setVisibility(View.VISIBLE);
				
				// Set value and show the textview
				TextView tvOrderAmount_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvAmount_ne);
				tvOrderAmount_ne.setVisibility(View.GONE);
				tvOrderAmount_ne.setText("");
			}
			
			// Execution type
			// hide the spinner
			Spinner executionType = (Spinner) mFragmentView.findViewById(R.id.trading_buy_spExecutionType);
			executionType.setVisibility(View.VISIBLE);
			
			// Set value and show the textview
			TextView executionType_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_spExecutionType_ne);
			executionType_ne.setVisibility(View.GONE);
			executionType_ne.setText("");
			
			
			// Order limit
			// hide the edittext
			EditText orderLimit = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit);
			
			// Set value and show the textview
			TextView orderLimit_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit_ne);
			if (orderLimit_ne.getVisibility() == View.VISIBLE){
				orderLimit_ne.setVisibility(View.GONE);
				orderLimit.setVisibility(View.VISIBLE);
			}
			orderLimit_ne.setText("");
			
			
			// trigger
			// hide the edittext
			EditText trigger = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvTrigger);
			
			// Set value and show the textview
			TextView trigger_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvTrigger_ne);
			if (trigger_ne.getVisibility() == View.VISIBLE){
				trigger.setVisibility(View.VISIBLE);
				trigger_ne.setVisibility(View.GONE);
			}
			trigger_ne.setText("");
			
			
			// execution date
			// hide the textview
			if (mustShowExpirationDate()){
				TextView executionDate = (TextView) mFragmentView.findViewById(R.id.trading_execution_datetime);
				executionDate.setVisibility(View.VISIBLE);

				// Set value and show the textview
				TextView executionDate_ne = (TextView) mFragmentView.findViewById(R.id.trading_execution_datetime_ne);
				executionDate_ne.setVisibility(View.GONE);
				executionDate_ne.setText("");
			}
			
		}else{
			// disable the fields
			
			// Order quantity
			// hide the edittext
			EditText tvOrderQuantity = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity);
			tvOrderQuantity.setVisibility(View.GONE);
			
			// Set value and show the textview
			TextView tvOrderQuantity_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvQuantity_ne);
			tvOrderQuantity_ne.setVisibility(View.VISIBLE);
			tvOrderQuantity_ne.setText(tvOrderQuantity.getText());
			
			if ( ((PositionSellFragmentInterface) getActivity()).isInstrumentMoneyTradable()){
				// Order amount
				// hide the edittext
				EditText tvOrderAmount = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvAmount);
				tvOrderAmount.setVisibility(View.GONE);
				
				// Set value and show the textview
				TextView tvOrderAmount_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvAmount_ne);
				tvOrderAmount_ne.setVisibility(View.VISIBLE);
				tvOrderAmount_ne.setText(tvOrderAmount.getText());
			}
			
			// Execution type
			// hide the spinner
			Spinner executionType = (Spinner) mFragmentView.findViewById(R.id.trading_buy_spExecutionType);
			executionType.setVisibility(View.GONE);
			
			// Set value and show the textview
			TextView executionType_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_spExecutionType_ne);
			executionType_ne.setVisibility(View.VISIBLE);
			executionType_ne.setText(getActivity().getResources().getIdentifier("trading_buy_"+((StexExecType)executionType.getSelectedItem()).toString().toLowerCase(), "string", getActivity().getPackageName()));
			
			
			// Order limit
			// hide the edittext
			EditText orderLimit = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit);
			
			// Set value and show the textview
			TextView orderLimit_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit_ne);
			if (orderLimit.getVisibility() == View.VISIBLE){
				orderLimit.setVisibility(View.GONE);
				orderLimit_ne.setVisibility(View.VISIBLE);
			}
			orderLimit_ne.setText(orderLimit.getText());
			
			
			// trigger
			// hide the edittext
			EditText trigger = (EditText) mFragmentView.findViewById(R.id.trading_buy_tvTrigger);
			
			// Set value and show the textview
			TextView trigger_ne = (TextView) mFragmentView.findViewById(R.id.trading_buy_tvTrigger_ne);
			if (trigger.getVisibility() == View.VISIBLE){
				trigger.setVisibility(View.GONE);
				trigger_ne.setVisibility(View.VISIBLE);
			}
			trigger_ne.setText(trigger.getText());
			
			
			// execution date
			// hide the textview
			if (mustShowExpirationDate()){
				TextView executionDate = (TextView) mFragmentView.findViewById(R.id.trading_execution_datetime);
				executionDate.setVisibility(View.GONE);

				// Set value and show the textview
				TextView executionDate_ne = (TextView) mFragmentView.findViewById(R.id.trading_execution_datetime_ne);
				executionDate_ne.setVisibility(View.VISIBLE);
				executionDate_ne.setText(executionDate.getText());
			}
			
		}
		
	}
	
	public void setIsEditable(boolean aIsEditable){
		isEditable = aIsEditable;
		setIsEditable();
	}
	
	public void setErrorQuantity(String errorQuantity) {
		mErrorQuantity = errorQuantity;
	}

	public void setErrorAmount(String errorAmount){
		mErrorAmount = errorAmount;
	}
	
	public void setErrorLimitPrice(String errorLimitPrice) {
		mErrorLimitPrice = errorLimitPrice;
	}

	public void setErrorTriggerPrice(String errorTriggerPrice) {
		mErrorTriggerPrice = errorTriggerPrice;
	}

	public void setErrorExecutionDate(String errorExecutionDate) {
		mErrorExecutionDate = errorExecutionDate;
	}

	public void resetErrors(){
		mErrorQuantity = "";
		mErrorAmount = "";
		mErrorLimitPrice = "";
		mErrorTriggerPrice = "";
		mErrorExecutionDate = "";		
	}
	
	public void displayErrors(){
		if (mErrorLimitPrice.length() > 0){
			((TextView) mFragmentView.findViewById(R.id.trading_error_limit)).setText(mErrorLimitPrice);
			((TextView) mFragmentView.findViewById(R.id.trading_error_limit)).setVisibility(View.VISIBLE);
		}else{
			((TextView) mFragmentView.findViewById(R.id.trading_error_limit)).setText("");
			((TextView) mFragmentView.findViewById(R.id.trading_error_limit)).setVisibility(View.GONE);
		}
		
		if (mErrorQuantity.length() > 0){
			((TextView) mFragmentView.findViewById(R.id.trading_error_quantity)).setText(mErrorQuantity);
			((TextView) mFragmentView.findViewById(R.id.trading_error_quantity)).setVisibility(View.VISIBLE);
		}else{
			((TextView) mFragmentView.findViewById(R.id.trading_error_quantity)).setText("");
			((TextView) mFragmentView.findViewById(R.id.trading_error_quantity)).setVisibility(View.GONE);	
		}
		
		if (mErrorAmount.length() > 0){
			((TextView) mFragmentView.findViewById(R.id.trading_error_amount)).setText(mErrorAmount);
			((TextView) mFragmentView.findViewById(R.id.trading_error_amount)).setVisibility(View.VISIBLE);
		}else{
			((TextView) mFragmentView.findViewById(R.id.trading_error_amount)).setText("");
			((TextView) mFragmentView.findViewById(R.id.trading_error_amount)).setVisibility(View.GONE);	
		}
		
		if (mErrorTriggerPrice.length() > 0){
			((TextView) mFragmentView.findViewById(R.id.trading_error_trigger)).setText(mErrorTriggerPrice);
			((TextView) mFragmentView.findViewById(R.id.trading_error_trigger)).setVisibility(View.VISIBLE);
		}else{
			((TextView) mFragmentView.findViewById(R.id.trading_error_trigger)).setText("");
			((TextView) mFragmentView.findViewById(R.id.trading_error_trigger)).setVisibility(View.GONE);
		}
		
		if (mErrorExecutionDate.length() > 0){
			((TextView) mFragmentView.findViewById(R.id.trading_error_execution)).setText(mErrorExecutionDate);
			((TextView) mFragmentView.findViewById(R.id.trading_error_execution)).setVisibility(View.VISIBLE);
		}else{
			((TextView) mFragmentView.findViewById(R.id.trading_error_execution)).setText("");
			((TextView) mFragmentView.findViewById(R.id.trading_error_execution)).setVisibility(View.GONE);
		}		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mFragmentView = inflater.inflate(R.layout.trd_position_sell_fragment, container, false);

		// Number of decimals used for the quantity input,
		if (((PositionSellFragmentInterface) getActivity()).getInstrumentType() == StexAssetGroupType.BONDS){
			mDecimalsForQuantity = 6;
		}else{
			mDecimalsForQuantity = 0;
		}
		
		
		mEstimateField = (TextView)mFragmentView.findViewById(R.id.trading_buy_tvEstimate);
		
		if (!AvaloqApplication.getInstance().getConfiguration().hideMarketData()){			
			// show estimated field
			mFragmentView.findViewById(R.id.trading_buy_llEstimate).setVisibility(View.VISIBLE);			
		}
		
		requestSellListData();
		
		requestUserSettings();

		setHintAndFilterForInputs(mFragmentView);

		populateSpinners();

		validateFields();
		
		executionTypeSelected();
		durationTypeSelected();
		
		displayErrors();
		
		// the instrument is "money tradable"
		if ( ((PositionSellFragmentInterface) getActivity()).isInstrumentMoneyTradable()){
			mFragmentView.findViewById(R.id.trading_buy_llAmount).setVisibility(View.VISIBLE);
			
			// Add the change listeners on the Amount and quantity: When the amount is
			// entered, the quantity should be removed and vice versa
			((TextView)mFragmentView.findViewById(R.id.trading_buy_tvAmount)).addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					if (after == 0) return;
					if (((TextView)mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).getText().toString().length() > 0){
						((TextView)mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).setText("");
					}					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					validateFields();	
					showEstimate();
				}
			});
			
			
			((TextView)mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					if (after == 0) return;
					if (((TextView)mFragmentView.findViewById(R.id.trading_buy_tvAmount)).getText().toString().length() > 0){
						((TextView)mFragmentView.findViewById(R.id.trading_buy_tvAmount)).setText("");
					}
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					validateFields();	
					showEstimate();
				}
			});
		}else{
			mFragmentView.findViewById(R.id.trading_buy_llAmount).setVisibility(View.GONE);
			
			((TextView)mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// not used
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// not used
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					validateFields();	
					showEstimate();
				}
			});
		}
		
		return mFragmentView;
	}

	/**
	 * 
	 */
	private void populateSpinners() {

		/*
		 * Set the values and change listeners for the execution type spinner
		 */
		Spinner spExecutionType = (Spinner) mFragmentView.findViewById(R.id.trading_buy_spExecutionType);
		
		CustomSpinnerAdapter<StexExecType> dataAdapter = new CustomSpinnerAdapter<StexExecType>(getActivity(), android.R.layout.simple_spinner_item, mExecTypes, null, false) {
			@Override
			protected String getTextResourceName(StexExecType t) {
				return "trading_buy_" + t.toString().toLowerCase();
			}
		};

		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spExecutionType.setAdapter(dataAdapter);

		spExecutionType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				mSelectedExecutionType = arg2;
				executionTypeSelected();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// do nothing
			}
		});

		/*
		 * Set the values and change listeners for the execution type spinner
		 */
		durationTypeSelected();
		
		if (mustShowExpirationDate()){
			// add click listener to the date select textview
			((TextView) mFragmentView.findViewById(R.id.trading_execution_datetime)).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
							Calendar cal = Calendar.getInstance();
							cal.set(Calendar.YEAR, year);
							cal.set(Calendar.MONTH, monthOfYear);
							cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
							mDurationDate = cal.getTime();
							durationTypeSelected();
						}

					};

					Calendar calendar = Calendar.getInstance();
					calendar.setTime(mDurationDate);

					DatePickerDialog dpd = new DatePickerDialog(getActivity(), datePickerListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));

					dpd.show();
				}
			});	
		}else{
			// hide the row with expiration date field
			mFragmentView.findViewById(R.id.trd_sell_expiration).setVisibility(View.GONE);
		}
	}

	/**
	 * 
	 */
	private void executionTypeSelected() {
		if (mExecTypes[mSelectedExecutionType] == StexExecType.MARKET) {
			// hide Order limit and Trigger, and clear their contents
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).setText("");
			((View) mFragmentView.findViewById(R.id.trading_buy_llOrderLimit)).setVisibility(View.GONE);
			((View) mFragmentView.findViewById(R.id.trading_error_limit)).setVisibility(View.GONE); 
			
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvTrigger)).setText("");
			((View) mFragmentView.findViewById(R.id.trading_buy_llTrigger)).setVisibility(View.GONE);
			((View) mFragmentView.findViewById(R.id.trading_error_trigger)).setVisibility(View.GONE);
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.LIMIT) {
			// show Order limit, hide Trigger
			((View) mFragmentView.findViewById(R.id.trading_buy_llOrderLimit)).setVisibility(View.VISIBLE);

			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvTrigger)).setText("");
			((View) mFragmentView.findViewById(R.id.trading_buy_llTrigger)).setVisibility(View.GONE);
			((View) mFragmentView.findViewById(R.id.trading_error_trigger)).setVisibility(View.GONE);
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.STOP) {
			// hide Order limit, show Trigger
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).setText("");
			((View) mFragmentView.findViewById(R.id.trading_buy_llOrderLimit)).setVisibility(View.GONE);
			((View) mFragmentView.findViewById(R.id.trading_error_limit)).setVisibility(View.GONE);

			((View) mFragmentView.findViewById(R.id.trading_buy_llTrigger)).setVisibility(View.VISIBLE);
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.STOP_LIMIT) {
			// show Order limit and Trigger
			((View) mFragmentView.findViewById(R.id.trading_buy_llOrderLimit)).setVisibility(View.VISIBLE);

			((View) mFragmentView.findViewById(R.id.trading_buy_llTrigger)).setVisibility(View.VISIBLE);
		}
		validateFields();
	}

	/**
	 * 
	 */
	private void durationTypeSelected() {

		if (mustShowExpirationDate()){
			if (mDurationDate == null){
				mDurationDate = Calendar.getInstance().getTime();
			}

			String strDate = DateFormat.getDateFormat(getActivity().getApplicationContext()).format(mDurationDate); 

			((TextView) mFragmentView.findViewById(R.id.trading_execution_datetime)).setText(strDate);
		}
	}

	/**
	 * 
	 * @param fragmentView
	 */
	private void setHintAndFilterForInputs(View fragmentView) {
		// prepare the hint for the currency input
		String hint = "0";
		if (((PositionSellFragmentInterface) getActivity()).getInstrumentType() == StexAssetGroupType.BONDS){
			hint = "0" + DecimalFormatSymbols.getInstance().getDecimalSeparator() + "00";
		}
		

		// set the filter and the hint for the quantity input field
		//((EditText) fragmentView.findViewById(R.id.trading_buy_tvQuantity)).setFilters(new InputFilter[] { new CurrencyFormatInputFilter(0.0, mBankingPosition.getQuantity()) });
		// Data about the currently held amount is not received yet, thus the filter cannot be set and the input is disabled
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvQuantity)).setEnabled(false);
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvQuantity)).setHint(hint);
		
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvAmount)).setEnabled(false);
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvAmount)).setHint(hint);		

		
		// set the filter and the hint for the order input field
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).setFilters(new InputFilter[] { new CurrencyFormatInputFilter(new BigDecimal(0.0), null, mDecimalsForQuantity) });
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).setHint(hint);
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// not used
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// not used
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				validateFields();
				showEstimate();
			}
		});

		// set the filter and the hint for the trigger input field
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvTrigger)).setFilters(new InputFilter[] { new CurrencyFormatInputFilter(new BigDecimal(0.0), null, mDecimalsForQuantity) });
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvTrigger)).setHint(hint);
		((EditText) fragmentView.findViewById(R.id.trading_buy_tvTrigger)).addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// not used
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// not used
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				validateFields();
				showEstimate();
			}
		});
	}

	private void requestSellListData(){
		
		mBankingPosition = ((PositionSellFragmentInterface)getActivity()).getInstrument();
		setOrderQuantity();
	}
	
	/**
	 * Update the dataset and notify the listeners on the UI thread
	 * @param aRequest
	 */
	protected void receivedPositionResults(final AbstractServerRequest<PositionsResult> aRequest) {
		
		PositionsResult positionResult = aRequest.getResponse().getData();
		if (positionResult != null) {

			Long lookupPosition = ((PositionSellFragmentInterface)getActivity()).getBankingPositionId();
			List<BankingPositionTO> positions = positionResult.getPositions();
			for (BankingPositionTO position: positions){
				
				if (position.getId().longValue() == lookupPosition.longValue()){
					
					mBankingPosition = position;
					setOrderQuantity();
				}
			}
		}				
	}
	
	/**
	 * Gets the information about the default account and portfolio
	 */
	private void requestUserSettings(){
		RequestStateEvent<UserSettingsTradingRequest> rse = new RequestStateEvent<UserSettingsTradingRequest>() {
			@Override
			public void onRequestCompleted(UserSettingsTradingRequest stexDefaultsRequest) {
				UserSettingsTradingResult result = stexDefaultsRequest.getResponse().getData();
				
				if (result != null){
					mDefaultAccountId = result.getTradingSettings().getDefaultSettlementAccount();
					mDefaultPortfolioId = result.getTradingSettings().getDefaultTradingPortfolio();
				}
				requestStexDefaults();
			}
		};

		// Initiate the request
		UserSettingsTradingRequest request = UserSettingsService.getTradingSettings(rse);

		// Add the request to the request queue
		request.initiateServerRequest();
	}
	
	/**
	 * Requests the Stex defaults to get the currency information
	 */
	private void requestStexDefaults() {

		RequestStateEvent<TradingDefaultsRequest> rse = new RequestStateEvent<TradingDefaultsRequest>() {
			@Override
			public void onRequestCompleted(TradingDefaultsRequest stexDefaultsRequest) {
				receiveStexDefaults(stexDefaultsRequest);
			}
		};

		// Initiate the request
		TradingDefaultsRequest request = TradingService.getTradingDefaults(rse);

		// Add some debug info
		request.setRequestIdentifier(request.getRequestIdentifier());

		// Add the request to the request queue
		request.initiateServerRequest();
	}

	/**
	 * Callback when receiving the stex defaults
	 * 
	 * @param stexDefaultsRequest
	 */
	private void receiveStexDefaults(TradingDefaultsRequest stexDefaultsRequest) {

		TradingDefaultsResult stexDefaultsResult = stexDefaultsRequest.getResponse().getData();
		if (stexDefaultsResult != null) {

			// pass the accounts to the activity
			List<PositionAccountRecord> accounts = new ArrayList<PositionAccountRecord>();
			for (MoneyAccountTO account : stexDefaultsResult.getAccounts()) {
				// add only the accounts that belong to the portfolio for this banking position
				if (mBankingPosition.getPortfolioId().longValue() == account.getPortfolioId().longValue()){
					PositionAccountRecord accountRecord = new PositionAccountRecord();
					accountRecord.id = account.getId();
					accountRecord.Name = account.getAccountType();
					accountRecord.Description = account.getAccountIban();
					accountRecord.Amount = CurrencyUtil.formatMoney(account.getAmount(), account.getCurrencyId());
					accountRecord.parentId = account.getPortfolioId();
					accounts.add(accountRecord);
				}
			}

			if (getActivity() == null) return;
			((PositionSellFragmentInterface) getActivity()).accountRecordsReceived(accounts, PositionBuyAccountRecordType.ACCOUNT);

			// pass the accounts to the activity
			List<PositionAccountRecord> portfolios = new ArrayList<PositionAccountRecord>();
			for (ContainerPortfolioTO portfolio : stexDefaultsResult.getPortfolios()) {
				
				// add only the portfolio that matches the portfolio ID of the currently traded banking position
				if (mBankingPosition.getPortfolioId().longValue() == portfolio.getId().longValue()){
					PositionAccountRecord portfolioRecord = new PositionAccountRecord();
					portfolioRecord.id = portfolio.getId();
					portfolioRecord.Name = portfolio.getName();
					portfolioRecord.Description = portfolio.getBusinessPartnerName();
					portfolioRecord.Amount = CurrencyUtil.formatMoney(portfolio.getTotalValue(), portfolio.getCurrencyId());
					portfolios.add(portfolioRecord);
				}
			}

			((PositionSellFragmentInterface) getActivity()).accountRecordsReceived(portfolios, PositionBuyAccountRecordType.PORTFOLIO);
		}
	}
	
	/**
	 * Depending on the values entered in the form, show or hide the Confirm button
	 */
	private void validateFields(){
		
		if (fieldsAreValid()){
			setConfirmButtonEnabled(true);
		}else{
			setConfirmButtonEnabled(false);
		}
	}
	
	/**
	 * Enables/disables the confirm button
	 * 
	 * @param state boolean
	 */
	public void setConfirmButtonEnabled(boolean state){
		if (getActivity() == null) return;
		((PositionSellFragmentInterface) getActivity()).setConfirmButtonEnabled(state);
	}
	
	/**
	 * Verify that the values in the fields are correct
	 * 
	 * @return
	 */
	public boolean fieldsAreValid(){
		
		// validate quantity
		if (getOrderQuantity() == null && getOrderAmount() == null) return false;
		
		// if the execution type is MARKET
		if (mExecTypes[mSelectedExecutionType] == StexExecType.MARKET) {
			// no need to validate trigger and limit
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.LIMIT) {
			// validate limit
			if (getLimitPrice() == null) return false;
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.STOP) {
			// validate trigger
			if (getTriggerPrice() == null) return false;
		} else if (mExecTypes[mSelectedExecutionType] == StexExecType.STOP_LIMIT) {
			// validate Order limit and Trigger
			if (getLimitPrice() == null) return false;
			if (getTriggerPrice() == null) return false;
		}
		
		return true;
	}
	
	/*
	 * The following methods will return data to the activity
	 */

	/**
	 * Returns the expiration date according to the values selected in the
	 * spinner. The date is already adjusted for weekends, that is the date
	 * returned here is not a Saturday or a Sunday
	 * 
	 * @return
	 */
	public Date getExpirationDate() {
		return mDurationDate;
	}

	/**
	 * Returns the order quantity by parsing the text in the TextView
	 * 
	 * @return
	 */
	public BigDecimal getOrderQuantity() {	
		return CurrencyFormatInputFilter.parseNumber(((EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).getText().toString());		
	}

	/**
	 * Returns the order amount by parsing the text in the TextView
	 * 
	 * @return
	 */
	public BigDecimal getOrderAmount() {
		return CurrencyFormatInputFilter.parseNumber( ((EditText) mFragmentView.findViewById(R.id.trading_buy_tvAmount)).getText().toString() );
	}
	
	public Long getDefaultAccountId() {		
		return mDefaultAccountId;
	}

	public Long getDefaultPortfolioId() {
		return mDefaultPortfolioId;
	}

	/**
	 * Sets the order quantity in the input field
	 */
	public void setOrderQuantity() {
		if (mBankingPosition != null && getActivity() != null){			
			
			if (((PositionSellFragmentInterface) getActivity()).getInstrumentType() == StexAssetGroupType.BONDS){
				((EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).setText(CurrencyUtil.formatInputNumber(mBankingPosition.getQuantity()));
			}else{
				((EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).setText(mBankingPosition.getQuantity().toString());
			}
						
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).setEnabled(true);
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvQuantity)).setFilters(new InputFilter[] { new CurrencyFormatInputFilter(new BigDecimal(0.0), null, mDecimalsForQuantity) });
			
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvAmount)).setEnabled(true);
			((EditText) mFragmentView.findViewById(R.id.trading_buy_tvAmount)).setFilters(new InputFilter[] { new CurrencyFormatInputFilter(new BigDecimal(0.0), null, 2) });
			showEstimate();
		}
	}
	
	/**
	 * Returns the execution type selected in the spinner
	 * 
	 * @return
	 */
	public StexExecType getStexExecType() {
		return StexExecType.values()[mSelectedExecutionType];
	}

	/**
	 * Returns the order limit price by parsing the text in the TextView
	 * 
	 * @return
	 */
	public BigDecimal getLimitPrice() {		
		return CurrencyFormatInputFilter.parseNumber(((EditText) mFragmentView.findViewById(R.id.trading_buy_tvOrderLimit)).getText().toString());
	}

	/**
	 * Returns the order trigger price by parsing the text in the TextView
	 * 
	 * @return
	 */
	public BigDecimal getTriggerPrice() {		
		return CurrencyFormatInputFilter.parseNumber(((EditText) mFragmentView.findViewById(R.id.trading_buy_tvTrigger)).getText().toString());
	}
	
	/**
	 * In some cases the expiration date does not have to be set. https://jira.insign.ch/browse/AVQ-270
	 * @return
	 */
	public boolean mustShowExpirationDate(){		
		if (((PositionSellFragmentInterface) getActivity()).getInstrumentType() == StexAssetGroupType.FUNDS){
			return AvaloqApplication.getInstance().getConfiguration().showExpDateOnFundSale();
		}
		return true;
	}
}
