package com.avaloq.banklet.trading;

import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.avaloq.afs.aggregation.to.marketdata.MarketDataChartResult;
import com.avaloq.afs.server.bsp.client.ws.ChartPeriod;
import com.avaloq.afs.server.bsp.client.ws.MarketDataChartQueryTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataChartRequest;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataService;
import com.avaloq.framework.util.Tools;

public class TradingSellDetailChartFullscreen extends Activity {

	public static final String EXTRA_CHART_PERIOD = "extraChartPeriod";
	public static final String EXTRA_CHART_INSTRUMENT_ID = "extraInstrumentId";
	public static final String EXTRA_CHART_MARKET_ID = "extraMarketId";
	
	private static ChartPeriod mChartPeriod = ChartPeriod.ONE_YEAR;
	private static Long mInstrumentId;
	
	// holders for the charts
	private static Bitmap mChartPortrait = null;	
	private static Bitmap mChartLandscape = null;

    private static Long mMarketId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trd_sell_detail_chart_fullscreen_activity);
		
		Bundle b = getIntent().getExtras();
		mChartPeriod = (ChartPeriod) b.get(EXTRA_CHART_PERIOD);
		mInstrumentId = b.getLong(EXTRA_CHART_INSTRUMENT_ID);
		mMarketId = b.getLong(EXTRA_CHART_MARKET_ID);
		requestAllCharts();
		
		attachClickListenerToChart();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.		
		return true;
	}

	/**
	 * 
	 */
	private void attachClickListenerToChart(){
		/*
		 * When taping the chart, it must close the activity
		 */
		((ImageView) findViewById(R.id.trading_ivChart)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				return;
			}
		});		
	}
	
	/**
	 * Request both charts (for landscape and portrait) from the webservice.
	 * Both images are request for a better UX when the user is rotating the device
	 * First, the image for the current orientation is requested
	 */
	private void requestAllCharts(){
		// get the market data charts		
		requestMarketDataChart(getResources().getConfiguration().orientation);
		
		// additionaly request the portrait/landscape image
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
			requestMarketDataChart(Configuration.ORIENTATION_LANDSCAPE);
		}else{
			requestMarketDataChart(Configuration.ORIENTATION_PORTRAIT);
		}
	}
	
	
	/**
	 * Requests the data chart
	 * 
	 * @param orientation - The orientation for which the chart is needed
	 */
	private void requestMarketDataChart(final int orientation){
						
		MarketDataChartQueryTO query = new MarketDataChartQueryTO();
		query.setChartPeriod(mChartPeriod);
		query.setCurrencyId(null);
		query.setInstrumentId(mInstrumentId);
		query.setMarketId(mMarketId);
		
		// We have to request the correct image for a specific orientation of the screen
		
		int chartImageHeight = 0;
		int chartImageWidth = 0;
		
		chartImageWidth = Tools.getDisplaySize(getBaseContext()).x;
		chartImageHeight = Tools.getDisplaySize(getBaseContext()).y;

		// if we are currently in portrait mode and requesting the portrait chart, then
		// we use X and Y as they are, but if we are currently in landscape, then we have to swap
		// the values for screen size such that we correctly get the width and height of the image
		if (orientation != getResources().getConfiguration().orientation){
			chartImageWidth = Tools.getDisplaySize(getBaseContext()).y;
			chartImageHeight = Tools.getDisplaySize(getBaseContext()).x;
		}
		
		query.setHeight(chartImageHeight);
		query.setWidth(chartImageWidth);
		
		RequestStateEvent<MarketDataChartRequest> rse = new RequestStateEvent<MarketDataChartRequest>() {
			@Override
			public void onRequestCompleted(MarketDataChartRequest marketDataChartRequest) {		
				try{
					
					showMarketDataChart(marketDataChartRequest, orientation);
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}			
		};
		
		// Initiate the request 
		MarketDataChartRequest request = MarketDataService.getMarketDataChart(query, rse);					
	
		// Add some debug info
		request.setRequestIdentifier(request.getRequestIdentifier() );
	
		// Add the request to the request queue
		request.initiateServerRequest();
	}
	
	/**
	 * Callback function. Called when the market data chart is received from the server
	 *
	 */
	private void showMarketDataChart(final AbstractServerRequest<MarketDataChartResult> aRequest, final int orientation){
		MarketDataChartResult marketDataChartResult = aRequest.getResponse().getData();
		if (marketDataChartResult != null) {
			
			// get the bytes out of the Base64 encoded string
			byte[] imageAsBytes = Base64.decode(marketDataChartResult.getImageDataBase64(), Base64.DEFAULT);
			
			if (orientation == Configuration.ORIENTATION_LANDSCAPE){						
				mChartLandscape = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			}else{						
				mChartPortrait = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			}
			 
			displayChart(orientation);
		}
	}
	
	/**
	 * Display the correct chart
	 * @param orientation
	 */
	private void displayChart(int orientation){
		
		ImageView image = (ImageView) findViewById(R.id.trading_ivChart);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			if (mChartLandscape != null){
				// display image in landscape
				image.setImageDrawable(null);
				image.setImageBitmap(mChartLandscape);				
			}
		}else{
			if (mChartPortrait != null){
				// display image in portrait
				image.setImageBitmap(null);				
				image.setImageBitmap(mChartPortrait);
				
			}
		}
	}
}
