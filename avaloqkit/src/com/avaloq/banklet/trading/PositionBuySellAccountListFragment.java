package com.avaloq.banklet.trading;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.banklet.trading.PositionBuySellSelectAccountFragment.PositionBuySellSelectType;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class PositionBuySellAccountListFragment extends BankletFragment {
		
	private ListView mLvAcounts;
	private PositionBuySellSelectType mType;
	private int mSelectedPosition;
	
	/**
	 * Interface that has to be implemented by the hosting activity
	 */
	public interface PositionBuySellAccountListFragmentInterface{
		public void listAccountItemSelected(PositionBuySellSelectType type, int position);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.trd_position_buy_sell_fragment_select_account_list, container, false);

		mLvAcounts = (ListView) fragmentView.findViewById(R.id.trading_buy_account_lvAccounts);

		return fragmentView;
	}

	/**
	 * Called by the activity. Passes the account list to be displayed and the selected position that has to be highlighted
	 * @param list
	 * @param context
	 * @param type
	 * @param selectedPosition
	 */
	public void setAccountList(List<PositionAccountRecord> list, Context context, PositionBuySellSelectType type, int selectedPosition){
		mType = type;
		mSelectedPosition = selectedPosition;
		AccountListAdapter adapter = new AccountListAdapter(context,
                R.layout.trd_position_buy_sell_fragment_select_account_list_item, list);
		mLvAcounts.setAdapter(adapter);
	}

	/**
	 * Adapter for the account list
	 * 
	 * @author victor
	 *
	 */
	private class AccountListAdapter extends ArrayAdapter<PositionAccountRecord>{

	    Context context;
	    int layoutResourceId;   
	    List<PositionAccountRecord> data = null;
	   
	    public AccountListAdapter(Context context, int layoutResourceId, List<PositionAccountRecord> data) {
	        super(context, layoutResourceId, data);
	        this.layoutResourceId = layoutResourceId;
	        this.context = context;
	        this.data = data;	        
	    }	 
	    
	    @Override
	    public int getCount(){	    	
	    	return data.size();
	    }
	    
	    @Override
	    public View getView(final int position, View convertView, ViewGroup parent) {
	        View row = convertView;
	        AccountHolder holder = null;
	        
	        if(row == null)
	        {
	            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
	            row = inflater.inflate(layoutResourceId, parent, false);
	           
	            holder = new AccountHolder();
	            
	            holder.tvName = (TextView) row.findViewById(R.id.trading_buy_account_list_tvName);
		        holder.tvDescription = (TextView) row.findViewById(R.id.trading_buy_account_list_tvDescription);
		        holder.tvAmount = (TextView) row.findViewById(R.id.trading_buy_account_list_tvAmount);
	            holder.imIndicator = (ImageView) row.findViewById(R.id.trading_buy_account_list_imIndicator);
		        
	            row.setTag(holder);
	        }
	        else
	        {
	            holder = (AccountHolder)row.getTag();
	        }
	       
	        PositionAccountRecord account = data.get(position);
	        
	        holder.tvName.setText(account.Name);
	        holder.tvDescription.setText(account.Description);
	        holder.tvAmount.setText(account.Amount);
	        
	        if (position == mSelectedPosition){
	        	holder.imIndicator.setVisibility(View.VISIBLE);
	        }else{
	        	holder.imIndicator.setVisibility(View.GONE);
	        }
	        
	        row.setOnClickListener(new OnClickListener() {
				/**
				 * When an account is selected, inform the activity about this
				 */
				@Override
				public void onClick(View v) {
					mSelectedPosition = position;
					notifyDataSetChanged();					
					((PositionBuySellAccountListFragmentInterface) getActivity()).listAccountItemSelected(mType, position); 
				}
			});
	        
	        return row;
	    }
	   
	    class AccountHolder
	    {
	        TextView tvName;
	        TextView tvDescription;
	        TextView tvAmount;
	        ImageView imIndicator;
	    }
	}

	
}
