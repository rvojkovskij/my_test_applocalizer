/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */

package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.trading.PositionsResult;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionTO;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MdsInstrumentKeyType;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.PositionsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.tools.ProgressiveBaseAdapter;
import com.avaloq.framework.util.CurrencyUtil;

public abstract class SellListAdapter extends ProgressiveBaseAdapter{

	protected static final String TAG = "SellListAdapter";

	private static String filterString = "";
	
	/*
	 * The list of porfolios is received from the server with a separate service call.
	 * It contains a lot of data, and for fast access, it will be written to a hash map
	 */
	private static HashMap<Long,String> portfolios = new HashMap<Long,String>();
		
	private List<BankingPositionTO> mResultsAll = new ArrayList<BankingPositionTO>();

	private String mCurrentRequestIdentifier = "";
	
	private Fragment mFragment;

	static class ViewHolder {
		public TextView name;
		public TextView ticker;
		public TextView isin;
		public TextView valor;
		public TextView currency;
		public TextView instrumentType;
		public Long instrumentId;
		public TextView quantity;
		public String instrumentSubType;
		public Long id;
		public TextView portfolioHolder;
		public TextView portfolioName;
		public TextView tvInstrumentSubType;
		public Long currencyId;
		public Long marketId;
		public String marketName;
		public String strIsin;
		public Long portfolioId;
		public ImageView image;
		public boolean isMoneyTradable;
		public StexAssetGroupType stexAssettGroupType;
		public BankingPositionTO instrument;
	}
	/**
	 * Constructor
	 * @param context
	 */
	public SellListAdapter(Fragment activity) {
		super(activity.getActivity());
		this.mFragment = activity;
	}

	
	public void requestSellListData(BankingPositionListQueryTO query, boolean refreshList){
		if (portfolios == null){
			requestStexDefaults(query, refreshList);
			return;
		}
		query.setMaxResultSize(refreshList ? mResultPerPage : (mResultPerPage+mResultsAll.size())); 
		query.setOnlyTradable(true);
		query.setInstrumentIdentificationKeyType(MdsInstrumentKeyType.ANY);
		query.setInklSold(false);		

		loadingStateChanged(true);
		
		// Define the callback
		RequestStateEvent<PositionsRequest> rse = new RequestStateEvent<PositionsRequest>() {
			@Override
			public void onRequestCompleted(PositionsRequest positionsRequest) {								
				receivedPositionResults(positionsRequest);	
				loadingStateChanged(false);
			}			
		};

		// Initiate the request 
		PositionsRequest request = TradingService.findPositionList(
				query, 
				0L, 
				refreshList ? Long.valueOf(mResultPerPage) : Long.valueOf((mResultPerPage+mResultsAll.size())), 
				rse);	
		if (refreshList)reset();
		refreshList = false;

		// Add the request to the request queue
		request.initiateServerRequest();
	}

	/**
	 * Requests the Stex defaults to get the currency information
	 */
	private void requestStexDefaults(final BankingPositionListQueryTO query, final boolean refreshList){
		loadingStateChanged(true);
		RequestStateEvent<TradingDefaultsRequest> rse = new RequestStateEvent<TradingDefaultsRequest>() {
			@Override
			public void onRequestCompleted(TradingDefaultsRequest stexDefaultsRequest) {
				if (!mCurrentRequestIdentifier.isEmpty() && mCurrentRequestIdentifier.compareTo(stexDefaultsRequest.getRequestIdentifier()) == 0){
					receiveStexDefaults(stexDefaultsRequest, query, refreshList);	
					showListLoading(false);
				}
			}			
		};
		
		// Initiate the request 
		TradingDefaultsRequest request = TradingService.getTradingDefaults(rse);			
	
		// Add some debug info
		request.setRequestIdentifier(request.getRequestIdentifier());
			
		mCurrentRequestIdentifier = request.getRequestIdentifier();
		
		// Add the request to the request queue
		request.initiateServerRequest();		
	}
	
	/**
	 * Callback when receiving the stex defaults
	 * 
	 * @param stexDefaultsRequest
	 */
	private void receiveStexDefaults(TradingDefaultsRequest stexDefaultsRequest, BankingPositionListQueryTO query, boolean refreshList){
		
		TradingDefaultsResult stexDefaultsResult = stexDefaultsRequest.getResponse().getData();
		if (stexDefaultsResult != null) {
			
			portfolios.clear();
			
			
			if (stexDefaultsResult.getPortfolios() != null){
				// create a hashmap to store all the currencies
				for(ContainerPortfolioTO portfolio: stexDefaultsResult.getPortfolios()){
					portfolios.put(portfolio.getId(), portfolio.getName());
				}
				
				// get the market data
				requestSellListData(query, refreshList);
			}
		}	
		showListLoading(false);
	}

	public void cancelRequest(){
		mCurrentRequestIdentifier = "";
		loadingStateChanged(false);
		showListLoading(false);
	}
	
	/**
	 * Update the dataset and notify the listeners on the UI thread
	 * @param aRequest
	 */
	protected void receivedPositionResults(final AbstractServerRequest<PositionsResult> aRequest) {
		PositionsResult positionResult = aRequest.getResponse().getData();
		if (positionResult != null) {

			List<BankingPositionTO> positions = positionResult.getPositions();
			mResultsAll.clear();
			mResultsAll.addAll(positions);

			notifyDataSetChanged();
		}
		showListLoading(false);
	}

	@Override
	public int getCount() {
		return mResultsAll.size();
	}
		
	@Override
	public Object getItem(int position) {
		return mResultsAll.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		final BankingPositionTO item = (BankingPositionTO) getItem(position);

		// nothing to recycle, create a new row
		if (rowView == null) {
			LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
			rowView = inflater.inflate(R.layout.trd_sell_list_item, null);

			ViewHolder viewHolder = new ViewHolder();	      
			viewHolder.name = (TextView) rowView.findViewById(R.id.trd_buy_list_name);
			viewHolder.isin = (TextView) rowView.findViewById(R.id.trd_buy_list_isin);
			viewHolder.valor = (TextView) rowView.findViewById(R.id.trd_buy_list_valor);
			viewHolder.currency = (TextView) rowView.findViewById(R.id.trd_buy_list_currency);
			
			viewHolder.instrumentType = (TextView) rowView.findViewById(R.id.trd_buy_list_instrumenttype);
			viewHolder.instrumentId = item.getInstrument().getId();
			viewHolder.quantity = (TextView) rowView.findViewById(R.id.trd_buy_list_value);
			viewHolder.portfolioHolder = (TextView) rowView.findViewById(R.id.trd_buy_list_portfolioHolderSub);
			viewHolder.portfolioName= (TextView) rowView.findViewById(R.id.trd_buy_list_portfolioHolder);
			viewHolder.tvInstrumentSubType = (TextView) rowView.findViewById(R.id.trd_buy_list_instrumentsubtype);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.trading_type_image);
			rowView.setTag(viewHolder);
		}


		final ViewHolder holder = (ViewHolder) rowView.getTag();

		holder.isMoneyTradable = item.getInstrument().isMoneyDrivenTradingCapable() && AvaloqApplication.getInstance().getConfiguration().allowedSellFundsByAmount();
		
		// Assign the correct values to the views of this row
		holder.name.setText(item.getInstrument().getTitle());		
		if (item.getInstrument().getIsin() != null){
			holder.isin.setText(item.getInstrument().getIsin());
		}
		
		holder.strIsin = item.getInstrument().getIsin();
		
		if (item.getInstrument().getValor() != null){
			holder.valor.setText(item.getInstrument().getValor());
		}
		holder.instrumentType.setText(item.getInstrument().getStexGroupType().toString().substring(0, 1).toUpperCase()+item.getInstrument().getStexGroupType().toString().toLowerCase().substring(1));
		holder.stexAssettGroupType = item.getInstrument().getStexGroupType();
		
		holder.instrumentId = item.getInstrument().getId();
		holder.instrumentSubType = item.getInstrument().getStexSubTypeName();

		if (holder.tvInstrumentSubType != null){
			holder.tvInstrumentSubType.setText(holder.instrumentSubType);
		}

		if (holder.portfolioHolder != null){
			holder.portfolioHolder.setText(item.getBusinessPartnerName());
		}
		
		if (holder.portfolioName != null){
			holder.portfolioName.setText(portfolios.get(item.getPortfolioId()));
		}
		
		

		holder.quantity.setText(CurrencyUtil.formatMoney(item.getQuantity()));
		holder.id = item.getId();

		holder.currencyId = item.getInstrument().getDefaultListing().getCurrencyId(); 
		holder.currency.setText(AvaloqApplication.getInstance().findCurrencyById(item.getInstrument().getDefaultListing().getCurrencyId()).getIsoCode());
		holder.marketId = item.getInstrument().getDefaultListing().getMarketId(); 
		holder.marketName = item.getInstrument().getDefaultListing().getMarketName();
		holder.portfolioId = item.getPortfolioId();
		holder.instrument = item;
		
		if (getCorrectIcon(item.getInstrument().getStexGroupType()) != null){
			holder.image.setImageDrawable(getCorrectIcon(item.getInstrument().getStexGroupType()));
		}
		
		// set the listener for the button
		if (rowView.findViewById(R.id.trading_list_btnSell) != null){
			rowView.findViewById(R.id.trading_list_btnSell).setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent();
					intent.setClass(mFragment.getActivity().getBaseContext(), PositionSellActivity.class);

					// add extra parameters to be passed to the called activity here

					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT, holder.instrument);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_TYPE, holder.stexAssettGroupType.toString());
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.strIsin);					
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);										
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_PORTFOLIO_ID, holder.portfolioId);					
					intent.putExtra(PositionSellActivity.EXTRA_BANKING_POSITION_ID, holder.id);
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					
					mFragment.startActivity(intent);
				}
			});
		}

		checkIfMustLoadNextData(position);
		
		return rowView;
	}

	/**
	 * Returns the correct value for Bonds, Equities or funds
	 */
	private Drawable getCorrectIcon(StexAssetGroupType instrumentType){		
		if (instrumentType == StexAssetGroupType.BONDS){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_bonds_small);			
		}else if (instrumentType == StexAssetGroupType.EQUITIES){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_equities_small);
		}else if (instrumentType == StexAssetGroupType.FUNDS){			
			return mFragment.getResources().getDrawable(R.drawable.trd_icon_funds_small);
		}
		return null;
	}
	
	/**
	 * Returns the header view for the instrument list. Contains the
	 * header with all the strings translated and formatted to match the list
	 * 
	 * @return View - The header view that can be directly added to the list.
	 */
	public View getHeaderView() {
		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		View header = inflater.inflate(R.layout.trd_sell_list_header, null);
		return header;
	}

}
