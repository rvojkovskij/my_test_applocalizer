package com.avaloq.banklet.trading;

import java.util.List;

import com.avaloq.framework.R;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;

public class HighlightedSpinnerAdapter<T> extends ArrayAdapter<T> implements SpinnerAdapter{

	private int mHighlightedPosition = -1;
	private LayoutInflater layoutInflater;	
	
	public HighlightedSpinnerAdapter(Context context, int textViewResourceId, List<T> objects) {
		super(context, textViewResourceId, objects);		
		layoutInflater = LayoutInflater.from(context);		
	}
	
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
    	View view = super.getDropDownView(position, convertView, parent);    	
    	
        if (mHighlightedPosition >= 0 && mHighlightedPosition == position){
        	view.setBackgroundColor(Color.WHITE);
        	
        }else{        	
        	view.setBackgroundDrawable(null);        	
        }    
        
        return view;
    }
    
    public void setHighlightedPosition(int position){
    	mHighlightedPosition = position;
    	notifyDataSetChanged();
    }
}