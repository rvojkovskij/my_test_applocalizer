package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.res.Resources.NotFoundException;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.trading.StexOrdersResult;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderDetailTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderStateType;
import com.avaloq.afs.server.bsp.client.ws.StexOrderType;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.PositionsRequest;
import com.avaloq.framework.comms.webservice.trading.StexOrdersRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.tools.ProgressiveBaseAdapter;
import com.avaloq.framework.util.CurrencyUtil;

public abstract class OrdersAdapter extends ProgressiveBaseAdapter{

	public class OrderIdValuePair{
		String id;
		String value;
	}
	
	private Fragment mFragment;
	
	private static String filterString = "";
	private static StexOrderType filterOrderType = null;
	private static StexOrderStateType filterOrderStatus = null;
	
	/*
	 * We will have two list holders for the results. One will hold all the
	 * results, and the other one (used to display the entries) will hold
	 * the results after the filter is applied
	 */
	private List<StexOrderDetailTO> mResultsAll = new ArrayList<StexOrderDetailTO>();
	
	static class ViewHolder {
		public LinearLayout status;
		public TextView instrumentName;
		public TextView isin;
		public TextView orderType;
		public TextView orderQuantity;
		public TextView tradeQuantity;
		public TextView expirationDate;
		public TextView executionDate;		
	}

	/**
	 * Constructor
	 */
	public OrdersAdapter(Fragment activity) {
		super(activity.getActivity());
		this.mFragment = activity;
	}
	
	@Override
	public int getCount() {
		return mResultsAll.size();
	}

	public int getCountAll() {
		return mResultsAll.size();
	}
	
	@Override
	public Object getItem(int position) {
		return mResultsAll.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		StexOrderDetailTO item = (StexOrderDetailTO) getItem(position);

		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		rowView = inflater.inflate(R.layout.trd_orders_list_item, null);

		ViewHolder viewHolder = new ViewHolder();
		
		viewHolder.status = (LinearLayout) rowView.findViewById(R.id.trading_orders_tvStatus);
		viewHolder.instrumentName = (TextView) rowView.findViewById(R.id.trading_orders_tvInstrument);
		viewHolder.isin = (TextView) rowView.findViewById(R.id.trading_orders_tvIsin);
		viewHolder.orderType = (TextView) rowView.findViewById(R.id.trading_orders_tvType);
		viewHolder.tradeQuantity = (TextView) rowView.findViewById(R.id.trading_orders_tvTradingQuantity);
		viewHolder.expirationDate = (TextView) rowView.findViewById(R.id.trading_orders_tvExpiryDate);			
		viewHolder.executionDate = (TextView) rowView.findViewById(R.id.trading_orders_tvExecutionDate);
		viewHolder.orderQuantity = (TextView) rowView.findViewById(R.id.trading_orders_tvOrderQuantity);
		
		rowView.setTag(viewHolder);

		ViewHolder holder = (ViewHolder) rowView.getTag();

		
		// Assign the correct values to the views of this row
		String layoutName = "trd_order_status_"+item.getStexOrderStateType().toString().toLowerCase(Locale.ENGLISH);
		int resId = mFragment.getResources().getIdentifier(layoutName, "layout", mFragment.getActivity().getBaseContext().getPackageName());
		try {
			View stub = mFragment.getActivity().getLayoutInflater().inflate(resId, parent, false);
			holder.status.addView(stub);
		} catch(NotFoundException e) {
			throw new RuntimeException("Failed to inflate the layout '"+layoutName+"'", e);
		}
		
		// get the string for order type
		resId = mFragment.getResources().getIdentifier("trading_order_type_"+item.getStexOrderType(), "string", mFragment.getActivity().getBaseContext().getPackageName());
	    String type = resId == 0 ? "" : (String) mFragment.getResources().getText(resId);
	    holder.orderType.setText(type);
		
		holder.isin.setText(item.getInstrument().getIsin());
		holder.instrumentName.setText(item.getInstrument().getTitle());
		
		
		if (item.getTradedQuantity() != null && holder.tradeQuantity != null){
			holder.tradeQuantity.setText(CurrencyUtil.formatMoney(item.getTradedQuantity()));
		}
		
		if (item.getOrderQuantity() != null && holder.orderQuantity != null){
			holder.orderQuantity.setText(CurrencyUtil.formatMoney(item.getOrderQuantity()));
		}
		
		if (item.getExpirationDate() != null && holder.expirationDate != null){
			holder.expirationDate.setText(DateFormat.getDateFormat(mFragment.getActivity().getBaseContext()).format(item.getExpirationDate()));
		}
		
		if (item.getExecutionDate() != null && holder.executionDate != null){
			holder.executionDate.setText(DateFormat.getDateFormat(mFragment.getActivity().getBaseContext()).format(item.getExecutionDate()));
		}		
		
		checkIfMustLoadNextData(position);
		
		return rowView;

	}
	
	/**
	 * Returns the header view for the orders list. Contains the
	 * header with all the strings translated and formatted to match the list
	 * 
	 * @return View - The header view that can be directly added to the list.
	 */
	public View getHeaderView() {
		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		View header = inflater.inflate(R.layout.trd_orders_header, null);
		return header;
	}

	
	/**
	 * Requests data from the server and calls @see receivedPositionResults() when the data finished loading
	 */
	public void getDataFromServer(StexOrderQueryTO query, boolean refreshList) {		
		
		query.setMaxResultSize(refreshList ? mResultPerPage : (mResultPerPage+mResultsAll.size()));

		// Define the callback
		RequestStateEvent<StexOrdersRequest> rse = new RequestStateEvent<StexOrdersRequest>() {
			@Override
			public void onRequestCompleted(StexOrdersRequest stexOrdersRequest) {								
				receivedStexOrderResults(stexOrdersRequest);
				loadingStateChanged(false);
				showListLoading(false);
			}			
		};

		// Initiate the request 
		StexOrdersRequest request = TradingService.getOrderList(					
				query, 
				0L, 
				refreshList ? Long.valueOf(mResultPerPage) : Long.valueOf((mResultPerPage+mResultsAll.size())), 
				rse);		
		if (refreshList)reset();
		refreshList = false;
		
		request.setCachePolicy(CachePolicy.REFRESH_CACHE);
		
		loadingStateChanged(true);
		// Add the request to the request queue
		request.initiateServerRequest();
	}

	/**
	 * Update the dataset and notify the listeners on the UI thread
	 * @param aRequest
	 */
	protected void receivedStexOrderResults(final AbstractServerRequest<StexOrdersResult> aRequest) {
		
		StexOrdersResult stexOrdersResult = aRequest.getResponse().getData();
		
		if (stexOrdersResult != null) {

			List<StexOrderDetailTO> positions = stexOrdersResult.getStexOrders();			
			
			mResultsAll.clear();
			mResultsAll.addAll(positions);
			notifyDataSetChanged();
		}
	}
}
