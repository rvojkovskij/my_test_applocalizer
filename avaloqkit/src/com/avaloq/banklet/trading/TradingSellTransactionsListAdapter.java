/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */
package com.avaloq.banklet.trading;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.wealth.TransactionsResult;
import com.avaloq.afs.server.bsp.client.ws.TransactionDirection;
import com.avaloq.afs.server.bsp.client.ws.TransactionListQueryTO;
import com.avaloq.afs.server.bsp.client.ws.TransactionTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.comms.webservice.trading.TransactionsRequest;
import com.avaloq.framework.util.CurrencyUtil;

public class TradingSellTransactionsListAdapter extends BaseAdapter{
protected static final String TAG = TradingSellTransactionsListAdapter.class.getSimpleName();
	
	private List<TransactionListEntry> mResultsAll = new ArrayList<TransactionListEntry>();
	
	private Fragment mFragment;
	
	static class ViewHolder {
		public TextView date;
		public TextView month;
		public TextView bookingText;
		public TextView bookingAmount;
		public TextView quantity;
		public TextView balance;
	}
		
	/**
	 * Constructor
	 */
	public TradingSellTransactionsListAdapter(Fragment activity) {
		this.mFragment = activity;
	}
	
	public void getDataFromServer(Long bankingPositionId) {				
		// get the market data
		requestDataFromServer(bankingPositionId);
	}
	
	public void requestDataFromServer(Long bankingPositionId) {		
		
		TransactionListQueryTO query = new TransactionListQueryTO();
		query.setFromDate(null);
		query.setToDate(null);
		query.setBankingPositionId(bankingPositionId);
		query.setTransactionDirection(TransactionDirection.ALL);
		query.setMaxResultSize(500);
		
		
		// Define the callback
		RequestStateEvent<TransactionsRequest> rse = new RequestStateEvent<TransactionsRequest>() {
			@Override
			public void onRequestCompleted(TransactionsRequest positionsRequest) {								
				receivedTransactionResults(positionsRequest);			
			}			
		};
				
		// Initiate the request 
		TransactionsRequest request = TradingService.findTransactionList(query, rse);					

		// Add the request to the request queue
		request.initiateServerRequest();
	}

	
	/**
	 * Update the dataset and notify the listeners on the ui thread
	 * @param aRequest
	 */
	protected void receivedTransactionResults(final AbstractServerRequest<TransactionsResult> aRequest) {

		TransactionsResult transactionResult = aRequest.getResponse().getData();
		if (transactionResult != null) {

			// List<TransactionTO> positions =
			// transactionResult.getTransactions();
			mResultsAll.clear();

			/*
			 * Loop through the results and split to buckets for distinct
			 * YearMonth combinations
			 */
			SparseArray<List<TransactionTO>> hashMap = new SparseArray<List<TransactionTO>>();
			// HashMap<Integer, List<TransactionTO>> hashMap = new
			// HashMap<Integer, List<TransactionTO>>();

			// save the keys to a list. We will sort this list later
			List<Integer> keyList = new ArrayList<Integer>();

			for (TransactionTO transaction : transactionResult.getTransactions()) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(transaction.getBookingDate());
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH);

				// calculate the key for the bucket
				Integer key = year * 100 + month; // just get a number like
													// 2012*100+11=201211, which
													// we will use for sorting

				// store the key
				if (!keyList.contains(key)) {
					keyList.add(key);
				}

				List<TransactionTO> bucket = new ArrayList<TransactionTO>();

				// request the bucket contents using the key
				bucket = hashMap.get(key);

				if (bucket == null) {
					// we had no records at this key until now, then we add one
					bucket = new ArrayList<TransactionTO>();

				}
				bucket.add(transaction);

				hashMap.put(key, bucket);
			}

			/*
			 * Sort the key list in descending order
			 */
			Collections.reverse(keyList);

			/*
			 * Add all the records to the main list, including the separators
			 */

			for (Integer key : keyList) {

				// get the entries from the Hashmap
				List<TransactionTO> bucket = hashMap.get(key);

				// add a record for the separator
				TransactionListEntry tle = new TransactionListEntry();
				tle.date = bucket.get(0).getBookingDate();
				tle.transaction = null;
				mResultsAll.add(tle);

				// add all the other records
				for (TransactionTO transaction : bucket) {

					tle = new TransactionListEntry();
					tle.date = null;
					tle.transaction = transaction;
					mResultsAll.add(tle);
				}
			}

			notifyDataSetChanged();
		}
	}

	@Override
	public int getCount() {
		return mResultsAll.size();
	}

	@Override
	public Object getItem(int position) {
		return mResultsAll.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		TransactionListEntry listEntry = (TransactionListEntry) getItem(position);
		TransactionTO item = listEntry.transaction;
	    
		//if (rowView == null) {
	      LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
	      
	      ViewHolder viewHolder = new ViewHolder();
	      
	      // not a grouping entry
	      if (item != null){
	    	  rowView = inflater.inflate(R.layout.trd_sell_detail_transactions_item, null);
	    	  viewHolder.date = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvDate);
		      viewHolder.month = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvMonth);
		      viewHolder.bookingText = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvBookingText);
		      viewHolder.bookingAmount = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvAmount);
		      viewHolder.quantity = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvNumber);
		      viewHolder.balance = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvBalance);
	      }else{
	    	  // grouping entry
	    	  rowView = inflater.inflate(R.layout.trd_sell_detail_transactions_grouping_row, null);
	    	  viewHolder.date = (TextView) rowView.findViewById(R.id.trading_sell_transactions_tvDate);
	      }
	      
	      rowView.setTag(viewHolder);
	    //}

		// this is not a grouping entry
		if (item != null){
			
			
			// display the day and the month in the correct format
			Calendar calendar = Calendar.getInstance();		
			calendar.setTime(item.getBookingDate());
			int date = calendar.get(Calendar.DATE);
			
			SimpleDateFormat formatter = new SimpleDateFormat("MMM", Locale.getDefault());
	
		    ViewHolder holder = (ViewHolder) rowView.getTag();
		    holder.date.setText(Integer.toString(date));
		    holder.month.setText(formatter.format(item.getBookingDate()));
		    holder.bookingText.setText(item.getBookingText());
		    holder.bookingAmount.setText(CurrencyUtil.formatMoney(item.getBookingAmount(), item.getBookingAmountCurrencyId()));
			holder.quantity.setText(CurrencyUtil.formatMoney(item.getQuantity()));
			holder.balance.setText(CurrencyUtil.formatMoney(item.getBalance()));
		}else{
			// this is a grouping row, just display the date
			ViewHolder holder = (ViewHolder) rowView.getTag();
			SimpleDateFormat formatter = new SimpleDateFormat("MMMMM, yyyy", Locale.getDefault());
			holder.date.setText(formatter.format(listEntry.date));
		}
	    return rowView;
	}

	public View getHeaderView() {
		LayoutInflater inflater = mFragment.getActivity().getLayoutInflater();
		View header = inflater.inflate(R.layout.trd_sell_detail_transactions_header, null);
		return header;
	}
	
	class TransactionListEntry{
		TransactionTO transaction;
		Date date;
	}
}
