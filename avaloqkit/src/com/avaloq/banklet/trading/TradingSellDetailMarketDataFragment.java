/**
 * @author      Victor Budilivschi <v.budilivschi@insign.ch>
 * @version     1.0
 * @since       2013-03-31
 */
package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.avaloq.afs.aggregation.to.marketdata.MarketDataChartResult;
import com.avaloq.afs.aggregation.to.marketdata.MarketDataResult;
import com.avaloq.afs.aggregation.to.wealth.ListingListResult;
import com.avaloq.afs.server.bsp.client.ws.ChartPeriod;
import com.avaloq.afs.server.bsp.client.ws.ListingTO;
import com.avaloq.afs.server.bsp.client.ws.MarketDataChartQueryTO;
import com.avaloq.afs.server.bsp.client.ws.MarketDataQueryTO;
import com.avaloq.afs.server.bsp.client.ws.MarketDataTO;
import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataChartRequest;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataRequest;
import com.avaloq.framework.comms.webservice.marketdata.MarketDataService;
import com.avaloq.framework.comms.webservice.trading.ListingListRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.ApiSpecificCalls;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.Tools;


public class TradingSellDetailMarketDataFragment extends BankletFragment implements OnClickListener{

	public interface DetailMarketDataFragmentInterface{
		public void onClick_ButtonSell();
		public void onClick_ButtonBuy(int selectedListingIndex);
		public Long getInstrumentId();
		public String getInstrumentName();
		public String getInstrumentType();
		public String getInstrumentSubType();
		public Long getCurrencyId();
		public String getMarketName();
		public Long getMarketId();
		public ButtonSellBuyStatus getSellBuyButtonStatus();        
	}
	
	private static String instrumentName;
	private static String instrumentType;
	private Long instrumentId;
	private static String instrumentSubType;
	private static String marketName;
	private static Long listingCurrency;
	private static Long marketId;

    private Long listingCurrencyId;
	
	private static ChartPeriod chartPeriod = ChartPeriod.ONE_YEAR;

	// holders for the charts
	private static Bitmap chartPortrait = null;	
	private static Bitmap chartLandscape = null;
	
    private int selectedListingIndex = 0;
    private static List<ListingTO> listings = new ArrayList<ListingTO>();
	
	private final int DECIMALS_PERCENT = 5;
	private final int DECIMALS_NUMBER = 3;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View fragmentView = inflater.inflate(R.layout.trd_sell_detail_market_data_fragment, container, false);

		((Button) fragmentView.findViewById(R.id.trading_detail_btnSell)).setVisibility(View.INVISIBLE);
		
		return fragmentView;
	}
	
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		if (savedInstanceState != null){
			selectedListingIndex = savedInstanceState.getInt("selectedListingIndex");
		}
		
		// Some of the data needed to be displayed is received through extra arguments
		// from the calling activity. It is accessed here through the members of the
		// activity that created this fragment
		try{			
			instrumentId =  ((DetailMarketDataFragmentInterface) getActivity()).getInstrumentId();
			instrumentName =  ((DetailMarketDataFragmentInterface) getActivity()).getInstrumentName();
			instrumentType = ((DetailMarketDataFragmentInterface) getActivity()).getInstrumentType();
			instrumentSubType = ((DetailMarketDataFragmentInterface) getActivity()).getInstrumentSubType();
			
			marketName = ((DetailMarketDataFragmentInterface) getActivity()).getMarketName();
			listingCurrency = ((DetailMarketDataFragmentInterface) getActivity()).getCurrencyId();
			//marketId = ((DetailMarketDataFragmentInterface) getActivity()).getMarketId();
			listingCurrencyId = ((DetailMarketDataFragmentInterface) getActivity()).getCurrencyId();
			marketId = ((DetailMarketDataFragmentInterface) getActivity()).getMarketId();
			
			
			((TextView) getActivity().findViewById(R.id.tvTitle)).setText(instrumentName);
			((TextView) getActivity().findViewById(R.id.tvInstrumentType)).setText(instrumentType);			
			
			if (((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){		
				((TextView) getActivity().findViewById(R.id.tvInstrumentSubType)).setText(getString(R.string.trading_header_isin)+ " "+instrumentSubType);
			}else{
				setBuyOrSellButton();
			}	
		}catch(Exception e){
			e.printStackTrace();
		}
		
		// request the data from the server
		requestData();
		
		attachClickListenersToChartButtons();
	
		setBuyOrSellButton();
		
		setCorrectIcon();
		
		attachClickListenerToChart();
		
		setActiveChartButton();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	  super.onSaveInstanceState(savedInstanceState);
	  savedInstanceState.putInt("selectedListingIndex", selectedListingIndex);	 
	}
	
	/**
	 * Adds the listing information and currency to the header
	 */
	private void addListingToHeader(){

		// add values to spinner
		Spinner spinnerListing = (Spinner) getView().findViewById(R.id.trading_detail_spinnerListing);		
		
		
		List<String> list = new ArrayList<String>();
		for (ListingTO listing: listings){
			list.add(listing.getMarketName()+"/"+getCurrencyTextByCurrencyId(listing.getCurrencyId()));
		}
		final HighlightedSpinnerAdapter<String> dataAdapter = new HighlightedSpinnerAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerListing.setAdapter(dataAdapter);
		spinnerListing.setVisibility(View.VISIBLE);
		spinnerListing.setSelection(selectedListingIndex);
		spinnerListing.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				selectedListingIndex = pos;
				dataAdapter.setHighlightedPosition(pos);
				// get the market data
				requestMarketData();
				requestAllCharts();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// do nothing
			}
		});		
		
		if (listings.size() == 1){
			TextView tvISIN = (TextView) getActivity().findViewById(R.id.tvInstrumentSubType);
			tvISIN.setText(tvISIN.getText()+"\n"+listings.get(0).getMarketName()+"/"+getCurrencyTextByCurrencyId(listings.get(0).getCurrencyId()));
			spinnerListing.setVisibility(View.GONE);			
		}
		
	}
	
	/**
	 * Sets as active the correct chart button
	 */
	private void setActiveChartButton(){
		
		voidSetInactiveAllChartButtons();
		
		if (chartPeriod == ChartPeriod.INTRA_DAY){
			((ToggleButton) getView().findViewById(R.id.btnIntra)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.ONE_WEEK){
			((ToggleButton) getView().findViewById(R.id.btn1W)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.ONE_MONTH){
			((ToggleButton) getView().findViewById(R.id.btn1M)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.YEAR_TO_DATE){
			((ToggleButton) getView().findViewById(R.id.btnYTD)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.SIX_MONTHS){
			((ToggleButton) getView().findViewById(R.id.btn6M)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.ONE_YEAR){
			((ToggleButton) getView().findViewById(R.id.btn1J)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.THREE_YEARS){
			((ToggleButton) getView().findViewById(R.id.btn3J)).setChecked(true);
		}else if (chartPeriod == ChartPeriod.ALL){
			((ToggleButton) getView().findViewById(R.id.btnMax)).setChecked(true);
		}

	}
	
	/**
	 * Sets the correct text for the button in the header (Buy/Sell) and attaches the on click event.
	 * This fragment is used on the activity that gets called either from the "Buy" list or from
	 * the "Sell" list. While all the other content is the same, this button must be different in both places
	 */
	private void setBuyOrSellButton(){
		
		if (((DetailMarketDataFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
			// must set the button to Buy
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setText(R.string.trading_buy);
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setVisibility(View.VISIBLE);
		}else if (((DetailMarketDataFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.SELL){
			// must set the button as Sell
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setText(R.string.trading_sell);
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setVisibility(View.VISIBLE);
		}else{
			// hide the button
			((Button) getView().findViewById(R.id.trading_detail_btnSell)).setVisibility(View.INVISIBLE);
		}
		
		// attach click listener
		((Button) getView().findViewById(R.id.trading_detail_btnSell)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (((DetailMarketDataFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
					((DetailMarketDataFragmentInterface) getActivity()).onClick_ButtonBuy(selectedListingIndex);
				}else if (((DetailMarketDataFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.SELL){
					((DetailMarketDataFragmentInterface) getActivity()).onClick_ButtonSell();
				}else{
					// do nothing, the button must not be visible in this case
				}
			}
		});		
	}
	
	/**
	 * 
	 */
	private void attachClickListenerToChart(){
		/*
		 * When taping the chart, it must open in full screen.
		 */
		((ImageView) getView().findViewById(R.id.trading_ivChart)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				intent.setClass(getActivity(), TradingSellDetailChartFullscreen.class);
				
				intent.putExtra(TradingSellDetailChartFullscreen.EXTRA_CHART_PERIOD, chartPeriod);
				intent.putExtra(TradingSellDetailChartFullscreen.EXTRA_CHART_INSTRUMENT_ID, instrumentId);
				if (((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
					intent.putExtra(TradingSellDetailChartFullscreen.EXTRA_CHART_MARKET_ID, listings.get(selectedListingIndex).getMarketId());
				}
				
				startActivity(intent);
			}
		});		
	}
	
	/**
	 * Sets the correct value for Bonds, Equities or funds
	 */
	private void setCorrectIcon(){		
		if (instrumentType.toUpperCase().compareTo("BONDS") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_bonds));			
		}else if (instrumentType.toUpperCase().compareTo("EQUITIES") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_equities));
		}else if (instrumentType.toUpperCase().compareTo("FUNDS") == 0){			
			((ImageView) getView().findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_funds));
		}
	}
	
	/**
	 * Attaches the click listeners to the buttons that display the charts for different periods
	 */
	private void attachClickListenersToChartButtons(){
		Button b = null;
		
		b = (Button) getActivity().findViewById(R.id.btnIntra);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btn1W);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btn1M);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btn6M);
        b.setOnClickListener(this);
                
        b = (Button) getActivity().findViewById(R.id.btnYTD);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btn1J);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btn3J);
        b.setOnClickListener(this);
        
        b = (Button) getActivity().findViewById(R.id.btnMax);
        b.setOnClickListener(this);
	} 
	
	
	/**
	 * Requests the data from the server
	 */
	private void requestData(){

		if (!(((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY)){	
			((TextView) getActivity().findViewById(R.id.tvInstrumentSubType)).setText(getString(R.string.trading_header_isin)+ " "+instrumentSubType+"\n"+marketName+" / "+getCurrencyTextByCurrencyId(listingCurrency));
		}
		
		if (((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
			// get the market data
			requestInstrumentListing();			
		}else{
			requestMarketData();
			requestAllCharts();
		}
	}

	/**
	 * Requests listing info
	 */
	private void requestInstrumentListing(){

		RequestStateEvent<ListingListRequest> rse = new RequestStateEvent<ListingListRequest>() {
			@Override
			public void onRequestCompleted(ListingListRequest stexDefaultsRequest) {								
				receiveInstrumentListing(stexDefaultsRequest);			
			}			
		};
		
		// Initiate the request 
		ListingListRequest request = TradingService.getInstrumentListingList(instrumentId, rse);		
		
		// Add the request to the request queue
		request.initiateServerRequest();		
	}
	
	/**
	 * Callback when receiving the stex defaults
	 */
	private void receiveInstrumentListing(ListingListRequest listingListRequest){
		
		ListingListResult listingListResult = listingListRequest.getResponse().getData();
		if (listingListResult != null) {

			listings.clear();
			listings.addAll(listingListResult.getListingList());
						
			// get the market data
			setBuyOrSellButton();
			requestMarketData();
			requestAllCharts();
			
			// add the listing information
			if (((DetailMarketDataFragmentInterface) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){
				addListingToHeader();
			}
		}	
	}
	
	/**
	 * Returns the currency ISO code given the currency ID
	 * 
	 * @param currencyId The Id of the currency 
	 * @return the currency ISO code as string, ex: EUR, CHF
	 */
	private String getCurrencyTextByCurrencyId(Long currencyId){
		return AvaloqApplication.getInstance().findCurrencyById(currencyId).getIsoCode();
	}
	
	/**
	 * Sets all the chart period buttons as inactive
	 */
	private void voidSetInactiveAllChartButtons(){
	
		// set all radiobuttons as inactive
        ((ToggleButton) getView().findViewById(R.id.btnIntra)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btn1W)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btn1M)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btn6M)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btnYTD)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btn1J)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btn3J)).setChecked(false);
        ((ToggleButton) getView().findViewById(R.id.btnMax)).setChecked(false);		
	}
	
	/**
	 * Request both charts (for landscape and portrait) from the webservice
	 */
	private void requestAllCharts(){
		// get the market data charts		
		requestMarketDataChart(getResources().getConfiguration().orientation);
		
//		// additionaly request the portrait/landscape image
//		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
//			requestMarketDataChart(Configuration.ORIENTATION_LANDSCAPE);
//		}else{
//			requestMarketDataChart(Configuration.ORIENTATION_PORTRAIT);
//		}
	}
	
	
	/**
	 * Requests the market data
	 */
	private void requestMarketData(){
		
		MarketDataQueryTO query = new MarketDataQueryTO();
		query.setCurrencyId(null);
		query.setInstrumentId(instrumentId);
		query.setMarketId(null);
		
		if (selectedListingIndex > listings.size()-1){
			selectedListingIndex = 0;
		}
		
		if (((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){			
			query.setMarketId(listings.get(selectedListingIndex).getMarketId());
		}else{			
			query.setMarketId(null);
		}
		RequestStateEvent<MarketDataRequest> rse = new RequestStateEvent<MarketDataRequest>() {
			@Override
			public void onRequestCompleted(MarketDataRequest positionsRequest) {	
				try{
					showMarketData(positionsRequest);
				}catch(Exception e){
					
				}
			}			
		};
		
		// Initiate the request 
		MarketDataRequest request = MarketDataService.getMarketData(query, rse);					
	
		// Add some debug info
		request.setRequestIdentifier(request.getRequestIdentifier() );
	
		// Add the request to the request queue
		request.initiateServerRequest();	
	}
	
	
	
	

	
	/**
	 * Requests the data chart
	 */
	private void requestMarketDataChart(final int orientation){
					
		final MarketDataChartQueryTO query = new MarketDataChartQueryTO();
		query.setChartPeriod(chartPeriod);
		// query.setChartPeriod("OneYear");
		query.setCurrencyId(null);
		query.setInstrumentId(instrumentId);
		
		if (selectedListingIndex > listings.size()-1){
			selectedListingIndex = 0;
		}
		
		if (((TradingSellDetailActivity) getActivity()).getSellBuyButtonStatus() == ButtonSellBuyStatus.BUY){			
			query.setMarketId(listings.get(selectedListingIndex).getMarketId());
		}else{
			query.setMarketId(null);
		}
		
				 		
//		int chartImageHeight = 0;
//		int chartImageWidth = 0;
		
		// get the image tag to see if the layout is xlarge-land
		String tag = (String)getView().findViewById(R.id.trading_ivChart).getTag();
		if (tag == null) tag = "";
		
		// get the image container size
		final View container;
		if (orientation == Configuration.ORIENTATION_PORTRAIT || (orientation == Configuration.ORIENTATION_LANDSCAPE && tag.compareTo("xlarge-land") == 0)){
			container = getView().findViewById(R.id.trading_ivChart);
		}else{
			container = getView().findViewById(R.id.trading_trChart);
		}
		
		
		if (container != null){
			if (container.getWidth() > 0){
				requestChartImage(query, container, orientation);
			}else{

				ViewTreeObserver vto = container.getViewTreeObserver();
				vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
					@Override
					public void onGlobalLayout() {
						// make sure that the container is actually ready
						if (container.getWidth() > 0){
							ApiSpecificCalls.removeOnGlobalLayoutListener(container.getViewTreeObserver(), this);
						}else{
							return;
						}

						requestChartImage(query, container, orientation);
					}
				});
			}
		}
	}
	
	private void requestChartImage(MarketDataChartQueryTO query, View container, final int orientation){
		
		query.setHeight(container.getHeight());
		query.setWidth(container.getWidth());
		
		if (orientation == Configuration.ORIENTATION_PORTRAIT){						
			query.setHeight((int) (0.25 * Tools.getDisplaySize(getActivity().getBaseContext()).y));
			query.setWidth(container.getWidth());
		}
		
		RequestStateEvent<MarketDataChartRequest> rse = new RequestStateEvent<MarketDataChartRequest>() {
			@Override
			public void onRequestCompleted(MarketDataChartRequest marketDataChartRequest) {		
				try{
					showMarketDataChart(marketDataChartRequest, orientation);
				}catch(Exception e){
					
				}
			}			
		};
		
		// Initiate the request 
		MarketDataChartRequest request = MarketDataService.getMarketDataChart(query, rse);					
	
		// Add some debug info
		request.setRequestIdentifier(request.getRequestIdentifier() );
	
		// Add the request to the request queue
		request.initiateServerRequest();
	}
	
	/**
	 * Callback function. Called when the market data is received from the server. Used
	 * to display the data.
	 */
	private void showMarketData(final AbstractServerRequest<MarketDataResult> aRequest){
		MarketDataResult marketDataResult = aRequest.getResponse().getData();
		if (marketDataResult != null) {
			
			MarketDataTO marketData = marketDataResult.getMarketData();

			String currencyCode = "";//(marketData.getCurrencyId() != null) ? getCurrencyTextByCurrencyId(marketData.getCurrencyId()) : "";
			//************************************************************************************
			
			// Set opening value
			if (marketData.getOpenPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvOpenPrice)).setText(CurrencyUtil.formatNumber(marketData.getOpenPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvOpenPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvOpenPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvOpenPriceCurrency)).setText("");
			}
			
			// Set opening date
			if (marketData.getOpenDate() != null){
				( (TextView) getActivity().findViewById(R.id.tvOpenPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getOpenDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getOpenDate()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvOpenPriceDate)).setText("");
			}
			
			//************************************************************************************
			
			// Set closing value
			if (marketData.getClosePrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvClosePrice)).setText(CurrencyUtil.formatNumber(marketData.getClosePrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvClosePriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvClosePrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvClosePriceCurrency)).setText("");
			}
			
			// Set closing date
			if (marketData.getCloseDate() != null){
				( (TextView) getActivity().findViewById(R.id.tvClosePriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getCloseDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getCloseDate()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvClosePriceDate)).setText("");
			}
			
			//************************************************************************************
			
			// Set bid value					
			if (marketData.getBidPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvBidPrice)).setText(CurrencyUtil.formatNumber(marketData.getBidPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvBidPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvBidPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvBidPriceCurrency)).setText("");
			}
			
			// Set bid date
			if (marketData.getBidDate() != null){
				( (TextView) getActivity().findViewById(R.id.tvBidPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getBidDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getBidDate()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvBidPriceDate)).setText("");
			}
			
			// Set bid volume
			if (marketData.getBidVolume() != null){
				( (TextView) getActivity().findViewById(R.id.tvBidPriceVolume)).setText("x"+CurrencyUtil.formatNumber(marketData.getBidVolume()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvBidPriceVolume)).setText("");
			}
			
			//************************************************************************************
			
			// Set ask value
			if (marketData.getAskPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvAskPrice)).setText(CurrencyUtil.formatNumber(marketData.getAskPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvAskPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvAskPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvAskPriceCurrency)).setText("");
			}
			
			// Set ask date
			if (marketData.getAskDate() != null){
				( (TextView) getActivity().findViewById(R.id.tvAskPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getAskDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getAskDate()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvAskPriceDate)).setText("");
			}
			
			// Set ask volume
			if (marketData.getAskVolume() != null){
				( (TextView) getActivity().findViewById(R.id.tvAskPriceVolume)).setText("x"+CurrencyUtil.formatNumber(marketData.getAskVolume()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvAskPriceVolume)).setText("");
			}
			//************************************************************************************
			
			// Set Intra High value
			if (marketData.getIntradayHighPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvIntraHighPrice)).setText(CurrencyUtil.formatNumber(marketData.getIntradayHighPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvIntraHighPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvIntraHighPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvIntraHighPriceCurrency)).setText("");
			}
			
			//************************************************************************************
			
			// Set Intra Low value
			if (marketData.getIntradayLowPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvIntraLowPrice)).setText(CurrencyUtil.formatNumber(marketData.getIntradayLowPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvIntraLowPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvIntraLowPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvIntraLowPriceCurrency)).setText("");
			}
			
			//************************************************************************************
			
			// Set Last value
			if (marketData.getLastPrice() != null){
				( (TextView) getActivity().findViewById(R.id.tvLastPrice)).setText(CurrencyUtil.formatNumber(marketData.getLastPrice(), DECIMALS_NUMBER));
				( (TextView) getActivity().findViewById(R.id.tvLastPriceCurrency)).setText(currencyCode);
			}else{
				( (TextView) getActivity().findViewById(R.id.tvLastPrice)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvLastPriceCurrency)).setText("");
			}
			
			// Set Last date
			if (marketData.getLastDate() != null){
				( (TextView) getActivity().findViewById(R.id.tvLastPriceDate)).setText(DateFormat.getDateFormat(getActivity().getApplicationContext()).format(marketData.getLastDate())+" "+DateFormat.getTimeFormat(getActivity().getApplicationContext()).format(marketData.getLastDate()));
			}else{
				( (TextView) getActivity().findViewById(R.id.tvLastPriceDate)).setText("");
			}
			
			// Set last volume
			if (marketData.getLastVolume() != null){
				( (TextView) getView().findViewById(R.id.tvLastPriceVolume)).setText("x"+CurrencyUtil.formatNumber(marketData.getLastVolume()));
			}else{
				( (TextView) getView().findViewById(R.id.tvLastPriceVolume)).setText("");
			}
			
			//************************************************************************************ 
			
			// Set Percent value
			if (marketData.getChangeLastClosedPercent() != null){
				( (TextView) getActivity().findViewById(R.id.tvPercent)).setText(CurrencyUtil.formatNumber(marketData.getChangeLastClosedPercent(), DECIMALS_PERCENT));
				( (TextView) getActivity().findViewById(R.id.tvPercentChar)).setText("%");
			}else{
				( (TextView) getActivity().findViewById(R.id.tvPercent)).setText(getActivity().getText(R.string.trd_unavailable));
				( (TextView) getActivity().findViewById(R.id.tvPercentChar)).setText(""); 
			}
								
		}
	}
	
	
	/**
	 * Callback function. Called when the market data chart is received from the server
	 */
	private void showMarketDataChart(final AbstractServerRequest<MarketDataChartResult> aRequest, final int orientation){		
		MarketDataChartResult marketDataChartResult = aRequest.getResponse().getData();
		if (marketDataChartResult != null) {
			
			// get the bytes out of the Base64 encoded string
			byte[] imageAsBytes = Base64.decode(marketDataChartResult.getImageDataBase64(), Base64.DEFAULT);
			
			if (orientation == Configuration.ORIENTATION_LANDSCAPE){						
				chartLandscape = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			}else{						
				chartPortrait = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			}
			 
			displayChart(orientation);
		}
	}
	
	
	/**
	 * Display the correct chart
	 * @param orientation
	 */
	private void displayChart(int orientation){
		
		ImageView image = (ImageView) getView().findViewById(R.id.trading_ivChart);
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
			if (chartLandscape != null){
				// display image in landscape
				image.setImageDrawable(null);
				image.setImageBitmap(chartLandscape);				
			}
		}else{
			if (chartPortrait != null){
				// display image in portrait
				image.setImageBitmap(null);				
				image.setImageBitmap(chartPortrait);
				
			}
		}
	}
	
	
	@Override
    public void onClick(View v) {
		int id = v.getId();
		if (id == R.id.btnIntra) {
			voidSetInactiveAllChartButtons();
			chartPeriod =  ChartPeriod.INTRA_DAY;
			setActiveChartButton();
		} else if (id == R.id.btn1W) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.ONE_WEEK;
			setActiveChartButton();
		} else if (id == R.id.btn1M) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.ONE_MONTH;
			setActiveChartButton();
		} else if (id == R.id.btn6M) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.SIX_MONTHS;
			setActiveChartButton();
		} else if (id == R.id.btnYTD) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.YEAR_TO_DATE;
			setActiveChartButton();
		} else if (id == R.id.btn1J) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.ONE_YEAR;
			setActiveChartButton();
		} else if (id == R.id.btn3J) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.THREE_YEARS;
			setActiveChartButton();
		} else if (id == R.id.btnMax) {
			voidSetInactiveAllChartButtons();
			chartPeriod = ChartPeriod.ALL;
			setActiveChartButton();
		}
        requestAllCharts();
    }

	public static void resetChartPeriod(){
		chartPeriod = ChartPeriod.ONE_YEAR;
	}

}


