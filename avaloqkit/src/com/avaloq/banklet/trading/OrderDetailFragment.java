package com.avaloq.banklet.trading;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

/**
 * Special Fragment for tablets: displays 2 fragments inside itself: {@link OrderDetailDataFragment} and {@link OrderDetailTransactionsFragment}.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class OrderDetailFragment extends BankletFragment {

	/**
	 * Takes the order id as an argument
	 */
	public static final String EXTRA_ORDER_DETAIL_ID = "extra_order_detail_id";
	public static final String EXTRA_ORDER_DETAIL = "extra_order_detail";
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.trd_fragment_order_detail, container, false);
	}
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		
		// find out which fragments we have to replace
		boolean replaceData = getFragmentManager().findFragmentById(R.id.trd_fragment_order_detail_data) == null;
		boolean replaceTransactions = getFragmentManager().findFragmentById(R.id.trd_fragment_order_detail_transactions) == null;
		// replace the fragments only if necessary
		if(replaceData || replaceTransactions) {
			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			if(replaceData) {
				// replace the OrderDetailDataFragment if necessary
				OrderDetailDataFragment data = new OrderDetailDataFragment();
				Bundle detailDataArguments = new Bundle();
				detailDataArguments.putLong(OrderDetailDataFragment.EXTRA_ORDER_DETAIL_ID, getArguments().getLong(EXTRA_ORDER_DETAIL_ID));				
				detailDataArguments.putSerializable(OrderDetailDataFragment.EXTRA_ORDER_DETAIL, getArguments().getSerializable(EXTRA_ORDER_DETAIL));
				data.setArguments(detailDataArguments);
				tx.replace(R.id.trd_fragment_order_detail_data, data);
			}
			if(replaceTransactions) {
				// replace the OrderDetailTransactionsFragment if necessary
				OrderDetailTransactionsFragment detail = new OrderDetailTransactionsFragment();
				Bundle detailArguments = new Bundle();
				detailArguments.putLong(OrderDetailTransactionsFragment.EXTRA_ORDER_DETAIL_ID, getArguments().getLong(EXTRA_ORDER_DETAIL_ID));
				detailArguments.putSerializable(OrderDetailTransactionsFragment.EXTRA_ORDER_DETAIL, getArguments().getSerializable(EXTRA_ORDER_DETAIL));
				detail.setArguments(detailArguments);
				tx.replace(R.id.trd_fragment_order_detail_transactions, detail);
			}
			tx.commit();
		}
	}
	
}
