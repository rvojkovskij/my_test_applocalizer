package com.avaloq.banklet.trading;

import java.util.Locale;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.LocalizedErrorNotification;
import com.avaloq.afs.aggregation.to.LocalizedNotification;
import com.avaloq.afs.aggregation.to.LocalizedValidationResult;
import com.avaloq.afs.aggregation.to.trading.StexOrderResult;
import com.avaloq.afs.aggregation.to.wealth.TradingDefaultsResult;
import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.afs.server.bsp.client.ws.StexOrderDetailTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderStateType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.StexOrderRequest;
import com.avaloq.framework.comms.webservice.trading.StexOrdersRequest;
import com.avaloq.framework.comms.webservice.trading.TradingDefaultsRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.DateUtil;

public class OrderDetailDataFragment extends BankletFragment {
	
	public static final String EXTRA_ORDER_DETAIL_ID = "extra_order_detail_id";
	public static final String EXTRA_ORDER_DETAIL = "extra_order_detail";
	
	protected View detailData;
	
	private LayoutInflater mInflater;
	
	public interface OrderDetailDataFragmentInterface{
		public void orderCancelled();
	}
	
	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mInflater = inflater;
		final Long detailId = getArguments().getLong(EXTRA_ORDER_DETAIL_ID);
		detailData = mInflater.inflate(R.layout.trd_fragment_order_detail_data, container, false);
		
		final StexOrderDetailTO detail = (StexOrderDetailTO) getArguments().getSerializable(EXTRA_ORDER_DETAIL);
		if(detail != null) {
			showDetailData(detail);
			detailData.findViewById(R.id.loading_container).setVisibility(View.GONE);
		}
		
		return detailData;
	}
	
	/**
	 * 
	 * @param detail
	 */
	private void showDetailData(final StexOrderDetailTO detail){
		
		DateUtil dateUtil = new DateUtil(getActivity());
		setTextView(R.id.trading_detail_order_quantity, CurrencyUtil.formatNumber(detail.getOrderQuantity(), 6));
		
		int execResId = getActivity().getResources().getIdentifier("trading_buy_" +detail.getStexExecType().toString().toLowerCase(), "string", getActivity().getPackageName());
		String execType = execResId == 0 ? "" : (String) getResources().getText(execResId);
		
		setTextView(R.id.trading_detail_execution_type, execType);
		setTextView(R.id.trading_detail_limit, CurrencyUtil.formatMoney(detail.getLimitPrice(), detail.getCurrencyId(), 6));
		setTextView(R.id.trading_detail_trigger, CurrencyUtil.formatMoney(detail.getTriggerPrice(), detail.getCurrencyId(), 6));
		if (detail.getExpirationDate() != null)
			setTextView(R.id.trading_detail_expiration_date, dateUtil.format(detail.getExpirationDate()));
		if (detail.getExecutionDate() != null)
			setTextView(R.id.trading_detail_execution_date, dateUtil.format(detail.getExecutionDate()));
		if (detail.getCreationDate() != null)
			setTextView(R.id.trading_detail_creation_date, dateUtil.format(detail.getCreationDate()));
		if (detail.getAveragePrice() != null)
			setTextView(R.id.trading_detail_average_price, CurrencyUtil.formatMoney(detail.getAveragePrice()));
		setTextView(R.id.trading_detail_execution_quantity, detail.getTradedQuantity() == null ? 0 : detail.getTradedQuantity());
		setTextView(R.id.trading_detail_open_quantity, CurrencyUtil.formatNumber(detail.getOpenQuantity(), 6));		
		if (detail.getTotalCost() != null)
			setTextView(R.id.trading_detail_cost, CurrencyUtil.formatMoney(detail.getTotalCost()));		
		setTextView(R.id.trading_detail_external_reference, detail.getExternalReference());
		
		setTextView(R.id.tvTitle, detail.getInstrument().getTitle());
		setTextView(R.id.tvInstrumentType, detail.getInstrument().getStexGroupType().name() +"\n"+ getString(R.string.trading_header_isin)+" "+detail.getInstrument().getIsin());		
		setTextView(R.id.tvInstrumentSubType, detail.getInstrument().getDefaultListing().getMarketName()+" / "+AvaloqApplication.getInstance().findCurrencyById(detail.getInstrument().getDefaultListing().getCurrencyId()).getIsoCode());
		
		// set portfolio and account
		showPortfolioAndAccount(detail.getPortfolioId(), detail.getMoneyAccountId());
		
		
		
		//replace the buy/sell button with the status
		LinearLayout ll = new LinearLayout(getActivity());
		LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		ll.setLayoutParams(params);
		
		View cancelButton = detailData.findViewById(R.id.trading_detail_btnCancel);
		// show Cancel button
		if (detail.getStexOrderStateType() != StexOrderStateType.CANCELED &&
			detail.getStexOrderStateType() != StexOrderStateType.PENDING_CANCEL &&
			detail.getStexOrderStateType() != StexOrderStateType.DONE &&
			detail.getStexOrderStateType() != StexOrderStateType.REJECTED){
						
			cancelButton.setVisibility(View.VISIBLE);
			cancelButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					cancelOrder(detail.getId(), detail.getVersionId());
				}
			});
		}else{
			cancelButton.setVisibility(View.GONE);
		}
		
		String statusLabel = "statusLabel";
		
		View button = detailData.findViewById(R.id.trading_detail_btnSell);
				
		if (button == null){
			button = detailData.findViewWithTag(statusLabel);
		}
		
		if (button != null){
			ViewGroup parent = (ViewGroup) button.getParent();
			int index = parent.indexOfChild(button);
			parent.removeView(button);
			int resId = getResources().getIdentifier("trd_order_status_"+detail.getStexOrderStateType().toString().toLowerCase(Locale.ENGLISH), "layout", getActivity().getBaseContext().getPackageName());
			button = mInflater.inflate(resId, parent, false);
			button.setTag(statusLabel);
			ll.addView(button);
			parent.addView(ll, index);
		}
		
		setCorrectIcon(detail);
		
	}
	
	/**
	 * Sets the correct value for Bonds, Equities or funds
	 */
	private void setCorrectIcon(final StexOrderDetailTO detail){		
		if (detail.getInstrument().getStexGroupType() == StexAssetGroupType.BONDS){			
			((ImageView) detailData.findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_bonds));			
		}else if (detail.getInstrument().getStexGroupType() == StexAssetGroupType.EQUITIES){			
			((ImageView) detailData.findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_equities));
		}else if (detail.getInstrument().getStexGroupType() == StexAssetGroupType.FUNDS){			
			((ImageView) detailData.findViewById(R.id.trd_header_icon)).setImageDrawable(getResources().getDrawable(R.drawable.trd_icon_funds));
		}
	}
	
	/**
	 * Cancel currently displayed order
	 */
	private void cancelOrder(final Long orderId, final Long versionId){

		// inform activity that an order was canceled wo it can request full reload of the list
		((OrderDetailDataFragmentInterface) getActivity()).orderCancelled();
				
		AlertDialog confirmCancelDialogBox = new AlertDialog.Builder(getActivity())
		// set message, title, and icon
		.setTitle(R.string.trading_confirm_cancel_order)
		.setMessage(R.string.trading_confirm_cancel_order_text)
		.setPositiveButton(R.string.trading_confirm_cancel_order_ok, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int whichButton) {
				
				StexOrderRequest request = TradingService.requestOrderCancelation(orderId, versionId, new RequestStateEvent<StexOrderRequest>() {
					@Override
					public void onRequestCompleted(final StexOrderRequest aRequest) {
						StexOrderResult result = aRequest.getResponse().getData();
						if (result != null && getActivity() != null){
							StexOrderDetailTO orderDetail = result.getStexOrderDetail();
							
							String errorString = "";
							
							// if orderDetail is not null and there are no errors, update the view using this data
							if (orderDetail != null && (result.getErrorNotificationList() == null || result.getErrorNotificationList().size() == 0) ){
								showDetailData(orderDetail);
							}
							
							if (result.getErrorNotificationList() != null && result.getErrorNotificationList().size() > 0){
								for (LocalizedErrorNotification localizedNotification: result.getErrorNotificationList()){							
									errorString += localizedNotification.getLocalizedMessage()+"\n";
								}						
							}
							
							if (result.getNotificationList() != null && result.getNotificationList().size() > 0){
								for (LocalizedNotification localizedNotification: result.getNotificationList()){
									
									errorString += ((LocalizedValidationResult)localizedNotification.getValidationResult()).getLocalizedMessage() +"\n";
								}
								
								// close the detail view if needs a reload
								if (result.getNotificationList().get(0).getValidationResult().getFieldName().compareTo("fullReload") == 0){
									Intent returnIntent = new Intent();
									returnIntent.putExtra(OrdersFragment.EXTRA_FULL_RELOAD, true);
									returnIntent.putExtra(OrdersFragment.EXTRA_MESSAGE, errorString);
									getActivity().setResult(Activity.RESULT_OK, returnIntent);     							
									getActivity().finish();
									return;
								} 
							}
							
							// show the errors if there are any
							if (errorString.length() > 0){
								DialogFragment.createAlert(getString(R.string.trading_order_cancel_error), errorString).show();
							}
						}
					}
				});
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
				
			}

		})
		.setNegativeButton(R.string.trading_confirm_cancel_order_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).create();
		confirmCancelDialogBox.show();
	}	
	
	private void setTextView(int id, Object text){
		TextView tv = (TextView)detailData.findViewById(id);
		if (tv != null && text != null){
			if (text instanceof Double)
				tv.setText(CurrencyUtil.formatNumber((Double)text));
			else
				tv.setText(text.toString());
		}
	}

	/**
	 * Requests the Trading defaults 
	 */
	private void showPortfolioAndAccount(final long portfolioId, final long accountId) {

		RequestStateEvent<TradingDefaultsRequest> rse = new RequestStateEvent<TradingDefaultsRequest>() {
			@Override
			public void onRequestCompleted(TradingDefaultsRequest stexDefaultsRequest) {
				receiveStexDefaults(stexDefaultsRequest, portfolioId, accountId);
			}
		};

		// Initiate the request
		TradingDefaultsRequest request = TradingService.getTradingDefaults(rse);
		
		// Add the request to the request queue
		request.initiateServerRequest();
	}

	/**
	 * Callback when receiving the stex defaults
	 * 
	 * @param stexDefaultsRequest
	 */
	private void receiveStexDefaults(TradingDefaultsRequest stexDefaultsRequest, long portfolioId, long accountId) {

		TradingDefaultsResult stexDefaultsResult = stexDefaultsRequest.getResponse().getData();
		if (stexDefaultsResult != null) {

			for (MoneyAccountTO account : stexDefaultsResult.getAccounts()) {
				
				if (account.getId().longValue() == accountId){
					setTextView(R.id.trading_order_detail_account, account.getAccountType()+" / "+account.getAccountIban());
				}
				
			}

			for (ContainerPortfolioTO portfolio : stexDefaultsResult.getPortfolios()) {
				
				if (portfolio.getId().longValue() == portfolioId){
					setTextView(R.id.trading_order_detail_portfolio, portfolio.getName());
				}
				
			}
		}
	}
	
}
