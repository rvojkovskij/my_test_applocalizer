package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.PaymentQueryTO;
import com.avaloq.afs.server.bsp.client.ws.PaymentSearchStateType;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.banklet.trading.BuyAdapter.ViewHolder;
import com.avaloq.banklet.trading.TradingSellDetailActivity.ButtonSellBuyStatus;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.ProgressiveBaseAdapter;
import com.avaloq.framework.tools.ProgressiveListView;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;
import com.avaloq.framework.ui.BankletFragment;

public class BuyFragment extends BankletFragment {

	public class InstrumentIdValuePair {
		String id;
		String value;
	}

	// private String TAG = BuyActivity.class.getSimpleName();

	private SearchView mSearchField;
	private BuyAdapter mBuyAdapter;
	private ListView mResultList;

	private View footerView;

	// used to store the index of the selected type
	private int mSelectedInstrumentTypeIndex;

	// holder for the search string
	private String mSearchString;

	// the timer used to delay the execution of the search
	private Timer searchTimer;

	// holder for the elements of the instrument type spinner
	private List<InstrumentIdValuePair> mInstrumentTypes = new ArrayList<InstrumentIdValuePair>();

	// The filter will set the value of this member
	private StexAssetGroupType selectedInstrumentTypeInFilter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.trd_buy_activity, container, false);

		mBuyAdapter = new BuyAdapter(this) {
			
			@Override
			public void loadNext() {
				
				// Load the next "page" of results
				showListLoading(true);
				mBuyAdapter.executeSearch(mSearchField.getText(), false, selectedInstrumentTypeInFilter);
			}

			@Override
			public ListView getListView() {				
				return mResultList;
			}

			@Override
			public void loadingStateChanged(boolean loading) {
				mSearchField.setProgressVisibility(loading);
			}
		};

		mSearchField = (SearchView) fragmentView.findViewById(R.id.search_widget);
		
		mResultList = (ListView) fragmentView.findViewById(R.id.trd_buy_list);
		mResultList.addHeaderView(mBuyAdapter.getHeaderView());
		mResultList.setAdapter(mBuyAdapter);

		footerView = mBuyAdapter.getFooterView();

		// add the click listener for the result list
		setResultClickListener(mResultList);

		// Attaches the listener for the search field
		addChangeListenerToSearchField(fragmentView);

		return fragmentView;
	}

	/**
	 * Sets the click listener to the elements of the result list
	 */
	private void setResultClickListener(ListView resultList) {
		resultList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View listElement, int arg2, long arg3) {

				ViewHolder holder = (ViewHolder) listElement.getTag();

				// if this is null, probably this is the Header row
				if (holder == null) {
					return;
				}
				
				if (!AvaloqApplication.getInstance().getConfiguration().hideMarketData()){

					Intent intent = new Intent();
					intent.setClass(getActivity(), TradingSellDetailActivity.class);

					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_TYPE, holder.instrumentType.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.isin.getText().toString());
					intent.putExtra(TradingSellDetailActivity.EXTRA_IS_BUY, ButtonSellBuyStatus.BUY);

					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);

					intent.putExtra(TradingSellDetailActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					startActivity(intent);
				}
				else {
					Intent intent = new Intent();
					intent.setClass(getActivity(), PositionBuyActivity.class);
					
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_ID, holder.instrumentId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_NAME, holder.name.getText().toString());
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_TYPE, holder.instrumentType.getText().toString().toUpperCase());
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_SUB_TYPE, holder.isin.getText().toString());
					
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_CURRENCY_ID, holder.currencyId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_ID, holder.marketId);
					intent.putExtra(PositionBuyActivity.EXTRA_INSTRUMENT_MARKET_NAME, holder.marketName);
					
					intent.putExtra(PositionSellActivity.EXTRA_INSTRUMENT_IS_MONEY_TRADABLE, holder.isMoneyTradable);
					
					startActivity(intent);
				}
			}
		});
	}

	/**
	 * Attaches the change listener to the search field and constructs the logic
	 * for the Clear button
	 * 
	 * @param fragmentView
	 *            The fragment
	 */
	private void addChangeListenerToSearchField(final View fragmentView) {

		
		mSearchField.init(this);
		mSearchField.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				for (Criterium crit: mSearchField.getCriteria())
					crit.addToQuery();

				mBuyAdapter.executeSearch(mSearchField.getText(), true, selectedInstrumentTypeInFilter);				
			}
		});
		
		
		mSearchField.addCriterium(new SingleChoiceCriterium<StexAssetGroupType>(getActivity()) {

			@Override
			public LinkedHashMap<StexAssetGroupType, String> getTypeLabelMapping() {
				LinkedHashMap<StexAssetGroupType, String> map =  new LinkedHashMap<StexAssetGroupType, String>();
				map.put(StexAssetGroupType.EQUITIES, getString(R.string.trading_equities));
				map.put(StexAssetGroupType.FUNDS, getString(R.string.trading_funds));
				map.put(StexAssetGroupType.BONDS, getString(R.string.trading_bonds));				
				return map;
			}

			@Override
			public void addToQuery() {
				selectedInstrumentTypeInFilter = mElement;
			}

			@Override
			public String getName() {
				return getString(R.string.trading_header_instrument_type);
			}

			@Override
			public ArrayList<Integer> getIcons() {
				ArrayList<Integer> ret = new ArrayList<Integer>();
				
				ret.add(R.drawable.trd_icon_equities_small);
				ret.add(R.drawable.trd_icon_funds_small);				
				ret.add(R.drawable.trd_icon_bonds_small);

				return ret;
			}
		});
	}

	/**
	 * Creates the options for the spinner Instrument type and assigns them to
	 * the private member mInstrumentTypes of the class
	 */
	private List<String> generateInstrumentTypeOptionsString() {
		
		List<String> ret = new ArrayList<String>();

		for (StexAssetGroupType type: StexAssetGroupType.values()){
			int resId = getActivity().getResources().getIdentifier("trading_"+type.name().toLowerCase(), "string", getActivity().getBaseContext().getPackageName());
			String strType = resId == 0 ? "" : (String) getActivity().getResources().getText(resId);
			ret.add(strType);
		}
		
		return ret;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SearchView.RESULT_CODE)
			mSearchField.onActivityResult(requestCode, resultCode, data);
	}
}