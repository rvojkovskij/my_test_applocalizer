package com.avaloq.banklet.trading;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.avaloq.banklet.trading.OrderDetailDataFragment.OrderDetailDataFragmentInterface;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletActivity;

/**
 * Activity for order details
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class OrderDetailActivity extends TabbedBankletActivity implements OrderDetailDataFragmentInterface{

	private boolean mWasOrderCancelled = false;
	
	/**
	 * This activity requires the id of an order detail to be set
	 */
	protected static final String EXTRA_ORDER_DETAIL_ID = "extra_order_detail_id";
	protected static final String EXTRA_ORDER_DETAIL = "extra_order_detail";

	@Override
	public void prepareNavigationItems() {

		Bundle dataArguments = new Bundle();
		dataArguments.putLong(OrderDetailDataFragment.EXTRA_ORDER_DETAIL_ID, getIntent().getExtras().getLong(EXTRA_ORDER_DETAIL_ID));
		dataArguments.putSerializable(OrderDetailDataFragment.EXTRA_ORDER_DETAIL, getIntent().getExtras().getSerializable(EXTRA_ORDER_DETAIL));
		
		// uses different logic for small and large layouts
		if (BankletActivityDelegate.isLargeDevice(this)) {

			// on large devices no navigation items, but 2 fragments on 1 page			
			showNavigationItems(new FragmentNavigationItem[] { new FragmentNavigationItem(OrderDetailFragment.class, getResources().getString(R.string.trd_activity_order_detail_title), dataArguments) });
		} else {
			// on smaller devices 2 navigation items with a fragment each
			Bundle transactionsArguments = new Bundle();
			transactionsArguments.putLong(OrderDetailTransactionsFragment.EXTRA_ORDER_DETAIL_ID, getIntent().getExtras().getLong(EXTRA_ORDER_DETAIL_ID));
			transactionsArguments.putSerializable(OrderDetailTransactionsFragment.EXTRA_ORDER_DETAIL, getIntent().getExtras().getSerializable(EXTRA_ORDER_DETAIL));			
			showNavigationItems(new FragmentNavigationItem[] { new FragmentNavigationItem(OrderDetailDataFragment.class, getResources().getString(R.string.trd_order_details_tab_details), dataArguments), new FragmentNavigationItem(OrderDetailTransactionsFragment.class, getResources().getString(R.string.trd_order_details_tab_history), transactionsArguments) });
		}
	}

	
	/**
	 * After canceling an order and returning to the list, the list has to be refreshed
	 */
	@Override
	public void onBackPressed() {
		if (mWasOrderCancelled){
			Intent returnIntent = new Intent();
			returnIntent.putExtra(OrdersFragment.EXTRA_FULL_RELOAD, true);		
			setResult(Activity.RESULT_OK, returnIntent);     							
			finish();
		}else{
			finish();
		}
	}


	@Override
	public void orderCancelled() {
		mWasOrderCancelled = true;
	}
	
}
