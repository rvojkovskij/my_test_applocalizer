package com.avaloq.banklet.trading;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.avaloq.afs.aggregation.to.LocalizedNotification;
import com.avaloq.afs.aggregation.to.LocalizedValidationResult;
import com.avaloq.afs.aggregation.to.Result;
import com.avaloq.afs.aggregation.to.trading.StexOrderResult;
import com.avaloq.afs.server.bsp.client.ws.BankingPositionTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.afs.server.bsp.client.ws.StexOrderTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderType;
import com.avaloq.afs.server.bsp.client.ws.StexWorkflowInstructionType;
import com.avaloq.banklet.trading.PositionBuyFragment.PositionBuyAccountRecordType;
import com.avaloq.banklet.trading.PositionBuySellAccountListFragment.PositionBuySellAccountListFragmentInterface;
import com.avaloq.banklet.trading.PositionBuySellHeaderFragment.PositionBuyFragmentHeaderInterface;
import com.avaloq.banklet.trading.PositionBuySellHeaderFragmentElement.PositionBuySellHeaderFragmentElementInterface;
import com.avaloq.banklet.trading.PositionBuySellSelectAccountFragment.PositionBuySellSelectAccountFragmentInterface;
import com.avaloq.banklet.trading.PositionBuySellSelectAccountFragment.PositionBuySellSelectType;
import com.avaloq.banklet.trading.PositionSellFragment.PositionSellFragmentInterface;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.webservice.trading.Request;
import com.avaloq.framework.comms.webservice.trading.StexOrderRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.BlockingRunnable;
import com.avaloq.framework.ui.DialogFragment;

public class PositionSellActivity extends BankletActivity 
		implements 
		PositionBuyFragmentHeaderInterface, 
		PositionSellFragmentInterface,
		PositionBuySellSelectAccountFragmentInterface, 
		PositionBuySellAccountListFragmentInterface, 
		PositionBuySellHeaderFragmentElementInterface{

	public static final String EXTRA_INSTRUMENT_ID = "instrumentID";
	public static final String EXTRA_INSTRUMENT = "instrument";
	public static final String EXTRA_INSTRUMENT_NAME = "instrumentName";
	public static final String EXTRA_INSTRUMENT_TYPE = "instrumentType";
	public static final String EXTRA_INSTRUMENT_SUB_TYPE = "instrumentSubType";
	public static final String EXTRA_INSTRUMENT_CURRENCY_ID = "currencyId";
	public static final String EXTRA_INSTRUMENT_MARKET_ID = "marketId";
	public static final String EXTRA_INSTRUMENT_MARKET_NAME = "marketName";
	public static final String EXTRA_INSTRUMENT_PORTFOLIO_ID = "portfolioId";
	public static final String EXTRA_BANKING_POSITION_ID = "bankingPositionId";
	public static final String EXTRA_INSTRUMENT_IS_MONEY_TRADABLE = "EXTRA_INSTRUMENT_IS_MONEY_TRADABLE";

	private BankingPositionTO mInstrument;
	private Long mInstrumentId;
	private String mInstrumentName;
	private StexAssetGroupType mInstrumentType;
	private String mInstrumentSubType;

	private Long mCurrencyId;
	private Long mMarketId;
	private String mMarketName;
	private Long mPortfolioId;

	private boolean isInstrumentMoneyTradable;
	
	private Long mBankingPositionId;

	private boolean mListIsOpened = false;

	private List<PositionAccountRecord> mAccountsList;
	private List<PositionAccountRecord> mPortfolioList;
	List<PositionAccountRecord> mAccountsListToDisplay = new ArrayList<PositionAccountRecord>();

	private int mSelectedAccount = -1;
	private int mSelectedPortfolio = 0;

	private boolean mConfimMode = false;
	
	private StexOrderTO mOrderToConfirm;
	
	private int mSelectedListingIndex;
	
	private String mErrorAccount = "";	
	private String mErrorPortfolio = "";
	
	private boolean headerDataLoaded = false;
	private boolean accountDataLoaded = false;
	
	protected BlockingRunnable mSpinner;
	
	public void setErrorAccount(String errorAccount){
		mErrorAccount = errorAccount;
	}
	
	public void setErrorPortfolio(String errorPortfolio){
		mErrorPortfolio = errorPortfolio;
	}
	
	public void resetErrors(){
		getFormFragment().resetErrors();
		mErrorAccount = "";
		mErrorPortfolio = "";
	}
	
	public void displayErrors(){
		getFormFragment().displayErrors();
		
		if (mErrorAccount.length() > 0){
			((TextView) findViewById(R.id.trading_error_account)).setText(mErrorAccount);
			((TextView) findViewById(R.id.trading_error_account)).setVisibility(View.VISIBLE);
		}else{
			((TextView) findViewById(R.id.trading_error_account)).setText("");
			((TextView) findViewById(R.id.trading_error_account)).setVisibility(View.GONE);
		}
		
		if (mErrorPortfolio.length() > 0){
			((TextView) findViewById(R.id.trading_error_portfolio)).setText(mErrorPortfolio);
			((TextView) findViewById(R.id.trading_error_portfolio)).setVisibility(View.VISIBLE);
		}else{
			((TextView) findViewById(R.id.trading_error_portfolio)).setText("");
			((TextView) findViewById(R.id.trading_error_portfolio)).setVisibility(View.GONE);
		}	
		
	}
	
	@Override
	public void onPause(){
		super.onPause();
		mConfimMode = false;
		getFormFragment().setIsEditable(true);		
		finish();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mSpinner = new BlockingRunnable(this, "", getString(R.string.trd_processing));
		
		// Getting the extra arguments
		Bundle b = getIntent().getExtras();
		mInstrument = (BankingPositionTO) b.getSerializable(EXTRA_INSTRUMENT);
		mInstrumentId = b.getLong(EXTRA_INSTRUMENT_ID);
		mInstrumentName = b.getString(EXTRA_INSTRUMENT_NAME);
		mInstrumentType = StexAssetGroupType.fromValue(b.getString(EXTRA_INSTRUMENT_TYPE));
		mInstrumentSubType = b.getString(EXTRA_INSTRUMENT_SUB_TYPE);

		mCurrencyId = b.getLong(EXTRA_INSTRUMENT_CURRENCY_ID);
		mMarketId = b.getLong(EXTRA_INSTRUMENT_MARKET_ID);
		mMarketName = b.getString(EXTRA_INSTRUMENT_MARKET_NAME);

		isInstrumentMoneyTradable = b.getBoolean(EXTRA_INSTRUMENT_IS_MONEY_TRADABLE);
		
		mPortfolioId = b.getLong(EXTRA_INSTRUMENT_PORTFOLIO_ID);

		mBankingPositionId = b.getLong(EXTRA_BANKING_POSITION_ID);

		setContentView(R.layout.trd_position_sell_activity);
		
		setConfirmMode();
		
		// get the default listing here		
		mSelectedListingIndex = -1;
		
		// set the title
		switch (mInstrumentType){
		case BONDS:
			setTitle(getText(R.string.trd_title_activity_position_sell) +" "+getText(R.string.trading_bonds).toString().toLowerCase());
			break;
		case EQUITIES:
			setTitle(getText(R.string.trd_title_activity_position_sell) +" "+getText(R.string.trading_equities).toString().toLowerCase());
			break;
		case FUNDS:
			setTitle(getText(R.string.trd_title_activity_position_sell) +" "+getText(R.string.trading_funds).toString().toLowerCase());
			break;
		default:
			setTitle(getText(R.string.trd_title_activity_position_sell));
			break;		
		}
	}
	
	/**
	 * 
	 */
	protected void showProgress(){
		mSpinner.show();
	}
	
	/**
	 * 
	 */
	protected void hideProgress(){
		mSpinner.hide();
	}
	
	@Override
	public Long getInstrumentId() {
		return mInstrumentId;
	}

	@Override
	public String getInstrumentName() {
		return mInstrumentName;
	}

	@Override
	public StexAssetGroupType getInstrumentType() {
		return mInstrumentType;
	}

	@Override
	public String getInstrumentSubType() {
		return mInstrumentSubType;
	}

	@Override
	public Long getCurrencyId() {
		return mCurrencyId;
	}

	@Override
	public String getMarketName() {
		return mMarketName;
	}

	public void confirmClick(View v) {
		if (getFormFragment().fieldsAreValid()) {
			// Get the necessary data from fragments to make the request
			StexOrderTO order = new StexOrderTO();

			order.setStexAssetGroupType(mInstrumentType);
			order.setMoneyAccountId((mSelectedAccount == -1) ? 0L : mAccountsList.get(mSelectedAccount).id);
			order.setPortfolioId((mSelectedPortfolio == -1) ? 0L : mPortfolioList.get(mSelectedPortfolio).id);	
			order.setCurrencyId(mCurrencyId);
			order.setInstrumentId(mInstrumentId);
			order.setMarketId(mMarketId);
			if (getFormFragment().mustShowExpirationDate()){
				order.setExpirationDate(getFormFragment().getExpirationDate());
			}
			if (isInstrumentMoneyTradable){
				order.setOrderAmount(getFormFragment().getOrderAmount());
			}
			order.setOrderQuantity(getFormFragment().getOrderQuantity());
			order.setStexExecType(getFormFragment().getStexExecType());
			order.setLimitPrice(getFormFragment().getLimitPrice());
			order.setStexOrderType(StexOrderType.SELL);
			order.setTriggerPrice(getFormFragment().getTriggerPrice());

			order.setStexWorkflowInstructionType(StexWorkflowInstructionType.SEND_TO_TRADING);
			
			verifyOrder(order);
		}
		/*
		 * + "stexAssetGroupType": "EQUITIES", + "moneyAccountId": "766", +
		 * "portfolioId": "765", + "currencyId":"53", + "instrumentId": "395", ?
		 * "marketId":"461", i+ "expirationDate":
		 * "2013-05-16T18:39:50.322+01:00", i+ "orderQuantity": "333", i+
		 * "stexExecType": "LIMIT", + "limitPrice": "50.03", + "stexOrderType":
		 * "BUY" + "triggerPrice": "50.00"
		 */
	}

	/**
	 * Second step confirmation
	 * @param view
	 */
	public void confirmClick_step2(View view){
		if(mOrderToConfirm != null) submitOrder(mOrderToConfirm);
	}
	
	/**
	 * Returns the fragment containing the form with input fields
	 * 
	 * @return
	 */
	private PositionSellFragment getFormFragment() {
		PositionSellFragment fragment = (PositionSellFragment) getSupportFragmentManager().findFragmentById(R.id.trading_positionSellFragment);

		if (fragment != null && fragment.isInLayout()) {
			return fragment;
		}

		return null;
	}

	/**
	 * Makes a request to verify the order
	 * 
	 * @param order
	 *            the order object
	 */
	private void verifyOrder(final StexOrderTO order) {
		
		// hide all errors
		resetErrors();
		displayErrors();
		
		RequestStateEvent<Request> rse = new RequestStateEvent<Request>() {
			@Override
			public void onRequestCompleted(Request request) {
				verificationResultReceived(request, order);
			}
		};

		// Initiate the request
		Request request = TradingService.verifyOrder(order, rse);

		setConfirmButtonEnabled(false);

		request.setCachePolicy(CachePolicy.NO_CACHE);
		
		// Add the request to the request queue
		request.initiateServerRequest();
		
		showProgress();
	}

	/**
	 * Callback function called when the verification service returns the result
	 * 
	 * @param request
	 * @param order
	 */
	private void verificationResultReceived(Request request, StexOrderTO order) {
		
		hideProgress();
		
		Result result = request.getResponse().getData();
		if (result != null) {

			// If the result has a notification list, there are errors
			if (result.getNotificationList() != null && result.getNotificationList().size() > 0) {
				// there are errors in the order, we have to display them
				String errorString = "";

				for (LocalizedNotification notification : result.getNotificationList()) {

					errorString += ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage()+'\n';
					
					String message = ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage();
					
					// set the error messages for the respective fields
					if(notification.getValidationResult().getFieldName().compareTo("limitPrice") == 0 ){
						getFormFragment().setErrorLimitPrice(message);
					}else if(notification.getValidationResult().getFieldName().compareTo("triggerPrice") == 0 ){
						getFormFragment().setErrorTriggerPrice(message);
					}else if(notification.getValidationResult().getFieldName().compareTo("orderQuantity") == 0 ){
						getFormFragment().setErrorQuantity(message);
					}else if(notification.getValidationResult().getFieldName().compareTo("orderAmount") == 0 ){
						getFormFragment().setErrorAmount(message);						
					}else if (notification.getValidationResult().getFieldName().compareTo("expirationDate") == 0){
						getFormFragment().setErrorExecutionDate(message);
					}else if (notification.getValidationResult().getFieldName().compareTo("moneyAccountId") == 0){
						setErrorAccount(message);
					}else if (notification.getValidationResult().getFieldName().compareTo("portfolioId") == 0){
						setErrorPortfolio(message);
					}
					
				}
				displayErrors();
				DialogFragment.createAlert("", errorString, this).show(this);
				setConfirmButtonEnabled(true);
				
				mConfimMode = false;
				setConfirmMode();
			} else {
				mConfimMode = true;
				setConfirmMode();
				mOrderToConfirm = order;
			}
		}
	}

	/**
	 * 
	 * @param confirmMode
	 */
	public void setConfirmMode(){		
		if (mConfimMode){
			getFormFragment().setIsEditable(false);
			
			// set the button to confirm
			((View)findViewById(R.id.trading_buy_btnConfirm)).setVisibility(View.GONE);
			((View)findViewById(R.id.trading_buy_btnConfirm_step2)).setVisibility(View.VISIBLE);
			((View)findViewById(R.id.trading_buy_btnConfirm_step2)).setEnabled(true);
		}else{
			getFormFragment().setIsEditable(true);
			
			// set the button to continue			
			((View)findViewById(R.id.trading_buy_btnConfirm)).setVisibility(View.VISIBLE);
			((View)findViewById(R.id.trading_buy_btnConfirm_step2)).setVisibility(View.GONE);
		}
	}
	
	/**
	 * Submit the order
	 * 
	 * @param order
	 */
	private void submitOrder(final StexOrderTO order) {
		RequestStateEvent<StexOrderRequest> rse = new RequestStateEvent<StexOrderRequest>() {
			@Override
			public void onRequestCompleted(StexOrderRequest request) {
				executionResultReceived(request, order);
			}
		};

		// Initiate the request
		StexOrderRequest request = TradingService.submitOrder(order, rse);

		setConfirmButtonEnabled(false);
		// Add the request to the request queue
		request.initiateServerRequest();
		
		showProgress();
	}

	/**
	 * Callback for the order submission
	 */
	private void executionResultReceived(StexOrderRequest request, StexOrderTO order) {
		
		hideProgress();
		
		StexOrderResult result = request.getResponse().getData();
		if (result != null) {

			// If the result has a notification list, there are errors
			if (result.getNotificationList() != null && result.getNotificationList().size() > 0) {
				// there are errors in the order, we have to display them
				String errorString = "";

				for (LocalizedNotification notification : result.getNotificationList()) {
					errorString += ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage()+'\n';
					
					String message = ((LocalizedValidationResult)notification.getValidationResult()).getLocalizedMessage();
					
					// set the error messages for the respective fields
					if(notification.getValidationResult().getFieldName().compareTo("limitPrice") == 0 ){
						getFormFragment().setErrorLimitPrice(message);
					}else if(notification.getValidationResult().getFieldName().compareTo("triggerPrice") == 0 ){
						getFormFragment().setErrorTriggerPrice(message);
					}else if(notification.getValidationResult().getFieldName().compareTo("orderQuantity") == 0 ){
						getFormFragment().setErrorQuantity(message);
					}else if (notification.getValidationResult().getFieldName().compareTo("expirationDate") == 0){
						getFormFragment().setErrorExecutionDate(message);
					}
				}
				
				getFormFragment().setIsEditable(true);
				
				// there are errors, display them and return to editable mode
				if (result.getNotificationList().size() > 0){
					DialogFragment.createAlert("", errorString).show();
					displayErrors();
					
				}else{					
					resetErrors();
				}

				mConfimMode = false;
				setConfirmMode();
				mOrderToConfirm = null;
				setConfirmButtonEnabled(true);
				return;
			}else{				
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BankletActivity.getActiveActivity()); 
				alertDialogBuilder
					.setMessage(R.string.trd_order_placed_text)
					.setPositiveButton(R.string.avq_ok, new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int which) {
				            // Finish activity
				            finish();
				        }
				    });
				alertDialogBuilder.create().show();	
			}
		}		
	}

	/**
	 * Hides the spinner and shows the content
	 */
	private void hideSpinner(){
		if ( (headerDataLoaded || BankletActivityDelegate.isLargeDevice(this)) && accountDataLoaded ){
			findViewById(R.id.loading_container).setVisibility(View.GONE);
			findViewById(R.id.trd_scrollview).setVisibility(View.VISIBLE);
		}
	}
	
	/**
	 * When the back button is pressed and the account or portfolio list is
	 * visible, do not return to previous activity, but hide the list
	 */
	@Override
	public void onBackPressed() {
		// the back button should just close the select list if it is open
		if (mListIsOpened) {
			hideListSelect();			
		} else if (mConfimMode){ // if on second step, go back
			mConfimMode = false;
			setConfirmMode();
			mOrderToConfirm = null;
			setConfirmButtonEnabled(true);
		}else{
			super.onBackPressed();
		}
	}

	/**
	 * When either the account select, or portfolio select is clicked
	 */
	@Override
	public void selectAccountItemClicked(PositionBuySellSelectType type) {

		// the selection has to be disabled if this form is in "confirmation" mode
		if (mConfimMode) return;
		
		// show the list of accounts or portfolios
		if (type == PositionBuySellSelectType.ACCOUNT) {
			PositionBuySellAccountListFragment fragment = (PositionBuySellAccountListFragment) getSupportFragmentManager().findFragmentById(
					R.id.trading_buy_selectAccountFragmentSelectList);
			if (fragment != null && fragment.isInLayout()) {
				fragment.setAccountList(mAccountsListToDisplay, this, type, mSelectedAccount);
			}
		}

		if (type == PositionBuySellSelectType.PORTFOLIO){
			PositionBuySellAccountListFragment fragment = (PositionBuySellAccountListFragment) getSupportFragmentManager().findFragmentById(R.id.trading_buy_selectAccountFragmentSelectList);
	        if (fragment != null && fragment.isInLayout()) {
	        	fragment.setAccountList(mPortfolioList, this, type, mSelectedPortfolio);	        	
	        } 
		}
		
		findViewById(R.id.trading_llPositionSellFragment).setVisibility(View.GONE);
		findViewById(R.id.trading_llPositionSellFragmentSelect).setVisibility(View.GONE);
		findViewById(R.id.trading_llPositionBuyFragmentSelectPortfolio).setVisibility(View.GONE);	
		findViewById(R.id.trading_buy_disclaimer).setVisibility(View.GONE);
		findViewById(R.id.trading_buy_btnConfirm).setVisibility(View.GONE);
		findViewById(R.id.trading_llPositionBuyFragmentSelectList).setVisibility(View.VISIBLE);
		findViewById(R.id.trading_error_account).setVisibility(View.GONE);
		findViewById(R.id.trading_error_portfolio).setVisibility(View.GONE);
		findViewById(R.id.listFragment).setVisibility(View.GONE);
		
		mListIsOpened = true;
	}

	/**
	 * Called by the fragment when an item is selected
	 */
	@Override
	public void listAccountItemSelected(PositionBuySellSelectType type, int position) {

		(new Timer()).schedule(new TimerTask() {
			@Override
			public void run() {
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						hideListSelect();
					}
				});
			}
		}, 300);

		if (type == PositionBuySellSelectType.ACCOUNT) {
			mSelectedAccount = position;
			showSelectedAccount();
		}
	}

	/**
	 * Hides the account or portfolio list
	 */
	private void hideListSelect() {
		findViewById(R.id.trading_llPositionBuyFragmentSelectList).setVisibility(View.GONE);
		findViewById(R.id.trading_buy_btnConfirm).setVisibility(View.VISIBLE);
		findViewById(R.id.trading_llPositionSellFragment).setVisibility(View.VISIBLE);
		findViewById(R.id.trading_llPositionSellFragmentSelect).setVisibility(View.VISIBLE);
		findViewById(R.id.trading_buy_disclaimer).setVisibility(View.VISIBLE);
		findViewById(R.id.trading_llPositionBuyFragmentSelectPortfolio).setVisibility(View.VISIBLE);
		findViewById(R.id.listFragment).setVisibility(View.VISIBLE);
		
		if (mErrorAccount.length()>0)findViewById(R.id.trading_error_account).setVisibility(View.VISIBLE);
		if (mErrorPortfolio.length()>0)findViewById(R.id.trading_error_portfolio).setVisibility(View.VISIBLE);
		
		mListIsOpened = false;
	}

	/**
	 * Because the account list is received after the activity is loaded, this
	 * method is called by the fragment when the account list is received from
	 * the server
	 */
	@Override
	public void accountRecordsReceived(List<PositionAccountRecord> records, PositionBuyAccountRecordType type) {
		if (type == PositionBuyAccountRecordType.ACCOUNT) {
			mAccountsList = records;
			showSelectedAccount();
		}
		if (type == PositionBuyAccountRecordType.PORTFOLIO){
			mPortfolioList = records;
			showSelectedPortfolio();
			

			// reset the list of accounts belonging to this portfolio
			mSelectedAccount = -1;
        	// create the list of accounts that belong to the selected portfolio
        	
			mAccountsListToDisplay.clear();
        	for (int i=0;i<mAccountsList.size();i++){
        		if (mAccountsList.get(i).parentId.longValue() == mPortfolioList.get(mSelectedPortfolio).id.longValue()){
        			mAccountsListToDisplay.add(mAccountsList.get(i));
        		}
        	}
        	
        	showSelectedAccount();
		}
		accountDataLoaded = true;
		hideSpinner();
	}

	private void showSelectedAccount(){
		PositionBuySellSelectAccountFragment fragment = (PositionBuySellSelectAccountFragment) getSupportFragmentManager().findFragmentById(R.id.trading_buy_selectAccountFragmentSelect);
        if (fragment != null && fragment.isInLayout()) {
        	
        	fragment.setType(PositionBuySellSelectType.ACCOUNT);
        	
        	if (mSelectedAccount == -1){      
        		// try to get the default account
        		if (getFormFragment() != null && getFormFragment().getDefaultAccountId() != null && mSelectedPortfolio >= 0 && mPortfolioList != null && mPortfolioList.size() > mSelectedPortfolio){
        			int i = 0;
        			long defaultAccountId = getFormFragment().getDefaultAccountId().longValue();
        			for (PositionAccountRecord account: mAccountsList){
        				if (account.id.longValue() == defaultAccountId && account.parentId.longValue() == mPortfolioList.get(mSelectedPortfolio).id.longValue() && mSelectedPortfolio >= 0){
        					mSelectedAccount = i;
        				}
        				i++;
        			}
        		}
        		if (mSelectedAccount != -1){
        			fragment.setData(mAccountsList.get(mSelectedAccount), mAccountsListToDisplay.size()>0);
        		}else{
        			fragment.setData(null, mAccountsListToDisplay.size()>0);
        		}
        	}else{
        		fragment.setData(mAccountsList.get(mSelectedAccount), mAccountsListToDisplay.size()>0);
        	}	        	
        }
	}
	
	private void showSelectedPortfolio(){
		PositionBuySellSelectAccountFragment fragment = (PositionBuySellSelectAccountFragment) getSupportFragmentManager().findFragmentById(R.id.trading_buy_selectPortfolioFragmentSelect);
        if (fragment != null && fragment.isInLayout()) {
        	
        	fragment.setType(PositionBuySellSelectType.PORTFOLIO);
        	
        	if (mSelectedPortfolio == -1){
        		// try to set the default portfolio
        		if (getFormFragment() != null && getFormFragment().getDefaultPortfolioId() != null){
        			int i = 0;
        			long defaultPortfolioId = getFormFragment().getDefaultPortfolioId().longValue();
        			for (PositionAccountRecord portfolio: mPortfolioList){        				
        				if (portfolio.id.longValue() == defaultPortfolioId){
        					mSelectedPortfolio = i;
        				}
        				i++;
        			}
        		}
        		// if the default portfolio was not found
        		if (mSelectedPortfolio == -1){
        			fragment.setData(null, mPortfolioList.size()>0);        		
        		}else{
        			fragment.setData(mPortfolioList.get(mSelectedPortfolio), mPortfolioList.size()>0);
        		}
        	}else{
        		fragment.setData(mPortfolioList.get(mSelectedPortfolio), mPortfolioList.size()>0);
        	}	        	
        }
	}

	/**
	 * Method for enabling/disabling the "Confirm" button
	 */
	@Override
	public void setConfirmButtonEnabled(boolean enabled) {
		if (findViewById(R.id.trading_buy_btnConfirm) != null)
			((Button) findViewById(R.id.trading_buy_btnConfirm)).setEnabled(enabled);
	}

	@Override
	public Long getBankingPositionId() {
		return mBankingPositionId;
	}

	@Override
	public boolean isBuy() {		
		return false;
	}

	@Override
	public int getSelectedListingIndex() {		
		return mSelectedListingIndex;
	}

	@Override
	public void setSelectedListing(int pos) {
		PositionBuySellHeaderFragment fragment = (PositionBuySellHeaderFragment) getSupportFragmentManager().findFragmentById(R.id.listFragment);
		fragment.setSelectedListing(pos);
		// get the market id from the fragment
		mMarketId = fragment.getMarketId();
	}

	@Override
	public Long getMarketId() {		
		return mMarketId;
	}

	@Override
	public boolean isInstrumentMoneyTradable() {		
		return isInstrumentMoneyTradable;
	}

	@Override
	public void showEstimate(BigDecimal price, Long currencyId) {
		headerDataLoaded = true;
		hideSpinner();
		getFormFragment().setUnitPrice(price, currencyId);
	}

	@Override
	public BankingPositionTO getInstrument() {		
		return mInstrument;
	}
}