package com.avaloq.banklet.trading;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.Banklet;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.ActivityNavigationItem;

public class TradingBanklet extends Banklet {
	
	private static final ActivityNavigationItem[] items = new ActivityNavigationItem[] {
		new ActivityNavigationItem(AvaloqApplication.getContext(), TradingOverviewActivity.class, R.string.title_activity_trading_overview, R.attr.IconTrading, R.style.Theme_Banklet_TRD)
	};

	@Override
	public void onCreate() {
		

	}

	@Override
	public ActivityNavigationItem getInitialActivity() {
		return items[0];
	}

	@Override
	public ActivityNavigationItem[] getMainNavigationItems() {
		return items;
	}

	@Override
	public void onEmptyCache() {
		// TODO Empty all model data
	}
	
	/**
	 * Get the TradingBanklet singleton instance
	 */
	public static TradingBanklet getInstance() {
		return (TradingBanklet) Banklet.getInstanceOf(TradingBanklet.class);
	}

}
