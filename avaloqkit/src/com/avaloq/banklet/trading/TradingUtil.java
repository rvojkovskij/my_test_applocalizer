package com.avaloq.banklet.trading;

import java.util.List;

import com.avaloq.afs.server.bsp.client.ws.StexOrderDetailTO;

/**
 * Contains helper methods for the trading banklet
 * @author Timo Schmid <t.schmid@insign.ch>
 *
 */
public class TradingUtil {
	
	/**
	 * Finds the detail of an order in a list
	 * @param orderDetailId The id of the order detail
	 * @param orderDetailList The list of order details
	 * @return The found order detail or null
	 */
	public static StexOrderDetailTO getOrderDetail(long orderDetailId, List<StexOrderDetailTO> orderDetailList) {
		for(StexOrderDetailTO detail : orderDetailList) {
			if(orderDetailId == detail.getId()) {
				return detail;
			}
		}
		return null;
	}

}
