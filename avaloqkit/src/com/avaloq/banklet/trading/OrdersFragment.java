package com.avaloq.banklet.trading;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.avaloq.afs.server.bsp.client.ws.ContainerPortfolioTO;
import com.avaloq.afs.server.bsp.client.ws.MoneyAccountTO;
import com.avaloq.afs.server.bsp.client.ws.StexAssetGroupType;
import com.avaloq.afs.server.bsp.client.ws.StexOrderDetailTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderStateType;
import com.avaloq.afs.server.bsp.client.ws.StexOrderType;
import com.avaloq.banklet.trading.OrdersAdapter.ViewHolder;
import com.avaloq.framework.R;
import com.avaloq.framework.tools.searchwidget.AccountCriterionSingle;
import com.avaloq.framework.tools.searchwidget.AccountCriterium;
import com.avaloq.framework.tools.searchwidget.Criterium;
import com.avaloq.framework.tools.searchwidget.DateCriterium;
import com.avaloq.framework.tools.searchwidget.MultipleChoiceCriterium;
import com.avaloq.framework.tools.searchwidget.PortfolioCriterion;
import com.avaloq.framework.tools.searchwidget.PortfolioCriterionSingle;
import com.avaloq.framework.tools.searchwidget.SearchView;
import com.avaloq.framework.tools.searchwidget.SingleChoiceCriterium;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.ui.DialogFragment;

public class OrdersFragment extends BankletFragment {

	private OrdersAdapter mOrdersAdapter;

	private ListView mResultList;

	private int DETAIL_REQUEST_CODE = 1;
	
	public static final String EXTRA_MESSAGE = "message";
	public static final String EXTRA_FULL_RELOAD = "fullReload";
	
	/*
	 * The search field element where the user can enter a string to filter the
	 * results
	 */
	private SearchView mSearchField;
	private StexOrderQueryTO mQuery;

	private StexOrderType filterStexOrderType;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.trd_orders_fragment, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mQuery = new StexOrderQueryTO();
		
		/*
		 * When the criteria in the search field changes we have to filter the results
		 */
		mSearchField = (SearchView) getView().findViewById(R.id.trd_orders_search);		
		mSearchField.init(this);
		
		/*
		 * We get the list element, create a list adapter and set it as adapter
		 * for the list. Additionally, we set the header row for the list.
		 */
		mOrdersAdapter = new OrdersAdapter(this){

			@Override
			public void loadNext() {				
				showListLoading(true);
				mOrdersAdapter.getDataFromServer(mQuery, false);
			}

			@Override
			public ListView getListView() {
				return mResultList;
			}

			@Override
			public void loadingStateChanged(boolean loading) {
				mSearchField.setProgressVisibility(loading);
			
			}
		};
		mResultList = (ListView) getView().findViewById(R.id.trd_orders_list);
		mResultList.addHeaderView(mOrdersAdapter.getHeaderView());
		mResultList.setAdapter(mOrdersAdapter);

		mResultList.setClickable(true);

		/*
		 * When a list is clicked, the detail view for that element is opened.
		 * Because we already have the data for the instrument and we do not
		 * want to load it again in the detail view, we just pass it here as
		 * extra arguments
		 */
		mResultList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View listElement, int position, long id) {

				ViewHolder holder = (ViewHolder) listElement.getTag();

				// if this is null, this is the header
				if (holder == null) {
					return;
				}

				 Intent intent = new Intent();
				 StexOrderDetailTO orderDetailTO = (StexOrderDetailTO) mOrdersAdapter.getItem(position - 1);
				 intent.putExtra(OrderDetailActivity.EXTRA_ORDER_DETAIL_ID, orderDetailTO.getId());
				 intent.putExtra(OrderDetailActivity.EXTRA_ORDER_DETAIL, orderDetailTO);
				 intent.setClass(getActivity(), OrderDetailActivity.class);
				
				 startActivityForResult(intent, DETAIL_REQUEST_CODE);
			}

		});

//		/*
//		 * This will make a call to the server to load the data
//		 */
		mOrdersAdapter.getDataFromServer(mQuery, true);
		
		initializeSearch();
	}
	
	
	private void initializeSearch(){

		mSearchField.setOnCriteriaChangedCallback(new Runnable(){
			@Override
			public void run() {
				
				for (Criterium crit: mSearchField.getCriteria())
					crit.addToQuery();

				/*
				 * This will make a call to the server to load the data
				 */
				mOrdersAdapter.getDataFromServer(mQuery, true);	
			}
		});
		
		mSearchField.addCriterium(new PortfolioCriterionSingle(getActivity()){
			@Override
			public void addToQuery() {
				mQuery.setPortfolioId(null);
				if (mElement != null){
					mQuery.setPortfolioId(mElement.getId());
				}
			}
		});
		
		mSearchField.addCriterium(new AccountCriterionSingle(getActivity()){
			@Override
			public void addToQuery() {
				mQuery.setMoneyAccountId(null);
				if (mElement != null) mQuery.setMoneyAccountId(mElement.getId());				
			}			
		});
		
		mSearchField.addCriterium(new DateCriterium(getActivity()){
			@Override
			public void addToQuery() {
				mQuery.setFromDate(null);
				mQuery.setToDate(null);
				if (mFrom != null)
					mQuery.setFromDate(mFrom);
				if (mTo != null)
					mQuery.setToDate(mTo);
			}
		});

		mSearchField.addCriterium(new SingleChoiceCriterium<StexOrderType>(getActivity()) {

			@Override
			public LinkedHashMap<StexOrderType, String> getTypeLabelMapping() {
				LinkedHashMap<StexOrderType, String> map =  new LinkedHashMap<StexOrderType, String>();
				map.put(StexOrderType.BUY, getString(R.string.trading_order_type_BUY));
				map.put(StexOrderType.SELL, getString(R.string.trading_order_type_SELL));
				return map;
			}

			@Override
			public void addToQuery() {
				mQuery.setOrderType(mElement);
			}

			@Override
			public String getName() {
				return getString(R.string.trading_header_type);
			}
		});
		
		mSearchField.addCriterium(new MultipleChoiceCriterium<StexOrderStateType>(getActivity()) {

			@Override
			public LinkedHashMap<StexOrderStateType, String> getTypeLabelMapping() {
				LinkedHashMap<StexOrderStateType, String> map =  new LinkedHashMap<StexOrderStateType, String>();
				map.put(StexOrderStateType.CANCELED, getString(R.string.trading_order_status_CANCELED));
				map.put(StexOrderStateType.DONE, getString(R.string.trading_order_status_DONE));
				map.put(StexOrderStateType.ERROR, getString(R.string.trading_order_status_ERROR));
				map.put(StexOrderStateType.EXPIRED, getString(R.string.trading_order_status_EXPIRED));
				map.put(StexOrderStateType.FULLY_EXECUTED, getString(R.string.trading_order_status_FULLY_EXECUTED));
				map.put(StexOrderStateType.NEW, getString(R.string.trading_order_status_NEW));
				map.put(StexOrderStateType.PARTIALLY_EXECUTED, getString(R.string.trading_order_status_PARTIALLY_EXECUTED));
				map.put(StexOrderStateType.PENDING_CANCEL, getString(R.string.trading_order_status_PENDING_CANCEL));
				map.put(StexOrderStateType.PENDING_NEW, getString(R.string.trading_order_status_PENDING_NEW));
				map.put(StexOrderStateType.RECTIFIED, getString(R.string.trading_order_status_RECTIFIED));
				map.put(StexOrderStateType.REJECTED, getString(R.string.trading_order_status_REJECTED));
				map.put(StexOrderStateType.REVERSED, getString(R.string.trading_order_status_RETURNED));
				map.put(StexOrderStateType.UNKNOWN, getString(R.string.trading_order_status_UNKNOWN));
				
				return map;
			}

			@Override
			public void addToQuery() { 
				mQuery.getOrderStates().clear();				
				for (StexOrderStateType orderState: mElements)
					mQuery.getOrderStates().add(orderState);
			}

			@Override
			public String getName() {
				return getString(R.string.trading_header_status);
			}
		});

	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == DETAIL_REQUEST_CODE) {
			if(resultCode == Activity.RESULT_OK){      
				boolean fullReload = data.getBooleanExtra(EXTRA_FULL_RELOAD, false);   
				String message = data.getStringExtra(EXTRA_MESSAGE);
				if (message != null && message.length() > 0){
					DialogFragment.createAlert(getString(R.string.trading_order_cancel_error), message, getActivity()).show();
				}
				if (fullReload){
					mOrdersAdapter.getDataFromServer(mQuery, true);
				}
			}			
		}
		if (requestCode == SearchView.RESULT_CODE)
			mSearchField.onActivityResult(requestCode, resultCode, data);
	}

//	/**
//	 * We override this method to spot when the fragment becomes visible
//	 */
//	@Override
//	public void setUserVisibleHint(boolean isVisibleToUser) {
//		super.setUserVisibleHint(isVisibleToUser);
//
//		if (!isVisibleToUser) {
//			// Becoming invisible
//		} else {
//			// Just became visible
//			if (mOrdersAdapter != null){
//				mOrdersAdapter.getDataFromServer(mQuery, false);
//			}
//		}
//	}

}
