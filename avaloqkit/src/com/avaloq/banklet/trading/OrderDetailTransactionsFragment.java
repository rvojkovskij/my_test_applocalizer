package com.avaloq.banklet.trading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.StexOrderDetailTO;
import com.avaloq.afs.server.bsp.client.ws.StexOrderQueryTO;
import com.avaloq.afs.server.bsp.client.ws.StexTradeTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.trading.StexOrdersRequest;
import com.avaloq.framework.comms.webservice.trading.TradingService;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.CurrencyUtil;
import com.avaloq.framework.util.DateUtil;
import com.avaloq.framework.util.InlineMessageUtil;

/**
 * The order detail transactions. <br />
 * This is implemented as a fragment so it can either be shown together with
 * {@link OrderDetailDataFragment} (tablets) or by itself as a navigation item
 * (phone).
 * 
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class OrderDetailTransactionsFragment extends BankletFragment {

	/**
	 * Takes an order detail id as an argument
	 */
	public static final String EXTRA_ORDER_DETAIL_ID = "extra_order_detail_id";
	public static final String EXTRA_ORDER_DETAIL = "extra_order_detail";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.trd_fragment_order_detail_transactions, container, false);
		final int orderDetailId = getArguments().getInt(EXTRA_ORDER_DETAIL_ID);
		final LinearLayout transactionsContainer = (LinearLayout) view.findViewById(R.id.trd_fragment_order_detail_transactions_container);
		
		// get the object form extras
		
		View transactionsHeader = getActivity().getLayoutInflater().inflate(R.layout.trd_order_detail_transactions_header, transactionsContainer, false);
		transactionsContainer.addView(transactionsHeader);
		StexOrderDetailTO orderDetail = (StexOrderDetailTO) getArguments().getSerializable(EXTRA_ORDER_DETAIL);
		showDetailItem(transactionsContainer, orderDetail);
		
		return view;
	}

	/**
	 * Display the transactions for an order detail or shows an error message
	 * when the item was not found
	 * 
	 * @param transactionsContainer
	 *            The container to show the error message in
	 * @param orderDetail
	 *            The order detail
	 */
	private void showDetailItem(LinearLayout transactionsContainer, StexOrderDetailTO orderDetail) {
		if (orderDetail != null) {
			if (orderDetail.getTradeList() != null) {
				for (StexTradeTO trade : orderDetail.getTradeList()) {
					View transactionsRow = getActivity().getLayoutInflater().inflate(R.layout.trd_order_detail_transactions_row, transactionsContainer, false);
					TextView textDateTime = (TextView) transactionsRow.findViewById(R.id.trd_order_detail_transactions_row_datetime);
					TextView textQuantity = (TextView) transactionsRow.findViewById(R.id.trd_order_detail_transactions_row_quantity);
					TextView textPrice = (TextView) transactionsRow.findViewById(R.id.trd_order_detail_transactions_row_price);
					textDateTime.setText(new DateUtil(getActivity()).format(trade.getTransactionTime()));
					textQuantity.setText(CurrencyUtil.formatNumber(trade.getQuantity()));
					textPrice.setText(CurrencyUtil.formatMoney(trade.getPrice()));
					transactionsContainer.addView(transactionsRow);
				}
			}
		} else {
			// TODO This is an example for the error handling:
			// we have to implement this everywhere else.
			InlineMessageUtil.display(transactionsContainer, R.string.trd_error);
		}
	}

}
