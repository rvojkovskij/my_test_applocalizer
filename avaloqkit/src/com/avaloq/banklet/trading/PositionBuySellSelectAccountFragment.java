package com.avaloq.banklet.trading;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class PositionBuySellSelectAccountFragment extends BankletFragment {
	
	PositionBuySellSelectType mType;
	PositionAccountRecord mData;
	boolean mOptionsAvailable;
	
	public enum PositionBuySellSelectType{
		ACCOUNT, PORTFOLIO
	}
	
	public interface PositionBuySellSelectAccountFragmentInterface{
		public void selectAccountItemClicked(PositionBuySellSelectType type);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.trd_position_buy_sell_fragment_select_account, container, false);

		// hide the view until we have the data
		fragmentView.findViewById(R.id.trading_buy_sell_llSelectAccount).setVisibility(View.GONE);
		
		fragmentView.findViewById(R.id.trading_buy_sell_llSelectAccount).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				if (mType != null && mOptionsAvailable) ((PositionBuySellSelectAccountFragmentInterface) getActivity()).selectAccountItemClicked(mType);
			}
		});
		
		return fragmentView;
	}

	public void setType(PositionBuySellSelectType type){
		mType = type;
	}
	
	public void setData(PositionAccountRecord data, boolean optionsAreAvailable){
		mData = data;
		mOptionsAvailable = optionsAreAvailable;
		showData(optionsAreAvailable);
	}
	
	private void showData(boolean optionsAreAvailable){
		
		if (mData == null){
			setEmpty(optionsAreAvailable);
			return;
		}
		
		((TextView)getView().findViewById(R.id.trading_buy_selectorName)).setText(mData.Name);
		((TextView)getView().findViewById(R.id.trading_buy_selectorDescription)).setText(mData.Description);
		((TextView)getView().findViewById(R.id.trading_buy_selectorAmount)).setText(mData.Amount);
		
		getView().findViewById(R.id.trading_buy_sell_llSelectAccount).setVisibility(View.VISIBLE);
	}
	
	private void setEmpty(boolean optionsAreAvailable){
		mOptionsAvailable = optionsAreAvailable;
		
		if (mType == PositionBuySellSelectType.ACCOUNT){
			if (optionsAreAvailable){
				((TextView)getView().findViewById(R.id.trading_buy_selectorName)).setText(R.string.trd_select_a_settlement_account);				
			}else{
				((TextView)getView().findViewById(R.id.trading_buy_selectorName)).setText(R.string.trd_none_available);
			}
		}
		if (mType == PositionBuySellSelectType.PORTFOLIO){
			if (optionsAreAvailable){
				((TextView)getView().findViewById(R.id.trading_buy_selectorName)).setText(R.string.trd_select_a_portfolio);
				
			}else{
				((TextView)getView().findViewById(R.id.trading_buy_selectorName)).setText(R.string.trd_no_portfolios_for_trading);
			}
		}
		((TextView)getView().findViewById(R.id.trading_buy_selectorDescription)).setText("");
		((TextView)getView().findViewById(R.id.trading_buy_selectorAmount)).setText("");
		getView().findViewById(R.id.trading_buy_sell_llSelectAccount).setVisibility(View.VISIBLE);
	}
}
