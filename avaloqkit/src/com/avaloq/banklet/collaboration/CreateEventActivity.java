package com.avaloq.banklet.collaboration;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.avaloq.banklet.collaboration.CreateBaseEventFragment.EventType;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;

public class CreateEventActivity extends BankletActivity{

	public static final String EXTRA_ADVISOR_NAME = "extra_advisor_name";
	public static final String EXTRA_EVENT_TYPE = "extra_event_type";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.col_create_event_activity);
		if (getSupportFragmentManager().findFragmentById(R.id.fragment_add_event) == null){
			FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
			
			CreateBaseEventFragment abef = null;
			View abef_view = findViewById(R.id.fragment_add_event);
			
			if (getIntent().getStringExtra(EXTRA_EVENT_TYPE).equals(EventType.PHONE_CALL.toString())){
				abef = new CreatePhoneCallEventFragment();
			}
			else if (getIntent().getStringExtra(EXTRA_EVENT_TYPE).equals(EventType.MESSAGE.toString())){
				abef = new CreateMessageEventFragment();
			}
			else if (getIntent().getStringExtra(EXTRA_EVENT_TYPE).equals(EventType.APPOINTMENT.toString())){
				abef = new CreateAppointmentEventFragment();
			}
			
			if (abef != null && abef_view != null){
				Bundle arguments = new Bundle();
				arguments.putBoolean(CreateBaseEventFragment.EXTRA_IS_DIALOG, false);
				abef.setArguments(arguments);
				tx.replace(R.id.fragment_add_event, abef);
			}
			
			tx.commit();
		}
	}
	
}
