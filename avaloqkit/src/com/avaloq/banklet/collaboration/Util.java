package com.avaloq.banklet.collaboration;

import java.util.Calendar;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Events;
import android.provider.CalendarContract.Instances;
import android.util.Log;

public class Util {

	public static boolean isCalendarFunctionalityAvailable() {
		if (Build.VERSION.SDK_INT < 14)
			return false;
		else
			return true;
	}

	@SuppressLint({ "NewApi", "InlinedApi" })
	public static void syncToCalendar(Activity activity, CrmIssueTO issue) {
		if (!isCalendarFunctionalityAvailable())
			return;
		Calendar beginTime = Calendar.getInstance();
		beginTime.setTime(issue.getAppointmentStartDate());
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(issue.getAppointmentEndDate());
		Intent intent = new Intent(Intent.ACTION_INSERT)
			.setData(Events.CONTENT_URI)
			.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
			.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
			.putExtra(Events.TITLE, issue.getSubject())
			.putExtra(Events.AVAILABILITY, Events.AVAILABILITY_BUSY);
		activity.startActivity(intent);
	}

	/**
	 * This function checks if the event has already been synchronized with the calendar.
	 * IMPORTANT!! Since this function requires the READ_CALENDAR permission, the customer
	 * wanted to remove it completely. Now this method always returns false.
	 * @param activity
	 * @param issue
	 * @return
	 */
	public static boolean eventExists(Activity activity, CrmIssueTO issue) {
		return false;
		/*if (!isCalendarFunctionalityAvailable() || issue.getAppointmentStartDate() == null || issue.getAppointmentEndDate() == null)
			return false;
		Calendar beginTime = Calendar.getInstance();
		beginTime.setTime(issue.getAppointmentStartDate());
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(issue.getAppointmentEndDate());

		long begin = beginTime.getTimeInMillis();
		long end = endTime.getTimeInMillis();
		String[] proj = new String[] { Instances._ID, Instances.EVENT_ID };
		Cursor cursor = Instances.query(activity.getContentResolver(), proj, begin, end,
				"\""+issue.getSubject()+"\"");
		if (cursor.getCount() > 0) {
			return true;
		}
		else
			return false;*/
	}
}
