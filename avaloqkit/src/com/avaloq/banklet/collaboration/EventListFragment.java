package com.avaloq.banklet.collaboration;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class EventListFragment extends BankletFragment{
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.col_main_portal_fragment, container, false);

		final ListView lv = (ListView)view.findViewById(R.id.event_list);
		final EditText search = (EditText)view.findViewById(R.id.search_edit_view);
		search.setOnTouchListener(new RightDrawableOnTouchListener(){
			@Override
			public boolean onDrawableTouch(MotionEvent event) {
				search.setText("");
				search.clearFocus();
				search.setSelected(false);
				AvaloqApplication.closeSoftKeyboard(getActivity());
				return true;
			}
		});
		search.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				List<CrmIssueTO> newIssues = new ArrayList<CrmIssueTO>();
				for (CrmIssueTO issue: Model.getInstance().getAllIssues()){
					if (issue.getSubject().toLowerCase().contains(s.toString().toLowerCase()))
						newIssues.add(issue);
				}
				lv.setAdapter(new EventListAdapter(getActivity(), R.id.title, newIssues));
				if (s.toString().length()>0){
					search.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.abs__ic_clear_holo_light, 0);
				}
				else
					search.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable s) {}
		});
				
		lv.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				onItemSelected(id);
			}
		});
				
		lv.setAdapter(new EventListAdapter(getActivity(), R.id.title, Model.getInstance().getAllIssues()));

		return view;
	}
	
	public void onItemSelected(long id){
		//close the soft keyboard if it is open
		AvaloqApplication.closeSoftKeyboard(getActivity());
		
		if (BankletActivityDelegate.isSmallDevice(getActivity())){
			Intent intent = new Intent(getActivity(), ConversationActivity.class);
			intent.putExtra(ConversationActivity.EXTRA_ISSUE_ID, id);
			getActivity().startActivity(intent);
		}
		else{
			FragmentTransaction tx = getActivity().getSupportFragmentManager().beginTransaction();
			ConversationFragment cf = new ConversationFragment();
			Bundle arguments = new Bundle();
			arguments.putLong(ConversationFragment.EXTRA_ISSUE_ID, id);
			cf.setArguments(arguments);
			tx.addToBackStack("conversation_to_overview");
			tx.replace(R.id.fragment_main, cf);
			tx.commit();
		}
		
	}
	
	public abstract class RightDrawableOnTouchListener implements OnTouchListener {
	    Drawable drawable;
	    private int fuzz = 10;

	    @Override
	    public boolean onTouch(final View v, final MotionEvent event) {
	    	TextView view = (TextView)v;
	    	final Drawable[] drawables = view.getCompoundDrawables();
	        if (drawables != null && drawables.length == 4)
	            this.drawable = drawables[2];
	        if (event.getAction() == MotionEvent.ACTION_DOWN && drawable != null) {
	            final int x = (int) event.getX();
	            final int y = (int) event.getY();
	            final Rect bounds = drawable.getBounds();
	            if (x >= (v.getRight() - bounds.width() - fuzz) && x <= (v.getRight() - v.getPaddingRight() + fuzz)
	                    && y >= (v.getPaddingTop() - fuzz) && y <= (v.getHeight() - v.getPaddingBottom()) + fuzz) {
	                return onDrawableTouch(event);
	            }
	        }
	        return false;
	    }

	    public abstract boolean onDrawableTouch(final MotionEvent event);
	}
}
