package com.avaloq.banklet.collaboration;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class EventListAdapter extends ArrayAdapter<CrmIssueTO> {

	List<CrmIssueTO> mItems;
	Context mContext;

	public EventListAdapter(Context context, int textViewResourceId, List<CrmIssueTO> aObjects) {
		super(context, R.layout.col_event_list_row, textViewResourceId, aObjects);
		mItems = aObjects;
		mContext = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = super.getView(position, convertView, parent);
		CrmIssueTO item = mItems.get(position);
		
		ImageView icon = (ImageView)row.findViewById(R.id.icon);
		int resId = getContext().getResources().getIdentifier("col_ico_"+item.getType().toString().toLowerCase(Locale.ENGLISH), "drawable", getContext().getPackageName());
		icon.setImageResource(resId);
		
		TextView title = (TextView)row.findViewById(R.id.title);
		title.setText(item.getSubject());
		
		
		if (item.getCommentList().size() > 0){
			TextView lastChange = (TextView)row.findViewById(R.id.lastChange);
			DateUtil util = new DateUtil(mContext);
			Date date = item.getCommentList().get(item.getCommentList().size() - 1).getTimestamp();
			lastChange.setText(mContext.getResources().getString(R.string.col_last_change)+" "+util.format(date)+" "+util.formatTime(date));
			
			TextView excerpt = (TextView)row.findViewById(R.id.excerpt);
			excerpt.setText(item.getCommentList().get(item.getCommentList().size() - 1).getComment());
		}
		
		// TODO no status anymore
		
//		TextView status = (TextView)row.findViewById(R.id.status);
//		resId = getContext().getResources().getIdentifier("col_event_status_"+item.getStatus().toString().toLowerCase(Locale.ENGLISH), "string", getContext().getPackageName());
//		status.setText(resId);
		
		return row;
	}
	
	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

}
