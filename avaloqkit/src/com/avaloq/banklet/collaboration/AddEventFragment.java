package com.avaloq.banklet.collaboration;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.avaloq.banklet.collaboration.CreateBaseEventFragment.EventType;
import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.ApiSpecificCalls;

public class AddEventFragment extends BankletFragment{
	
	int whole_width = 0;
	int initial_advisor_width = 0;
	int initial_add_contact_width = 0;
	int events_height = 0;
	
	@Override
	// There are several animation properties used in this class, which require API Level >= 11.
	// This check is made in the code via Build version codes, but a suppression of the warnings
	// is nevertheless necessary.
	@SuppressLint("NewApi")
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.col_add_event_fragment, container, false);
		if (!getActivity().getResources().getBoolean(R.bool.col_calendar_enabled)){
			View show_calendar = view.findViewById(R.id.button_show_calendar);
			if (show_calendar != null)
				show_calendar.setVisibility(View.GONE);
		}
		final View events = view.findViewById(R.id.event_types);
		final View call = view.findViewById(R.id.icon_call);
		final View message = view.findViewById(R.id.icon_message); 
		final View appointment = view.findViewById(R.id.icon_appointment);
		final View icon_plus = view.findViewById(R.id.button_add_cancel);
		final View container_add_cancel = view.findViewById(R.id.container_add_cancel);
		final ViewGroup advisor_container = (ViewGroup)view.findViewById(R.id.advisor_container);
		final View add_contact_container = view.findViewById(R.id.add_contact_container);
		
		if (events.getVisibility() != View.VISIBLE){
			events.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
				@Override
			    public void onGlobalLayout() {
					if (events.getHeight() > 0){
						events_height = events.getHeight();
						ApiSpecificCalls.removeOnGlobalLayoutListener(events.getViewTreeObserver(), this);
						events.getLayoutParams().height = 0;
						events.getLayoutParams().width = 0;
						events.requestLayout();
						events.setVisibility(View.VISIBLE);
					}
				}
			});
		}
		
		final Animation pushOut = new Animation()
	    {
			@Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	        	add_contact_container.getLayoutParams().width = (int)(initial_add_contact_width + initial_advisor_width * interpolatedTime);
	        	if (advisor_container != null)
	        		advisor_container.getLayoutParams().width = (int)(initial_advisor_width - initial_advisor_width * interpolatedTime);
	        	events.getLayoutParams().width = (int)(whole_width * interpolatedTime);
	        	events.getLayoutParams().height = (int)(events_height*interpolatedTime);
	        	
	        	if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
	        		events.setTranslationX(whole_width*(interpolatedTime-1));
		        	icon_plus.setRotation(45*interpolatedTime);
		        	icon_plus.requestLayout();
	        	}
	        	events.requestLayout();
	        	add_contact_container.requestLayout();
	        	if (advisor_container != null){
	        		advisor_container.requestLayout();
	        	}
	        }
			
			

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };
	    pushOut.setDuration(300);
	    
	    final Animation pushIn = new Animation()
	    {
	        @Override
	        protected void applyTransformation(float interpolatedTime, Transformation t) {
	        	add_contact_container.getLayoutParams().width = (int)(whole_width - initial_advisor_width * interpolatedTime);
	        	if (advisor_container != null)
	        		advisor_container.getLayoutParams().width = (int)(0 + initial_advisor_width * interpolatedTime);
	        	events.getLayoutParams().width = (int)(whole_width * (1-interpolatedTime));
	        	events.getLayoutParams().height = (int)(events_height*(1-interpolatedTime));
	        	if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
		        	events.setTranslationX(-whole_width*interpolatedTime);
		        	icon_plus.setRotation(45*(1-interpolatedTime));
		        	icon_plus.requestLayout();
	        	}
	        	events.requestLayout();
	        	add_contact_container.requestLayout();
	        	if (advisor_container != null)
	        		advisor_container.requestLayout();
	        }

	        @Override
	        public boolean willChangeBounds() {
	            return true;
	        }
	    };
	    pushIn.setDuration(300);

		
		
	    container_add_cancel.setOnClickListener(new OnClickListener(){
			private boolean expanded = false;
			
			@Override
			public void onClick(View v) {
				if (whole_width == 0){
	        		initial_add_contact_width = add_contact_container.getWidth();
	        		if (advisor_container != null)
	        			initial_advisor_width = advisor_container.getWidth();
	        		else
	        			initial_advisor_width = 0;
					whole_width = initial_advisor_width + initial_add_contact_width;
				}
				
				if (!expanded){
					add_contact_container.startAnimation(pushOut);
					expanded = true;
				}
				else {
					add_contact_container.startAnimation(pushIn);
					expanded = false;
				}
			}
		});
	    
	    if (getActivity().getResources().getBoolean(R.bool.col_phone_call_enabled)){
		    call.setOnClickListener(new OnClickListener(){
		    	@Override
				public void onClick(View v) {
		    		showAddEventDialog(EventType.PHONE_CALL);
		    	}
		    });
	    }
	    else
	    	call.setAlpha(0.4f);
	    
	    if (getActivity().getResources().getBoolean(R.bool.col_messaging_enabled)){
		    message.setOnClickListener(new OnClickListener(){
		    	@Override
				public void onClick(View v) {
		    		showAddEventDialog(EventType.MESSAGE);
		    	}
		    });
	    }
	    else
	    	message.setAlpha(0.4f);
	    
	    if (getActivity().getResources().getBoolean(R.bool.col_appointment_enabled)){
		    appointment.setOnClickListener(new OnClickListener(){
		    	@Override
				public void onClick(View v) {
		    		showAddEventDialog(EventType.APPOINTMENT);
		    	}
		    });
	    }
	    else
	    	appointment.setAlpha(0.4f);
		
		return view;
	}
	
	public void showAddEventDialog(EventType evntType){
		if (BankletActivityDelegate.isLargeDevice(getActivity())){
			SherlockDialogFragment df = null;
			if (evntType.equals(EventType.PHONE_CALL)){
				df = new CreatePhoneCallEventFragment();
			}
			else if (evntType.equals(EventType.MESSAGE)){
				df = new CreateMessageEventFragment();
			}
			else if (evntType.equals(EventType.APPOINTMENT)){
				df = new CreateAppointmentEventFragment();
			}
			if (df != null){
				Bundle arguments = new Bundle();
				arguments.putBoolean(CreateBaseEventFragment.EXTRA_IS_DIALOG, true);
				df.setArguments(arguments);
				df.show(getActivity().getSupportFragmentManager(), "createEvent");
			}
			
		}
		else{
			Intent intent = new Intent(getActivity(), CreateEventActivity.class);
			intent.putExtra(CreateEventActivity.EXTRA_EVENT_TYPE, evntType.toString());
			//intent.putExtra(AddEventActivity.EXTRA_ADVISOR_NAME, "");
			getActivity().startActivity(intent);
		}
	}
}
