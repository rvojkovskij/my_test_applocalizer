package com.avaloq.banklet.collaboration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.util.Log;

import com.avaloq.afs.server.bsp.client.ws.BusinessPartnerTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueCommentTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueQueryTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.collaboration.CollaborationService;
import com.avaloq.framework.comms.webservice.collaboration.CrmIssueRequest;
import com.avaloq.framework.comms.webservice.collaboration.model.CrmIssueResult;
import com.avaloq.framework.util.DateUtil;


public class Model extends java.util.Observable {
	private static Model instance = null;
	
	CrmIssueResult result;
	List<BusinessPartnerTO> mBusinessPartners;

	public static Model getInstance() {
		if(instance == null) {
			instance = new Model();
		}
		return instance;
	}
	
	public void loadData(boolean allowFetchFromCache){
		
		CrmIssueQueryTO query = new CrmIssueQueryTO();
		
		// TODO check what parameters have to be set for the query
		// FIXME query parameters are not set
		
		CrmIssueRequest request = CollaborationService.getCrmIssuesWithQuery(query, 0L, 100L, new RequestStateEvent<CrmIssueRequest>() {
			@Override
			public void onRequestCompleted(CrmIssueRequest aRequest) {
				result = sortMessages(aRequest.getResponse().getData());
				result = sortConversations(result);
				Model.this.setChanged();
				Model.this.notifyObservers(result);
			}
			
			@Override
			public void onRequestFailed(CrmIssueRequest aRequest) {
				Model.this.setChanged();
				Model.this.notifyObservers(null);
			}
		});
		if (!allowFetchFromCache)
			request.setCachePolicy(CachePolicy.REFRESH_CACHE);
		request.initiateServerRequest();
	}
	
	public void loadData(){
		loadData(true);
	}
	
	public List<CrmIssueTO> getAllIssues(){
		return result.getIssueList();
	}
	
	public CrmIssueTO getIssue(long id){	
		for (CrmIssueTO issue: getAllIssues()){
			if (issue.getId().equals(id))
				return issue;
		}
		return null;
	}
	
	public List<CrmIssueTO> getEventsByDay(int day, int month, int year){
		List<CrmIssueTO> list = new ArrayList<CrmIssueTO>();
		Calendar calStart = Calendar.getInstance();
		calStart.set(year, month-1, day, 0, 0);
		Calendar calEnd = Calendar.getInstance();
		calEnd.set(year, month-1, day, 23, 59);
		Date todayStart = calStart.getTime();
		Date todayEnd = calEnd.getTime();
		
		for (CrmIssueTO issue: getAllIssues()){
			if (issue.getAppointmentStartDate() == null)
				continue;
			Date dateStart = DateUtil.getCalendar(issue.getAppointmentStartDate()).getTime();
			Date dateEnd = dateStart;
			if (issue.getAppointmentEndDate() != null)
				dateEnd = DateUtil.getCalendar(issue.getAppointmentEndDate()).getTime();
			if (
					//(date.get(Calendar.DAY_OF_MONTH) == day && date.get(Calendar.MONTH)+1 == month && date.get(Calendar.YEAR) == year)
					(dateStart.after(todayStart) && dateStart.before(todayEnd)) ||
					(dateEnd.after(todayStart) && dateEnd.before(todayEnd))     ||
					(dateStart.before(todayStart) && dateEnd.after(todayEnd))
				)
				list.add(issue);
		}
		
		if (list.size() > 0)
			Log.d("Test", "Size of list for "+day+"."+month+"."+year+": "+list.size());
		
		return list;
	}
	
	public List<CrmIssueTO> getEventsByDay(Date date){
		Calendar cal = DateUtil.getCalendar(date);
		return getEventsByDay(cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH)+1, cal.get(Calendar.YEAR));
	}

	public void setBusinessPartners(List<BusinessPartnerTO> businessPartners) {
		mBusinessPartners = businessPartners;
	}
	
	public List<BusinessPartnerTO> getBusinessPartners(){
		return mBusinessPartners;
	}
	
	public boolean isManager(){
		return (mBusinessPartners != null && mBusinessPartners.size() > 1);
	}
	
	public CrmIssueResult sortMessages(CrmIssueResult result){
		for (CrmIssueTO issue: result.getIssueList()){
			int size = issue.getCommentList().size();
			for (int i=0; i<size-1; i++){
				for (int j=i+1; j<size; j++){
					if (issue.getCommentList().get(i).getTimestamp().after(issue.getCommentList().get(j).getTimestamp())){
						CrmIssueCommentTO tmp = issue.getCommentList().get(i);
						issue.getCommentList().set(i, issue.getCommentList().get(j));
						issue.getCommentList().set(j, tmp);
					}
				}
			}
		}
		return result;
	}
	
	public CrmIssueResult sortConversations(CrmIssueResult result){
		int size = result.getIssueList().size();
		for (int i=0; i<size-1; i++){
			for (int j=i+1; j<size; j++){
				int indexi = result.getIssueList().get(i).getCommentList().size()-1;
				int indexj = result.getIssueList().get(j).getCommentList().size()-1;
				
				if (indexi >= 0) {
					if (indexj < 0)
						swap(i, j);
					else if (result.getIssueList().get(i).getCommentList().get(indexi).getTimestamp().before(result.getIssueList().get(j).getCommentList().get(indexj).getTimestamp())){
						swap(i, j);
					}
				}
			}
		}
		return result;
	}
	
	private void swap(int i, int j){
		CrmIssueTO tmp = result.getIssueList().get(i);
		result.getIssueList().set(i, result.getIssueList().get(j));
		result.getIssueList().set(j, tmp);
	}
}
