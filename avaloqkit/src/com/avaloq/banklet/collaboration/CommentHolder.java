package com.avaloq.banklet.collaboration;

import java.util.Date;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class CommentHolder {
	
	ViewGroup mParent;
	
	public CommentHolder(ViewGroup aParent){
		mParent = aParent;
	}
	
	public void addComment(String aText, Date aDate, boolean aOwnedByUser, final Runnable callback){
		LayoutInflater inflater = LayoutInflater.from(mParent.getContext());
		View view = inflater.inflate(R.layout.col_comment, mParent, false);
		TextView userText = (TextView)view.findViewById(R.id.comment_text_right);
		TextView advisorText = (TextView)view.findViewById(R.id.comment_text_left);
		TextView date = (TextView)view.findViewById(R.id.comment_date);
		
		if (aOwnedByUser){
			advisorText.setVisibility(View.GONE);
			userText.setText(aText);
		}
		else{
			userText.setVisibility(View.GONE);
			advisorText.setText(aText);
		}
		date.setText(DateUtil.formatLong(aDate));
		
		if (callback != null){
			view.post(new Runnable() {
				
				@Override
				public void run() {
					callback.run();
				}
			});
		}
		
		mParent.addView(view);
	}
	
	public void empty(){
		mParent.removeAllViews();
	}
	
	public void addComment(String aText, Date aDate, boolean aOwnedByUser){
		addComment(aText, aDate, aOwnedByUser, null);
	}
}
