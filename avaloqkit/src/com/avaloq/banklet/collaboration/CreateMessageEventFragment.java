package com.avaloq.banklet.collaboration;

import java.util.Calendar;
import java.util.Date;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueType;
import com.avaloq.framework.R;

public class CreateMessageEventFragment extends CreateBaseEventFragment{
	public CreateMessageEventFragment(){
		super();
		eventType = EventType.MESSAGE;
	}
	
	@Override
	public boolean hasExtendedTime() {
		return false;
	}

	@Override
	public int getNoticeMessageId() {
		return R.string.col_message_default_message;
	}

	@Override
	public int getSaveButtonId() {
		return R.string.col_event_message_save;
	}

	@Override
	public CrmIssueTO onValidationSuccess() {
		CrmIssueTO issue = new CrmIssueTO();
		Date dateFrom, dateTo;
		dateFrom = new Date();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, END_OF_WORK_HOUR);
		cal.set(Calendar.MINUTE, END_OF_WORK_MINUTE);
		dateTo = cal.getTime();

		// These fields are set statically in the iOS code
		issue.setType(CrmIssueType.MESSAGING);
		issue.setOpenDate(new Date());
		if (partnerId != 0)
			issue.setBusinessPartnerId(partnerId);
		
		issue.setDueDate(dateTo);
		issue.setSubject(subject.getText().toString());
		issue.setComment(message.getText().toString());
		issue.setAppointmentStartDate(dateFrom);
		issue.setAppointmentEndDate(dateTo);
		
		return issue;
	}

	@Override
	public int getDialogTitleId() {
		return R.string.col_dialog_title_message;
	}
}
