package com.avaloq.banklet.collaboration;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.collaboration.CollaborationService;
import com.avaloq.framework.comms.webservice.collaboration.CrmIssueRequest;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.DateUtil;

public abstract class CreateBaseEventFragment extends SherlockDialogFragment{	
	public enum EventType{
        PHONE_CALL, APPOINTMENT, MESSAGE;
        
        @Override
        public String toString(){
        	switch (this){
			case APPOINTMENT:
				return "APPOINTMENT";
			case MESSAGE:
				return "MESSAGING";
			case PHONE_CALL:
				return "CALL_REQUEST";
			default:
				return "";
        	}
        }
    };
    
    public static final int END_OF_WORK_HOUR = 18;
    public static final int END_OF_WORK_MINUTE = 0;
    
    private static final int RESULT_CODE = 2;
    
	protected EventType eventType;
	protected TextView view_date_from, view_date_to, view_time_from, view_time_to, subject, message, partner;
	protected Button save;
	
	public abstract int getNoticeMessageId();
	public abstract int getSaveButtonId();
	public abstract int getDialogTitleId();
	public abstract CrmIssueTO onValidationSuccess();
	
	public static final String EXTRA_IS_DIALOG = "extra_is_dialog";
	private boolean isDialog = false;
	long partnerId;
	
	/**
	 * Determines if the event has to display date and time data. Default is true,
	 * it could be overridden in the specific implementation of the class.
	 */
	public boolean hasExtendedTime(){
		return true;
	}
	
	/**
	 * Validates the form. Returns the resource id of an error string.
	 * In case the form validates successfully, returns 0.
	 * @return
	 */
	public int validate(){
		if (subject.getText().toString().trim().equals(""))
			return R.string.col_validation_error_message;
		if (message.getText().toString().trim().equals(""))
			return R.string.col_validation_error_message_description;
		if (hasExtendedTime()){
			try{
				DateUtil dateUtil = new DateUtil(getActivity());
				Date dateFrom = dateUtil.parseDateTime((String)view_date_from.getText(), (String)view_time_from.getText());
				Date dateTo = dateUtil.parseDateTime((String)view_date_to.getText(), (String)view_time_to.getText());
				if (!dateTo.after(dateFrom))
					return R.string.col_validation_error_message_datetime;
			}
			catch (Exception e){
				return R.string.col_validation_error_message_datetime;
			}
		}
		return 0;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		try {
			getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		}
		catch (NullPointerException e){
			// It means we are currently in an Activity, not Dialog. No further action needed.
		}
		
	}
	
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.col_create_event_fragment, container, false);
		isDialog = getArguments().getBoolean(EXTRA_IS_DIALOG);
		
		if (getDialog() != null)
			getDialog().setTitle(getActivity().getString(getDialogTitleId()));
		
		subject = (TextView)view.findViewById(R.id.subject);
		partner = (TextView)view.findViewById(R.id.business_partner);
		message = (TextView)view.findViewById(R.id.message);
		message.setText(getNoticeMessageId());
		save = (Button)view.findViewById(R.id.event_save);
		save.setText(getSaveButtonId());
		
		if (hasExtendedTime()){
			view_date_from = (TextView)view.findViewById(R.id.date_from);
			view_date_to = (TextView)view.findViewById(R.id.date_to);
			view_time_from = (TextView)view.findViewById(R.id.time_from);
			view_time_to = (TextView)view.findViewById(R.id.time_to);

			view_date_from.setOnClickListener(new EventDateListenerFactory(view_date_from));
			view_date_to.setOnClickListener(new EventDateListenerFactory(view_date_to));
			view_time_from.setOnClickListener(new EventTimeListenerFactory(view_time_from));
			view_time_to.setOnClickListener(new EventTimeListenerFactory(view_time_to, true));
		}
		else {
			ViewGroup vg = (ViewGroup)view.findViewById(R.id.extended_date_info);
			vg.setVisibility(View.GONE);
		}
		
		if (isDialog){
			// TODO: Maybe add a cancel button here? Or not?
		}
		
		save.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				final OnClickListener listener = this;
				int validationErrorId = validate();
				if (validationErrorId == 0){
					save.setOnClickListener(null);
					CrmIssueTO issue = onValidationSuccess();
					if (issue == null){
						showAlert(R.string.col_validation_error_title, R.string.col_validation_error_message);
						return;
					}
					CrmIssueRequest request = CollaborationService.createCrmIssue(issue, new RequestStateEvent<CrmIssueRequest>(){
						@Override
						public void onRequestCompleted(CrmIssueRequest aRequest) {
							
							Observer observer = new Observer(){
								@Override
								public void update(Observable observable, Object data) {
									if (isDialog){
										CreateBaseEventFragment.this.dismiss();
									}
									Intent intent = new Intent(getActivity(), PortalActivity.class);
									getActivity().startActivity(intent);
									Model.getInstance().deleteObserver(this);
								}
							};
							
							Model.getInstance().addObserver(observer);
							Model.getInstance().loadData(false);
						}
						
						@Override
						public void onRequestFailed(CrmIssueRequest aRequest) {
							DialogFragment.showNetworkError();
							save.setOnClickListener(listener);
						}
					});
					request.setCachePolicy(CachePolicy.NO_CACHE);
					request.initiateServerRequest();
				}
				else{
					showAlert(R.string.col_validation_error_title, validationErrorId);
				}
			}
		});
		
		if (Model.getInstance().isManager()){
			partner.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity(), BusinessPartnerActivity.class);
					if (partnerId != 0)
						intent.putExtra(BusinessPartnerActivity.EXTRA_BUSINESS_PARTNER_ID, partnerId);
					startActivityForResult(intent, RESULT_CODE);
				}
			});
		}
		else{
			view.findViewById(R.id.business_partner_label).setVisibility(View.GONE);
			partner.setVisibility(View.GONE);
		}

		return view;
	}
	
	
	
	public class EventDateListenerFactory implements OnDateSetListener, OnClickListener{
		TextView mTarget;
		public EventDateListenerFactory(TextView aTarget){
			mTarget = aTarget;
			Calendar cal = Calendar.getInstance();
			onDateSet(null, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
		}
		
		@Override
		public void onClick(View v) {
			Calendar cal = Calendar.getInstance();
			DateUtil dateUtil = new DateUtil(getActivity());
			try{
				Date date = dateUtil.parse((String)mTarget.getText());
				cal.setTime(date);
			}
			catch (Exception e){
				// Nothing to do here. If the date can't be parsed, today's date is
				// taken by default from the Calendar class.
			}
			Dialog dialog = new DatePickerDialog(getActivity(),
					this, cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
			DialogFragment df = new DialogFragment();
			df.setDialog(dialog);
			df.show();
		}
		
		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
			Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
			DateUtil dateUtil = new DateUtil(getActivity());
			mTarget.setText(dateUtil.format(cal.getTime()));
		}
	}
	
	public class EventTimeListenerFactory implements OnTimeSetListener, OnClickListener{
		TextView mTarget;
		public EventTimeListenerFactory(TextView aTarget){
			mTarget = aTarget;
			Calendar cal = Calendar.getInstance();
			onTimeSet(null, Calendar.HOUR_OF_DAY, cal.get(Calendar.MINUTE));
		}
		
		public EventTimeListenerFactory(TextView aTarget, boolean useEndOfWorkHours){
			this(aTarget);
			if (useEndOfWorkHours)
				onTimeSet(null, END_OF_WORK_HOUR, END_OF_WORK_MINUTE);
		}
		
		@Override
		public void onClick(View v) {
			Calendar cal = Calendar.getInstance();
			DateUtil dateUtil = new DateUtil(getActivity());
			try{
				Date date = dateUtil.parseTime((String)mTarget.getText());
				cal.setTime(date);
			}
			catch (Exception e){
				// Nothing to do here. If the date can't be parsed, today's date is
				// taken by default from the Calendar class.
			}
			Dialog dialog = new TimePickerDialog(getActivity(), this, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);
			DialogFragment df = new DialogFragment();
			df.setDialog(dialog);
			df.show();
		}

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			DateUtil dateUtil = new DateUtil(getActivity());
			Calendar cal = Calendar.getInstance();
			cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), hourOfDay, minute, 00);
			mTarget.setText(dateUtil.formatTime(cal.getTime()));
		}
	}
	
	private void showAlert(int title, int body){
		DialogFragment.createAlert(
			getActivity().getResources().getString(title), 
			getActivity().getResources().getString(body)
		).show();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == RESULT_CODE) {
			if(resultCode == Activity.RESULT_OK){
				if (data.hasExtra(BusinessPartnerActivity.EXTRA_BUSINESS_PARTNER_ID)){
					partnerId = data.getLongExtra(BusinessPartnerActivity.EXTRA_BUSINESS_PARTNER_ID, 0l);
					String partnerName = data.getStringExtra(BusinessPartnerActivity.EXTRA_BUSINESS_PARTNER_NAME);
					displayBusinessPartner(partnerId, partnerName);
				}
			}
		}
	}
	
	private void displayBusinessPartner(long id, String name){
		partner.setText(name);
	}
	
	
	
}
