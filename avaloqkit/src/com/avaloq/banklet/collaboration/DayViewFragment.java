package com.avaloq.banklet.collaboration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.util.DateUtil;

public class DayViewFragment extends BankletFragment{
	
	public static final String EXTRA_TIMESTAMP = "extra_date";
	private List<List<CrmIssueTO>> lines = new ArrayList<List<CrmIssueTO>>();
	private int hourHeight = 0;
	private Date todayStart;
	private Date todayEnd;
	private Date displayStart;
	private Date displayEnd;
	
	// At what time should events be displayed, which have started on a previous day
	public static final int START_HOUR_FOR_CONTINUOUS_EVENTS = 0;
	public static final int START_MINUTE_FOR_CONTINUOUS_EVENTS = 0;
	
	// Until what time should events be displayed, which proceed in the next day
	public static final int END_HOUR_FOR_CONTINUOUS_EVENTS = 23;
	public static final int END_MINUTE_FOR_CONTINUOUS_EVENTS = 59;
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		long timestamp =  getArguments().getLong(EXTRA_TIMESTAMP, 0);
		if (timestamp == 0)
			throw new IllegalArgumentException("The argument EXTRA_TIMESTAMP must be set.");
		prepareTodayTimeVars(timestamp);
		
		List<CrmIssueTO> events = Model.getInstance().getEventsByDay(new Date(timestamp));
		Log.d("Test", "minutes "+events.size());
		
		final View view = inflater.inflate(R.layout.col_day_view_fragment, container, false);
		ViewGroup template = (ViewGroup)view.findViewById(R.id.dayview_template);
		ViewGroup hoursVG = (ViewGroup)template.findViewById(R.id.hours);
		ViewGroup eventContainer = (ViewGroup)template.findViewById(R.id.event_lines);
		hourHeight = (int)getActivity().getResources().getDimension(R.dimen.dayview_hour_height);
		
		for (int i=0; i<24; i++){
			TextView hour = new TextView(getActivity());
			hour.setBackgroundResource(R.drawable.avq_table_divider_header);
			// According to the documentation, IDs larger than 0x00FFFFFF are reserved 
			// for views inside the res-xml files. So setting the ID to the hour is safe in this case.
			hour.setId(i);
			hour.setText((i<10)?"0"+i:""+i);
			hour.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, hourHeight));
			hoursVG.addView(hour);
		}
		
		
		/*
		// Creating some dummy data in order to test the algorithm thoroughly
		// TODO: remove after the algorithm is done
		// BEGIN HERE
		List<CrmIssueTO> events = new ArrayList<CrmIssueTO>();
		CrmIssueTO issue = new CrmIssueTO();
		issue.setAppointmentStartDate(new Date(2013-1900, 8, 12, 9, 15, 0));
		issue.setAppointmentEndDate(new Date(2013-1900, 8, 12, 12, 0, 0));
		issue.setSubject("Event 1");
		events.add(issue);
		issue = new CrmIssueTO();
		issue.setAppointmentStartDate(new Date(2013-1900, 8, 12, 10, 30, 0));
		issue.setAppointmentEndDate(new Date(2013-1900, 8, 12, 15, 0, 0));
		issue.setSubject("Event 2");
		events.add(issue);
		issue = new CrmIssueTO();
		issue.setAppointmentStartDate(new Date(2013-1900, 8, 12, 11, 0, 0));
		issue.setAppointmentEndDate(new Date(2013-1900, 8, 12, 15, 0, 0));
		issue.setSubject("Event 3");
		events.add(issue);
		issue = new CrmIssueTO();
		issue.setAppointmentStartDate(new Date(2013-1900, 8, 12, 13, 0, 0));
		issue.setAppointmentEndDate(new Date(2013-1900, 8, 12, 16, 0, 0));
		issue.setSubject("Event 4");
		events.add(issue);
		issue = new CrmIssueTO();
		issue.setAppointmentStartDate(new Date(2013-1900, 8, 12, 17, 0, 0));
		issue.setAppointmentEndDate(new Date(2013-1900, 8, 12, 19, 0, 0));
		issue.setSubject("Event 5");
		events.add(issue);
		// END HERE
		*/
		
		
		
		eventLoop:
		for (CrmIssueTO event: events){
			Log.d("Test", "Line Processing event "+lines.size());
			if (event.getAppointmentEndDate() == null || event.getAppointmentStartDate() == null)
				continue;
			for (int lineNumber=0; lineNumber<lines.size(); lineNumber++){
				List<CrmIssueTO> line = lines.get(lineNumber);
				// In case the line is avq_activity_empty, add the event to it
				if (line.size()==0){
					addEventToLine(event, lineNumber);
					continue eventLoop;
				}
				// otherwise, if there are no overlappings with the events in 
				// the current line, add it
				if (!eventOverlapsList(event, line)){
					addEventToLine(event, lineNumber);
					continue eventLoop;
				}
			}
			// if it is the last line, and there are no more lines available
			// then create a new line for the event
			addEventToLine(event, lines.size());
		}

		displayEvents(inflater, eventContainer);

		return view;
	}
	
	private void addEventToLine(CrmIssueTO issue, int lineNumber){
		Log.d("Test", "Adding event to Line "+lineNumber);
		if (lines.size() <= lineNumber){
			List<CrmIssueTO> list = new ArrayList<CrmIssueTO>();
			list.add(issue);
			lines.add(list);
		}
		else{
			lines.get(lineNumber).add(issue);
		}
	}
	
	private boolean eventOverlapsList(CrmIssueTO event, List<CrmIssueTO> list){
		Date eventStart = event.getAppointmentStartDate();
		if (eventStart.before(todayStart))
			eventStart = displayStart;
		Date eventEnd = event.getAppointmentEndDate();
		if (eventEnd.after(todayEnd))
			eventEnd = displayEnd;
		for (CrmIssueTO existing: list){
			Date existingStart = existing.getAppointmentStartDate();
			Date existingEnd = existing.getAppointmentEndDate();
			if ( (eventStart.after(existingStart) && eventStart.before(existingEnd)) 
					|| (eventEnd.after(existingStart) && eventEnd.before(existingEnd)) ){
				return true;
			}
		}
		return false;
	}
	
	private void displayEvents(LayoutInflater inflater, ViewGroup view){
		for (List<CrmIssueTO> list: lines){
			ViewGroup lineView = (ViewGroup)inflater.inflate(R.layout.col_day_view_line, view, false);
			int currentMargin = 0;
			
			for (CrmIssueTO issue: list){
				Date eventStart = issue.getAppointmentStartDate();
				if (eventStart.before(todayStart))
					eventStart = displayStart;
				Date eventEnd = issue.getAppointmentEndDate();
				if (eventEnd.after(todayEnd))
					eventEnd = displayEnd;
				double hours = (double)Minutes.minutesBetween(new DateTime(eventStart), new DateTime(eventEnd)).getMinutes() / 60;
				double topMargin = DateUtil.getCalendar(eventStart).get(Calendar.HOUR_OF_DAY) + ((double)DateUtil.getCalendar(eventStart).get(Calendar.MINUTE)/60);
				int eventHeightDp = (int)(hours*hourHeight);
				int topMarginDp = (int)(topMargin*hourHeight);
				
				TextView eventView = (TextView)inflater.inflate(R.layout.col_day_view_event, lineView, false);
				LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, eventHeightDp);
				int paramMargin = topMarginDp-currentMargin;
				params.setMargins(0, paramMargin, 0, 0);
				currentMargin = topMarginDp + eventHeightDp;
				Log.d("Test", "Minutes margin: "+currentMargin+" set to "+paramMargin+" "+eventHeightDp);
				final long currentId = issue.getId();
				
				eventView.setOnClickListener(new OnClickListener(){
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(getActivity(), ConversationActivity.class);
						intent.putExtra(ConversationActivity.EXTRA_ISSUE_ID, currentId);
						getActivity().startActivity(intent);
					}
				});
				
				eventView.setLayoutParams(params);
				eventView.setText(issue.getSubject());
				lineView.addView(eventView);
			}
			
			view.addView(lineView);
		}
	}
	
	private void prepareTodayTimeVars(long timestamp){
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(new Date(timestamp));
		calStart.set(Calendar.HOUR_OF_DAY, 0);
		calStart.set(Calendar.MINUTE, 0);
		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(new Date(timestamp));
		calEnd.set(Calendar.HOUR_OF_DAY, 23);
		calEnd.set(Calendar.MINUTE, 59);
		
		todayStart = calStart.getTime();
		todayEnd = calEnd.getTime();
		
		Calendar cal = DateUtil.getCalendar(todayStart);
		cal.set(Calendar.HOUR_OF_DAY, START_HOUR_FOR_CONTINUOUS_EVENTS);
		cal.set(Calendar.MINUTE, START_MINUTE_FOR_CONTINUOUS_EVENTS);
		displayStart = cal.getTime();
		cal.set(Calendar.HOUR_OF_DAY, END_HOUR_FOR_CONTINUOUS_EVENTS);
		cal.set(Calendar.MINUTE, END_MINUTE_FOR_CONTINUOUS_EVENTS);
		displayEnd = cal.getTime();
		
	}
}
