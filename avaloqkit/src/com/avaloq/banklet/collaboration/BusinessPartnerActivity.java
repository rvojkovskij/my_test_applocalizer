package com.avaloq.banklet.collaboration;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.BusinessPartnerTO;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.collaboration.BusinessPartnersRequest;
import com.avaloq.framework.comms.webservice.collaboration.CollaborationService;
import com.avaloq.framework.ui.BankletActivity;

public class BusinessPartnerActivity extends BankletActivity{
	
	public final static String EXTRA_BUSINESS_PARTNER_NAME = "extra_partner_name";
	public final static String EXTRA_BUSINESS_PARTNER_ID = "extra_partner_id";
	private long partnerId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.col_business_partner);
		setTitle(R.string.business_partner_title);
		partnerId = getIntent().getLongExtra(EXTRA_BUSINESS_PARTNER_ID, 0);
		
		final ListView list = (ListView)findViewById(R.id.business_partner_list);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
				BusinessPartnerTO partner = ((BusinessPartnerAdapter)list.getAdapter()).getItem(position);
				Intent intent = new Intent();
				intent.putExtra(EXTRA_BUSINESS_PARTNER_NAME, partner.getName());
				intent.putExtra(EXTRA_BUSINESS_PARTNER_ID, partner.getId());
				BusinessPartnerActivity.this.setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});
		CollaborationService.getBusinessPartners(new RequestStateEvent<BusinessPartnersRequest>() {
			@Override
			public void onRequestCompleted(BusinessPartnersRequest aRequest) {
				list.setAdapter(new BusinessPartnerAdapter(BusinessPartnerActivity.this, aRequest.getResponse().getData().getBusinessPartners()));
			}
		}).initiateServerRequest();
	}
	
	public class BusinessPartnerAdapter extends ArrayAdapter<BusinessPartnerTO>{

		public BusinessPartnerAdapter(Context context, List<BusinessPartnerTO> objects) {
			super(context, R.layout.col_business_partner_row, R.id.business_partner_name, objects);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = LayoutInflater.from(getContext()).inflate(R.layout.col_business_partner_row, null, false);
			BusinessPartnerTO partner = getItem(position);
			
			TextView name = (TextView)view.findViewById(R.id.business_partner_name);
			View selected = view.findViewById(R.id.business_partner_selected);
			
			name.setText(partner.getName());
			if (partner.getId() == partnerId){
				selected.setVisibility(View.VISIBLE);
			}
			
			return view;
		}
		
	}
}
