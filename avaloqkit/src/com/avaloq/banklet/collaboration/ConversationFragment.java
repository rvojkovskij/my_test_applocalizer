package com.avaloq.banklet.collaboration;

import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueCommentTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueDescriptionActionType;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.banklet.collaboration.CreateBaseEventFragment.EventType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.AbstractServerRequest.CachePolicy;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.collaboration.CollaborationService;
import com.avaloq.framework.comms.webservice.collaboration.CrmIssueRequest;
import com.avaloq.framework.comms.webservice.collaboration.Request;
import com.avaloq.framework.tools.RefreshSpinner;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.ui.DialogFragment;
import com.avaloq.framework.util.DateUtil;

public class ConversationFragment extends BankletFragment implements Observer {
	
	public static final String EXTRA_ISSUE_ID = "extra_issue_id";
	private long issueId;
	
	CrmIssueTO issue;
	CommentHolder ch;
	
	EventType mEventType = EventType.PHONE_CALL;
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		issueId =  getArguments().getLong(EXTRA_ISSUE_ID, 0);
		if (issueId == 0)
			throw new IllegalArgumentException("The argument EXTRA_ISSUE_ID must be set.");
		
		issue = Model.getInstance().getIssue(issueId);

		final View view = inflater.inflate(R.layout.col_conversation_fragment, container, false);
		ImageView icon = (ImageView)view.findViewById(R.id.icon);
		TextView title = (TextView)view.findViewById(R.id.title);
		TextView formatted_day_from = (TextView)view.findViewById(R.id.formatted_day_from);
		TextView formatted_day_to = (TextView)view.findViewById(R.id.formatted_day_to);
		View sync_container = view.findViewById(R.id.sync_container);
		View refresh_conteiner = view.findViewById(R.id.refresh_container);
		final RefreshSpinner spinner = (RefreshSpinner)view.findViewById(R.id.refresh_conversation);
		final View delete_container = view.findViewById(R.id.delete_container);
		
		delete_container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(getActivity()).
						setMessage(getActivity().getString(R.string.col_warning_delete_message)).
						setTitle(getActivity().getString(R.string.col_warning_delete_title))
						.setNegativeButton(R.string.col_no, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						})
						.setPositiveButton(R.string.col_yes, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, int which) {
								CollaborationService.closeCrmIssues(issueId, new RequestStateEvent<Request>(){
									@Override
									public void onRequestCompleted(Request aRequest) {
										Intent intent = new Intent(getActivity(), PortalActivity.class);
										intent.putExtra(PortalActivity.EXTRA_FORCE_REFRESH, true);
										getActivity().startActivity(intent);
									}
									
									@Override
									public void onRequestFailed(Request aRequest) {
										dialog.dismiss();
										// TODO Show error message
									}
								}).initiateServerRequest();
								
							}
						})
						.create().show();
			}
		});
		
		sync_container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Util.syncToCalendar(getActivity(), issue);
			}
		});
		
		
		refresh_conteiner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!spinner.isRefreshing()){
					spinner.start();
					Model.getInstance().addObserver(ConversationFragment.this);
					Model.getInstance().loadData(false);
				}
			}
		});

		final ScrollView sv = (ScrollView)view.findViewById(R.id.conversation_scroll);
		
		int resId = getActivity().getResources().getIdentifier("col_ico_"+issue.getType().toString().toLowerCase(Locale.ENGLISH), "drawable", getActivity().getPackageName());
		icon.setImageResource(resId);
		
		title.setText(issue.getSubject());
		
		// TODO no status anymore
		
//		resId = getActivity().getResources().getIdentifier("col_event_status_"+issue.getStatus().toString().toLowerCase(Locale.ENGLISH), "string", getActivity().getPackageName());
//		status.setText(resId);
		
		DateUtil dateUtil = new DateUtil(getActivity());
		if (issue.getAppointmentStartDate() != null && issue.getAppointmentEndDate() != null){
			formatted_day_from.setText(dateUtil.formatDateTime(issue.getAppointmentStartDate()));
			formatted_day_to.setText(dateUtil.formatDateTime(issue.getAppointmentEndDate()));
		}
		else {
			view.findViewById(R.id.calendar_functions).setVisibility(View.GONE);
			sync_container.setVisibility(View.GONE);
		}
		
		if (!Util.isCalendarFunctionalityAvailable() || Util.eventExists(getActivity(), issue)){
			sync_container.setVisibility(View.GONE);
		}
		
		ch = new CommentHolder((ViewGroup)view.findViewById(R.id.conversation_messages));
		refreshConversation();
		
		Button send = (Button)view.findViewById(R.id.button_new_message);
		final TextView message = (TextView)view.findViewById(R.id.text_new_message);
		send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AvaloqApplication.closeSoftKeyboard(getActivity());
				
				final String messageText = message.getText().toString().trim();
				message.setText("");
				if (messageText.equals(""))
					return;
				
				CrmIssueRequest request = CollaborationService.addCommentToCrmIssue(issueId, messageText, new RequestStateEvent<CrmIssueRequest>(){
					@Override
					public void onRequestCompleted(CrmIssueRequest aRequest) {
						CrmIssueCommentTO newComment = new CrmIssueCommentTO();
						newComment.setComment(messageText);
						newComment.setTimestamp(new Date());
						newComment.setActionType(CrmIssueDescriptionActionType.FROM_AFS);
						issue.getCommentList().add(newComment);
						Runnable callback = new Runnable() {
							@Override
							public void run() {
								sv.smoothScrollTo(0, sv.getChildAt(0).getHeight());
							}
						};
						refreshConversation();
						callback.run();
						//ch.addComment(newComment.getComment(), newComment.getTimestamp(), newComment.getActionType().equals(CrmIssueDescriptionActionType.FROM_AFS), callback);
					}
					
					@Override
					public void onRequestFailed(CrmIssueRequest aRequest) {
						DialogFragment.showNetworkError();
					}
				});
				request.setCachePolicy(CachePolicy.NO_CACHE);
				request.initiateServerRequest();
			}
		});
		
		sv.post(new Runnable() {
			@Override
			public void run() {
				sv.scrollTo(0, sv.getChildAt(0).getHeight());
			}
		});
		
		return view;
	}
	
	public void setEventType(EventType aEventType){
		mEventType = aEventType;
	}
	
	public void refreshConversation(){
		ch.empty();
		if(issue != null) {
			for(CrmIssueCommentTO comment: issue.getCommentList()){
				ch.addComment(comment.getComment(), comment.getTimestamp(), (comment.getActionType().equals(CrmIssueDescriptionActionType.FROM_AFS)));
			}
		}
	}

	@Override
	public void update(Observable observable, Object data) {
		Model.getInstance().deleteObserver(this);
		issue = Model.getInstance().getIssue(issueId);
		refreshConversation();
		View view = getView();
		if(view != null) {
			RefreshSpinner spinner = (RefreshSpinner)view.findViewById(R.id.refresh_conversation);
			if(spinner != null) {
				spinner.stop();
			}
		}
	}
}
