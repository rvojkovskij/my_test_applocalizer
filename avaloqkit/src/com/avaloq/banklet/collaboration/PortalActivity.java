package com.avaloq.banklet.collaboration;

import java.util.Observable;
import java.util.Observer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;

import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.collaboration.BusinessPartnersRequest;
import com.avaloq.framework.comms.webservice.collaboration.CollaborationService;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.util.InlineMessageUtil;

/**
 * Point of entry for the Collaboration Banklet
 * @author zahariev
 */
public class PortalActivity extends BankletActivity implements Observer {
	
	public static final String EXTRA_FORCE_REFRESH = "extra_force_refresh";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.col_portal_activity);
		Model.getInstance().deleteObservers();
		Model.getInstance().addObserver(this);
		boolean forceRefresh = getIntent().getBooleanExtra(EXTRA_FORCE_REFRESH, false);
		if (forceRefresh)
			Model.getInstance().loadData(false);
		else
			Model.getInstance().loadData();
	}
	
	@Override
	/**
	 * Frees the observer, so that it doesn't get called again, in case the web-service request gets
	 * executed again. Apparently this is necessary even though the activity has been destroyed.
	 */
	protected void onDestroy (){
		Model.getInstance().deleteObserver(this);
		super.onDestroy();
	}
	
	public void startCalendarActivity(View view){
		Intent intent = new Intent(this, CalendarActivity.class);
		startActivity(intent);
	}
	
	public void startAdvisorActivity(View view){
		Intent intent = new Intent(this, AdvisorDataActivity.class);
		startActivity(intent);
	}

	@Override
	public void update(Observable observable, final Object data) {
		CollaborationService.getBusinessPartners(new RequestStateEvent<BusinessPartnersRequest>() {
			@Override
			public void onRequestCompleted(BusinessPartnersRequest aRequest) {
				Model.getInstance().setBusinessPartners(aRequest.getResponse().getData().getBusinessPartners());
				if (data == null){
					onCommunicationBreakdown();
				}
				else{
					findViewById(R.id.loading_container).setVisibility(View.GONE);
					setupFragments();
				}
			}
			
			@Override
			public void onRequestFailed(BusinessPartnersRequest aRequest) {
				onCommunicationBreakdown();
			}
		}).initiateServerRequest();
	}
	
	public void onCommunicationBreakdown(){
		ViewGroup vg = (ViewGroup)findViewById(android.R.id.content);
		InlineMessageUtil.showNetworkError(vg);
	}
	
	private void setupFragments(){
		FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
		boolean commitRequired = false;
		if (getSupportFragmentManager().findFragmentById(R.id.fragment_main) == null){
			MainFragment elf = new MainFragment();
			tx.replace(R.id.fragment_main, elf);
			commitRequired = true;
		}
		
		View aef_view = findViewById(R.id.fragment_add_event);
		if (aef_view != null && getSupportFragmentManager().findFragmentById(R.id.fragment_add_event) == null){
			AddEventFragment aef = new AddEventFragment();
			tx.replace(R.id.fragment_add_event, aef);
			commitRequired = true;
		}
		
		View adf_view = findViewById(R.id.fragment_advisor_data);
		if (adf_view != null && getSupportFragmentManager().findFragmentById(R.id.fragment_advisor_data) == null){
			AdvisorDataFragment adf = new AdvisorDataFragment();
			tx.replace(R.id.fragment_advisor_data, adf);
			commitRequired = true;
		}
		if (commitRequired)
			try {
				tx.commit();
			}
			catch (Exception e){}
	}
	

}