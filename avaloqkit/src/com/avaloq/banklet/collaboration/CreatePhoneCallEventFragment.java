package com.avaloq.banklet.collaboration;

import java.util.Date;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueType;
import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class CreatePhoneCallEventFragment extends CreateBaseEventFragment{
	public CreatePhoneCallEventFragment(){
		super();
		eventType = EventType.PHONE_CALL;
	}

	@Override
	public int getNoticeMessageId() {
		return R.string.col_phonecall_default_message;
	}

	@Override
	public int getSaveButtonId() {
		return R.string.col_event_phonecall_save;
	}

	@Override
	public int getDialogTitleId() {
		return R.string.col_dialog_title_phonecall;
	}

	@Override
	public CrmIssueTO onValidationSuccess() {
		CrmIssueTO issue = new CrmIssueTO();
		DateUtil dateUtil = new DateUtil(getActivity());
		Date dateFrom, dateTo;
		try{
			dateFrom = dateUtil.parseDateTime((String)view_date_from.getText(), (String)view_time_from.getText());
			dateTo = dateUtil.parseDateTime((String)view_date_to.getText(), (String)view_time_to.getText());
		}
		catch (Exception e){
			return null;
		}

		// These fields are set statically in the iOS code
		issue.setType(CrmIssueType.CALL_REQUEST);
		issue.setOpenDate(new Date());
		if (partnerId != 0)
			issue.setBusinessPartnerId(partnerId);
		
		issue.setDueDate(dateFrom);
		issue.setSubject(subject.getText().toString());
		issue.setComment(message.getText().toString());
		issue.setAppointmentStartDate(dateFrom);
		issue.setAppointmentEndDate(dateTo);
		
		return issue;
	}
}
