package com.avaloq.banklet.collaboration;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.Banklet;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.ActivityNavigationItem;

/**
 * Banklet class for Collaboration
 * @author Timo Schmid <t.schmid@insign.ch>
 *
 */
public class CollaborationBanklet extends Banklet {
	
	/**
	 * The array of navigation items for this banklet
	 */
	private static ActivityNavigationItem[] items = {
		new ActivityNavigationItem(AvaloqApplication.getContext(), PortalActivity.class, R.string.col_banklet_name, R.attr.IconCollaboration, R.style.Theme_Banklet_COL)
	};

	@Override
	public void onCreate() {
	}

	@Override
	public ActivityNavigationItem getInitialActivity() {
		return items[0];
	}

	@Override
	public ActivityNavigationItem[] getMainNavigationItems() {
		return items;
	}

	@Override
	public void onEmptyCache() {
		CollaborationBanklet.getInstance();
		
	}
	
	/**
	 * Get the CollaborationBanklet singleton instance
	 */
	public static CollaborationBanklet getInstance() {
		return (CollaborationBanklet) Banklet.getInstanceOf(CollaborationBanklet.class);
	}

}
