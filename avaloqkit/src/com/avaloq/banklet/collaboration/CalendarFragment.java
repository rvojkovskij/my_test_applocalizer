package com.avaloq.banklet.collaboration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.joda.time.DateTime;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.framework.R;
import com.caldroid.CaldroidFragment;
import com.caldroid.CaldroidGridAdapter;
import com.caldroid.CaldroidListener;

public class CalendarFragment extends CaldroidFragment {
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState);
		
		setCaldroidListener(new CaldroidListener() {
			@Override
			public void onSelectDate(Date date, View view) {
				final FragmentTransaction ft = getFragmentManager().beginTransaction();
				DayViewFragment dvf = new DayViewFragment();
				Bundle args = new Bundle();
				args.putLong(DayViewFragment.EXTRA_TIMESTAMP, date.getTime());
				dvf.setArguments(args);
				ft.replace(getId(), dvf, getTag());
				ft.addToBackStack(null);
				ft.commit();
			}
		});
		
		return view;
	}
	
	@Override
	public CaldroidGridAdapter getNewDatesGridAdapter(int month, int year) {
		return new CollaborationGridAdapter(getActivity(), month, year,
				getCaldroidData(), extraData);
	}
	
	public class CollaborationGridAdapter extends CaldroidGridAdapter{
		public CollaborationGridAdapter(Context context, int month, int year,
				HashMap<String, Object> caldroidData,
				HashMap<String, Object> extraData) {
			super(context, month, year, caldroidData, extraData);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			DateTime dateTime = datetimeList.get(position);
			List<CrmIssueTO> list = new ArrayList<CrmIssueTO>();
			if (dateTime.getMonthOfYear() == month)
				list = Model.getInstance().getEventsByDay(dateTime.getDayOfMonth(), month, year);
			
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			ViewGroup cellView = (ViewGroup)inflater.inflate(R.layout.col_calendar_cell, null);
			ViewGroup eventList = (ViewGroup)cellView.findViewById(R.id.events);
			
			TextView superView = (TextView)super.getView(position, null, parent);
			superView.setGravity(Gravity.TOP | Gravity.RIGHT);
			superView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			// int superViewPadding = (int)CalendarFragment.this.getActivity().getResources().getDimension(R.dimen.monthview_cell_padding);
			// superView.setPadding(superViewPadding, superViewPadding, superViewPadding, superViewPadding);
			superView.setPadding(0, 0, 0, 0);
			
			if (dateTime.equals(getToday())) {
				superView.setBackgroundResource(0);
				superView.setText("");
				View todayLabel = cellView.findViewById(R.id.label_today);
				todayLabel.setVisibility(View.VISIBLE);
			}
			
			cellView.addView(superView);
			for (CrmIssueTO issue: list)
				addEvent(eventList, issue.getType().toString());
			
			eventList.bringToFront();
			return cellView;
		}
		
		public void addEvent(ViewGroup parent, String eventType){
			Resources resources = CalendarFragment.this.getActivity().getResources();
			Drawable drawable = resources.getDrawable(R.drawable.col_calendar_event);
			int colorId = resources.getIdentifier("event_"+eventType.toLowerCase(Locale.ENGLISH), "color", CalendarFragment.this.getActivity().getPackageName());
			drawable.setColorFilter(resources.getColor(colorId), PorterDuff.Mode.SRC_ATOP);
			ImageView image = new ImageView(CalendarFragment.this.getActivity());
			int eventSize = (int)CalendarFragment.this.getActivity().getResources().getDimension(R.dimen.monthview_event_circle_size);
			image.setLayoutParams(new LayoutParams(eventSize, eventSize));
			image.setImageDrawable(drawable);
			parent.addView(image);
		}
		
	}
}