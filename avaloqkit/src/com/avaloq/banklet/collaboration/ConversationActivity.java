package com.avaloq.banklet.collaboration;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;

public class ConversationActivity extends BankletActivity{
	
	public static final String EXTRA_ISSUE_ID = "extra_issue_id";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.col_conversation_activity);
		FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
		
		long issueId = getIntent().getLongExtra(EXTRA_ISSUE_ID, 0);
		if (issueId == 0)
			throw new IllegalArgumentException("The argument EXTRA_ISSUE_ID must be set.");
		
		ConversationFragment cf = new ConversationFragment();
		Bundle arguments = new Bundle();
		arguments.putLong(ConversationFragment.EXTRA_ISSUE_ID, issueId);
		cf.setArguments(arguments);
		View cf_view = findViewById(R.id.fragment_conversation);
		
		if (cf_view != null)
			tx.replace(R.id.fragment_conversation, cf);
		
		tx.commit();
	}
}
