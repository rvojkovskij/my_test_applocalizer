package com.avaloq.banklet.collaboration;

import com.avaloq.framework.BankletActivityDelegate;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.FragmentNavigationItem;
import com.avaloq.framework.ui.TabbedBankletFragment;

public class MainFragment extends TabbedBankletFragment{

	@Override
	public FragmentNavigationItem[] getNavigationItems() {
		FragmentNavigationItem[] items;
		if (!BankletActivityDelegate.isSmallDevice(getActivity()) && getActivity().getResources().getBoolean(R.bool.col_calendar_enabled)) {
			items = new FragmentNavigationItem[2];
			items[0] = new FragmentNavigationItem(EventListFragment.class, getActivity().getString(R.string.col_event_list_tab_title));
			items[1] = new FragmentNavigationItem(CalendarFragment.class, getActivity().getString(R.string.col_calendar_tab_title));
		}
		else {
			items = new FragmentNavigationItem[1];
			items[0] = new FragmentNavigationItem(EventListFragment.class, getActivity().getString(R.string.col_event_list_tab_title));
		}
		
		return items;
	}
	
	
}
