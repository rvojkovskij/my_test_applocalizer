package com.avaloq.banklet.collaboration;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletActivity;

public class AdvisorDataActivity extends BankletActivity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.col_conversation_activity);
		FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
		
		AdvisorDataFragment cf = new AdvisorDataFragment();
		View cf_view = findViewById(R.id.fragment_conversation);
		
		if (cf_view != null)
			tx.replace(R.id.fragment_conversation, cf);
		
		tx.commit();
	}
}
