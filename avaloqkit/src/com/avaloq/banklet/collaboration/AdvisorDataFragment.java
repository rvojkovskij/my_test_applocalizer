package com.avaloq.banklet.collaboration;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class AdvisorDataFragment extends BankletFragment{
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.col_advisor_data, container, false);
		final TextView location = (TextView)view.findViewById(R.id.advisor_location);

		// This is necessary, because the autoLink="map" often doesn't recognize strings as addresses
		location.setText(Html.fromHtml("<u>"+location.getText()+"</u>"));
		location.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+location.getText()));
				getActivity().startActivity(intent);
			}
		});
		
		String[] links = getActivity().getResources().getStringArray(R.array.col_static_advisor_links);
		if (links != null && links.length > 0){
			LinearLayout linksLayout = (LinearLayout)view.findViewById(R.id.col_static_advisor_links);
			for (String linkText: links){
				TextView link = new TextView(getActivity());
				
				link.setAutoLinkMask(Linkify.WEB_URLS);
				link.setText(linkText);
				link.setLinkTextColor(Color.BLACK);
				linksLayout.addView(link);
				try {
					link.setGravity(((LinearLayout.LayoutParams)linksLayout.getLayoutParams()).gravity);
				}
				catch (Exception e){}
			}
		}
		
		
		return view;
	}
}