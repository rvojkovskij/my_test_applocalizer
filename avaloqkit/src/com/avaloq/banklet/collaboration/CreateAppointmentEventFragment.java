package com.avaloq.banklet.collaboration;

import java.util.Date;

import com.avaloq.afs.server.bsp.client.ws.CrmIssueTO;
import com.avaloq.afs.server.bsp.client.ws.CrmIssueType;
import com.avaloq.framework.R;
import com.avaloq.framework.util.DateUtil;

public class CreateAppointmentEventFragment extends CreateBaseEventFragment {
	public CreateAppointmentEventFragment(){
		super();
		eventType = EventType.APPOINTMENT;
	}

	@Override
	public int getNoticeMessageId() {
		return R.string.col_appointment_default_message;
	}

	@Override
	public int getSaveButtonId() {
		return R.string.col_event_appointment_save;
	}

	@Override
	public CrmIssueTO onValidationSuccess() {
		CrmIssueTO issue = new CrmIssueTO();
		DateUtil dateUtil = new DateUtil(getActivity());
		Date dateFrom, dateTo;
		try{
			dateFrom = dateUtil.parseDateTime((String)view_date_from.getText(), (String)view_time_from.getText());
			dateTo = dateUtil.parseDateTime((String)view_date_to.getText(), (String)view_time_to.getText());
		}
		catch (Exception e){
			return null;
		}

		// These fields are set statically in the iOS code
		issue.setType(CrmIssueType.APPOINTMENT);
		issue.setOpenDate(new Date());
		if (partnerId != 0)
			issue.setBusinessPartnerId(partnerId);
		
		issue.setDueDate(dateFrom);
		issue.setSubject(subject.getText().toString());
		issue.setComment(message.getText().toString());
		issue.setAppointmentStartDate(dateFrom);
		issue.setAppointmentEndDate(dateTo);
		
		return issue;
	}

	@Override
	public int getDialogTitleId() {
		return R.string.col_dialog_title_appointment;
	}
}
