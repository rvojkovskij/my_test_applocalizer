package com.avaloq.app.bs;

import android.os.Bundle;

import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.ui.AbstractLaunchActivity;


/**
 * This is the main app launcher activity as needed by every app.
 * It will load the banklet configuration and then open the app's configured first activity.
 * 
 * Normally empty - overwrite lifecycle methods to customize per-app behaviour 
 * but ensure you call through.
 * 
 */
public class LaunchActivity extends AbstractLaunchActivity {
	
	@Override
	protected void onResume() {
		super.onResume();
		if(getAppConfiguration().getBaseUrl() == null) {
			BSConfigurationDialog dialog = new BSConfigurationDialog();
			dialog.setRunnable(new Runnable() {
				@Override
				public void run() {
					AvaloqApplication.getInstance().initialize(LaunchActivity.this, getAppConfiguration());
				}
			});
			dialog.show(this);
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	private AppConfigurationInterface mAppConfiguration;

	@Override
	protected AppConfigurationInterface getAppConfiguration() {
		if(mAppConfiguration == null) {
			mAppConfiguration = new AppConfiguration(this);
		}
		return mAppConfiguration;
	}

}