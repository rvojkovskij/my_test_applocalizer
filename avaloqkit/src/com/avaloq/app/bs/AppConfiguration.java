package com.avaloq.app.bs;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.avaloq.banklet.collaboration.CollaborationBanklet;
import com.avaloq.banklet.documentsafe.DocumentSafeBanklet;
import com.avaloq.banklet.payments.PaymentBanklet;
import com.avaloq.banklet.trading.TradingBanklet;
import com.avaloq.banklet.wealth.WealthBanklet;
import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.comms.authentication.medusa.MedusaAuthenticationHandler;
import com.avaloq.framework.comms.authentication.simplepost.SimplePostAuthenticationHandler;

/**
 * Contains this application's configuration
 */
public class AppConfiguration implements AppConfigurationInterface {
	
	private static final String TAG = AppConfiguration.class.getSimpleName();
	
	private Context mContext;
	
	public AppConfiguration(Context context) {
		mContext = context;
	}
	
	/**
	 * Banklets used in this  application. They are aranged in the given order.
	 */
	public Class<?>[] getBanklets() {
		Class<?> banklets[] = {
                WealthBanklet.class,
                CollaborationBanklet.class,
                TradingBanklet.class,
                DocumentSafeBanklet.class,
                PaymentBanklet.class
		};
		return banklets;
	}

	/**
	 * Returns the Authentication Handler Classes for this app
	 * @return An array of Authentication handler classes
	 */
	@Override
	public Class<?>[] getAuthenticationHandlers() {
		Class<?> authHandlers[] = {
				MedusaAuthenticationHandler.class,
				SimplePostAuthenticationHandler.class
		};
		return authHandlers;
	}

	public String getBaseUrl() {
		String baseUrl = BSConfigurationDialog.getBaseUrl(mContext);
		Log.d(TAG, "getBaseUrl: " + baseUrl);
		return baseUrl;
		/*
		// return "http://10.140.24.23:80//frontAggregationServices/";
		return "http://afsdemo1.avaloq.com:80";
		*/
	}
	
	@Override
	public String getWebserviceBaseUrl() {
		return getBaseUrl() + "/frontAggregationServices/";
	}
	
	@Override
	public Class<? extends Activity> getStartupActivity() {
		return LaunchActivity.class;
	}

	@Override
	public int getServerSessionDefaultTimeout() {
		return 1800;
	}

	@Override
	public boolean showExpDateOnFundSale() {
		return true;
	}
	
	private static HashMap<String, String> endpointMap = new HashMap<String,String>();
	
	static {
		endpointMap.put("Avaloq Internal Medusa (Timo)", "http://10.140.24.23:80/");
		endpointMap.put("Avaloq Internal Medusa", "http://10.140.24.23:80/");
		endpointMap.put("Avaloq Demo Server SSL", "https://afsdemo1.avaloq.com:443/");
		endpointMap.put("Avaloq Demo Server", "http://afsdemo1.avaloq.com/");
		endpointMap.put("Avaloq Demo Server 3 SSL", "https://afsdemo3.avaloq.com:443/");
		endpointMap.put("Avaloq Demo Server 3", "http://afsdemo3.avaloq.com/");
        endpointMap.put("Avaloq Dev Server 1", "http://tuxdmzdev01.dmz.avaloq.com:9080/");
        endpointMap.put("Avaloq Dev Server 2", "http://tuxdmzdev02.dmz.avaloq.com:9080/");
        endpointMap.put("Avaloq Dev Server 2 (SSL)", "https://tuxdmzdev02.dmz.avaloq.com:9081/");
        endpointMap.put("Avaloq Dev Server 3", "http://tuxdmzdev03.dmz.avaloq.com:9080/");
		endpointMap.put("Ergon Test Server", "https://llbgate.ergon.ch/");
		endpointMap.put("Intellicard Test Server", "https://medusa.llb.loc:9443/");
	}
	
	@Override
	public Map<String, String> getApplicationEndpoints() {
		return endpointMap;
	}
	

	@Override
	public boolean showWealthCharts() {
		return true;
	}

	@Override
	public boolean hideMarketData() {
		return true;
	}


	@Override
	public boolean allowedSellFundsByAmount() {
		return true;
	}

	@Override
	public String[] getSSLTrustedPins() {
		String[] pins = new String[] {
				"c07a98688d89fbab05640c117daa7d65b8cacc4e"  // GeoTrust
				};
		return pins;
	}

	
}
