package com.avaloq.app.bs;

import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.DialogFragment;

public class BSConfigurationDialog extends DialogFragment {
	
	protected static final String PREFERENCES_CONFIG = "prefs_config";

	private static final String KEY_SERVICE_URL = "serviceUrl";
	
	private Entry<String,String> mSelectedEntry = null;
	
	private static final String TAG = BSConfigurationDialog.class.getSimpleName();
	
	private Runnable mRunnable;
	
	public BSConfigurationDialog() {}
	
	public void setRunnable(Runnable runnable) {
		mRunnable = runnable;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		getDialog().setTitle("Configuration");
		
		final View view = inflater.inflate(R.layout.bs_configuration, container);
		
		TableLayout tableLayout = (TableLayout)view.findViewById(R.id.bs_configuration_endpoint_table);
		
		tableLayout.removeAllViews();
		
		Map<String,String> entryPointMap = AvaloqApplication.getInstance().getConfiguration().getApplicationEndpoints();
		
		view.findViewById(R.id.bs_configuration_ok).setEnabled(getPreferences().contains(KEY_SERVICE_URL));
		
		for(final Entry<String,String> entryPoint : entryPointMap.entrySet()) {
			TableRow tableRow = new TableRow(getActivity());
			TextView textView = new TextView(getActivity());
			textView.setText(entryPoint.getValue());
			tableRow.addView(textView);
			tableRow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mSelectedEntry = entryPoint;
					((EditText)view.findViewById(R.id.bs_configuration_base_url_value)).setText(entryPoint.getValue());
					view.findViewById(R.id.bs_configuration_ok).setEnabled(true);
					//getPreferences().edit().putString(KEY_SERVICE_URL, mSelectedEntry.getValue()).commit();
				}
			});
			tableLayout.addView(tableRow);
		}
		
		view.findViewById(R.id.bs_configuration_cancel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(getPreferences().contains(KEY_SERVICE_URL)) {
					getDialog().dismiss();
				} else {
					/*
			    	AuthenticationManager.getInstance().authenticationCancelledByUser();
			    	Intent startMain = new Intent(Intent.ACTION_MAIN);
			    	startMain.addCategory(Intent.CATEGORY_HOME);
			    	startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    	startActivity(startMain);
			    	*/
				}
			}
		});
		
		view.findViewById(R.id.bs_configuration_ok).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String base_url = ((EditText)view.findViewById(R.id.bs_configuration_base_url_value)).getText().toString();
				getPreferences().edit().putString(KEY_SERVICE_URL, base_url).commit();
				getDialog().dismiss();
				if(mRunnable != null) mRunnable.run();
			}
		});
		
		view.findViewById(R.id.bs_configuration_ok).setEnabled(false);
		
		return view;
	}
	
	private SharedPreferences getPreferences() {
		return getPreferences(getActivity());
	}
	
	public static SharedPreferences getPreferences(Context c) {
		return c.getSharedPreferences(PREFERENCES_CONFIG, Context.MODE_PRIVATE);
	}
	
	public static String getBaseUrl(Context c) {
		return getPreferences(c).getString(KEY_SERVICE_URL, null);
	}

}
