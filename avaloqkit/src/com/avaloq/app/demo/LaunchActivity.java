package com.avaloq.app.demo;

import com.avaloq.framework.AppConfigurationInterface;
import com.avaloq.framework.ui.AbstractLaunchActivity;


/**
 * This is the main app launcher activity as needed by every app.
 * It will load the banklet configuration and then open the app's configured first activity.
 * 
 * Normally empty - overwrite lifecycle methods to customize per-app behaviour 
 * but ensure you call through.
 * 
 */
public class LaunchActivity extends AbstractLaunchActivity {

	@Override
	protected AppConfigurationInterface getAppConfiguration() {
		return new AppConfiguration();
	}

}