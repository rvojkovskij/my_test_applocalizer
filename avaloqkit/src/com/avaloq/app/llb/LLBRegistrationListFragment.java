package com.avaloq.app.llb;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import ch.intellicard.mks.api.MKSDeviceDeregistrationDelegate;
import ch.intellicard.mks.api.MKSException;
import ch.intellicard.mks.api.MKSUserID;

import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.ui.DialogFragment;

public class LLBRegistrationListFragment extends BankletFragment implements MKSDeviceDeregistrationDelegate {

	private ListView mRegisteredUsersList;
	private Button mDeletedButton;

	private RegistrationListAdapter mAdapter;

	//	// TODO remove this, just for testing
	//	private String preferedUserID;

	public interface RegistrationListFragmentInterface{
		public void registerNewUserId();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.llb_registration_list, container, false);

		mRegisteredUsersList = (ListView) fragmentView.findViewById(R.id.avq_registered_ids_list);

		mDeletedButton = (Button) fragmentView.findViewById(R.id.avq_delete_user_button);
		mDeletedButton.setEnabled(false);

		mDeletedButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeRegisteredUser();
			}
		});

		((Button) fragmentView.findViewById(R.id.avq_register_new_user_button)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				((RegistrationListFragmentInterface) getActivity()).registerNewUserId();
			}
		});

		displayRegisteredIds();

		return fragmentView;
	}

	//	/**
	//	 * 
	//	 * @return
	//	 */
	//	private MKSUserID[] getTestUserIds(){
	//		MKSUserID[] registeredUsers =  new MKSUserID[2];
	//		MKSUserID u1 = new MKSUserID() {
	//			
	//			@Override
	//			public boolean isEqualToUserID(MKSUserID arg0) {
	//				return (arg0.getUserIdentifier().compareTo(this.getUserIdentifier()) == 0);
	//			}
	//			
	//			@Override
	//			public String getUserIdentifier() {
	//				return "1";
	//			}
	//			
	//			@Override
	//			public String getCombinedIdentifier() {
	//				return null;
	//			}
	//			
	//			@Override
	//			public String getBusinessUnitIdentifier() {
	//				return "asd";
	//			}
	//		};
	//		
	//		registeredUsers[0] = u1;
	//		
	//		MKSUserID u2 = new MKSUserID() {
	//			
	//			@Override
	//			public boolean isEqualToUserID(MKSUserID arg0) {
	//				return (arg0.getUserIdentifier().compareTo(this.getUserIdentifier()) == 0);
	//			}
	//			
	//			@Override
	//			public String getUserIdentifier() {
	//				return "2";
	//			}
	//			
	//			@Override
	//			public String getCombinedIdentifier() {
	//				return null;
	//			}
	//			
	//			@Override
	//			public String getBusinessUnitIdentifier() {
	//				return "ert";
	//			}
	//		};
	//		
	//		registeredUsers[1] = u2;
	//		
	//		return registeredUsers;
	//	}

	/**
	 * 
	 */
	private void displayRegisteredIds(){
		try {

			MKSUserID[] registeredUsers = LLBKeystore.getApplicationMobileKeystore().getRegisteredUserIDs();


			//			// TODO remove, just for testing
			//			registeredUsers = getTestUserIds();

			mAdapter = new RegistrationListAdapter(getActivity(), 0, 0, registeredUsers, LLBKeystore.getApplicationMobileKeystore().getPreferredUserID());

			mRegisteredUsersList.setAdapter(mAdapter);

		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**	
	 * 
	 * @param isAnythingSelected
	 */
	public void setSelection(boolean isAnythingSelected){
		if (isAnythingSelected){
			mDeletedButton.setEnabled(true);
		}else{
			mDeletedButton.setEnabled(false);
		}
	}

	/**
	 * 
	 */
	public void removeRegisteredUser(){

		// show confirmation dialog first
		AlertDialog confirmDeleteDialogBox = new AlertDialog.Builder(getActivity())
		// set message, title, and icon
		.setTitle(R.string.avq_registration_registration_confirm_delete)
		.setMessage(R.string.avq_registration_confirmation_question)
		.setPositiveButton(R.string.avq_registration_confirm, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int whichButton) {

				if (mAdapter.getHighlightedId() != null){
					try {
						LLBKeystore.getApplicationMobileKeystore().deregisterDevice(
								mAdapter.getHighlightedId().getUserIdentifier(), 
								mAdapter.getHighlightedId().getBusinessUnitIdentifier(), 
								LLBRegistrationListFragment.this);
					} catch (ConfigurationNotInitializedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (CannotOpenKeystoreException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}					
				}

				dialog.dismiss();
			}

		})
		.setNegativeButton(R.string.avq_registration_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}).create();
		confirmDeleteDialogBox.show();		
	}

	/**
	 * 
	 * @author victor
	 *
	 */
	public class RegistrationListAdapter extends ArrayAdapter<MKSUserID>{

		private final Context context;
		private MKSUserID mSelectedId;
		private int mHighlightedPosition = -1;

		private MKSUserID[] items;
		
		class RegistrationListViewHolder{
			TextView textId;
			RadioButton radiobutton;
		}

		@Override
		public MKSUserID getItem(int position){
			return items[position];
		}
		
		@Override
		public int getCount(){
			return items.length;
		}
		
		/**
		 * 
		 * @param context
		 * @param resource
		 * @param textViewResourceId
		 * @param objects
		 * @param selectedId
		 */
		public RegistrationListAdapter(Context context, int resource, int textViewResourceId, MKSUserID[] objects, MKSUserID selectedId) {
			super(context, resource, textViewResourceId, objects);
			items = objects;
			this.context = context;		
			mSelectedId = selectedId;
		}

		/**
		 * 
		 */
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View rowView = convertView; 

			if (rowView == null){	
				rowView = inflater.inflate(R.layout.llb_registration_list_item, parent, false);

				RegistrationListViewHolder viewHolder = new RegistrationListViewHolder();

				viewHolder.textId = (TextView) rowView.findViewById(R.id.avq_registration_id);
				viewHolder.radiobutton = (RadioButton) rowView.findViewById(R.id.avq_registration_checkbox);

				rowView.setTag(viewHolder);
			}

			RegistrationListViewHolder viewHolder = (RegistrationListViewHolder) rowView.getTag();

			// set the text of the item in the list
			viewHolder.textId.setText(getItem(position).getUserIdentifier());

			// check/uncheck the radiobutton 
			if (mSelectedId != null && mSelectedId.isEqualToUserID(getItem(position))){
				viewHolder.radiobutton.setChecked(true);
			}else{
				viewHolder.radiobutton.setChecked(false);
			}

			// when the radiobutton is checked, set the user as default
			viewHolder.radiobutton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					if (((RadioButton)v).isChecked()){
						try {							
							LLBKeystore.getApplicationMobileKeystore().setPreferredUserIdentifier(getItem(position).getUserIdentifier(), getItem(position).getBusinessUnitIdentifier());
							mSelectedId = getItem(position);
							notifyDataSetChanged();
						} catch (ConfigurationNotInitializedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							((RadioButton)v).setChecked(false);
						} catch (CannotOpenKeystoreException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							((RadioButton)v).setChecked(false);
						}						
					}		
				}
			});

			// set the row as selected/unselected
			rowView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					mHighlightedPosition = position;
					setSelection(true);
					notifyDataSetChanged();
				}
			});

			if (mHighlightedPosition == position){
				rowView.setBackgroundColor(getResources().getColor(R.color.gray_transparent));
			}else{
				rowView.setBackgroundColor(Color.WHITE);
			}

			return rowView;
		}

		public void loadData(){
			try {
				MKSUserID[] registeredUsers = LLBKeystore.getApplicationMobileKeystore().getRegisteredUserIDs();
				items = registeredUsers;
			} catch (ConfigurationNotInitializedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CannotOpenKeystoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		/**
		 * 
		 * @return
		 */
		public MKSUserID getHighlightedId(){
			if (mHighlightedPosition != -1){
				return getItem(mHighlightedPosition);
			}
			return null;
		}

		/**
		 * 
		 */
		public void resetHighlightedPosition(){
			mHighlightedPosition = -1;
			notifyDataSetChanged();
		}
	}

	@Override
	public void deregistrationRequestFailed(MKSException arg0) {
		DialogFragment.createAlert(getSherlockActivity().getString(R.string.avq_registration_device_deregistration_failed), arg0.getLocalizedMessage(), getActivity()).show(getActivity());
	}

	@Override
	public void deregistrationRequestSuccessful() {
		mAdapter.resetHighlightedPosition();
		mAdapter.loadData();
		mAdapter.notifyDataSetChanged();
	}
}
