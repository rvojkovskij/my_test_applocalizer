package com.avaloq.app.llb;

import android.content.Intent;
import android.util.Log;

import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.comms.AbstractServerRequest;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.authentication.AbstractAuthenticationHandler;
import com.avaloq.framework.comms.http.AbstractHTTPServerRequest;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.LogoutActivity;

public class LLBAuthenticationHandler extends AbstractAuthenticationHandler {

    private static final String TAG = LLBAuthenticationHandler.class.getSimpleName();
	
	private Thread mTimeoutHandlerThread;
	
	/**
	 * Monitor possible timeouts and invalidate the authentication if reached
	 */
	private class TimeoutHandlerThread extends Thread {
		
		private String TAG = TimeoutHandlerThread.class.getSimpleName();
		
		public TimeoutHandlerThread() {
			setName(TAG);			
		}

		@Override
		public void run() {

            int sessionTimeout = AvaloqApplication.getInstance().getConfiguration().getServerSessionDefaultTimeout();            
            Log.i(TAG, "Starting session timeout monitoring. Session timeout period is " + sessionTimeout + "s.");
			
			while (true) {

                    Log.d(TAG, "is authenticated: " + LLBAuthenticationHandler.this.isAuthenticated());
				
				// If logged in by this auth handler, continually check for timeout conditions
				// and auto-logout + show login screen if met.
				if (LLBAuthenticationHandler.this.isAuthenticated()) {

                    long timeSinceLastRequest = QueueManager.getInstance().getElapsedTimeSinceLastExecutedRequest();

//                    Log.d(TAG, "sessionTimeout: " + sessionTimeout);
//                    Log.d(TAG, "timeSinceLastRequest: " + timeSinceLastRequest);

					if(timeSinceLastRequest > (sessionTimeout * 1000)) {
						Log.i(TAG, "Session timeout occured in SimplePostAuthenticationHandler. Last request is " + (timeSinceLastRequest/1000) + "s old.");
		            	BankletActivity.getActiveActivity().startActivity(new Intent(BankletActivity.getActiveActivity(), LogoutActivity.class));
		            	LLBAuthenticationHandler.this.setAuthenticationStatus(false);
		            	LLBAuthenticationHandler.this.showLoginDialog();
		            }
				}
				
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {					
					e.printStackTrace();
				}				
			}
		}
	}
	
	
	
	@Override
	public boolean isAuthenticationResponse(AbstractServerRequest<?> aRequest) {
		if (aRequest instanceof AbstractHTTPServerRequest<?>) {						
			int responseCode = ((AbstractHTTPServerRequest<?>) aRequest).getHttpResponse().getHTTPResponseCode();

			// The 500 http response code is our trigger for the simple demo POST login request 
			return !isAuthenticated();//responseCode == 500;
		}

		return false;
	}

	@Override
	public void processAuthenticationResponse(AbstractServerRequest<?> aRequest) {

		// Re-check if it's an auth response
		if (!isAuthenticationResponse(aRequest)) return;

		// Step 1 of 3: Show login user dialog (further steps handled in the corresponding activity)
		showLoginDialog();
	}

	/**
	 * Show the login activity. Input handling etc. is controlled from there.
	 */
	public void showLoginDialog() {
		//AvaloqApplication.getInstance().setInitialLoginDone(false);
		Intent intent = new Intent(AvaloqApplication.getInstance(), LLBLoginActivity.class);
		intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
		AvaloqApplication.getInstance().startActivity(intent);
	}

	
	/**
	 * Turn the session timeout monitoring on/off depending on the set auth status
	 */
	@Override
	public void setAuthenticationStatus(boolean isAuthenticated) {
        Log.d(TAG, "setAuthenticationStatus: " + isAuthenticated);
        for(StackTraceElement el : Thread.currentThread().getStackTrace()) {
            Log.d(TAG, el.toString());
        }
        super.setAuthenticationStatus(isAuthenticated);
		
		// Start the timeout thread on the first login (and ensure it's still running on subsequent ones)
		// if a session timeout is configured
		
		if (isAuthenticated && AvaloqApplication.getInstance().getConfiguration().getServerSessionDefaultTimeout() > 0) {
			startTimeoutHandler();
		}
		
	}
	
	/**
	 * Start the timeout handler thread if it's not yet running.
	 */
	protected void startTimeoutHandler() {
		if (mTimeoutHandlerThread == null || !mTimeoutHandlerThread.isAlive()) {
			mTimeoutHandlerThread = new TimeoutHandlerThread();
			mTimeoutHandlerThread.setDaemon(true);
			//mTimeoutHandlerThread.start();
		}
	}
	
	@Override
	public void onLogout() {
		AbstractHTTPServerRequest.emptyCookieStore();
	}

}
