package com.avaloq.app.llb;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import ch.intellicard.mks.api.MKSMutableConfig;

import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.ui.DialogFragment;

public class LLBConfigurationDialog extends DialogFragment {
	
	private Entry<String,String> mSelectedEntry = null;
	
	private static final String TAG = LLBConfigurationDialog.class.getSimpleName();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		getDialog().setTitle("Configuration");
		
		final View view = inflater.inflate(R.layout.llb_configuration, container);
		
		((TextView)view.findViewById(R.id.llb_configuration_mks_library_name_value)).setText(LLBKeystore.getName());
		
		((TextView)view.findViewById(R.id.llb_configuration_mks_library_version_value)).setText(LLBKeystore.getVersion());
		
		TableLayout tableLayout = (TableLayout)view.findViewById(R.id.llb_configuration_endpoint_table);
		
		tableLayout.removeAllViews();
		
		Map<String,String> entryPointMap = AvaloqApplication.getInstance().getConfiguration().getApplicationEndpoints();
		
		for(final Entry<String,String> entryPoint : entryPointMap.entrySet()) {
			TableRow tableRow = new TableRow(getActivity());
			TextView textView = new TextView(getActivity());
			textView.setText(entryPoint.getValue());
			tableRow.addView(textView);
			tableRow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mSelectedEntry = entryPoint;
					((EditText)view.findViewById(R.id.llb_configuration_base_url_value)).setText(entryPoint.getValue());
					view.findViewById(R.id.llb_configuration_ok).setEnabled(true);
				}
			});
			tableLayout.addView(tableRow);
		}
		
		List<String> configurationEntries = LLBKeystore.getSavedConfigurations();
		
		for(String config : configurationEntries) {
			Log.d(TAG, "config entry: " + config);
		}
		
		view.findViewById(R.id.llb_configuration_cancel).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					LLBKeystore.getApplicationMobileKeystore();
				} catch(ConfigurationNotInitializedException e) {
					Log.d(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
			    	AuthenticationManager.getInstance().authenticationCancelledByUser();
			    	Intent startMain = new Intent(Intent.ACTION_MAIN);
			    	startMain.addCategory(Intent.CATEGORY_HOME);
			    	startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    	startActivity(startMain);
				} catch(CannotOpenKeystoreException e) {
					Log.d(TAG, e.getClass().getSimpleName() + ": " + e.getMessage());
				}
				getDialog().dismiss();
			}
		});
		
		view.findViewById(R.id.llb_configuration_ok).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MKSMutableConfig config = LLBKeystore.createConfig();
				config.setKeyStoreDirectoryName(mSelectedEntry.getKey());
				config.setAuthenticationBaseURL(mSelectedEntry.getValue());
				LLBKeystore.saveConfig(config);
				try {
					LLBKeystore.getApplicationMobileKeystore();
					getDialog().dismiss();
				} catch (ConfigurationNotInitializedException e) {
					// TODO find a better solution
					getActivity().finish();
				} catch (CannotOpenKeystoreException e) {
					// TODO find a better solution
					getActivity().finish();
				}
			}
		});
		
		view.findViewById(R.id.llb_configuration_ok).setEnabled(false);
		
		return view;
	}

}
