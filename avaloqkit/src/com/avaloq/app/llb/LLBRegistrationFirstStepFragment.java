package com.avaloq.app.llb;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class LLBRegistrationFirstStepFragment extends BankletFragment {
	
	public interface RegistrationFirstStepFragmentInterface {
		public void continueAfterLogin();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.llb_registration_first, container, false);

		((Button) fragmentView.findViewById(R.id.avq_register_next_step)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				((RegistrationFirstStepFragmentInterface) getActivity()).continueAfterLogin();
			}
		});

		return fragmentView;
	}
}
