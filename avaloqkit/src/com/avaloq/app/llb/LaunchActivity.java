package com.avaloq.app.llb;

import android.os.Bundle;
import android.util.Log;

import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.webservice.framework.FrameworkCurrencyListRequest;
import com.avaloq.framework.comms.webservice.framework.FrameworkService;
import com.avaloq.framework.comms.webservice.wealth.WealthOverviewRequest;
import com.avaloq.framework.comms.webservice.wealth.WealthService;
import com.avaloq.framework.ui.BankletActivity;
import com.avaloq.framework.ui.DialogActivity;

/**
 * 
 * FIXME: THIS SHOULD BE NEARLY EMPTY AND INHERIT FROM THE ABSTRACT LAUNCH ACTIVITY
 * do once back on LLB
 * 
 */
public class LaunchActivity extends BankletActivity {
	
	private static final String TAG = LaunchActivity.class.getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.d(TAG, "onCreate");
		
		setContentView(R.layout.llb_activity_empty);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
		
		AvaloqApplication.getInstance().initialize(this, new AppConfiguration());
		
		Log.w(TAG, "is logged in: " + isLoggedIn());

        Log.d(TAG, "doing framework request");
        FrameworkService.getFullCurrencyList(new RequestStateEvent<FrameworkCurrencyListRequest>() {
            @Override
            public void onRequestCompleted(final FrameworkCurrencyListRequest aRequest) {

                // in case the server returns wrong or no data
                if (aRequest.getResponse().getData() == null) {
                	DialogActivity.showNetworkError();
                    return;
                }

                AvaloqApplication.getInstance().setCurrencyList(aRequest.getResponse().getData().getCurrencyList());

                if(isLoggedIn()) {
                    // we are or were already logged in
                    AvaloqApplication.getInstance().startFirstActivity(LaunchActivity.this);
                    finish();
                } else {
                    WealthService.getWealthOverview(new RequestStateEvent<WealthOverviewRequest>() {
                        public void onRequestCompleted(WealthOverviewRequest aRequest) {
                        	finish();
                        };
                        public void onRequestFailed(WealthOverviewRequest aRequest) {
                        	DialogActivity.showNetworkError();
                        };
                    }).initiateServerRequest();
                }

            }

            @Override
            public void onRequestFailed(FrameworkCurrencyListRequest aRequest) {
            	DialogActivity.showNetworkError();
            }
        }).initiateServerRequest();
	}
	
	private boolean isLoggedIn() {
		try {
			return LLBKeystore.getApplicationMobileKeystore().isDeviceAuthenticated() && AvaloqApplication.getInstance().getCurrencyList() != null;
		} catch (ConfigurationNotInitializedException e) {
	    	// show the login dialog
	    	LLBConfigurationDialog config = new LLBConfigurationDialog();
	    	config.setCancelable(false);
	    	config.show(getSupportFragmentManager(), "LLB-CONFIG");
	    	return false;
		} catch (CannotOpenKeystoreException e) {
			return false;
		}
	}
	 
}
