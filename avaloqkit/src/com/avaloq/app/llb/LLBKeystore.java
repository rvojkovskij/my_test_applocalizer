package com.avaloq.app.llb;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.util.Log;
import ch.intellicard.mks.api.MKSEntryPoint;
import ch.intellicard.mks.api.MKSMobileKeyStore;
import ch.intellicard.mks.api.MKSMutableConfig;
import ch.intellicard.mks.fake.MKSEntryPointFake;
import ch.intellicard.mks.fake.MKSUserDatabaseFake;

import com.avaloq.framework.AvaloqApplication;

public class LLBKeystore {
	
	private static class MKSFakeUserEntry {
		
		private String userId;
		private String buId;
		private String password;
		// private String resName;
		private String fileName;
		private boolean needPwChange;

		public MKSFakeUserEntry(String userId, String buId, String password, String resName, String fileName, boolean needPwChange) {
			this.userId = userId;
			this.buId = buId;
			this.password = password;
			// this.resName = resName;
			this.fileName = fileName;
			this.needPwChange = needPwChange;
		}
		
	}
	
	private static MKSFakeUserEntry[] fakeUserEntries = new MKSFakeUserEntry[] {
		new MKSFakeUserEntry("190101", "2", "AFSdemo2", "190101_2", "190101_2.p12", false),
		new MKSFakeUserEntry("llbflt1", "2", "password1", "llbflt1_2", "llbflt1_2.p12", false),
		new MKSFakeUserEntry("llbflt2", "2", "password1", "llbflt2_2", "llbflt2_2.p12", false),
		new MKSFakeUserEntry("llbflt3", "2", "password1", "llbflt3_2", "llbflt3_2.p12", false),
		new MKSFakeUserEntry("1004754", "2", "LLBtest1", "1004754_2", "1004754_2.p12", false),
		new MKSFakeUserEntry("1009933", "2", "LLBtest1", "1009933_2", "1009933_2.p12", false),
		new MKSFakeUserEntry("1071665", "2", "LLBtest1", "1071665_2", "1071665_2.p12", false),
		new MKSFakeUserEntry("1131422", "2", "LLBtest1", "1131422_2", "1131422_2.p12", false),
		new MKSFakeUserEntry("1230678", "2", "LLBtest1", "1230678_2", "1230678_2.p12", false),
		new MKSFakeUserEntry("8014792", "2", "LLBtest1", "8014792_2", "8014792_2.p12", false),
		new MKSFakeUserEntry("8099793", "2", "LLBtest1", "8099793_2", "8099793_2.p12", false),
		new MKSFakeUserEntry("1030751", "2", "LLBtest1", "1030751_2", "1030751_2.p12", false),
		new MKSFakeUserEntry("user196519", "2", "user196519", "user196519_2", "user196519_2.p12", false),
		new MKSFakeUserEntry("UID-190102", "2", "pwd2", "UID-190102_2", "UID-190102_2.p12", true)
	};
	
	// Constants
	
	private static final String CONFIG_KEY_DEFAULT = "default";
	
	private static final String CONFIG_KEY_SERVER_URLS = "Webservices_serverURLs";

	private static final String CONFIG_KEY_LAST_USED_ROOT_SERVER_URL = "WebServices_lastUsedRootServerURL";

	private static final String CONFIG_KEY_ENROLLMENT_LOGIN_URL = "Intellicard_EnrollmentLoginURL";

	private static final String CONFIG_KEY_ENROLLMENT_VERIFICATION_URL = "Intellicard_EnrollmentVerificationURL";

	private static final String CONFIG_KEY_REQUEST_KEY_MATERIAL_URL = "Intellicard_RequestKeyMaterialURL";

	private static final String CONFIG_KEY_LOGIN_VERIFICATION_URL = "Intellicard_LoginVerificationURL";

	private static final String CONFIG_KEY_AUTHENTICATION_BASE_URL = "Intellicard_AuthenticationBaseURL";

	private static final String CONFIG_KEY_AUTHENTICATION_COOKIE_NAME = "Intellicard_AuthenticationCookieName";

	private static final String CONFIG_KEY_NETWORK_TIMEOUT = "Intellicard_NetworkTimeout";

	private static final String DEFAULT_ENROLLMENT_LOGIN_URL = "https://mbanking.llb.li/mbanking-authen/mobile-enroll";

	private static final String DEFAULT_ENROLLMENT_VERIFICATION_URL = "https://mbanking.llb.li/mbanking-authen-cert/mobile-enroll";

	private static final String DEFAULT_REQUEST_KEY_MATERIAL_URL = "https://mbanking.llb.li/mbanking-authen/mobile-login";

	private static final String DEFAULT_LOGIN_VERIFICATION_URL = "https://mbanking.llb.li/mbanking-authen-cert/mobile-login";
	
	private static final String DEFAULT_AUTHENTICATION_BASE_URL = "https://mbanking.llb.li";

	private static final String DEFAULT_AUTHENTICATION_COOKIE_NAME = "SES_SESS-S";

	private static final int DEFAULT_NETWORK_TIMEOUT = 60;

	private static final String TAG = LLBKeystore.class.getSimpleName();
	
	private static final MKSEntryPoint entryPoint = MKSEntryPointFake.getSharedInstance();

	private static MKSMobileKeyStore mobileKeyStore = null;
	
	private static MKSMutableConfig mobileKeyStoreConfig = null;
	
	private static boolean mobileKeyStoreOpen = false;
	
	static {
		MKSUserDatabaseFake fakeDatabase = MKSEntryPointFake.createUserDatabase();
		if(entryPoint instanceof MKSEntryPointFake) {
			copyAssets();
			for(MKSFakeUserEntry fakeUser : fakeUserEntries) {
				fakeDatabase.addUserIdentifier(fakeUser.userId, fakeUser.buId, fakeUser.password);
				/*fakeDatabase.setRequirePasswordChangeForUserIdentifier(fakeUser.userId, fakeUser.buId, fakeUser.needPwChange);*/
				Log.d(TAG, "Added user: " + fakeUser.userId);
			}
			fakeDatabase.storeToSettings(AvaloqApplication.getContext());
		}
	}
	
	private static boolean isCertificateFile(String fileName) {
		if(fileName == null) {
			return false;
		}
		for(MKSFakeUserEntry entry : fakeUserEntries) {
			if(fileName.equals(entry.fileName)) {
				return true;
			}
		}
		return false;
	}
	
	private static void copyAssets() {
	    AssetManager assetManager = AvaloqApplication.getContext().getAssets();
	    String[] files = null;
	    try {
	        files = assetManager.list("");
	    } catch (IOException e) {
	        Log.e("tag", "Failed to get asset file list.", e);
	    }
	    for(String filename : files) {
	    	if(!isCertificateFile(filename)) {
	    		continue;
	    	}
	        InputStream in = null;
	        OutputStream out = null;
	        try {
	          in = assetManager.open(filename);
	          String outFileName = AvaloqApplication.getContext().getFilesDir().getAbsolutePath() + "/MobileKeyStore/" + filename;
	          Log.d(TAG, "Copying file: " + outFileName);
	          File outFile = new File(outFileName);
	          File parentDir = outFile.getParentFile();
	          if(!parentDir.exists()) {
	        	  parentDir.mkdirs();
	          }
	          if(!outFile.exists()) {
		          outFile.createNewFile();
	          }
	          out = new FileOutputStream(outFile);
	          copyFile(in, out);
	          in.close();
	          in = null;
	          out.flush();
	          out.close();
	          out = null;
	        } catch(IOException e) {
	            Log.e("tag", "Failed to copy asset file: " + filename, e);
	        }       
	    }
	}
	
	private static void copyFile(InputStream in, OutputStream out) throws IOException {
	    byte[] buffer = new byte[1024];
	    int read;
	    while((read = in.read(buffer)) != -1){
	      out.write(buffer, 0, read);
	    }
	}
	
	public static String listToString(List<String> strings) {
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < strings.size(); i++) {
			if(i != 0) sb.append("|");
			sb.append(strings.get(i));
		}
		return sb.toString();
	}
	
	public static List<String> stringToList(String str) {
		List<String> strings = new ArrayList<String>(Arrays.asList(str.split("|")));
		if(strings.contains("")) {
			strings.remove("");
		}
		return strings;
	}
	
	/**
	 * Creates the mobile keystore.
	 * @return The mobile keystore
	 * @throws ConfigurationNotInitializedException Is thrown when the configuration was not initialized
	 * @throws CannotOpenKeystoreException Is thrown when the configuration was not initialized
	 */
	public static MKSMobileKeyStore getApplicationMobileKeystore() throws ConfigurationNotInitializedException, CannotOpenKeystoreException {
		
		if(mobileKeyStore == null) {
			mobileKeyStore = entryPoint.createMobileKeyStore();
			mobileKeyStoreConfig = null;
		}

		Log.d(TAG, "mobileKeyStore: " + mobileKeyStore);
		
		if(mobileKeyStoreConfig == null) {
			mobileKeyStoreConfig = loadConfig();
		}

		Log.d(TAG, "mobileKeyStoreConfig: " + mobileKeyStoreConfig);
		
		if(!mobileKeyStoreOpen) {
			Log.d(TAG, "Opening keystore");
			Log.d(TAG, "AuthenticationBaseURL: " + mobileKeyStoreConfig.getAuthenticationBaseURL());
			Log.d(TAG, "AuthenticationCookieName: " + mobileKeyStoreConfig.getAuthenticationCookieName());
			Log.d(TAG, "EnrollmentLoginURL: " + mobileKeyStoreConfig.getEnrollmentLoginURL());
			Log.d(TAG, "EnrollmentVerificationURL: " + mobileKeyStoreConfig.getEnrollmentVerificationURL());
			Log.d(TAG, "KeyStoreDirectoryName: " + mobileKeyStoreConfig.getKeyStoreDirectoryName());
			Log.d(TAG, "LoginVerificationURL: " + mobileKeyStoreConfig.getLoginVerificationURL());
			Log.d(TAG, "NetworkTimeout: " + mobileKeyStoreConfig.getNetworkTimeout());
			Log.d(TAG, "RequestKeyMaterialURL: " + mobileKeyStoreConfig.getRequestKeyMaterialURL());
			Log.d(TAG, "Context: " + mobileKeyStoreConfig.getContext());
			if(mobileKeyStore.open(mobileKeyStoreConfig)) {
				mobileKeyStoreOpen = true;
			} else {
				throw new CannotOpenKeystoreException();
			}
		}

		Log.d(TAG, "mobileKeyStoreOpen: " + mobileKeyStoreOpen);
		
		return mobileKeyStore;
	}
	
	/**
	 * Returns the name of the Keystore implementation
	 * @return The name of the Keystore implementation
	 */
	public static String getName() {
		return entryPoint.getName();
	}
	
	/**
	 * Returns the version of the Keystore implementation
	 * @return The version of the Keystore implementation
	 */
	public static String getVersion() {
		return entryPoint.getVersion();
	}
	
	/**
	 * Creates a new instance of the configuration object
	 * @return A configuration object
	 */
	public static MKSMutableConfig createConfig() {
		return entryPoint.createMobileKeyStore().createConfig();
	}
	
	/**
	 * Loads the configuration from the SharedPreferences
	 * @return The configuration
	 * @throws ConfigurationNotInitializedException Is thrown when the configuration was not initialized
	 */
	private static MKSMutableConfig loadConfig() throws ConfigurationNotInitializedException {
		
		MKSMutableConfig config = mobileKeyStore.createConfig();
		
		config.setContext(AvaloqApplication.getContext());
		
		SharedPreferences sharedPreferences = AvaloqApplication.getContext().getSharedPreferences(CONFIG_KEY_DEFAULT, Context.MODE_PRIVATE);
		
		if(sharedPreferences.contains(CONFIG_KEY_LAST_USED_ROOT_SERVER_URL)) {
			String configKey = sharedPreferences.getString(CONFIG_KEY_LAST_USED_ROOT_SERVER_URL, "");
			config.setAuthenticationBaseURL(configKey);
			sharedPreferences = AvaloqApplication.getContext().getSharedPreferences(config.getKeyStoreDirectoryName(), Context.MODE_PRIVATE);
		} else {
			throw new ConfigurationNotInitializedException();
		}

		// Set the Intellicard library configuration from the selected configuration parameters
		if (sharedPreferences.contains(CONFIG_KEY_ENROLLMENT_LOGIN_URL)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_ENROLLMENT_LOGIN_URL + " => " + sharedPreferences.getString(CONFIG_KEY_ENROLLMENT_LOGIN_URL, DEFAULT_ENROLLMENT_VERIFICATION_URL));
			config.setEnrollmentLoginURL(sharedPreferences.getString(CONFIG_KEY_ENROLLMENT_LOGIN_URL, DEFAULT_ENROLLMENT_LOGIN_URL));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_ENROLLMENT_LOGIN_URL);
			config.setEnrollmentLoginURL(DEFAULT_ENROLLMENT_LOGIN_URL);
		}

		if (sharedPreferences.contains(CONFIG_KEY_ENROLLMENT_VERIFICATION_URL)) {
			Log.d(TAG, "Getting value from the shared preferences: " + DEFAULT_ENROLLMENT_VERIFICATION_URL + " => " + sharedPreferences.getString(DEFAULT_ENROLLMENT_VERIFICATION_URL, DEFAULT_ENROLLMENT_VERIFICATION_URL));
			config.setEnrollmentVerificationURL(sharedPreferences.getString(CONFIG_KEY_ENROLLMENT_VERIFICATION_URL, DEFAULT_ENROLLMENT_VERIFICATION_URL));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_ENROLLMENT_VERIFICATION_URL);
			config.setEnrollmentVerificationURL(DEFAULT_ENROLLMENT_VERIFICATION_URL);
		}

		if (sharedPreferences.contains(CONFIG_KEY_REQUEST_KEY_MATERIAL_URL)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_REQUEST_KEY_MATERIAL_URL + " => " + sharedPreferences.getString(CONFIG_KEY_REQUEST_KEY_MATERIAL_URL, DEFAULT_REQUEST_KEY_MATERIAL_URL));
			config.setRequestKeyMaterialURL(sharedPreferences.getString(CONFIG_KEY_REQUEST_KEY_MATERIAL_URL, DEFAULT_REQUEST_KEY_MATERIAL_URL));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_REQUEST_KEY_MATERIAL_URL);
			config.setRequestKeyMaterialURL(DEFAULT_REQUEST_KEY_MATERIAL_URL);
		}

		if (sharedPreferences.contains(CONFIG_KEY_LOGIN_VERIFICATION_URL)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_LOGIN_VERIFICATION_URL + " => " + sharedPreferences.getString(CONFIG_KEY_LOGIN_VERIFICATION_URL, DEFAULT_LOGIN_VERIFICATION_URL));
			config.setLoginVerificationURL(sharedPreferences.getString(CONFIG_KEY_LOGIN_VERIFICATION_URL, DEFAULT_LOGIN_VERIFICATION_URL));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_LOGIN_VERIFICATION_URL);
			config.setLoginVerificationURL(DEFAULT_LOGIN_VERIFICATION_URL);
		}

		if (sharedPreferences.contains(CONFIG_KEY_AUTHENTICATION_BASE_URL)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_AUTHENTICATION_BASE_URL + " => " + sharedPreferences.getString(CONFIG_KEY_AUTHENTICATION_BASE_URL, DEFAULT_AUTHENTICATION_BASE_URL));
			config.setAuthenticationBaseURL(sharedPreferences.getString(CONFIG_KEY_AUTHENTICATION_BASE_URL, DEFAULT_AUTHENTICATION_BASE_URL));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_AUTHENTICATION_BASE_URL);
			config.setAuthenticationBaseURL(DEFAULT_AUTHENTICATION_BASE_URL);
		}

		if (sharedPreferences.contains(CONFIG_KEY_AUTHENTICATION_COOKIE_NAME)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_AUTHENTICATION_COOKIE_NAME + " => " + sharedPreferences.getString(CONFIG_KEY_AUTHENTICATION_COOKIE_NAME, DEFAULT_AUTHENTICATION_COOKIE_NAME));
			config.setAuthenticationCookieName(sharedPreferences.getString(CONFIG_KEY_AUTHENTICATION_COOKIE_NAME, DEFAULT_AUTHENTICATION_COOKIE_NAME));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_AUTHENTICATION_COOKIE_NAME);
			config.setAuthenticationCookieName(DEFAULT_AUTHENTICATION_COOKIE_NAME);
		}

		if (sharedPreferences.contains(CONFIG_KEY_NETWORK_TIMEOUT)) {
			Log.d(TAG, "Getting value from the shared preferences: " + CONFIG_KEY_NETWORK_TIMEOUT + " => " + sharedPreferences.getInt(CONFIG_KEY_NETWORK_TIMEOUT, DEFAULT_NETWORK_TIMEOUT));
			config.setNetworkTimeout(sharedPreferences.getInt(CONFIG_KEY_NETWORK_TIMEOUT, DEFAULT_NETWORK_TIMEOUT));
		} else {
			Log.e(TAG, "Intellicard configuration item missing: " + CONFIG_KEY_NETWORK_TIMEOUT);
			config.setNetworkTimeout(DEFAULT_NETWORK_TIMEOUT);
		}

		config.setEnforceZeroSizeURLCache(false);
		
		return config;
	}
	
	/**
	 * Saves the configuration values from an object to the SharedPreferences
	 * @param config The configuration
	 */
	public static void saveConfig(MKSMutableConfig config) {
		
		SharedPreferences defaultSharedPreferences = AvaloqApplication.getContext().getSharedPreferences(CONFIG_KEY_DEFAULT, Context.MODE_PRIVATE);
		
		Log.d(TAG, "directory name: " + config.getKeyStoreDirectoryName());
		
		List<String> strings = stringToList(defaultSharedPreferences.getString(CONFIG_KEY_SERVER_URLS, ""));
		
		String authBaseUrl = config.getAuthenticationBaseURL();
		
		if(!strings.contains(authBaseUrl)) {
			strings.add(authBaseUrl);
		}
		
		Editor defaultEditor = defaultSharedPreferences.edit();
		
		defaultEditor.putString(CONFIG_KEY_SERVER_URLS, listToString(strings));
		
		defaultEditor.putString(CONFIG_KEY_LAST_USED_ROOT_SERVER_URL, authBaseUrl);
		
		defaultEditor.commit();
		
		SharedPreferences sharedPreferences = AvaloqApplication.getContext().getSharedPreferences(CONFIG_KEY_DEFAULT, Context.MODE_PRIVATE);
		
		Editor editor = sharedPreferences.edit();

		if(config.getEnrollmentLoginURL() != null)
			editor.putString(CONFIG_KEY_ENROLLMENT_LOGIN_URL, config.getEnrollmentLoginURL());

		if(config.getEnrollmentVerificationURL() != null)
			editor.putString(CONFIG_KEY_ENROLLMENT_VERIFICATION_URL, config.getEnrollmentVerificationURL());

		if(config.getRequestKeyMaterialURL() != null)
			editor.putString(CONFIG_KEY_REQUEST_KEY_MATERIAL_URL, config.getRequestKeyMaterialURL());

		if(config.getLoginVerificationURL() != null)
			editor.putString(CONFIG_KEY_LOGIN_VERIFICATION_URL, config.getLoginVerificationURL());

		if(config.getAuthenticationBaseURL() != null)
			editor.putString(CONFIG_KEY_AUTHENTICATION_BASE_URL, config.getAuthenticationBaseURL());

		if(config.getAuthenticationCookieName() != null)
			editor.putString(CONFIG_KEY_AUTHENTICATION_COOKIE_NAME, config.getAuthenticationCookieName());

		if(config.getNetworkTimeout() != 0)
			editor.putInt(CONFIG_KEY_NETWORK_TIMEOUT, config.getNetworkTimeout());
		
		editor.commit();
		
	}

	/**
	 * Returns the saved configurations 
	 * @return The saved configurations
	 */
	public static List<String> getSavedConfigurations() {
		SharedPreferences defaultSharedPreferences = AvaloqApplication.getContext().getSharedPreferences(CONFIG_KEY_DEFAULT, Context.MODE_PRIVATE);
		return stringToList(defaultSharedPreferences.getString(CONFIG_KEY_SERVER_URLS, ""));
	}
	
	public static final class CannotOpenKeystoreException extends Exception {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 7045710876586955268L;

		private CannotOpenKeystoreException() {
			super("The keystore cannot be opened.");
		}
		
	}
	
	public static final class ConfigurationNotInitializedException extends Exception {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -2487156194497501309L;

		private ConfigurationNotInitializedException() {
			super("The configuration was not initialized.");
		}
		
	}

}
