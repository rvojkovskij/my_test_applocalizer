package com.avaloq.app.llb;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;

import com.avaloq.framework.AppConfigurationInterface;

/**
 * Contains this application's configuration
 */
public class AppConfiguration implements AppConfigurationInterface {
	
	// private static final String TAG = AppConfiguration.class.getSimpleName();
	
	/**
	 * Banklets used in this  application. They are aranged in the given order.
	 */
	public Class<?>[] getBanklets() {
		Class<?> banklets[] = {
				com.avaloq.banklet.wealth.WealthBanklet.class,
				com.avaloq.banklet.trading.TradingBanklet.class,
				com.avaloq.banklet.collaboration.CollaborationBanklet.class,
				com.avaloq.banklet.documentsafe.DocumentSafeBanklet.class,
                com.avaloq.banklet.payments.PaymentBanklet.class
		};
		return banklets;
	}

	/**
	 * Returns the Authentication Handler Classes for this app
	 * @return An array of Authentication handler classes
	 */
	@Override
	public Class<?>[] getAuthenticationHandlers() {
		Class<?> authHandlers[] = {
				LLBAuthenticationHandler.class				
		};
		return authHandlers;
	}

	public String getBaseUrl() {
		return "http://tuxdmzdev02.dmz.avaloq.com:9080";
	}
	
	@Override
	public String getWebserviceBaseUrl() {
		return getBaseUrl() + "/frontAggregationServices/";
	}
	
	@Override
	public Class<? extends Activity> getStartupActivity() {
		return LaunchActivity.class;
	}

	@Override
	public int getServerSessionDefaultTimeout() {
		return 300;
	}

	@Override
	public boolean showExpDateOnFundSale() {
		return false;
	}
	
	@Override
	public boolean showWealthCharts() {
		return true;
	}
	
	private static HashMap<String, String> endpointMap = new HashMap<String,String>();
	 
	static { 
		endpointMap.put("LLB Test Server", "https://mbanking-test.llb.li/frontAggregationServices/");
		endpointMap.put("Avaloq Demo Server", "http://afsdemo1.avaloq.com/frontAggregationServices/");
		endpointMap.put("Avaloq Dev Server", "http://tuxdmzdev01.dmz.avaloq.com:9080/frontAggregationServices/");
		endpointMap.put("Ergon Test Server", "https://llbgate.ergon.ch/frontAggregationServices/");
		endpointMap.put("Intellicard Test Server", "https://medusa.llb.loc:9443/frontAggregationServices/");
	}
	
	@Override
	public Map<String, String> getApplicationEndpoints() {
		return endpointMap;
	}

	@Override
	public boolean hideMarketData() {		
		return false;
	}

	@Override
	public boolean allowedSellFundsByAmount() {
		return false;
	}

	@Override
	public String[] getSSLTrustedPins() {
		String[] pins = new String[] {
				"c07a98688d89fbab05640c117daa7d65b8cacc4e"  // GeoTrust
				};
		return pins;
	}
}
