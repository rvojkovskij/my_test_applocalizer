package com.avaloq.app.llb;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import ch.intellicard.mks.api.MKSDeviceRegistrationDelegate;
import ch.intellicard.mks.api.MKSException;
import ch.intellicard.mks.api.MKSRegistrationAuthenticationDelegate;

import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.app.llb.LLBRegistrationFirstStepFragment.RegistrationFirstStepFragmentInterface;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;
import com.avaloq.framework.ui.DialogFragment;

public class LLBRegistrationThirdStepFragment extends BankletFragment 
implements OnEditorActionListener, MKSDeviceRegistrationDelegate, MKSRegistrationAuthenticationDelegate {
	
	public interface LLBRegistrationThirdStepFragmentInterface{
		public void registrationSuccessful();
		public void informBadQRCode();
	}
	
	private String mScanResult="";
	
	private EditText mEdtPassword;
	
	private String mEncryptedUrl;
	private String mUserIdentifier;
	private String mBusinessUnitIdentifier;
	private String mPassword;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.llb_registration_third, container, false);
		
		mEdtPassword = (EditText) fragmentView.findViewById(R.id.avq_registration_password);

		mEdtPassword.setOnEditorActionListener(this);
		
        Bundle b = getArguments();
        if(b != null) mScanResult = b.getString("SCAN_RESULT");
		
        if (!parseContent()){
        	((RegistrationFirstStepFragmentInterface) getActivity()).continueAfterLogin();
        	((LLBRegistrationThirdStepFragmentInterface) getActivity()).informBadQRCode();
        }
        
        displayUserId(fragmentView);
        
		return fragmentView;
	}

	private boolean parseContent(){
		// The expected format of the data in the QR code is UID,BUID,URL
	    // where:
	    // UID is the user identifier for the ebanking contract
	    // BUID is the business unit identifier for the ebanking contract
	    // URL is the encrypted registration URL
	    
	    if (mScanResult != null && mScanResult.length() > 8) {
	        String[] parts = mScanResult.split(",");
	        
	        if (parts.length == 3){
	        	mUserIdentifier = parts[0];
	        	mBusinessUnitIdentifier = parts[1];
	        	mEncryptedUrl = parts[2];
	        	return true;
	        }
	    }
		return false;
	}
	
	private void authenticateUser(){
		try {			
			LLBKeystore.getApplicationMobileKeystore().authenticateRegistration(
					mUserIdentifier, 
					mBusinessUnitIdentifier, 
					mPassword, 
					this);
		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void registerDevice(){
		try {			
			LLBKeystore.getApplicationMobileKeystore().registerDevice(
					mEncryptedUrl,
					mUserIdentifier, 
					mBusinessUnitIdentifier, 
					mPassword, 
					this);
		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void displayUserId(View fragmentView){
		((TextView)fragmentView.findViewById(R.id.avq_registration_user_identifier)).setText(mUserIdentifier);
	}
	
	@Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		
        if (EditorInfo.IME_ACTION_DONE == actionId ) {
        	mPassword = mEdtPassword.getText().toString();
        	
        	if (mPassword.length() > 0){
        		authenticateUser();
        	}else{
        		// TODO inform that the password is empty
        	}
        	
            return true;
        }
        
        return false;
    }

	@Override
	public void registrationRequestFailed(MKSException arg0, boolean arg1) {		
		DialogFragment.createAlert(getSherlockActivity().getString(R.string.avq_registration_registration_failed), arg0.getLocalizedMessage(), getActivity()).show(getActivity());
	}

	@Override
	public void registrationRequestSuccessful() {
		((LLBRegistrationThirdStepFragmentInterface) getActivity()).registrationSuccessful();		
	}

	@Override
	public void registrationAuthenticationRequestFailed(MKSException arg0) {
		DialogFragment.createAlert(getSherlockActivity().getString(R.string.avq_registration_authentication_failed), arg0.getLocalizedMessage(), getActivity()).show(getActivity());
	}

	@Override
	public void registrationAuthenticationRequestSuccessful() {
		registerDevice();		
	}
}
