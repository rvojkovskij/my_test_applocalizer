package com.avaloq.app.llb;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import ch.intellicard.mks.api.MKSDeviceID;
import ch.intellicard.mks.api.MKSDeviceIdentifierDelegate;
import ch.intellicard.mks.api.MKSException;

import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.framework.R;
import com.avaloq.framework.ui.BankletFragment;

public class LLBRegistrationSecondStepFragment extends BankletFragment {
	
	private TextView textDeviceIdentifier;
	private Button buttonContinue;
	
	public interface RegistrationSecondStepFragmentInterface{
		public void continueToCodeScan();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View fragmentView = inflater.inflate(R.layout.llb_registration_second, container, false);

		textDeviceIdentifier = (TextView)fragmentView.findViewById(R.id.avq_registration_code);
		
		buttonContinue = (Button) ((Button) fragmentView.findViewById(R.id.avq_register_next_step));
		buttonContinue.setEnabled(false);
		buttonContinue.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {				
				((RegistrationSecondStepFragmentInterface) getActivity()).continueToCodeScan();
			}
		});

		displayRegistrationCode(fragmentView);
		
		return fragmentView;
	}
		
	private void displayRegistrationCode(final View fragmentView){
		
		try {
			if (LLBKeystore.getApplicationMobileKeystore().hasUniqueDeviceIdentifier()){
				showIdentifier();
			}else{
				LLBKeystore.getApplicationMobileKeystore().generateUniqueDeviceIdentifier(new MKSDeviceIdentifierDelegate() {
					
					@Override
					public void uniqueDeviceIdentifierFailed(MKSException arg0) {
						// TODO Auto-generated method stub						
					}
					
					@Override
					public void uniqueDeviceIdentifierAvailable(MKSDeviceID arg0) {
						showIdentifier();						
					}
				});
			}
		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	private void showIdentifier(){
		try {
			textDeviceIdentifier.setText(LLBKeystore.getApplicationMobileKeystore().getUniqueDeviceIdentifier());
		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buttonContinue.setEnabled(true);
	}
}
