package com.avaloq.app.llb;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.app.llb.LLBRegistrationFirstStepFragment.RegistrationFirstStepFragmentInterface;
import com.avaloq.app.llb.LLBRegistrationListFragment.RegistrationListFragmentInterface;
import com.avaloq.app.llb.LLBRegistrationSecondStepFragment.RegistrationSecondStepFragmentInterface;
import com.avaloq.app.llb.LLBRegistrationThirdStepFragment.LLBRegistrationThirdStepFragmentInterface;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.ui.DialogFragment;

public class LLBRegistrationActivity extends SherlockFragmentActivity
		implements RegistrationListFragmentInterface,
		RegistrationFirstStepFragmentInterface,
		RegistrationSecondStepFragmentInterface,
		LLBRegistrationThirdStepFragmentInterface
		{
	
	private static final String TAG = LLBRegistrationActivity.class.getSimpleName();

	public enum RegistrationActivityPageType {
		LIST,
		FIRST_STEP,
		SECOND_STEP,
		THIRD_STEP 
	}

	//test comment
	
	public static final String EXTRA_FIRST_PAGE_TYPE = "FIRST_PAGE_TYPE";

	private String mScanresult="";
	
	private RegistrationActivityPageType mPageType;
//	private static RegistrationActivityPageType mLandingPage;

	private final int REQUEST_SCAN = 10; // Request code for Intent result

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);

		savedInstanceState.putInt("current_page", mPageType.ordinal());
		savedInstanceState.putString("scan_result", mScanresult);
	}
	
	/**
	 * 
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.d(TAG, "onCreate");
		
		// get the saved page from the bundle
		if (savedInstanceState != null){
			mPageType = RegistrationActivityPageType.values()[savedInstanceState.getInt("current_page")];
			mScanresult = savedInstanceState.getString("scan_result");
		}
		

		setContentView(R.layout.llb_registration_activity);

		if (mPageType == null) {
			// Getting the extra arguments
			Bundle b = getIntent().getExtras();
			try {
				mPageType = (RegistrationActivityPageType) b.getSerializable(EXTRA_FIRST_PAGE_TYPE);
				
				if (mPageType == RegistrationActivityPageType.LIST && LLBKeystore.getApplicationMobileKeystore().getRegisteredUserIDs().length == 0){
					mPageType = RegistrationActivityPageType.FIRST_STEP;
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		showStep(mPageType);
	}

	/**
	 * 
	 * @param type
	 */
	private void showStep(final RegistrationActivityPageType type) {
		
		Log.d("DDD show step", type.name());
		mPageType = type;
		
		new Handler().post(new Runnable() {
			public void run() {
				
				FragmentManager fragmentManager = getSupportFragmentManager();

				Fragment newFragment;

				if (type == RegistrationActivityPageType.LIST) {
					newFragment = new LLBRegistrationListFragment();
				} else if (type == RegistrationActivityPageType.FIRST_STEP) {
					newFragment = new LLBRegistrationFirstStepFragment();
				} else if (type == RegistrationActivityPageType.SECOND_STEP) {
					newFragment = new LLBRegistrationSecondStepFragment();
				} else if (type == RegistrationActivityPageType.THIRD_STEP) {
					newFragment = new LLBRegistrationThirdStepFragment();
					Bundle args = new Bundle();
					args.putString("SCAN_RESULT", mScanresult);
					newFragment.setArguments(args);
					
				} else {
					newFragment = new LLBRegistrationFirstStepFragment();
				}

				FragmentTransaction transaction = fragmentManager.beginTransaction();

				// Replace whatever is in the fragment_container view with this fragment

				transaction.replace(R.id.registration_fragment_container, newFragment);
				/*if (type != mLandingPage && type != RegistrationActivityPageType.LIST){
					Log.d("DDD add to back stack", mPageType.name());
					//transaction.addToBackStack(null);
				}*/

				// Commit the transaction
				transaction.commit();
			}
		});
	}
	
	/**
	 * Used to intercept the back button.
	 * @param keyCode The Key Code
	 * @param event The Key Event
	 * @return true, if it's the back button.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * 
	 */
	@Override
	public void onBackPressed(){		
		try {
			if (mPageType == RegistrationActivityPageType.LIST || (mPageType == RegistrationActivityPageType.FIRST_STEP && LLBKeystore.getApplicationMobileKeystore().getRegisteredUserIDs().length == 0)){

				Log.d(TAG, "onBackPressed");
				
				AuthenticationManager.getInstance().authenticationCancelledByUser();
				
				Intent intent = new Intent(this, AvaloqApplication.getInstance().getConfiguration().getStartupActivity());
				intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
				overridePendingTransition(0, 0);
			}
			else
			{			
				showStep(RegistrationActivityPageType.values()[mPageType.ordinal()-1]);
			}
		} catch (ConfigurationNotInitializedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CannotOpenKeystoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*********************************************************************************
	 * From the Registration list fragment
	 * 
	 *********************************************************************************/
	/**
	 * Open the fragment for registration of new users
	 */
	@Override
	public void registerNewUserId() {
		mPageType = RegistrationActivityPageType.FIRST_STEP;
		showStep(mPageType);
	}

	/*********************************************************************************
	 * From the first step fragment
	 * 
	 *********************************************************************************/
	/**
	 * Open the step two fragment
	 */
	@Override
	public void continueAfterLogin() {
		mPageType = RegistrationActivityPageType.SECOND_STEP;
		showStep(mPageType);
	}

	/*********************************************************************************
	 * From the second step fragment
	 * 
	 *********************************************************************************/
	/**
	 * Open the scanning activity
	 */
	@Override
	public void continueToCodeScan() {	
		String packageString = this.getPackageName();
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
		intent.setPackage(packageString);

		// Add any optional extras to pass
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE");

		intent.putExtra("PROMPT_MESSAGE", getString(R.string.avq_registration_scanner_hint));
		
		intent.putExtra("RESULT_DISPLAY_DURATION_MS", 0l);
		
		// Launch
		startActivityForResult(intent, REQUEST_SCAN);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		Log.d("DDD scan result", "received wrong scan result");
		if (requestCode == REQUEST_SCAN) {
			if (resultCode == RESULT_OK) {
				String contents = intent.getStringExtra("SCAN_RESULT");
				// String format = intent.getStringExtra("SCAN_RESULT_FORMAT");

				// Handle successful scan

				mPageType = RegistrationActivityPageType.THIRD_STEP;
				mScanresult = contents;
				showStep(mPageType);

			} else if (resultCode == RESULT_CANCELED) {
				// Handle cancel
			}
		}
	}

	@Override
	public void registrationSuccessful() {
		finish();
		
		Intent intent = new Intent(this, LLBLoginActivity.class);
		intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
		intent.putExtra(LLBLoginActivity.EXTRA_REGISTRATION_SUCCESSFUL, true);
		startActivity(intent);
		

	}

	@Override
	public void informBadQRCode() {
		DialogFragment.createAlert("", getString(R.string.avq_registration_bad_qr_code), this).show(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
	}

}
