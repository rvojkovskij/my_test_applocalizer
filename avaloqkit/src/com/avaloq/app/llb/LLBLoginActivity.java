package com.avaloq.app.llb;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import ch.intellicard.mks.api.MKSDeviceAuthenticationDelegate;
import ch.intellicard.mks.api.MKSException;
import ch.intellicard.mks.api.MKSPasswordChangeContext;
import ch.intellicard.mks.api.MKSUserID;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.avaloq.app.llb.LLBKeystore.CannotOpenKeystoreException;
import com.avaloq.app.llb.LLBKeystore.ConfigurationNotInitializedException;
import com.avaloq.app.llb.LLBRegistrationActivity.RegistrationActivityPageType;
import com.avaloq.framework.AvaloqApplication;
import com.avaloq.framework.R;
import com.avaloq.framework.comms.QueueManager;
import com.avaloq.framework.comms.RequestStateEvent;
import com.avaloq.framework.comms.authentication.AuthenticationManager;
import com.avaloq.framework.comms.authentication.simplepost.SimplePostAuthenticationRequest;
import com.avaloq.framework.ui.DialogFragment;

/**
 * This activity handles the UI aspect of a specific authentication handler (SimplePostAuthenticationHandler).
 * It is not meant as an independent login screen usable for different authentication handlers.
 * 
 * TODO: Base this one also on AbstractUsernamePasswordActivity 
 *
 */
public class LLBLoginActivity extends SherlockFragmentActivity implements OnEditorActionListener {
	
	private static final String TAG = LLBLoginActivity.class.getSimpleName();

	private LinearLayout mLoginBox;
	private ProgressBar mProgress;

	// private EditText mUsername;
	private EditText mPassword;
	private EditText mContract;
	
	private LLBAuthenticationHandler authHandler; 

	public static final String EXTRA_REGISTRATION_SUCCESSFUL = "EXTRA_REGISTRATION_SUCCESSFUL";
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		
		Log.d(TAG, "onCreate");
		
		setContentView(R.layout.llb_login_dialog);
		
		// Get our authentication handler
		authHandler = (LLBAuthenticationHandler) AuthenticationManager.getInstance().getAuthenticationHandler(LLBAuthenticationHandler.class);
		if (authHandler == null) {
			// This is not recoverable.
			throw new IllegalStateException("LoginActivity could not retrieve its authentication provider.");
		}    // Protect against problems initializing the keystore

		mLoginBox = (LinearLayout)findViewById(R.id.avq_login_fieldbox);
		mProgress = (ProgressBar)findViewById(R.id.avq_login_progress);
		
		mLoginBox.setVisibility(View.VISIBLE);
		mProgress.setVisibility(View.GONE);
		
		// mUsername = (EditText)findViewById(R.id.avq_login_username);
		mPassword = (EditText)findViewById(R.id.avq_login_password);
		mContract = (EditText)findViewById(R.id.avq_login_contract);
		
		// mUsername.setOnEditorActionListener(this);
		mPassword.setOnEditorActionListener(this);
		mContract.setOnEditorActionListener(this);
		
		mPassword.setTypeface(Typeface.DEFAULT);
		mPassword.setTransformationMethod(new PasswordTransformationMethod());
		
	    try {
	        MKSUserID userId = LLBKeystore.getApplicationMobileKeystore().getPreferredUserID();
	        if(userId != null) {
		        mContract.setText(userId.getUserIdentifier());
	        }
	    } catch (ConfigurationNotInitializedException e) {
	        // Display a localized error message
//	    	new AlertDialog.Builder(this).setTitle("Keystore unavailable").setMessage("Reason: " + e.getMessage()).create();
//	    	Log.d(TAG, "keystore unavailable: "+e.getMessage());
	    	
	    	// show the login dialog
	    	LLBConfigurationDialog config = new LLBConfigurationDialog();
	    	config.setCancelable(false);
	    	config.show(getSupportFragmentManager(), "LLB-CONFIG");
	    } catch (CannotOpenKeystoreException e) {
	    	new AlertDialog.Builder(this).setTitle("Keystore unavailable").setMessage("Reason: " + e.getMessage()).create();
	    	Log.d(TAG, "keystore unavailable: "+e.getMessage());
	    }
		
        
		// Convenience hack
        ((ImageView) findViewById(R.id.avq_login_headerimg)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// mUsername.setText("demo");
				mPassword.setText("AFSdemo1");
				mContract.setText("190101");
			}
		});
        
        ((ImageButton) findViewById(R.id.avq_login_settings)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showRegistration();
			}
		});
        
        // Show registration successful message
        Bundle extras = getIntent().getExtras(); 

        if (extras != null) {
            if (extras.getBoolean(EXTRA_REGISTRATION_SUCCESSFUL)){
            	DialogFragment.createAlert("Registration successful", "Your device has been successfully registered, and may now be used for mobile banking", this).show(this);
            }
        }
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
        	new AsyncTask<Void, Void, SimplePostAuthenticationRequest>() {
        		protected void onPreExecute() {
        			mLoginBox.setVisibility(View.GONE);
        			mProgress.setVisibility(View.VISIBLE);
        		};
    			// Step 2 of 3: Execute POST req. immediately (no queueing)
    			@Override
    			protected SimplePostAuthenticationRequest doInBackground(Void... params) {
    				SimplePostAuthenticationRequest securityRequest = new SimplePostAuthenticationRequest(
							new RequestStateEvent<SimplePostAuthenticationRequest>() {},
							"demo",
							mPassword.getText().toString(),
							mContract.getText().toString()
    				);
    				
    				// Execute and handle the response
    				securityRequest.executeRequest();    				
    				if (securityRequest.getResponse() != null) {
    					
    					// Since we bypass the queue, we need to manually timestamp this request 
    					QueueManager.getInstance().markTimeOfLastExecutedRequest();
    					
    					// Handle the returned data
    					securityRequest.getResponse().onParseServerResponseData();
    				}
    				return securityRequest;
    			}						

    			@Override
    			protected void onPostExecute(SimplePostAuthenticationRequest securityRequest) {
    				
    				// Step 3 of 3: Evaluate response
    				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LLBLoginActivity.this);	// FIXME: This is not a good context-giver as it can be null
    				
    				// Network error
    				if (securityRequest.getResponse() == null) {
    					Log.w(TAG, "No answer from SimplePostAuth request");

    					alertDialogBuilder
    						.setTitle(R.string.avq_login_not_possible_title)
    						.setMessage(R.string.avq_login_not_possible_text)
    						.setPositiveButton(R.string.avq_ok, null);
    					DialogFragment.create(alertDialogBuilder.create()).show();

    					securityRequest.setRequestStateFailed();
    					
    					mProgress.setVisibility(View.GONE);
    					mLoginBox.setVisibility(View.VISIBLE);

    				} else if (securityRequest.getResponse().wasRequestSuccessfullyExecuted()) {
    					
    					// Successful login
    					Log.i(TAG, "Login successful.");
    					
    					securityRequest.setRequestStateCompleted();    					    					
    					authHandler.setAuthenticationCompleted(true);
    					
    					// TODO Find out if we have to finish now...
                        /*
    					if(AvaloqApplication.getInstance().isInitialLoginDone()) {
        					Log.d(TAG, "finishing");
        					finish();
    					} else {
        					AvaloqApplication.getInstance().setInitialLoginDone(true);
        					Log.d(TAG, "initial login done");
    					}
    					*/
                        AvaloqApplication.getInstance().startFirstActivity(LLBLoginActivity.this);
                        finish();
    				} else {
    					
    					// Invalid login
    					Log.i(TAG, "Login invalid.");
    					securityRequest.setRequestStateFailed();
    					alertDialogBuilder
    						.setTitle(R.string.avq_login_invalid_title)
    						.setMessage(R.string.avq_login_invalid_text)
    						.setPositiveButton(R.string.avq_login_invalid_button, null);
    					DialogFragment frag = DialogFragment.create(alertDialogBuilder.create());
    					frag.setCancelable(false);
    					frag.show();
    					
    					mPassword.setText("");
    					
    					mProgress.setVisibility(View.GONE);
    					mLoginBox.setVisibility(View.VISIBLE);
    					
    				}
    			}

    		}.execute();
			
			try {
				LLBKeystore.getApplicationMobileKeystore().authenticateDevice(mContract.getText().toString(), "2", mPassword.getText().toString(), new MKSDeviceAuthenticationDelegate() {
					
					@Override
					public void authenticationRequestSuccessful() {
						Log.d(TAG, "authenticationRequestSuccessful");
					}
					
					@Override
					public void authenticationRequestFailed(MKSException exc) {
						Log.d(TAG, "authenticationRequestFailed");
					}
					
					@Override
					public void authenticationRequiresNewPassword(MKSPasswordChangeContext context) {
						Log.d(TAG, "authenticationRequiresNewPassword");
					}

				});
			} catch (ConfigurationNotInitializedException e) {
				e.printStackTrace();
			} catch (CannotOpenKeystoreException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
			
			
//        	new AsyncTask<Void, Void, TestSecurityRequest>() {
//        		protected void onPreExecute() {
//        			mLoginBox.setVisibility(View.GONE);
//        			mProgress.setVisibility(View.VISIBLE);
//        		};
//    			// Step 2 of 3: Execute POST req. immediately (no queueing)
//    			@Override
//    			protected TestSecurityRequest doInBackground(Void... params) {
//    				
//    				TestSecurityRequest securityRequest = new TestSecurityRequest(
//							new RequestStateEvent<TestSecurityRequest>() {},
//							mUsername.getText().toString(),
//							mPassword.getText().toString(),
//							mContract.getText().toString()
//    				);
//    				
//    				// LLBKeystore.getApplicationMobileKeystore().authenticateDevice(arg0, arg1, arg2, arg3)
//    				
//    				// Execute and handle the response
//    				securityRequest.executeRequest();    				
//    				if (securityRequest.getResponse() != null) {
//    					
//    					// Since we bypass the queue, we need to manually timestamp this request 
//    					QueueManager.getInstance().markTimeOfLastExecutedRequest();
//    					
//    					// Handle the returned data
//    					securityRequest.getResponse().onParseServerResponseData();
//    				}
//    				return securityRequest;
//    			}						
//
//    			@Override
//    			protected void onPostExecute(TestSecurityRequest securityRequest) {
//    				
//    				// Step 3 of 3: Evaluate response
//    				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LLBLoginActivity.this);	// FIXME: This is not a good context-giver as it can be null
//    				
//    				// Network error
//    				if (securityRequest.getResponse() == null) {
//    					Log.w(TAG, "No answer from SimplePostAuth request");
//
//    					alertDialogBuilder
//    						.setTitle(R.string.avq_login_not_possible_title)
//    						.setMessage(R.string.avq_login_not_possible_text)
//    						.setPositiveButton(R.string.avq_ok, null);
//    					DialogFragment.create(alertDialogBuilder.create()).show();
//
//    					securityRequest.setRequestStateFailed();
//    					
//    					mProgress.setVisibility(View.GONE);
//    					mLoginBox.setVisibility(View.VISIBLE);
//
//    				} else if (securityRequest.getResponse().wasRequestSuccessfullyExecuted()) {
//    					
//    					// Successful login
//    					Log.i(TAG, "Login successful.");
//    					
//    					securityRequest.setRequestStateCompleted();    					    					
//    					authHandler.setAuthenticationCompleted(true);
//    					
//    					// TODO Find out if we have to finish now...
//    					if(AvaloqApplication.getInstance().isInitialLoginDone()) {
//        					Log.d(TAG, "finishing");
//        					finish();
//    					} else {
//        					AvaloqApplication.getInstance().setInitialLoginDone(true);
//        					Log.d(TAG, "initial login done");
//    					}
//    					
//    				} else {
//    					
//    					// Invalid login
//    					Log.i(TAG, "Login invalid.");
//    					securityRequest.setRequestStateFailed();
//    					alertDialogBuilder
//    						.setTitle(R.string.avq_login_invalid_title)
//    						.setMessage(R.string.avq_login_invalid_text)
//    						.setPositiveButton(R.string.avq_login_invalid_button, null);
//    					DialogFragment frag = DialogFragment.create(alertDialogBuilder.create());
//    					frag.setCancelable(false);
//    					frag.show();
//    					
//    					mUsername.setText("");
//    					mPassword.setText("");
//    					mContract.setText("");
//    					
//    					mProgress.setVisibility(View.GONE);
//    					mLoginBox.setVisibility(View.VISIBLE);
//    					
//    				}
//    			}
//
//    		}.execute();
            return true;
        }
        
        return false;
    }
    
    @Override
    public void onBackPressed() {
    	Log.d(TAG, "cancelling requests");
    	// QueueManager.getInstance().dump(TAG);
    	AuthenticationManager.getInstance().authenticationCancelledByUser();
    	Intent startMain = new Intent(Intent.ACTION_MAIN);
    	startMain.addCategory(Intent.CATEGORY_HOME);
    	startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	Log.d(TAG, "going to home screen");
    	startActivity(startMain);
    }
	
	private void showRegistration() {
		// start the registration activity 
		Intent intent = new Intent(LLBLoginActivity.this, LLBRegistrationActivity.class);
		
		// show the list of registered IDs
		intent.putExtra(LLBRegistrationActivity.EXTRA_FIRST_PAGE_TYPE, RegistrationActivityPageType.LIST);				
		//intent.addFlags(intent.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY);
		
		startActivity(intent);
	}
	
}
