/*******************************************************************************
 * Copyright (c) 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * This package provides the {@link com.google.gson.Gson} class to convert Json to Java and
 * vice-versa.
 *
 * <p>The primary class to use is {@link com.google.gson.Gson} which can be constructed with
 * {@code new Gson()} (using default settings) or by using {@link com.google.gson.GsonBuilder}
 * (to configure various options such as using versioning and so on).</p>
 *
 * @author Inderjeet Singh, Joel Leitch
 */
package com.google.gson;