/*******************************************************************************
 * Copyright (c) 2013 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
/**
 * Do NOT use any class in this package as they are meant for internal use in Gson.
 * These classes will very likely change incompatibly in future versions. You have been warned.
 *
 * @author Inderjeet Singh, Joel Leitch, Jesse Wilson
 */
package com.google.gson.internal;