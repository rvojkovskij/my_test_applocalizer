package com.avaloq.med.avaloqocrservices;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class TestActivity extends Activity{ 
	
	private final int REQUEST_CODE = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
	}

	public void startClick(View v){
		Intent intent = new Intent(this, OrangePaymentSlipActivity.class);
		startActivityForResult(intent, REQUEST_CODE);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == REQUEST_CODE){
			if (resultCode == RESULT_OK){
				Toast.makeText(this, "Scan successful", Toast.LENGTH_SHORT).show();
				String strData = data.getStringExtra("data");
				if (strData != null){
					((TextView) findViewById(R.id.txtResult)).setText(strData);
				}
				
			}else if(resultCode == RESULT_CANCELED){
				Toast.makeText(this, "Scan cancelled", Toast.LENGTH_SHORT).show();
			}
		}
	}
}
