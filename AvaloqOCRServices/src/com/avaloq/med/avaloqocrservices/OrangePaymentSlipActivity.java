package com.avaloq.med.avaloqocrservices;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.avaloq.med.avaloqocrservices.OrangePaymentSlipFragment.OrangePaymentSlipDelegate;

public class OrangePaymentSlipActivity extends FragmentActivity implements OrangePaymentSlipDelegate {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orange_payment_slip);
	} 

	@Override
	public void scanningCanceledByUser() {	
		finish();
	}

	@Override
	public void scanningFinnished(OrangePaymentSlipData resultData) {
		if(resultData != null) {
			Log.d("Scanning result", "Account: " + resultData.accountNumber);
			Log.d("Scanning result", "Ref. number: " + resultData.referenceNumber);
			Log.d("Scanning result", "Amount: " + resultData.amount);
			Log.d("Scanning result", "Slip type: " + resultData.slipType);
			Log.d("Scanning result", "Code: " + resultData.codeLine);
			
			Intent intent = new Intent();
			
			intent.putExtra("data", resultData.codeLine);
			setResult(RESULT_OK, intent);
			
		}
		finish();
	}
}
