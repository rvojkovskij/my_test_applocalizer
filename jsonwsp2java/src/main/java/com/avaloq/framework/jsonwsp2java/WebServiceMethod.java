package com.avaloq.framework.jsonwsp2java;

import java.util.Map;

/**
 * Holds all Information for a Web Service Method
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class WebServiceMethod {

	/**
	 * Comment Lines for this Method
	 */
	private String[] doc_lines;
	
	/**
	 * The Parameters for this method
	 */
	private Map<String,WebServiceParam> params;
	
	/**
	 * The Return-Info for this method
	 */
	private WebServiceReturnInfo ret_info;
	
	public String[] getDocLines() {
		return doc_lines;
	}
	
	public void setDocLines(String[] doc_lines) {
		this.doc_lines = doc_lines;
	}
	
	public Map<String, WebServiceParam> getParams() {
		return params;
	}
	
	public void setParams(Map<String, WebServiceParam> params) {
		this.params = params;
	}
	
	public WebServiceReturnInfo getRetInfo() {
		return ret_info;
	}
	
	public void setRetInfo(WebServiceReturnInfo ret_info) {
		this.ret_info = ret_info;
	}
	
	public String getCleanServiceName() {
		final String type = ret_info.getType();
		if(type.endsWith("Result")) {
			return type.substring(0, type.length() - 6) + "Request";
		} else {
			throw new IllegalStateException("The Method's return type does not end with 'Result'.");
		}
	}
	
}
