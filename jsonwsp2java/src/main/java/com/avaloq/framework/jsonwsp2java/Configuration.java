package com.avaloq.framework.jsonwsp2java;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.Properties;

public final class Configuration extends Properties {

	private static final long serialVersionUID = 3074106420215110978L;
	
	/**
	 * Key for the Output Directory
	 */
	private static final String PROPERTY_OUTPUT_DIRECTORY = "jsonwsp2java.outputDirectory";

	/**
	 * URL for the Webservices to be built
	 */
	private static final String PROPERTY_WEBSERVICE_URL = "jsonwsp2java.webserviceUrl";

	/**
	 * Key for the Webservices to be built
	 */
	private static final String PROPERTY_WEBSERVICES = "jsonwsp2java.webservices";
	
	/**
	 * Key for the Model Package
	 */
	private static final String PROPERTY_PACKAGE_MODEL = "jsonwsp2java.package.model";
	
	/**
	 * Key for the Service Package
	 */
	private static final String PROPERTY_PACKAGE_SERVICE = "jsonwsp2java.package.service";
	
	/**
	 * Singleton Instance
	 */
	private static Configuration instance = null;
	
	public static final Configuration getInstance() throws IOException {
		if(instance == null) {
			instance = new Configuration();
			instance.load(ClassLoader.getSystemResourceAsStream("jsonwsp2java.properties"));
		}
		return instance;
	}
	
	public String[] getWebservices() {
		return getProperty(PROPERTY_WEBSERVICES, "").split(",");
	}
	
	public String getOutputDirectory() {
		return getProperty(PROPERTY_OUTPUT_DIRECTORY, ".");
	}
	
	public String getWebServiceUrl(final String webServiceName) {
		return new MessageFormat(getProperty(PROPERTY_WEBSERVICE_URL, ".")).format(new String[] { webServiceName });
	}
	
	public String getServicePackageName(final String webServiceName) {
		return new MessageFormat(getProperty(PROPERTY_PACKAGE_SERVICE, "com.avaloq.framework.comms.webservice.{0}")).format(new String[] { webServiceName });
	}
	
	public String getModelPackageName(final String webServiceName) {
		return new MessageFormat(getProperty(PROPERTY_PACKAGE_MODEL, "com.avaloq.framework.comms.webservice.{0}.model")).format(new String[] { webServiceName });
	}
	
}