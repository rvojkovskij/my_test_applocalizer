package com.avaloq.framework.jsonwsp2java;


public class WebServiceParam {
	
	private int def_order;
	private String[] doc_lines;
	private String type;
	private boolean optinal;
	
	public int getDefOrder() {
		return def_order;
	}
	
	public void setDefOrder(int def_order) {
		this.def_order = def_order;
	}
	
	public String[] getDocLines() {
		return doc_lines;
	}
	
	public void setDocLines(String[] doc_lines) {
		this.doc_lines = doc_lines;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public boolean isOptinal() {
		return optinal;
	}
	
	public void setOptinal(boolean optinal) {
		this.optinal = optinal;
	}

}
