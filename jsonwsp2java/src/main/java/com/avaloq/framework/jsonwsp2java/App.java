package com.avaloq.framework.jsonwsp2java;

import java.io.IOException;
import java.util.HashMap;

import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.avaloq.afs.aggregation.to.framework.ClientAppInfoTO;

/**
 * Runs the Web-Service generator
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class App {
	
	static {
    	Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM, new Slf4jLogChute());
    	Velocity.init();
	}
	
	/**
	 * The Logger for this Class
	 */
	private static Logger log = LoggerFactory.getLogger(App.class);
	
	private static WebServiceGenerator generator;
	
	/**
	 * Runs the Application
	 * @param args Command-Line arguments
	 */
    public static void main(final String[] args) {
    	try {
	    	Configuration configuration = Configuration.getInstance();
	    	final String[] services = configuration.getWebservices();
	    	for(final String serviceName : services) {
	    		log.info("Generating Web Service: " + serviceName);
	    		generator = new WebServiceGenerator(serviceName, configuration);
//	    		old code (now we use the server-side jars)
//	    		generator.createWebserviceModels();
    			generator.createWebserviceServiceClass();
    			generator.createWebserviceResultClasses();
    			/*
	    		final String upperCaseServiceName = serviceName.substring(0, 1).toUpperCase() + serviceName.substring(1, serviceName.length());
	        	final String webServiceUrl = "http://afsdemo3.avaloq.com/frontAggregationServices/" + upperCaseServiceName + "Service?description";
	        	log.info("Using WebService-URL: "+webServiceUrl);
	        	try {
	        		final WebServiceSpecification wsSpec = getWebServiceSpecification(serviceName, webServiceUrl);
	    			createWebserviceTypes(serviceName, wsSpec);
	    			createWebserviceMethods(serviceName, wsSpec);
	        		log.info("Got the specification for the Webservice "+wsSpec.getServiceName()+".");
	        	} catch (Exception e) {
	        		log.error("Failed to get the Web Service Specification.");
	        		log.error(e.getMessage(), e);
	        	}
	        	*/
	    	}
    	} catch(IOException e) {
    		log.error(e.getMessage(), e);
    	}
    	/*
    	*/
    }
    
    private static final HashMap<String,String> fullClassNames = new HashMap<String,String>();

	public static String getFQCN(String originalType) throws IOException {
		if(fullClassNames.containsKey(originalType)) {
			return fullClassNames.get(originalType);
		} else {
			
	    	String[] possiblePackageNames = new String[] { 
	    			"com.avaloq.afs.server.bsp.client.ws",
	    			"com.avaloq.afs.aggregation.to", 
	    			"com.avaloq.front.common.util.validation", 
	    			"com.avaloq.afs.aggregation.to.documents",
	    			"com.avaloq.afs.aggregation.to.framework",
	    			"com.avaloq.afs.aggregation.to.investmentproposition",
	    			"com.avaloq.afs.aggregation.to.marketdata",
	    			"com.avaloq.afs.aggregation.to.payment",
	    			"com.avaloq.afs.aggregation.to.payment.domestic",
	    			"com.avaloq.afs.aggregation.to.payment.internal",
	    			"com.avaloq.afs.aggregation.to.payment.international",
	    			"com.avaloq.afs.aggregation.to.payment.orange",
	    			"com.avaloq.afs.aggregation.to.payment.red",
	    			"com.avaloq.afs.aggregation.to.wealth",
	    			"com.avaloq.afs.aggregation.to.stex",
	    			"com.avaloq.afs.aggregation.to.trading",
	    			"com.avaloq.afs.aggregation.to.user",
	    			"com.avaloq.afs.aggregation.to.user.settings"
	    	};
	    	for(String possiblePackageName : possiblePackageNames) {
	    		String possibleClassName = possiblePackageName + "." + originalType;
	    		try {
					Class<?> clazz = Class.forName(possibleClassName);
					fullClassNames.put(originalType, clazz.getCanonicalName());
					return clazz.getCanonicalName();
				} catch (ClassNotFoundException e) {
					log.debug("Class was not found: " + originalType);
				}
	    	}
			String clazzName = generator.createWebserviceModel(originalType);
			if(clazzName != null) {
				fullClassNames.put(originalType, clazzName);
				return clazzName;
			}
	    	throw new IOException("Class does not exist and cannot be generated: " + originalType);
		}
	}
    
}