package com.avaloq.framework.jsonwsp2java;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Holds all information for a Web Service Type.
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class WebServiceType {
	
	Logger log = LoggerFactory.getLogger(WebServiceType.class);
	
	private Map<String,String> members;
	
	private Map<String,String> memberLists;
	
	public Map<String, String> getMembers() {
		return members;
	}
	
	public void setMembers(Map<String, String> members) {
		this.members = members;
	}
	
	public Map<String, String> getMemberLists() {
		return memberLists;
	}
	
	public void setMemberLists(Map<String, String> memberLists) {
		this.memberLists = memberLists;
	}
	
	public List<WebServiceField> getFieldList() {
		final List<WebServiceField> fieldList = new ArrayList<WebServiceField>();
		for(final Entry<String,String> entry : getMembers().entrySet()) {
			fieldList.add(new WebServiceField(entry.getKey(), entry.getValue()));
		}
		for(final Entry<String,String> entry : getMemberLists().entrySet()) {
			fieldList.add(new WebServiceField(entry.getKey(), "java.util.List<"+entry.getValue()+">"));
		}
		return fieldList;
	}

}
