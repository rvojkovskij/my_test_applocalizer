package com.avaloq.framework.jsonwsp2java;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class WebServiceGenerator {
	
	private static final Logger log = Logger.getLogger(WebServiceGenerator.class);
	
	private final String name;
	
	private final Configuration configuration;
	
	private final WebServiceSpecification webServiceSpecification;
	
	protected WebServiceGenerator(final String name, Configuration configuration) throws IOException {
		assert(name != null);
		assert(!"".equals(name));
		this.name = name;
		this.configuration = configuration;
		this.webServiceSpecification = getWebServiceSpecification();
	}
	
	private String getModelPackageName() {
		return configuration.getModelPackageName(this.name.toLowerCase());
	}
	
	private String getServicePackageName() {
		return configuration.getServicePackageName(this.name.toLowerCase());
	}
	
	private String getServiceClassName() {
		return this.name + "Service";
	}

	/**
     * Loads the Web Service Description from HTTP and Maps it to a Java-Object using GSON
     * @param webServiceUrl The Web Service URL
     * @return The Specification as an Object
	 * @throws IOException 
	 * @throws ClientProtocolException 
     * @throws Exception If an error occurs, an exception of any type is thrown
     */
    private WebServiceSpecification getWebServiceSpecification() throws ClientProtocolException, IOException {
    	final Gson gson = new GsonBuilder()
    							.registerTypeAdapter(WebServiceType.class, new WebServiceTypeDeserializer())
    							.registerTypeAdapter(WebServiceParam.class, new WebServiceParamDeserializer())
    							.create();
    	final HttpClient httpClient = new DefaultHttpClient();
    	final String requestUrl = configuration.getWebServiceUrl(this.name);
    	final HttpGet httpGet = new HttpGet(requestUrl);
    	final HttpResponse httpResponse = httpClient.execute(httpGet);
    	final int httpStatusCode = httpResponse.getStatusLine().getStatusCode();
    	if(HttpStatus.SC_OK == httpStatusCode) {
    		
    		final InputStreamReader isr = new InputStreamReader(httpResponse.getEntity().getContent());
    		final WebServiceSpecification wsSpec = gson.fromJson(isr, WebServiceSpecification.class);
    		isr.close();
    		return wsSpec;
    	} else {
    		throw new IOException("Failed to fetch: " + requestUrl + "\nThe HTTP Status from the Server is not ok: " + httpStatusCode);
    	}
    }
    
    /**
     * Creates the Types for this Web Service
     * @param name The name of the model to generate
     * @throws IOException If a file cannot be written for any reason
     */
    public String createWebserviceModel(String name) throws IOException {
    	final Template template = Velocity.getTemplate("src/main/resources/type.class.vm");
    	for(Entry<String,WebServiceType> type : webServiceSpecification.getTypes().entrySet()) {
    		if(type.getKey().equals(name)) {
        		final String packageName = getModelPackageName();
        		final String packageDirectory = packageName.replaceAll("\\.", File.separator);
        		final String destDir = configuration.getOutputDirectory();
        		final File destFile = new File(destDir + packageDirectory + File.separator + type.getKey() + ".java");
        		new File(destFile.getParent()).mkdirs();
        		destFile.createNewFile();
    	    	final VelocityContext context = new VelocityContext();
    	    	context.put("packageName", packageName);
    	    	context.put("className", type.getKey());
    	    	context.put("fieldList", type.getValue().getFieldList());
    	    	FileWriter fw = new FileWriter(destFile, false);
    	    	template.merge(context, fw);
    	    	fw.close();
    	    	return packageName + "." + name;
    		} else {
    			continue;
    		}
    	}
    	return null;
	}
    
    /**
     * Creates the Webservice Methods in the Service Class
     * @throws IOException If the Web Service Class file cannot be written
     */
    public void createWebserviceServiceClass() throws IOException {
    	final Template template = Velocity.getTemplate("src/main/resources/service.class.vm");
		final String packageName = getServicePackageName();
		final String packageDirectory = packageName.replaceAll("\\.", File.separator);
		final String destDir = configuration.getOutputDirectory();
    	final String serviceClassName = getServiceClassName();
		final File destFile = new File(destDir + packageDirectory + File.separator + serviceClassName + ".java");
		new File(destFile.getParent()).mkdirs();
		destFile.createNewFile();
    	final VelocityContext context = new VelocityContext();
    	context.put("packageName", packageName);
    	context.put("className", serviceClassName);
    	context.put("webserviceType", getWebServiceSpecification().getType());
    	context.put("webserviceVersion", getWebServiceSpecification().getVersion());
    	context.put("modelPackage", getModelPackageName());
    	context.put("serviceUrl", webServiceSpecification.getUrl());
    	context.put("serviceName", getWebServiceSpecification().getServiceName());
    	context.put("methodList", webServiceSpecification.getMethods().entrySet());
    	FileWriter fw = new FileWriter(destFile, false);
    	template.merge(context, fw);
    	fw.close();
    }
    
    /**
     * Creates the Webservice Result Classes
     * @throws IOException If the Web Service Class file cannot be written
     */
    public void createWebserviceResultClasses() throws IOException {
    	final List<String> resultClasses = new ArrayList<String>();
    	for(Entry<String,WebServiceMethod> entry : webServiceSpecification.getMethods().entrySet()) {
    		final String requestClass = entry.getValue().getCleanServiceName();
    		if(resultClasses.contains(requestClass)) {
    			continue;
    		} else {
    			resultClasses.add(requestClass);
    		}
	    	final Template template = Velocity.getTemplate("src/main/resources/result.class.vm");
			final String packageName = getServicePackageName();
			final String packageDirectory = packageName.replaceAll("\\.", File.separator);
			final String destDir = configuration.getOutputDirectory();
			final File destFile = new File(destDir + packageDirectory + File.separator + requestClass + ".java");
			new File(destFile.getParent()).mkdirs();
			destFile.createNewFile();
	    	final VelocityContext context = new VelocityContext();
	    	context.put("packageName", packageName);
	    	context.put("className", requestClass);
	    	context.put("serviceClassName", getServiceClassName());
	    	context.put("resultClassName", App.getFQCN(entry.getValue().getRetInfo().getType()));
	    	context.put("webserviceType", getWebServiceSpecification().getType());
	    	context.put("webserviceVersion", getWebServiceSpecification().getVersion());
	    	context.put("modelPackage", getModelPackageName());
	    	context.put("serviceName", getWebServiceSpecification().getServiceName());
	    	context.put("serviceUrl", webServiceSpecification.getUrl());
	    	FileWriter fw = new FileWriter(destFile, false);
	    	template.merge(context, fw);
	    	fw.close();
    	}
    }
    
    /**
     * Implementation of JsonDeserializer<WebServiceType>.
     * Used to deserialize the WebServiceType class
     * @author Timo Schmid <t.schmid@insign.ch>
     */
    private class WebServiceTypeDeserializer implements JsonDeserializer<WebServiceType> {
    	
    	private WebServiceTypeDeserializer() {
		}

    	/**
    	 * Does the actual work
    	 * @param jsonElement The Json Element for the Type
    	 * @param type The type of the Json Element
    	 * @param context The current Json Context
    	 */
		public WebServiceType deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
			try {
				if(jsonElement.isJsonObject()) {
					final WebServiceType webServiceType = new WebServiceType();
					webServiceType.setMembers(new HashMap<String,String>());
					webServiceType.setMemberLists(new HashMap<String,String>());
					final JsonObject jsonObject = jsonElement.getAsJsonObject();
					for(final Entry<String,JsonElement> mapEntry : jsonObject.entrySet()) {
						if(mapEntry.getValue().isJsonArray()) {
							// We have a list of objects
							webServiceType.getMemberLists().put(mapEntry.getKey(), getNormalizedType(mapEntry.getValue().getAsString()));
						} else if (mapEntry.getValue().isJsonPrimitive()) {
							webServiceType.getMembers().put(mapEntry.getKey(), getNormalizedType(mapEntry.getValue().getAsString()));
						}
					}
					return webServiceType;
				} else {
					throw new JsonParseException("The element for a type is not a Json Object.");
				}
			} catch (IOException e) {
				throw new JsonParseException(e);
			}
		}
    }
    
    /**
     * Implementation of JsonDeserializer<WebServiceParam>.
     * Used to deserialize the WebServiceType class
     * @author Timo Schmid <t.schmid@insign.ch>
     */
    private class WebServiceParamDeserializer implements JsonDeserializer<WebServiceParam> {
    	
    	private WebServiceParamDeserializer() {
		}

    	/**
    	 * Does the actual work
    	 * @param jsonElement The Json Element for the Type
    	 * @param type The type of the Json Element
    	 * @param context The current Json Context
    	 */
		public WebServiceParam deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
			try {
				if(jsonElement.isJsonObject()) {
					final WebServiceParam webServiceParam = new WebServiceParam();
					final JsonObject jsonObject = jsonElement.getAsJsonObject();
					webServiceParam.setDefOrder(jsonObject.get("def_order").getAsInt());
					JsonArray docLines = jsonObject.get("doc_lines").getAsJsonArray();
					final String[] strDocLines = new String[docLines.size()];
					for(int i = 0;i < docLines.size();i++) {
						strDocLines[i] = docLines.get(i).getAsString();
					}
					webServiceParam.setDocLines(strDocLines);
					final JsonElement typeElement = jsonObject.get("type");
					if(typeElement.isJsonArray()) {
						// We have a list of objects
						webServiceParam.setType(getNormalizedType(typeElement.getAsJsonArray().get(0).getAsString()));
					} else {
						webServiceParam.setType(getNormalizedType(typeElement.getAsString()));
					}
					webServiceParam.setOptinal(jsonObject.get("optional").getAsBoolean());
					return webServiceParam;
				} else {
					throw new JsonParseException("The element for a type is not a Json Object.");
				}
			} catch (IOException e) {
				throw new JsonParseException(e);
			}
		}
    }
    
    /**
     * Normalizes a type from the Web-Service to it's Java Pendent
     * @param originalType The name of the original Type, from the Web Service
     * @return The Java Type for the Web-Service's Type
     * @throws IOException 
     */
    private String getNormalizedType(final String originalType) throws IOException {
    	if(originalType.equals("integer")) {
    		return "Long";
    	}
    	if(originalType.equals("decimal")) {
    		return "Double";
    	}
    	if(originalType.equals("string")) {
    		return "String";
    	}
    	if(originalType.equals("boolean")) {
    		return "Boolean";
    	}
    	if(originalType.equals("date")) {
    		return "java.util.Date";
    	}
    	// TODO Check if there are more types we have to implement, Wikipedia did not even know about all of the above
    	// log.warn("Unknown Type: "+originalType);
    	//return getModelPackageName() + "." + originalType;
    	
    	// String possibleClassName = getModelPackageName() + "." + originalType;
    	
    	return App.getFQCN(originalType);
    }
	
}
