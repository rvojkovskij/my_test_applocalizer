package com.avaloq.framework.jsonwsp2java;

public class WebServiceField {
	
	private final String fieldName;
	private final String dataType;

	public WebServiceField(final String fieldName, final String dataType) {
		this.fieldName = fieldName;
		this.dataType = dataType;
	}
	
	public String getFieldName() {
		return fieldName;
	}
	
	public String getDataType() {
		return dataType;
	}

	public String getGetterName() {
		return "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
	}

	public String getSetterName() {
		return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1, fieldName.length());
	}

}
