package com.avaloq.framework.jsonwsp2java;

/**
 * Holds all Information for a Web Service's return value
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class WebServiceReturnInfo {

	/**
	 * The Lines of Documentation for this return statement
	 */
	private String[] doc_lines;
	
	/**
	 * The type of object that is returned
	 */
	private String type;
	
	public String[] getDocLines() {
		return doc_lines;
	}
	
	public void setDocLines(String[] doc_lines) {
		this.doc_lines = doc_lines;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
}
