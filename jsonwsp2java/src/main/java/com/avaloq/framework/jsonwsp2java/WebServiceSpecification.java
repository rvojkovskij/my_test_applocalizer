package com.avaloq.framework.jsonwsp2java;

import java.util.Map;

/**
 * Holds all the information for a specific Web Service
 * @author Timo Schmid <t.schmid@insign.ch>
 */
public class WebServiceSpecification {

	/**
	 * The Type of this Webservice
	 */
	private String type;
	
	/**
	 * The Version Name/Number
	 */
	private String version;
	
	/**
	 * The Web Service Name
	 */
	private String servicename;
	
	/**
	 * The Web Service URL
	 */
	private String url;
	
	/**
	 * The Types in this Web Service
	 */
	private Map<String,WebServiceType> types;
	
	/**
	 * The Methods in this Web Service
	 */
	private Map<String,WebServiceMethod> methods;
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getServiceName() {
		
		String s[] = this.url.split("/");
		return s[s.length-1];
		
		//return servicename;
	}
	
	public void setServiceName(String servicename) {
		this.servicename = servicename;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public Map<String, WebServiceType> getTypes() {
		return types;
	}
	
	public void setTypes(Map<String, WebServiceType> types) {
		this.types = types;
	}
	
	public Map<String, WebServiceMethod> getMethods() {
		return methods;
	}
	
	public void setMethods(Map<String, WebServiceMethod> methods) {
		this.methods = methods;
	}
	
}
